﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="result-add2.aspx.cs" Inherits="result_add2" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Jquery/Number/jquery.number.js" type="text/javascript"></script>
    <script src="Javascript/Jquery/Number/jquery.number.min.js" type="text/javascript"></script>
    <style type="text/css">
        .cHideControl
        {
            display: none;
        }
        
        .cShowControl
        {
            display: block;
        }
        
        /*.cInline
        {
            display: inline;
        }*/
        
        .cTDAlter
        {
            background-color: #F7F7F7;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function CheckNumber(s, e) {

            if (s.GetValue() != null && s.GetValue() != "") {
                var values = s.GetValue().replace(/,/g, '');
                if ($.isNumeric(values)) {
                }
                else {
                    s.SetValue("");
                }
            }
        }

        function CalPipe() {

            if (txtG5_22_1.GetValue() != null && txtG5_22_1.GetValue() != "" && txtG5_23_1.GetValue() != null && txtG5_23_1.GetValue() != "") {

                if (txtG5_22_1.GetValue() == 1) {
                    var Cal = 0.5574 * parseFloat(txtG5_23_1.GetValue());
                    txtG5_23_2.SetText(Cal.toFixed(4));
                }
                else if (txtG5_22_1.GetValue() == 1.25) {
                    var Cal = 0.9665 * parseFloat(txtG5_23_1.GetValue());
                    txtG5_23_2.SetText(Cal.toFixed(4));
                }
                else if (txtG5_22_1.GetValue() == 1.5) {
                    var Cal = 1.3164 * parseFloat(txtG5_23_1.GetValue());
                    txtG5_23_2.SetText(Cal.toFixed(4));
                }
                else if (txtG5_22_1.GetValue() == 2) {
                    var Cal = 2.1631 * parseFloat(txtG5_23_1.GetValue());
                    txtG5_23_2.SetText(Cal.toFixed(4));
                }
                else if (txtG5_22_1.GetValue() == 2.5) {
                    var Cal = 3.0857 * parseFloat(txtG5_23_1.GetValue());
                    txtG5_23_2.SetText(Cal.toFixed(4));
                }
                else if (txtG5_22_1.GetValue() == 3) {
                    var Cal = 4.7686 * parseFloat(txtG5_23_1.GetValue());
                    txtG5_23_2.SetText(Cal.toFixed(4));
                }
                else if (txtG5_22_1.GetValue() == 4) {
                    var Cal = 8.213 * parseFloat(txtG5_23_1.GetValue());
                    txtG5_23_2.SetText(Cal.toFixed(4));
                }
                else if (txtG5_22_1.GetValue() == 6) {
                    var Cal = 18.6458 * parseFloat(txtG5_23_1.GetValue());
                    txtG5_23_2.SetText(Cal.toFixed(4));
                }
                else if (txtG5_22_1.GetValue() == 8) {
                    var Cal = 32.2826 * parseFloat(txtG5_23_1.GetValue());
                    txtG5_23_2.SetText(Cal.toFixed(4));
                }
                else if (txtG5_22_1.GetValue() == 10) {
                    var Cal = 50.8544 * parseFloat(txtG5_23_1.GetValue());
                    txtG5_23_2.SetText(Cal.toFixed(4));
                }
                else if (txtG5_22_1.GetValue() == 12) {
                    var Cal = 76.9467 * parseFloat(txtG5_23_1.GetValue());
                    txtG5_23_2.SetText(Cal.toFixed(4));
                }
                else if (txtG5_22_1.GetValue() == 0) {
                    var Cal = 0 * parseFloat(txtG5_23_1.GetValue());
                    txtG5_23_2.SetText(Cal.toFixed(4));
                }
                else {
                    var Cal = 22 / 7 * txtG5_22_1.GetValue() * 2.64 * txtG5_22_1.GetValue() * 2.64 / 40;
                    txtG5_23_2.SetText(Cal.toFixed(4));
                }

            }
            else {
                txtG5_23_2.SetText('');
            }
        }

        function AllEnable() {
            rblG2_Head_Checking(); Dis10(); Dis11(); Dis22(); Dis29(); Dis45();
        }

        function rblG2_Head_Checking() {
            var Value = rblG2_Head.GetValue();

            if (Value == "Y") {
                rblG2_7.SetEnabled(true);
                rblG2_8.SetEnabled(true);
                rblG2_9.SetEnabled(true);
                txtG2_10_1.SetEnabled(true);
                txtG2_10_2.SetEnabled(true);
                rblG2_10.SetEnabled(true);
                txtG2_11_1.SetEnabled(true);
                txtG2_11_2.SetEnabled(true);
                rblG2_11.SetEnabled(true);
                rblG2_12.SetEnabled(true);
            }
            else {
                rblG2_7.SetEnabled(false);
                rblG2_8.SetEnabled(false);
                rblG2_9.SetEnabled(false);
                txtG2_10_1.SetEnabled(false);
                txtG2_10_2.SetEnabled(false);
                rblG2_10.SetEnabled(false);
                txtG2_11_1.SetEnabled(false);
                txtG2_11_2.SetEnabled(false);
                rblG2_11.SetEnabled(false);
                rblG2_12.SetEnabled(false);


                rblG2_7.SetValue('');
                rblG2_8.SetValue('');
                rblG2_9.SetValue('');
                txtG2_10_1.SetText('');
                txtG2_10_2.SetText('');
                rblG2_10.SetValue('');
                txtG2_11_1.SetText('');
                txtG2_11_2.SetText('');
                rblG2_11.SetValue('');
                rblG2_12.SetValue('');
            }

        }

        function Dis10() {
           

            var state = rblG2_10.GetValue();
            if (state == "W") {
                txtG2_10_1.SetEnabled(false);
                txtG2_10_2.SetEnabled(false);
            }
            else {
                txtG2_10_1.SetEnabled(true);
                txtG2_10_2.SetEnabled(true);
            }

            var Value = rblG2_Head.GetValue();
            if (Value == "N") {
                txtG2_10_1.SetEnabled(false);
                txtG2_10_2.SetEnabled(false);
            }
        }

        function Dis11() {
          

            var state = rblG2_11.GetValue();
            if (state == "W") {
                txtG2_11_1.SetEnabled(false);
                txtG2_11_2.SetEnabled(false);
            }
            else {
                txtG2_11_1.SetEnabled(true);
                txtG2_11_2.SetEnabled(true);
            }

            var Value = rblG2_Head.GetValue();
            if (Value == "N") {
                txtG2_11_1.SetEnabled(false);
                txtG2_11_2.SetEnabled(false);
            }
        }

        function Dis22() {
            var state = ckbG5_21_1.GetCheckState();
            if (state == "Unchecked") {
                txtG5_21_1.SetEnabled(true);
                txtG5_21_2.SetEnabled(true);
            }
            else {
                txtG5_21_1.SetEnabled(false);
                txtG5_21_2.SetEnabled(false);
            }
        }

        function Dis29() {
            var state = rblG7_26.GetValue();
            if (state == "W") {
                txtG7_26_1.SetEnabled(false);
                txtG7_26_2.SetEnabled(false);
            }
            else {
                txtG7_26_1.SetEnabled(true);
                txtG7_26_2.SetEnabled(true);
            }
        }

        function Dis45() {
            var state = rblG10_42.GetValue();
            if (state == "W") {
                txtG10_42_1.SetEnabled(false);
            }
            else {
                txtG10_42_1.SetEnabled(true);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" ClientInstanceName="xcpn" HideContentOnCallback="false"
        CausesValidation="False" OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){  AllEnable(); eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="5" cellspacing="1">
                                            <tr>
                                                <td colspan="4" align="right">
                                                    <%--<input type="submit" name="button3" id="button3" value="ดูประวัติรถ" onClick="javascript:location.href='car_history.htm'">--%>
                                                    <%--<input type="submit" name="button7" id="button7" value="ประวัติการรับบริการ" onClick="javascript:location.href='service_history.htm'">--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#B9EAEF">
                                                    วันที่ยืนคำขอ
                                                </td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblREQUEST_DATE" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td bgcolor="#B9EAEF">
                                                    วันที่หมดอายุวัดน้ำ
                                                </td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblDWATEREXPIRE" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="18%" bgcolor="#B9EAEF">
                                                    วันที่นัดหมาย
                                                </td>
                                                <td width="32%" class="active">
                                                    <dx:ASPxLabel ID="lblSERVICE_DATE" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td width="19%" bgcolor="#B9EAEF">
                                                    วันที่ รข. อนุมัติคำขอ
                                                </td>
                                                <td width="31%">
                                                    <dx:ASPxLabel ID="lblAPPOINTMENT_BY_DATE" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#B9EAEF">
                                                    ประเภทคำขอ
                                                </td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblREQTYPE_NAME" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td bgcolor="#B9EAEF">
                                                    สาเหตุ
                                                </td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblCAUSE_NAME" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#B9EAEF">
                                                    ทะเบียนรถ
                                                </td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblREGISTERNO" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td bgcolor="#B9EAEF">
                                                    ประเภทรถ
                                                </td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblTypeCar" runat="server">
                                                    </dx:ASPxLabel>
                                                    - ความจุ
                                                    <dx:ASPxLabel ID="lblTotalCap" runat="server">
                                                    </dx:ASPxLabel>
                                                    ลิตร
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#B9EAEF">
                                                    บริษัทขนส่ง
                                                </td>
                                                <td colspan="3">
                                                    <dx:ASPxLabel ID="lblVendorName" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr id="trtab">
                                    <td>
                                        <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                            <tr>
                                                <td width="15%" align="center" bgcolor="#EDEDED">
                                                    <dx:ASPxHyperLink ID="hplT1" runat="server" Text="ประวัติการตรวจ" Style="text-decoration: none;
                                                        cursor: pointer; color: Blue;">
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT1;')}" />
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                <td width="15%" align="center" bgcolor="#00CC33">
                                                    <dx:ASPxHyperLink ID="hplT2" runat="server" Text="ตรวจสภาพภายนอก/ใน" Style="text-decoration: none;
                                                        cursor: pointer; color: Blue;">
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                <td width="15%" align="center" bgcolor="#EDEDED">
                                                    <dx:ASPxHyperLink ID="hplT3" runat="server" Text="ผลตรวจลงน้ำ" Style="text-decoration: none;
                                                        cursor: pointer; color: Blue;">
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT3;')}" />
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                <td width="15%" align="center" bgcolor="#EDEDED">
                                                    <dx:ASPxHyperLink ID="hplT4" runat="server" Text="คำนวณค่าธรรมเนียมเพิ่ม" Style="text-decoration: none;
                                                        cursor: pointer; color: Blue;">
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT4;')}" />
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                <td width="15%" align="center" bgcolor="#EDEDED">
                                                    <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text="รูปภาพการตรวจสอบ" Style="text-decoration: none;
                                                        cursor: pointer; color: Blue;">
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectImg;')}" />
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                <td width="15%" align="center" bgcolor="#EDEDED">
                                                    <dx:ASPxHyperLink ID="hplT5" runat="server" Text="บันทึกผลปิดงาน" Style="text-decoration: none;
                                                        cursor: pointer; color: Blue;">
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT5;')}" />
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                <td width="10%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="3" cellspacing="2">
                                            <tr>
                                                <td width="6%" align="left" bgcolor="#B9EAEF">
                                                    ตรวจอุปกรณ์ความปลอดภัย
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                                        <tr>
                                                            <td width="70%" align="left">
                                                                1. สภาพสายไฟ / คัทเอาท์ดี
                                                            </td>
                                                            <td width="30%">
                                                                <dx:ASPxRadioButtonList ID="rblG1_1" runat="server" ClientInstanceName="rblG1_1"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F7F7F7">
                                                                2. แบตเตอร์รี่มีฝาฉนวนครอบ
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <dx:ASPxRadioButtonList ID="rblG1_2" runat="server" ClientInstanceName="rblG1_2"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                3. ถังดับเพลิงเคมี ขนาด 20 ปอนด์
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG1_3" runat="server" ClientInstanceName="rblG1_3"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F7F7F7">
                                                                4. แชชชีย์ไม่ผุ
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <dx:ASPxRadioButtonList ID="rblG1_4" runat="server" ClientInstanceName="rblG1_4"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                5. ท่อไอเสียไม่รั่ว,ไม่ผุ
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG1_5" runat="server" ClientInstanceName="rblG1_5"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F7F7F7">
                                                                6. สายดินประจำรถ 2 จุด
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <dx:ASPxRadioButtonList ID="rblG1_6" runat="server" ClientInstanceName="rblG1_1"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" bgcolor="#B9EAEF" valign="bottom">
                                                    <table width="100%" border="0" cellpadding="0">
                                                        <tr>
                                                            <td width="150px">
                                                                ตรวจอุปกรณ์จรวดหรือไม่
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG2_Head" runat="server" CssClass="cInline" ClientInstanceName="rblG2_Head"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ใช้งาน(ใส่น้ำมัน)" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ใช้งาน" />
                                                                    </Items>
                                                                    <ClientSideEvents ValueChanged="function(){ rblG2_Head_Checking(); }" />
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                                        <tr>
                                                            <td width="70%">
                                                                7. ฐานจรวดหนักไม่เกิน 1.5 หุน
                                                            </td>
                                                            <td width="30%">
                                                                <dx:ASPxRadioButtonList ID="rblG2_7" runat="server" ClientInstanceName="rblG2_7"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                        <dx:ListEditItem Value="W" Text="ไม่มี" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F7F7F7">
                                                                8. ฐานจรวดไม่ปิดข้าง
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <dx:ASPxRadioButtonList ID="rblG2_8" runat="server" ClientInstanceName="rblG2_8"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                        <dx:ListEditItem Value="W" Text="ไม่มี" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                9. มีน๊อตยึดฐานสามารถถอดได้
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG2_9" runat="server" ClientInstanceName="rblG2_9"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                        <dx:ListEditItem Value="W" Text="ไม่มี" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F7F7F7" class="cInline">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td width="150px" align="left">
                                                                            10. จรวดใส่น้ำมันขนาด
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG2_10_1" runat="server" ClientInstanceName="txtG2_10_1" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td width="20px" align="left">
                                                                            นิ้ว
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG2_10_2" runat="server" ClientInstanceName="txtG2_10_2" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left">
                                                                            ท่อ อยู่ด้าน
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <dx:ASPxRadioButtonList ID="rblG2_10" runat="server" ClientInstanceName="rblG2_10"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus" Width="265px">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="LR" Text="ซ้ายและขวา" />
                                                                        <dx:ListEditItem Value="L" Text="ซ้าย" />
                                                                        <dx:ListEditItem Value="R" Text="ขวา" />
                                                                        <dx:ListEditItem Value="W" Text="ไม่มี" />
                                                                    </Items>
                                                                    <ClientSideEvents ValueChanged="function(s,e){ Dis10(); }" />
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true" Display="Dynamic">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td width="150px" align="left">
                                                                            11. จรวดใส่สายยางขนาด
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG2_11_1" runat="server" ClientInstanceName="txtG2_11_1" Width="60"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td width="20px" align="left">
                                                                            นิ้ว
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG2_11_2" runat="server" ClientInstanceName="txtG2_11_2" Width="60"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            นิ้ว ท่อ อยู่ด้าน
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG2_11" runat="server" ClientInstanceName="rblG2_11"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus" Width="265px">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="LR" Text="ซ้ายและขวา" />
                                                                        <dx:ListEditItem Value="L" Text="ซ้าย" />
                                                                        <dx:ListEditItem Value="R" Text="ขวา" />
                                                                        <dx:ListEditItem Value="W" Text="ไม่มี" />
                                                                    </Items>
                                                                    <ClientSideEvents ValueChanged="function(s,e){ Dis11(s.GetValue()); }" />
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true" Display="Dynamic">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F7F7F7">
                                                                12. จรวดเจาะรู สามารถมองทะลุผ่านได้
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <dx:ASPxRadioButtonList ID="rblG2_12" runat="server" ClientInstanceName="rblG2_12"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                        <dx:ListEditItem Value="W" Text="ไม่มี" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class="corp">
                                                                หมายเหตุ &quot;ไม่มี&quot; หมายถึง อุปกรณ์นั้นไม่จำเป็น หรือไม่ถูกบังคับให้มีที่รถ
                                                                เมื่อไม่มีการติดตั้งอุปกรณ์ดังกล่าว ผลตรวจสอบก็ยังผ่านได้
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" bgcolor="#B9EAEF">
                                                    ตรวจยางรถหัวลากเทเลอร์และแหนบ
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                                        <tr>
                                                            <td width="70%">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td width="150px" align="left">
                                                                            13. ยางสองล้อหน้าขนาด
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG3_13_1" runat="server" ClientInstanceName="txtG3_13_1" Width="60"
                                                                                HorizontalAlign="Right">
                                                                                <%--<ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />--%>
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td width="20px" align="left">
                                                                            ลม
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG3_13_2" runat="server" ClientInstanceName="txtG3_13_2" Width="60"
                                                                                HorizontalAlign="Right">
                                                                                <%--<ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />--%>
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            ปอนด์
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="30%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F7F7F7">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td width="150px" align="left">
                                                                            14. แหนบหน้าซ้าย
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG3_14_1" runat="server" ClientInstanceName="txtG3_14_1" Width="60"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td width="130px" align="left">
                                                                            แผ่น&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;แหนบหน้าขวา
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG3_14_2" runat="server" ClientInstanceName="txtG3_14_2" Width="60"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            แผ่น
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <dx:ASPxCheckBox ID="ckbG3_14" runat="server" ClientInstanceName="ckbG3_14" Text="ถุงลม">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td width="150px" align="left">
                                                                            15. ยางแปดล้อหลังขนาด
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG3_1414_1" runat="server" ClientInstanceName="txtG3_1414_1"
                                                                                Width="60" HorizontalAlign="Right">
                                                                                <%--<ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />--%>
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td width="20px" align="left">
                                                                            ลม
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG3_1414_2" runat="server" ClientInstanceName="txtG3_1414_2"
                                                                                Width="60" HorizontalAlign="Right">
                                                                                <%--<ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />--%>
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            ปอนด์
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F7F7F7">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td width="150px" align="left">
                                                                            16. แหนบหลังซ้าย
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG3_15_1" runat="server" ClientInstanceName="txtG3_15_1" Width="60"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td width="130px" align="left">
                                                                            แผ่น&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;แหนบหน้าขวา
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG3_15_2" runat="server" ClientInstanceName="txtG3_15_2" Width="60"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            แผ่น
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <dx:ASPxCheckBox ID="ckbG3_15" runat="server" ClientInstanceName="ckbG3_15" Text="ถุงลม">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" bgcolor="#B9EAEF">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left" width="170px">
                                                                ตรวจยางรถตัวเทเลอร์และแหนบ
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG4_Head" runat="server" SkinID="rblStatus" ClientInstanceName="rblG4_Head">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="1" Text="ยาง 8 ล้อหลังเทเลอร์" />
                                                                        <dx:ListEditItem Value="2" Text="ยาง 12 ล้อหลังเทเลอร์" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                                        <tr>
                                                            <td width="70%">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="190px">
                                                                            17. ขนาดยางล้อหลังเทเลอร์ขนาด
                                                                        </td>
                                                                        <td width="100px">
                                                                            <dx:ASPxTextBox ID="txtG4_16_1" runat="server" ClientInstanceName="txtG4_16_1" Width="80px"
                                                                                HorizontalAlign="Right">
                                                                                <%--<ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />--%>
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left" width="20px">
                                                                            ลม
                                                                        </td>
                                                                        <td width="85px">
                                                                            <dx:ASPxTextBox ID="txtG4_16_2" runat="server" ClientInstanceName="txtG4_16_2" Width="80px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            ปอนด์
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="30%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F7F7F7">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="120px">
                                                                            18. แหนบหน้าซ้าย
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG4_17_1" runat="server" ClientInstanceName="txtG4_17_1" Width="60"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left" width="120px">
                                                                            แผ่น&nbsp;&nbsp;&nbsp;แหนบหน้าขวา
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG4_17_2" runat="server" ClientInstanceName="txtG4_17_2" Width="60"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            แผ่น
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <dx:ASPxCheckBox ID="ckbG4_17" runat="server" ClientInstanceName="ckbG4_17" Text="ถุงลม">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="120px">
                                                                            19. แหนบกลางซ้าย
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG4_18_1" runat="server" ClientInstanceName="txtG4_18_1" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left" width="120px">
                                                                            แผ่น&nbsp;&nbsp;&nbsp;แหนบกลางขวา
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG4_18_2" runat="server" ClientInstanceName="txtG4_18_2" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            แผ่น
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxCheckBox ID="ckbG4_18" runat="server" ClientInstanceName="ckbG4_18" Text="ถุงลม">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F7F7F7">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="120px">
                                                                            20. แหนบท้ายซ้าย
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG4_19_1" runat="server" ClientInstanceName="txtG4_19_1" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left" width="120px">
                                                                            แผ่น&nbsp;&nbsp;&nbsp;แหนบท้ายขวา
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG4_19_2" runat="server" ClientInstanceName="txtG4_19_2" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            แผ่น
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <dx:ASPxCheckBox ID="ckbG4_19" runat="server" ClientInstanceName="ckbG4_19" Text="ถุงลม">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" bgcolor="#B9EAEF">
                                                    ตรวจท่อจ่ายน้ำมัน
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                                        <tr>
                                                            <td width="70%">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td align="left" width="130px">
                                                                            21. ท่อจ่ายน้ำมันขนาด
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG5_20_1" runat="server" ClientInstanceName="txtG5_20_1" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left" width="20px">
                                                                            นิ้ว
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG5_20_2" runat="server" ClientInstanceName="txtG5_20_2" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            ท่อ
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="30%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F7F7F7">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="130px">
                                                                            22. ท่อร่วมทางจ่าย
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG5_21_1" runat="server" ClientInstanceName="txtG5_21_1" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left" width="70px">
                                                                            นิ้ว&nbsp;&nbsp;&nbsp;&nbsp; ยาว
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG5_21_2" runat="server" ClientInstanceName="txtG5_21_2" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            เมตร
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <dx:ASPxCheckBox ID="ckbG5_21_1" runat="server" ClientInstanceName="ckbG5_21_1" Text="ไม่มี">
                                                                    <ClientSideEvents CheckedChanged="function(){ Dis22(); }" />
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="130px">
                                                                            23. ท่อใต้ท้อง ขนาด
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG5_22_1" runat="server" ClientInstanceName="txtG5_22_1" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e);  }" TextChanged="function (s,e){CalPipe(); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left" width="20px">
                                                                            นิ้ว
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG5_22_2" runat="server" ClientInstanceName="txtG5_22_2" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            ท่อ
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="cTDAlter">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="130px">
                                                                            24. ยาวรวมทั้งหมด
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG5_23_1" runat="server" ClientInstanceName="txtG5_23_1" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e);}" TextChanged="function (s,e){CalPipe(); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left" width="100px">
                                                                            เมตร
                                                                        </td>
                                                                        <td>
                                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td align="left" width="70px">
                                                                                        ความจุรวม
                                                                                    </td>
                                                                                    <td width="95px">
                                                                                        <dx:ASPxTextBox ID="txtG5_23_2" runat="server" ClientInstanceName="txtG5_23_2" Width="90px"
                                                                                            HorizontalAlign="Right" ReadOnly="true">
                                                                                            <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                                RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                                <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                            </ValidationSettings>
                                                                                        </dx:ASPxTextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        ลิตร
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="cTDAlter">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" bgcolor="#B9EAEF">
                                                    วัดค่า Gas Detector
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                                        <tr>
                                                            <td width="70%">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="105px">
                                                                            25. Gas1(HC)=
                                                                        </td>
                                                                        <td width="105px">
                                                                            <dx:ASPxTextBox ID="txtG6_52_1" runat="server" ClientInstanceName="txtG6_52_1" Width="100px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            %LEL
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="30%">
                                                                <dx:ASPxRadioButtonList ID="rblG6_52" runat="server" ClientInstanceName="rblG6_24"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F7F7F7">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="105px">
                                                                            26. Gas2(O2)=
                                                                        </td>
                                                                        <td width="105px">
                                                                            <dx:ASPxTextBox ID="txtG6_53_1" runat="server" ClientInstanceName="txtG6_53_1" Width="100px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            %vol(O2)
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <dx:ASPxRadioButtonList ID="rblG6_53" runat="server" ClientInstanceName="rblG6_53"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" bgcolor="#B9EAEF">
                                                    ตรวจวาล์ว
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                                        <tr>
                                                            <td width="70%">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="80px">
                                                                            ชนิดวาล์ว
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxComboBox ID="cmbG7" runat="server" DataSourceID="sdsValeType" ValueField="VALVE_TYPEID"
                                                                                TextField="VALVE_TYPENAME">
                                                                            </dx:ASPxComboBox>
                                                                            <asp:SqlDataSource ID="sdsValeType" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                                SelectCommand="SELECT * FROM
                                                                        (
                                                                        SELECT  CASE WHEN  1 = 1 THEN 0 END AS  VALVE_TYPEID,
                                                                        CASE WHEN 1 = 1 THEN '-ระบุประเภทวาล์ว-' END AS  VALVE_TYPENAME FROM TBL_VALVETYPE WHERE   VALVE_TYPEID = 1
                                                                        UNION ALL
                                                                        SELECT VALVE_TYPEID,VALVE_TYPENAME FROM TBL_VALVETYPE WHERE CACTIVE = 'Y'
                                                                        )"></asp:SqlDataSource>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="30%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F7F7F7">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="130px">
                                                                            27. วาล์วฉุกเฉิน ขนาด
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG7_24_1" runat="server" ClientInstanceName="txtG7_24_1" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left" width="40px">
                                                                            นิ้ว
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG7_24_2" runat="server" ClientInstanceName="txtG7_24_2" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            ตัว
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <dx:ASPxRadioButtonList ID="rblG7_24" runat="server" ClientInstanceName="rblG7_25"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="170px">
                                                                            28. วาล์วแยกแต่ละช่องขนาด
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG7_25_1" runat="server" ClientInstanceName="txtG7_25_1" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left" width="40px">
                                                                            นิ้ว
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG7_25_2" runat="server" ClientInstanceName="txtG7_25_2" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            ตัว
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG7_25" runat="server" ClientInstanceName="rblG7_25"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F7F7F7">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="170px">
                                                                            29. วาล์วท่อจ่ายเพาเวอร์ขนาด
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG7_26_1" runat="server" ClientInstanceName="txtG7_26_1" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left" width="40px">
                                                                            นิ้ว
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG7_26_2" runat="server" ClientInstanceName="txtG7_26_2" Width="60px"
                                                                                HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            ตัว
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <dx:ASPxRadioButtonList ID="rblG7_26" runat="server" ClientInstanceName="rblG7_26"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                        <dx:ListEditItem Value="W" Text="ไม่มี" />
                                                                    </Items>
                                                                    <ClientSideEvents ValueChanged="function(s,e){ Dis29(); }" />
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                30. มือหมุนวาล์วหรือก้านวาล์วเชื่อมติดแกนทุกตัว
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG7_27" runat="server" ClientInstanceName="rblG7_27"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="cTDAlter">
                                                                31. เจาะรูแกนวาล์วหรือด้ามบอลล์วาล์วทุกตัว
                                                            </td>
                                                            <td class="cTDAlter">
                                                                <dx:ASPxRadioButtonList ID="rblG7_28" runat="server" ClientInstanceName="rblG7_28"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                32. เชื่อมน๊อตหน้าแปลนวาล์วสกัดตัวที่ 1 ตรงข้าม 2 ตัว
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG7_29" runat="server" ClientInstanceName="rblG7_29"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="cTDAlter">
                                                                33. เชื่อมน๊อตหน้าแปลนวาล์วสกัดตัวสุดท้าย ตรงข้าม 2 ตัว
                                                            </td>
                                                            <td class="cTDAlter">
                                                                <dx:ASPxRadioButtonList ID="rblG7_30" runat="server" ClientInstanceName="rblG7_30"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                34. เชื่อมน๊อตหน้าแปลนตรงข้ามวาล์วสกัดทุกตัว
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG7_31" runat="server" ClientInstanceName="rblG7_31"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="cTDAlter">
                                                                35. ขารับถังเจาะรู 0.5 นิ้ว มองผ่านได้
                                                            </td>
                                                            <td class="cTDAlter">
                                                                <dx:ASPxRadioButtonList ID="rblG7_32" runat="server" ClientInstanceName="rblG7_32"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                        <dx:ListEditItem Value="W" Text="ไม่มี" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" bgcolor="#B9EAEF">
                                                    ตรวจถังน้ำมัน
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                                        <tr>
                                                            <td width="70%">
                                                                36. ใต้ท้องถังต้องไม่ทำเป็น 2 ชั้น
                                                            </td>
                                                            <td width="30%">
                                                                <dx:ASPxRadioButtonList ID="rblG10_33" runat="server" ClientInstanceName="rblG10_33"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr class="cTDAlter">
                                                            <td>
                                                                37. ภายในตองกั้นถังต้องไม่เป็น 2 ชั้น
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG10_34" runat="server" ClientInstanceName="rblG10_34"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                38. ตองกั้นถังต้องแข็งแรงไม่บิดตัว
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG10_35" runat="server" ClientInstanceName="rblG10_35"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr class="cTDAlter">
                                                            <td>
                                                                39. สลักฝาปิดช่องลงน้ำมันเชื่อมติดแกน
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG10_36" runat="server" ClientInstanceName="rblG10_36"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                40. มีวาล์วระบายไอแก๊สที่ฝาปิดช่องลงน้ำมัน
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG10_37" runat="server" ClientInstanceName="rblG10_37"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr class="cTDAlter">
                                                            <td>
                                                                41. สภาพถังไม่บุบเสียรูปทรง
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG10_38" runat="server" ClientInstanceName="rblG10_38"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                42. ช่องลงน้ำมัน/ตำแหน่งแป้นอยู่กลางระหว่างตองกั้นถัง
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG10_39" runat="server" ClientInstanceName="rblG10_39"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr class="cTDAlter">
                                                            <td>
                                                                43. ขอบฝาถังเจาะรูและลึกไม่เกิน 25 ซม.
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG10_40" runat="server" ClientInstanceName="rblG10_40"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                        <dx:ListEditItem Value="W" Text="ไม่มี" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                44. ภายในถังไม่เจาะทะลุออกเพื่อการใด
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG10_41" runat="server" ClientInstanceName="rblG10_41"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                        <dx:ListEditItem Value="W" Text="ไม่มี" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr class="cTDAlter">
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td width="140px">
                                                                            45. ภายในถังช่องที่
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG10_42_1" runat="server" ClientInstanceName="txtG10_42_1"
                                                                                Width="60px" HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            ทะลุผ่านถึงกัน
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG10_42" runat="server" ClientInstanceName="rblG10_42"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                        <dx:ListEditItem Value="W" Text="ไม่มี" />
                                                                    </Items>
                                                                    <ClientSideEvents ValueChanged="function(s,e){ Dis45(); }" />
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                46. ท่ออุ่นน้ำมันภายในถัง
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG10_43" runat="server" ClientInstanceName="rblG10_43"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                        <dx:ListEditItem Value="W" Text="ไม่มี" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr class="cTDAlter">
                                                            <td>
                                                                47. ไม่มีแผ่นเหล็กบังสายตาภายใน
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG10_44" runat="server" ClientInstanceName="rblG10_44"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                48. มีวงแหวนกันงัดที่ฝาแมนโฮลและเต็มวงทุกช่อง
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG10_45" runat="server" ClientInstanceName="rblG10_45"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                        <dx:ListEditItem Value="W" Text="ไม่มี" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr class="cTDAlter">
                                                            <td>
                                                                49. ตัดแป้นเก่าออก,แต่งรอยให้เรียบ
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG10_46" runat="server" ClientInstanceName="rblG10_46"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                        <dx:ListEditItem Value="W" Text="ไม่มี" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                50. ภายในถังล้างสะอาดไม่มีคราบน้ำมัน
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG10_47" runat="server" ClientInstanceName="rblG10_47"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr class="cTDAlter">
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="150px">
                                                                            51. อู่ต่อถังรถบรรทุกน้ำมัน
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxTextBox ID="txtG10_48" runat="server" ClientInstanceName="txtG10_48" Width="50%" MaxLength="30">
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="200px">
                                                                            52. ชนิดของน๊อตฝา MANHOLD
                                                                        </td>
                                                                        <td align="left">
                                                                            <dx:ASPxRadioButtonList ID="rblG10_50" runat="server" SkinID="rblStatus" ClientInstanceName="rblG10_50">
                                                                                <Items>
                                                                                    <dx:ListEditItem Value="1" Text="น๊อตยึดใน" />
                                                                                    <dx:ListEditItem Value="2" Text="น๊อตรอบ" />
                                                                                    <dx:ListEditItem Value="3" Text="แค้มรัดรอบฝา" />
                                                                                </Items>
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxRadioButtonList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr class="cTDAlter">
                                                            <td>
                                                                53. ช่องว่างระยะติดแป้น ไม่ต่ำกว่า 15 ซม.
                                                            </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rblG10_51" runat="server" ClientInstanceName="rblG10_51"
                                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                                    </Items>
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" bgcolor="#B9EAEF">
                                                    รายการวัสดุที่ใช้ไปทั้งหมด
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                                        <tr>
                                                            <td width="70%">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="250px">
                                                                            54. แป้นทองเหลือง(ไม่มีตัวเลข)
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG11_55_1" runat="server" ClientInstanceName="txtG11_55_1"
                                                                                Width="60px" HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left">
                                                                            อัน
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="30%">
                                                            </td>
                                                        </tr>
                                                        <tr class="cTDAlter">
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="250px">
                                                                            55. เหล็กรองแป้น 2 ม.
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG11_56_1" runat="server" ClientInstanceName="txtG11_56_1"
                                                                                Width="60px" HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left">
                                                                            อัน
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="250px">
                                                                            56. เหล็กร้อยซีล
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG11_57_1" runat="server" ClientInstanceName="txtG11_57_1"
                                                                                Width="60px" HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left">
                                                                            ตัว
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr class="cTDAlter">
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="250px">
                                                                            57. ลวดสลิง1.5 มม. ขนาดร้อยฝาแมนโฮล
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG11_58_1" runat="server" ClientInstanceName="txtG11_58_1"
                                                                                Width="60px" HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left">
                                                                            ม.
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="250px">
                                                                            58. ลวดสลิงขนาด 2.5 มม. ร้อยแป้นระดับ
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG11_59_1" runat="server" ClientInstanceName="txtG11_59_1"
                                                                                Width="60px" HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left">
                                                                            ม.
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr class="cTDAlter">
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="250px">
                                                                            59. ฟาสซีล
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG11_60_1" runat="server" ClientInstanceName="txtG11_60_1"
                                                                                Width="60px" HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left">
                                                                            ตัว
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="250px">
                                                                            60. ซีลตะกั่ว
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG11_61_1" runat="server" ClientInstanceName="txtG11_61_1"
                                                                                Width="60px" HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left">
                                                                            ตัว
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr class="cTDAlter">
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="250px">
                                                                            61. ป้ายบอกระยะสีแดง
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG11_62_1" runat="server" ClientInstanceName="txtG11_62_1"
                                                                                Width="60px" HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left">
                                                                            แผ่น
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="250px">
                                                                            62. ป้ายบอกระยะน้ำเงิน
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG11_63_1" runat="server" ClientInstanceName="txtG11_63_1"
                                                                                Width="60px" HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left">
                                                                            แผ่น
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr class="cTDAlter">
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="250px">
                                                                            63. ลวดเชื่อม 3.2 มม.
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG11_64_1" runat="server" ClientInstanceName="txtG11_64_1"
                                                                                Width="60px" HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left">
                                                                            อัน
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="250px">
                                                                            64. ดอกรีเวท
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG11_65_1" runat="server" ClientInstanceName="txtG11_65_1"
                                                                                Width="60px" HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left" width="100px">
                                                                            อัน&nbsp;&nbsp;&nbsp;&nbsp;ดอกสว่าน
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG11_65_2" runat="server" ClientInstanceName="txtG11_65_2"
                                                                                Width="60px" HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            อัน
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr class="cTDAlter">
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="250px">
                                                                            65. สีสเปรย์ สีขาว
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG11_66_1" runat="server" ClientInstanceName="txtG11_66_1"
                                                                                Width="60px" HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td align="left" width="100px">
                                                                            กระป๋อง&nbsp;&nbsp;&nbsp;สีน้ำเงิน
                                                                        </td>
                                                                        <td width="65px">
                                                                            <dx:ASPxTextBox ID="txtG11_66_2" runat="server" ClientInstanceName="txtG11_66_2"
                                                                                Width="60px" HorizontalAlign="Right">
                                                                                <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณากรอกข้อมูล"
                                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td>
                                                                            กระป๋อง
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" bgcolor="#B9EAEF">
                                                    รายชื่อผู้ปฏิบัติงาน
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td valign="top" width="30%">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td width="90%">
                                                                            <dx:ASPxComboBox ID="cmbUser" runat="server" ClientInstanceName="cmbUser" Width="100%"
                                                                                TextFormatString="{0}" CallbackPageSize="30" EnableCallbackMode="True" CssClass="dxeLineBreakFix"
                                                                                SkinID="xcbbATC" ValueField="SUID" OnItemsRequestedByFilterCondition="cmbUser_ItemsRequestedByFilterCondition"
                                                                                OnItemRequestedByValue="cmbUser_ItemRequestedByValue">
                                                                                <Columns>
                                                                                    <dx:ListBoxColumn Caption="ชือ - สกุล" FieldName="SFULLNAME" Width="300px" />
                                                                                </Columns>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnAddEmpWorker" runat="server" ClientInstanceName="btnAddEmpWorker"
                                                                                SkinID="_add" AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                                                <ClientSideEvents Click="function(s,e){  AllEnable();  xcpn.PerformCallback('addempwork;');     } " />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="70%">
                                                                <dx:ASPxTextBox runat="server" ID="txtIndexgvwService" ClientInstanceName="txtIndexgvwService"
                                                                    ClientVisible="false">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxGridView ID="gvwEmpWorker" runat="server" ClientInstanceName="gvwEmpWorker"
                                                                    AutoGenerateColumns="false" SkinID="_gvw" Width="100%">
                                                                    <Columns>
                                                                        <dx:GridViewDataColumn Caption="" Width="4%">
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxButton runat="server" ID="btnDelete" CssClass="dxeLineBreakFix" EnableTheming="False"
                                                                                    Cursor="pointer" AutoPostBack="false" EnableDefaultAppearance="false" SkinID="dd">
                                                                                    <ClientSideEvents Click="function (s, e)
                                                                    {
                                                                        
                                                                        txtIndexgvwService.SetText(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));
                                                                        dxConfirm('คุณต้องการยืนยันการลบข้อมูลใช่หรือไม่','คุณต้องการยืนยันการลบข้อมูลใช่หรือไม่', function (s, e) { xcpn.PerformCallback('deleteEmp');  dxPopupConfirm.Hide(); }, function(s, e) {  dxPopupConfirm.Hide();} );
                                                                    }" />
                                                                                    <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                                                    </Image>
                                                                                </dx:ASPxButton>
                                                                            </DataItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn Caption="No." Width="4%">
                                                                            <DataItemTemplate>
                                                                                <%#Container.ItemIndex + 1 %>.
                                                                            </DataItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn Width="100%" FieldName="sFullName" Caption="ชื่อ - นามสกุล"
                                                                            HeaderStyle-HorizontalAlign="Center">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <CellStyle HorizontalAlign="Left">
                                                                            </CellStyle>
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataTextColumn Caption="EmpID" FieldName="sEmpID" Visible="false">
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                    <SettingsPager Mode="ShowAllRecords" />
                                                                    <Settings ShowColumnHeaders="true" ShowFooter="false" />
                                                                    <%-- <Styles>
                                                                        <Table>
                                                                            <Border BorderStyle="None" />
                                                                        </Table>
                                                                        <Cell>
                                                                            <Border BorderWidth="0px" />
                                                                        </Cell>
                                                                    </Styles>
                                                                    <Border BorderWidth="0px" />--%>
                                                                </dx:ASPxGridView>
                                                                <asp:SqlDataSource ID="sdsEmpWorker" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                                </asp:SqlDataSource>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" bgcolor="#B9EAEF">
                                                    สรุปผลการตรวจสอบ
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <dx:ASPxRadioButtonList ID="rblG9" runat="server" ClientInstanceName="rblG9" RepeatDirection="Horizontal"
                                                        SkinID="rblStatus">
                                                        <Items>
                                                            <dx:ListEditItem Value="Y" Text="ผ่าน" />
                                                            <dx:ListEditItem Value="N" Text="ไม่ผ่าน" />
                                                        </Items>
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กรุณาตรวจสอบ"
                                                            RequiredField-IsRequired="true" SetFocusOnError="true">
                                                            <RequiredField IsRequired="True" ErrorText="กรุณาตรวจสอบ"></RequiredField>
                                                        </ValidationSettings>
                                                    </dx:ASPxRadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left" width="200px">
                                                                ความคิดเห็นผู้ตรวจ
                                                            </td>
                                                            <td align="left">
                                                                <dx:ASPxCheckBox ID="ckbCheckSendMailToVerdor" runat="server" ClientInstanceName="ckbCheckSendMailToVerdor"
                                                                    Text="แจ้งผู้ขนส่ง">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" colspan="2">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxMemo ID="txtOpinion" runat="server" ClientInstanceName="txtOpinion" Rows="5"
                                                                                Width="50%">
                                                                                <ValidationSettings RequiredField-IsRequired="true" ErrorDisplayMode="ImageWithTooltip"
                                                                                    RequiredField-ErrorText="ระบุหมายเหตุ" ValidationGroup="add">
                                                                                    <RequiredField IsRequired="True" ErrorText="ระบุหมายเหตุ"></RequiredField>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxMemo>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <dx:ASPxButton ID="btnSaveT2" runat="server" ClientInstanceName="btnSaveT2" AutoPostBack="false"
                                                        Text="บันทึกผล" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function(s,e){ if(rblG9.GetValue() == 'N') { if(!ASPxClientEdit.ValidateGroup('add')){ AllEnable();  return false; }else{  AllEnable(); xcpn.PerformCallback('SAVE;');}   } else {  AllEnable(); txtOpinion.HideErrorCell(); xcpn.PerformCallback('SAVE;');} }" />
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="btnPrintT2" runat="server" ClientInstanceName="btnPrintT2" AutoPostBack="true"
                                                        Text="พิมพ์ใบสรุปผลการตรวจสภาพรถ" CssClass="dxeLineBreakFix" OnClick="btnPrintT2_Click">
                                                        <ClientSideEvents Click="function(s,e){  AllEnable(); }" />
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="ASPxButton1" runat="server" ClientInstanceName="btnPrintT2" AutoPostBack="true"
                                                        Text="พิมพ์ใบสรุปผลการตรวจสภาพรถไม่ผ่าน" CssClass="dxeLineBreakFix" OnClick="btnPrintNopass_Click">
                                                        <ClientSideEvents Click="function(s,e){ AllEnable(); }" />
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="btnRedirectT3" runat="server" ClientInstanceName="btnRedirectT3"
                                                        AutoPostBack="false" Text="ผลตรวจลงน้ำ" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function(s,e){  AllEnable(); xcpn.PerformCallback('RedirectT3;') }" />
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="bntExitT3" runat="server" ClientInstanceName="bntExitT3" AutoPostBack="false"
                                                        Text="ออกจากหน้านี้" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function(s,e) {  AllEnable(); window.location = 'approve_mv.aspx';}" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
