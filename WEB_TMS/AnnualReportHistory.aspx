﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AnnualReportHistory.aspx.cs" Inherits="AnnualReportHistory" MasterPageFile="~/MP.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"] {
            margin-left: 0px;
        }

        .checkbox label, .radio label {
            padding-right: 10px;
            padding-left: 15px;
        }

        .required.control-label:after {
            content: "*";
            color: red;
        }

        .form-horizontal .control-label.text-left {
            text-align: left;
            left: 0px;
        }

        .GridColorHeader th {
            text-align: inherit !important;
            align-content: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="grvMain" EventName="PageIndexChanging" />
            <asp:PostBackTrigger ControlID="btnExportPdf" />
            <asp:PostBackTrigger ControlID="btnExport" />
        </Triggers>
        <ContentTemplate>
            <div class="panel panel-info" style="margin-top: 20px;">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>ค้นหาข้อมูลทั่วไป
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <div class="form-group form-horizontal row">
                                <label for="<%=ddlYearSearch.ClientID %>" class="col-md-1 control-label">ปี</label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="ddlYearSearch" runat="server" CssClass="form-control marginTp">
                                    </asp:DropDownList>
                                </div>
                                <label for="<%=ddlVendorSearch.ClientID %>" class="col-md-2 control-label">ผู้ประกอบการขนส่ง</label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="ddlVendorSearch" runat="server" DataTextField="SABBREVIATION" CssClass="form-control marginTp" DataValueField="SVENDORID">
                                    </asp:DropDownList>
                                </div>

                                <label for="<%=chkStatus.ClientID %>" class="col-md-2 control-label">สถานะเอกสาร</label>
                                <div class="col-md-3 checkbox">
                                    <asp:CheckBoxList ID="chkStatus" RepeatDirection="Horizontal" runat="server" RepeatLayout="Flow">
                                    </asp:CheckBoxList>
                                </div>
                            </div>

                            <div class="form-group form-horizontal row">
                                <div style="text-align: right;">
                                    <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-hover btn-info" OnClick="btnClear_Click" />
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-hover btn-info" OnClick="btnSearch_Click" />
                                    <asp:Button ID="btnExport" runat="server" Text="Export Excel" CssClass="btn btn-hover btn-info" OnClick="btnExport_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>ประวัติการแจ้งผลประเมินการปฏิบัติงานในรอบ 12 เดือน
                </div>
                <div class="panel-body">
                    <asp:GridView ID="grvMain" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                        HorizontalAlign="Center" AutoGenerateColumns="false" OnRowCommand="grvMain_RowCommand" OnRowDataBound="grvMain_RowDataBound" OnPageIndexChanging="grvMain_PageIndexChanging" DataKeyNames="ID"
                        EmptyDataText="[ ไม่มีข้อมูล ]" AllowPaging="True" OnDataBound="grvMain_DataBound">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="10px" ItemStyle-CssClass="center" HeaderText="ลำดับ" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="10px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="บริษัทผู้ขนส่ง" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSABBREVIATION" runat="server" Text='<%# Eval("SABBREVIATION") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ชื่อเอกสาร" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblDocDesc" runat="server" Text='<%# "ผลประเมินการปปฏิบัติงานในรอบ 12 เดือน ประจำปี "+ Eval("YEAR") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="เลขที่เอกสาร" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblDocNo" runat="server" Text='<%# Eval("DOCNO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ประจำปี" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblYear" runat="server" Text='<%# Eval("YEAR") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ผู้ดำเนินการ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSenderName" runat="server" Text='<%# Eval("SENDER_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันที่" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSEND_TO_VENDOR_DATE" runat="server" Text='<%# Eval("UPDATE_DATE_STR") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สถานะ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblStatusName" runat="server" Text='<%# Eval("CONFIG_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ผู้ขนส่ง" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblStatusVendor" runat="server" Text=''></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Document" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImagePDF" runat="server" ImageUrl="~/Images/ic_pdf2.gif"
                                        Width="16px" Height="16px" BorderWidth="0" Style="cursor: pointer" CausesValidation="False"  />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="เรียกดูข้อมูล" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageView" runat="server" ImageUrl="~/Images/btnSearch.png" Width="16px" Height="16px" BorderWidth="0" Style="cursor: pointer" CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                        <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                        <PagerStyle CssClass="pagination-ys" />
                    </asp:GridView>
                </div>
            </div>
            <div style="display: none;">
                <asp:Button ID="btnExportPdf" runat="server" OnClick="btnExportPdf_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="display: none;">
        <asp:Button ID="btnVendorDownload" runat="server" Text="Export Excel" CssClass="btn btn-hover btn-info" OnClick="btnVendorDownload_Click" />
        <asp:Button ID="btnView" runat="server" OnClick="btnView_Click" />
        <asp:Button ID="btnNotice" runat="server" OnClick="btnView_Click" />
        <input type="text" name="hdnNum" id="hdnNum" runat="server" value="0">
    </div>
    <script type="text/javascript">
        function clickDownload() {
            $("#<%=btnVendorDownload.ClientID%>").click();
        }

        function gotoView(num) {
            $('#<%=hdnNum.ClientID%>').attr('value', num);
            document.getElementById("<%=btnView.ClientID%>").click();
            openInNewTab('AnnaulReportHistoryDetails.aspx');
        }

        function gotoNotice(num) {
            $('#<%=hdnNum.ClientID%>').attr('value', num);
            document.getElementById("<%=btnNotice.ClientID%>").click();
            openInNewTab('AnnualReportNotice.aspx');
        }

        function openInNewTab(url) {
            var win = window.open(url, '_blank');
            win.focus();
        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
