﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="final-score_detail.aspx.cs" Inherits="final_score_detail" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2.Export, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td width="30%" align="right">
                            <dx:ASPxTextBox ID="dateStart" ClientInstanceName="dateStart" Width="10px" ClientVisible="false"
                                runat="server">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox ID="dateEnd" ClientInstanceName="dateStart" Width="10px" ClientVisible="false"
                                runat="server" >
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox ID="txtSearch" runat="server" NullText="เลขที่สัญญา,ชื่อผู้ประกอบการขนส่ง"
                                Width="160px">
                            </dx:ASPxTextBox>
                        </td>
                        <td width="8%" align="right">
                            ปี ที่ประเมิน
                        </td>
                        <td width="15%" align="center">
                            <dx:ASPxComboBox ID="cmbYear" runat="server" Width="100px">
                            </dx:ASPxComboBox>
                        </td>
                        <td width="25%">
                            <dx:ASPxRadioButtonList ID="rblCheck" runat="server" SkinID="rblStatus">
                                <ClientSideEvents SelectedIndexChanged="function(s,e){cmbMonth.PerformCallback();}" />
                                <Items>
                                    <dx:ListEditItem Text="รายไตรมาส" Value="1" Selected="true" />
                                    <dx:ListEditItem Text="รายเดือน" Value="2" />
                                </Items>
                            </dx:ASPxRadioButtonList>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td width="12%">
                            <dx:ASPxComboBox ID="cmbMonth" ClientInstanceName="cmbMonth" runat="server" Width="220px"
                                OnCallback="cmbMonth_Callback">
                            </dx:ASPxComboBox>
                        </td>
                        <td width="10%">
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search">
                                <ClientSideEvents Click="function (s, e) {gvw.PerformCallback('Search');   }">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8">
                            *H = Hold
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                                DataSourceID="sds" KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px" Width="100%"
                                OnAfterPerformCallback="gvw_AfterPerformCallback">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="0"
                                        Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" FieldName="SCONTRACTID" ShowInCustomizationForm="True"
                                        VisibleIndex="1" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ชื่อผู้ประกอบการ " FieldName="SVENDORNAME" ShowInCustomizationForm="True"
                                        VisibleIndex="2" Width="25%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" ShowInCustomizationForm="True"
                                        VisibleIndex="3" Width="18%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewBandColumn Caption="ข้อมูลตัดคะแนน" ShowInCustomizationForm="True" VisibleIndex="4">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="ครั้ง" FieldName="NCOUNT" ShowInCustomizationForm="True"
                                                VisibleIndex="5" Width="8%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="คะแนน" FieldName="SUMNPOINT" ShowInCustomizationForm="True"
                                                VisibleIndex="6" Width="8%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="ข้อมูลการอุทธรณ์" ShowInCustomizationForm="True"
                                        VisibleIndex="7">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="บริษัทยื่น<br>อุทธรณ์" FieldName="TOTALAPPEAL"
                                                ShowInCustomizationForm="True" VisibleIndex="8" Width="8%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="อยู่ระหว่าง<br>พิจารณา" FieldName="OAPPEAL" ShowInCustomizationForm="True"
                                                VisibleIndex="9" Width="8%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="พิจารณาแล้ว" FieldName="FAPPEAL" ShowInCustomizationForm="True"
                                                VisibleIndex="10" Width="8%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewDataDateColumn Caption="ตัดคะแนนล่าสุด" FieldName="DCHECK" VisibleIndex="11"
                                        Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataColumn Width="5%" CellStyle-Cursor="hand" VisibleIndex="12">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" CausesValidation="False" AutoPostBack="false"
                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer">
                                                <ClientSideEvents Click="function (s, e) {gvw.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                <Image Width="16px" Height="16px" Url="Images/search.png">
                                                </Image>
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <Templates>
                                    <EditForm>
                                        <table width="100%">
                                            <tr align="left">
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" style="margin-bottom: 16px">
                                                        <tr>
                                                            <td style="padding-right: 4px">
                                                                <div style="float: left">
                                                                    <dx:ASPxButton ID="btnPdfExport" ToolTip="Export to PDF" runat="server" UseSubmitBehavior="False"
                                                                        EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                        OnClick="btnPdfExport_Click">
                                                                        <Image Width="16px" Height="16px" Url="Images/ic_pdf2.gif">
                                                                        </Image>
                                                                    </dx:ASPxButton>
                                                                </div>
                                                                <div style="float: left">
                                                                    PDF</div>
                                                            </td>
                                                            &nbsp
                                                            <td style="padding-right: 4px">
                                                                <div style="float: left">
                                                                    <dx:ASPxButton ID="btnXlsExport" ToolTip="Export to Excel" runat="server" UseSubmitBehavior="False"
                                                                        EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                        OnClick="btnXlsExport_Click">
                                                                        <Image Width="16px" Height="16px" Url="Images/ic_ms_excel.gif">
                                                                        </Image>
                                                                    </dx:ASPxButton>
                                                                </div>
                                                                <div style="float: left">
                                                                    Excel</div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr align="center">
                                                <td>
                                                    <dx:ASPxGridView ID="sgvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="sgvw"
                                                        DataSourceID="sds1" KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px"
                                                        Width="100%" OnCustomColumnDisplayText="gvw_CustomColumnDisplayText" SettingsBehavior-AllowSort="false">
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="0"
                                                                Width="3%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="เรื่องที่ตัดคะแนน" FieldName="SPROCESSNAME" ShowInCustomizationForm="True"
                                                                VisibleIndex="1" Width="10%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataDateColumn Caption="วันที่ตัดคะแนน" FieldName="DREDUCE" VisibleIndex="2"
                                                                Width="10%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataDateColumn>
                                                            <dx:GridViewDataTextColumn Caption="ทะเบียนหัว" FieldName="SHEADREGISTERNO" ShowInCustomizationForm="True"
                                                                VisibleIndex="3" Width="12%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" ShowInCustomizationForm="True"
                                                                VisibleIndex="4" Width="12%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ชื่อพนักงานขับ" FieldName="SEMPLOYEENAME" VisibleIndex="5"
                                                                Width="10%">
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ผู้ตัดคะแนน" FieldName="SREDUCEBY" ShowInCustomizationForm="True"
                                                                VisibleIndex="6" Width="12%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="รายละเอียด" FieldName="STOPICNAME" ShowInCustomizationForm="True"
                                                                VisibleIndex="7" Width="20%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="สถานะการอุทธรณ์" FieldName="STATUS" ShowInCustomizationForm="True"
                                                                VisibleIndex="8" Width="10%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="คะแนนที่ตัด" FieldName="SUMNPOINT" ShowInCustomizationForm="True"
                                                                VisibleIndex="9" Width="10%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ค่าปรับ" FieldName="COST" ShowInCustomizationForm="True"
                                                                VisibleIndex="9">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ระงับ พขร." FieldName="DISABLE_DRIVER" ShowInCustomizationForm="True"
                                                                VisibleIndex="9">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="BLACKLIST" FieldName="BLACKLIST" ShowInCustomizationForm="True"
                                                                VisibleIndex="9">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                        </Columns>
                                                        <Settings ShowFooter="True" />
                                                        <TotalSummary>
                                                            <dx:ASPxSummaryItem FieldName="SUMNPOINT" ShowInColumn="ผู้ตัดคะแนน" SummaryType="Sum"
                                                                DisplayFormat="รวมตัดคะแนน {0}" />
                                                                <dx:ASPxSummaryItem FieldName="COST" ShowInColumn="รายละเอียด" SummaryType="Sum"
                                                                DisplayFormat="รวมค่าปรับ {0}" />
                                                                
                                                        </TotalSummary>
                                                    </dx:ASPxGridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <dx:ASPxButton ID="btnCancel" runat="server" AutoPostBack="false" CssClass="dxeLineBreakFix"
                                                        SkinID="_close">
                                                        <ClientSideEvents Click="function (s, e) { gvw.CancelEdit()}"></ClientSideEvents>
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </EditForm>
                                </Templates>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                EnableCaching="True" CacheKeyDependency="ckdUser" SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY MAX(o.DREDUCE),ct.SCONTRACTID) AS ID1, ct.SCONTRACTID,CT.SCONTRACTNO,VS.SVENDORNAME ,COUNT(o.SCONTRACTID) AS NCOUNT,SUM(o.NPOINT) AS SUMNPOINT ,TO_CHAR(MAX(o.DREDUCE), 'dd/mm/yyyy', 'NLS_DATE_LANGUAGE = ENGLISH') AS DCHECK,SUM(o.TOTALAPPEAL) AS TOTALAPPEAL,SUM(o.OAPPEAL) AS OAPPEAL, SUM(o.FAPPEAL) AS FAPPEAL FROM 
 (TCONTRACT ct LEFT JOIN 
 
 (SELECT CASE WHEN R.SPROCESSID = '020'AND r.SCONTRACTID is null THEN CASE WHEN T.SCONTRACTID is null THEN CASE WHEN CT2.SCONTRACTID is null THEN 0 ELSE CT2.SCONTRACTID END ELSE  T.SCONTRACTID END ELSE r.SCONTRACTID END as SCONTRACTID ,nvl(r.NPOINT,0) AS NPOINT ,r.DREDUCE,
  CASE WHEN ap.SAPPEALID IS NULL THEN 0 ELSE 1 END AS TOTALAPPEAL , CASE WHEN CSTATUS = '0' OR CSTATUS = '1' OR CSTATUS = '2' OR CSTATUS = '4' OR CSTATUS = '5' OR CSTATUS = '6' THEN 1 ELSE 0 END AS OAPPEAL, CASE WHEN CSTATUS = '3' THEN 1 ELSE 0 END AS FAPPEAL
FROM ((TREDUCEPOINT r LEFT JOIN TTRUCK t ON R.SHEADREGISTERNO = T.SHEADREGISTERNO )
LEFT JOIN TCONTRACT_TRUCK ct2 ON nvl(T.STRUCKID,1) = nvl(CT2.STRUCKID,1)) LEFT JOIN TAPPEAL ap ON R.NREDUCEID = ap.NREDUCEID LEFT JOIN  TCONTRACT c ON c.SCONTRACTID = ct2.SCONTRACTID WHERE nvl(R.CACTIVE,'0') = '1' AND r.DREDUCE Between To_Date(:dStart,'dd/MM/yyyy') And To_Date(:dEnd,'dd/MM/yyyy')  AND NVL(c.CACTIVE,'Y') = 'Y' ) 

 o ON o.SCONTRACTID = CT.SCONTRACTID) LEFT JOIN TVENDOR_SAP vs ON Ct.SVENDORID = VS.SVENDORID
 LEFT JOIN TVENDOR ON vs.SVENDORID = TVENDOR.SVENDORID
  WHERE TVENDOR.CACTIVE = '1' AND TVENDOR.INUSE = '1' AND NVL(ct.CACTIVE,'Y') = 'Y' AND (nvl(CT.SCONTRACTNO,'') LIKE '%' || :oSearch || '%' OR nvl(VS.SVENDORNAME,'') LIKE '%' || :oSearch || '%') AND TOTALAPPEAL IS NOT NULL GROUP BY  ct.SCONTRACTID,CT.SCONTRACTNO,VS.SVENDORNAME ORDER BY VS.SVENDORNAME">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="txtSearch" Name="oSearch" PropertyName="Text" />
                                    <asp:ControlParameter ControlID="dateStart" Name="dStart" PropertyName="Value" />
                                    <asp:ControlParameter ControlID="dateEnd" Name="dEnd" PropertyName="Value" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="sds1" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                EnableCaching="True" CacheKeyDependency="ckdUser" SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY o.NREDUCEID DESC) AS ID1, o.NREDUCEID,o.STOPICNAME  , TO_CHAR(o.DREDUCE, 'dd/mm/yyyy', 'NLS_DATE_LANGUAGE = ENGLISH') AS DREDUCE , o.SREDUCEBY,o.SPROCESSNAME, o.SEMPLOYEENAME,o.SREDUCENAME, o.SUMNPOINT, o.COST, o.DISABLE_DRIVER, o.BLACKLIST, o.STATUS,o.SHEADREGISTERNO,o.STRAILERREGISTERNO  FROM 
(
SELECT R.NREDUCEID,CASE WHEN R.SPROCESSID IN ('080', '090', '040') THEN R.SREDUCENAME ELSE Tp.STOPICNAME END STOPICNAME  , PC.SPROCESSNAME, r.DREDUCE,R.SREDUCEBY || ' ' || U.SFIRSTNAME || ' ' || U.SLASTNAME AS SREDUCEBY  ,ES.FNAME || ' ' || ES.LNAME AS SEMPLOYEENAME , R.SREDUCENAME,R.SHEADREGISTERNO,R.STRAILERREGISTERNO , - nvl(r.NPOINT,0) AS SUMNPOINT, nvl(r.COST,0) AS COST, CASE WHEN R.SPROCESSID = '090' AND  R.DISABLE_DRIVER = -2 THEN 'H'  
     else nvl(r.DISABLE_DRIVER,0) || '' end AS DISABLE_DRIVER, CASE WHEN nvl(r.BLACKLIST, '') = 1 THEN 'Y' ELSE '' END AS BLACKLIST,

CASE WHEN ap.SAPPEALID IS NULL THEN 'ไม่ได้ยื่นอุทธรณ์'  ELSE
CASE WHEN ap.CSTATUS = '0' THEN 'รอพิจารณา' ELSE
CASE WHEN ap.CSTATUS = '1' THEN 'กำลังดำเนินการ'  ELSE
CASE WHEN ap.CSTATUS = '2' THEN 'ขอเอกสารเพิ่มเติม' ELSE
CASE WHEN ap.CSTATUS = '4' THEN 'ส่งเอกสารเพิ่มเติมแล้ว' ELSE
CASE WHEN ap.CSTATUS = '5' THEN 'รอคณะกรรมการพิจารณา' ELSE
CASE WHEN ap.CSTATUS = '6' THEN 'ไม่รับอุทธรณ์' ELSE
CASE WHEN ap.CSTATUS = '3' THEN  CASE WHEN ap.SSENTENCER = '1' THEN 'ตัดสิน (ถูก)' ELSE CASE WHEN ap.SSENTENCER = '2' THEN 'ตัดสิน (ผิด)' ELSE 'ตัดสิน (เปลี่ยน)' END  END  END  END END END END END END END As STATUS ,

CASE WHEN R.SPROCESSID = '020'AND r.SCONTRACTID is null THEN CASE WHEN T.SCONTRACTID is null THEN CASE WHEN CT2.SCONTRACTID is null THEN 0 ELSE CT2.SCONTRACTID END ELSE  T.SCONTRACTID END ELSE r.SCONTRACTID END as SCONTRACTID
FROM ((((((TREDUCEPOINT r LEFT JOIN TTRUCK t ON R.SHEADREGISTERNO = T.SHEADREGISTERNO )
LEFT JOIN 
(
SELECT tct.* FROM TCONTRACT_TRUCK tct
LEFT JOIN TCONTRACT ct
ON tct.SCONTRACTID = ct.SCONTRACTID
WHERE NVL(ct.CACTIVE,'Y') = 'Y'
) ct2 ON nvl(T.STRUCKID,1) = nvl(CT2.STRUCKID,1))LEFT JOIN (SELECT STOPICID,STOPICNAME,SVERSION FROM TTOPIC) tp ON R.STOPICID = Tp.STOPICID AND nvl(R.SVERSIONLIST,'1') = TP.SVERSION ) LEFT JOIN TAPPEAL ap on R.NREDUCEID = ap.NREDUCEID)
LEFT JOIN TEMPLOYEE_SAP es ON R.SDRIVERNO = ES.SEMPLOYEEID ) LEFT JOIN TUSER u ON R.SREDUCEBY = U.SUID) LEFT JOIN TPROCESS pc ON R.SPROCESSID = PC.SPROCESSID
WHERE nvl(r.CACTIVE,'0') = '1' AND r.DREDUCE Between To_Date(:dStart,'dd/MM/yyyy') And To_Date(:dEnd,'dd/MM/yyyy')) o      WHERE o.SCONTRACTID = :oSSCONTRACTID">
                                <SelectParameters>
                                    <asp:SessionParameter Name="oSSCONTRACTID" SessionField="oSSCONTRACTID" />
                                    <asp:SessionParameter Name="dStart" SessionField="dateStart" />
                                    <asp:SessionParameter Name="dEnd" SessionField="dateEnd" />
                                   
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="sgvw" PageHeader-Font-Names="Tahoma"
                                PageHeader-Font-Bold="true">
                                <Styles>
                                    <Header Font-Names="Tahoma" Font-Size="10">
                                    </Header>
                                    <Cell Font-Names="Tahoma" Font-Size="8">
                                    </Cell>
                                </Styles>
                                <PageHeader>
                                    <Font Bold="True" Names="Tahoma"></Font>
                                </PageHeader>
                            </dx:ASPxGridViewExporter>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
