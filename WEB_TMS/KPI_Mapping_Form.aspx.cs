﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class KPI_Mapping_Form : PageBase
{
    DataTable dt;
    DataSet ds;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDrowDownList();
            GetData();
            SetData();
            //if (Session["CGROUP"] + string.Empty == "0")
            //{
            //    ddlVendor.SelectedValue = Session["SVDID"] + string.Empty;
            //    ddlVendor.Enabled = false;
            //    ddlVendor_SelectedIndexChanged(null, null);

            //}
            if (Session["chkurl"] != null && Session["chkurl"] == "1")
            {
                btnSave.Visible = false;
            }
            this.AssignAuthen();
        }
    }
    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
            }
            if (!CanWrite)
            {
                btnSave.Enabled = false;
                btnSave.CssClass = "btn btn-md bth-hover btn-info";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region DrowDownList
    private void SetDrowDownList()
    {
        for (int i = 2015; i < DateTime.Now.Year + 4; i++)
        {
            ddlYear.Items.Add(new ListItem()
            {
                Text = i + string.Empty,
                Value = i + string.Empty
            });
        }
        ddlYear.SelectedValue = DateTime.Now.Year + string.Empty;
        //ViewState["DataMonth"] = KPI2BLL.Instance.MonthSelect();


    }
    #endregion

    #region Btn_Cilck
    #region btnSave_Click
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            GetData();
            CreateData();
            DataTable dtOld = (DataTable)ViewState["DataMonth"];
            DataRow dr, drOld;
            bool isSave = false;
            string strWhere = " AND T_KPI_HEADER.YEAR = '" + ddlYear.SelectedValue + "' AND (";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dr = dt.Rows[i];
                drOld = dtOld.Rows[i];
                if (dr["TOPIC_HEADER_ID"] + string.Empty != drOld["TOPIC_HEADER_ID"] + string.Empty && !drOld.IsNull("TOPIC_HEADER_ID"))
                {
                    isSave = true;
                    strWhere += " (T_KPI_HEADER.MONTH_ID = " + dr["MONTH_ID"] + " AND T_KPI_DETAIL.TOPIC_HEADER_ID = " + drOld["TOPIC_HEADER_ID"] + ") OR ";
                    //strTOPIC_HEADER_ID += "," + drOld["TOPIC_HEADER_ID"] + string.Empty;
                }
            }
            if (strWhere.Trim().Substring(strWhere.Length - 4).Trim() == "OR")
            {
                strWhere = strWhere.Trim().Substring(0, strWhere.Length - 4);
            }
            strWhere += ")";
            if (isSave)
            {
                dt = KPI2BLL.Instance.CheckKPIMappingForm(strWhere);
                string mess = string.Empty, strOld = string.Empty;
                if (dt != null && dt.Rows.Count > 0)
                {

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dr = dt.Rows[i];

                        if (strOld == dr["YEAR"] + string.Empty + dr["NAME_TH"] + string.Empty + dr["HEADER_NAME"] + string.Empty)
                        {

                        }
                        else
                        {
                            mess += "ปี " + dr["YEAR"] + " เดือน " + dr["NAME_TH"] + " แบบประเมิน " + dr["HEADER_NAME"] + " ถูกใช้แล้ว ข้อมูล KPI เดิม จะถูกลบออก เมื่อเปลี่ยนแบบประเมิน<br/>";
                            strOld = dr["YEAR"] + string.Empty + dr["NAME_TH"] + string.Empty + dr["HEADER_NAME"] + string.Empty;
                        }

                    }
                    mess += "คุณต้องการบันทึกข้อมูลใช่หรือไม่";
                    mpConfirmSave.TextDetail = mess;
                }
                else
                {
                    mess = "คุณต้องการบันทึกข้อมูลใช่หรือไม่";
                }


                dt.TableName = "DT";
                Session["DataCheck"] = dt;
                
            }
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "confirm", "confirmChangevendor();", true);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }

    #endregion

    #region mpConfirmSave_ClickOK
    protected void mpConfirmSave_ClickOK(object sender, EventArgs e)
    {
        try
        {

            CreateData();
            DataSet ds = new DataSet("DS");
            ds.Tables.Add(dt.Copy());
            DataSet dsDataCheck = new DataSet("DS");

            if (Session["DataCheck"] != null)
            {
                dsDataCheck.Tables.Add(((DataTable)Session["DataCheck"]).Copy());
                Session.Remove("DataCheck");
            }

            KPI2BLL.Instance.SaveKPIMappingForm(ddlYear.SelectedValue, Session["UserID"] + string.Empty, ds, dsDataCheck);
            alertSuccess("บันทึกสำเร็จ", "KPI_Mapping_Form.aspx");
            //gvKPI.DataSource = dt;
            //gvKPI.DataBind();
            //lblItem.Text = dt.Rows.Count + string.Empty;
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    #endregion

    #region btnClear_Click
    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlYear.SelectedValue = DateTime.Now.Year + string.Empty;
    }
    #endregion

    #endregion

    #region GetData
    private void GetData()
    {
        ds = KPI2BLL.Instance.KPIMappingFormSelect(ddlYear.SelectedValue);

        if (ds.Tables.Count > 0)
        {
            dt = ds.Tables["MONTH"];
            if (dt != null && dt.Rows.Count > 0)
            {
                DataTable dtMAPPING = ds.Tables["MAPPING"];
                if (dtMAPPING != null && dtMAPPING.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMAPPING.Rows.Count; i++)
                    {
                        DataRow[] drs = dt.Select("MONTH_ID = " + dtMAPPING.Rows[i]["MONTH_ID"]);
                        if (drs.Any())
                        {
                            DataRow dr = drs[0];
                            dr["TOPIC_HEADER_ID"] = dtMAPPING.Rows[i]["TOPIC_HEADER_ID"];
                            dr["MAPPING_ID"] = dtMAPPING.Rows[i]["MAPPING_ID"];
                        }
                    }
                }
            }
            ViewState["DataMonth"] = dt;
            dt = ds.Tables["TOPIC"];
            ViewState["DataTopic"] = dt;
        }
    }
    #endregion

    #region SetData
    private void SetData()
    {
        gvKPIMapping.DataSource = ViewState["DataMonth"];
        gvKPIMapping.DataBind();
    }
    #endregion

    #region gvKPIMapping_RowDataBound
    protected void gvKPIMapping_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dr = (DataRowView)e.Row.DataItem;
            DropDownList ddl = (DropDownList)e.Row.FindControl("ddlKPITopic");
            if (ddl != null)
            {
                ddl.DataTextField = "HEADER_NAME";
                ddl.DataValueField = "TOPIC_HEADER_ID";
                ddl.DataSource = ViewState["DataTopic"];
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem()
                {
                    Text = "--เลือก--",
                    Value = "0"
                });
                if (!string.IsNullOrEmpty(dr["TOPIC_HEADER_ID"] + string.Empty))
                {
                    ddl.SelectedValue = dr["TOPIC_HEADER_ID"] + string.Empty;
                }

            }

        }
    }
    #endregion

    #region ddlYear_SelectedIndexChanged
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetData();
        SetData();
    }
    #endregion


    #region CreateData
    private void CreateData()
    {
        dt = new DataTable("DT");
        dt.Columns.Add("MONTH_ID", typeof(string));
        dt.Columns.Add("TOPIC_HEADER_ID", typeof(string));

        if (gvKPIMapping.Rows.Count > 0)
        {
            for (int i = 0; i < gvKPIMapping.Rows.Count; i++)
            {
                GridViewRow row = gvKPIMapping.Rows[i];
                HiddenField hidMONTH_ID = (HiddenField)row.FindControl("hidMONTH_ID");
                DropDownList ddlKPITopic = (DropDownList)row.FindControl("ddlKPITopic");
                dt.Rows.Add(hidMONTH_ID.Value, ddlKPITopic.SelectedValue);
            }
        }
    }
    #endregion

}