﻿<%@ WebHandler Language="C#" Class="DownloadFile" %>

using System;
using System.Web;

public class DownloadFile : IHttpHandler {

	public void ProcessRequest(HttpContext context)
	{
		try
		{
			//@"\UploadFile\Vendor\1145\2016-09-06 11-11-30-912.txt"
			string filePath = context.Request.Params.Get("link");
			string fileServerPath = System.Web.HttpContext.Current.Server.MapPath(filePath);
			string[] filePaths = filePath.Split('\\');
			string returnFileName = filePaths[filePaths.Length - 1];
			//string returnFileName = HttpUtility.UrlDecode(context.Request.Params.Get("defaultFileName"));
			string fileHeader = string.Format("attachment; filename= {0}", returnFileName);
			context.Response.Clear();
			context.Response.ContentType = "application/octet-stream";
			context.Response.AddHeader("Content-Disposition", fileHeader);
			context.Response.WriteFile(filePath);
			context.Response.End();
		}
		catch(Exception ex)
		{
			
		}
	}

	public bool IsReusable
	{
		get
		{
			return false;
		}
	}

}