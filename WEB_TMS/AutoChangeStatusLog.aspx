﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="AutoChangeStatusLog.aspx.cs"
    Inherits="AutoChangeStatusLog" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <%--<br />
    <br />--%>
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>
                    <asp:Label ID="lblHeaderTab1" runat="server" Text="Log ข้อมูลการปรับสถานะ อัตโนมัติ"></asp:Label>
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:Table runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" Width="15%">
                                        <asp:Label ID="Label1" runat="server" Text="ประเภท Log :&nbsp;"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell  Width="25%">
                                        <asp:DropDownList ID="ddlLogType" runat="server" CssClass="form-control" Width="200px">
                                            <asp:ListItem Text="Driver" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Truck" Value="1"></asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" Width="15%">
                                        
                                    </asp:TableCell>
                                    <asp:TableCell Width="25%">
                                        
                                    </asp:TableCell>
                                    <asp:TableCell Width="10%"></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" Width="15%">
                                        <asp:Label ID="Label2" runat="server" Text="วันที่ปรับสถานะ (เริ่มต้น) :&nbsp;"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell  Width="25%">
                                        <asp:TextBox ID="dteStart" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" Width="15%">
                                        <asp:Label ID="Label3" runat="server" Text="วันที่ปรับสถานะ (สิ้นสุด) :&nbsp;"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="25%">
                                        <asp:TextBox ID="dteEnd" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Width="10%"></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>

                            <br />

                            <asp:GridView ID="dgvDriverLog" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" PageSize="10" AllowPaging="true"
                                CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" OnPageIndexChanging="dgvDriverLog_PageIndexChanging"
                                HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="ID"
                                AlternatingRowStyle BackColor="White" ForeColor="#284775">
                                <Columns>
                                    <asp:TemplateField HeaderText="ลำดับที่">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ID" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="USER_NAME" HeaderText="USER_NAME">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CHG_DATETIME" HeaderText="วัน-เวลา ปรับสถานะ (SAP)">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DRIVER_CODE" HeaderText="รหัสพนักงานขับรถ">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DRVSTATUS_SAP" HeaderText="สถานะ (SAP)">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DRVSTATUS_TMS" HeaderText="สถานะ (TMS)">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PERS_CODE" HeaderText="รหัสบัตรประชาชน">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="STATUS" HeaderText="STATUS">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MESSAGE" HeaderText="MESSAGE">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CREATE_DATETIME" HeaderText="วัน-เวลา ปรับสถานะ (TMS)">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                <PagerStyle CssClass="pagination-ys" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: right">
                    <asp:button id="cmdSearch" runat="server" text="ค้นหา" cssclass="btn btn-md btn-hover btn-info" usesubmitbehavior="false" style="width: 100px" OnClick="cmdSearch_Click"/>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
