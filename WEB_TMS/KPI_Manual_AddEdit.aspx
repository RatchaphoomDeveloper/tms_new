﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="KPI_Manual_AddEdit.aspx.cs" Inherits="KPI_Manual_AddEdit" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="tab-pane fade active in" id="TabGeneral">
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="lblHeaderTab1" runat="server" Text="สรุปผลประเมินดัชนีชี้วัดประสิทธิภาพการทำงาน KPI แยกสัญญา (รายเดือน)"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table runat="server" Width="100%">
                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="lblYear" runat="server" Text="ประจำปี :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtYear" runat="server" CssClass="form-control" Width="350px" style="text-align:center" Enabled="false"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                <asp:Label ID="lblMonth" runat="server" Text="เดือน :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtMonth" runat="server" CssClass="form-control" Width="350px" style="text-align:center" Enabled="false"></asp:TextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="lblFormName" runat="server" Text="ชื่อแบบประเมิน :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtFormName" runat="server" CssClass="form-control" Width="350px" style="text-align:center" Enabled="false"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                <asp:Label ID="lblVendor" runat="server" Text="บริษัทผู้ขนส่ง :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control" Width="350px" style="text-align:center" Enabled="false"></asp:TextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="lblContract" runat="server" Text="เลขที่สัญญา :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtContract" runat="server" CssClass="form-control" Width="350px" style="text-align:center" Enabled="false"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                    <br />
                                    <asp:GridView ID="dgvMapping" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                        CellPadding="4" GridLines="Both" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                        HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                        AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                        <Columns>
                                            <asp:TemplateField HeaderText = "ที่" ItemStyle-Width="40">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="TOPIC_HEADER_ID" Visible="false">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FORMULA_ID" Visible="false">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="METHOD_ID" Visible="false">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="METHOD_NAME" HeaderText="วิธีคำนวณผลรวม">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TOPIC_NAME" HeaderText="ชื่อหัวข้อประเมิน">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FORMULA_NAME" HeaderText="สูตรการคำนวณ">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FREQUENCY" HeaderText="ความถี่">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LEVEL1" HeaderText="1">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LEVEL2" HeaderText="2">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LEVEL3" HeaderText="3">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LEVEL4" HeaderText="4">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LEVEL5" HeaderText="5">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="WEIGHT" HeaderText="Weight">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="A" ItemStyle-Width="80">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtA" runat="server" style="text-align:center; width:80px" Text='<%# Eval("VALUE_A") %>' CssClass="form-control"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="B" ItemStyle-Width="80">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtB" runat="server" style="text-align:center; width:80px" Text='<%# Eval("VALUE_B") %>' CssClass="form-control"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText = "ผลลัพธ์">
                                                <ItemStyle HorizontalAlign="Right" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPoint_Before" runat="server" style="text-align:center; width:100%" Text='<%# Eval("POINT_Before") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText = "คะแนนที่ได้">
                                                <ItemStyle HorizontalAlign="Right" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPoint" runat="server" style="text-align:center; width:100%" Text='<%# Eval("POINT") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="TOPIC_DETAIL_ID" Visible="false">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                        <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                        <PagerStyle CssClass="pagination-ys" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdSave" runat="server" Text="บันทึกผล" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 100px" data-toggle="modal" data-target="#ModalConfirmBeforeSave" />
                            <asp:Button ID="cmdCancel" runat="server" Text="ยกเลิก" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdCancel_Click" />
                            <asp:Button ID="cmdSaveDraft" runat="server" Text="บันทึกร่าง" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdSaveDraft_Click" />
                            <asp:Button ID="cmdRecalculate" runat="server" Text="คำนวณคะแนน" CssClass="btn btn-md btn-hover btn-success" UseSubmitBehavior="false" Style="width: 110px" OnClick="cmdRecalculate_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSave_Click"
        TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>