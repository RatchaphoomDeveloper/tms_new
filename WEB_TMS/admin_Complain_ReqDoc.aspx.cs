﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using System.Linq;
using System.Drawing;
using System.Data.OracleClient;
using System.Configuration;
using System.Web.Configuration;
using EmailHelper;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxEditors;

public partial class admin_Complain_ReqDoc : PageBase
{
    #region + ViewState +
    private DataTable dtTopic
    {
        get
        {
            if ((DataTable)ViewState["dtTopic"] != null)
                return (DataTable)ViewState["dtTopic"];
            else
                return null;
        }
        set
        {
            ViewState["dtTopic"] = value;
        }
    }
    private DataTable dtDeliveryNo
    {
        get
        {
            if ((DataTable)ViewState["dtDeliveryNo"] != null)
                return (DataTable)ViewState["dtDeliveryNo"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtDeliveryNo"] = value;
        }
    }
    public string strHeadRegistNo
    {
        get
        {
            if (ViewState["HeadRegistNo"] != null)
                return (ViewState["HeadRegistNo"].ToString().Trim());
            else return string.Empty;
        }
        set { ViewState["HeadRegistNo"] = value; }
    }
    private DataTable dtEmployee
    {
        get
        {
            if ((DataTable)ViewState["dtEmployee"] != null)
                return (DataTable)ViewState["dtEmployee"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtEmployee"] = value;
        }
    }

    private DataTable dtEmployee2
    {
        get
        {
            if ((DataTable)ViewState["dtEmployee2"] != null)
                return (DataTable)ViewState["dtEmployee2"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtEmployee2"] = value;
        }
    }

    private DataTable dtEmployee3
    {
        get
        {
            if ((DataTable)ViewState["dtEmployee3"] != null)
                return (DataTable)ViewState["dtEmployee3"];
            else
                return null;
        }
        set
        {
            ViewState["dtEmployee3"] = value;
        }
    }

    private DataTable dtEmployee4
    {
        get
        {
            if ((DataTable)ViewState["dtEmployee4"] != null)
                return (DataTable)ViewState["dtEmployee4"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtEmployee4"] = value;
        }
    }
    private DataTable dtRequire
    {
        get
        {
            if ((DataTable)ViewState["dtRequire"] != null)
                return (DataTable)ViewState["dtRequire"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequire"] = value;
        }
    }
    private DataTable dtComplainType
    {
        get
        {
            if ((DataTable)ViewState["dtComplainType"] != null)
                return (DataTable)ViewState["dtComplainType"];
            else
                return null;
        }
        set
        {
            ViewState["dtComplainType"] = value;
        }
    }
    private DataTable dtTruck
    {
        get
        {
            if ((DataTable)ViewState["dtTruck"] != null)
                return (DataTable)ViewState["dtTruck"];
            else
                return null;
        }
        set
        {
            ViewState["dtTruck"] = value;
        }
    }
    private DataTable dtAddDriver
    {
        get
        {
            if ((DataTable)ViewState["dtAddDriver"] != null)
                return (DataTable)ViewState["dtAddDriver"];
            else
                return null;
        }
        set
        {
            ViewState["dtAddDriver"] = value;
        }
    }
    private DataTable dtData
    {
        get
        {
            if ((DataTable)ViewState["dtData"] != null)
                return (DataTable)ViewState["dtData"];
            else
                return null;
        }
        set
        {
            ViewState["dtData"] = value;
        }
    }
    private int DocStatusID
    {
        get
        {
            if ((int)ViewState["DocStatusID"] != null)
                return (int)ViewState["DocStatusID"];
            else
                return 0;
        }
        set
        {
            ViewState["DocStatusID"] = value;
        }
    }

    private string DocID
    {
        get
        {
            if ((string)ViewState["DocID"] != null)
                return (string)ViewState["DocID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["DocID"] = value;
        }
    }

    private DataTable dtUserLogin
    {
        get
        {
            if ((DataTable)ViewState["dtUserLogin"] != null)
                return (DataTable)ViewState["dtUserLogin"];
            else
                return null;
        }
        set
        {
            ViewState["dtUserLogin"] = value;
        }
    }

    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }

    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }

    private DataTable dtUploadReqDoc
    {
        get
        {
            if ((DataTable)ViewState["dtUploadReqDoc"] != null)
                return (DataTable)ViewState["dtUploadReqDoc"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadReqDoc"] = value;
        }
    }

    private DataTable dtRequestFile
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFile"] != null)
                return (DataTable)ViewState["dtRequestFile"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFile"] = value;
        }
    }

    private DataTable dtHeader
    {
        get
        {
            if ((DataTable)ViewState["dtHeader"] != null)
                return (DataTable)ViewState["dtHeader"];
            else
                return null;
        }
        set
        {
            ViewState["dtHeader"] = value;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Culture = "en-US";
            this.UICulture = "en-US";

            if (!IsPostBack)
            {
                dtUserLogin = (DataTable)Session["UserLogin"];
                this.InitialForm();
                this.DropdownList();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void DropdownList()
    {
        try
        {
            dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL("COMPLAIN_REQ");
            DropDownListHelper.BindDropDownList(ref cboUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }


    //private void AssignAuthen()
    //{
    //    try
    //    {
    //        if (!CanRead)
    //        {
    //        }
    //        if (!CanWrite)
    //        {
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}

    private void InitialForm()
    {
        try
        {
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                string check = Request.QueryString.ToString();
                if (check.Contains("DocID"))
                {
                    var decryptedBytes = MachineKey.Decode(Request.QueryString["DocID"], MachineKeyProtection.All);
                    var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                    DocID = decryptedValue;
                }
                else
                {
                    throw new Exception("ไม่มีการขอข้อมูลเพิ่มเติม กรุณาตรวจสอบ");
                }
            }
            int Index = 0;

            this.InitialCreateDoc();
            this.LoadTopic();
            this.LoadComplainType();
            this.InitialUpload();
            this.InitialHeader();
            this.LoadVendor();
            this.LoadWarehouse();
            //this.LoadData();

            dtData = ComplainBLL.Instance.ComplainSelectBLL(" AND c.DOC_ID = '" + DocID + "'");
            if (dtData.Rows.Count > 0)
                for (int i = 0; i < dtData.Rows.Count; i++)
                    if (!string.Equals(dtData.Rows[i]["DOC_STATUS_ID"].ToString(), "1") && !string.Equals(dtData.Rows[i]["DOC_STATUS_ID"].ToString(), "2"))
                        throw new Exception("ข้อร้องเรียนถูกส่งเรื่องพิจาณาไปแล้ว");
            dtHeader = ComplainBLL.Instance.ComplainSelectHeaderBLL(" AND TCOMPLAIN.DOC_ID = '" + DocID + "'");
            dtAddDriver = ComplainBLL.Instance.ComplainSelectAddDriverBLL(" AND DOC_ID = '" + DocID + "'");
            cmbTopic.SelectedValue = dtHeader.Rows[Index]["TOPICID"].ToString();
            cmbTopic_SelectedIndexChanged(null, null);

            txtDocID.Value = DocID;
            cboComplainType.SelectedValue = dtHeader.Rows[Index]["COMPLAINID"].ToString();
            cboComplainType_SelectedIndexChanged(null, null);
            cbxOrganiz.SelectedValue = dtHeader.Rows[Index]["ORGANIZID"].ToString();
            chkLock.Checked = (string.Equals(dtHeader.Rows[Index]["LOCKDRIVERID"].ToString(), "0") ? false : true);
            rblTypeProduct.Value = dtHeader.Rows[Index]["TYPEPRODUCTID"].ToString();
            txtTrailerRegist.Value = dtHeader.Rows[Index]["CARDETAIL"].ToString();

            cboVendor.SelectedValue = dtHeader.Rows[Index]["VENDORID"].ToString();
            cboVendor_SelectedIndexChanged(null, null);

            cboContract.SelectedValue = dtHeader.Rows[Index]["CONTRACTID"].ToString();
            cboContract_SelectedIndexChanged(null, null);

            //cboHeadRegist.SelectedValue = dtHeader.Rows[Index]["TRUCKID"].ToString();
            //cboHeadRegist_SelectedIndexChanged(null, null);
            try
            {
                cboHeadRegist.SelectedValue = dtHeader.Rows[Index]["TRUCKID"].ToString();
                if (!string.Equals(dtHeader.Rows[Index]["TRUCKID"].ToString(), string.Empty) && (string.Equals(cboHeadRegist.SelectedValue, string.Empty)))
                {
                    this.TruckSelect();
                    cboHeadRegist.SelectedValue = dtHeader.Rows[Index]["TRUCKID"].ToString();
                }
            }
            catch
            {
                this.TruckSelect();
                cboHeadRegist.SelectedValue = dtHeader.Rows[Index]["TRUCKID"].ToString();
            }
            finally
            {
                cboHeadRegist_SelectedIndexChanged(null, null);
            }

            if (!string.Equals(dtHeader.Rows[Index]["DELIVERYID"].ToString(), string.Empty))
                cboDelivery.SelectedValue = dtHeader.Rows[Index]["DELIVERYID"].ToString();

            cmbPersonalNo.SelectedValue = dtHeader.Rows[Index]["PERSONALID"].ToString();

            if (dtAddDriver.Rows.Count > 0)
            {
                if (dtAddDriver.Rows.Count > 0)
                    cmbPersonalNo2.SelectedValue = dtAddDriver.Rows[0]["SDRIVERNO"].ToString();
                if (dtAddDriver.Rows.Count > 1)
                    cmbPersonalNo3.SelectedValue = dtAddDriver.Rows[1]["SDRIVERNO"].ToString();
                if (dtAddDriver.Rows.Count > 2)
                    cmbPersonalNo4.SelectedValue = dtAddDriver.Rows[2]["SDRIVERNO"].ToString();
            }

            txtDateTrans.Text = (dtHeader.Rows[Index]["DELIVERYDATE"] != null) ? dtHeader.Rows[Index]["DELIVERYDATE"].ToString() : string.Empty;
            //teTimeTrans.Text = dtHeader.Rows[Index]["DELIVERYTIME"].ToString();
            txtComplainAddress.Value = dtHeader.Rows[Index]["COMPLAINADDRESS"].ToString();
            txtTotalCar.Text = dtHeader.Rows[Index]["TOTALCAR"].ToString();
            txtShipTo.Text = dtHeader.Rows[Index]["WAREHOUSE_TO"].ToString();

            this.CheckAddHeader(this.CheckIsAddMultipleHeader(dtHeader.Rows[0]["COMPLAINID"].ToString()));

            if (dtData.Rows.Count > 0)
            {
                DocStatusID = int.Parse(dtData.Rows[0]["DOC_STATUS_ID"].ToString());
                txtDetail.Value = dtData.Rows[0]["SDETAIL"] + "";
            }
            GridViewHelper.BindGridView(ref dgvHeader, dtHeader);
            if (dtHeader.Rows[0]["IS_URGENT"].ToString() == "1")
            {
                if (DateTime.ParseExact(dtHeader.Rows[0]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) > DateTime.ParseExact(dtHeader.Rows[0]["COMPLAIN_VENDOR_DELAY"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo))
                    alertFail("หมดเวลาแนบเอกสารแล้ว");
            }
            else
            {
                if (DateTime.ParseExact(dtHeader.Rows[0]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) > DateTime.ParseExact(dtHeader.Rows[0]["COMPLAIN_VENDOR_NORMAL"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo))
                    alertFail("หมดเวลาแนบเอกสารแล้ว");
            }
            dtUploadReqDoc = ComplainImportFileBLL.Instance.ImportFileSelectBLL(DocID, "COMPLAIN_REQ", string.Empty);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUploadReqDoc, false);
            //this.InitialHeader();
            this.InitialRequestFile();
            //this.InitialUpload();
            if (dtUploadReqDoc.Rows.Count > 0)
                for (int i = 0; i < dtUploadReqDoc.Rows.Count; i++)
                    dtUpload.Rows.Add(dtUploadReqDoc.Rows[i]["UPLOAD_ID"], dtUploadReqDoc.Rows[i]["UPLOAD_NAME"], dtUploadReqDoc.Rows[i]["FILENAME_SYSTEM"], dtUploadReqDoc.Rows[i]["FILENAME_USER"], dtUploadReqDoc.Rows[i]["FULLPATH"]);
        }
        catch (Exception ex)
        {
            cmdSave.Enabled = false;
            throw new Exception(ex.Message);
        }
    }
    private void LoadWarehouse()
    {
        try
        {
            DataTable dtWarehouse = new DataTable();
            dtWarehouse = WarehouseBLL.Instance.WarehouseSelectBLL();
            DropDownListHelper.BindDropDownList(ref cbxOrganiz, dtWarehouse, "STERMINALID", "STERMINALNAME", true);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private void InitialCreateDoc()
    {
        try
        {
            DataTable dtLogin = new DataTable();
            dtLogin = ComplainBLL.Instance.LoginSelectBLL(" AND SUID = '" + Session["UserID"].ToString() + "'");

            //txtFName.Text = dtLogin.Rows[0]["SFULLNAME"].ToString();
            //txtFNameDivision.Text = dtLogin.Rows[0]["SUNITNAME"].ToString();

            //txtComplainBy.Value = dtLogin.Rows[0]["SFULLNAME"].ToString();
            //txtComplainDivisionBy.Value = dtLogin.Rows[0]["SUNITNAME"].ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void LoadTopic()
    {
        try
        {
            dtTopic = TopicBLL.Instance.TopicSelect(" AND TYPE = 'COMPLAIN'");
            DropDownListHelper.BindDropDownList(ref cmbTopic, dtTopic, "TOPIC_ID", "TOPIC_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cboContract_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.TruckSelect();
    }
    protected void cboComplainType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.ComplainTypeSelectedIndex();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void TruckSelect()
    {
        try
        {
            txtTrailerRegist.Value = string.Empty;

            DropDownListHelper.BindDropDownList(ref cboHeadRegist, null, string.Empty, string.Empty, false);        //ทะเบียนรถ
            DropDownListHelper.BindDropDownList(ref cboDelivery, null, string.Empty, string.Empty, false);          //Delivery No

            if (cboContract.SelectedIndex > 0)
            {
                dtTruck = ComplainBLL.Instance.TruckSelectBLL(this.GetConditionVendor() + this.GetConditionContract());
                DropDownListHelper.BindDropDownList(ref cboHeadRegist, dtTruck, "STRUCKID", "SHEADREGISTERNO", true);
                if (!string.IsNullOrEmpty(strHeadRegistNo)) // Filter By ทะเบียนรถ
                {
                    ListItem selectedListItem = cboHeadRegist.Items.FindByText(strHeadRegistNo);
                    if (selectedListItem != null) selectedListItem.Selected = true;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cboHeadRegist_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.GetCarDetail(string.Empty);
    }
    private void GetCarDetail(string Type)
    {
        try
        {
            DropDownListHelper.BindDropDownList(ref cboDelivery, null, string.Empty, string.Empty, false);          //Delivery No
            txtTrailerRegist.Value = string.Empty;

            if (!string.Equals(Type, string.Empty) || (cboHeadRegist.SelectedIndex > 0))
            {
                txtTrailerRegist.Value = dtTruck.Rows[cboHeadRegist.SelectedIndex]["STRAILERREGISTERNO"].ToString();
                dtDeliveryNo = ComplainBLL.Instance.OutboundSelectBLL(this.GetConditionVendor() + this.GetConditionContract() + this.GetConditionTruck() + " AND TPLANSCHEDULELIST.SDELIVERYNO IS NOT NULL");
                DropDownListHelper.BindDropDownList(ref cboDelivery, dtDeliveryNo, "SDELIVERYNO", "SDELIVERYNO", true);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private string GetConditionTruck()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (cboHeadRegist.SelectedIndex > 0)
                sb.Append(" AND TTRUCK.STRUCKID = '" + cboHeadRegist.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private string GetConditionContract()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (cboVendor.SelectedIndex > 0)
                sb.Append(" AND TCONTRACT_TRUCK.SCONTRACTID = '" + cboContract.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmbTopic_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.LoadComplainType();
            chkLock.Checked = false;
            if (dtHeader.Rows.Count == 0)
                this.ClearRequire();
            else
                this.CheckAddHeader(this.CheckIsAddMultipleHeader(dtHeader.Rows[0]["COMPLAINID"].ToString()));
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private void LoadComplainType()
    {
        try
        {
            cboComplainType.Items.Clear();
            if (cmbTopic.SelectedIndex > 0)
            {
                dtComplainType = ComplainTypeBLL.Instance.ComplainTypeSelect(this.GetConditionComplain());
                DropDownListHelper.BindDropDownList(ref cboComplainType, dtComplainType, "COMPLAIN_TYPE_ID", "COMPLAIN_TYPE_NAME", true);
            }
            this.ComplainTypeSelectedIndex();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private void ComplainTypeSelectedIndex()
    {
        if (cmbTopic.SelectedIndex > 0)
        {
            if (dgvHeader.Rows.Count == 0)
            {
                this.ClearValue();
                this.ClearRequire();

                if (cboComplainType.SelectedIndex > 0)
                {
                    if (string.Equals(dtComplainType.Rows[cboComplainType.SelectedIndex]["LOCK_DRIVER"].ToString(), "0"))
                    {
                        chkLock.Enabled = false;
                        chkLock.Checked = false;
                    }
                    else
                    {
                        chkLock.Enabled = false;
                        chkLock.Checked = true;
                    }

                    this.InitialRequireField();
                }
                else
                {
                    chkLock.Enabled = false;
                    chkLock.Checked = false;
                }
            }
            else
            {
                this.ClearRequire();
                this.CheckAddHeader(this.CheckIsAddMultipleHeader(dtHeader.Rows[0]["COMPLAINID"].ToString()));
            }
        }
    }
    private string CheckIsAddMultipleHeader(string ComplainTypeID)
    {
        try
        {
            for (int i = 0; i < dtComplainType.Rows.Count; i++)
            {
                if (string.Equals(dtComplainType.Rows[i]["COMPLAIN_TYPE_ID"].ToString(), ComplainTypeID))
                    return dtComplainType.Rows[i]["IS_ADD_MULTIPLE_CAR"].ToString();
            }
            return string.Empty;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void CheckAddHeader(string IsAddMultipleCar)
    {
        try
        {
            this.ClearRequire();
            this.InitialRequireField();

            if (string.Equals(IsAddMultipleCar, "0") || string.Equals(IsAddMultipleCar, string.Empty))
                this.EnableMultipleTopic(true);
            else
                this.EnableMultipleTopic(false);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void EnableMultipleTopic(bool IsEnable)
    {//ถ้าเปิดอยู่ให้ปิด
        //Header
        cmbTopic.Enabled = false;
        cboComplainType.Enabled = false;

        //Detail
        if (IsEnable)
        {
            cboVendor.Enabled = !IsEnable;
            cboContract.Enabled = !IsEnable;
            cboHeadRegist.Enabled = !IsEnable;
            cmbPersonalNo.Enabled = !IsEnable;
            cmbPersonalNo2.Enabled = !IsEnable;
            cmbPersonalNo3.Enabled = !IsEnable;
            cmbPersonalNo4.Enabled = !IsEnable;
            txtDateTrans.Enabled = !IsEnable;
            cbxOrganiz.Enabled = !IsEnable;
            rblTypeProduct.Enabled = !IsEnable;
            cboDelivery.Enabled = !IsEnable;
            txtComplainAddress.Disabled = IsEnable;
            txtTotalCar.Enabled = false;
        }
    }
    private void InitialRequireField()
    {
        try
        {
            this.ClearRequire();

            if (cboComplainType.SelectedIndex < 1)
                return;

            dtRequire = ComplainBLL.Instance.ComplainRequireFieldBLL(int.Parse(cboComplainType.SelectedValue.ToString()), "COMPLAIN");

            object tmp;
            Label tmpReq;
            string Type;

            for (int i = 0; i < dtRequire.Rows.Count; i++)
            {
                tmp = this.FindControlRecursive(Page, dtRequire.Rows[i]["CONTROL_ID"].ToString());

                if (tmp != null)
                {
                    tmpReq = (Label)this.FindControlRecursive(Page, dtRequire.Rows[i]["CONTROL_ID_REQ"].ToString());

                    if (tmpReq != null)
                    {
                        if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "1"))
                        {//Require Field
                            tmpReq.Visible = true;
                            if (string.Equals(dtRequire.Rows[i]["CONTROL_ID"].ToString(), "txtDateTrans"))
                            {
                                if (dtHeader.Rows.Count == 0)
                                    txtDateTrans.Text = DateTime.Now.ToString("dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo);
                                else
                                    txtDateTrans.Text = (dtHeader.Rows[0]["DELIVERYDATE"] != null) ? dtHeader.Rows[0]["DELIVERYDATE"].ToString() : string.Empty;
                            }
                        }
                        else if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "2"))
                        {//Optional Field
                            tmpReq.Visible = false;
                        }
                        else if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "3"))
                        {//Disable Field
                            Type = tmp.GetType().ToString();
                            if (string.Equals(Type, "DevExpress.Web.ASPxEditors.ASPxComboBox"))
                            {
                                ASPxComboBox cbo = (ASPxComboBox)tmp;
                                if (cbo.SelectedIndex > 0)
                                    cbo.SelectedIndex = 0;
                                cbo.Value = string.Empty;
                                cbo.ClientEnabled = false;
                            }
                            else if (string.Equals(Type, "DevExpress.Web.ASPxEditors.ASPxTextBox"))
                            {
                                ASPxTextBox txt = (ASPxTextBox)tmp;
                                txt.Text = string.Empty;
                                txt.Value = string.Empty;
                                txt.ClientEnabled = false;
                            }
                            else if (string.Equals(Type, "System.Web.UI.WebControls.TextBox"))
                            {
                                TextBox txt = (TextBox)tmp;
                                txt.Text = string.Empty;
                                txt.Enabled = false;
                            }
                            else if (string.Equals(Type, "System.Web.UI.WebControls.DropDownList"))
                            {
                                DropDownList cbo = (DropDownList)tmp;
                                if (cbo.SelectedIndex > 0)
                                    cbo.SelectedIndex = 0;
                                cbo.Enabled = false;
                                cbo.CssClass = "form-control";

                                if (dtRequire.Rows[i]["CONTROL_ID"].ToString() == "cboHeadRegist")
                                {
                                }
                            }
                            else if (string.Equals(Type, "System.Web.UI.HtmlControls.HtmlInputText"))
                            {
                                HtmlInputText txt = (HtmlInputText)tmp;
                                txt.Value = string.Empty;
                                txt.Disabled = true;
                            }
                        }
                    }
                }
            }


        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnSearchLicenseReset_Click(object sender, EventArgs e)
    {
        strHeadRegistNo = string.Empty;
        this.LoadVendor();
        cboVendor_SelectedIndexChanged(null, null);
    }
    protected void cboVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownListHelper.BindDropDownList(ref cboHeadRegist, null, string.Empty, string.Empty, false);        //ทะเบียนรถ
            DropDownListHelper.BindDropDownList(ref cboDelivery, null, string.Empty, string.Empty, false);          //Delivery No
            DropDownListHelper.BindDropDownList(ref cboContract, null, string.Empty, string.Empty, false);          //เลขที่สัญญา
            DropDownListHelper.BindDropDownList(ref cmbPersonalNo, null, string.Empty, string.Empty, false);        //พขร

            if (cboVendor.SelectedIndex > 0)
            {
                dtEmployee = ComplainBLL.Instance.PersonalSelectBLL(this.GetConditionPersonal());
                dtEmployee2 = ComplainBLL.Instance.PersonalSelectBLL(this.GetConditionPersonal());
                dtEmployee3 = ComplainBLL.Instance.PersonalSelectBLL(this.GetConditionPersonal());
                dtEmployee4 = ComplainBLL.Instance.PersonalSelectBLL(this.GetConditionPersonal());
                if (!String.IsNullOrEmpty(strHeadRegistNo))
                {
                    string Condition = String.Format(@" AND TTRUCK.SHEADREGISTERNO LIKE '%{0}%'", strHeadRegistNo.Replace("'", "''"));
                    DropDownListHelper.BindDropDownList(ref cboContract, ComplainBLL.Instance.ContractSelectBLL(this.GetConditionVendor() + Condition), "SCONTRACTID", "SCONTRACTNO", true);
                }
                else
                    DropDownListHelper.BindDropDownList(ref cboContract, ComplainBLL.Instance.ContractSelectBLL(this.GetConditionVendor()), "SCONTRACTID", "SCONTRACTNO", true);

                DropDownListHelper.BindDropDownList(ref cmbPersonalNo, dtEmployee, "SEMPLOYEEID", "FULLNAME", true);
                DropDownListHelper.BindDropDownList(ref cmbPersonalNo2, dtEmployee2, "SEMPLOYEEID", "FULLNAME", true);
                DropDownListHelper.BindDropDownList(ref cmbPersonalNo3, dtEmployee3, "SEMPLOYEEID", "FULLNAME", true);
                DropDownListHelper.BindDropDownList(ref cmbPersonalNo4, dtEmployee4, "SEMPLOYEEID", "FULLNAME", true);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private string GetConditionVendor()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (cboVendor.SelectedIndex > 0)
                sb.Append(" AND TCONTRACT.SVENDORID = '" + cboVendor.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private string GetConditionPersonal()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (cboVendor.SelectedIndex > 0)
                sb.Append(" AND TEMPLOYEE.STRANS_ID = '" + cboVendor.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null)
                return controlToReturn;
        }
        return null;
    }
    private void ClearRequire()
    {
        cmbTopic.Enabled = false;
        cboComplainType.Enabled = false;
        cboVendor.Enabled = true;
        cboContract.Enabled = true;
        cboHeadRegist.Enabled = true;
        cboDelivery.Enabled = true;
        cmbPersonalNo.Enabled = true;
        txtDateTrans.Enabled = true;
        cbxOrganiz.Enabled = true;

        //txtTotalCar.Enabled = false;

        lblReqcmbTopic.Visible = false;
        lblReqtxtComplainAddress.Visible = false;
        lblReqcboComplainType.Visible = false;
        lblReqcboDelivery.Visible = false;
        lblReqcboHeadRegist.Visible = false;
        lblReqcboVendor.Visible = false;
        lblReqcmbPersonalNo.Visible = false;
        lblReqtxtDateTrans.Visible = false;
        lblReqcbxOrganiz.Visible = false;
        lblReqcboContract.Visible = false;
        lblReqtxtTotalCar.Visible = false;

        if (dtHeader.Rows.Count == 0)
            txtDateTrans.Text = DateTime.Now.ToString("dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo);
        else
            txtDateTrans.Text = (dtHeader.Rows[0]["DELIVERYDATE"] != null) ? dtHeader.Rows[0]["DELIVERYDATE"].ToString() : string.Empty;
    }
    private void ClearValue()
    {
        try
        {
            cboDelivery.Items.Clear();
            cboHeadRegist.Items.Clear();
            cboContract.Items.Clear();
            cmbPersonalNo.Items.Clear();

            cboVendor.Items.Clear();
            strHeadRegistNo = string.Empty;
            this.LoadVendor();

            chkLock.Checked = false;
            txtComplainAddress.Value = string.Empty;
            txtTotalCar.Text = string.Empty;
            txtTrailerRegist.Value = string.Empty;

            if (dtHeader.Rows.Count > 0)
                txtDateTrans.Text = (dtHeader.Rows[0]["DELIVERYDATE"] != null) ? dtHeader.Rows[0]["DELIVERYDATE"].ToString() : string.Empty;
            else
                txtDateTrans.Text = DateTime.Now.ToString("dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private void LoadVendor()
    {
        try
        {
            DataTable dtVendor = new DataTable();
            if (!String.IsNullOrEmpty(strHeadRegistNo))
            {
                DropDownListHelper.BindDropDownList(ref cboVendor, ComplainBLL.Instance.VendorSelectByHeadRegisterNoBLL(strHeadRegistNo), "SVENDORID", "SABBREVIATION", true);
                if (cboVendor.Items.Count < 1)
                {
                    throw new Exception("ไม่พบข้อมูล บริษัทผู้ประกอบการขนส่งจากเลขทะเบียนรถที่เลือก");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dialogHEADREGISTERNOSearch", "$('#dialogHEADREGISTERNOSearch').modal('hide');", true);
                }
            }
            else
            {
                dtVendor = ComplainBLL.Instance.VendorSelectBLL();
                DropDownListHelper.BindDropDownList(ref cboVendor, dtVendor, "SVENDORID", "SABBREVIATION", true);
            }

        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private string GetConditionComplain()
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" AND M_TOPIC.TOPIC_ID = '" + cmbTopic.SelectedValue + "'");
            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void InitialHeader()
    {
        try
        {
            dtHeader = new DataTable();
            dtHeader.Columns.Add("TOPICID");
            dtHeader.Columns.Add("TOPICNAME");
            dtHeader.Columns.Add("ORGANIZID");
            dtHeader.Columns.Add("ORGANIZNAME");
            dtHeader.Columns.Add("COMPLAINID");
            dtHeader.Columns.Add("COMPLAINNAME");
            dtHeader.Columns.Add("LOCKDRIVERID");
            dtHeader.Columns.Add("LOCKDRIVERNAME");
            dtHeader.Columns.Add("DELIVERYID");
            dtHeader.Columns.Add("DELIVERYNAME");
            dtHeader.Columns.Add("TYPEPRODUCTID");
            dtHeader.Columns.Add("TYPEPRODUCTNAME");
            dtHeader.Columns.Add("TRUCKID");
            dtHeader.Columns.Add("CARHEAD");
            dtHeader.Columns.Add("CARDETAIL");
            dtHeader.Columns.Add("VENDORID");
            dtHeader.Columns.Add("VENDORNAME");
            dtHeader.Columns.Add("CONTRACTID");
            dtHeader.Columns.Add("CONTRACTNAME");
            dtHeader.Columns.Add("PERSONALID");
            dtHeader.Columns.Add("EMPLOYEENAME");
            dtHeader.Columns.Add("DELIVERYDATE");
            dtHeader.Columns.Add("COMPLAINADDRESS");
            dtHeader.Columns.Add("TOTALCAR");
            dtHeader.Columns.Add("SSERVICEID");
            dtHeader.Columns.Add("ISNEW");
            dtHeader.Columns.Add("WAREHOUSE_TO");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void InitialUpload()
    {
        dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL("COMPLAIN_REQ");
        DropDownListHelper.BindDropDownList(ref cboUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);

        dtUpload = new DataTable();
        dtUpload.Columns.Add("UPLOAD_ID");
        dtUpload.Columns.Add("UPLOAD_NAME");
        dtUpload.Columns.Add("FILENAME_SYSTEM");
        dtUpload.Columns.Add("FILENAME_USER");
        dtUpload.Columns.Add("FULLPATH");

    }

    private void InitialRequestFile()
    {
        try
        {
            dtRequestFile = new DataTable();
            GridViewHelper.BindGridView(ref dgvRequestFile, dtRequestFile, false);

            if (dtHeader.Rows.Count > 0)
            {
                string tmp = string.Empty;

                dtRequestFile = ComplainBLL.Instance.ComplainRequestFileBLL(this.GetConditionRequestFile());
                if (dtRequestFile.Rows.Count > 0)
                {
                    tmp = dtRequestFile.Rows[0]["COMPLAIN_TYPE_NAME"].ToString();

                    for (int i = 1; i < dtRequestFile.Rows.Count; i++)
                    {
                        if (string.Equals(dtRequestFile.Rows[i]["COMPLAIN_TYPE_NAME"].ToString(), tmp))
                            dtRequestFile.Rows[i]["COMPLAIN_TYPE_NAME"] = string.Empty;
                        else
                            tmp = dtRequestFile.Rows[i]["COMPLAIN_TYPE_NAME"].ToString();
                    }
                    GridViewHelper.BindGridView(ref dgvRequestFile, dtRequestFile, false);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string GetConditionRequestFile()
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" AND M_COMPLAIN_TYPE.COMPLAIN_TYPE_ID IN (");

            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                sb.Append("'");
                sb.Append(dtHeader.Rows[i]["COMPLAINID"].ToString());
                sb.Append("',");
            }
            sb.Remove(sb.Length - 1, 1);
            sb.Append(")");

            sb.Append(" AND M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE = 'COMPLAIN_REQ'");
            sb.Append(" ORDER BY M_COMPLAIN_TYPE.COMPLAIN_TYPE_ID");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void StartUpload()
    {
        try
        {
            if (cboUploadType.SelectedIndex == 0)
            {
                alertFail("กรุณาเลือกประเภทไฟล์เอกสาร");
                return;
            }

            if (fileUpload.HasFile)
            {
                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                dtUpload.Rows.Add(cboUploadType.SelectedValue, cboUploadType.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload, false);
            }
            else
            {
                alertFail("กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    private void ValidateUploadFile(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadType.Rows[cboUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "Complain" + "\\" + DocID;
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdUpload_Click(object sender, EventArgs e)
    {
        this.StartUpload();
    }

    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload, false);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
                if ("" + Session["CheckPermission"] == "1")
                //if (!CanWrite)
                {
                    imgDelete.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSaveTab2();
            this.ValidateEmail();

            DataTable dtReqDoc = new DataTable();
            dtReqDoc.Columns.Add("VENDOR");
            dtReqDoc.Columns.Add("LINK");
            string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "admin_Complain_Add_New.aspx?DocID=" + MachineKey.Encode(Encoding.UTF8.GetBytes(DocID), MachineKeyProtection.All);
            //dtReqDoc.Rows.Add(dtUserLogin.Rows[0]["VENDOR_NAME_TH"].ToString(), Link);
            ComplainImportFileBLL.Instance.ComplainAttachInsertBLL(dtUpload, DocID, int.Parse(dtUserLogin.Rows[0]["SUID"].ToString()), string.Empty, string.Empty, string.Empty, ConfigValue.DocStatus2, ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(dtUserLogin.Rows[0]["SUID"].ToString()), string.Empty, true, false), "COMPLAIN_REQ");

            this.SendEmail(12);

            alertSuccess("บันทึกข้อมูล เรียบร้อย (หมายเลขเอกสาร " + DocID + ")", "admin_Complain_lst_New.aspx");
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void SendEmail(int TemplateID)
    {
        try
        {
            DataSet dsSendEmail = new DataSet();
            DataTable dt = new DataTable();
            dt.Columns.Add("EMAIL");
            string EmailList = string.Empty;

            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtEmail = new DataTable();

            if (dtTemplate.Rows.Count > 0)
            {
                this.removeInvalidEmailBeforSend();
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                DataTable dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(DocID);

                Subject = Subject.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());

                Body = Body.Replace("{VENDOR}", dtComplainEmail.Rows[0]["VENDOR"].ToString());
                string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "admin_Complain_add_New.aspx?DocID=" + ConfigValue.GetEncodeText(DocID);
                Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                string CCEmail = "";// (!string.Equals(txtCCEmail.Text.Trim(), string.Empty)) ? "," + txtCCEmail.Text.Trim() : string.Empty;
                string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;

                MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dtComplainEmail.Rows[0]["VENDOR"].ToString(), true, true) + CCEmail + CreateMail, Subject, Body);

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateEmail()
    {
        string EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, ConfigValue.DeliveryDepartmentCode, int.Parse(dtUserLogin.Rows[0]["SUID"].ToString()), string.Empty, true, false);
        var lstEmail = EmailList.Split(',').Select(p => p.Trim()).ToList();
        foreach (var lst in lstEmail)
        {
            if (!IsValidEmail(lst))
            {
                throw new Exception(string.Format("E-mail ไม่ถูกต้อง \'{0}\'", lst));
            }
        }

    }

    private bool IsValidEmail(string _email)
    {
        return Regex.IsMatch(_email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
    }

    private void ValidateSaveTab2()
    {
        try
        {
            //dtRequestFile, dtUpload
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            if (dtUpload.Rows.Count == 0)
            {
                for (int j = 0; j < dtRequestFile.Rows.Count; j++)
                {
                    if (j == 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>โปรดแนบเอกสาร</td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr>");
                        sb.Append("<td></td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
            }
            else
            {
                for (int i = 0; i < dtRequestFile.Rows.Count; i++)
                {
                    for (int j = 0; j < dtUpload.Rows.Count; j++)
                    {
                        if (string.Equals(dtRequestFile.Rows[i]["UPLOAD_ID"].ToString(), dtUpload.Rows[j]["UPLOAD_ID"].ToString()))
                            break;

                        if (j == dtUpload.Rows.Count - 1)
                        {
                            if (string.Equals(sb.ToString(), "<table>"))
                            {
                                sb.Append("<tr>");
                                sb.Append("<td>โปรดแนบเอกสาร</td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                            else
                            {
                                sb.Append("<tr>");
                                sb.Append("<td></td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                        }
                    }
                }
            }
            sb.Append("</table>");

            if (!string.Equals(sb.ToString(), "<table></table>"))
            {
                throw new Exception(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void removeInvalidEmailBeforSend()
    {
        //if (!string.IsNullOrEmpty(txtCCEmail.Text))
        //{
        //    txtCCEmail.Text = txtCCEmail.Text.Trim();
        //    var lstEmail = txtCCEmail.Text.Split(',').ToList();
        //    foreach (var lst in lstEmail)
        //    {
        //        if (!IsValidEmail(lst)) lstEmail.Remove(lst);
        //    }
        //    txtCCEmail.Text = String.Join(",", lstEmail);
        //}
    }
    protected void dgvHeader_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
    protected void dgvHeader_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void dgvHeader_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void cmbPersonalNo_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void cmbPersonalNo2_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void cmbPersonalNo3_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void cmbPersonalNo4_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}