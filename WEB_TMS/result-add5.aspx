﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="result-add5.aspx.cs" Inherits="result_add5" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" Runat="Server">
<dx:ASPxCallbackPanel ID = "xcpn" runat="server" ClientInstanceName="xcpn"   HideContentOnCallback="false" CausesValidation="False"   oncallback="xcpn_Callback" onload="xcpn_Load" >
      <ClientSideEvents EndCallback="function(s,e){ eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="5" cellspacing="1">
                                              <tr>
                                                <td colspan="4" align="right"><%--<input type="submit" name="button3" id="button3" value="ดูประวัติรถ" onClick="javascript:location.href='car_history.htm'">--%>
                                                <%--<input type="submit" name="button7" id="button7" value="ประวัติการรับบริการ" onClick="javascript:location.href='service_history.htm'">--%>
                                                </td>
                                                </tr>
                                                <tr>
                                                <td bgcolor="#B9EAEF">วันที่ยืนคำขอ</td>
                                                <td><dx:ASPxLabel ID = "lblREQUEST_DATE" runat="server"></dx:ASPxLabel></td>
                                                <td bgcolor="#B9EAEF">วันที่หมดอายุวัดน้ำ</td>
                                                <td><dx:ASPxLabel ID = "lblDWATEREXPIRE" runat="server"></dx:ASPxLabel></td>
                                                </tr>
                                                <tr>
                                                <td width="18%" bgcolor="#B9EAEF">วันที่นัดหมาย</td>
                                                <td width="32%" class="active"><dx:ASPxLabel ID = "lblSERVICE_DATE" runat="server"></dx:ASPxLabel> </td>
                                                <td width="19%" bgcolor="#B9EAEF">วันที่ รข. อนุมัติคำขอ</td>
                                                <td width="31%"><dx:ASPxLabel ID = "lblAPPOINTMENT_BY_DATE" runat="server"></dx:ASPxLabel></td>
                                                </tr>
                                                <tr>
                                                <td bgcolor="#B9EAEF">ประเภทคำขอ</td>
                                                <td><dx:ASPxLabel ID = "lblREQTYPE_NAME" runat="server"></dx:ASPxLabel></td>
                                                <td bgcolor="#B9EAEF">สาเหตุ</td>
                                                <td><dx:ASPxLabel ID = "lblCAUSE_NAME" runat="server"></dx:ASPxLabel></td>
                                                </tr>
                                                <tr>
                                                <td bgcolor="#B9EAEF">ทะเบียนรถ</td>
                                                <td><dx:ASPxLabel ID = "lblREGISTERNO" runat="server"></dx:ASPxLabel></td>
                                                <td bgcolor="#B9EAEF">ประเภทรถ</td>
                                                <td><dx:ASPxLabel ID = "lblTypeCar" runat="server"></dx:ASPxLabel> - ความจุ <dx:ASPxLabel ID = "lblTotalCap" runat="server"></dx:ASPxLabel> ลิตร</td>
                                                </tr>
                                                <tr>
                                                <td bgcolor="#B9EAEF">บริษัทขนส่ง </td>
                                                <td colspan="3"><dx:ASPxLabel ID = "lblVendorName" runat="server"></dx:ASPxLabel></td>
                                                </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr id = "trtab">
                                    <td>
                                         <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                            <tr>
                                            <td width="15%" align="center" bgcolor="#EDEDED">
                                                <dx:ASPxHyperLink ID = "hplT1" runat="server" Text="ประวัติการตรวจ" style="text-decoration:none; cursor:pointer; color:Blue;"><ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT1;')}" /></dx:ASPxHyperLink>
                                            </td>
                                            <td width="15%" align="center" bgcolor="#EDEDED">
                                                <dx:ASPxHyperLink ID = "hplT2" runat="server" Text="ตรวจสภาพภายนอก/ใน" style="text-decoration:none; cursor:pointer; color:Blue;"><ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT2;')}" /></dx:ASPxHyperLink>
                                            </td>
                                            <td width="15%" align="center" bgcolor="#EDEDED">
                                                <dx:ASPxHyperLink ID = "hplT3" runat="server" Text="ผลตรวจลงน้ำ" style="text-decoration:none; cursor:pointer; color:Blue;"><ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT3;')}" /></dx:ASPxHyperLink>
                                            </td>
                                            <td width="15%" align="center" bgcolor="#EDEDED">
                                             <dx:ASPxHyperLink ID = "hplT4" runat="server" Text="คำนวณค่าธรรมเนียมเพิ่ม" style="text-decoration:none; cursor:pointer; color:Blue;"><ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT4;')}" /></dx:ASPxHyperLink>
                                            </td>
                                              <td width="15%" align="center" bgcolor="#EDEDED">
                                                                <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text="รูปภาพการตรวจสอบ" Style="text-decoration: none;
                                                                    cursor: pointer; color: Blue;">
                                                                    <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectImg;')}" />
                                                                </dx:ASPxHyperLink>
                                                            </td>
                                            <td width="15%" align="center" bgcolor="#00CC33">
                                               <dx:ASPxHyperLink ID = "hplT5" runat="server" Text="บันทึกผลปิดงาน" style="text-decoration:none; cursor:pointer; color:Blue;"></dx:ASPxHyperLink>
                                            </td>
                                            <td width="10%">&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td>
                                                &nbsp;
                                             </td>
                                          </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="3" cellspacing="2">
                                          <tr id="trTeabpan" runat="server">
                                            <td width="6%" align="left" bgcolor="#B9EAEF">ผลการเทียบแป้น</td>
                                            </tr>
                                          <tr>
                                            <td align="left">
                                                <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                                  <tr id="trService" runat="server">
                                                    <td width="30%" align="right" bgcolor="#F7F7F7">ใบเสร็จค่าธรรมเนียมเพิ่มเติม</td>
                                                    <td width="70%">
                                                        <dx:ASPxLabel ID = "lblOpenInvoiceMore" runat="server" ClientInstanceName="lblOpenInvoiceMore" EncodeHtml="false"></dx:ASPxLabel>
                                                    </td>
                                                    </tr>
                                                  <tr id="trPan" runat="server">
                                                    <td align="right" bgcolor="#F7F7F7">เอกสารเทียบแป้น</td>
                                                    <td>
                                                        <dx:ASPxLabel ID = "lblOpenDocPan" runat="server" ClientInstanceName="lblOpenDocPan" EncodeHtml="false"></dx:ASPxLabel>
                                                    </td>
                                                  </tr>
                                                  <tr  id="trPass" runat="server">
                                                    <td align="right" bgcolor="#F7F7F7">ผลการตรวจสอบ</td>
                                                    <td>
                                                        <dx:ASPxRadioButtonList ID = "rblStatusChecking" runat="server" RepeatDirection="Horizontal" SkinID="rblStatus">
                                                            <Items>
                                                                <dx:ListEditItem Value = "Y" Text="เทียบแป้นผ่าน" />
                                                                <dx:ListEditItem Value = "N" Text="เทียบแป้นไม่ผ่าน" />
                                                            </Items>
                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add">
                                                                <RequiredField ErrorText="ระบุผลตรวจสอบ" IsRequired="true"/>
                                                            </ValidationSettings>
                                                        </dx:ASPxRadioButtonList>
                                                      </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            </tr>
                                          <tr>
                                            <td align="left" bgcolor="#B9EAEF">รายละเอียด</td>
                                            </tr>
                                          <tr>
                                            <td align="left" valign="top">
                                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                              <tr>
                                                <td width="30%" align="left">1. วันที่ยื่นคำขอ </td>
                                                <td width="70%">
                                                    <dx:ASPxLabel ID = "lblREQUEST_DATE2" runat="server"></dx:ASPxLabel>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td align="left" bgcolor="#F7F7F7">2. วันที่ตอบรับ</td>
                                                <td bgcolor="#F7F7F7"><dx:ASPxLabel ID = "lblDMVCONFIRM_DATE" runat="server"></dx:ASPxLabel></td>
                                              </tr>
                                              <tr>
                                                <td align="left">3. วันที่ดำเนินการ </td>
                                                <td><dx:ASPxLabel ID = "lblSERVICE_DATE2" runat="server"></dx:ASPxLabel></td>
                                              </tr>
                                              <tr>
                                                <td align="left" bgcolor="#F7F7F7">4. วันที่บันทึกผลการตรวจสอบ </td>
                                                <td bgcolor="#F7F7F7"><dx:ASPxLabel ID = "lblRESULT_CHECKING_DATE" runat="server"></dx:ASPxLabel></td>
                                              </tr>
                                              <tr>
                                                <td align="left">5. วันที่ชำระค่าธรรมเนียม </td>
                                                <td><dx:ASPxLabel ID = "lblDoc0001" runat="server"></dx:ASPxLabel></td>
                                              </tr>
                                              <tr>
                                                <td align="left" bgcolor="#F7F7F7">6. วันที่ชำระค่าธรรมเนียมเพิ่มเติม </td>
                                                <td bgcolor="#F7F7F7"><dx:ASPxLabel ID = "lblDoc0005" runat="server"></dx:ASPxLabel></td>
                                              </tr>
                                              <tr>
                                                <td align="left">7. วันที่ส่งใบเทียบแป้น </td>
                                                <td><dx:ASPxLabel ID = "lblDoc0006" runat="server"></dx:ASPxLabel></td>
                                              </tr>
                                              <tr>
                                                <td align="left" bgcolor="#F7F7F7">8. วันที่ปิดงาน </td>
                                                <td bgcolor="#F7F7F7"><dx:ASPxLabel ID = "lblCLOSEJOB_DATE" runat="server"></dx:ASPxLabel></td>
                                              </tr>
                                            </table>
                                            </td>
                                          </tr>
                                     </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <dx:ASPxButton ID = "btnSaveT5" runat="server" ClientInstanceName="btnSaveT5" AutoPostBack="false" Text = "บันทึกข้อมูล" CssClass="dxeLineBreakFix">
                                            <ClientSideEvents Click="function(s,e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('SAVET5;') }" />
                                        </dx:ASPxButton>

                                        <dx:ASPxButton ID = "bntExitT5" runat="server" ClientInstanceName="bntExitT5" AutoPostBack="false" Text="ออกจากหน้านี้" CssClass="dxeLineBreakFix">
                                          <ClientSideEvents Click="function(s,e) {  window.location = 'approve_mv.aspx';}" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
</dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" Runat="Server">
</asp:Content>

