﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="Truck_History_Info.aspx.cs"
    Inherits="Truck_History_Info" StylesheetTheme="Aqua" UICulture="en-US" Culture="en-US" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
<style type="text/css">
.green
{
    color:green;
}
.orange
{
     color: orange;
}
.red
{
     color: red;
}   
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpnMain" runat="server" ClientInstanceName="xcpnMain"
        CausesValidation="False">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
        <PanelCollection>
            <dx:PanelContent ID="pncMain" runat="server">
                <dx:ASPxRoundPanel ID="rpnInfomation" runat="Server" ClientInstanceName="rpnInfomation" Width="980px"
                    HeaderText="รายละเอียดการใช้งาน" BackColor="#EEEEEE">
                    <PanelCollection>
                        <dx:PanelContent ID="pctInfomation" runat="Server">
                            <table border="0" cellspacing="2" cellpadding="3" width="100%">
                                <tr>
                                    <td colspan="4" align="right">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ทะเบียนรถ : </td>
                                    <td>
                                        <dx:ASPxTextBox ID="txtHTruckNo" runat="Server" ClientEnabled="false"
                                            Width="180px">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxTextBox ID="txtVendorID" runat="server" Width="200px" ClientVisible="false"></dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        หมายเลขแชสซีย์ : </td>
                                    <td>
                                        <dx:ASPxTextBox ID="txtHChasis" runat="Server" ClientInstanceName="txtHChasis" ClientEnabled="false"
                                            Width="180px">
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>                               
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
                <br />
                <dx:ASPxRoundPanel ID="rpnDetail" runat="Server" ClientInstanceName="rpnDetail" Width="980px" HeaderText="ประวัติการเปลี่ยนแปลงข้อมูล">
                    <PanelCollection>
                            <dx:PanelContent ID="pctDetail" runat="Server">
                                <table border="0" cellspacing="2" cellpadding="3" width="100%">
                                    <tr>
                                        <td align="center">
                                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="false"  width="100%" SettingsPager-Mode="ShowAllRecords">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn FieldName="UPDATE_DATE" Caption="วันที่อัพเดทข้อมูล" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="dd/MM/yyyy HH:mm">
                                                      <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                                      <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="FULLNAME" Caption="ผู้อัพเดทข้อมูล"  HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Left">
                                                      <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                                      <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="ข้อมูล" HeaderStyle-HorizontalAlign="Center">
                                                    <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="center">
                                                        </CellStyle>
                                                        <DataItemTemplate>
                                                        <div class="row">
                                                            <label class="col-md-6  text-right"><%#Eval("HEADDER") %>:</label>
                                                            <div class="col-md-6"> <%# Eval("OLDDATA")%> </div>
                                                            </div>
                                                            <div class="row ">
                                                            <label class="col-md-6  text-right">>></label>
                                                            <div class="col-md-6"><%# Eval("NEWDATA")%> </div>
                                                        </div>
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                            </dx:ASPxGridView> 
                                            <asp:SqlDataSource ID="dts1" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                    CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                    SelectCommand="">
                                                                    
                                                                    </asp:SqlDataSource>
                                                                
                           
                                    </td>
                                </tr>
                            </table>
                            </dx:PanelContent>
                        </PanelCollection>
                </dx:ASPxRoundPanel>               
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
