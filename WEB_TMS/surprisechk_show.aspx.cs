﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class surprisechk_show : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadTerminal();
        this.Culture = "en-US";
        this.UICulture = "en-US";
        if (!IsPostBack)
        {
            dteStart.Text = DateTime.Now.ToString("dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo);

            //Cache.Remove(sds.CacheKeyDependency);
            //Cache[sds.CacheKeyDependency] = new object();
            //sds.Select(new System.Web.UI.DataSourceSelectArguments());
            //sds.DataBind();

            LogUser("27", "R", "เปิดดูข้อมูลหน้า Surprise check", "");

            this.AssignAuthen();           

            DataLoad();
        }
        
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;                
            }
            if (!CanWrite)
            {
                btnSave.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    private void LoadTerminal()
    {
        string message = string.Empty;
        try
        {
            //cboStatus.SelectedValue
            string query = "SELECT STERMINALID, STERMINALNAME FROM TTERMINAL_SAP WHERE STERMINALID LIKE 'H%' OR STERMINALID LIKE 'J%' OR STERMINALID LIKE 'K%'";

            DataTable dt = CommonFunction.Get_Data(sql, query);

            if (dt != null && dt.Rows.Count > 0)
            {
                DataTable dt_result = new DataTable();
                dt_result.Columns.Add("STERMINALNAME", typeof(string));
                dt_result.Columns.Add("STERMINALID", typeof(string));
                dt_result.Rows.Add("ทั้งหมด", "");
                for (int i=0;i<dt.Rows.Count;i++)
                {
                    dt_result.Rows.Add(dt.Rows[i]["STERMINALNAME"], dt.Rows[i]["STERMINALID"]);
                }
                cboStatus.DataSource = dt_result;
                cboStatus.DataTextField = "STERMINALNAME";
                cboStatus.DataValueField = "STERMINALID";
                cboStatus.DataBind();
            }
   
        }

        catch (Exception ex)
        {
            message = ex.Message;
        }
        finally
        {
            //GlobalClass.CloseConnection(out message);

        }
    }

    public void DataLoad()
    {
        string message = string.Empty;
        try
        {
            //cboStatus.SelectedValue
            string query = "SELECT ROW_NUMBER () OVER (ORDER BY  C.SCONTRACTID) AS ID1,C.SCONTRACTID,VS.SVENDORNAME,C.SCONTRACTNO,"
                            + "TRUNC(nvl(C.NTRUCK, 0)) AS NTRUCK, TRUNC(nvl(sc.STANDBYCAR, 0)) AS STANDBYCAR,"
                            + "TRUNC(nvl(o.SURPRISECOUNT, 0)) AS SURPRISECOUNT,o.DCHECK,"
                            + "TRUNC((nvl(o.SURPRISECOUNT, 0) / CASE WHEN C.NTRUCK = 0 THEN 1 ELSE nvl(C.NTRUCK, 1) END) * 100) AS PERSENT, CT.COUNTTRUCK FROM "
                            + "((TCONTRACT c LEFT JOIN TVENDOR_SAP vs ON c.SVENDORID = VS.SVENDORID) LEFT JOIN(SELECT SCONTRACTID, COUNT(*) AS STANDBYCAR FROM TCONTRACT_TRUCK WHERE CSTANDBY = 'Y' GROUP BY SCONTRACTID) sc ON sc.SCONTRACTID = C.SCONTRACTID) "
                            + " LEFT JOIN(SELECT SCONTRACTID, COUNT(*) AS SURPRISECOUNT, MAX(DCHECK) AS DCHECK FROM TSURPRISECHECKHISTORY "
                            + " WHERE COK = '1' AND nvl(CNOCHECK,'0') = '0' GROUP BY SCONTRACTID)  o ON o.SCONTRACTID = c.SCONTRACTID LEFT JOIN "
                            + " (select SCONTRACTID, count(CHECKCONFIRM) as COUNTTRUCK FROM (SELECT tt.SCONTRACTID, o.CHECKCONFIRM FROM(TCONTRACT_TRUCK tt INNER JOIN TTRUCK t ON TT.STRUCKID = T.STRUCKID) LEFT JOIN "
                            + " (SELECT MAX(NSURPRISECHECKID) AS CHECKID, SCONTRACTID, STRUCKID, COUNT(*) AS CHECKCONFIRM, MAX(DCHECK) AS DCHECK FROM TSURPRISECHECKHISTORY "
                            + " WHERE nvl(CNOCHECK, '0') = '0' AND COK = '1' GROUP BY SCONTRACTID, STRUCKID) o ON o.SCONTRACTID = TT.SCONTRACTID AND o.STRUCKID = TT.STRUCKID) "
                            + " WHERE nvl(CHECKCONFIRM, NULL) <> '0' group by SCONTRACTID ) ct on C.SCONTRACTID = ct.SCONTRACTID ";
            if (txtSearch.Text != "" || dteStart.Text != "" || cboStatus.SelectedItem.Value != "")
            {
                query += " WHERE ";
                bool first_value = true;
                if (txtSearch.Text != "")
                {
                    query += " (C.SCONTRACTNO LIKE '%' || '" + txtSearch.Text + "' || '%' OR VS.SVENDORNAME LIKE '%' || '" + txtSearch.Text + "' || '%' ) ";
                    first_value = false;
                }
                
                if (dteStart.Text != "")
                {
                    
                    string[] d = dteStart.Text.Split('/');
                    int y = 0;
                    string dd = "";
                    if (d.Length == 3)
                    {
                        dd = d[0] + "/" + d[1] + "/";
                        y =  Convert.ToInt32(d[2]);
                        dd += y.ToString();
                    }
                    if (dd != "")
                    {
                        if (!first_value)
                        {
                            query += " AND ";
                        }
                        query += " (TO_DATE('" + dd + "','dd/MM/yyyy') >= c.DBEGIN  AND TO_DATE('" + dd + "','dd/MM/yyyy') <= C.DEND) ";
                        first_value = false;
                    }                    
                }                
                if (cboStatus.SelectedItem.Value != "")
                {
                    if (!first_value)
                    {
                        query += " AND ";
                    }
                    query += " nvl(c.STERMINALID,1) LIKE '%' || '" + cboStatus.SelectedItem.Value + "' || '%'";
                }
            }
            //+ "WHERE (C.SCONTRACTNO LIKE '%' || '" + txtSearch.Text + "' || '%' OR VS.SVENDORNAME LIKE '%' || '" + txtSearch.Text + "' || '%' ) "
            //+ "AND (TO_DATE('" + dteStart.Text + "','dd/MM/yyyy') >= c.DBEGIN  AND TO_DATE('" + dteStart.Text + "','dd/MM/yyyy') <= C.DEND) "
            //+ "AND nvl(c.STERMINALID,1) LIKE '%' || '" + cboStatus.SelectedItem.Value + "' || '%'";

            DataTable dt = CommonFunction.Get_Data(sql, query);
            gvw.DataSource = dt;
            gvw.DataBind();

            //gvw.HeaderRow.Cells[7].Attributes["data-class"] = "expand";
            //gvw.HeaderRow.TableSection = TableRowSection.TableHeader;

            lblCarCount.Text = dt.Rows.Count.ToString();
        }

        catch (Exception ex)
        {
            message = ex.Message;
        }
        finally
        {
            //GlobalClass.CloseConnection(out message);

        }
    }

    public void DataLoad1()
    {
        string message = string.Empty;
        try
        {
            //cboStatus.SelectedValue
            string query = "SELECT ROW_NUMBER () OVER (ORDER BY  C.SCONTRACTID) AS ID1,C.SCONTRACTID,VS.SVENDORNAME,C.SCONTRACTNO,"
                            + "TRUNC(nvl(C.NTRUCK, 0)) AS NTRUCK, TRUNC(nvl(sc.STANDBYCAR, 0)) AS STANDBYCAR,"
                            + "TRUNC(nvl(o.SURPRISECOUNT, 0)) AS SURPRISECOUNT,o.DCHECK,"
                            + "TRUNC((nvl(o.SURPRISECOUNT, 0) / CASE WHEN C.NTRUCK = 0 THEN 1 ELSE nvl(C.NTRUCK, 1) END) * 100) AS PERSENT, CT.COUNTTRUCK FROM "
                            + "((TCONTRACT c LEFT JOIN TVENDOR_SAP vs ON c.SVENDORID = VS.SVENDORID) LEFT JOIN(SELECT SCONTRACTID, COUNT(*) AS STANDBYCAR FROM TCONTRACT_TRUCK WHERE CSTANDBY = 'Y' GROUP BY SCONTRACTID) sc ON sc.SCONTRACTID = C.SCONTRACTID) "
                            + " LEFT JOIN(SELECT SCONTRACTID, COUNT(*) AS SURPRISECOUNT, MAX(DCHECK) AS DCHECK FROM TSURPRISECHECKHISTORY "
                            + " WHERE COK = '1' AND nvl(CNOCHECK,'0') = '0' GROUP BY SCONTRACTID)  o ON o.SCONTRACTID = c.SCONTRACTID LEFT JOIN "
                            + " (select SCONTRACTID, count(CHECKCONFIRM) as COUNTTRUCK FROM (SELECT tt.SCONTRACTID, o.CHECKCONFIRM FROM(TCONTRACT_TRUCK tt INNER JOIN TTRUCK t ON TT.STRUCKID = T.STRUCKID) LEFT JOIN "
                            + " (SELECT MAX(NSURPRISECHECKID) AS CHECKID, SCONTRACTID, STRUCKID, COUNT(*) AS CHECKCONFIRM, MAX(DCHECK) AS DCHECK FROM TSURPRISECHECKHISTORY "
                            + " WHERE nvl(CNOCHECK, '0') = '0' AND COK = '1' GROUP BY SCONTRACTID, STRUCKID) o ON o.SCONTRACTID = TT.SCONTRACTID AND o.STRUCKID = TT.STRUCKID) "
                            + " WHERE nvl(CHECKCONFIRM, NULL) <> '0' group by SCONTRACTID ) ct on C.SCONTRACTID = ct.SCONTRACTID ";
                            //"SELECT ROW_NUMBER () OVER (ORDER BY  C.SCONTRACTID) AS ID1,"
                            //+ "C.SCONTRACTID,VS.SVENDORNAME,C.SCONTRACTNO,TRUNC(nvl(C.NTRUCK,0)) AS NTRUCK,TRUNC(nvl(sc.STANDBYCAR,0)) AS STANDBYCAR,"
                            //+ "TRUNC(nvl(o.SURPRISECOUNT,0)) AS SURPRISECOUNT,o.DCHECK,"
                            //+ " TRUNC((nvl(o.SURPRISECOUNT,0) / CASE WHEN C.NTRUCK = 0 THEN 1 ELSE nvl(C.NTRUCK,1) END)  * 100) AS PERSENT "
                            //+ " FROM ((TCONTRACT c LEFT JOIN TVENDOR_SAP vs ON c.SVENDORID = VS.SVENDORID) LEFT JOIN "
                            //+ "(SELECT SCONTRACTID, COUNT(*) AS STANDBYCAR FROM TCONTRACT_TRUCK WHERE CSTANDBY = 'Y' GROUP BY SCONTRACTID) sc "
                            //+ "ON sc.SCONTRACTID = C.SCONTRACTID)LEFT JOIN (SELECT SCONTRACTID ,COUNT(*) AS SURPRISECOUNT,MAX(DCHECK) AS DCHECK "
                            //+ "FROM TSURPRISECHECKHISTORY WHERE COK = '1' AND nvl(CNOCHECK,'0') = '0' GROUP BY SCONTRACTID)  o  ON o.SCONTRACTID  = c.SCONTRACTID "
                            ////+ "WHERE (C.SCONTRACTNO LIKE '%' || '" + txtSearch.Text + "' || '%' OR VS.SVENDORNAME LIKE '%' || '" + txtSearch.Text + "' || '%' ) "
                            ////+ "AND (TO_DATE('" + dteStart.Text + "','dd/MM/yyyy') >= c.DBEGIN  AND TO_DATE('" + dteStart.Text + "','dd/MM/yyyy') <= C.DEND) "
                            ////+ "AND nvl(c.STERMINALID,1) LIKE '%' || '" + cboStatus.SelectedItem.Value + "' || '%'";

            DataTable dt = CommonFunction.Get_Data(sql, query);
            gvw.DataSource = dt;
            gvw.DataBind();

            //gvw.HeaderRow.Cells[7].Attributes["data-class"] = "expand";
            //gvw.HeaderRow.TableSection = TableRowSection.TableHeader;

            lblCarCount.Text = dt.Rows.Count.ToString();
        }

        catch (Exception ex)
        {
            message = ex.Message;
        }
        finally
        {
            //GlobalClass.CloseConnection(out message);

        }
    }

    protected void gvw_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DataLoad();
    }

    protected void gvw_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Visible = false; 
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[2].Visible = false;
            e.Row.Cells[6].Visible = false;
            e.Row.Cells[7].Visible = false;
            e.Row.Cells[8].Visible = false;
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string ContractID = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "SCONTRACTID"));
            string strContract = @"SELECT ROW_NUMBER () OVER (ORDER BY  t.SHEADREGISTERNO) AS ID1, tt.STRUCKID, t.SHEADREGISTERNO,t.STRAILERREGISTERNO,
                CASE WHEN tt.CSTANDBY = 'Y' THEN 'รถสำรอง' ELSE 'รถในสัญญา' END AS  CARTYPE,o.CHECKCONFIRM,o.DCHECK ,
                (SELECT SSURPRISECHECKBY FROM TSURPRISECHECKHISTORY WHERE NSURPRISECHECKID = o.CHECKID) AS SSURPRISECHECKBY
                 FROM (TCONTRACT_TRUCK tt INNER JOIN TTRUCK t ON TT.STRUCKID = T.STRUCKID) LEFT JOIN
                 (SELECT MAX(NSURPRISECHECKID) AS CHECKID, SCONTRACTID,STRUCKID,COUNT(*) AS CHECKCONFIRM,MAX(DCHECK) AS DCHECK FROM TSURPRISECHECKHISTORY
                  WHERE nvl(CNOCHECK,'0') = '0' AND COK = '1' GROUP BY SCONTRACTID,STRUCKID) o ON o.SCONTRACTID = TT.SCONTRACTID AND o.STRUCKID = TT.STRUCKID WHERE tt.SCONTRACTID = " + ContractID;

            DataTable dt = CommonFunction.Get_Data(sql, strContract);
            GridView sgvw = new GridView();            
            sgvw.DataSource = dt;
            sgvw.ID = "SCONTRACTID" + ContractID;
            sgvw.AutoGenerateColumns = false;
            sgvw.CssClass = "table table-primary";
            sgvw.HeaderStyle.CssClass = "HeaderStyle";
            sgvw.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            sgvw.BorderColor = System.Drawing.ColorTranslator.FromHtml("#DEDFDE");
            sgvw.BorderStyle = BorderStyle.None;
            sgvw.BorderWidth = 1;
            // BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
            sgvw.RowDataBound += new GridViewRowEventHandler(sgvw_RowDataBound);

            BoundField bf1 = new BoundField();
            bf1.DataField = "ID1";
            bf1.HeaderText = "ที่";
            bf1.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            sgvw.Columns.Add(bf1);

            BoundField bf2 = new BoundField();
            bf2.DataField = "SHEADREGISTERNO";
            bf2.HeaderText = "ทะเบียน(หัว)";
            bf2.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            sgvw.Columns.Add(bf2);

            BoundField bf3 = new BoundField();
            bf3.DataField = "STRAILERREGISTERNO";
            bf3.HeaderText = "ทะเบียน(ท้าย)";
            bf3.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            sgvw.Columns.Add(bf3);

            BoundField bf4 = new BoundField();
            bf4.DataField = "CARTYPE";
            bf4.HeaderText = "ประเภทรถ";
            bf4.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            sgvw.Columns.Add(bf4);

            BoundField bf5 = new BoundField();
            bf5.DataField = "CHECKCONFIRM";
            bf5.HeaderText = "ตรวจแล้ว(ครั้ง)";
            bf5.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            sgvw.Columns.Add(bf5);

            BoundField bf6 = new BoundField();
            bf6.DataField = "DCHECK";
            bf6.HeaderText = "วันที่ตรวจล่าสุด";
            bf6.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            sgvw.Columns.Add(bf6);

            BoundField bf7 = new BoundField();
            bf7.DataField = "SSURPRISECHECKBY";
            bf7.HeaderText = "เช็คครั้งล่าสุดโดย";
            bf7.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            sgvw.Columns.Add(bf7);

            Image btn = new Image();
            btn.ID = "bnDetail";
            btn.ImageUrl = "~/Images/autocomplete.gif";
            btn.Attributes.Add("onclick", "javascript: gvrowtoggle(" + (e.Row.RowIndex + (e.Row.RowIndex + 3)) + ")");
            //btn.CssClass = "btn btn-info";

            Table tbl = (Table)e.Row.Parent;
            //tbl.CssClass = "table table-primary";
            //tbl.Style.Add("width", "100% !important");
            GridViewRow tr = new GridViewRow(e.Row.RowIndex + 1, -1, DataControlRowType.EmptyDataRow, DataControlRowState.Normal);
            tr.CssClass = "hidden";
            TableCell tc = new TableCell();
            tc.ColumnSpan = gvw.Columns.Count;
            tc.BorderStyle = BorderStyle.None;
            tc.BackColor = System.Drawing.Color.White;//System.Drawing.Color.AliceBlue;
            tc.Controls.Add(sgvw);
            tr.Cells.Add(tc);
            tbl.Rows.Add(tr);
            e.Row.Cells[7].Controls.Add(btn);

            sgvw.DataBind();
        }
    }

    protected void gvw_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        DataLoad();
        gvw.PageIndex = e.NewPageIndex;
        gvw.DataBind();
    }

    private void sgvw_RowDataBound(object sender, EventArgs e)
    {
        
    }

    
    

    protected void gvw_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView ProductGrid = (GridView)sender;

            // Creating a Row
            GridViewRow HeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

            TableCell HeaderCell = new TableCell();
            HeaderCell.Text = "ที่";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.VerticalAlign = VerticalAlign.Middle;
            HeaderCell.RowSpan = 2;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb"); 
            HeaderRow.Cells.Add(HeaderCell);


            HeaderCell = new TableCell();
            HeaderCell.Text = "ชื่อผู้ประกอบการ";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.VerticalAlign = VerticalAlign.Middle;
            HeaderCell.RowSpan = 2;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "สัญญา";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.VerticalAlign = VerticalAlign.Middle;
            HeaderCell.RowSpan = 2;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "จำนวนรถ";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.VerticalAlign = VerticalAlign.Middle;
            HeaderCell.ColumnSpan = 2;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "ประวัติการตรวจ";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.VerticalAlign = VerticalAlign.Middle;
            HeaderCell.RowSpan = 1;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "วันที่ตรวจล่าสุด";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.VerticalAlign = VerticalAlign.Middle;
            HeaderCell.RowSpan = 2;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.VerticalAlign = VerticalAlign.Middle;
            HeaderCell.RowSpan = 2;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);

            //HeaderCell = new TableCell();
            //HeaderCell.Text = "ContactID";
            //HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            //HeaderCell.RowSpan = 2;
            //HeaderCell.CssClass = "HeaderStyle";
            //HeaderRow.Cells.Add(HeaderCell);

            ProductGrid.Controls[0].Controls.AddAt(0, HeaderRow);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Response.Redirect("surpriseChk_lst.aspx?s=1");
    }
}