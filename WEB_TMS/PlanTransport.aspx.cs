﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using DevExpress.Web.ASPxGridView;
using System.Data.OracleClient;
using DevExpress.Web.ASPxClasses;
using System.Globalization;
using DevExpress.Web.ASPxCallback;
using TMS_BLL.Master;
using TMS_BLL.Transaction.OrderPlan;

public partial class PlanTransport : PageBase
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    string connIVMS = WebConfigurationManager.ConnectionStrings["IVMSConnectionString"].ConnectionString;
    string SMENUID = "59";
    OracleTransaction tran = null;
    int countPlan
    {
        get { return Session["countPlan"] == null ? 0 : (int)Session["countPlan"]; }
        set { Session["countPlan"] = value; }
    }
    //private  DataTable dtCarRegis = new DataTable();
    //private  DataTable dtDo = new DataTable();

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        gvw.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvw_AfterPerformCallback);
        // gvw.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        gvw.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(gvw_HtmlRowPrepared);
        gvw2.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvw2_AfterPerformCallback);
        gvw2.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(gvw2_HtmlRowPrepared);
        //gvw2.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvw2_HtmlDataCellPrepared);
        gvw3.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvw3_AfterPerformCallback);
        gvwPop.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwPop_CustomColumnDisplayText);

        if (!IsPostBack)
        {

            LogUser(SMENUID, "R", "เปิดดูข้อมูลหน้า จัดแผน", "");
            Session["dtCarRegis"] = null;
            //DataTable dtccc = Session["dtCarRegis"] as DataTable;
            //UserID = Session["UserID"] + "";
            edtAssign.Text = DateTime.Now.ToString("dd/MM/yyyy");
            SetDataTableCAR();
            //var watch = System.Diagnostics.Stopwatch.StartNew();
            //watch.Start();
            ListData();
            //watch.Stop();
            //ListData2();
            //ListData3();
            SETSCONTRACT();

            #region Check Permission
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            //getOldAuthenticateCode();  //ถ้าเอาขึ้นโปร ต้องเปิดใช้งาน   
            this.AssignAuthen(); //ถ้าเอาขึ้นโปร ต้องปิดเปิดใช้งานทั้ง function   
            #endregion


        }

        if (Session["DataTTERMINAL"] == null)
        {
            Session["DataTTERMINAL"] = ContractBLL.Instance.TTERMINALSelect();
        }
        if (Session["DataTruckByVendorID"] == null)
        {
            Session["DataTruckByVendorID"] = PlanTransportBLL.Instance.GetTruckByVendorID(Session["SVDID"] + string.Empty);
        }
        cboSelect.DataSource = Session["DataTTERMINAL"];
        cboSelect.DataBind();
        cboCarregis.DataSource = Session["DataTruckByVendorID"];
        cboCarregis.DataBind();
    }
    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;

            }
            if (!CanWrite)
            {

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void getOldAuthenticateCode()
    {
        bool chkurl = false;
        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == "2")
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            chkurl = true;

                            break;

                        case "2":
                            chkurl = true;

                            break;
                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        if (chkurl == false)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
    }


    #region gvw
    protected void gvw_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (pageControl.ActiveTabIndex == 0)
        {
            SetRowColumnINGrid(sender, e);
        }

    }

    protected void gvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        string UID = CommonFunction.ReplaceInjection(Session["UserID"] + "");

        switch (e.CallbackName)
        {
            //เมื่อ gvw callback
            case "CUSTOMCALLBACK":

                string[] param = e.Args[0].Split(';');
                int Inx = 0;
                if (param.Length > 1)
                {
                    Inx = int.Parse(param[1] + "");
                }
                ASPxTextBox txtLoop = gvw.FindRowCellTemplateControl(Inx, null, "txtLoop") as ASPxTextBox;
                ASPxComboBox gvwcboCarregis = gvw.FindRowCellTemplateControl(Inx, null, "gvwcboCarregis") as ASPxComboBox;

                string Round = !string.IsNullOrEmpty(txtLoop.Text) ? CommonFunction.ReplaceInjection(txtLoop.Text.Trim()) : "null";
                string STRUCKID = !string.IsNullOrEmpty(gvwcboCarregis.Value + "") ? CommonFunction.ReplaceInjection(gvwcboCarregis.Value + "") : "null";
                switch (param[0])
                {
                    case "save":
                        //เช็คว่าถ้าไม่ใส่ DROP
                        if (!string.IsNullOrEmpty(txtLoop.Text))
                        {
                            dynamic data = gvw.GetRowValues(Inx, "SDELIVERYNO", "NVALUE", "NWINDOWTIMEID", "DDELIVERY", "SVENDORID", "STERMINALID", "SCONTRACTID", "SCONTRACTNO", "SHIP_TO", "ORDERTYPE", "SWINDOWTIMENAME");
                            string SDELIVERYNO = data[0] + "";
                            string QUERY = "";
                            string Chk = "SELECT SDELIVERYNO FROM TPLANSCHEDULELIST WHERE SDELIVERYNO = '" + CommonFunction.ReplaceInjection(SDELIVERYNO) + "'";

                            decimal CAPACITY = !string.IsNullOrEmpty(data[1] + "") ? decimal.Parse(data[1] + "") : 0;
                            string STIMEWINDOW = !string.IsNullOrEmpty(data[2] + "") ? data[2] + "" : "null";
                            DateTime DDELIVERY = !string.IsNullOrEmpty(data[3] + "") ? DateTime.Parse(data[3] + "") : 0;
                            string SVENDORID = data[4] + "";
                            string STERMINALID = data[5] + "";
                            string SCONTRACTID = data[6] + "";
                            string sDate = DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                            string SCONTRACTNO = data[7] + "";
                            string SHIP_TO = data[8] + "";
                            string ORDERTYPE = data[9] + "";
                            string STIMEWINDOWNAME = data[10] + "";
                            string Error = CheckTruckFull(STRUCKID, CAPACITY, STIMEWINDOW, DDELIVERY, Round, SVENDORID, STIMEWINDOWNAME, gvwcboCarregis.Text, STERMINALID);
                            if (string.IsNullOrEmpty(Error))
                            {

                                string ROUND = !string.IsNullOrEmpty(txtLoop.Text) ? txtLoop.Text : "null";
                                //เช็คว่ามี Deliver นี้ใน TPLANSCHEDULELIST หรือยังถ้ายังไม่มีให้แอด ถ้ามีแล้วให้อัพเดท
                                DataTable dt = CommonFunction.Get_Data(conn, Chk);
                                OracleConnection con = new OracleConnection(conn);
                                try
                                {
                                    #region Open Connection
                                    if (con.State == ConnectionState.Closed)
                                    {
                                        con.Open();
                                    }
                                    tran = con.BeginTransaction();

                                    #region Data
                                    if (dt.Rows.Count > 0)
                                    {
                                        UPDATE_TO_TPLANSCHEDULE(STRUCKID, SDELIVERYNO, ref con);
                                        UPDATE_TO_TPLANSCHEDULELIST(ROUND, SDELIVERYNO, ref con);
                                    }
                                    else
                                    {
                                        //สร้างไอดี TPLANSCHEDULE
                                        string genid = CommonFunction.Gen_ID(conn, "SELECT nPlanID FROM (SELECT nPlanID FROM TPLANSCHEDULE ORDER BY nPlanID DESC) WHERE ROWNUM <= 1");
                                        //สร้างไอดี TPLANSCHEDULELIST
                                        //string genidLIST = CommonFunction.Gen_ID(conn, "SELECT SPLANLISTID FROM (SELECT SPLANLISTID FROM TPLANSCHEDULELIST ORDER BY SPLANLISTID DESC) WHERE ROWNUM <= 1");
                                        string genidLIST = "0";
                                        //เช็คจาก แผนว่า เคยมีไหมโดยหาจาก วันที่ส่ง(DDELIVERY) เที่ยวการขนส่ง(STIMEWINDOW) รถ(STRUCKID)
                                        string QUERY_MULTIDROP = "SELECT * FROM TPLANSCHEDULE WHERE DDELIVERY = TO_DATE('" + DDELIVERY + "','dd/MM/yyyy') AND STIMEWINDOW = '" + STIMEWINDOW + "' AND  STRUCKID = '" + STRUCKID + "'";
                                        DataTable dt_ChkMultiDrop = CommonFunction.Get_Data(conn, QUERY_MULTIDROP);
                                        if (dt_ChkMultiDrop.Rows.Count > 0)
                                        {
                                            string NPLANID = dt_ChkMultiDrop.Rows[0]["NPLANID"] + "";
                                            string NDROP = (dt_ChkMultiDrop.Rows.Count + 1) + "";
                                            INSERT_TO_TPLANSCHEDULELIST(genidLIST, genid, NDROP, SDELIVERYNO, CAPACITY + "", SHIP_TO, ORDERTYPE, ROUND, ref con);
                                        }
                                        else
                                        {
                                            genid = INSERT_TO_TPLANSCHEDULE(genid, SVENDORID, STERMINALID, SCONTRACTID, sDate, STIMEWINDOW, SCONTRACTNO, STRUCKID, ref con);
                                            INSERT_TO_TPLANSCHEDULELIST(genidLIST, genid, "1", SDELIVERYNO, CAPACITY + "", SHIP_TO, ORDERTYPE, ROUND, ref con);
                                        }
                                    }
                                    #endregion

                                    tran.Commit();

                                    #endregion
                                }
                                catch (Exception ex)
                                {
                                    if (tran != null)
                                    {
                                        tran.Rollback();
                                    }

                                }
                                finally
                                {
                                    con.Close();
                                }
                                ListData();
                                CommonFunction.SetPopupOnLoad(gvw, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Complete + "')");
                            }
                            else
                            {
                                ListData();

                                CommonFunction.SetPopupOnLoad(gvw, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "','" + Error + "')");

                            }

                        }
                        else
                        {
                            CommonFunction.SetPopupOnLoad(gvw, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ระบุเที่ยวการขนส่ง')");
                        }

                        break;
                }
                break;

            case "PAGERONCLICK": ListData();
                break;
            case "SORT": ListData();
                break;
        }

    }

    protected void xcpnOutBound_Callback(object sender, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
    {
        ASPxGridView gv = gvw;
        ASPxLabel plan = lblPlan, planall = lblPlanAll;
        if (pageControl.ActiveTabIndex == 1)
        {
            plan = lblPlan2;
            planall = lblPlanAll2;
            gv = gvw2;
        }
        string paras = CommonFunction.ReplaceInjection(e.Parameter);
        int Inx = int.Parse(paras);
        ASPxCallback call = gv.FindRowCellTemplateControl(Inx, null, "xcpnOutBound") as ASPxCallback;
        string LOG_DO = "";
        Session["LOG_PROCESS"] = "";
        Session["LOG_DATA"] = "";
        try
        {
            ASPxTextBox txtLoop = gv.FindRowCellTemplateControl(Inx, null, "txtLoop") as ASPxTextBox;
            ASPxComboBox gvwcboCarregis = gv.FindRowCellTemplateControl(Inx, null, "gvwcboCarregis") as ASPxComboBox;

            string Round = !string.IsNullOrEmpty(txtLoop.Text) ? CommonFunction.ReplaceInjection(txtLoop.Text.Trim()) : "null";
            string STRUCKID = !string.IsNullOrEmpty(gvwcboCarregis.Value + "") ? CommonFunction.ReplaceInjection(gvwcboCarregis.Value + "") : "null";

            if (!string.IsNullOrEmpty(txtLoop.Text))
            {
                Session["REPLAN"] = "";
                dynamic data = gv.GetRowValues(Inx, "SDELIVERYNO", "NVALUE", "NWINDOWTIMEID", "DDELIVERY", "SVENDORID", "STERMINALID", "SCONTRACTID", "SCONTRACTNO", "SHIP_TO", "ORDERTYPE", "SWINDOWTIMENAME", "LATEDATE", "LATETIME", "FULLNAME", "SEMAIL", "ORDERID");
                string SDELIVERYNO = data[0] + "";

                LOG_DO = data[0] + "";
                string STIMEWINDOW = !string.IsNullOrEmpty(data[2] + "") ? data[2] + "" : "null";
                string STERMINALID = data[5] + "";
                bool isIteminal = true;
                if (STERMINALID == "H503" || STERMINALID == "K148")
                {
                    //กรณ๊เป็นคลัง ภูเก็ต ไม่ต้องจัดแผนที่ Iteminal  fix ไว้ก่อน 
                    isIteminal = false;
                }
                string TRUCK_TO_DATE = "";

                if (!string.IsNullOrEmpty(data[3] + ""))
                {

                    DateTime dte = DateTime.Parse(data[3] + "");//เปลี่ยนมาใช้วัน Assign เพราะว่าถ้าเป็นงานขนส่งภายในวันไม่ต้องสนใจ DeliveryDate ให้สนวัน Assign แทน
                    if (dte != null)
                    {
                        TRUCK_TO_DATE = CheckDate(dte.AddDays(-1).ToString(), "V");

                    }

                }

                DataTable dtCarRegis = Session["dtCarRegis"] as DataTable;

                //เช็คว่ามีรถในสัญญาของวันนั้นไหม
                if (dtCarRegis.Select("SHEADID = '" + STRUCKID + "' AND DDATE = '" + TRUCK_TO_DATE + "'").Count() > 0 && !string.IsNullOrEmpty(gvwcboCarregis.Text))
                {

                    Session["LOG_PROCESS"] += "C1;";

                    decimal CAPACITY = !string.IsNullOrEmpty(data[1] + "") ? decimal.Parse(data[1] + "") : 0;

                    //DateTime DDELIVERY = !string.IsNullOrEmpty(data[3] + "") ? DateTime.Parse(data[3] + "") : 0;
                    DateTime? dte = DateTime.Parse(data[3] + "");//เปลี่ยนมาใช้วัน Assign เพราะว่าถ้าเป็นงานขนส่งภายในวันไม่ต้องสนใจ DeliveryDate ให้สนวัน Assign แทน
                    DateTime DDELIVERY = new DateTime();
                    if (dte != null)
                    {
                        DDELIVERY = dte.Value;
                    }//ที่ไม่ใช้ DDELIVERY จาก Excel เพราะว่าถ้าในกรณีที่วันที่ขนส่งเกินวันปัจจุบันจะทำให้การทำงานผิดเนื่องจากระบบจะไปมองว่าต้องขนส่งวันที่ อื่น
                    string SVENDORID = data[4] + "";

                    string SCONTRACTID = data[6] + "";
                    string sDate = DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                    string SCONTRACTNO = data[7] + "";
                    string SHIP_TO = data[8] + "";
                    string ORDERTYPE = data[9] + "";
                    string STIMEWINDOWNAME = data[10] + "";

                    Session["LOG_DATA"] = STIMEWINDOW + ";" + STERMINALID + ";" + TRUCK_TO_DATE + ";" + CAPACITY + ";" + SVENDORID + ";" + sDate + ";" + SCONTRACTNO + ";" + SHIP_TO + ";" + ORDERTYPE + ";" + STIMEWINDOWNAME;

                    //string Chk = "SELECT SDELIVERYNO FROM TPLANSCHEDULELIST WHERE SDELIVERYNO = '" + CommonFunction.ReplaceInjection(SDELIVERYNO) + "' AND CACTIVE = '1'";
                    string Chk = @"SELECT TPL.SDELIVERYNO,TPC.NPLANID FROM TPLANSCHEDULELIST TPL
LEFT JOIN TPLANSCHEDULE   TPC
ON TPC.NPLANID = TPL.NPLANID
WHERE TPL.SDELIVERYNO = '" + CommonFunction.ReplaceInjection(SDELIVERYNO) + "' AND TPL.CACTIVE = '1' AND TPC.CACTIVE = '1' AND TPC.STRUCKID = '" + STRUCKID + "'";
                    string genid = string.Empty;
                    string Error = CheckTruckFull(STRUCKID, CAPACITY, STIMEWINDOW, DDELIVERY, Round, SVENDORID, STIMEWINDOWNAME, gvwcboCarregis.Text, STERMINALID);
                    if (string.IsNullOrEmpty(Error))
                    {
                        Session["LOG_PROCESS"] += "C2;";

                        string sNew_Drop = "";
                        string ROUND = !string.IsNullOrEmpty(txtLoop.Text) ? txtLoop.Text : "null";
                        //เช็คว่ามี Deliver นี้ใน TPLANSCHEDULELIST หรือยังถ้ายังไม่มีให้แอด ถ้ามีแล้วให้อัพเดท
                        DataTable dt = CommonFunction.Get_Data(conn, Chk);
                        #region Open Connection
                        OracleConnection con = new OracleConnection(conn);

                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }
                        tran = con.BeginTransaction();

                        #region Data
                        if (dt.Rows.Count > 0)
                        {
                            Session["LOG_PROCESS"] += "C3;";

                            UPDATE_TO_TPLANSCHEDULE(STRUCKID, SDELIVERYNO, ref con);
                            UPDATE_TO_TPLANSCHEDULELIST(ROUND, SDELIVERYNO, ref con);

                        }
                        else
                        {

                            Session["LOG_PROCESS"] += "C7;";
                            //ลบ DO เพราะในกรณีที่เกิดเป็นมันติดรอปขึ้นจะต้องแก้ไขข้อมูลแผนเก่าด้วย เกิดกรณีไปลบ DROP 1
                            Del_Plan(SDELIVERYNO, STIMEWINDOW, STRUCKID, STERMINALID, ref con, isIteminal);
                            if ((Session["REPLAN"] + string.Empty).Split(';')[0] == "S")
                            {
                                countPlan -= 1;
                            }
                            //สร้างไอดี TPLANSCHEDULE
                            genid = CommonFunction.Gen_ID(conn, "SELECT nPlanID FROM (SELECT nPlanID FROM TPLANSCHEDULE ORDER BY nPlanID DESC) WHERE ROWNUM <= 1");
                            //สร้างไอดี TPLANSCHEDULELIST
                            //string genidLIST = CommonFunction.Gen_ID(conn, "SELECT SPLANLISTID FROM (SELECT SPLANLISTID FROM TPLANSCHEDULELIST ORDER BY SPLANLISTID DESC) WHERE ROWNUM <= 1");
                            string genidLIST = "0";//ใช้ Auto Runing ให้database แทน ทำให้ID ไม่ซ้ำกัน
                            //INSERT_TO_TPLANSCHEDULE(genid, SVENDORID, STERMINALID, SCONTRACTID, sDate, STIMEWINDOW, SCONTRACTNO, STRUCKID);
                            //INSERT_TO_TPLANSCHEDULELIST(genidLIST, genid, "1", SDELIVERYNO, CAPACITY + "", SHIP_TO, ORDERTYPE, ROUND);

                            //เช็คจาก แผนว่า เคยมีไหมโดยหาจาก วันที่ส่ง(DDELIVERY) เที่ยวการขนส่ง(STIMEWINDOW) รถ(STRUCKID)
                            string QUERY_MULTIDROP = "SELECT * FROM TPLANSCHEDULE WHERE DDELIVERY = TO_DATE('" + sDate + "','dd/MM/yyyy') AND STIMEWINDOW = '" + STIMEWINDOW + "' AND  STRUCKID = '" + STRUCKID + "' AND CACTIVE = '1' AND STERMINALID = '" + STERMINALID + "'";
                            DataTable dt_ChkMultiDrop = CommonFunction.Get_Data(conn, QUERY_MULTIDROP);
                            if (dt_ChkMultiDrop.Rows.Count > 0)
                            {
                                Session["LOG_PROCESS"] += "C12;";
                                string NPLANID = dt_ChkMultiDrop.Rows[0]["NPLANID"] + "";
                                genid = NPLANID;
                                DataTable dt_NDROP = CommonFunction.Get_Data(conn, "SELECT NPLANID,NDROP FROM TPLANSCHEDULELIST WHERE NPLANID = '" + NPLANID + "' AND CACTIVE = '1' ORDER BY NDROP DESC");
                                if (dt_NDROP.Rows.Count > 0)
                                {
                                    string NDROP = (int.Parse(dt_NDROP.Rows[0]["NDROP"] + "") + 1) + "";
                                    INSERT_TO_TPLANSCHEDULELIST(genidLIST, NPLANID, NDROP, SDELIVERYNO, CAPACITY + "", SHIP_TO, ORDERTYPE, ROUND, ref con);
                                    sNew_Drop = "O";
                                }
                                else
                                {
                                    string NDROP = "1";
                                    INSERT_TO_TPLANSCHEDULELIST(genidLIST, NPLANID, NDROP, SDELIVERYNO, CAPACITY + "", SHIP_TO, ORDERTYPE, ROUND, ref con);
                                    sNew_Drop = "O";
                                }
                            }
                            else
                            {
                                Session["LOG_PROCESS"] += "C13;";
                                genid = INSERT_TO_TPLANSCHEDULE(genid, SVENDORID, STERMINALID, SCONTRACTID, sDate, STIMEWINDOW, SCONTRACTNO, STRUCKID, ref con);
                                INSERT_TO_TPLANSCHEDULELIST(genidLIST, genid, "1", SDELIVERYNO, CAPACITY + "", SHIP_TO, ORDERTYPE, ROUND, ref con);
                                sNew_Drop = "N";
                            }

                        }

                        string Veh = "SELECT STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO FROM TTRUCK WHERE STRUCKID = '" + STRUCKID + "'";
                        DataTable dt_Veh = CommonFunction.Get_Data(conn, Veh);
                        string VehNo = "";
                        string TuNo = "";
                        if (dt_Veh.Rows.Count > 0)
                        {
                            VehNo = dt_Veh.Rows[0]["SHEADREGISTERNO"] + "";
                            TuNo = dt_Veh.Rows[0]["STRAILERREGISTERNO"] + "";
                        }

                        string CHK = "SELECT NPLANID,SDELIVERYNO FROM TPLANSCHEDULELIST WHERE SDELIVERYNO = '" + SDELIVERYNO + "' AND CACTIVE = '1'";
                        DataTable dt_PLAN = CommonFunction.Get_Data(conn, CHK);
                        string PLANID = genid;
                        if (dt_PLAN.Rows.Count > 0)
                        {
                            Session["LOG_PROCESS"] += "C14;";
                            if (string.IsNullOrEmpty(genid))
                            {
                                PLANID = dt_PLAN.Rows[0]["NPLANID"] + "";
                            }
                            //PLANID = dt_PLAN.Rows[0]["NPLANID"] + "";
                        }
                        else
                        {
                            Session["LOG_PROCESS"] += "C15;";
                            //e.Result = "ไม่พบแผนจากเลข DO นี้";
                            //return;
                        }

                        LogUser(SMENUID, "I", "บันทึกข้อมูลจัดแผนภายในวัน", SDELIVERYNO);

                        string ChkTeminal = "";
                        if (isIteminal)
                        {
                            if (sNew_Drop == "N")
                            {
                                Session["LOG_PROCESS"] += "C16;";

                                ChkTeminal = ITEMINAL(SDELIVERYNO, STIMEWINDOW, VehNo, TuNo, STERMINALID, PLANID, DateTime.Now.ToString(), ref con);


                            }
                            else
                            {
                                Session["LOG_PROCESS"] += "C17;";

                                //string DO_ITEAMINAL = GenDOByTruck(PLANID, SDELIVERYNO);
                                //ChkTeminal = ITEMINAL_DELETE(DO_ITEAMINAL, STIMEWINDOW, VehNo, TuNo, STERMINALID, PLANID, DateTime.Now.ToString(), ref con);
                                //ChkTeminal = ITEMINAL(DO_ITEAMINAL + "," + SDELIVERYNO, STIMEWINDOW, VehNo, TuNo, STERMINALID, PLANID, DateTime.Now.ToString(), ref con);
                                ChkTeminal = ITEMINAL(SDELIVERYNO, STIMEWINDOW, VehNo, TuNo, STERMINALID, PLANID, DateTime.Now.ToString(), ref con);
                            }
                        }


                        //เช็คว่าอัพเดทใน Iteminal ผ่านไหมถ้าผ่านให้ทำตามระบบ TMS
                        string ResultTerminal = !string.IsNullOrEmpty(ChkTeminal) ? ChkTeminal.Substring(0, 1) : "";
                        if (ResultTerminal != "E")
                        {
                            Session["LOG_PROCESS"] += "C18;";
                            call.JSProperties["cpssss"] = "S";

                            e.Result = "S";
                            countPlan += 1;
                            tran.Commit();

                            #region IVMS
                            if (!string.IsNullOrEmpty("" + gv.GetRowValues(Inx, "DATETIMEEXPECT")))
                            {
                                string SHEADREGISTERNO = string.Empty;

                                ASPxTextBox txtLateTextBox = gv.FindRowCellTemplateControl(Inx, null, "txtLateTextBox") as ASPxTextBox;
                                if (!string.IsNullOrEmpty(gvwcboCarregis.Text))
                                {
                                    string[] str = gvwcboCarregis.Text.Split(' ');
                                    if (str.Any())
                                    {
                                        SHEADREGISTERNO = str[0];
                                    }
                                }
                                if (!string.IsNullOrEmpty(data[11] + string.Empty))
                                {

                                    DateTime Latetime = Convert.ToDateTime(data[11] + string.Empty);
                                    if (!string.IsNullOrEmpty(data[12] + string.Empty))
                                    {
                                        string[] str = (data[12] + string.Empty).Split('.');
                                        if (str.Any())
                                        {
                                            Latetime = Latetime.AddHours(int.Parse(str[0]));
                                            Latetime = Latetime.AddMinutes(int.Parse(str[1]));
                                        }
                                    }
                                    string mess = PlanTransportBLL.Instance.GetLateFormIVMS(connIVMS, data[0] + string.Empty, data[5] + string.Empty, (data[8] + string.Empty).Replace("000009", ""), SHEADREGISTERNO, Latetime.ToString("dd/MM/yyyy HH:mm"), data[13] + string.Empty, data[14] + string.Empty, data[15] + string.Empty, txtLateTextBox.Text.Trim());

                                }
                            }

                            #endregion
                        }
                        else
                        {
                            Session["LOG_PROCESS"] += "C19;";
                            call.JSProperties["cpssss"] = "SI";
                            e.Result = "ITERMINAL : " + ChkTeminal.Replace("E;", "");
                            if ((Session["REPLAN"] + string.Empty).Split(';')[0] == "S")
                            {
                                countPlan += 1;
                            }
                            tran.Rollback();
                        }
                        #endregion


                        con.Close();



                        #endregion


                    }
                    else
                    {
                        Session["LOG_PROCESS"] += "C20;";

                        e.Result = "TMS : " + Error;
                    }
                }
                else
                {
                    Session["LOG_PROCESS"] += "C21;";

                    Session["REPLAN"] = "";
                    #region Open Connection
                    OracleConnection con = new OracleConnection(conn);

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    tran = con.BeginTransaction();

                    #region Data
                    Del_Plan(SDELIVERYNO, STIMEWINDOW, STRUCKID, STERMINALID, ref con, isIteminal);

                    #endregion

                    tran.Commit();
                    con.Close();

                    if ((Session["REPLAN"] + string.Empty).Split(';')[0] == "S")
                    {
                        #region IVMS
                        string SHEADREGISTERNO = string.Empty;

                        ASPxTextBox txtLateTextBox = gv.FindRowCellTemplateControl(Inx, null, "txtLateTextBox") as ASPxTextBox;
                        if (!string.IsNullOrEmpty(gvwcboCarregis.Text))
                        {
                            string[] str = gvwcboCarregis.Text.Split(' ');
                            if (str.Any())
                            {
                                SHEADREGISTERNO = str[0];
                            }
                        }
                        if (!string.IsNullOrEmpty(data[11] + string.Empty))
                        {

                            DateTime Latetime = Convert.ToDateTime(data[11] + string.Empty);
                            if (!string.IsNullOrEmpty(data[12] + string.Empty))
                            {
                                string[] str = (data[12] + string.Empty).Split('.');
                                if (str.Any())
                                {
                                    Latetime = Latetime.AddHours(int.Parse(str[0]));
                                    Latetime = Latetime.AddMinutes(int.Parse(str[1]));
                                }
                            }
                            //ส่ง ทะเบียนรถเป็นค่าว่างไปกรณียกเลิกหรือลบแผน
                            string mess = PlanTransportBLL.Instance.GetLateFormIVMS(connIVMS, data[0] + string.Empty, data[5] + string.Empty, (data[8] + string.Empty).Replace("000009", ""), "", Latetime.ToString("dd/MM/yyyy HH:mm"), data[13] + string.Empty, data[14] + string.Empty, data[15] + string.Empty, string.Empty);

                        }
                        #endregion
                    }
                    #endregion


                    string[] RESULT = (Session["REPLAN"] + "").Split(',');
                    if (RESULT.Length > 0)
                    {
                        string POPUP = "";
                        string Detail = "";
                        for (int i = 0; i < RESULT.Length; i++)
                        {
                            if (i == 0)
                            {
                                if (!string.IsNullOrEmpty(RESULT[i] + ""))
                                {
                                    POPUP += (RESULT[i] + "").Length > 0 ? (RESULT[i] + "").Substring(0, 1) : "";
                                    if ((RESULT[i] + "").Substring(0, 1) == "E")
                                    {
                                        Detail += RESULT[i].Replace("E;", "") + "<br>";
                                    }
                                }
                                else
                                {
                                    Detail = "ไม่พบแผนงานที่ต้องการยกเลิก";
                                }
                            }
                            else if (i == 1)
                            {
                                POPUP += (RESULT[i] + "").Length > 0 ? (RESULT[i] + "").Substring(0, 1) : "";
                                if (!string.IsNullOrEmpty(RESULT[i] + ""))
                                {
                                    if ((RESULT[i] + "").Substring(0, 1) == "E")
                                    {
                                        Detail += RESULT[i].Replace("E;", "") + "";
                                    }
                                }
                            }
                        }

                        if (POPUP == "SS" || POPUP == "S")
                        {
                            e.Result = "C";
                            countPlan -= 1;
                        }
                        else
                        {
                            if (Detail == "ไม่พบแผนงานที่ต้องการยกเลิก")
                            {
                                e.Result = "TMS : " + Detail;
                            }
                            else
                            {
                                e.Result = "ITERMINAL : " + Detail;
                            }
                        }
                    }
                }
            }
            else
            {
                e.Result = "TMS : ระบุเที่ยวการขนส่ง";
                if (string.IsNullOrEmpty(gvwcboCarregis.Value + string.Empty))
                {
                    e.Result += "และทะเบียนรถ";
                }
            }

            //STRUCKID
            SetPlanCount(gv, planall, plan, false);
            e.Result += ";;" + plan.Text;
        }
        catch (Exception ex)
        {
            if (tran != null)
            {
                tran.Rollback();
            }
            call.JSProperties["cpssss"] = "SI";
            e.Result = "ITERMINAL : " + ex.Message;
            Session["LOG_PROCESS"] += "ITERMINAL : " + ex.Message;
        }

        #region Open Connection
        OracleConnection con2 = new OracleConnection(conn);

        if (con2.State == ConnectionState.Closed)
        {
            con2.Open();
        }
        tran = con2.BeginTransaction();

        #region Data
        string QLOG = "INSERT INTO LOG_PLAN (DO,LDATE,LOGPROCESS,LOGDATA) VALUES ('" + LOG_DO + "',SYSDATE,'" + Session["LOG_PROCESS"] + "','" + Session["LOG_DATA"] + "')";
        AddTODB(QLOG, ref con2);
        #endregion

        tran.Commit();
        con2.Close();
        #endregion

    }

    private void ListData()
    {
        string STARTDATE = "";
        string ENDDATE = "";
        string AssignDate = string.Empty;
        if (!string.IsNullOrEmpty(edtAssign.Text))
        {
            DateTime? dte = ConvertToDateNull(edtAssign.Text);//เปลี่ยนมาใช้วัน Assign เพราะว่าถ้าเป็นงานขนส่งภายในวันไม่ต้องสนใจ DeliveryDate ให้สนวัน Assign แทน
            DateTime datestart = new DateTime();
            if (dte != null)
            {
                datestart = dte.Value;
            }

            STARTDATE = datestart.AddDays(-3).ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            ENDDATE = datestart.AddDays(3).ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            AssignDate = datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
        }

        #region แบบเดิม

        //if (!string.IsNullOrEmpty(cboSelect.Text))
        //{
        //    Condition += " AND ODP.STERMINALID = '" + CommonFunction.ReplaceInjection(cboSelect.Value + "") + "'";
        //}

        //if (!string.IsNullOrEmpty(cboSCONTRACT.Text))
        //{
        //    Condition += " AND ODP.SCONTRACTID = '" + CommonFunction.ReplaceInjection(cboSCONTRACT.Value + "") + "'";
        //}

        //if (!string.IsNullOrEmpty(cboCarregis.Value + string.Empty))
        //{

        //    Condition += " AND TPC.STRUCKID = '" + cboCarregis.Value + "'";
        //}

        //if (!string.IsNullOrEmpty(txtDNO.Text))
        //{
        //    Condition += " AND ODP.SDELIVERYNO LIKE '%" + CommonFunction.ReplaceInjection(txtDNO.Text) + "%'";
        //}
        //string VENOR_ID = Session["SVDID"] + string.Empty;

        //        string QUERY = @"SELECT ODP.ORDERID, ODP.SVENDORID, ODP.SABBREVIATION, ODP.SCONTRACTID, ODP.SCONTRACTNO, 
        //CASE WHEN ODP.ORDERTYPE = '1' THEN 'งานขนส่งภายในวัน' ELSE 'งานขนส่งล่วงหน้า' END as TRAN_TYPE,
        //ODP.DDELIVERY, ODP.NWINDOWTIMEID, ODP.SDELIVERYNO,ODP.DATE_CREATE, ODP.ORDERTYPE,
        //ODP.SCREATE,ODP.DATE_UPDATE,ODP.SUPDATE, SHM.OTHER as NVALUE, ODP.STERMINALID,SHM.CUST_ABBR as STERMINALNAME 
        //,CASE 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = NVL(TO_CHAR(ODP.DDELIVERY),'xxx') OR TO_CHAR(ODP.DDELIVERY,'DAY') IN('เสาร์','อาทิตย์') THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> NVL(TO_CHAR(ODP.DDELIVERY),'xxx') THEN '0'
        //ELSE '' END AS CDAYTYPE -- เช็คว่า ถ้าไม่มีวันตรงกะวันหยุด และ CDAYTYPE = 0  ให้ค่าเป็น 1 และถ้า เป็นวันหยุด และ  CDAYTYPE = 1 ให้ค่าเป็น 1
        //,'เที่ยวที่ '||TWT.NLINE||' เวลา '||TWT.TSTART||'-'||TWT.TEND||' น.' as SWINDOWTIMENAME,TPC.ROUND,TPC.STRUCKID,TPC.DPLAN,
        //CASE WHEN NVL(TPC.STRUCKID,'xxx') <> 'xxx' THEN 1 ELSE 0 END as PLANADD
        //,CASE WHEN TPC.CACTIVE = '1' THEN 'จัดแผน' WHEN TPC.CACTIVE = '0' THEN 'ยกเลิกแผน' WHEN NVL(SHM.SHIP_TO,'xxx') <> 'xxx' AND NVL(TPC.STRUCKID,'xxx') = 'xxx' THEN '' ELSE '' END as STATUS
        //,CASE WHEN NVL(SHM.SHIP_TO,'xxx') <> 'xxx' THEN '0' ELSE  TPC.CACTIVE END as CANCEL--, NVL(SHM.SHIP_TO,'xxx')
        //FROM TBL_ORDERPLAN ODP
        //LEFT JOIN 
        //(
        //    SELECT DISTINCT T.SHIP_TO,T.DELIVERY_NO,C.CUST_ABBR,T.OTHER FROM TDELIVERY T 
        //    LEFT JOIN TCUSTOMER C ON  C.SHIP_TO = CASE WHEN LENGTH(T.SHIP_TO) < 5 THEN '000009'||T.SHIP_TO  ELSE T.SHIP_TO END
        //    WHERE T.DELIVERY_NO IN (SELECT SDELIVERYNO FROM TBL_ORDERPLAN WHERE 1=1 " + ConditionD + @")  " + DateShipment + @"
        //)SHM
        //ON SHM.DELIVERY_NO = ODP.SDELIVERYNO
        //LEFT JOIN LSTHOLIDAY LHD
        //ON TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy')  =  TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy')  
        //LEFT JOIN TWINDOWTIME TWT
        //ON TWT.STERMINALID =  ODP.STERMINALID AND TWT.NLINE = ODP.NWINDOWTIMEID AND TWT.CDAYTYPE = CASE 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = NVL(TO_CHAR(ODP.DDELIVERY),'xxx') OR TO_CHAR(ODP.DDELIVERY,'DAY') IN('เสาร์','อาทิตย์') THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> NVL(TO_CHAR(ODP.DDELIVERY),'xxx') THEN '0' ELSE '' END
        //LEFT JOIN 
        //(
        //    SELECT TPS.NPLANID,TPS.DPLAN,TPS.STRUCKID,TPL.SDELIVERYNO,TPL.ROUND,TPL.NVALUE,TPL.CACTIVE,TPS.SVENDORID  FROM TPLANSCHEDULE TPS
        //    LEFT JOIN TPLANSCHEDULELIST TPL
        //    ON TPS.NPLANID = TPL.NPLANID
        //    WHERE TPL.CACTIVE = '1' AND NVL(TPS.CFIFO,'XXX') <> '1'
        //)TPC
        //ON ODP.SDELIVERYNO = TPC.SDELIVERYNO AND  ODP.SVENDORID =  TPC.SVENDORID
        //WHERE 1=1 AND 
        //CASE 
        //--กรณีรถที่ไม่ได้จัดจะแสดงด้านบนโดยไม่สนไทป์ แต่วันต้องน้อยกว่าวันปัจจุบัน
        //WHEN ODP.CACTIVE = 'Y'  AND TRUNC(ODP.DATE_CREATE) <  " + ConditionCAUSE + @"   AND  NVL(TPC.STRUCKID,'xxx') = 'xxx' THEN '1' 
        //--กรณีวันปัจจุบันทั้งที่จัดแผนแล้วและยังไม่ได้จัดและไทป์เป็น 1 เสมอ
        //WHEN TRUNC(ODP.DATE_CREATE) =  " + ConditionCAUSE + @"  AND ODP.ORDERTYPE = '1' AND ODP.CACTIVE = 'Y'  THEN '1' 
        //--กรณีวันปัจจุบันย้อนหลัง 1 วัน โดยไทป์จะต้องเป็น 2 มาแสดง
        //WHEN ODP.ORDERTYPE = '2' AND ODP.CACTIVE = 'Y'  AND TRUNC(ODP.DATE_CREATE+1) =  " + ConditionCAUSE + @"   THEN '1' 
        //--กรณีเป็นวันที่ต้องขนส่งแต่ยังไม่ได้จัด
        //WHEN TRUNC(ODP.DDELIVERY) =  " + ConditionCAUSE + @" AND  NVL(TPC.STRUCKID,'xxx') = 'xxx'  AND ODP.CACTIVE = 'Y'  THEN '1' 
        //ELSE '' END = '1'
        //--CASE WHEN ODP.ORDERTYPE = '1' AND ODP.CACTIVE = 'Y'  AND TRUNC(ODP.DATE_CREATE) <= " + ConditionCAUSE + @"  THEN '1' 
        //--WHEN TRUNC(ODP.DDELIVERY) <= " + ConditionCAUSE + @"    AND  NVL(TPC.STRUCKID,'xxx') = 'xxx'  AND ODP.CACTIVE = 'Y'  THEN '1' 
        //--WHEN TRUNC(ODP.DATE_CREATE) = " + ConditionCAUSE + @"    AND  TRUNC(ODP.DDELIVERY) = " + ConditionCAUSE + @"   AND ODP.CACTIVE = 'Y' THEN '1' 
        //--WHEN ODP.ORDERTYPE = '2' AND ODP.CACTIVE = 'Y'  AND TRUNC(ODP.DATE_CREATE+1) <=  " + ConditionCAUSE + @"   THEN '1' 
        //--ELSE '' END = '1'
        //AND  ODP.SVENDORID = '" + VENOR_ID + @"'
        //" + Condition + @"
        //ORDER BY PLANADD ASC, ODP.DDELIVERY ASC, ODP.DATE_CREATE ASC,ODP.NWINDOWTIMEID ASC,TPC.STRUCKID ASC,TPC.ROUND ASC";
        #endregion
        //ตัวจริง
        //var watch = System.Diagnostics.Stopwatch.StartNew();
        //watch.Start();
        gvw.DataSource = PlanTransportBLL.Instance.GetOrderByAssign(Session["SVDID"] + string.Empty, STARTDATE, ENDDATE, AssignDate, cboSelect.Value + string.Empty, cboSCONTRACT.Value + "", cboCarregis.Value + "", txtDNO.Text);
        //watch.Stop();
        gvw.DataBind();
        SetPlanCount(gvw, lblPlanAll, lblPlan, true);
    }
    #endregion

    #region gvw2
    protected void gvw2_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (pageControl.ActiveTabIndex == 1)
        {
            SetRowColumnINGrid(sender, e);
        }
    }
    protected void gvw2_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        string UID = CommonFunction.ReplaceInjection(Session["UserID"] + "");

        switch (e.CallbackName)
        {
            //เมื่อ gvw callback
            case "CUSTOMCALLBACK":

                string[] param = e.Args[0].Split(';');
                int Inx = 0;
                if (param.Length > 1)
                {
                    Inx = int.Parse(param[1] + "");
                }
                ASPxTextBox txtLoop = gvw2.FindRowCellTemplateControl(Inx, null, "txtLoop") as ASPxTextBox;
                ASPxComboBox gvwcboCarregis = gvw2.FindRowCellTemplateControl(Inx, null, "gvwcboCarregis2") as ASPxComboBox;

                string Round = !string.IsNullOrEmpty(txtLoop.Text) ? CommonFunction.ReplaceInjection(txtLoop.Text.Trim()) : "null";
                string STRUCKID = !string.IsNullOrEmpty(gvwcboCarregis.Value + "") ? CommonFunction.ReplaceInjection(gvwcboCarregis.Value + "") : "null";
                switch (param[0])
                {
                    case "save":
                        if (!string.IsNullOrEmpty(txtLoop.Text))
                        {
                            dynamic data = gvw2.GetRowValues(Inx, "SDELIVERYNO", "NVALUE", "STIMEWINDOW", "DDELIVERY", "SVENDORID", "SWINDOWTIMENAME");

                            string QUERY = "";
                            string Chk = "SELECT SDELIVERYNO FROM TPLANSCHEDULELIST WHERE SDELIVERYNO = '" + CommonFunction.ReplaceInjection(data[0] + "") + "'";

                            decimal CAPACITY = !string.IsNullOrEmpty(data[1] + "") ? decimal.Parse(data[1] + "") : 0;
                            string STIMEWINDOW = data[2] + "";
                            DateTime DDELIVERY = !string.IsNullOrEmpty(data[3] + "") ? DateTime.Parse(data[3] + "") : 0;
                            string SVENDORID = data[4] + "";
                            string STIMEWINDOWNAME = data[5] + "";
                            string Error = CheckTruckFull(STRUCKID, CAPACITY, STIMEWINDOW, DDELIVERY, Round, SVENDORID, STIMEWINDOWNAME, gvwcboCarregis.Text, "");

                            if (string.IsNullOrEmpty(Error))
                            {
                                DataTable dt = CommonFunction.Get_Data(conn, Chk);

                                OracleConnection con = new OracleConnection(conn);
                                try
                                {
                                    #region Open Connection
                                    if (con.State == ConnectionState.Closed)
                                    {
                                        con.Open();
                                    }
                                    tran = con.BeginTransaction();

                                    #region Data
                                    string SDELIVERYNO = data[0] + "";
                                    UPDATE_TO_TPLANSCHEDULE(STRUCKID, SDELIVERYNO, ref con);

                                    string ROUND = !string.IsNullOrEmpty(txtLoop.Text) ? txtLoop.Text : "null";
                                    UPDATE_TO_TPLANSCHEDULELIST(ROUND, SDELIVERYNO, ref con);
                                    #endregion

                                    tran.Commit();

                                    #endregion
                                }
                                catch (Exception ex)
                                {
                                    if (tran != null)
                                    {
                                        tran.Rollback();
                                    }
                                }
                                finally
                                {
                                    con.Close();
                                }

                                ListData();
                                CommonFunction.SetPopupOnLoad(gvw2, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Complete + "')");
                            }
                            else
                            {
                                ListData();

                                CommonFunction.SetPopupOnLoad(gvw2, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "','" + Error + "')");

                            }
                        }
                        else
                        {
                            CommonFunction.SetPopupOnLoad(gvw2, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ระบุเที่ยวการขนส่ง')");
                        }

                        break;
                }
                break;
            case "PAGERONCLICK": ListData2();
                break;
            case "SORT": ListData2();
                break;
        }
    }

    private void ListData2()
    {
        string VENOR_ID = Session["SVDID"] + string.Empty;
        string Condition = "";
        string ConditionD = "";
        string DateShipment = "";
        string AssignDate = "";
        if (!string.IsNullOrEmpty(edtAssign.Text))
        {
            DateTime? dte = ConvertToDateNull(edtAssign.Text);//เปลี่ยนมาใช้วัน Assign เพราะว่าถ้าเป็นงานขนส่งภายในวันไม่ต้องสนใจ DeliveryDate ให้สนวัน Assign แทน
            DateTime datestart = new DateTime();
            if (dte != null)
            {
                datestart = dte.Value;
            }
            //  DateTime dateend = DateTime.Parse(edtEnd.Value.ToString());
            //ConditionCAUSE = " TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy') ";
            //ConditionD += " AND (TRUNC(DATE_CREATE) = TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')) ";
            //DateShipment = " AND (TRUNC(DATE_CREATED) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.AddDays(-30).ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy') AND  TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')) ";
            AssignDate = datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
        }
        #region แบบเดิม
        //        if (!string.IsNullOrEmpty(cboSelect.Text))
        //        {
        //            Condition += " AND ODP.STERMINALID = '" + CommonFunction.ReplaceInjection(cboSelect.Value + "") + "'";
        //        }

        //        if (!string.IsNullOrEmpty(cboSCONTRACT.Text))
        //        {
        //            Condition += " AND ODP.SCONTRACTID = '" + CommonFunction.ReplaceInjection(cboSCONTRACT.Value + "") + "'";
        //        }

        //        if (!string.IsNullOrEmpty(txtDNO.Text))
        //        {
        //            Condition += " AND ODP.SDELIVERYNO LIKE '%" + CommonFunction.ReplaceInjection(txtDNO.Text) + "%'";
        //        }

        //        if (!string.IsNullOrEmpty(cboCarregis.Text))
        //        {
        //            string QUERY_GET = @"SELECT * FROM TTRUCK T
        //INNER JOIN TCONTRACT_TRUCK TT
        //ON T.STRUCKID = TT.STRUCKID
        //INNER JOIN TCONTRACT TC
        //ON TC.SCONTRACTID = TT.SCONTRACTID
        //WHERE T.STRUCKID||';'||T.SHEADREGISTERNO||';'||T.STRAILERID||';'||T.STRAILERREGISTERNO LIKE '%" + cboCarregis.Value + "%' AND NVL(TC.CACTIVE,'Y') = 'Y' AND T.SCARTYPEID IN ('0','3')";

        //            DataTable dt_T = CommonFunction.Get_Data(conn, QUERY_GET);
        //            string STRUCKID = "";
        //            if (dt_T.Rows.Count > 0)
        //            {
        //                STRUCKID = dt_T.Rows[0]["STRUCKID"] + "";
        //            }
        //            Condition += " AND TPC.STRUCKID = '" + CommonFunction.ReplaceInjection(STRUCKID) + "'";
        //        }
        //        string QUERY = @"SELECT ODP.ORDERID, ODP.SVENDORID, ODP.SABBREVIATION, ODP.SCONTRACTID, ODP.SCONTRACTNO, 
        //CASE WHEN ODP.ORDERTYPE = '1' THEN 'งานขนส่งภายในวัน' ELSE 'งานขนส่งล่วงหน้า' END as TRAN_TYPE,
        //ODP.DDELIVERY, ODP.NWINDOWTIMEID, ODP.SDELIVERYNO,ODP.DATE_CREATE, ODP.ORDERTYPE,
        //ODP.SCREATE,ODP.DATE_UPDATE,ODP.SUPDATE, SHM.OTHER as NVALUE, ODP.STERMINALID,SHM.CUST_ABBR as STERMINALNAME 
        //,CASE 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = NVL(TO_CHAR(ODP.DDELIVERY),'xxx') OR TO_CHAR(ODP.DDELIVERY,'DAY') IN('เสาร์','อาทิตย์') THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> NVL(TO_CHAR(ODP.DDELIVERY),'xxx') THEN '0'
        //ELSE '' END AS CDAYTYPE -- เช็คว่า ถ้าไม่มีวันตรงกะวันหยุด และ CDAYTYPE = 0  ให้ค่าเป็น 1 และถ้า เป็นวันหยุด และ  CDAYTYPE = 1 ให้ค่าเป็น 1
        //,'เที่ยวที่ '||TWT.NLINE||' เวลา '||TWT.TSTART||'-'||TWT.TEND||' น.' as SWINDOWTIMENAME,TPC.ROUND,TPC.STRUCKID,TPC.DPLAN,
        //CASE WHEN NVL(TPC.STRUCKID,'xxx') <> 'xxx' THEN 1 ELSE 0 END as PLANADD
        //,CASE WHEN TPC.CACTIVE = '1' THEN 'จัดแผน' WHEN TPC.CACTIVE = '0' THEN 'ยกเลิกแผน' WHEN NVL(SHM.SHIP_TO,'xxx') <> 'xxx' AND NVL(TPC.STRUCKID,'xxx') = 'xxx' THEN '' ELSE '' END as STATUS
        //,CASE WHEN NVL(SHM.SHIP_TO,'xxx') <> 'xxx' THEN '0' ELSE  TPC.CACTIVE END as CANCEL--, NVL(SHM.SHIP_TO,'xxx')
        //FROM TBL_ORDERPLAN ODP
        //LEFT JOIN 
        //(
        //    SELECT DISTINCT T.SHIP_TO,T.DELIVERY_NO,C.CUST_ABBR,T.OTHER FROM TDELIVERY T 
        //    LEFT JOIN TCUSTOMER C ON  C.SHIP_TO = CASE WHEN LENGTH(T.SHIP_TO) < 5 THEN '000009'||T.SHIP_TO  ELSE T.SHIP_TO END
        //    WHERE T.DELIVERY_NO IN (SELECT SDELIVERYNO FROM TBL_ORDERPLAN WHERE 1=1 " + ConditionD + @")  " + DateShipment + @"
        //)SHM
        //ON SHM.DELIVERY_NO = ODP.SDELIVERYNO
        //LEFT JOIN LSTHOLIDAY LHD
        //ON TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy')  =  TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy')  
        //LEFT JOIN TWINDOWTIME TWT
        //ON TWT.STERMINALID =  ODP.STERMINALID AND TWT.NLINE = ODP.NWINDOWTIMEID AND TWT.CDAYTYPE = CASE 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = NVL(TO_CHAR(ODP.DDELIVERY),'xxx') OR TO_CHAR(ODP.DDELIVERY,'DAY') IN('เสาร์','อาทิตย์') THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> NVL(TO_CHAR(ODP.DDELIVERY),'xxx') THEN '0' ELSE '' END
        //LEFT JOIN 
        //(
        //    SELECT TPS.NPLANID,TPS.DPLAN,TPS.STRUCKID,TPL.SDELIVERYNO,TPL.ROUND,TPL.NVALUE,TPL.CACTIVE,TPS.SVENDORID  FROM TPLANSCHEDULE TPS
        //    LEFT JOIN TPLANSCHEDULELIST TPL
        //    ON TPS.NPLANID = TPL.NPLANID
        //    WHERE TPL.CACTIVE = '1' AND NVL(TPS.CFIFO,'XXX') <> '1'
        //)TPC
        //ON ODP.SDELIVERYNO = TPC.SDELIVERYNO AND  ODP.SVENDORID =  TPC.SVENDORID
        //WHERE 1=1 AND 
        //CASE
        //--กรณีวันปัจจุบันทั้งที่จัดแผนแล้วและยังไม่ได้จัดและไทป์เป็น 2 เสมอ 
        //WHEN ODP.ORDERTYPE = '2' AND ODP.CACTIVE = 'Y'  AND TRUNC(ODP.DATE_CREATE) =   " + ConditionCAUSE + @"  THEN '1' 
        //--กรณีวันล่วงหน้า 1 วัน โดยไทป์จะต้องเป็นไม่สนไทป์ให้นำมาแสดง
        //WHEN TRUNC(ODP.DATE_CREATE-1) = " + ConditionCAUSE + @"     AND  NVL(TPC.STRUCKID,'xxx') = 'xxx' AND ODP.CACTIVE = 'Y'  THEN '1' 
        //--กรณีเป็นวันที่โยนแผนล่วงหน้า และวันที่โยนมากกว่าวันปัจจุบัน และวันที่ขนส่งมากกว่าวันปัจจุบัน และยังไม่ได้มีการจัดแผน
        //WHEN TRUNC(ODP.DDELIVERY) > " + ConditionCAUSE + @" AND TRUNC(ODP.DATE_CREATE) > " + ConditionCAUSE + @"  AND ODP.CACTIVE = 'Y'  AND  NVL(TPC.STRUCKID,'xxx') = 'xxx' THEN '1' 
        //ELSE '' END = '1'
        //--CASE WHEN ODP.ORDERTYPE = '2' AND ODP.CACTIVE = 'Y'  AND TRUNC(ODP.DATE_CREATE) = " + ConditionCAUSE + @"  THEN '1' 
        //--WHEN TRUNC(ODP.DDELIVERY) > " + ConditionCAUSE + @"    AND  NVL(TPC.STRUCKID,'xxx') = 'xxx'  AND ODP.CACTIVE = 'Y'  THEN '1' 
        //--WHEN TRUNC(ODP.DDELIVERY) = " + ConditionCAUSE + @"   AND ODP.CACTIVE = 'Y'  THEN '1' 
        //--ELSE '' END = '1'
        //AND  ODP.SVENDORID = '" + VENOR_ID + @"'
        //" + Condition + @"
        //ORDER BY PLANADD ASC, ODP.DDELIVERY ASC, ODP.DATE_CREATE ASC,ODP.NWINDOWTIMEID ASC";

        //ตัวจริง
        //        string QUERY = @"SELECT ODP.ORDERID, ODP.SVENDORID, ODP.SABBREVIATION, ODP.SCONTRACTID, ODP.SCONTRACTNO, 
        //        CASE WHEN ODP.ORDERTYPE = '1' THEN 'งานขนส่งภายในวัน' ELSE 'งานขนส่งล่วงหน้า' END as TRAN_TYPE,
        //        ODP.DDELIVERY, ODP.NWINDOWTIMEID, ODP.SDELIVERYNO,ODP.DATE_CREATE, ODP.ORDERTYPE,
        //        ODP.SCREATE,ODP.DATE_UPDATE,ODP.SUPDATE, SHM.OTHER as NVALUE, ODP.STERMINALID,SHM.CUST_ABBR as STERMINALNAME 
        //        ,CASE 
        //        WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = NVL(TO_CHAR(ODP.DDELIVERY),'xxx') OR TO_CHAR(ODP.DDELIVERY,'DAY') IN('เสาร์','อาทิตย์') THEN '1' 
        //        WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> NVL(TO_CHAR(ODP.DDELIVERY),'xxx') THEN '0'
        //        ELSE '' END AS CDAYTYPE -- เช็คว่า ถ้าไม่มีวันตรงกะวันหยุด และ CDAYTYPE = 0  ให้ค่าเป็น 1 และถ้า เป็นวันหยุด และ  CDAYTYPE = 1 ให้ค่าเป็น 1
        //        ,'เที่ยวที่ '||TWT.NLINE||' เวลา '||TWT.TSTART||'-'||TWT.TEND||' น.' as SWINDOWTIMENAME,TPC.ROUND,TPC.STRUCKID,TPC.DPLAN,
        //        CASE WHEN NVL(TPC.STRUCKID,'xxx') <> 'xxx' THEN 1 ELSE 0 END as PLANADD
        //        ,CASE WHEN TPC.CACTIVE = '1' THEN 'จัดแผน' WHEN TPC.CACTIVE = '0' THEN 'ยกเลิกแผน' WHEN NVL(SHM.SHIP_TO,'xxx') <> 'xxx' AND NVL(TPC.STRUCKID,'xxx') = 'xxx' THEN '' ELSE '' END as STATUS
        //        ,CASE WHEN NVL(SHM.SHIP_TO,'xxx') <> 'xxx' THEN '0' ELSE  TPC.CACTIVE END as CANCEL--, NVL(SHM.SHIP_TO,'xxx')
        //        ,TT.SABBREVIATION as STERMINALNAME_FROM
        //        FROM TBL_ORDERPLAN ODP
        //        LEFT JOIN 
        //        (
        //            SELECT d.DELIVERY_NO,d.SPLNT_CODE,d.OTHER,C.CUST_ABBR,
        //            (CASE 
        //            WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0') 
        //            WHEN TRN.REV_CODE IS NOT NULL OR TBO.RECIEVEPOINT IS NOT NULL  THEN NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) ELSE ''  END)  
        //            ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END) SHIP_TO
        //            FROM 
        //            (
        //                 --SELECT * FROM TDELIVERY
        //                 --WHERE DELIVERY_NO IN (SELECT SDELIVERYNO FROM TBL_ORDERPLAN WHERE 1=1 AND CACTIVE = 'Y'   AND SVENDORID = '" + VENOR_ID + @"'  AND TRUNC(DDELIVERY) > " + ConditionCAUSE + @")
        //                SELECT LM.* FROM TDELIVERY LM
        //                INNER JOIN 
        //                (
        //                    SELECT DELIVERY_NO,MAX(DATE_CREATED) as DATE_CREATED,MAX(DELIVERY_DATE) as DELIVERY_DATE FROM TDELIVERY 
        //WHERE  
        //                DELIVERY_NO IN (SELECT SDELIVERYNO FROM TBL_ORDERPLAN WHERE 1=1 AND CACTIVE = 'Y'  AND SVENDORID = '" + VENOR_ID + @"'  AND TRUNC(DDELIVERY) > " + ConditionCAUSE + @")
        //                    GROUP BY DELIVERY_NO
        //                )LL
        //                ON LM.DELIVERY_NO = LL.DELIVERY_NO AND LM.DATE_CREATED = LL.DATE_CREATED  AND LM.DELIVERY_DATE = LL.DELIVERY_DATE
        //                WHERE  
        //                LM.DELIVERY_NO IN (SELECT SDELIVERYNO FROM TBL_ORDERPLAN WHERE 1=1 AND CACTIVE = 'Y'  AND SVENDORID = '" + VENOR_ID + @"'  AND TRUNC(DDELIVERY) > " + ConditionCAUSE + @")
        //            )d  
        //            LEFT JOIN 
        //            (
        //                --SELECT DISTINCT DOC_NO,SHIP_ID,REV_CODE FROM TRANS_ORDER   WHERE NVL(VALTYP,'-') !='REBRAND' 
        //                SELECT  LM.DOC_NO,LM.SHIP_ID,LM.REV_CODE FROM TRANS_ORDER   LM
        //                INNER JOIN 
        //                (
        //                    SELECT  DOC_NO,MAX(DBSYS_DATE) as DBSYS_DATE  FROM TRANS_ORDER  
        //                    WHERE NVL(VALTYP,'-') !='REBRAND' AND NVL(STATUS,'N') IN ('N','G')
        //                    GROUP BY DOC_NO
        //                )LL
        //                ON LM.DOC_NO = LL.DOC_NO AND LM.DBSYS_DATE = LL.DBSYS_DATE
        //                WHERE NVL(LM.VALTYP,'-') !='REBRAND' AND NVL(LM.STATUS,'N') IN ('N','G')
        //                GROUP BY  LM.DOC_NO,LM.SHIP_ID,LM.REV_CODE
        //            )TRN
        //            ON TRN.DOC_NO = d.SALES_ORDER
        //            LEFT JOIN 
        //            (
        //                --SELECT DISTINCT DOCNO,RECIEVEPOINT FROM TBORDER WHERE RECIEVEPOINT IS NOT NULL
        //                SELECT  LM.DOCNO,LM.RECIEVEPOINT FROM TBORDER LM
        //                INNER JOIN
        //                (
        //                    SELECT  DOCNO,MAX(UPDATEDATE) as UPDATEDATE FROM TBORDER 
        //                    WHERE RECIEVEPOINT IS NOT NULL
        //                    GROUP BY DOCNO
        //                )LL
        //                ON LM.DOCNO = LL.DOCNO AND LM.UPDATEDATE = LL.UPDATEDATE
        //                GROUP BY LM.DOCNO,LM.RECIEVEPOINT
        //            )TBO
        //            ON TBO.DOCNO = d.SALES_ORDER
        //            LEFT JOIN TCUSTOMER C ON  C.SHIP_TO =  (CASE  WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0')  WHEN TRN.REV_CODE IS NOT NULL OR TBO.RECIEVEPOINT IS NOT NULL  THEN NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) ELSE ''  END)   ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END) 
        //        )SHM
        //        ON SHM.DELIVERY_NO = ODP.SDELIVERYNO
        //        LEFT JOIN LSTHOLIDAY LHD
        //        ON TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy')  =  TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy')  
        //        LEFT JOIN TWINDOWTIME TWT
        //        ON TWT.STERMINALID =  ODP.STERMINALID AND TWT.NLINE = ODP.NWINDOWTIMEID AND TWT.CDAYTYPE = CASE 
        //        WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = NVL(TO_CHAR(ODP.DDELIVERY),'xxx') OR TO_CHAR(ODP.DDELIVERY,'DAY') IN('เสาร์','อาทิตย์') THEN '1' 
        //        WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> NVL(TO_CHAR(ODP.DDELIVERY),'xxx') THEN '0' ELSE '' END
        //        LEFT JOIN 
        //        (
        //            SELECT TPS.NPLANID,TPS.DPLAN,TPS.STRUCKID,TPL.SDELIVERYNO,TPL.ROUND,TPL.NVALUE,TPL.CACTIVE,TPS.SVENDORID  FROM TPLANSCHEDULE TPS
        //            LEFT JOIN TPLANSCHEDULELIST TPL
        //            ON TPS.NPLANID = TPL.NPLANID
        //            WHERE TPL.CACTIVE = '1' AND NVL(TPS.CFIFO,'XXX') <> '1'
        //        )TPC
        //        ON ODP.SDELIVERYNO = TPC.SDELIVERYNO AND  ODP.SVENDORID =  TPC.SVENDORID
        //        LEFT JOIN TTERMINAL TT
        //        ON TT.STERMINALID = SHM.SPLNT_CODE
        //        WHERE 1=1 AND 
        //        CASE
        //        --กรณีวันปัจจุบันทั้งที่จัดแผนแล้วและยังไม่ได้จัดและไทป์เป็น 2 เสมอ 
        //        WHEN ODP.ORDERTYPE = '2' AND ODP.CACTIVE = 'Y' AND TRUNC(ODP.DDELIVERY) > " + ConditionCAUSE + @" AND  NVL(TPC.STRUCKID,'xxx') <> 'xxx' THEN '1' 
        //        --กรณีเป็นวันที่โยนแผนล่วงหน้า และวันที่โยนมากกว่าวันปัจจุบัน และวันที่ขนส่งมากกว่าวันปัจจุบัน และยังไม่ได้มีการจัดแผน
        //        WHEN ODP.ORDERTYPE = '2' AND TRUNC(ODP.DDELIVERY) > " + ConditionCAUSE + @" AND ODP.CACTIVE = 'Y'  AND  NVL(TPC.STRUCKID,'xxx') = 'xxx' AND CASE WHEN (TRUNC(SYSDATE) - TRUNC(ODP.DDELIVERY)) > 0 THEN SYSDATE ELSE TO_DATE(ODP.DDELIVERY||TO_CHAR(SYSDATE, ' HH24:MI:SS'),'dd/MM/yyyy HH24:MI:SS') END  < TRUNC(ODP.DDELIVERY)+(1)+(6/24) THEN '1' 
        //        --กรณีวันล่วงหน้า 1 วัน โดยไทป์จะต้องเป็นไม่สนไทป์ให้นำมาแสดง
        //        WHEN ODP.ORDERTYPE = '1' AND TRUNC(ODP.DATE_CREATE-1) = " + ConditionCAUSE + @" AND  NVL(TPC.STRUCKID,'xxx') = 'xxx' AND ODP.CACTIVE = 'Y' AND CASE WHEN (TRUNC(SYSDATE) - TRUNC(ODP.DATE_CREATE)) > 0 THEN SYSDATE ELSE TO_DATE(ODP.DATE_CREATE||TO_CHAR(SYSDATE, ' HH24:MI:SS'),'dd/MM/yyyy HH24:MI:SS') END  < TRUNC(ODP.DATE_CREATE)+(1)+(6/24)  THEN '1' 
        //        ELSE '' END = '1'
        //        --CASE WHEN ODP.ORDERTYPE = '2' AND ODP.CACTIVE = 'Y'  AND TRUNC(ODP.DATE_CREATE) = " + ConditionCAUSE + @"  THEN '1' 
        //        --WHEN TRUNC(ODP.DDELIVERY) > " + ConditionCAUSE + @"    AND  NVL(TPC.STRUCKID,'xxx') = 'xxx'  AND ODP.CACTIVE = 'Y'  THEN '1' 
        //        --WHEN TRUNC(ODP.DDELIVERY) = " + ConditionCAUSE + @"   AND ODP.CACTIVE = 'Y'  THEN '1' 
        //        --ELSE '' END = '1'
        //        AND  ODP.SVENDORID = '" + VENOR_ID + @"'
        //        " + Condition + @"
        //        ORDER BY PLANADD ASC, ODP.DDELIVERY ASC, ODP.DATE_CREATE ASC,ODP.NWINDOWTIMEID ASC";

        #endregion


        gvw2.DataSource = PlanTransportBLL.Instance.GetOrderByAssign2(Session["SVDID"] + string.Empty, AssignDate, cboSelect.Value + string.Empty, cboSCONTRACT.Value + "", cboCarregis.Value + "", txtDNO.Text);
        gvw2.DataBind();
        SetPlanCount(gvw2, lblPlanAll2, lblPlan2, true);
    }

    #endregion

    #region gvwPop
    void gvwPop_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ลำดับ")
        {
            e.DisplayText = (e.VisibleRowIndex + 1) + ".";
        }
    }
    protected void xcpnPopup_Load(object sender, EventArgs e)
    {


        if (gvwPop.IsCallback)
        {
            string DO = Session["DO_ID"] + "";
            ListDataPopup(DO);
        }
    }

    protected void xcpnPopup_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "ListData":
                int Inx = 0;
                if (!string.IsNullOrEmpty(txtDeliveryNo.Text))
                {
                    Inx = int.Parse(txtDeliveryNo.Text);
                }

                string DO_ID = "";
                switch (txtSelectgvw.Text)
                {
                    case "1":
                        dynamic data = gvw.GetRowValues(Inx, "SDELIVERYNO");
                        DO_ID = data + "";
                        break;
                    case "2":
                        dynamic data2 = gvw2.GetRowValues(Inx, "SDELIVERYNO");
                        DO_ID = data2 + "";
                        break;
                    case "3":
                        dynamic data3 = gvw3.GetRowValues(Inx, "SDELIVERYNO");
                        DO_ID = data3 + "";
                        break;

                }
                //else
                //{
                //    dynamic data = gvw2.GetRowValues(Inx, "SDELIVERYNO");
                //    DO_ID = data + "";
                //}
                lblDoNo.Text = "เลขที่ DO : " + DO_ID;
                Session["DO_ID"] = DO_ID;
                ListDataPopup(DO_ID);
                break;
        }
    }

    private void ListDataPopup(string DO_ID)
    {


        try
        {
            //            string QUERY = @"SELECT TDO.DOC_NO,TDO.DO_NO,TDO.DEV_DATE,TROD.SHIP_ID,TDO.ITEM_NO
            //                        ,TDO.MAT_CODE,TDO.QTY_TRAN,TDO.UOM_TRAN,PLNT_CODE SUPPLY_PLANT 
            //                        ,CASE WHEN TDO.DO_NO LIKE '008%' THEN REV_CODE ELSE SHIP_ID  END PLANT
            //                        , NVL(TDO.STATUS,'N') STATUS 
            //                        ,SYSDATE DATE_CREATED ,'SYS_SP' USER_CREATED 
            //                        ,TDO.DBSYS_DATE DUPDATE ,MS_PRD.MATERIAL_NAME
            //                         FROM (
            //                                 SELECT TRN_DO.* 
            //                                FROM TRANS_DO  TRN_DO 
            //                                INNER JOIN (
            //                                     SELECT DO_NO,DBSYS_DATE ,CASE WHEN MAX(NSTATUS)=1 THEN 'G' ELSE 'N' END STATUS
            //                                     FROM(
            //                                        SELECT DO_NO ,MAX(NSTATUS) NSTATUS,MAX(DBSYS_DATE) DBSYS_DATE 
            //                                        FROM(
            //                                             SELECT DO_NO,(CASE WHEN NVL(STATUS,'N') ='G' THEN 1 ELSE 0 END) NSTATUS ,MAX(DBSYS_DATE) DBSYS_DATE 
            //                                            FROM TRANS_DO 
            //                                            WHERE NVL(STATUS,'N') IN ('G','N')  
            //                                            GROUP BY DO_NO,CASE WHEN NVL(STATUS,'N') ='G' THEN 1 ELSE 0 END 
            //                                       )  GROUP BY DO_NO 
            //                                    )  
            //                                    WHERE 1=1 
            //                                    GROUP BY DO_NO,DBSYS_DATE
            //                                ) MAXTRN_DO ON TRN_DO.DO_NO=MAXTRN_DO.DO_NO AND NVL(TRN_DO.STATUS,'N')=MAXTRN_DO.STATUS AND TRN_DO.DBSYS_DATE=MAXTRN_DO.DBSYS_DATE
            //                         ) TDO
            //                        LEFT JOIN (
            //                            SELECT ORD.* 
            //                            FROM TRANS_ORDER  ORD 
            //                            INNER JOIN (
            //                                 SELECT DOC_NO,ITEM_NO,MAX(DBSYS_DATE) DBSYS_DATE ,CASE WHEN MAX(NSTATUS)=1 THEN 'G' ELSE 'N' END STATUS
            //                                 FROM(
            //                                     SELECT DOC_NO,ITEM_NO
            //                                    ,(CASE WHEN NVL(STATUS,'N') ='G' THEN 1 ELSE 0 END) NSTATUS
            //                                    ,MAX(DBSYS_DATE) DBSYS_DATE
            //                                    FROm TRANS_ORDER TROD 
            //                                    where 1=1
            //                                    AND  NVL(TROD .STATUS,'N') IN('N','G')
            //                                    GROUP BY DOC_NO,ITEM_NO,CASE WHEN NVL(STATUS,'N') ='G' THEN 1 ELSE 0 END 
            //                                )
            //                                GROUP BY DOC_NO,ITEM_NO --,DBSYS_DATE
            //                            ) MAXTROD ON ORD.DOC_NO=MAXTROD.DOC_NO  AND  ORD.ITEM_NO=MAXTROD.ITEM_NO  AND NVL(ORD.STATUS,'N')=MAXTROD.STATUS AND ORD.DBSYS_DATE=MAXTROD.DBSYS_DATE
            //                        ) TROD ON TDO.DOC_NO=TROD.DOC_NO AND  TDO.ITEM_NO=TROD.ITEM_NO 
            //                        INNER JOIN (SELECT DO_NO,NVL(STATUS,'N') STATUS,MAX(DBSYS_DATE) MAXDBSYSDATE FROM TRANS_DO WHERE NVL(STATUS,'N') IN ('G','N')  GROUP BY DO_NO,STATUS) TDOMAX
            //                        ON TDO.DO_NO=TDOMAX.DO_NO AND NVL(TDO.STATUS,'N')=TDOMAX.STATUS AND TDO.DBSYS_DATE=TDOMAX.MAXDBSYSDATE
            //                        LEFT JOIN  SAP_MATERIAL_MASTER_DATA MS_PRD ON TDO.MAT_CODE=MS_PRD.MATERIAL_NUMBER
            //                        WHERE 1=1 AND NVL(TDO.STATUS,'N') IN('N','G') 
            //                        AND TDO.DO_NO='" + CommonFunction.ReplaceInjection(DO_ID) + @"'
            //                        GROUP BY TDO.DOC_NO,TDO.DO_NO,TDO.DEV_DATE,TROD.SHIP_ID,TDO.ITEM_NO,TDO.MAT_CODE,TDO.QTY_TRAN,TDO.UOM_TRAN,PLNT_CODE 
            //                        ,CASE WHEN TDO.DO_NO LIKE '008%' THEN REV_CODE ELSE SHIP_ID  END , NVL(TDO.STATUS,'N')   
            //                        ,TDO.DBSYS_DATE,MS_PRD.MATERIAL_NAME ORDER BY TDO.ITEM_NO ASC";

            string QUERY = @"SELECT TGRPDATE.DOC_NO  ,TGRPDATE.DO_NO ,TVOL.DEV_DATE ,(CASE WHEN TGRPDATE.DO_NO LIKE '008%' THEN (CASE WHEN LENGTH(NVL(TORD.REV_CODE,iORD.REV_CODE))=4  THEN LPAD('9'||NVL(TORD.REV_CODE,iORD.REV_CODE),10,'0') ELSE NVL(TORD.REV_CODE,iORD.REV_CODE) END) ELSE  TORD.SHIP_ID END) SHIP_TO 
 ,ROUND(NVL(TVOL.QTY_TRAN,0),-1) QTY_TRAN
 ,TVOL.MAT_CODE 
 ,TGRPDATE.DBSYS_DATE ,'SCHD_GET3' USER_CREATED, SYSDATE DATE_UPDATED,'SCHD_GET3' USER_UPDATED ,TVOL.PLNT_CODE 
 ,MS_PRD.MATERIAL_NAME
 FROM (
     SELECT TDO.DOC_NO  ,TDO.DO_NO, MIN(NVL(TDO.STATUS,'N')) STATUS ,MAX(TDO.DBSYS_DATE) DBSYS_DATE  
     FROM TRANS_DO TDO
     WHERE 1=1  AND TDO.DO_NO='" + CommonFunction.ReplaceInjection(DO_ID) + @"'
    AND NVL(TDO.STATUS,'N') IN('N'  ,'G')   AND NVL(TDO.VALTYP,'-') !='REBRAND' 
    GROUP BY TDO.DOC_NO ,TDO.DO_NO
) TGRPDATE      
LEFT JOIN TRANS_DO TVOL ON TGRPDATE.DOC_NO=TVOL.DOC_NO  AND TGRPDATE.DO_NO=TVOL.DO_NO AND TGRPDATE.DBSYS_DATE=TVOL.DBSYS_DATE AND TGRPDATE.STATUS=NVL(TVOL.STATUS,'N')
LEFT JOIN  (SELECT DISTINCT DOC_NO,REV_CODE,SHIP_ID FROM TRANS_ORDER )  TORD ON TGRPDATE.DOC_NO=TORD.DOC_NO
LEFT JOIN (SELECT DISTINCT DOCNO DOC_NO,RECIEVEPOINT REV_CODE FROM TBORDER  WHERE RECIEVEPOINT IS NOT NULL ) IORD ON  TGRPDATE.DOC_NO=IORD.DOC_NO
LEFT JOIN  SAPECP1000087.SAP_MATERIAL_MASTER_DATA@MASTER MS_PRD ON TVOL.MAT_CODE = MS_PRD.MATERIAL_NUMBER AND TVOL.PLNT_CODE = MS_PRD.PLANT
WHERE 1=1     
GROUP BY TGRPDATE.DOC_NO  ,TGRPDATE.DO_NO,TVOL.DEV_DATE ,(CASE WHEN TGRPDATE.DO_NO LIKE '008%' THEN (CASE WHEN LENGTH(NVL(TORD.REV_CODE,iORD.REV_CODE))=4  THEN LPAD('9'||NVL(TORD.REV_CODE,iORD.REV_CODE),10,'0') ELSE NVL(TORD.REV_CODE,iORD.REV_CODE) END) ELSE  TORD.SHIP_ID END),TGRPDATE.DBSYS_DATE
,TVOL.PLNT_CODE,TVOL.MAT_CODE ,MS_PRD.MATERIAL_NAME ,ROUND(NVL(TVOL.QTY_TRAN,0),-1)";

            DataTable dt = CommonFunction.Get_Data(conn, QUERY);
            if (dt.Rows.Count > 0)
            {

                LogUser(SMENUID, "R", "เปิดดูข้อมูลรายละเอียดผลิตภัณฑ์", DO_ID);
                gvwPop.DataSource = dt;
                gvwPop.DataBind();
                lblDoNo.ClientVisible = true;
                gvwPop.ClientVisible = true;

            }
            else
            {
                lblDONODATA.ClientVisible = true;
            }
        }
        catch (Exception e)
        {
            DataTable dt = new DataTable();
            dt.Rows.Clear();
            dt.Columns.Add("SNO", typeof(int));
            dt.Columns.Add("MATERIAL_NAME", typeof(string));
            dt.Columns.Add("QTY_TRAN", typeof(int));
            dt.Columns.Add("DEV_DATE", typeof(DateTime));

            //dt.Rows.Add(1, "ERROR", 16000, DateTime.Now);

            gvwPop.DataSource = dt;
            gvwPop.DataBind();
            //CommonFunction.SetPopupOnLoad(gvwPop, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "','" + e + "')");
        }

    }
    #endregion

    #region xcpn
    protected void xcpn_Load(object sender, EventArgs e)
    {
        //if (gvw.IsCallback)
        //{
        //    ListData();
        //}
        //if (gvw2.IsCallback)
        //{
        //    ListData2();
        //}
        //if (gvw3.IsCallback)
        //{
        //    ListData3();
        //}
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        var watch = System.Diagnostics.Stopwatch.StartNew();
        watch.Start();
        string[] paras = e.Parameter.Split(';');
        int Inx = 0;
        if (paras.Length > 1)
        {
            Inx = int.Parse(paras[1] + "");
        }

        switch (paras[0])
        {
            case "Clear":
            case "Search":
                if (paras[0] == "Clear")
                {
                    cboCarregis.Value = cboSelect.Value = cboSCONTRACT.Value = null;
                    txtDNO.Text = string.Empty;
                }
                if (pageControl.ActiveTabIndex == 0)
                {
                    SetDataTableCAR();
                    ListData();
                }
                if (pageControl.ActiveTabIndex == 1)
                {
                    SetDataTableCAR();
                    ListData2();
                }

                if (pageControl.ActiveTabIndex == 2)
                    ListData3();
                //LogUser(SMENUID, "S", "ค้าหาข้อมูลหน้า จัดแผน", "");
                break;

            case "save":
                //                dynamic data = gvw.GetRowValues(Inx, "ORDERID");
                //                ASPxTextBox txtLoop = gvw.FindRowCellTemplateControl(Inx, null, "txtLoop") as ASPxTextBox;
                //                ASPxComboBox gvwcboCarregis = gvw.FindRowCellTemplateControl(Inx, null, "gvwcboCarregis") as ASPxComboBox;

                //                string QUERYINS = @"INSERT INTO TBL_PLANTRANSPORT (PLAN_ID, ORDERID, STRUCKID,ROUND,DATE_CREATE, SCREATE) 
                //VALUES ( '" + genID() + "','" + data + "','" + gvwcboCarregis.Value + "'," + txtLoop.Text + ", SYSDATE,'" + UserID + "');";

                //                string Chk = QUERYINS;
                break;

            //case "Search":
            //    ListData();
            //    break;

            case "BACK": xcpn.JSProperties["cpRedirectTo"] = "vendor_HomeAlert.aspx";
                break;
        }
        watch.Stop();
    }

    #endregion

    #region Method
    private void SetRowColumnINGrid(object sender, ASPxGridViewTableRowEventArgs e)
    {

        ASPxGridView gv = (ASPxGridView)sender;
        switch (e.RowType.ToString().ToLower())
        {
            case "data":
                #region SetControl
                ASPxLabel lblStatus = gv.FindRowCellTemplateControl(e.VisibleIndex, null, "lblStatus") as ASPxLabel;
                string lblStatusClientID = lblStatus.ClientID + "_" + e.VisibleIndex;
                lblStatus.ClientInstanceName = lblStatusClientID;

                ASPxTextBox txtLoop = gv.FindRowCellTemplateControl(e.VisibleIndex, null, "txtLoop") as ASPxTextBox;
                string txtLoopClientID = txtLoop.ClientID + "_" + e.VisibleIndex;
                txtLoop.ClientInstanceName = txtLoopClientID;
                ASPxComboBox gvwcboCarregis = gv.FindRowCellTemplateControl(e.VisibleIndex, null, "gvwcboCarregis") as ASPxComboBox;
                ASPxButton btnConfirm = gv.FindRowCellTemplateControl(e.VisibleIndex, null, "btnConfirm") as ASPxButton;
                ASPxButton btnCancel = gv.FindRowCellTemplateControl(e.VisibleIndex, null, "btnCancel") as ASPxButton;
                ASPxCallback call = gv.FindRowCellTemplateControl(e.VisibleIndex, null, "xcpnOutBound") as ASPxCallback;
                ASPxCallback callLate = gv.FindRowCellTemplateControl(e.VisibleIndex, null, "xcpnCheckLate") as ASPxCallback;

                ASPxLabel txtLate = gv.FindRowCellTemplateControl(e.VisibleIndex, null, "txtLate") as ASPxLabel;

                ASPxLabel txtLateBefore = gv.FindRowCellTemplateControl(e.VisibleIndex, null, "txtLateBefore") as ASPxLabel;

                ASPxTextBox txtLateTextBox = gv.FindRowCellTemplateControl(e.VisibleIndex, null, "txtLateTextBox") as ASPxTextBox;
                DataTable dt = Session["dtCarRegis"] as DataTable;
                if (!string.IsNullOrEmpty(e.GetValue("DDELIVERY") + string.Empty))
                {
                    DateTime DDELIVERY = DateTime.Parse(e.GetValue("DDELIVERY") + string.Empty);

                    DataRow[] drs = dt.Select("DDATE = '" + DDELIVERY.AddDays(-1).ToString("dd/MM/yyyy") + "'");
                    if (drs.Any())
                    {
                        dt = drs.CopyToDataTable();
                    }
                    else
                    {
                        dt = dt.Clone();
                    }
                }
                gvwcboCarregis.DataSource = dt;
                gvwcboCarregis.DataBind();
                gvwcboCarregis.Value = "" + e.GetValue("STRUCKID");
                #endregion

                #region Set สี
                if (("" + e.GetValue("STATUS")).Equals("ลบแผน"))
                {
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F6E3CE");
                }
                else if (("" + e.GetValue("STATUS")).Equals("จัดแผน"))
                { //CEECF5
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#CEECF5");
                }
                else if (("" + e.GetValue("STATUS")).Contains("Iterminal"))
                { //CEECF5
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F6E3CE");
                }

                string color = "#000";
                if (!string.IsNullOrEmpty(e.GetValue("DATETIMEEXPECT") + string.Empty) && !string.IsNullOrEmpty(e.GetValue("DATETIMETOTERMINAL") + string.Empty))
                {
                    DateTime? DATETIMEEXPECT = null, DATETIMETOTERMINAL = null;
                    string[] strDATETIMEEXPECT = (e.GetValue("DATETIMEEXPECT") + string.Empty).Trim().Split(' ');
                    string[] strDATETIMETOTERMINAL = (e.GetValue("DATETIMETOTERMINAL") + string.Empty).Trim().Split(' ');
                    string[] strDATETIMEEXPECTDate, strDATETIMEEXPECTTime, strDATETIMETOTERMINALDate, strDATETIMETOTERMINALTime;
                    if (strDATETIMEEXPECT.Any())
                    {
                        strDATETIMEEXPECTDate = strDATETIMEEXPECT[0].Split('/');
                        strDATETIMEEXPECTTime = strDATETIMEEXPECT[1].Split(':');
                        DATETIMEEXPECT = new DateTime(int.Parse(strDATETIMEEXPECTDate[2]), int.Parse(strDATETIMEEXPECTDate[1]), int.Parse(strDATETIMEEXPECTDate[0]), int.Parse(strDATETIMEEXPECTTime[0]), int.Parse(strDATETIMEEXPECTTime[1]), 0);
                    }
                    if (strDATETIMETOTERMINAL.Any())
                    {
                        strDATETIMETOTERMINALDate = strDATETIMETOTERMINAL[0].Split('/');
                        strDATETIMETOTERMINALTime = strDATETIMETOTERMINAL[1].Split(':');
                        DATETIMETOTERMINAL = new DateTime(int.Parse(strDATETIMETOTERMINALDate[2]), int.Parse(strDATETIMETOTERMINALDate[1]), int.Parse(strDATETIMETOTERMINALDate[0]), int.Parse(strDATETIMETOTERMINALTime[0]), int.Parse(strDATETIMETOTERMINALTime[1]), 0);
                    }
                    if (DATETIMEEXPECT != null && DATETIMETOTERMINAL != null)
                    {
                        if (DATETIMEEXPECT > DATETIMETOTERMINAL)
                        {
                            color = "#1A15EC";
                        }
                        else
                        {
                            color = "#FF0A0A";
                        }
                    }
                    txtLate.ForeColor = System.Drawing.ColorTranslator.FromHtml(color);
                }
                #endregion

                #region Set Enabled
                if (!string.IsNullOrEmpty(e.GetValue("DDELIVERY") + string.Empty)
                                    && (DateTime)e.GetValue("DDELIVERY") < DateTime.Today)
                {
                    txtLoop.ClientEnabled = false;
                    gvwcboCarregis.ClientEnabled = false;
                    btnConfirm.ClientEnabled = false;
                    btnCancel.ClientEnabled = false;
                }
                #endregion

                #region Set CellBack

                string Work_Program = "" + call.ClientID + "_" + e.VisibleIndex + ".PerformCallback(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));";
                string gvwcboCarregisClientID = gvwcboCarregis.ClientID + "_" + e.VisibleIndex;
                string txtLateClientID = txtLate.ClientID + "_" + e.VisibleIndex;
                string txtLateBeforeClientID = txtLateBefore.ClientID + "_" + e.VisibleIndex;
                string txtLateTextBoxClientID = txtLateTextBox.ClientID + "_" + e.VisibleIndex;

                txtLate.ClientInstanceName = txtLateClientID;
                txtLateTextBox.ClientInstanceName = txtLateTextBoxClientID;
                txtLateBefore.ClientInstanceName = txtLateBeforeClientID;
                gvwcboCarregis.ClientInstanceName = gvwcboCarregisClientID;
                btnConfirm.ClientSideEvents.Click = "function (s, e) { if(" + txtLateBeforeClientID + ".GetText().indexOf('ล่าช้า') > -1  ){dxConfirm('ยืนยันการทำงาน'," + txtLateBeforeClientID + ".GetText() + ' คุณยืนยันที่จะส่งรถคันนี้หรือไม่ ?',function(ss,ee){ dxPopupConfirm.Hide(); if(" + txtLoopClientID + ".GetText() != '') { if(" + gvwcboCarregisClientID + ".GetText() != '') { " + Work_Program + " } } else {  dxWarning('แจ้งเตือน', 'ระบุเที่ยวการขนส่งและทะเบียนรถ'); }} ,function(s,e){ dxPopupConfirm.Hide(); });} else{  if(" + txtLoopClientID + ".GetText() != '') { if(" + gvwcboCarregisClientID + ".GetText() != '') { " + Work_Program + " } } else {  dxWarning('แจ้งเตือน', 'ระบุเที่ยวการขนส่งและทะเบียนรถ'); }}  }";
                btnCancel.ClientSideEvents.Click = @"function (s, e) {  
                " + gvwcboCarregisClientID + @".SetText(''); 
                " + gvwcboCarregisClientID + @".SetValue(''); 
                " + txtLateBeforeClientID + @".SetText(''); 
                " + txtLateClientID + @".SetText(''); 
                " + Work_Program + @" 
            }";
                call.ClientInstanceName = call.ClientID + "_" + e.VisibleIndex;
                call.ClientSideEvents.CallbackComplete = @"function(s,e){ 
                    if(e.result != '') 
                    {  
                        var res = e.result.split(';;');
                        if(res[0] == 'S')
                        { 
                            if(" + txtLateBeforeClientID + @".GetText() != '')
                                dxInfo('แจ้งให้ทราบ'," + txtLateBeforeClientID + @".GetText()); 
                            else
                                dxInfo('แจ้งให้ทราบ','จัดแผนผ่านไม่ตรวจสอบ Late Delivery'); 
                            " + lblStatusClientID + @".SetText('จัดแผน'); 
                            lblPlan.SetText(res[1]);
                            lblPlan2.SetText(res[1]);
                            $('#" + e.Row.ClientID + @"').css('background-color', '#CEECF5'); 
                        }else if(res[0] == 'C')
                        { dxInfo('แจ้งให้ทราบ','ลบแผนผ่าน'); 
                        " + lblStatusClientID + @".SetText('รอยืนยัน'); 
                            lblPlan.SetText(res[1]);
                            lblPlan2.SetText(res[1]);
                            $('#" + e.Row.ClientID + @"').css('background-color', ''); 
                            " + txtLoopClientID + @".SetValue('');
                        }else{ 
                            dxError('พบข้อผิดพลาด',res[0]);  
                            " + gvwcboCarregisClientID + @".SetText(''); 
                            " + gvwcboCarregisClientID + @".SetValue(''); 
                            " + txtLateClientID + @".SetText(''); 
                        }
                    } 
                    else{ 
                        var res = e.result.split(';;');
                        " + txtLoopClientID + @".SetText('" + e.GetValue("ROUND") + @"'); 
                        " + gvwcboCarregisClientID + @".SetValue('" + e.GetValue("STRUCKID") + "'); dxError('พบข้อผิดพลาด',res[0]);   } }";

                #endregion


                #region Set CellBack Late
                if (!string.IsNullOrEmpty("" + e.GetValue("DATETIMEEXPECT")))
                {
                    string Work_Program_Late = "" + callLate.ClientID + "_" + e.VisibleIndex + ".PerformCallback(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));";
                    gvwcboCarregis.ClientInstanceName = gvwcboCarregisClientID;
                    gvwcboCarregis.ClientSideEvents.SelectedIndexChanged = "function (s, e) { if(" + gvwcboCarregisClientID + ".GetText() != '') { " + Work_Program_Late + " } }   ";

                    callLate.ClientInstanceName = callLate.ClientID + "_" + e.VisibleIndex;
                    callLate.ClientSideEvents.CallbackComplete = @"function(s,e){ 
                    if(e.result != '') 
                    {  
                        " + txtLateClientID + @".SetValue(e.result);
                        " + txtLateTextBoxClientID + @".SetValue(e.result);
                       if(e.result.indexOf('ไม่มีรถในระบบ') > -1)
                        {
                            dxWarning('แจ้งให้ทราบ',e.result.split(';')[0]);
                        }
                        else
                        {
                            " + txtLateClientID + @".SetValue(e.result.split(';')[1]);
                            " + txtLateTextBoxClientID + @".SetValue(e.result.split(';')[1]);
                            " + txtLateClientID + @".GetMainElement().style.color = e.result.split(';')[2];
                            " + txtLateBeforeClientID + @".SetValue(e.result.split(';')[3]);
                            dxInfo('แจ้งให้ทราบ',e.result.split(';')[0]);
                        }
                    } 
                    else{ }}";
                }

                #endregion
                break;
        }
    }
    private void Del_Plan(string DO_ID, string STIMEWINDOW, string STRUCKID, string STERMINAL, ref OracleConnection con, bool isIterminal)
    {
        //เอา DO ไปหาแผน
        string QUERY_MULTIDROP = "SELECT SDELIVERYNO,NPLANID FROM TPLANSCHEDULELIST WHERE SDELIVERYNO = '" + CommonFunction.ReplaceInjection(DO_ID) + "' AND CACTIVE = '1'";
        DataTable dt_ChkMultiDrop = CommonFunction.Get_Data(conn, QUERY_MULTIDROP);
        if (dt_ChkMultiDrop.Rows.Count > 0)
        {
            //countPlan -= 1;
            Session["LOG_PROCESS"] += "C8;";

            //เอา DO ไปหาแผน ที่เป็นมันติดรอปทั้งหมด
            string NPLANID = dt_ChkMultiDrop.Rows[0]["NPLANID"] + "";

            string QUERY_MULTIDROP2 = "SELECT SDELIVERYNO,NPLANID FROM TPLANSCHEDULELIST WHERE NPLANID = '" + CommonFunction.ReplaceInjection(NPLANID) + "' AND CACTIVE = '1' ORDER BY NDROP ASC";
            DataTable dt_ChkMultiDrop2 = CommonFunction.Get_Data(conn, QUERY_MULTIDROP2);
            if (dt_ChkMultiDrop2.Rows.Count > 0)
            {
                Session["LOG_PROCESS"] += "C9;";

                string QUERY_STRUCKID = "SELECT STRUCKID,NPLANID,STERMINALID FROM TPLANSCHEDULE WHERE NPLANID = '" + CommonFunction.ReplaceInjection(NPLANID) + "' AND CACTIVE = '1'";
                DataTable dt_ChkMultiDropTruck = CommonFunction.Get_Data(conn, QUERY_STRUCKID);

                string VehNo = "";
                string TuNo = "";
                string TerminalID = "";
                if (dt_ChkMultiDropTruck.Rows.Count > 0)//หารถจาก TPLANSCHEDULE ใหม่เพื่อไม่ให้เกิดเรื่องบัคของข้อมูล
                {
                    TerminalID = dt_ChkMultiDropTruck.Rows[0]["STERMINALID"] + "";

                    string Veh = "SELECT STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO FROM TTRUCK WHERE STRUCKID = '" + dt_ChkMultiDropTruck.Rows[0]["STRUCKID"] + "'";
                    DataTable dt_Veh = CommonFunction.Get_Data(conn, Veh);
                    if (dt_Veh.Rows.Count > 0)
                    {
                        VehNo = dt_Veh.Rows[0]["SHEADREGISTERNO"] + "";
                        TuNo = dt_Veh.Rows[0]["STRAILERREGISTERNO"] + "";
                    }
                }

                //กรณีถ้า NPLAN มีจะวนลบก่อน
                string SPLAN_DELFIRST = "";
                for (int i = 0; i < dt_ChkMultiDrop2.Rows.Count; i++)
                {
                    SPLAN_DELFIRST += "," + dt_ChkMultiDrop2.Rows[i]["SDELIVERYNO"] + "";
                }
                //ลบแผนเก่าออกจาก Iterminal
                SPLAN_DELFIRST = !string.IsNullOrEmpty(SPLAN_DELFIRST) ? SPLAN_DELFIRST.Remove(0, 1) : "";
                string CLEAR = string.Empty;
                if (isIterminal)
                {
                    CLEAR = ITEMINAL_DELETE(SPLAN_DELFIRST, STIMEWINDOW, VehNo, TuNo, STERMINAL, NPLANID, DateTime.Now.ToString(), ref con);

                }
                else
                {
                    CLEAR = "S;Delete Success";
                }


                //วันเพื่ออัพเดทดรอปใหม่
                AddTODB("UPDATE TPLANSCHEDULELIST SET  CACTIVE = 0 WHERE NPLANID = " + NPLANID + " AND SDELIVERYNO = '" + CommonFunction.ReplaceInjection(DO_ID) + "'", ref con);
                DataRow[] myResultSet = dt_ChkMultiDrop2.Select("SDELIVERYNO <> '" + CommonFunction.ReplaceInjection(DO_ID) + "'");
                SPLAN_DELFIRST = "";
                for (int i = 0; i < myResultSet.Count(); i++)
                {
                    AddTODB("UPDATE TPLANSCHEDULELIST SET  NDROP = " + (i + 1) + " WHERE SDELIVERYNO = '" + CommonFunction.ReplaceInjection(myResultSet[i]["SDELIVERYNO"] + "") + "' AND CACTIVE = '1' ", ref con);
                    SPLAN_DELFIRST += "," + myResultSet[i]["SDELIVERYNO"] + "";
                }
                string ADD = "";
                //และเซฟแผนใหม่
                SPLAN_DELFIRST = !string.IsNullOrEmpty(SPLAN_DELFIRST) ? SPLAN_DELFIRST.Remove(0, 1) : "";
                if (!string.IsNullOrEmpty(SPLAN_DELFIRST) && isIterminal)
                {
                    ADD = ITEMINAL(SPLAN_DELFIRST, STIMEWINDOW, VehNo, TuNo, STERMINAL, NPLANID, DateTime.Now.ToString(), ref  con);
                }
                //ถ้าแผนที่สร้างจาก DO และ TPLANSCHEDULELIST ไม่มี Drop เลยจะตีเป็นไม่ใช้แผนนั้น
                if (myResultSet.Count() == 0)
                {
                    AddTODB("UPDATE TPLANSCHEDULE SET  CACTIVE = 0 WHERE NPLANID = '" + CommonFunction.ReplaceInjection(dt_ChkMultiDrop.Rows[0]["NPLANID"] + "") + "'", ref con);
                }

                Session["REPLAN"] = CLEAR + "," + ADD;

            }
            else
            {
                Session["LOG_PROCESS"] += "C10;";
            }

        }
        else
        {
            Session["LOG_PROCESS"] += "C11;";
        }


    }

    private void SetPlanCount(ASPxGridView gv, ASPxLabel PlanAll, ASPxLabel Plan, bool isLoad)
    {
        PlanAll.Text = gv.VisibleRowCount + string.Empty;
        if (isLoad)
        {
            countPlan = 0;
            for (int i = 0; i < gv.VisibleRowCount; i++)
            {

                var data = gv.GetRowValues(i, "STRUCKID");
                //if (isLoad)
                //{
                if (!string.IsNullOrEmpty(data + string.Empty))
                {
                    countPlan += 1;
                }
                //}
                //else
                //{
                //    ASPxComboBox gvwcboCarregis = gv.FindRowCellTemplateControl(i, null, "gvwcboCarregis") as ASPxComboBox;
                //    if (gvwcboCarregis != null)
                //    {
                //        if (!string.IsNullOrEmpty(gvwcboCarregis.Value + string.Empty))
                //        {
                //            countPlan += 1;
                //        }
                //    }
                //    else
                //    {
                //        if (!string.IsNullOrEmpty(data + string.Empty))
                //        {
                //            countPlan += 1;
                //        }
                //    }
                //}


            }
        }

        Plan.Text = countPlan + string.Empty;
    }


    private void AddTODB(string strQuery, ref OracleConnection con)
    {
        using (OracleCommand com = new OracleCommand(strQuery, con))
        {
            com.Transaction = tran;
            com.ExecuteNonQuery();
        }
    }

    private string genID()
    {
        string sql = @"SELECT PLAN_ID FROM TBL_PLANTRANSPORT ORDER BY PLAN_ID DESC";
        string Result = "";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, sql);
        if (dt.Rows.Count > 0)
        {
            string sss = dt.Rows[0]["PLAN_ID"] + "";

            if (!string.IsNullOrEmpty(sss))
            {
                sss = sss.Substring(7, 4);

                Result = DateTime.Now.ToString("yyMMdd") + "-" + (int.Parse(sss) + 1).ToString().PadLeft(4, '0') + "";
            }
            else
            {

            }
        }
        else
        {

            Result = DateTime.Now.ToString("yyMMdd") + "-0001";
        }

        return Result;
    }

    private string CheckTruckFull(string STRUCKID, decimal Capacity, string STIMEWINDOW, DateTime DDELIVERY, string Round, string SVENDORID, string STIMEWINDOWNAME, string Carregis, string STERMINALID)
    {
        string Result = "";

        if (!string.IsNullOrEmpty(STRUCKID))
        {

            //string DPLAN = edtAssign.Text;
            string SDDELIVERY = DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            //หาความจุของรถ
            //DataTable dtCapacityCar = CommonFunction.Get_Data(conn, "SELECT STRUCKID,NTOTALCAPACITY FROM TTRUCK WHERE STRUCKID = '" + STRUCKID + "'");
            string TRUCKCAPACITY = GetCarpacity(STRUCKID);
            //หาความจุรวมของรถที่ได้ลงแผนไปแล้ว โดยจำแนกตาม วันที่จัดแผน รถ กะที่
            string QUERY = @"SELECT MAX(ODP.DATE_CREATE) as DPLAN,TSP.STIMEWINDOW,TSP.STRUCKID,SUM(TPL.NVALUE) as NVALUE,MAX(TPL.ROUND) as ROUND ,
TRUNC(DDELIVERY) DDELIVERY,TSP.STERMINALID
FROM TPLANSCHEDULE TSP
LEFT JOIN TPLANSCHEDULELIST TPL
ON TSP.NPLANID = TPL.NPLANID
LEFT JOIN TBL_ORDERPLAN ODP
ON ODP.SDELIVERYNO = TPL.SDELIVERYNO
WHERE TRUNC(DDELIVERY) = TO_DATE('" + SDDELIVERY + @"','dd/MM/yyyy') AND TSP.STRUCKID = '" + STRUCKID + @"'
AND STIMEWINDOW = '" + STIMEWINDOW + @"' AND TSP.CACTIVE = '1' AND NVL(ODP.CACTIVE,'Y')!='N' AND TPL.CACTIVE = '1' AND TSP.STERMINALID = '" + STERMINALID + @"'
GROUP BY TRUNC(DDELIVERY),TSP.STIMEWINDOW,TSP.STRUCKID,TSP.STERMINALID";

            DataTable dtCarAddOil = CommonFunction.Get_Data(conn, QUERY);
            //เช็คว่ารถคันนี้เคยมีความจุไหมถ้าไม่มีให้เช็คจากความจุที่จะเซฟโดยตรง
            if (dtCarAddOil.Rows.Count > 0)
            {
                //เช็คว่าใส่รอบ

                //เช็คว่ารถมีความจุไหม
                if (!string.IsNullOrEmpty(TRUCKCAPACITY))
                {
                    decimal CapacityTruck = decimal.Parse(TRUCKCAPACITY) + 50;// บวกห้าสิบให้รถ เพราะมีน้ำมันโหลดเกินเนื่องจากการเติมที่คาดเคลื่อนได้
                    decimal CaPacityAddOil = decimal.Parse(dtCarAddOil.Rows[0]["NVALUE"] + "");
                    decimal AllCapacity = Capacity + CaPacityAddOil;//ความจุแผนใหม่ + ความจุแผนที่เคย บรรทึก
                    if (CapacityTruck >= AllCapacity)
                    {

                    }
                    else
                    {

                        string QueryS = @"SELECT ODP.DATE_CREATE as DPLAN, TRUNC(DDELIVERY)  DDELIVERY,TSP.STIMEWINDOW,TSP.STRUCKID,TPL.NVALUE as NVALUE,TPL.ROUND as ROUND,TPL.SDELIVERYNO,TSP.STERMINALID FROM TPLANSCHEDULE TSP
LEFT JOIN TPLANSCHEDULELIST TPL
ON TSP.NPLANID = TPL.NPLANID
LEFT JOIN TBL_ORDERPLAN ODP
ON ODP.SDELIVERYNO = TPL.SDELIVERYNO
WHERE  TRUNC(DDELIVERY) = TO_DATE('" + SDDELIVERY + @"','dd/MM/yyyy') AND TSP.STRUCKID = '" + STRUCKID + @"'
AND STIMEWINDOW = '" + STIMEWINDOW + @"' AND TSP.CACTIVE = '1' AND NVL(ODP.CACTIVE,'Y') = 'Y' AND TPL.CACTIVE = '1' AND TSP.STERMINALID = '" + STERMINALID + @"'";

                        string Error_detail = "";
                        DataTable dtEr = CommonFunction.Get_Data(conn, QueryS);
                        if (dtEr.Rows.Count > 0)
                        {

                            for (int i = 0; i < dtEr.Rows.Count; i++)
                            {
                                Error_detail += "เลขที่DO :" + dtEr.Rows[i]["SDELIVERYNO"] + "<br> ความจุ :" + dtEr.Rows[i]["NVALUE"] + "<br> กะการขนส่ง :" + STIMEWINDOWNAME + "<br>";
                            }
                            Result = "รถ " + Carregis + " นี้มีความจุ " + TRUCKCAPACITY + " ไม่พอขนส่ง เนื่องจากได้มีการขนส่ง <br>" + Error_detail;
                        }
                        else
                        {
                            Result = "รถ " + Carregis + " นี้มีความจุ " + TRUCKCAPACITY + " ไม่พอขนส่ง";
                        }

                    }

                }
                else
                {
                    Result = "รถ " + Carregis + " นี้ไม่พบความจุรวม";
                }

            }
            else
            {
                //เช็คว่ารถมีความจุไหม
                if (!string.IsNullOrEmpty(TRUCKCAPACITY))
                {
                    decimal CapacityTruck = decimal.Parse(TRUCKCAPACITY) + 50;// บวกห้าสิบให้รถ เพราะมีน้ำมันโหลดเกินเนื่องจากการเติมที่คาดเคลื่อนได้
                    if (CapacityTruck >= Capacity)
                    {

                    }
                    else
                    {
                        Result = "รถ " + Carregis + " นี้มีความจุ " + TRUCKCAPACITY + " ไม่พอขนส่ง";
                    }

                }
                else
                {

                    Result = "รถ " + Carregis + "  นี้ไม่พบความจุรวม";
                }
            }
        }
        else
        {
            Result = "ทะเบียนรถที่ส่งมาเป็นค่าว่าง";
        }


        return Result;
    }

    private string INSERT_TO_TPLANSCHEDULE(string genid, string SVENDORID, string STERMINALID, string SCONTRACTID, string sDate, string STIMEWINDOW, string SCONTRACTNO, string STRUCKID, ref OracleConnection con)
    {
        string sUserID = Session["UserID"] + "";

        string CHK2 = "SELECT SHEADREGISTERNO,STRAILERREGISTERNO FROM TTRUCK WHERE STRUCKID = '" + STRUCKID + "'";
        DataTable dt2 = CommonFunction.Get_Data(conn, CHK2);
        string SHEADREGISTERNO = "";
        string STRAILERREGISTERNO = "";
        if (dt2.Rows.Count > 0)
        {

            if (dt2.Rows.Count > 0)
            {
                SHEADREGISTERNO = dt2.Rows[0]["SHEADREGISTERNO"] + "";
                STRAILERREGISTERNO = dt2.Rows[0]["STRAILERREGISTERNO"] + "";
            }
        }

        string QUERY = @"
            INSERT INTO TPLANSCHEDULE(NNO,SVENDORID,STERMINALID,SCONTRACTID,DPLAN,SPLANDATE,DDELIVERY,STIMEWINDOW,CACTIVE,DCREATE,SCREATE,SCONTRACTNO,STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO,CCONFIRM) 
            VALUES ( 
             1,
             '" + SVENDORID + @"',
             '" + STERMINALID + @"',
             '" + SCONTRACTID + @"',
             SYSDATE,
             TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + @"','dd/MM/yyyy'),
             TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + @"','dd/MM/yyyy'),
             " + CommonFunction.ReplaceInjection(STIMEWINDOW) + @",
             '1',
             SYSDATE,
             '" + sUserID + @"',
             '" + SCONTRACTNO + @"',
             '" + STRUCKID + @"',
             '" + SHEADREGISTERNO + @"',
             '" + STRAILERREGISTERNO + @"','1')  RETURNING nPlanID INTO :nPlanID";

        //AddTODB(QUERY, ref con);
        string nPlanID = "0";
        using (OracleCommand com = new OracleCommand(QUERY, con))
        {
            com.Transaction = tran;
            com.Parameters.Clear();
            com.Parameters.Add(new OracleParameter()
            {
                Direction = ParameterDirection.Output,
                ParameterName = "nPlanID",
                OracleType = OracleType.Number,
                IsNullable = true,
            });
            com.ExecuteNonQuery();
            nPlanID = com.Parameters["nPlanID"].Value + string.Empty;
        }
        return nPlanID;
    }

    private void INSERT_TO_TPLANSCHEDULELIST(string genid, string NPLANID, string NDROP, string SDELIVERYNO, string CAPACITY, string SSHIPTO, string TRAN_TYPE, string ROUND, ref OracleConnection con)
    {

        string QUERY = @"
            INSERT INTO TPLANSCHEDULELIST(SPLANLISTID,NPLANID,NDROP,SDELIVERYNO,NVALUE,CACTIVE,SSHIPTO,TRAN_TYPE,ROUND) 
            VALUES ( '" + genid + @"',
             " + NPLANID + @",
             '" + NDROP + @"',
             '" + SDELIVERYNO + @"',
             " + CAPACITY + @",
             '1',
             '" + CommonFunction.ReplaceInjection(SSHIPTO) + @"',
             '" + CommonFunction.ReplaceInjection(TRAN_TYPE) + @"',
             " + ROUND + ")";

        AddTODB(QUERY, ref con);
    }

    //    private void UPDATE_TO_TPLANSCHEDULE(string SVENDORID, string STERMINALID, string SCONTRACTID, string SDELIVERYNO, string sDate, string STIMEWINDOW, string SCONTRACTNO)
    //    {
    //        string sUserID = Session["UserID"] + "";
    //        string CHK = "SELECT NPLANID,SDELIVERYNO FROM TPLANSCHEDULELIST WHERE SDELIVERYNO = '" + SDELIVERYNO + "'";
    //        DataTable dt = CommonFunction.Get_Data(conn, CHK);
    //        string PLANID = dt.Rows[0]["NPLANID"] + "";
    //        if (dt.Rows.Count > 0)
    //        {
    //            string QUERY = @"UPDATE TPLANSCHEDULE 
    //                         SET
    //                            SVENDORID = '" + SVENDORID + @"',
    //                            STERMINALID = '" + STERMINALID + @"',
    //                            SCONTRACTID = '" + SCONTRACTID + @"',
    //                            SPLANDATE =   TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + @"','dd/MM/yyyy'),
    //                            DDELIVERY =   TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + @"','dd/MM/yyyy'),
    //                            STIMEWINDOW =  " + CommonFunction.ReplaceInjection(STIMEWINDOW) + @",
    //                            DUPDATE = sysdate,
    //                            SUPDATE = '" + sUserID + @"',
    //                            SCONTRACTNO = '" + SCONTRACTNO + @"'
    //                            WHERE NPLANID = " + PLANID + @"";

    //            AddTODB(QUERY);
    //        }
    //    }

    //    private void UPDATE_TO_TPLANSCHEDULELIST(string CAPACITY, string SSHIPTO, string TRAN_TYPE, string SDELIVERYNO)
    //    {
    //        string sUserID = Session["UserID"] + "";
    //        string QUERY = @"UPDATE TPLANSCHEDULELIST 
    //                                SET
    //                                    NVALUE =   " + CAPACITY + @",
    //                                    SSHIPTO =   '" + CommonFunction.ReplaceInjection(SSHIPTO) + @"',
    //                                    TRAN_TYPE =  '" + CommonFunction.ReplaceInjection(TRAN_TYPE) + @"'
    //                                    WHERE SDELIVERYNO = '" + SDELIVERYNO + @"'";

    //        AddTODB(QUERY);
    //    }

    private void UPDATE_TO_TPLANSCHEDULE(string STRUCKID, string SDELIVERYNO, ref OracleConnection con)
    {
        string sUserID = Session["UserID"] + "";
        string CHK = "SELECT NPLANID,SDELIVERYNO FROM TPLANSCHEDULELIST WHERE SDELIVERYNO = '" + SDELIVERYNO + "'";
        DataTable dt = CommonFunction.Get_Data(conn, CHK);
        string PLANID = dt.Rows[0]["NPLANID"] + "";

        string CHK2 = "SELECT SHEADREGISTERNO,STRAILERREGISTERNO FROM TTRUCK WHERE STRUCKID = '" + STRUCKID + "'";
        DataTable dt2 = CommonFunction.Get_Data(conn, CHK2);

        if (dt.Rows.Count > 0)
        {
            Session["LOG_PROCESS"] += "C4;";

            string SHEADREGISTERNO = "";
            string STRAILERREGISTERNO = "";
            if (dt2.Rows.Count > 0)
            {
                SHEADREGISTERNO = dt2.Rows[0]["SHEADREGISTERNO"] + "";
                STRAILERREGISTERNO = dt2.Rows[0]["STRAILERREGISTERNO"] + "";
            }

            string QUERY = @"UPDATE TPLANSCHEDULE 
                         SET
                            STRUCKID = '" + STRUCKID + @"',
                            SHEADREGISTERNO = '" + SHEADREGISTERNO + @"',
                            STRAILERREGISTERNO = '" + STRAILERREGISTERNO + @"'
                            WHERE NPLANID = " + PLANID + @"";

            AddTODB(QUERY, ref con);
        }
        else
        {
            Session["LOG_PROCESS"] += "C5;";
        }
    }

    private void UPDATE_TO_TPLANSCHEDULELIST(string ROUND, string SDELIVERYNO, ref OracleConnection con)
    {
        Session["LOG_PROCESS"] += "C6;";
        string sUserID = Session["UserID"] + "";
        string QUERY = @"UPDATE TPLANSCHEDULELIST 
                                SET
                                    ROUND =   " + ROUND + @"
                                    WHERE SDELIVERYNO = '" + SDELIVERYNO + @"'";

        AddTODB(QUERY, ref con);
    }

    void SetDataTableCAR()
    {
        //string UserID = Session["UserID"] + "";
        //string vehID = Session["SVDID"] + string.Empty;

        DateTime? dte = new DateTime();
        string Datestart = string.Empty, DateEnd = string.Empty;
        if (!string.IsNullOrEmpty(edtAssign.Text + ""))
        {
            dte = ConvertToDateNull(edtAssign.Text);
            if (dte != null)
            {
                Datestart = dte.Value.AddDays(-1).ToString("dd/MM/yyyy");
                if (pageControl.ActiveTabIndex == 0)
                {
                    DateEnd = Datestart;
                }
                else
                {
                    DateEnd = (dte.Value.AddDays(6)).ToString("dd/MM/yyyy");
                }
            }
        }
        #region Query รถที่ยืนยัน แปปเดิม
        //        string DATESET = @"SELECT MAX(ODP.DDELIVERY) as DMAX,MIN(ODP.DDELIVERY) as DMIN
        //FROM TBL_ORDERPLAN ODP
        //LEFT JOIN 
        //(
        //    SELECT TPS.NPLANID,TPS.DPLAN,TPS.STRUCKID,TPL.SDELIVERYNO,TPL.ROUND,TPL.NVALUE,TPL.CACTIVE,TPS.SVENDORID  FROM TPLANSCHEDULE TPS
        //    LEFT JOIN TPLANSCHEDULELIST TPL
        //    ON TPS.NPLANID = TPL.NPLANID
        //    WHERE TPL.CACTIVE = '1'
        //)TPC
        //ON ODP.SDELIVERYNO = TPC.SDELIVERYNO AND  ODP.SVENDORID =  TPC.SVENDORID
        //WHERE 1=1 AND ODP.CACTIVE = 'Y' --AND NVL(TPC.STRUCKID,'xxx') = 'xxx' 
        //AND  ODP.SVENDORID = '" + CommonFunction.ReplaceInjection(vehID) + "'";
        //        DataTable dt_DATE = CommonFunction.Get_Data(conn, DATESET);

        //        string Codition_date = "";
        //        string Datestart = "";
        //        string DateEnd = "";
        //        if (dt_DATE.Rows.Count > 0)
        //        {
        //            Datestart = !string.IsNullOrEmpty(dt_DATE.Rows[0]["DMIN"] + "") ? "TO_DATE('" + DateTime.Parse(dt_DATE.Rows[0]["DMIN"] + "").ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy')" : "";
        //            DateEnd = !string.IsNullOrEmpty(dt_DATE.Rows[0]["DMAX"] + "") ? "TO_DATE('" + DateTime.Parse(dt_DATE.Rows[0]["DMAX"] + "").ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy')" : "";

        //            if (!string.IsNullOrEmpty(Datestart) && !string.IsNullOrEmpty(DateEnd))
        //            {
        //                Codition_date = "WHERE TRUNC(TCF.DDATE+1) BETWEEN " + Datestart + " AND " + DateEnd + "";
        //            }
        //        }

        //string vehID = "0010001778";

        //        string QUERY = @"SELECT SHEADID , SHEADREGISTERNO,STRAILERREGISTERNO,CASE  WHEN  NVL(SHEADREGISTERNO,'xxx') <> 'xxx' THEN '/'||STRAILERREGISTERNO ELSE ' ' END as tail1 FROM
        //(
        //    SELECT ROW_NUMBER()OVER(ORDER BY TUC.SHEADID) AS RN ,TUC.SHEADID,TUC.SHEADREGISTERNO,TUC.STRAILERREGISTERNO FROM  TCONTRACT CON
        //    INNER JOIN
        //    (
        //        SELECT TCF.SCONTRACTID,TCFL.SHEADID,TCFL.SHEADREGISTERNO,TCFL.STRAILERREGISTERNO FROM TTRUCKCONFIRM TCF
        //        INNER JOIN 
        //        (
        //            SELECT TRKC.NCONFIRMID,TRKC.SHEADID,TRKH.SHEADREGISTERNO,TRKT.SHEADREGISTERNO as STRAILERREGISTERNO FROM TTRUCKCONFIRMLIST TRKC
        //            INNER JOIN TTRUCK TRKH
        //            ON TRKH.STRUCKID = TRKC.SHEADID
        //            LEFT JOIN TTRUCK TRKT
        //            ON TRKT.STRUCKID = TRKC.STRAILERID
        //            WHERE TRKC.CCONFIRM = '1'
        //            GROUP BY TRKC.NCONFIRMID,TRKC.SHEADID,TRKH.SHEADREGISTERNO,TRKT.SHEADREGISTERNO
        //        ) TCFL
        //        ON TCFL.NCONFIRMID = TCF.NCONFIRMID AND TCF.CCONFIRM = '1'
        //        --WHERE TO_CHAR(TCF.DCREATE,'dd/MM/yyyy') = '" + DATESEARCH + @"'
        //        GROUP BY TCF.SCONTRACTID,TCFL.SHEADID,TCFL.SHEADREGISTERNO,TCFL.STRAILERREGISTERNO
        //    )TUC
        //    ON TUC.SCONTRACTID = CON.SCONTRACTID
        //    WHERE CON.SVENDORID = '" + CommonFunction.ReplaceInjection(vehID) + @"' AND CON.CACTIVE = 'Y'
        //    GROUP BY  TUC.SHEADID,TUC.SHEADREGISTERNO,TUC.STRAILERREGISTERNO
        //)";

        //        string Codition_date = " AND TRUNC(TCF.DDATE+1) BETWEEN TO_DATE('" + Datestart + "','dd/mm/yyyy') AND TO_DATE('" + DateEnd + "','dd/mm/yyyy') ";
        //        string QUERY = @"SELECT SHEADID , SHEADREGISTERNO,STRAILERREGISTERNO,CASE  WHEN  NVL(SHEADREGISTERNO,'xxx') <> 'xxx' THEN '/'||STRAILERREGISTERNO ELSE ' ' END as tail1,TO_CHAR(DDATE,'dd/MM/yyyy') as DDATE,SCONTRACTID,CSTANDBY,SVENDORID FROM
        //(
        //    SELECT ROW_NUMBER()OVER(ORDER BY TUC.SHEADID) AS RN ,TUC.SHEADID,TUC.SHEADREGISTERNO,TUC.STRAILERREGISTERNO,TUC.DDATE,CON.SCONTRACTID,TCT.CSTANDBY,CON.SVENDORID  FROM  TCONTRACT CON
        //    INNER JOIN
        //    (
        //        SELECT TCF.SCONTRACTID,TCFL.SHEADID,TCFL.STRAILERID,TCFL.SHEADREGISTERNO,TCFL.STRAILERREGISTERNO,TRUNC(TCF.DDATE) as  DDATE FROM TTRUCKCONFIRM TCF
        //        INNER JOIN 
        //        (
        //            SELECT TRKC.NCONFIRMID,TRKC.SHEADID,TRKC.STRAILERID,TRKH.SHEADREGISTERNO,TRKT.SHEADREGISTERNO as STRAILERREGISTERNO FROM TTRUCKCONFIRMLIST TRKC
        //            INNER JOIN TTRUCK TRKH
        //            ON TRKH.STRUCKID = TRKC.SHEADID
        //            LEFT JOIN TTRUCK TRKT
        //            ON NVL(TRKT.STRUCKID,'TRxxx') = NVL(TRKC.STRAILERID,'TRxxx')
        //            WHERE TRKC.CCONFIRM = '1'
        //            GROUP BY TRKC.NCONFIRMID,TRKC.SHEADID,TRKC.STRAILERID,TRKH.SHEADREGISTERNO,TRKT.SHEADREGISTERNO
        //        ) TCFL
        //        ON TCFL.NCONFIRMID = TCF.NCONFIRMID AND TCF.CCONFIRM = '1'
        //        " + Codition_date + @"
        //        GROUP BY TCF.SCONTRACTID,TCFL.SHEADID,TCFL.STRAILERID,TCFL.SHEADREGISTERNO,TCFL.STRAILERREGISTERNO,TRUNC(TCF.DDATE)
        //    )TUC
        //    ON TUC.SCONTRACTID = CON.SCONTRACTID
        //    LEFT JOIN TCONTRACT_TRUCK TCT
        //    ON CON.SCONTRACTID = TCT.SCONTRACTID AND TUC.SHEADID = TCT.STRUCKID AND NVL(TUC.STRAILERID,'XXX') = NVL(TCT.STRAILERID,'XXX')
        //    WHERE CON.SVENDORID = '" + CommonFunction.ReplaceInjection(vehID) + @"' AND CON.CACTIVE = 'Y'
        //    GROUP BY  TUC.SHEADID,TUC.SHEADREGISTERNO,TUC.STRAILERREGISTERNO,TUC.DDATE,CON.SCONTRACTID,TCT.CSTANDBY,CON.SVENDORID
        //)";
        #endregion

        Session["dtCarRegis"] = PlanTransportBLL.Instance.GetTruckByAssign(Session["SVDID"] + string.Empty, Datestart, DateEnd);
        #region Session["dtDo"] ยกเลิกใช้
        //        string ConditionCAUSE = "";
        //        if (!string.IsNullOrEmpty(edtAssign.Text))
        //        {
        //            dte = ConvertToDateNull(edtAssign.Text);//เปลี่ยนมาใช้วัน Assign เพราะว่าถ้าเป็นงานขนส่งภายในวันไม่ต้องสนใจ DeliveryDate ให้สนวัน Assign แทน
        //            DateTime datestart = new DateTime();
        //            if (dte != null)
        //            {
        //                datestart = dte.Value;
        //            }
        //            ConditionCAUSE = " TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy') ";
        //        }


        //        string QUERY_DO = @"SELECT DELIVERY_NO 
        //FROM TSHIPMENT 
        //WHERE SHIPMET_STS > 5 AND DELIVERY_NO IN(
        //SELECT DISTINCT ODP.SDELIVERYNO
        //FROM TBL_ORDERPLAN ODP
        //LEFT JOIN 
        //(
        //    SELECT TPS.NPLANID,TPS.DPLAN,TPS.STRUCKID,TPL.SDELIVERYNO,TPL.ROUND,TPL.NVALUE,TPL.CACTIVE,TPS.SVENDORID  FROM TPLANSCHEDULE TPS
        //    LEFT JOIN TPLANSCHEDULELIST TPL
        //    ON TPS.NPLANID = TPL.NPLANID
        //    WHERE TPL.CACTIVE = '1' AND NVL(TPS.CFIFO,'XXX') <> '1'
        //)TPC
        //ON ODP.SDELIVERYNO = TPC.SDELIVERYNO AND  ODP.SVENDORID =  TPC.SVENDORID
        //WHERE 1=1 AND 
        //CASE 
        //--กรณีรถที่ไม่ได้จัดจะแสดงด้านบนโดยไม่สนไทป์ แต่วันต้องน้อยกว่าวันปัจจุบัน
        //WHEN ODP.CACTIVE = 'Y'  AND TRUNC(ODP.DATE_CREATE) <  " + ConditionCAUSE + @"   AND  NVL(TPC.STRUCKID,'xxx') = 'xxx' THEN '1' 
        //--กรณีวันปัจจุบันทั้งที่จัดแผนแล้วและยังไม่ได้จัดและไทป์เป็น 1 เสมอ
        //WHEN TRUNC(ODP.DATE_CREATE) =  " + ConditionCAUSE + @"  AND ODP.ORDERTYPE = '1' AND ODP.CACTIVE = 'Y'  THEN '1' 
        //--กรณีวันปัจจุบันย้อนหลัง 1 วัน โดยไทป์จะต้องเป็น 2 มาแสดง
        //WHEN ODP.ORDERTYPE = '2' AND ODP.CACTIVE = 'Y'  AND TRUNC(ODP.DATE_CREATE+1) =  " + ConditionCAUSE + @"   THEN '1' 
        //--กรณีเป็นวันที่ต้องขนส่งแต่ยังไม่ได้จัด
        //WHEN TRUNC(ODP.DDELIVERY) =  " + ConditionCAUSE + @" AND  NVL(TPC.STRUCKID,'xxx') = 'xxx'  AND ODP.CACTIVE = 'Y'  THEN '1' 
        //--กรณีวันปัจจุบันทั้งที่จัดแผนแล้วและยังไม่ได้จัดและไทป์เป็น 2 เสมอ 
        //WHEN ODP.ORDERTYPE = '2' AND ODP.CACTIVE = 'Y'  AND TRUNC(ODP.DATE_CREATE) =   " + ConditionCAUSE + @"  THEN '1' 
        //--กรณีวันล่วงหน้า 1 วัน โดยไทป์จะต้องเป็นไม่สนไทป์ให้นำมาแสดง
        //WHEN TRUNC(ODP.DATE_CREATE-1) = " + ConditionCAUSE + @"     AND  NVL(TPC.STRUCKID,'xxx') = 'xxx' AND ODP.CACTIVE = 'Y'  THEN '1' 
        //--กรณีเป็นวันที่โยนแผนล่วงหน้า และวันที่โยนมากกว่าวันปัจจุบัน และวันที่ขนส่งมากกว่าวันปัจจุบัน และยังไม่ได้มีการจัดแผน
        //WHEN TRUNC(ODP.DDELIVERY) > " + ConditionCAUSE + @" AND TRUNC(ODP.DATE_CREATE) > " + ConditionCAUSE + @"  AND ODP.CACTIVE = 'Y'  AND  NVL(TPC.STRUCKID,'xxx') = 'xxx' THEN '1' 
        //ELSE '' END = '1'
        //AND  ODP.SVENDORID = '" + CommonFunction.ReplaceInjection(vehID) + @"')
        //GROUP BY DELIVERY_NO -- HAVING MAX(SHIPMET_STS) > 5";

        //        DataTable dtDo = CommonFunction.Get_Data(conn, QUERY_DO);
        //        Session["dtDo"] = dtDo;
        #endregion

    }

    private void SETSCONTRACT()
    {
        string UserID = Session["UserID"] + "";
        string vehID = Session["SVDID"] + string.Empty;

        string QUERY = @" SELECT VEN.SVENDORID,VEN.SABBREVIATION,TCT.SCONTRACTID,TCT.SCONTRACTNO  FROM TVENDOR VEN
    INNER JOIN TCONTRACT TCT
    ON TCT.SVENDORID = VEN.SVENDORID
    INNER JOIN (SELECT SCONTRACTID FROM  TTRUCKCONFIRM GROUP BY SCONTRACTID ) TCF
    ON TCT.SCONTRACTID = TCF.SCONTRACTID
    WHERE VEN.SVENDORID = '" + vehID + "'  AND NVL(TCT.CACTIVE,'xxx')  = 'Y' ORDER BY TCT.SCONTRACTID";

        DataTable dt_CONTRACT = CommonFunction.Get_Data(conn, QUERY);

        cboSCONTRACT.DataSource = dt_CONTRACT;
        cboSCONTRACT.DataBind();
    }

    private string ITEMINAL(string Delivery, string TimeWindow, string Vehno, string Tuno, string Teminal, string PlanID, string PlanAdd, ref OracleConnection con)
    {
        string Error = "";
        if (string.IsNullOrEmpty(Delivery))
        {
            return "E;ไม่มี Do นี้";
        }
        else if (string.IsNullOrEmpty(TimeWindow))
        {
            return "E;ไม่มีเที่ยวการขนส่งนี้";
        }
        else if (string.IsNullOrEmpty(Vehno))
        {
            return "E;ไม่พบรถในระบบ";
        }
        else if (string.IsNullOrEmpty(Teminal))
        {
            return "E;ไม่พบคลัง";
        }
        else if (string.IsNullOrEmpty(PlanID))
        {
            return "E;ไม่พบแผนในระบบ";
        }
        else
        {
            string Result = "";
            #region Webservices iterminal
            DateTime outdate;
            DateTime dateplan = (DateTime.TryParseExact(PlanAdd, "d/M/yyyy HH.mm", new CultureInfo("th-TH"), DateTimeStyles.None, out outdate) ? outdate : DateTime.Now);

            iTerminal itm = new iTerminal();

            string USERID = Session["UserName"] + "";
            string CGROUP = Session["CGROUP"] + "";

            //itm.DeleteMAP(Delivery, TimeWindow, Vehno, Tuno, Teminal, "", PlanID, USERID, "", CGROUP, dateplan);
            Result = itm.UpdateMAP(Delivery, TimeWindow, Vehno, Tuno, Teminal, "", PlanID, USERID, "", CGROUP, dateplan);
            #endregion

            //Result = !string.IsNullOrEmpty(Result) ? Result.Substring(0, 1) : "";
            Result = !string.IsNullOrEmpty(Result) ? Result : "";


            return Result;
        }
    }

    private string ITEMINAL_DELETE(string Delivery, string TimeWindow, string Vehno, string Tuno, string Teminal, string PlanID, string PlanAdd, ref OracleConnection con)
    {
        string Error = "";
        if (string.IsNullOrEmpty(Delivery))
        {
            return "ไม่มี Do นี้";
        }
        else if (string.IsNullOrEmpty(TimeWindow))
        {
            return "ไม่มีเที่ยวการขนส่งนี้";
        }
        else if (string.IsNullOrEmpty(Vehno))
        {
            return "ไม่พบรถในระบบ";
        }
        else if (string.IsNullOrEmpty(Teminal))
        {
            return "ไม่พบคลัง";
        }
        else if (string.IsNullOrEmpty(PlanID))
        {
            return "ไม่พบแผนในระบบ";
        }
        else
        {
            string Result = "";
            #region Webservices iterminal
            DateTime outdate;
            DateTime dateplan = (DateTime.TryParseExact(PlanAdd, "d/M/yyyy HH.mm", new CultureInfo("th-TH"), DateTimeStyles.None, out outdate) ? outdate : DateTime.Now);

            iTerminal itm = new iTerminal();

            string USERID = Session["UserName"] + "";
            string CGROUP = Session["CGROUP"] + "";


            Result = itm.DeleteMAP(Delivery, TimeWindow, Vehno, Tuno, Teminal, "", PlanID, USERID, "", CGROUP, dateplan);
            #endregion

            //Result = !string.IsNullOrEmpty(Result) ? Result.Substring(0, 1) : "";
            Result = !string.IsNullOrEmpty(Result) ? Result : "";


            return Result;
        }
    }

    private string CheckDate(string _DDATE, string Type)
    {
        if (!string.IsNullOrEmpty(_DDATE))
        {
            string FormateDate = "";
            switch (Type)
            {
                case "V": FormateDate = "dd/MM/yyyy";
                    break;
                case "E": FormateDate = "yyyy-MM-dd";
                    break;
            }

            DateTime date;
            DateTime _FIFODATE = DateTime.TryParse(_DDATE, out date) ? date : DateTime.Now;
            //if (_FIFODATE.Year < 1500)
            //{
            //    //_FIFODATE = _FIFODATE.AddYears(543);
            //}
            //else
            //{
            //    _FIFODATE = _FIFODATE.AddYears(-543);
            //}

            return _FIFODATE.ToString(FormateDate);
        }
        else
        {
            return "";
        }
    }

    private string GenDOByTruck(string NPLANID, string SDELIVERYNO)
    {
        string QUERY_MULTIDROP = "SELECT NPLANID,SDELIVERYNO,NDROP FROM TPLANSCHEDULELIST WHERE NPLANID = '" + NPLANID + "' AND SDELIVERYNO <> '" + SDELIVERYNO + "' AND CACTIVE = '1' ORDER BY NDROP ASC";
        DataTable dt_ChkMultiDrop = CommonFunction.Get_Data(conn, QUERY_MULTIDROP);
        string DO = "";
        if (dt_ChkMultiDrop.Rows.Count > 0)
        {
            for (int i = 0; i < dt_ChkMultiDrop.Rows.Count; i++)
            {
                DO += "," + dt_ChkMultiDrop.Rows[i]["SDELIVERYNO"] + "";
            }
        }

        DO = !string.IsNullOrEmpty(DO) ? DO.Remove(0, 1) : "";
        return DO;
    }

    private int CheckStateSix(string SDELIVERY)
    {
        int result = 0;

        //        string DO_QUERY = @"SELECT * FROM(
        //SELECT (DELIVERY_NO) ,MAX(SHIPMET_STS) SHIPMET_STS
        // FROM TSHIPMENT  WHERE DELIVERY_NO IN( 
        //    SELECT DELIVERY_NO FROM TDELIVERY TD
        //    INNER JOIN TBL_ORDERPLAN OD
        //    ON TD.DELIVERY_NO = OD.SDELIVERYNO
        //    WHERE DELIVERY_NO ='" + SDELIVERY + @"'
        // ) 
        // GROUP BY (DELIVERY_NO)  
        // ) TTBL
        // WHERE SHIPMET_STS='6'";

        if (!string.IsNullOrEmpty(SDELIVERY))
        {
            string DO_QUERY = @"SELECT DELIVERY_NO 
FROM TSHIPMENT 
WHERE DELIVERY_NO IN(SELECT SDELIVERYNO FROM  TBL_ORDERPLAN WHERE SDELIVERYNO ='" + SDELIVERY + @"')
GROUP BY DELIVERY_NO  HAVING MAX(SHIPMET_STS) > 5";

            DataTable dt = CommonFunction.Get_Data(conn, DO_QUERY);
            if (dt.Rows.Count > 0)
            {
                result = dt.Rows.Count;
            }
        }

        return result;
    }

    private string GetCarpacity(string STRUCKID)
    {
        string Result = "";

        DataTable CHECK_CARTYPE = CommonFunction.Get_Data(conn, "SELECT SCARTYPEID,STRUCKID,STRAILERID FROM TTRUCK WHERE STRUCKID = '" + STRUCKID + "'");
        if (CHECK_CARTYPE.Rows.Count > 0)
        {
            string SQL = @"SELECT  STRUCKID,SUM(NCAPACITY) as NCAPACITY  FROM 
                            (
                                SELECT STRUCKID, NCOMPARTNO,MAX(NCAPACITY) as NCAPACITY
                                FROM TTRUCK_COMPART
                                WHERE STRUCKID = '{0}'
                                GROUP BY STRUCKID, NCOMPARTNO
                            )
                            GROUP BY   STRUCKID";
            DataTable dt_VALUE = new DataTable();
            switch (CHECK_CARTYPE.Rows[0]["SCARTYPEID"] + "")
            {
                case "0": dt_VALUE = CommonFunction.Get_Data(conn, string.Format(SQL, CHECK_CARTYPE.Rows[0]["STRUCKID"] + ""));

                    break;
                case "3": dt_VALUE = CommonFunction.Get_Data(conn, string.Format(SQL, CHECK_CARTYPE.Rows[0]["STRAILERID"] + ""));

                    break;
                case "4": dt_VALUE = CommonFunction.Get_Data(conn, string.Format(SQL, CHECK_CARTYPE.Rows[0]["STRUCKID"] + ""));

                    break;
            }

            if (dt_VALUE.Rows.Count > 0)
            {
                Result = dt_VALUE.Rows[0]["NCAPACITY"] + "";
            }

        }

        return Result;
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, conn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
    #endregion

    #region ListData3
    protected void gvw3_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        string UID = CommonFunction.ReplaceInjection(Session["UserID"] + "");

        switch (e.CallbackName)
        {
            case "PAGERONCLICK": ListData3();
                break;
            case "SORT": ListData3();
                break;
        }
    }
    private void ListData3()
    {
        string ConditionCAUSE = "";
        string Condition = "";
        string ConditionD = "";
        string DateShipment = "";
        string ConditionBack = "";
        if (!string.IsNullOrEmpty(edtAssign.Text))
        {
            DateTime? dte = ConvertToDateNull(edtAssign.Text);//เปลี่ยนมาใช้วัน Assign เพราะว่าถ้าเป็นงานขนส่งภายในวันไม่ต้องสนใจ DeliveryDate ให้สนวัน Assign แทน
            DateTime datestart = new DateTime();
            if (dte != null)
            {
                datestart = dte.Value;
            }
            //  DateTime dateend = DateTime.Parse(edtEnd.Value.ToString());
            //ConditionCAUSE = " AND (TRUNC(ODP.DATE_CREATE) = TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')) ";
            ConditionCAUSE = " TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')";
            ConditionBack += " AND (TRUNC(ODP.DATE_CREATE) = TO_DATE('" + CommonFunction.ReplaceInjection(datestart.AddDays(-1).ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')) ";
            //ConditionD += " AND (TRUNC(DATE_CREATE) = TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')) ";
            //DateShipment = " AND (TRUNC(DATE_CREATED) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.AddDays(-30).ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy') AND  TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')) ";
        }

        if (!string.IsNullOrEmpty(cboSelect.Text))
        {
            Condition += " AND ODP.STERMINALID = '" + CommonFunction.ReplaceInjection(cboSelect.Value + "") + "'";
        }

        if (!string.IsNullOrEmpty(cboSCONTRACT.Text))
        {
            Condition += " AND ODP.SCONTRACTID = '" + CommonFunction.ReplaceInjection(cboSCONTRACT.Value + "") + "'";
        }

        if (!string.IsNullOrEmpty(txtDNO.Text))
        {
            Condition += " AND ODP.SDELIVERYNO LIKE '%" + CommonFunction.ReplaceInjection(txtDNO.Text) + "%'";
        }
        string VENOR_ID = Session["SVDID"] + string.Empty;
        string QUERY = @"SELECT DISTINCT ODP.ORDERID, ODP.SVENDORID, ODP.SABBREVIATION, ODP.SCONTRACTID, ODP.SCONTRACTNO, 
CASE WHEN ODP.ORDERTYPE = '1' THEN 'งานขนส่งภายในวัน' ELSE 'งานขนส่งล่วงหน้า' END as TRAN_TYPE,
ODP.DDELIVERY, ODP.NWINDOWTIMEID, ODP.SDELIVERYNO,ODP.DATE_CREATE, ODP.ORDERTYPE,
ODP.SCREATE,ODP.DATE_UPDATE,ODP.SUPDATE, SHM.OTHER as NVALUE, ODP.STERMINALID,SHM.CUST_ABBR as STERMINALNAME 
,CASE 
WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = NVL(TO_CHAR(ODP.DDELIVERY),'xxx') OR TO_CHAR(ODP.DDELIVERY,'DAY') IN('เสาร์','อาทิตย์') THEN '1' 
WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> NVL(TO_CHAR(ODP.DDELIVERY),'xxx') THEN '0'
ELSE '' END AS CDAYTYPE -- เช็คว่า ถ้าไม่มีวันตรงกะวันหยุด และ CDAYTYPE = 0  ให้ค่าเป็น 1 และถ้า เป็นวันหยุด และ  CDAYTYPE = 1 ให้ค่าเป็น 1
,'เที่ยวที่ '||TWT.NLINE||' เวลา '||TWT.TSTART||'-'||TWT.TEND||' น.' as SWINDOWTIMENAME,TPC.ROUND,TPC.STRUCKID,TPC.DPLAN,
CASE WHEN NVL(TPC.STRUCKID,'xxx') <> 'xxx' THEN 1 ELSE 0 END as PLANADD
,CASE WHEN TPC.CACTIVE = '1' THEN 'จัดแผน' WHEN TPC.CACTIVE = '0' THEN 'ยกเลิกแผน' WHEN NVL(SHM.SHIP_TO,'xxx') <> 'xxx' AND NVL(TPC.STRUCKID,'xxx') = 'xxx' THEN '' ELSE '' END as STATUS
,CASE WHEN NVL(SHM.SHIP_TO,'xxx') <> 'xxx' THEN '0' ELSE  TPC.CACTIVE END as CANCEL--, NVL(SHM.SHIP_TO,'xxx')
,TT.SABBREVIATION as STERMINALNAME_FROM
FROM TBL_ORDERPLAN ODP
LEFT JOIN 
(
SELECT d.DELIVERY_NO,d.SPLNT_CODE,d.OTHER,C.CUST_ABBR,
    (CASE 
    WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0') 
    WHEN TRN.REV_CODE IS NOT NULL OR TBO.RECIEVEPOINT IS NOT NULL  THEN NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) ELSE ''  END)  
    ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END) SHIP_TO
    FROM 
    (
           --SELECT * FROM TDELIVERY
           --WHERE DELIVERY_NO IN (SELECT SDELIVERYNO FROM TBL_ORDERPLAN WHERE 1=1  AND SVENDORID = '" + VENOR_ID + @"'  AND (TRUNC(SYSDATE) - TRUNC(DDELIVERY)) < 31)
            SELECT LM.* FROM TDELIVERY LM
            INNER JOIN 
            (
                SELECT DELIVERY_NO,MAX(DATE_CREATED) as DATE_CREATED,MAX(DELIVERY_DATE) as DELIVERY_DATE FROM TDELIVERY 
WHERE  
            DELIVERY_NO IN (SELECT SDELIVERYNO FROM TBL_ORDERPLAN WHERE 1=1  AND CACTIVE = 'Y'  AND SVENDORID = '" + VENOR_ID + @"'  AND (TRUNC(SYSDATE) - TRUNC(DDELIVERY)) < 31)
                GROUP BY DELIVERY_NO
            )LL
            ON LM.DELIVERY_NO = LL.DELIVERY_NO AND LM.DATE_CREATED = LL.DATE_CREATED  AND LM.DELIVERY_DATE = LL.DELIVERY_DATE
            WHERE  
            LM.DELIVERY_NO IN (SELECT SDELIVERYNO FROM TBL_ORDERPLAN WHERE 1=1  AND CACTIVE = 'Y'  AND SVENDORID = '" + VENOR_ID + @"'  AND (TRUNC(SYSDATE) - TRUNC(DDELIVERY)) < 31)
    )d 
    LEFT JOIN 
    (
        --SELECT DISTINCT DOC_NO,SHIP_ID,REV_CODE FROM TRANS_ORDER   WHERE NVL(VALTYP,'-') !='REBRAND' 
                SELECT  LM.DOC_NO,LM.SHIP_ID,LM.REV_CODE FROM TRANS_ORDER   LM
                INNER JOIN 
                (
                    SELECT  DOC_NO,MAX(DBSYS_DATE) as DBSYS_DATE  FROM TRANS_ORDER  
                    WHERE NVL(VALTYP,'-') !='REBRAND' AND NVL(STATUS,'N') IN ('N','G')
                    GROUP BY DOC_NO
                )LL
                ON LM.DOC_NO = LL.DOC_NO AND LM.DBSYS_DATE = LL.DBSYS_DATE
                WHERE NVL(LM.VALTYP,'-') !='REBRAND' AND NVL(LM.STATUS,'N') IN ('N','G')
                GROUP BY  LM.DOC_NO,LM.SHIP_ID,LM.REV_CODE
    )TRN
    ON TRN.DOC_NO = d.SALES_ORDER
    LEFT JOIN 
    (
       -- SELECT DISTINCT DOCNO,RECIEVEPOINT FROM TBORDER WHERE RECIEVEPOINT IS NOT NULL
            SELECT  LM.DOCNO,LM.RECIEVEPOINT FROM TBORDER LM
            INNER JOIN
            (
                SELECT  DOCNO,MAX(UPDATEDATE) as UPDATEDATE FROM TBORDER 
                WHERE RECIEVEPOINT IS NOT NULL
                GROUP BY DOCNO
            )LL
            ON LM.DOCNO = LL.DOCNO AND LM.UPDATEDATE = LL.UPDATEDATE
            GROUP BY LM.DOCNO,LM.RECIEVEPOINT
    )TBO
    ON TBO.DOCNO = d.SALES_ORDER
    LEFT JOIN TCUSTOMER C ON  C.SHIP_TO =  (CASE  WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0')  WHEN TRN.REV_CODE IS NOT NULL OR TBO.RECIEVEPOINT IS NOT NULL  THEN NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) ELSE ''  END)   ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END) 
)SHM
ON SHM.DELIVERY_NO = ODP.SDELIVERYNO
LEFT JOIN LSTHOLIDAY LHD
ON TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy')  =  TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy')  
LEFT JOIN TWINDOWTIME TWT
ON TWT.STERMINALID =  ODP.STERMINALID AND TWT.NLINE = ODP.NWINDOWTIMEID AND TWT.CDAYTYPE = CASE 
WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = NVL(TO_CHAR(ODP.DDELIVERY),'xxx') OR TO_CHAR(ODP.DDELIVERY,'DAY') IN('เสาร์','อาทิตย์') THEN '1' 
WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> NVL(TO_CHAR(ODP.DDELIVERY),'xxx') THEN '0' ELSE '' END
LEFT JOIN 
(
    SELECT TPS.NPLANID,TPS.DPLAN,TPS.STRUCKID,TPL.SDELIVERYNO,TPL.ROUND,TPL.NVALUE,TPL.CACTIVE,TPS.SVENDORID  FROM TPLANSCHEDULE TPS
    LEFT JOIN TPLANSCHEDULELIST TPL
    ON TPS.NPLANID = TPL.NPLANID
    WHERE TPL.CACTIVE = '1' AND NVL(TPS.CFIFO,'XXX') <> '1'
)TPC
ON ODP.SDELIVERYNO = TPC.SDELIVERYNO AND  ODP.SVENDORID =  TPC.SVENDORID
LEFT JOIN TTERMINAL TT
ON TT.STERMINALID = SHM.SPLNT_CODE
WHERE 1=1 AND 
CASE 
--กรณีที่ DELIVERYDATE มากกว่าวันปัจจุบันหกโมงเช้าจะแสดงเนื่องจากจัดแผนช้า
WHEN  ODP.CACTIVE = 'Y'  
--เช็คว่าถ้านำวันที่ปัจจุบันมาลบกับวันที่ขนส่งแล้วมีค่ามากกว่า 1 ให้ใช้ SYSDATE เพราะ ถ้าใช้ DDELIVERY จะไม่มีทางที่เวลามากกว่าวันปัจจุบัน + 1
AND CASE WHEN (TRUNC(SYSDATE) - TRUNC(ODP.DDELIVERY)) > 0 THEN SYSDATE ELSE TO_DATE(ODP.DDELIVERY||TO_CHAR(SYSDATE, ' HH24:MI:SS'),'dd/MM/yyyy HH24:MI:SS') END  > TRUNC(ODP.DDELIVERY)+(1)+(6/24)
AND  NVL(TPC.STRUCKID,'xxx') = 'xxx' THEN '1' 
ELSE '' END = '1'
AND  ODP.SVENDORID = '" + VENOR_ID + @"'
AND (TRUNC(SYSDATE) - TRUNC(ODP.DDELIVERY)) < 31
ORDER BY PLANADD ASC, ODP.DDELIVERY ASC, ODP.DATE_CREATE ASC,ODP.NWINDOWTIMEID ASC,TPC.STRUCKID ASC,TPC.ROUND ASC";

        DataTable dtORDER = CommonFunction.Get_Data(conn, QUERY);
        if (dtORDER.Rows.Count > 0)
        {
            gvw3.DataSource = dtORDER;

        }
        gvw3.DataBind();

    }
    #endregion

    #region AutoComplete

    //    protected void cboTerminal_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    //    {

    //    }

    //    protected void cboTerminal_ItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    //    {

    //        ASPxComboBox comboBox = (ASPxComboBox)source;
    //        sdsTerminal.SelectCommand = @"SELECT STERMINALID , SABBREVIATION FROM (SELECT ROW_NUMBER()OVER(ORDER BY STERMINALID) AS RN , STERMINALID , SABBREVIATION
    //          FROM TTERMINAL WHERE STERMINALID||SABBREVIATION LIKE :fillter ) WHERE RN BETWEEN :startIndex AND :endIndex ";

    //        sdsTerminal.SelectParameters.Clear();
    //        sdsTerminal.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
    //        sdsTerminal.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
    //        sdsTerminal.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

    //        comboBox.DataSource = sdsTerminal;
    //        comboBox.DataBind();

    //    }

    protected void cboCarregisXc_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        if (comboBox.Text != "")
        {
            string TRUCK_TO_DATE = "";
            string[] index = comboBox.ClientID.Split('_');
            string Condition = "";
            if (index.Length > 0)
            {
                if (index[3] + "" == "gvw")
                {
                    int indexs = int.Parse(index[index.Length - 1]);
                    dynamic data = gvw.GetRowValues(indexs, "DDELIVERY");

                    if (!string.IsNullOrEmpty(data + ""))
                    {
                        DateTime dte = DateTime.Parse(data + "");
                        TRUCK_TO_DATE = CheckDate(dte.AddDays(-1).ToString(), "V");
                    }
                    Condition = "DDATE = '" + TRUCK_TO_DATE + "'";
                }
                else
                {
                    int indexs = int.Parse(index[index.Length - 1]);
                    dynamic data = gvw2.GetRowValues(indexs, "DDELIVERY");
                    if (!string.IsNullOrEmpty(data + ""))
                    {
                        DateTime dte = DateTime.Parse(data + "");
                        TRUCK_TO_DATE = CheckDate(dte.AddDays(-1).ToString(), "V");
                    }
                    Condition = "DDATE = '" + TRUCK_TO_DATE + "'";
                }
            }
            string UserID = Session["UserID"] + "";
            string VENDORID_G = Session["SVDID"] + string.Empty;
            DataTable dtCarRegis = Session["dtCarRegis"] as DataTable;
            DataView dv = new DataView(dtCarRegis);
            dv.RowFilter = Condition + " AND SVENDORID = '" + VENDORID_G + "'";
            //dv.RowFilter = "DDATE = '25/11/2014'";

            comboBox.DataSource = dv;
            comboBox.DataBind();
        }
    }

    protected void cboCarregisXc_ItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        string Condition = "";
        string TRUCK_TO_DATE = "";
        string[] index = comboBox.ClientID.Split('_');
        if (!string.IsNullOrEmpty(e.Filter))
        {
            if (index.Length > 0)
            {
                if (index[3] + "" == "gvw")
                {
                    int indexs = int.Parse(index[index.Length - 1]);
                    dynamic data = gvw.GetRowValues(indexs, "DDELIVERY");
                    if (!string.IsNullOrEmpty(data + ""))
                    {
                        DateTime dte = DateTime.Parse(data + "");
                        TRUCK_TO_DATE = CheckDate(dte.AddDays(-1).ToString(), "V");
                    }
                }
                else
                {
                    int indexs = int.Parse(index[index.Length - 1]);
                    dynamic data = gvw2.GetRowValues(indexs, "DDELIVERY");
                    if (!string.IsNullOrEmpty(data + ""))
                    {
                        DateTime dte = DateTime.Parse(data + "");
                        TRUCK_TO_DATE = CheckDate(dte.AddDays(-1).ToString(), "V");
                    }


                }
                Condition = " WHERE TO_CHAR(TCF.DDATE,'dd/MM/yyyy') = '" + TRUCK_TO_DATE + @"'";
            }
            string UserID = Session["UserID"] + "";
            string vehID = Session["SVDID"] + string.Empty;


            string QUERY = @"SELECT SHEADID , SHEADREGISTERNO,STRAILERREGISTERNO,CASE  WHEN  NVL(SHEADREGISTERNO,'xxx') <> 'xxx' THEN '/'||STRAILERREGISTERNO ELSE ' ' END as tail1 FROM
(
    SELECT ROW_NUMBER()OVER(ORDER BY TUC.SHEADID) AS RN ,TUC.SHEADID,TUC.SHEADREGISTERNO,TUC.STRAILERREGISTERNO FROM  TCONTRACT CON
    INNER JOIN
    (
        SELECT TCF.SCONTRACTID,TCFL.SHEADID,TCFL.SHEADREGISTERNO,TCFL.STRAILERREGISTERNO FROM TTRUCKCONFIRM TCF
        INNER JOIN 
        (
            SELECT TRKC.NCONFIRMID,TRKC.SHEADID,TRKH.SHEADREGISTERNO,TRKT.SHEADREGISTERNO as STRAILERREGISTERNO FROM TTRUCKCONFIRMLIST TRKC
            INNER JOIN TTRUCK TRKH
            ON TRKH.STRUCKID = TRKC.SHEADID
            LEFT JOIN TTRUCK TRKT
            ON TRKT.STRUCKID = TRKC.STRAILERID
            WHERE TRKC.CCONFIRM = '1'
            GROUP BY TRKC.NCONFIRMID,TRKC.SHEADID,TRKH.SHEADREGISTERNO,TRKT.SHEADREGISTERNO
        ) TCFL
        ON TCFL.NCONFIRMID = TCF.NCONFIRMID AND TCF.CCONFIRM = '1'
        " + Condition + @"
        GROUP BY TCF.SCONTRACTID,TCFL.SHEADID,TCFL.SHEADREGISTERNO,TCFL.STRAILERREGISTERNO
    )TUC
    ON TUC.SCONTRACTID = CON.SCONTRACTID
    WHERE CON.SVENDORID = '" + CommonFunction.ReplaceInjection(vehID) + @"' AND CON.CACTIVE = 'Y' AND TUC.SHEADID||TUC.SHEADREGISTERNO||TUC.STRAILERREGISTERNO LIKE '%" + e.Filter + @"%'
    GROUP BY  TUC.SHEADID,TUC.SHEADREGISTERNO,TUC.STRAILERREGISTERNO
)WHERE RN BETWEEN " + (e.BeginIndex + 1).ToString() + " AND " + (e.EndIndex + 1).ToString() + "";


            DataTable dtss = CommonFunction.Get_Data(conn, QUERY);


            comboBox.DataSource = dtss;
            comboBox.DataBind();

        }

        //sdsCarregis.SelectParameters.Clear();
        //sdsCarregis.SelectParameters.Add(":startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        //sdsCarregis.SelectParameters.Add(":endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());
        //sdsCarregis.SelectParameters.Add(":fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        //comboBox.DataSource = sdsCarregis;
        //comboBox.DataBind();
    }

    protected void cboCarregis_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        if (comboBox.Text != "")
        {
            DataTable dtCarRegis = Session["dtCarRegis"] as DataTable;
            comboBox.DataSource = dtCarRegis;
            comboBox.DataBind();
        }
    }

    protected void cboCarregis_ItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        string UserID = Session["UserID"] + "";
        string vehID = Session["SVDID"] + string.Empty;
        // string vehID = "0010001778";
        DateTime? dte = ConvertToDateNull(edtAssign.Text);
        string DATESEARCH = string.Empty;
        if (dte != null)
        {
            DATESEARCH = CheckDate(dte.Value.ToString(), "V");
        }
        //string DATESEARCH = CheckDate(edtAssign.Text, "V");
        ASPxComboBox comboBox = (ASPxComboBox)source;

        string QUERY = @"SELECT SHEADID , SHEADREGISTERNO,STRAILERREGISTERNO,CASE  WHEN  NVL(SHEADREGISTERNO,'xxx') <> 'xxx' THEN '/'||STRAILERREGISTERNO ELSE ' ' END as tail1 FROM
(
    SELECT ROW_NUMBER()OVER(ORDER BY TUC.SHEADID) AS RN ,TUC.SHEADID,TUC.SHEADREGISTERNO,TUC.STRAILERREGISTERNO FROM  TCONTRACT CON
    INNER JOIN
    (
        SELECT TCF.SCONTRACTID,TCFL.SHEADID,TCFL.SHEADREGISTERNO,TCFL.STRAILERREGISTERNO FROM TTRUCKCONFIRM TCF
        INNER JOIN 
        (
            SELECT TRKC.NCONFIRMID,TRKC.SHEADID,TRKH.SHEADREGISTERNO,TRKT.SHEADREGISTERNO as STRAILERREGISTERNO FROM TTRUCKCONFIRMLIST TRKC
            INNER JOIN TTRUCK TRKH
            ON TRKH.STRUCKID = TRKC.SHEADID
            LEFT JOIN TTRUCK TRKT
            ON TRKT.STRUCKID = TRKC.STRAILERID
            WHERE TRKC.CCONFIRM = '1'
            GROUP BY TRKC.NCONFIRMID,TRKC.SHEADID,TRKH.SHEADREGISTERNO,TRKT.SHEADREGISTERNO
        ) TCFL
        ON TCFL.NCONFIRMID = TCF.NCONFIRMID AND TCF.CCONFIRM = '1'
        WHERE TO_CHAR(TCF.DDATE+1,'dd/MM/yyyy') = '" + DATESEARCH + @"'
        GROUP BY TCF.SCONTRACTID,TCFL.SHEADID,TCFL.SHEADREGISTERNO,TCFL.STRAILERREGISTERNO
    )TUC
    ON TUC.SCONTRACTID = CON.SCONTRACTID
    WHERE CON.SVENDORID = '" + CommonFunction.ReplaceInjection(vehID) + @"' AND CON.CACTIVE = 'Y' AND TUC.SHEADID||TUC.SHEADREGISTERNO||TUC.STRAILERREGISTERNO LIKE '%" + e.Filter + @"%'
    GROUP BY  TUC.SHEADID,TUC.SHEADREGISTERNO,TUC.STRAILERREGISTERNO
)WHERE RN BETWEEN " + (e.BeginIndex + 1).ToString() + " AND " + (e.EndIndex + 1).ToString() + "";

        DataTable dtss = CommonFunction.Get_Data(conn, QUERY);
        comboBox.DataSource = dtss;
        comboBox.DataBind();
    }

    //    protected void cboCarregis_ItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    //    {

    //        // string vehID = SystemFunction.GET_VENDORID(UserID);
    //        string vehID = "0010001778";

    //        ASPxComboBox comboBox = (ASPxComboBox)source;

    //        sdsCarregis.SelectCommand = @"SELECT SHEADID , SHEADREGISTERNO,STRAILERREGISTERNO,CASE  WHEN  NVL(SHEADREGISTERNO,'xxx') <> 'xxx' THEN '/'||STRAILERREGISTERNO ELSE ' ' END as tail1 FROM
    //(
    //    SELECT ROW_NUMBER()OVER(ORDER BY TUC.SHEADID) AS RN ,TUC.SHEADID,TUC.SHEADREGISTERNO,TUC.STRAILERREGISTERNO FROM  TCONTRACT CON
    //    INNER JOIN
    //    (
    //        SELECT TCF.SCONTRACTID,TCFL.SHEADID,TCFL.SHEADREGISTERNO,TCFL.STRAILERREGISTERNO FROM TTRUCKCONFIRM TCF
    //        INNER JOIN 
    //        (
    //            SELECT TRKC.NCONFIRMID,TRKC.SHEADID,TRKH.SHEADREGISTERNO,TRKT.SHEADREGISTERNO as STRAILERREGISTERNO FROM TTRUCKCONFIRMLIST TRKC
    //            INNER JOIN TTRUCK TRKH
    //            ON TRKH.STRUCKID = TRKC.SHEADID
    //            INNER JOIN TTRUCK TRKT
    //            ON TRKT.STRUCKID = TRKC.STRAILERID
    //            WHERE TRKC.CCONFIRM = '1'
    //            GROUP BY TRKC.NCONFIRMID,TRKC.SHEADID,TRKH.SHEADREGISTERNO,TRKT.SHEADREGISTERNO
    //        ) TCFL
    //        ON TCFL.NCONFIRMID = TCF.NCONFIRMID AND TCF.CCONFIRM = '1'
    //        GROUP BY TCF.SCONTRACTID,TCFL.SHEADID,TCFL.SHEADREGISTERNO,TCFL.STRAILERREGISTERNO
    //    )TUC
    //    ON TUC.SCONTRACTID = CON.SCONTRACTID
    //    WHERE CON.SVENDORID = '" + CommonFunction.ReplaceInjection(vehID) + @"' AND CON.CACTIVE = 'Y' AND TUC.SHEADID||TUC.SHEADREGISTERNO||TUC.STRAILERREGISTERNO LIKE :fillter
    //    GROUP BY  TUC.SHEADID,TUC.SHEADREGISTERNO,TUC.STRAILERREGISTERNO
    //)
    //WHERE RN BETWEEN :startIndex AND :endIndex";

    //        sdsCarregis.SelectParameters.Clear();
    //        sdsCarregis.SelectParameters.Add(":startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
    //        sdsCarregis.SelectParameters.Add(":endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());
    //        sdsCarregis.SelectParameters.Add(":fillter", TypeCode.String, String.Format("%{0}%", e.Filter));

    //        //if (!string.IsNullOrEmpty(comboBox.Text))
    //        //{
    //        //if(sdsCarregis.c)
    //        comboBox.DataSource = sdsCarregis;
    //        comboBox.DataBind();
    //        //}
    //    }

    protected void cboSCONTRACT_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    protected void cboSCONTRACT_ItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        string UserID = Session["UserID"] + "";
        string vehID = Session["SVDID"] + string.Empty;
        // string vehID = "0010001778";

        ASPxComboBox comboBox = (ASPxComboBox)source;
        comboBox.Items.Clear();
        SqlDataSource1.SelectCommand = @"SELECT SCONTRACTID,SCONTRACTNO FROM
    (
    SELECT ROW_NUMBER()OVER(ORDER BY TUC.SCONTRACTID) AS RN ,TUC.SCONTRACTID,CON.SCONTRACTNO FROM  TCONTRACT CON
    INNER JOIN
    (
        SELECT TCF.SCONTRACTID,TCFL.SHEADID,TCFL.SHEADREGISTERNO,TCFL.STRAILERREGISTERNO FROM TTRUCKCONFIRM TCF
        INNER JOIN 
        (
            SELECT TRKC.NCONFIRMID,TRKC.SHEADID,TRKH.SHEADREGISTERNO,TRKT.SHEADREGISTERNO as STRAILERREGISTERNO FROM TTRUCKCONFIRMLIST TRKC
            INNER JOIN TTRUCK TRKH
            ON TRKH.STRUCKID = TRKC.SHEADID
            INNER JOIN TTRUCK TRKT
            ON TRKT.STRUCKID = TRKC.STRAILERID
            WHERE TRKC.CCONFIRM = '1'
            GROUP BY TRKC.NCONFIRMID,TRKC.SHEADID,TRKH.SHEADREGISTERNO,TRKT.SHEADREGISTERNO
        ) TCFL
        ON TCFL.NCONFIRMID = TCF.NCONFIRMID AND TCF.CCONFIRM = '1'
        GROUP BY TCF.SCONTRACTID,TCFL.SHEADID,TCFL.SHEADREGISTERNO,TCFL.STRAILERREGISTERNO
    )TUC
    ON TUC.SCONTRACTID = CON.SCONTRACTID

    WHERE CON.SVENDORID = '" + CommonFunction.ReplaceInjection(vehID) + @"' AND CON.CACTIVE = 'Y' AND TUC.SCONTRACTID||TUC.STRAILERREGISTERNO LIKE :fillter
    GROUP BY TUC.SCONTRACTID,CON.SCONTRACTNO,CON.SVENDORID
)
WHERE RN BETWEEN :startIndex AND :endIndex";

        SqlDataSource1.SelectParameters.Clear();
        SqlDataSource1.SelectParameters.Add(":startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        SqlDataSource1.SelectParameters.Add(":endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());
        SqlDataSource1.SelectParameters.Add(":fillter", TypeCode.String, String.Format("%{0}%", e.Filter));

        //if (!string.IsNullOrEmpty(comboBox.Text))
        //{
        //if(sdsCarregis.c)
        comboBox.DataSource = SqlDataSource1;
        comboBox.DataBind();
        //}

    }
    #endregion

    #region class
    [Serializable]

    class TData
    {
        public string STRUCKID { get; set; }
        public decimal Capacity { get; set; }
        public string ROUND { get; set; }
        public DateTime DODATE { get; set; }
        public string TIMEWINDOW { get; set; }
    }

    #endregion
    protected void xcpnCheckLate_Callback(object source, CallbackEventArgs e)
    {
        string paras = CommonFunction.ReplaceInjection(e.Parameter);
        int Inx = int.Parse(paras);
        ASPxGridView gv = gvw;
        ASPxLabel plan = lblPlan, planall = lblPlanAll;
        if (pageControl.ActiveTabIndex == 1)
        {
            plan = lblPlan2;
            planall = lblPlanAll2;
            gv = gvw2;
        }

        ASPxComboBox gvwcboCarregis = gv.FindRowCellTemplateControl(Inx, null, "gvwcboCarregis") as ASPxComboBox;
        dynamic data = gv.GetRowValues(Inx, "SDELIVERYNO", "STERMINALID", "SHIP_TO", "LATEDATE", "LATETIME", "FULLNAME", "SEMAIL", "ORDERID", "DATETIMEEXPECT");

        ASPxLabel txtLate = gv.FindRowCellTemplateControl(Inx, null, "txtLate") as ASPxLabel;
        try
        {
            string SHEADREGISTERNO = string.Empty;
            if (!string.IsNullOrEmpty(gvwcboCarregis.Text))
            {
                string[] str = gvwcboCarregis.Text.Split(' ');
                if (str.Any())
                {
                    SHEADREGISTERNO = str[0];
                }
            }
            if (!string.IsNullOrEmpty(data[3] + string.Empty))
            {

                DateTime Latetime = Convert.ToDateTime(data[3] + string.Empty);
                if (!string.IsNullOrEmpty(data[4] + string.Empty))
                {
                    string[] str = (data[4] + string.Empty).Split('.');
                    if (str.Any())
                    {
                        Latetime = Latetime.AddHours(int.Parse(str[0]));
                        Latetime = Latetime.AddMinutes(int.Parse(str[1]));
                    }
                }
                string mess = PlanTransportBLL.Instance.CheckLateFormIVMS(connIVMS, data[0] + string.Empty, data[1] + string.Empty, (data[2] + string.Empty).Replace("000009", ""), SHEADREGISTERNO, Latetime.ToString("dd/MM/yyyy HH:mm"), data[5] + string.Empty, data[6] + string.Empty, data[7] + string.Empty);


                if (!string.IsNullOrEmpty(mess))
                {
                    string color = "#000";
                    if (!string.IsNullOrEmpty(data[8] + string.Empty))
                    {
                        DateTime? DATETIMEEXPECT = null, DATETIMETOTERMINAL = null;
                        string[] strDATETIMEEXPECT = (data[8] + string.Empty).Trim().Split(' ');
                        string[] strDATETIMETOTERMINAL = mess.Trim().Split(' ');
                        string[] strDATETIMEEXPECTDate, strDATETIMEEXPECTTime, strDATETIMETOTERMINALDate, strDATETIMETOTERMINALTime;
                        if (strDATETIMEEXPECT.Any())
                        {
                            strDATETIMEEXPECTDate = strDATETIMEEXPECT[0].Split('/');
                            strDATETIMEEXPECTTime = strDATETIMEEXPECT[1].Split(':');
                            DATETIMEEXPECT = new DateTime(int.Parse(strDATETIMEEXPECTDate[2]), int.Parse(strDATETIMEEXPECTDate[1]), int.Parse(strDATETIMEEXPECTDate[0]), int.Parse(strDATETIMEEXPECTTime[0]), int.Parse(strDATETIMEEXPECTTime[1]), 0);
                        }
                        if (strDATETIMETOTERMINAL.Any())
                        {
                            strDATETIMETOTERMINALDate = strDATETIMETOTERMINAL[0].Split('/');
                            strDATETIMETOTERMINALTime = strDATETIMETOTERMINAL[1].Split(':');
                            DATETIMETOTERMINAL = new DateTime(int.Parse(strDATETIMETOTERMINALDate[2]), int.Parse(strDATETIMETOTERMINALDate[1]), int.Parse(strDATETIMETOTERMINALDate[0]), int.Parse(strDATETIMETOTERMINALTime[0]), int.Parse(strDATETIMETOTERMINALTime[1]), 0);
                        }
                        if (DATETIMEEXPECT != null && DATETIMETOTERMINAL != null)
                        {
                            if (DATETIMEEXPECT > DATETIMETOTERMINAL)
                            {
                                color = "#1A15EC";
                            }
                            else
                            {
                                color = "#FF0A0A";
                            }
                        }
                    }

                    string messBefore = PlanTransportBLL.Instance.CheckLateFormIVMSBefore(connIVMS, data[0] + string.Empty, data[1] + string.Empty, (data[2] + string.Empty).Replace("000009", ""), SHEADREGISTERNO, Latetime.ToString("dd/MM/yyyy HH:mm"), data[5] + string.Empty, data[6] + string.Empty, data[7] + string.Empty);

                    switch (messBefore.ToUpper())
                    {
                        case "OK": messBefore = "รถทะเบียน " + gvwcboCarregis.Text + " มีโอกาสขนส่งถึงปลายทางได้ทันตามกำหนด"; break;
                        case "LATE TIME": messBefore = "รถทะเบียน " + gvwcboCarregis.Text + " มีโอกาสขนส่งถึงปลายทางล่าช้ากว่ากำหนด"; break;
                        case "VEHICLE NOT SUPPORT": messBefore = "ไม่มีทะเบียนรถ " + gvwcboCarregis.Text + " ในระบบ"; break;
                        case "ROUTE NOT SUPPORT": messBefore = "ไม่มีเส้นทางการขนส่งนี้ ในระบบ IVMS"; break;
                        case "MDVR IS UNAVAILABLE": messBefore = "ไม่มี MDVR ในระบบ IVMS"; break;
                        default:
                            messBefore = "ไม่พบข้อมูลในระบบ IVMS";
                            break;
                    }

                    e.Result = "รถทะเบียน " + gvwcboCarregis.Text + " จะถึงคลังต้นทางในวันที่ " + mess + ";" + mess + ";" + color + ";" + messBefore;
                }
                else
                {
                    e.Result = "ไม่มี Route ไม่มีรถในระบบ IVMS";
                }


            }

        }
        catch (Exception ex)
        {
            e.Result = "ERROR IVMS Message : " + RemoveSpecialCharacters(ex.Message);
        }
    }
}