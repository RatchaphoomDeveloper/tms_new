﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OracleClient;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxClasses.Internal;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;

public partial class terminal_checkproduct : System.Web.UI.Page
{
    int defaultInt = 0; double defDouble = 0.00;
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString);
    const string UploadDirectory = "~/UploadFile/EvidenceOfChecks/";
    const int ThumbnailSize = 100;
    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler
        #endregion
        if (Session["UserID"] == null || Session["UserID"] + "" == "") { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }
        if (!IsPostBack)
        {
            bool chkurl = false;
            Session["CheckPermission"] = null;
            string AddEdit = "";

            if (Session["cPermission"] != null)
            {
                string[] url = (Session["cPermission"] + "").Split('|');
                string[] chkpermision;
                bool sbreak = false;

                foreach (string inurl in url)
                {
                    chkpermision = inurl.Split(';');
                    if (chkpermision[0] == "15")
                    {
                        switch (chkpermision[1])
                        {
                            case "0":
                                chkurl = false;

                                break;
                            case "1":
                                chkurl = true;

                                break;

                            case "2":
                                chkurl = true;
                                AddEdit = "1";
                                break;
                        }
                        sbreak = true;
                    }

                    if (sbreak == true) break;
                }
            }

            if (chkurl == false)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            }
            Session["CheckPermission"] = "1";
            Session["ss_data"] = null; Session["SCHECKID"] = null;
            Session["IssueData"] = null; Session["UploadMode"] = null;
            cboTerminal.DataBind();
            cboTerminal.Items.Insert(0, new ListEditItem("ระบุคลังน้ำมันปลายทาง", ""));
            cboTerminal.Value = (Session["SVDID"].ToString().Length > 5) ? "" : "" + Session["SVDID"];
            dteStart.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();

            Cache.Remove(sdsCheckLists.CacheKeyDependency);
            Cache[sdsCheckLists.CacheKeyDependency] = new object();
            sdsCheckLists.Select(new System.Web.UI.DataSourceSelectArguments());

            Cache.Remove(sdsCheckListsProd.CacheKeyDependency);
            Cache[sdsCheckListsProd.CacheKeyDependency] = new object();
            sdsCheckListsProd.Select(new System.Web.UI.DataSourceSelectArguments());

            BindData();

            LogUser("15", "R", "เปิดดูข้อมูลหน้า ตรวจสภาพรถ-สินค้าคลังปลายทาง", "");
        }
    }
    #region Events
    //CallbackPanel
    protected void xcpn_Load(object sender, EventArgs e)
    {


    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        string[] parameters = paras[0].Split('$');
        string mode = "";
        mode = (paras[0].StartsWith("complain") || paras[0].StartsWith("carban") || paras[0].StartsWith("CHECKTRUCK")) ? "goto" : paras[0] + "";

        switch (mode)
        {
            case "goto":
                mode = parameters[0] + "";
                break;
        }
        switch (mode)
        {
            case "Search":
                Session["UploadMode"] = null; Session["SCHECKID"] = null;
                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();

                Cache.Remove(sdsCheckLists.CacheKeyDependency);
                Cache[sdsCheckLists.CacheKeyDependency] = new object();
                sdsCheckLists.Select(new System.Web.UI.DataSourceSelectArguments());

                Cache.Remove(sdsCheckListsProd.CacheKeyDependency);
                Cache[sdsCheckListsProd.CacheKeyDependency] = new object();
                sdsCheckListsProd.Select(new System.Web.UI.DataSourceSelectArguments());

                Session["ss_data"] = null;
                Session["IssueData"] = null;
                BindData();
                gvw.CancelEdit();
                break;
            case "senddata":
                if ("" + Session["CheckPermission"] == "1")
                {
                    if (Session["ss_data"] == null)
                    {
                        for (int nGridRows = 0; nGridRows < gvw.VisibleRowCount; nGridRows++)
                        {
                            if (gvw.Selection.IsRowSelected(nGridRows))
                            {
                                dynamic griddata = gvw.GetRowValues(nGridRows, "NPLANID", "SPLANLISTID", "SDELIVERYNO", "DDELIVERY", "STIMEWINDOW", "SHEADID", "SHEADREGISTERNO", "STRAILID", "STRAILERREGISTERNO", "NVALUE", "CPASSED", "CCONFIRM", "NDROP", "CCHECKTRUCKA");

                                using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                                {
                                    if (con.State == ConnectionState.Closed) con.Open();
                                    OracleCommand ora_cmd = new OracleCommand("UPDATE TPlanScheduleList SET CCHECKTRUCKB='1',CCHECKPRODUCT='1' WHERE NPLANID=:N_PLANID AND SPLANLISTID=:S_PLANLISTID AND NDROP=:N_DROP AND LPAD(SDELIVERYNO,10,'0')=:S_DELIVERYNO", con);
                                    ora_cmd.Parameters.Add(":N_PLANID", OracleType.VarChar).Value = "" + griddata[0];
                                    ora_cmd.Parameters.Add(":S_PLANLISTID", OracleType.VarChar).Value = "" + griddata[1];
                                    ora_cmd.Parameters.Add(":N_DROP", OracleType.VarChar).Value = "" + griddata[12];
                                    ora_cmd.Parameters.Add(":S_DELIVERYNO", OracleType.VarChar).Value = "" + griddata[2];
                                    ora_cmd.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                    else
                    {
                        DataTable dt_data = (DataTable)Session["ss_data"];

                        for (int nGridRows = 0; nGridRows < gvw.VisibleRowCount; nGridRows++)
                        {
                            if (gvw.Selection.IsRowSelected(nGridRows))
                            {
                                dynamic griddata = gvw.GetRowValues(nGridRows, "NPLANID", "SPLANLISTID", "SDELIVERYNO", "DDELIVERY", "STIMEWINDOW", "SHEADID", "SHEADREGISTERNO", "STRAILID", "STRAILERREGISTERNO", "NVALUE", "CPASSED", "CCONFIRM", "NDROP", "CCHECKTRUCKA");
                                DataRow[] drdata = dt_data.Select("NPLANID='" + griddata[0] + "' AND SPLANLISTID='" + griddata[1] + "' AND LPAD(SDELIVERYNO,10,'0')='" + griddata[2] + "' AND NDROP='" + griddata[12] + "'");
                                if (drdata.Length > 0)
                                {
                                    using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                                    {
                                        if (con.State == ConnectionState.Closed) con.Open();
                                        foreach (DataRow dr_data in drdata)
                                        {
                                            OracleCommand ora_cmd = new OracleCommand("UPDATE TPlanScheduleList SET CCOMMIT=:C_PERMIT ,CCHECKTRUCKB='1' WHERE NPLANID=:N_PLANID AND SPLANLISTID=:S_PLANLISTID AND NDROP=:N_DROP AND SDELIVERYNO=:S_DELIVERYNO", con);
                                            ora_cmd.Parameters.Add(":N_PLANID", OracleType.VarChar).Value = "" + griddata[0];
                                            ora_cmd.Parameters.Add(":C_PERMIT", OracleType.VarChar).Value = "" + dr_data["CPASSED"];
                                            ora_cmd.Parameters.Add(":S_PLANLISTID", OracleType.VarChar).Value = "" + griddata[1];
                                            ora_cmd.Parameters.Add(":N_DROP", OracleType.VarChar).Value = "" + griddata[12];
                                            ora_cmd.Parameters.Add(":S_DELIVERYNO", OracleType.VarChar).Value = "" + griddata[2];
                                            ora_cmd.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    LogUser("15", "I", "บันทึกข้อมูลหน้า ตรวจสภาพรถ-สินค้าคลังปลายทาง", "");
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');"); return;
                }
                BindData();
                break;
            case "complain":
                string complain_getmethod = "{0}&{1}&{2}&{3}&{4}&{5}&{6}&{7}&{8}&{9}&{10}&{11}", sEncrypt = "&&&&&&&&&&&";

                dynamic val_complain = gvw.GetRowValues(int.Parse(parameters[1] + ""), "NPLANID", "SPLANLISTID", "NDROP", "SDELIVERYNO", "DDELIVERY", "SHEADID", "SHEADREGISTERNO", "STRAILID", "STRAILERREGISTERNO", "SVENDORID", "STERMINALID");
                using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                {
                    DataTable dtval_complain = CommonFunction.Get_Data(con, @"SELECT PLNLST.SDELIVERYNO, PLN.DDELIVERY ,PLN.SHEADREGISTERNO ,PLN.STRAILERREGISTERNO ,TRCK.STRUCKID ,CONT_TRCK.SCONTRACTID ,PLN.SEMPLOYEEID ,EMPL.SPERSONELNO ,CONT.SVENDORID
FROM TPLANSCHEDULE PLN
LEFT JOIN TPlanScheduleList PLNLST ON PLN.NPLANID=PLNLST.NPLANID
LEFT JOIN TTRUCK TRCK ON PLN.SHEADREGISTERNO= TRCK.SHEADREGISTERNO
LEFT JOIN TCONTRACT_TRUCK CONT_TRCK ON CONT_TRCK.STRUCKID=TRCK.STRUCKID
LEFT JOIN TCONTRACT CONT ON CONT_TRCK.SCONTRACTID=CONT.SCONTRACTID
LEFT JOIN TEMPLOYEE EMPL ON PLN.SEMPLOYEEID=EMPL.SEMPLOYEEID
WHERE PLN.CACTIVE = '1' AND PLNLST.CACTIVE = '1' AND PLN.NPLANID='" + val_complain[0] + "'  AND PLNLST.SDELIVERYNO='" + val_complain[3] + "'");

                    if (dtval_complain.Rows.Count > 0)
                    {//NPLANID ,SDELIVERYNO ,DDELIVERY ,SHEADREGISTERNO ,STRAILERREGISTERNO  ,STRUCKID   ,SCONTRACTID ,SEMPLOYEEID ,SPERSONELNO ,SVENDORID
                        sEncrypt = string.Format(complain_getmethod
                            , "" + val_complain[0]
                            , "" + val_complain[3]
                            , "" + val_complain[4]
                            , "" + val_complain[6]
                            , "" + val_complain[8]
                            , "" + val_complain[5]
                            , "" + val_complain[7]
                            , "" + dtval_complain.Rows[0]["SCONTRACTID"]
                            , "" + dtval_complain.Rows[0]["SEMPLOYEEID"]
                            , "" + dtval_complain.Rows[0]["SPERSONELNO"]
                            , "" + val_complain[9]
                            , "" + val_complain[10]
                            );
                    }
                    //ClientScript.RegisterStartupScript(this.GetType(), "goto_complain", "<script>window.location='admin_Complain_lst.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt(sEncrypt)) + "';</script>");
                    xcpn.JSProperties["cpRedirectTo"] = "admin_Complain_add.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt(sEncrypt));
                    //CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='admin_ChkList_lst.aspx';});");
                    //Response.Redirect("admin_Complain_lst.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt(sEncrypt)));
                }
                break;
            case "carban":
                string carban_getmethod = "{0}&{1}&{2}&{3}&{4}&{5}&{6}&{7}&{8}&{9}&{10}&{11}", strEncrypt = "&&&&&&&&&&&";

                dynamic val_carban = gvw.GetRowValues(int.Parse(parameters[1] + ""), "NPLANID", "SPLANLISTID", "NDROP", "SDELIVERYNO", "DDELIVERY", "SHEADID", "SHEADREGISTERNO", "STRAILID", "STRAILERREGISTERNO", "SVENDORID", "STERMINALID");
                using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                {
                    DataTable dtval_carban = CommonFunction.Get_Data(con, @"SELECT PLNLST.SDELIVERYNO, PLN.DDELIVERY ,PLN.SHEADREGISTERNO ,PLN.STRAILERREGISTERNO ,TRCK.STRUCKID ,CONT_TRCK.SCONTRACTID ,PLN.SEMPLOYEEID ,EMPL.SPERSONELNO
,DELI.SHIP_TO,CUST.DEPO_ID
FROM TPLANSCHEDULE PLN
LEFT JOIN TPlanScheduleList PLNLST ON PLN.NPLANID=PLNLST.NPLANID
LEFT JOIN TTRUCK TRCK ON PLN.SHEADREGISTERNO= TRCK.SHEADREGISTERNO
LEFT JOIN TCONTRACT_TRUCK CONT_TRCK ON CONT_TRCK.STRUCKID=TRCK.STRUCKID
LEFT JOIN TEMPLOYEE EMPL ON PLN.SEMPLOYEEID=EMPL.SEMPLOYEEID
LEFT JOIN TDELIVERY DELI ON PLNLST.SDELIVERYNO=DELI.DELIVERY_NO
LEFT JOIN TCUSTOMER CUST ON DELI.SHIP_TO=CUST.SHIP_TO
WHERE PLN.CACTIVE = '1' AND PLNLST.CACTIVE = '1' AND PLN.NPLANID='" + val_carban[0] + "'  AND PLNLST.SDELIVERYNO='" + val_carban[3] + "'");

                    if (dtval_carban.Rows.Count > 0)
                    {// "NPLANID", "SDELIVERYNO", "DDELIVERY", "SHEADID", "SHEADREGISTERNO", "STRAILID", "STRAILERREGISTERNO","SCONTRACTID" ,"SEMPLOYEEID" ,"SPERSONELNO", "SVENDORID","DEPO_ID"
                        strEncrypt = string.Format(carban_getmethod
                            , "" + val_carban[0]
                            , "" + val_carban[3]
                            , "" + val_carban[4]
                            , "" + val_carban[6]
                            , "" + val_carban[8]
                            , "" + val_carban[5]
                            , "" + val_carban[7]
                            , "" + dtval_carban.Rows[0]["SCONTRACTID"]
                            , "" + dtval_carban.Rows[0]["SEMPLOYEEID"]
                            , "" + dtval_carban.Rows[0]["SPERSONELNO"]
                            , "" + val_carban[9]
                            , "" + dtval_carban.Rows[0]["DEPO_ID"]
                            );
                    }
                    xcpn.JSProperties["cpRedirectTo"] = "depo_terminal_remain_add.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt(strEncrypt));
                }
                break;

            case "CHECKTRUCK":
                if ("" + Session["CheckPermission"] == "1")
                {
                    dynamic dataCheck = gvw.GetRowValues(Convert.ToInt32(parameters[1] + ""), "SHEADID", "SHEADREGISTERNO", "STRAILERREGISTERNO", "DDELIVERY", "NPLANID");

                    string SCONTRACT = CommonFunction.Get_Value(sql, "SELECT SCONTRACTID FROM TCONTRACT_TRUCK WHERE STRUCKID = '" + dataCheck[0] + "'");

                    //CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + @"','ไปยังหน้าตรวจสภาพรถ',function(){window.location='checktruck.aspx?str=" +
                    //    Server.UrlEncode(STCrypt.Encrypt(SCONTRACT + "&" + dataCheck[0] + "&" + dataCheck[1] + "&" + dataCheck[2] + "&" + dataCheck[3] + "&" + dataCheck[4] + "&1&060")) + "';});");

                    xcpn.JSProperties["cpRedirectOpen"] = "checktruck.aspx?str=" +
                    Server.UrlEncode(STCrypt.Encrypt(SCONTRACT + "&" + dataCheck[0] + "&" + dataCheck[1] + "&" + dataCheck[2] + "&" + dataCheck[3] + "&" + dataCheck[4] + "&1&050"));
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');"); return;
                }
                break;

        }
    }
    //GridView    
    protected void gvw_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        switch (e.RowType.ToString().ToLower())
        {
            case "data":
                if (("" + e.GetValue("CCHECKTRUCKB")).Equals("1"))
                { //CEECF5
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#CEECF5");
                }
                break;
        }
    }
    protected void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption.Equals("ลำดับที่"))
        {
            e.Cell.Text = string.Format("{0}.", e.VisibleIndex + 1);
        }
        else if (e.DataColumn.Caption == "เวลาถึงปลายทาง")
        {
            ((ASPxTimeEdit)gvw.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "dteArrive")).DateTime = (DateTime.Now);
        }
        else if (e.DataColumn.Caption == "#")
        {
            #region Set&event column #
            /*
            int VisibleIndex = e.VisibleIndex;
            ASPxButton imbconfirm = gvw.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "imbconfirm") as ASPxButton;
            ASPxButton imbcancel = gvw.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "imbcancel") as ASPxButton;
            ASPxTextBox txtconfirm = gvw.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtconfirm") as ASPxTextBox;
            if (("" + e.GetValue("CCHECKTRUCKB")).Equals("1"))
            {//ถ้าส่งข้อมูลแล้ว
                imbconfirm.ClientEnabled = false;
                imbcancel.ClientEnabled = false;
            }
            else
            {
                //config ClientInstanceName
                txtconfirm.ClientInstanceName = txtconfirm.ClientInstanceName + "_" + VisibleIndex;
                imbconfirm.ClientInstanceName = imbconfirm.ClientInstanceName + "_" + VisibleIndex;
                imbcancel.ClientInstanceName = imbcancel.ClientInstanceName + "_" + VisibleIndex;

                //Add Event
                imbcancel.ClientSideEvents.Click = "function (s, e) { " + txtconfirm.ClientInstanceName + ".SetValue('0');  s.SetEnabled(false); " + imbconfirm.ClientInstanceName + ".SetEnabled(true);  }";
                imbconfirm.ClientSideEvents.Click = "function (s, e) { " + txtconfirm.ClientInstanceName + ".SetValue('1'); s.SetEnabled(false); " + imbcancel.ClientInstanceName + ".SetEnabled(true);  }";

                bool IsEnabled = ("" + txtconfirm.Value.ToString() == "1") ? true : false;
                //BindingData
                if (Session["ss_data"] != null)
                {
                    DataTable dtss_data = ((DataTable)Session["ss_data"]);
                    DataRow[] drSect = dtss_data.Select("SDELIVERYNO='" + e.GetValue("SDELIVERYNO").ToString() + "' AND DDELIVERY='" + e.GetValue("DDELIVERY").ToString() + "' AND SHEADID='" + e.GetValue("SHEADID").ToString() + "'");
                    if (drSect.Length > 0)
                    {
                        txtconfirm.Value = ("" + drSect[0]["CCHECKTRUCKB"] == "0") ? "0" : "1";
                        IsEnabled = ("" + txtconfirm.Value.ToString() == "1") ? true : false;
                    }

                }
                imbconfirm.ClientEnabled = !IsEnabled;
                imbcancel.ClientEnabled = IsEnabled;
            } 
 */
            #endregion
        }
    }
    protected void gvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        switch (e.CallbackName)
        {
            case "SORT":
                Session["UploadMode"] = null;
                Session["SCHECKID"] = null;
                BindData();
                gvw.CancelEdit();
                break;

            case "CANCELEDIT":
                Session["UploadMode"] = null;
                Session["SCHECKID"] = null;
                gvw.CancelEdit();
                break;
            case "CUSTOMCALLBACK":
                int idxMaster = 0;
                int idxParent = 0;
                int VisibleIndex = 0;
                string sCallbackName = "", sData = "";
                string[] para = e.Args[0].Split('$');
                sCallbackName = para[0] + "";
                sData = e.Args[0].Remove(0, sCallbackName.Length + 1);
                string[] ArrayData = sData.Split('$');

                switch (sCallbackName.ToUpper())
                {
                    case "STARTEDIT":
                        #region STARTEDIT
                        VisibleIndex = int.TryParse(ArrayData[0] + "", out defaultInt) ? int.Parse(ArrayData[0] + "") : 0;
                        idxMaster = VisibleIndex;
                        BackUpData(VisibleIndex, (ASPxGridView)sender);
                        ((ASPxGridView)sender).StartEdit(VisibleIndex);
                        if (((ASPxGridView)sender).IsEditing)
                        {
                            BindData();//binf เพื่อให้กริดแสดง Editing 
                            ((ASPxTextBox)gvw.FindEditFormTemplateControl("txtMode")).Text = "" + ArrayData[1].ToLower();

                            if ("" + ArrayData[1].ToLower() == "imbissue")
                            {
                                Session["UploadMode"] = "imbissue";
                                #region ตรวจสภาพรถ
                                #region Tab1
                                DataView dvCheckLists = (DataView)sdsCheckLists.Select(DataSourceSelectArguments.Empty);
                                DataTable dtCheckLists = dvCheckLists.ToTable();
                                dynamic xgvwData = gvw.GetRowValues(idxMaster, "STRUCKID", "SHEADID", "SHEADREGISTERNO", "STRAILID", "STRAILERREGISTERNO", "CMA", "ICMA");
                                DataTable dtGetData = CommonFunction.Get_Data(connection, @"SELECT  TTRUCKCONFIRM.SCONTRACTID ,TCONTRACT.CGAS,TCONTRACT.COIL 
FROm TTRUCKCONFIRMLIST 
LEFT JOIN TTRUCKCONFIRM ON TTRUCKCONFIRMLIST.NCONFIRMID=TTRUCKCONFIRM.NCONFIRMID
LEFT JOIN TCONTRACT ON TTRUCKCONFIRM.SCONTRACTID=TCONTRACT.SCONTRACTID
where STRUCKID='" + CommonFunction.ReplaceInjection(xgvwData[1] + "") + @"'
GROUP BY TTRUCKCONFIRM.SCONTRACTID ,TCONTRACT.CGAS,TCONTRACT.COIL
ORDER BY MAX(TTRUCKCONFIRM.DDATE) ");
                                if (dtGetData.Rows.Count > 0)
                                {
                                    xgvwData[0] = dtGetData.Rows[0]["SCONTRACTID"] + "";
                                    xgvwData[5] = dtGetData.Rows[0]["COIL"] + "";//OIL
                                    xgvwData[6] = dtGetData.Rows[0]["CGAS"] + "";//GAS
                                }

                                string scondition = ("" + xgvwData[5] == "1" ? " AND COIL='1'" : "") + ("" + xgvwData[6] == "1" ? " AND CGAS='1'" : "");
                                Session["IssueData"] = null;
                                ///SCONTRACTID  ,SHEADID    ,SHEADREGISTERNO    ,STRAILID   ,STRAILERREGISTERNO 
                                Session["IssueData"] = "" + idxMaster + "$" + xgvwData[0] + "^" + xgvwData[1] + "^" + xgvwData[2] + "^" + xgvwData[3] + "^" + xgvwData[4] + "#";

                                ASPxPageControl PageControl = ((ASPxGridView)sender).FindEditFormTemplateControl("pageControl") as ASPxPageControl; PageControl.ActiveTabIndex = 0;
                                ASPxGridView gvwGroupCheckList = PageControl.FindControl("gvwGroupCheckList") as ASPxGridView;
                                DataTable dtGroupCheckLists = CommonFunction.GroupDatable("grpdtCheckList", dtCheckLists, "SGROUPID,SGROUPNAME,COIL,CGAS,GOCL_CACTIVE", "GOCL_CACTIVE='1'" + scondition, "SGROUPID,SGROUPNAME,COIL,CGAS,GOCL_CACTIVE");
                                //PageControl.TabPages[0].Text = "ปัญหาการขนส่ง";
                                gvwGroupCheckList.DataSource = dtGroupCheckLists;
                                gvwGroupCheckList.DataBind();
                                #endregion
                                #region Tab2
                                #region Other Remark
                                DataTable dtOtherRemark = CommonFunction.Get_Data(connection, @"SELECT ichktrck.* 
     FROm TCHECKTRUCK chktrck
     LEFT JOIN TCHECKTRUCKITEM ichktrck ON chktrck.SCHECKID=ichktrck.SCHECKID
    WHERE 1=1 AND ichktrck.COTHER='1' AND SCONTRACTID='" + xgvwData[0] + "' AND STRUCKID='" + xgvwData[1] + "' ");/*AND TO_DATE(DCHECKLIST,'DD/MM/YYYY')=TO_DATE('" + Convert.ToDateTime("" + xgvwData[3]).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY')*/
                                if (dtOtherRemark.Rows.Count > 0)
                                {
                                    string sOtherRemark = "", sRemark = "";
                                    ASPxCheckBox cbxOtherIssue = PageControl.FindControl("cbxOtherIssue") as ASPxCheckBox;
                                    ASPxMemo txtOtherIssue = PageControl.FindControl("txtOtherIssue") as ASPxMemo;
                                    ASPxMemo txtDetailIssue = PageControl.FindControl("txtDetailIssue") as ASPxMemo;
                                    ASPxTextBox txtNMA = PageControl.FindControl("txtNMA") as ASPxTextBox;
                                    foreach (DataRow drOtherRemark in dtOtherRemark.Rows)
                                    {
                                        sOtherRemark += "," + drOtherRemark["sOtherRemark"];
                                        sRemark += "," + drOtherRemark["sRemark"];
                                        break;
                                    }//(Eval("CBAN")+""=="1")?"2":((Eval("NDAY_MA")+""!="")?"1":"0")
                                    cbxOtherIssue.Checked = (true) ? true : false;
                                    if (cbxOtherIssue.Checked)
                                    {
                                        txtOtherIssue.Text = sOtherRemark.Length > 0 ? sOtherRemark.Remove(0, 1) : "";
                                        txtDetailIssue.Text = sRemark.Length > 0 ? sRemark.Remove(0, 1) : "";

                                    }

                                }
                                dtOtherRemark.Dispose();

                                #endregion
                                #region AttachmentFile
                                DataTable dtatt = CommonFunction.Get_Data(connection, @"SELECT * 
FROM TCHECKTRUCKATTACHMENT 
WHERE 1=1 AND SCONTRACTID='" + xgvwData[0] + @"'  AND NVL(SATTACHTYPEID,'1')='1'
AND SCHECKID IN(
    SELECT TCHECKTRUCKITEM.SCHECKID
    FROM TCHECKTRUCKITEM  
    WHERE 1=1 AND NVL(CEDITED,'0')='0' AND DEND_LIST>= SYSDATE
    and TCHECKTRUCKITEM.SCHECKID IN(SELECT distinct SCHECKID FROM TCHECKTRUCK WHERE 1=1 AND STRUCKID='" + xgvwData[1] + @"')
)  ");
                                string satt = "<table width='100%' id='tb_attachmentedfile'>", attment = "", sfile = "<a href='{0}{1}?dx={2}' target='{3}'>{4}</a>";
                                foreach (DataRow dratt in dtatt.Rows)
                                {
                                    string LinkFile = string.Format(sfile, ("" + dratt["SPATH"]).Replace("~/", ""), "" + dratt["SSYSFILE"], DateTime.Now.ToString("hhmmss"), "_blank", "" + dratt["SFILE"]);

                                    attment += string.Format("<tr id='tb_attachmentedfile" + dratt["SCHECKID"] + "_" + dratt["STRUCKID"] + "_" + dratt["SCONTRACTID"] + "_" + dratt["NATTACHMENT"] + "'><td width='60%'>{0}</td><td width='20%'>{1}</td><td width='18%'>{2}</td></tr>", LinkFile
                                        , ""
                                        , "<a href='javascript:void(0);' onclick=\"if(confirm('ท่านต้องกรทำการลบไฟล์นี้ ใช่หรือไม่?')){ RemoveFileOnServer('tb_attachmentedfile','" + dratt["SCONTRACTID"] + "','" + dratt["STRUCKID"] + "','" + dratt["SCHECKID"] + "','" + dratt["NATTACHMENT"] + "');}else{return false;}\"><img title='ลบไฟล์' alt='ลบไฟล์' src='Images/del.gif' /></a>");

                                }
                                attment = attment.Length > 0 ? attment : "<tr><td width='60%'></td><td width='20%'></td><td width='18%'></td></tr>";
                                satt += attment + "</table>";
                                ((HtmlGenericControl)PageControl.TabPages[1].FindControl("rplAttachmented").FindControl("pnctTab2").FindControl("AttachmentedListFiles")).InnerHtml = "" + satt;

                                dtatt.Dispose();
                                #endregion
                                #endregion
                                dtCheckLists.Dispose();
                                dtGroupCheckLists.Dispose();
                                #endregion
                            }
                            else if ("" + ArrayData[1].ToLower() == "imbissueprod")
                            {
                                Session["UploadMode"] = "imbissueprod";
                                #region ตรวจสภาพสินค้า

                                #region Tab1
                                DataView dvCheckLists = (DataView)sdsCheckListsProd.Select(DataSourceSelectArguments.Empty);
                                DataTable dtCheckLists = dvCheckLists.ToTable();
                                dynamic xgvwData = gvw.GetRowValues(idxMaster, "STRUCKID", "SHEADID", "SHEADREGISTERNO", "STRAILID", "STRAILERREGISTERNO", "CMA", "ICMA");
                                DataTable dtGetData = CommonFunction.Get_Data(connection, @"SELECT  TTRUCKCONFIRM.SCONTRACTID ,TCONTRACT.CGAS,TCONTRACT.COIL 
FROm TTRUCKCONFIRMLIST 
LEFT JOIN TTRUCKCONFIRM ON TTRUCKCONFIRMLIST.NCONFIRMID=TTRUCKCONFIRM.NCONFIRMID
LEFT JOIN TCONTRACT ON TTRUCKCONFIRM.SCONTRACTID=TCONTRACT.SCONTRACTID
where STRUCKID='" + CommonFunction.ReplaceInjection(xgvwData[1] + "") + @"'
GROUP BY TTRUCKCONFIRM.SCONTRACTID ,TCONTRACT.CGAS,TCONTRACT.COIL
ORDER BY MAX(TTRUCKCONFIRM.DDATE) ");
                                if (dtGetData.Rows.Count > 0)
                                {
                                    xgvwData[0] = dtGetData.Rows[0]["SCONTRACTID"] + "";
                                    xgvwData[5] = dtGetData.Rows[0]["COIL"] + "";//OIL
                                    xgvwData[6] = dtGetData.Rows[0]["CGAS"] + "";//GAS
                                }

                                string scondition = ("" + xgvwData[5] == "1" ? " AND COIL='1'" : "") + ("" + xgvwData[6] == "1" ? " AND CGAS='1'" : "");
                                Session["IssueData"] = null;
                                ///SCONTRACTID  ,SHEADID    ,SHEADREGISTERNO    ,STRAILID   ,STRAILERREGISTERNO 
                                Session["IssueData"] = "" + idxMaster + "$" + xgvwData[0] + "^" + xgvwData[1] + "^" + xgvwData[2] + "^" + xgvwData[3] + "^" + xgvwData[4] + "#";

                                ASPxPageControl PageControl = ((ASPxGridView)sender).FindEditFormTemplateControl("pageControl") as ASPxPageControl; PageControl.ActiveTabIndex = 0;
                                ASPxGridView gvwGroupCheckList = PageControl.FindControl("gvwGroupCheckList") as ASPxGridView;
                                DataTable dtGroupCheckLists = CommonFunction.GroupDatable("grpdtCheckListProd", dtCheckLists, "SGROUPID,SGROUPNAME,COIL,CGAS,GOCL_CACTIVE", "GOCL_CACTIVE='1'" + scondition, "SGROUPID,SGROUPNAME,COIL,CGAS,GOCL_CACTIVE");
                                //PageControl.TabPages[0].Text = "ปัญหาผลิตภัณฑ์";
                                gvwGroupCheckList.DataSource = dtGroupCheckLists;
                                gvwGroupCheckList.DataBind();
                                #endregion
                                #region Tab2

                                #region Other Remark
                                DataTable dtOtherRemark = CommonFunction.Get_Data(connection, @"SELECT ichktrck.* ,CHASDIRTY,CHASLOSS
,loss.SPRODUCTID ,loss.NLOSSVOLUMN ,LOSS.NPRODUCTUNITID ,LOSS.SLOSSID
     FROm TCHECKPRODUCT chktrck
     LEFT JOIN TCHECKPRODUCTITEM ichktrck ON chktrck.SCHECKID=ichktrck.SCHECKID
     LEFT JOIN TLOSS loss ON chktrck.SCHECKID=loss.SCHECKID
    WHERE 1=1 AND ichktrck.COTHER='1' AND SCONTRACTID='" + xgvwData[0] + "' AND STRUCKID='" + xgvwData[1] + "' ");
                                if (dtOtherRemark.Rows.Count > 0)
                                {
                                    string sOtherRemark = "", sRemark = "", sHasDirty = "0", sHasLoss = "0", sProdID = "", nLoss = "0", sProdUnit = "", sLossID = "";
                                    TextBox txtLossID = PageControl.FindControl("txtLossID") as TextBox;
                                    ASPxCheckBox cbxOtherIssue = PageControl.FindControl("cbxOtherIssue") as ASPxCheckBox;
                                    ASPxCheckBox cbxHasDirty = PageControl.FindControl("cbxHasDirty") as ASPxCheckBox;
                                    ASPxCheckBox cbxHasLoss = PageControl.FindControl("cbxHasLoss") as ASPxCheckBox;
                                    ASPxComboBox cbxProduct = PageControl.FindControl("cbxProduct") as ASPxComboBox;
                                    ASPxTextBox txtnLoss = PageControl.FindControl("txtnLoss") as ASPxTextBox;
                                    ASPxComboBox cbxProdUnits = PageControl.FindControl("cbxProdUnits") as ASPxComboBox;
                                    ASPxMemo txtOtherIssue = PageControl.FindControl("txtOtherIssue") as ASPxMemo;
                                    ASPxMemo txtDetailIssue = PageControl.FindControl("txtDetailIssue") as ASPxMemo;
                                    ASPxTextBox txtNMA = PageControl.FindControl("txtNMA") as ASPxTextBox;
                                    foreach (DataRow drOtherRemark in dtOtherRemark.Rows)
                                    {
                                        sLossID = "" + drOtherRemark["SLOSSID"];
                                        nLoss = "" + drOtherRemark["NLOSSVOLUMN"];
                                        sProdUnit = "" + drOtherRemark["NPRODUCTUNITID"];
                                        sProdID = "" + drOtherRemark["SPRODUCTID"];
                                        sHasDirty = "" + drOtherRemark["CHASDIRTY"];
                                        sHasLoss = "" + drOtherRemark["CHASLOSS"];
                                        sOtherRemark += "," + drOtherRemark["sOtherRemark"];
                                        sRemark += "," + drOtherRemark["sRemark"];

                                        break;
                                    }

                                    cbxOtherIssue.Checked = (true) ? true : false;
                                    cbxHasDirty.Checked = (sHasDirty == "1") ? true : false;
                                    cbxHasLoss.Checked = (sHasLoss == "1") ? true : false;
                                    if (cbxHasLoss.Checked)
                                    {
                                        txtLossID.Text = "" + sLossID;
                                        cbxProduct.Value = "" + sProdID;
                                        txtnLoss.Text = "" + nLoss;
                                        cbxProdUnits.Value = "" + sProdUnit;
                                    }
                                    if (cbxOtherIssue.Checked)
                                    {
                                        txtOtherIssue.Text = sOtherRemark.Length > 0 ? sOtherRemark.Remove(0, 1) : "";
                                        txtDetailIssue.Text = sRemark.Length > 0 ? sRemark.Remove(0, 1) : "";

                                    }

                                }
                                dtOtherRemark.Dispose();

                                #endregion
                                //sdsProduct
                                sdsProduct.SelectCommand = @"SELECT PROD_ID,PROD_CATEGORY  ,PROD_ID||' - '||PROD_ABBR PROD_ABBR ,DENSITY 
FROM TPRODUCT prod
LEFT JOIN TDELIVERY_SAP delisap ON PROD.PROD_ID =delisap.MATERIAL_NO
WHERE 1=1 
ORDER BY PROD.PROD_ABBR"; sdsProduct.DataBind();


                                #region AttachmentFile
                                DataTable dtatt = CommonFunction.Get_Data(connection, @"SELECT * 
FROM TCHECKTRUCKATTACHMENT 
WHERE 1=1 AND SCONTRACTID='" + xgvwData[0] + @"'  AND NVL(SATTACHTYPEID,'1')='2'
AND SCHECKID IN(
    SELECT TCHECKTRUCKITEM.SCHECKID
    FROM TCHECKTRUCKITEM  
    WHERE 1=1 AND NVL(CEDITED,'0')='0' AND DEND_LIST>= SYSDATE
    and TCHECKTRUCKITEM.SCHECKID IN(SELECT distinct SCHECKID FROM TCHECKTRUCK WHERE 1=1 AND STRUCKID='" + xgvwData[1] + @"')
)  ");
                                string satt = "<table width='100%' id='tb_attachmentedfile'>", attment = "", sfile = "<a href='{0}{1}?dx={2}' target='{3}'>{4}</a>";
                                foreach (DataRow dratt in dtatt.Rows)
                                {
                                    string LinkFile = string.Format(sfile, ("" + dratt["SPATH"]).Replace("~/", ""), "" + dratt["SSYSFILE"], DateTime.Now.ToString("hhmmss"), "_blank", "" + dratt["SFILE"]);

                                    attment += string.Format("<tr id='tb_attachmentedfile" + dratt["SCHECKID"] + "_" + dratt["STRUCKID"] + "_" + dratt["SCONTRACTID"] + "_" + dratt["NATTACHMENT"] + "'><td width='60%'>{0}</td><td width='20%'>{1}</td><td width='18%'>{2}</td></tr>", LinkFile
                                        , ""
                                        , "<a href='javascript:void(0);' onclick=\"if(confirm('ท่านต้องกรทำการลบไฟล์นี้ ใช่หรือไม่?')){ RemoveFileOnServer('tb_attachmentedfile','" + dratt["SCONTRACTID"] + "','" + dratt["STRUCKID"] + "','" + dratt["SCHECKID"] + "','" + dratt["NATTACHMENT"] + "');}else{return false;}\"><img title='ลบไฟล์' alt='ลบไฟล์' src='Images/del.gif' /></a>");

                                }
                                attment = attment.Length > 0 ? attment : "<tr><td width='60%'></td><td width='20%'></td><td width='18%'></td></tr>";
                                satt += attment + "</table>";
                                ((HtmlGenericControl)PageControl.TabPages[1].FindControl("rplAttachmented").FindControl("pnctTab2").FindControl("AttachmentedListFiles")).InnerHtml = "" + satt;
                                ((HtmlGenericControl)PageControl.TabPages[1].FindControl("spnNMA")).Style.Add("display", "none");
                                ((HtmlTableRow)PageControl.TabPages[1].FindControl("tr_ProdIssue")).Style.Add("display", "");
                                dtatt.Dispose();
                                #endregion
                                #endregion
                                dtCheckLists.Dispose();
                                dtGroupCheckLists.Dispose();

                                #endregion
                            }
                            else { }
                        }
                        #endregion
                        break;
                    case "SAVE":
                        if ("" + Session["CheckPermission"] == "1")
                        {
                            string modes = "imbissue";
                            modes = ((ASPxTextBox)gvw.FindEditFormTemplateControl("txtMode")).Text;
                            VisibleIndex = int.TryParse(ArrayData[0] + "", out defaultInt) ? int.Parse(ArrayData[0] + "") : 0;
                            dynamic values = gvw.GetRowValues(VisibleIndex, "STRUCKID", "SHEADID", "SHEADREGISTERNO", "STRAILID", "STRAILERREGISTERNO", "CMA", "ICMA", "CPASSED", "CCONFIRM", "SDELIVERYNO", "DDELIVERY", "STIMEWINDOW", "NPLANID", "SPLANLISTID", "NDROP", "SSHIPTO", "STERMINALID", "SVENDORID", "SHIPMENT_NO", "NVALUE");
                            #region SAVE
                            if (modes == "imbissue")
                            {
                                #region Issue

                                if (ArrayData[1] + "" == "1")
                                {
                                    #region TAB1
                                    ASPxPageControl PageControl = ((ASPxGridView)sender).FindEditFormTemplateControl("pageControl") as ASPxPageControl;
                                    ASPxGridView gvwGroupCheckList_SAVE = PageControl.FindControl("gvwGroupCheckList") as ASPxGridView;

                                    TCHECKTRUCK ctrck = new TCHECKTRUCK(Page, connection);
                                    TCHECKTRUCKITEM ctrckItem = new TCHECKTRUCKITEM(Page, connection);



                                    TREDUCEPOINT reduce = new TREDUCEPOINT(Page, connection);
                                    double NPOINT = 0;
                                    string sIssueData = "", SCHECKID = "";
                                    for (int idxGroupGrid = 0; idxGroupGrid < gvwGroupCheckList_SAVE.VisibleRowCount; idxGroupGrid++)
                                    {///Gird GroupIssue
                                        ASPxGridView gvwItemCheckList_SAVE = gvwGroupCheckList_SAVE.FindRowCellTemplateControl(idxGroupGrid, (GridViewDataColumn)gvwGroupCheckList_SAVE.Columns[0], "gvwItemCheckList") as ASPxGridView;
                                        for (int idxItemGrid = 0; idxItemGrid < gvwItemCheckList_SAVE.VisibleRowCount; idxItemGrid++)
                                        {//Grid Checklist
                                            ASPxCheckBox cbxSCHECKLISTID = gvwItemCheckList_SAVE.FindRowCellTemplateControl(idxItemGrid, (GridViewDataColumn)gvwItemCheckList_SAVE.Columns[0], "cbxSCHECKLISTID") as ASPxCheckBox;
                                            if (cbxSCHECKLISTID.Checked && cbxSCHECKLISTID.ClientEnabled)
                                            {
                                                dynamic datagriditem = gvwItemCheckList_SAVE.GetRowValues(idxItemGrid, "SVERSIONLIST", "SVERSION", "STOPICID", "SCHECKLISTID", "NLIST", "NDA_YMA", "CCUT", "CBAN", "CL_CACTIVE", "NPOINT", "STYPECHECKLISTID");
                                                string gvwCarIndex = "" + ("" + Session["IssueData"]).Split('$')[0];
                                                string[] TruckData = ("" + Session["IssueData"]).Split('$')[1].Split('#')[0].Split('^');
                                                ///1.Call StoreProcedure for insert into DB
                                                #region TCHECKPRODUCT
                                                ///TCHECKPRODUCT
                                                ctrck.SCHECKID = SCHECKID != "" ? SCHECKID : "";//send blank for gen new id
                                                ctrck.NPLANID = "" + values[12];
                                                ctrck.STERMINALID = "" + Session["SVDID"];
                                                ctrck.SCONTRACTID = "" + TruckData[0];
                                                ctrck.CGROUPCHECK = "1";//fix หน้านี้เป็นการทำงานของ หน้าคลัง
                                                ctrck.CCLEAN = "0";
                                                ctrck.STRUCKID = "" + values[1];
                                                ctrck.SHEADERREGISTERNO = "" + values[2];
                                                ctrck.STRAILERREGISTERNO = "" + values[4];
                                                ctrck.CMA = (("" + datagriditem[7] == "1") ? "2" : (("" + datagriditem[6] == "1") ? "1" : "0"));//2 hold ,1 mainternent ,0 ปกติ
                                                ctrck.NDAY_MA = (("" + datagriditem[5] == "") ? "0" : "" + datagriditem[5]);
                                                ctrck.SCREATE = "" + Session["UserID"];
                                                ctrck.SUPDATE = "" + Session["UserID"];

                                                ctrck.Insert();
                                                SCHECKID = ctrck.SCHECKID;//BackUp SCHECKID for Child rows.
                                                #endregion
                                                #region///TCHECKPRODUCTITEM
                                                ctrckItem.SCHECKID = SCHECKID;
                                                ctrckItem.SCHECKLISTID = "" + datagriditem[3];
                                                ctrckItem.SVERSIONLIST = "" + datagriditem[0];
                                                ctrckItem.NDAY_MA = (("" + datagriditem[5] == "") ? "0" : "" + datagriditem[5]);
                                                ctrckItem.NPOINT = ("" + datagriditem[9] != "") ? ("" + datagriditem[9]) : "0";
                                                ctrckItem.CMA = (("" + datagriditem[7] == "1") ? "2" : (("" + datagriditem[6] == "1") ? "1" : "0"));//2 hold ,1 mainternent ,0 ปกติ
                                                ctrckItem.COTHER = "0";
                                                ctrckItem.SOTHERREMARK = "";
                                                ctrckItem.SCREATE = "" + Session["UserID"];
                                                ctrckItem.SUPDATE = "" + Session["UserID"];
                                                ctrckItem.STYPECHECKLISTID = "" + datagriditem[10];
                                                ctrckItem.Insert();
                                                #endregion
                                                #region บันทึกห้ามวิ่ง+ตัดแต้ม
                                                sIssueData += "^$SCHECKID:" + "" + "$CMA:" + ctrckItem.CMA + "$SCHECKLISTID:" + datagriditem[3] + "$SVERSIONLIST:" + datagriditem[0] + "$NPOINT:" + ctrckItem.NPOINT + "$SCREATE:" + ctrckItem.SCREATE + "$CBAN:" + datagriditem[7] + "$CCUT:" + datagriditem[6] + "$NDAY_MA:" + ctrckItem.NDAY_MA + "$STYPECHECKLISTID:" + datagriditem[10] + "$mode:imbissueprod";//+"$:"
                                                string[] sArrayData = sIssueData.Split('^');
                                                if ("" + datagriditem[7] == "1")
                                                {//ถ้าเป้นเคส ห้ามวิ่ง 
                                                    #region//บันทึกสถานะ ห้ามวิ่ง TTRUCK
                                                    using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                                                    {
                                                        if (con.State == ConnectionState.Closed) con.Open();
                                                        OracleCommand ora_cmd = new OracleCommand("UPDATE TTRUCK SET CHOLD='1' WHERE STRUCKID=:S_TRUCKID", con);
                                                        ora_cmd.Parameters.Add(":S_TRUCKID", OracleType.VarChar).Value = ctrck.STRUCKID;
                                                        ora_cmd.ExecuteNonQuery();


                                                        AlertTruckOnHold onHold = new AlertTruckOnHold(Page, con);
                                                        onHold.VehNo = ctrck.SHEADERREGISTERNO;
                                                        onHold.TUNo = ctrck.STRAILERREGISTERNO;
                                                        onHold.Subject = "การตรวจสภาพรถ/ละเลยการแก้สภาพรถเกินตามที่กำหนดหรือโดยประการใดๆซึ่งเป็นผลต่อการห้ามวิ่ง";
                                                        onHold.VehID = ctrck.STRUCKID;
                                                        onHold.SendTo();
                                                    }
                                                    #endregion
                                                }
                                                if ("" + datagriditem[6] == "1")
                                                {//ถ้าตัดคะแนนทันที
                                                    //บันทึกตัดแต้มในตาราง TREDUCEPOINT 
                                                    //SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT ,SCHECKID ,SCONTRACTID ,SHEADID ,SHEADERREGISTERNO ,STRAILERID ,STRAILERREGISTERNO ,SDRIVERNO
                                                    ReducePoint("01", "030", "" + cbxSCHECKLISTID.Text, "" + datagriditem[3], "" + datagriditem[2], "" + datagriditem[0], "" + (("" + datagriditem[9] != "") ? ("" + datagriditem[9]) : "0")
                                                         , SCHECKID, ctrck.SCONTRACTID, ctrck.STRUCKID, ctrck.SHEADERREGISTERNO, "", ctrck.STRAILERREGISTERNO, "");
                                                }
                                                #endregion
                                                NPOINT += (ctrckItem.NPOINT != "") ? double.Parse(ctrckItem.NPOINT) : 0;
                                                ///2.List Data for future

                                            }//End Checklist Item
                                        }//End loop grid item
                                    }//EndLoop grid group checklist
                                    sIssueData = Session["IssueData"] + sIssueData;
                                    #region Update คะแนนรวมที่ตัดที่ รายการหลัก
                                    ctrck.SCHECKID = SCHECKID;
                                    ctrck.UpdateMA();
                                    #endregion
                                    #endregion
                                }
                                else if (ArrayData[1] + "" == "2")
                                {
                                    #region TAB2
                                    string ss_IssueData = Session["IssueData"] + "";
                                    string[] ArrayIssueData = ss_IssueData.Split('^');
                                    if (ArrayIssueData.Length <= 0) return;
                                    int idxEditMaster = gvw.EditingRowVisibleIndex;
                                    dynamic gvwDataMaster = gvw.GetRowValues(idxEditMaster, "SCONTRACTID", "COIL", "CGAS", "DDATE");
                                    string[] TruckData = ("" + Session["IssueData"]).Split('$')[1].Split('#')[0].Split('^');
                                    TCHECKTRUCK ctrck = new TCHECKTRUCK(Page, connection);
                                    TCHECKTRUCKITEM chkItem = new TCHECKTRUCKITEM(this.Page, connection);
                                    ASPxPageControl PageControlTab2 = ((ASPxGridView)sender).FindEditFormTemplateControl("pageControl") as ASPxPageControl;
                                    ASPxCheckBox cbxOtherIssue = PageControlTab2.FindControl("cbxOtherIssue") as ASPxCheckBox;
                                    ASPxMemo txtOtherIssue = PageControlTab2.FindControl("txtOtherIssue") as ASPxMemo;
                                    ASPxTextBox txtNMA = PageControlTab2.FindControl("txtNMA") as ASPxTextBox;
                                    ASPxMemo txtDetailIssue = PageControlTab2.FindControl("txtDetailIssue") as ASPxMemo;
                                    string SCHECKID_TAB2 = "";

                                    SCHECKID_TAB2 = CommonFunction.Get_Value(connection, @"SELECT TCHECKTRUCK.SCHECKID FROM TCHECKTRUCK  WHERE SCONTRACTID='" + CommonFunction.ReplaceInjection("" + TruckData[0]) + @"' 
AND STRUCKID='" + CommonFunction.ReplaceInjection("" + values[1]) + @"' AND NPLANID='" + CommonFunction.ReplaceInjection("" + values[12]) + "' ");//--AND TO_DATE(DCHECK,'DD/MM/YYYY')=TO_DATE('" + CommonFunction.ReplaceInjection("" + Convert.ToDateTime("" + values[10]).ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','DD/MM/YYYY') 
                                    if (SCHECKID_TAB2 == "")
                                    {
                                        #region TCHECKTRUCK
                                        ///TCHECKTRUCK
                                        ctrck.SCHECKID = SCHECKID_TAB2 != "" ? SCHECKID_TAB2 : "";//send blank for gen new id
                                        ctrck.NPLANID = "" + values[12];
                                        ctrck.STERMINALID = "" + Session["SVDID"];
                                        ctrck.SCONTRACTID = "" + TruckData[0];
                                        ctrck.CGROUPCHECK = "1";//fix หน้านี้เป็นการทำงานของ หน้าคลัง
                                        ctrck.CCLEAN = "0";
                                        ctrck.STRUCKID = "" + values[1];
                                        ctrck.SHEADERREGISTERNO = "" + values[2];
                                        ctrck.STRAILERREGISTERNO = "" + values[4];
                                        ctrck.CMA = "0";//2 hold ,1 mainternent ,0 ปกติ
                                        ctrck.NDAY_MA = "0";
                                        ctrck.SCREATE = "" + Session["UserID"];
                                        ctrck.SUPDATE = "" + Session["UserID"];

                                        ctrck.Insert();
                                        SCHECKID_TAB2 = ctrck.SCHECKID;//BackUp SCHECKID for Child rows.

                                        #endregion
                                    }
                                    #region TCHECKTRUCKITEM
                                    chkItem.SCHECKID = SCHECKID_TAB2;
                                    chkItem.SCHECKLISTID = "0";
                                    chkItem.CMA = "0";
                                    chkItem.NPOINT = "0";
                                    chkItem.SCHECKLISTID = "0";
                                    chkItem.SVERSIONLIST = "0";
                                    chkItem.NDAY_MA = txtNMA.Text == "" ? "0" : txtNMA.Text;
                                    chkItem.COTHER = cbxOtherIssue.Checked ? "1" : "0";
                                    chkItem.SOTHERREMARK = cbxOtherIssue.Checked ? txtOtherIssue.Text : "";
                                    chkItem.SREMARK = txtDetailIssue.Text;
                                    chkItem.SCREATE = "" + Session["UserID"];
                                    chkItem.SUPDATE = "" + Session["UserID"];
                                    chkItem.InsertTab2(gvwDataMaster[0] + "", ArrayIssueData[2] + "", Convert.ToDateTime("" + values[10]).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "");
                                    #endregion
                                    #region Attchment File
                                    if (Session["DataAttechment"] != null)
                                    {
                                        using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                                        {
                                            if (con.State == ConnectionState.Closed) con.Open();
                                            DataTable dtDataAttechment = ((DataTable)Session["DataAttechment"]);
                                            foreach (DataRow drDataAttechment in dtDataAttechment.Select("cRowFlag='Page'"))
                                            {
                                                OracleCommand ora_cmd = new OracleCommand(@"INSERT INTO TCHECKTRUCKATTACHMENT ( SCHECKID, NATTACHMENT, STRUCKID,SCONTRACTID, SATTACHTYPEID, SPATH,SFILE, SSYSFILE, DCREATE,SCREATE, DUPDATE, SUPDATE) 
 VALUES ( :S_CHECKID,FC_GENID_TCHECKTRUCKATTACHMENT(:S_CONTRACTID, :S_TRUCKID ,SYSDATE),:S_TRUCKID, :S_CONTRACTID,:S_ATTACHTYPEID,:S_PATH, :S_FILE,:S_SYSFILE, SYSDATE, :S_CREATE, SYSDATE,:S_UPDATE ) ", con);
                                                ora_cmd.Parameters.Add(":S_CHECKID", OracleType.VarChar).Value = SCHECKID_TAB2;
                                                ora_cmd.Parameters.Add(":S_TRUCKID", OracleType.VarChar).Value = "" + drDataAttechment["STRUCKID"];
                                                ora_cmd.Parameters.Add(":S_CONTRACTID", OracleType.VarChar).Value = "" + drDataAttechment["SCONTRACTID"];
                                                ora_cmd.Parameters.Add(":S_ATTACHTYPEID", OracleType.VarChar).Value = "1";
                                                ora_cmd.Parameters.Add(":S_PATH", OracleType.VarChar).Value = "" + drDataAttechment["SPATH"];
                                                ora_cmd.Parameters.Add(":S_FILE", OracleType.VarChar).Value = "" + drDataAttechment["SFILE"];
                                                ora_cmd.Parameters.Add(":S_SYSFILE", OracleType.VarChar).Value = "" + drDataAttechment["SSYSFILE"];
                                                ora_cmd.Parameters.Add(":S_CREATE", OracleType.VarChar).Value = "" + Session["SVDID"];
                                                ora_cmd.Parameters.Add(":S_UPDATE", OracleType.VarChar).Value = "" + Session["SVDID"];
                                                ora_cmd.ExecuteNonQuery();
                                            }
                                        }
                                    }
                                    #endregion
                                    ((ASPxGridView)sender).CancelEdit();
                                    #endregion

                                }
                                else { }

                                #endregion
                            }
                            else if (modes == "imbissueprod")
                            {
                                #region Product

                                if (ArrayData[1] + "" == "1")
                                {
                                    #region TAB1
                                    ASPxPageControl PageControl = ((ASPxGridView)sender).FindEditFormTemplateControl("pageControl") as ASPxPageControl;
                                    ASPxGridView gvwGroupCheckList_SAVE = PageControl.FindControl("gvwGroupCheckList") as ASPxGridView;

                                    TCHECKPRODUCT ctrck = new TCHECKPRODUCT(Page, connection);
                                    TCHECKPRODUCTITEM ctrckItem = new TCHECKPRODUCTITEM(Page, connection);
                                    TREDUCEPOINT reduce = new TREDUCEPOINT(Page, connection);
                                    int NPOINT = 0;
                                    string sIssueData = "", SCHECKID = "";
                                    for (int idxGroupGrid = 0; idxGroupGrid < gvwGroupCheckList_SAVE.VisibleRowCount; idxGroupGrid++)
                                    {///Gird GroupIssue
                                        ASPxGridView gvwItemCheckList_SAVE = gvwGroupCheckList_SAVE.FindRowCellTemplateControl(idxGroupGrid, (GridViewDataColumn)gvwGroupCheckList_SAVE.Columns[0], "gvwItemCheckList") as ASPxGridView;
                                        for (int idxItemGrid = 0; idxItemGrid < gvwItemCheckList_SAVE.VisibleRowCount; idxItemGrid++)
                                        {//Grid Checklist
                                            ASPxCheckBox cbxSCHECKLISTID = gvwItemCheckList_SAVE.FindRowCellTemplateControl(idxItemGrid, (GridViewDataColumn)gvwItemCheckList_SAVE.Columns[0], "cbxSCHECKLISTID") as ASPxCheckBox;
                                            if (cbxSCHECKLISTID.Checked && cbxSCHECKLISTID.ClientEnabled)
                                            {
                                                dynamic datagriditem = gvwItemCheckList_SAVE.GetRowValues(idxItemGrid, "SVERSIONLIST", "SVERSION", "STOPICID", "SCHECKLISTID", "NLIST", "NDAY_MA", "CCUT", "CBAN", "CL_CACTIVE", "NPOINT", "STYPECHECKLISTID");
                                                string gvwCarIndex = "" + ("" + Session["IssueData"]).Split('$')[0];
                                                string[] TruckData = ("" + Session["IssueData"]).Split('$')[1].Split('#')[0].Split('^');
                                                ///1.Call StoreProcedure for insert into DB
                                                #region TCHECKPRODUCT
                                                ///TCHECKPRODUCT
                                                ctrck.SCHECKID = SCHECKID != "" ? SCHECKID : "";//send blank for gen new id
                                                ctrck.NPLANID = "" + values[12];
                                                ctrck.STERMINALID = "" + Session["SVDID"];
                                                ctrck.SCONTRACTID = "" + TruckData[0];
                                                ctrck.CGROUPCHECK = "1";//fix หน้านี้เป็นการทำงานของ หน้าคลัง
                                                ctrck.CCLEAN = "0";
                                                ctrck.STRUCKID = "" + values[1];
                                                ctrck.SHEADERREGISTERNO = "" + values[2];
                                                ctrck.STRAILERREGISTERNO = "" + values[4];
                                                ctrck.CMA = (("" + datagriditem[7] == "1") ? "2" : (("" + datagriditem[6] == "1") ? "1" : "0"));//2 hold ,1 mainternent ,0 ปกติ
                                                ctrck.NDAY_MA = (("" + datagriditem[5] == "") ? "0" : "" + datagriditem[5]);
                                                ctrck.SCREATE = "" + Session["UserID"];
                                                ctrck.SUPDATE = "" + Session["UserID"];

                                                ctrck.Insert();
                                                SCHECKID = ctrck.SCHECKID;//BackUp SCHECKID for Child rows.
                                                Session["SCHECKID"] = SCHECKID;
                                                #endregion
                                                #region///TCHECKTRUCKITEM
                                                ctrckItem.SCHECKID = SCHECKID;
                                                ctrckItem.SCHECKLISTID = "" + datagriditem[3];
                                                ctrckItem.SVERSIONLIST = "" + datagriditem[0];
                                                ctrckItem.STYPECHECKLISTID = "" + datagriditem[10];
                                                //ctrckItem.NDAY_MA = (("" + datagriditem[5] == "") ? "0" : "" + datagriditem[5]);
                                                ctrckItem.NPOINT = ("" + datagriditem[9] != "") ? ("" + datagriditem[9]) : "0";
                                                //ctrckItem.CMA = (("" + datagriditem[7] == "1") ? "2" : (("" + datagriditem[6] == "1") ? "1" : "0"));//2 hold ,1 mainternent ,0 ปกติ
                                                ctrckItem.COTHER = "0";
                                                ctrckItem.SOTHERREMARK = "";
                                                ctrckItem.SCREATE = "" + Session["UserID"];
                                                ctrckItem.SUPDATE = "" + Session["UserID"];
                                                ctrckItem.Insert();
                                                #endregion
                                                #region บันทึกห้ามวิ่ง+ตัดแต้ม
                                                sIssueData += "^$SCHECKID:" + "" + "$CMA:0$SCHECKLISTID:" + datagriditem[3] + "$SVERSIONLIST:" + datagriditem[0] + "$NPOINT:" + ctrckItem.NPOINT + "$SCREATE:" + ctrckItem.SCREATE + "$CBAN:" + datagriditem[7] + "$CCUT:" + datagriditem[6] + "$NDAY_MA:0$STYPECHECKLISTID:" + datagriditem[10] + "$mode:imbissueprod";
                                                string[] sArrayData = sIssueData.Split('^');
                                                if ("" + datagriditem[7] == "1")
                                                {//ถ้าเป้นเคส ห้ามวิ่ง 
                                                    #region//บันทึกสถานะ ห้ามวิ่ง TTRUCK
                                                    using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                                                    {
                                                        if (con.State == ConnectionState.Closed) con.Open();
                                                        OracleCommand ora_cmd = new OracleCommand("UPDATE TTRUCK SET CHOLD='1' WHERE STRUCKID=:S_TRUCKID", con);
                                                        ora_cmd.Parameters.Add(":S_TRUCKID", OracleType.VarChar).Value = ctrck.STRUCKID;
                                                        ora_cmd.ExecuteNonQuery();
                                                    }
                                                    #endregion
                                                }
                                                if ("" + datagriditem[6] == "1")
                                                {//ถ้าตัดคะแนนทันที
                                                    //บันทึกตัดแต้มในตาราง TREDUCEPOINT 
                                                    //SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT ,SCHECKID ,SCONTRACTID ,SHEADID ,SHEADERREGISTERNO ,STRAILERID ,STRAILERREGISTERNO ,SDRIVERNO
                                                    ReducePoint("05", "060", "" + cbxSCHECKLISTID.Text, "" + datagriditem[3], "" + datagriditem[2], "" + datagriditem[0], "" + (("" + datagriditem[9] != "") ? ("" + datagriditem[9]) : "0")
                                                         , SCHECKID, ctrck.SCONTRACTID, ctrck.STRUCKID, ctrck.SHEADERREGISTERNO, "", ctrck.STRAILERREGISTERNO, "");
                                                }
                                                #endregion
                                                NPOINT += (ctrckItem.NPOINT != "") ? int.Parse(ctrckItem.NPOINT) : 0;
                                                ///2.List Data for future

                                            }//End Checklist Item
                                        }//End loop grid item
                                    }//EndLoop grid group checklist
                                    sIssueData = Session["IssueData"] + sIssueData;

                                    #endregion
                                }
                                else if (ArrayData[1] + "" == "2")
                                {
                                    #region TAB2
                                    string ss_IssueData = Session["IssueData"] + "";
                                    string[] ArrayIssueData = ss_IssueData.Split('^');
                                    if (ArrayIssueData.Length <= 0) return;
                                    int idxEditMaster = gvw.EditingRowVisibleIndex;
                                    dynamic gvwDataMaster = gvw.GetRowValues(idxEditMaster, "SCONTRACTID", "COIL", "CGAS", "DDATE");
                                    string[] TruckData = ("" + Session["IssueData"]).Split('$')[1].Split('#')[0].Split('^');
                                    TCHECKPRODUCT ctrck = new TCHECKPRODUCT(Page, connection);
                                    TCHECKPRODUCTITEM chkItem = new TCHECKPRODUCTITEM(this.Page, connection);
                                    ASPxPageControl PageControlTab2 = ((ASPxGridView)sender).FindEditFormTemplateControl("pageControl") as ASPxPageControl;
                                    ASPxCheckBox cbxOtherIssue = PageControlTab2.FindControl("cbxOtherIssue") as ASPxCheckBox;
                                    ASPxMemo txtOtherIssue = PageControlTab2.FindControl("txtOtherIssue") as ASPxMemo;
                                    ASPxTextBox txtNMA = PageControlTab2.FindControl("txtNMA") as ASPxTextBox;
                                    ASPxMemo txtDetailIssue = PageControlTab2.FindControl("txtDetailIssue") as ASPxMemo;
                                    ASPxCheckBox cbxHasLoss = PageControlTab2.FindControl("cbxHasLoss") as ASPxCheckBox;
                                    ASPxCheckBox cbxHasDirty = PageControlTab2.FindControl("cbxHasDirty") as ASPxCheckBox;
                                    ASPxComboBox cbxProduct = PageControlTab2.FindControl("cbxProduct") as ASPxComboBox;
                                    ASPxTextBox txtnLoss = PageControlTab2.FindControl("txtnLoss") as ASPxTextBox;
                                    ASPxComboBox cbxProdUnits = PageControlTab2.FindControl("cbxProdUnits") as ASPxComboBox;
                                    TextBox txtLossID = PageControlTab2.FindControl("txtLossID") as TextBox;
                                    string SCHECKID_TAB2 = "";
                                    if (Session["SCHECKID"] == null)
                                    {
                                        SCHECKID_TAB2 = CommonFunction.Get_Value(connection, @"SELECT TCHECKPRODUCT.SCHECKID FROM TCHECKPRODUCT WHERE SCONTRACTID='" + CommonFunction.ReplaceInjection("" + TruckData[0]) + @"' 
AND STRUCKID='" + CommonFunction.ReplaceInjection("" + values[1]) + @"' AND NPLANID='" + CommonFunction.ReplaceInjection("" + values[12]) + "' ");//--AND TO_DATE(DCHECK,'DD/MM/YYYY')=TO_DATE('" + CommonFunction.ReplaceInjection("" + Convert.ToDateTime("" + values[10]).ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','DD/MM/YYYY') 
                                    }
                                    else
                                    {
                                        SCHECKID_TAB2 = "" + Session["SCHECKID"];
                                    }
                                    if (SCHECKID_TAB2 == "")
                                    {
                                        #region TCHECKPRODUCT
                                        ///TCHECKPRODUCT
                                        ctrck.SCHECKID = SCHECKID_TAB2 != "" ? SCHECKID_TAB2 : "";//send blank for gen new id
                                        ctrck.NPLANID = "" + values[12];
                                        ctrck.STERMINALID = "" + Session["SVDID"];
                                        ctrck.SCONTRACTID = "" + TruckData[0];
                                        ctrck.CGROUPCHECK = "1";//fix หน้านี้เป็นการทำงานของ หน้าคลัง
                                        ctrck.CCLEAN = "0";
                                        ctrck.STRUCKID = "" + values[1];
                                        ctrck.SHEADERREGISTERNO = "" + values[2];
                                        ctrck.STRAILERREGISTERNO = "" + values[4];
                                        ctrck.CMA = "0";//2 hold ,1 mainternent ,0 ปกติ
                                        ctrck.NDAY_MA = "0";
                                        ctrck.SCREATE = "" + Session["UserID"];
                                        ctrck.SUPDATE = "" + Session["UserID"];

                                        ctrck.Insert();
                                        SCHECKID_TAB2 = ctrck.SCHECKID;//BackUp SCHECKID for Child rows.

                                        #endregion
                                    }
                                    #region TCHECKPRODUCTITEM
                                    chkItem.SCHECKID = SCHECKID_TAB2;
                                    chkItem.SCHECKLISTID = "0";
                                    //chkItem.CMA = "0";
                                    chkItem.NPOINT = "0";
                                    chkItem.SVERSIONLIST = "0";
                                    chkItem.STYPECHECKLISTID = "0";
                                    //chkItem.NDAY_MA = txtNMA.Text == "" ? "0" : txtNMA.Text;
                                    chkItem.COTHER = cbxOtherIssue.Checked ? "1" : "0";
                                    chkItem.SOTHERREMARK = cbxOtherIssue.Checked ? txtOtherIssue.Text : "";
                                    //chkItem.SREMARK = txtDetailIssue.Text;
                                    chkItem.SCREATE = "" + Session["UserID"];
                                    chkItem.SUPDATE = "" + Session["UserID"];

                                    chkItem.InsertTab2(TruckData[0] + "", ArrayIssueData[1] + "", Convert.ToDateTime("" + values[10]).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "", txtDetailIssue.Text);
                                    #endregion
                                    #region Loss
                                    if (cbxHasLoss.Checked)
                                    {
                                        /*"STRUCKID", "SHEADID", "SHEADREGISTERNO", "STRAILID", "STRAILERREGISTERNO",  
                                         * "CMA", "ICMA", "CPASSED", "CCONFIRM", "SDELIVERYNO", 
                                         * "DDELIVERY", "STIMEWINDOW", "NPLANID", "SPLANLISTID", "NDROP", 
                                         * "SSHIPTO", "STERMINALID", "SVENDORID" ,"SHIPMENT_NO" ,"NVALUE"*/
                                        TLOSS loss = new TLOSS(this.Page, connection);
                                        loss.SLOSSID = txtLossID.Text.Equals("") ? "" : txtLossID.Text;
                                        loss.DELIVERY_NO = "" + values[9];
                                        loss.SHIPMENT_NO = "" + values[18];
                                        loss.TRUCK_ID = "" + values[1];
                                        loss.TRANS_ID = "" + values[17];
                                        loss.TRANS_TYPE = "2";//ปตท ว่าจ้าง ผู้รับจ้างขนส่ง
                                        loss.CONTRACT_ID = "" + TruckData[0];
                                        loss.REGISTER_NUM = "" + values[2];
                                        loss.DDELIVERY = "" + Convert.ToDateTime("" + values[10]).ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                                        loss.NORDERVOLUMN = "" + values[19];
                                        loss.NPLANID = "" + values[12];
                                        loss.NPLANLISTID = "" + values[13];
                                        loss.SFROMTERMINAL = "" + values[16];
                                        loss.STOTERMINAL = "" + values[15];
                                        loss.SSHIPTO = "" + values[15];
                                        loss.SCREATE = "" + Session["UserID"];
                                        loss.SUPDATE = "" + Session["UserID"];

                                        loss.SPRODUCTID = "" + cbxProduct.Value;
                                        loss.NLOSSVOLUMN = "" + (double.TryParse("" + txtnLoss.Text.Trim(), out defDouble) ? double.Parse("" + txtnLoss.Text.Trim()) : 0);
                                        loss.NPRODUCTUNITID = "" + cbxProdUnits.Value;

                                        //loss.DCREATE = "";
                                        //loss.DUPDATE = "";
                                        loss.SCHECKID = SCHECKID_TAB2;

                                        loss.Insert();
                                    }
                                    #endregion
                                    #region Attchment File
                                    if (Session["DataAttechment"] != null)
                                    {
                                        using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                                        {
                                            if (con.State == ConnectionState.Closed) con.Open();
                                            DataTable dtDataAttechment = ((DataTable)Session["DataAttechment"]);
                                            foreach (DataRow drDataAttechment in dtDataAttechment.Select("cRowFlag='Page'"))
                                            {
                                                OracleCommand ora_cmd = new OracleCommand(@"INSERT INTO TCHECKTRUCKATTACHMENT ( SCHECKID, NATTACHMENT, STRUCKID,SCONTRACTID, SATTACHTYPEID, SPATH,SFILE, SSYSFILE, DCREATE,SCREATE, DUPDATE, SUPDATE) 
 VALUES ( :S_CHECKID,FC_GENID_TCHECKTRUCKATTACHMENT(:S_CONTRACTID, :S_TRUCKID ,SYSDATE),:S_TRUCKID, :S_CONTRACTID,:S_ATTACHTYPEID,:S_PATH, :S_FILE,:S_SYSFILE, SYSDATE, :S_CREATE, SYSDATE,:S_UPDATE ) ", con);
                                                ora_cmd.Parameters.Add(":S_CHECKID", OracleType.VarChar).Value = SCHECKID_TAB2;
                                                ora_cmd.Parameters.Add(":S_TRUCKID", OracleType.VarChar).Value = "" + drDataAttechment["STRUCKID"];
                                                ora_cmd.Parameters.Add(":S_CONTRACTID", OracleType.VarChar).Value = "" + drDataAttechment["SCONTRACTID"];
                                                ora_cmd.Parameters.Add(":S_ATTACHTYPEID", OracleType.VarChar).Value = "2";
                                                ora_cmd.Parameters.Add(":S_PATH", OracleType.VarChar).Value = "" + drDataAttechment["SPATH"];
                                                ora_cmd.Parameters.Add(":S_FILE", OracleType.VarChar).Value = "" + drDataAttechment["SFILE"];
                                                ora_cmd.Parameters.Add(":S_SYSFILE", OracleType.VarChar).Value = "" + drDataAttechment["SSYSFILE"];
                                                ora_cmd.Parameters.Add(":S_CREATE", OracleType.VarChar).Value = "" + Session["SVDID"];
                                                ora_cmd.Parameters.Add(":S_UPDATE", OracleType.VarChar).Value = "" + Session["SVDID"];
                                                ora_cmd.ExecuteNonQuery();
                                            }
                                        }
                                    }
                                    #endregion
                                    using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                                    {
                                        if (con.State == ConnectionState.Closed) con.Open();
                                        OracleCommand ora_cmd = new OracleCommand("UPDATE TCHECKPRODUCT SET CHASLOSS=:C_HASLOSS ,CHASDIRTY=:C_HASDIRTY WHERE SCHECKID=:S_CHECKID ", con);
                                        ora_cmd.Parameters.Add(":C_HASLOSS", OracleType.VarChar).Value = (cbxHasLoss.Checked ? "1" : "0");
                                        ora_cmd.Parameters.Add(":C_HASDIRTY", OracleType.VarChar).Value = (cbxHasDirty.Checked ? "1" : "0");
                                        ora_cmd.Parameters.Add(":S_CHECKID", OracleType.VarChar).Value = CommonFunction.ReplaceInjection(SCHECKID_TAB2);
                                        ora_cmd.ExecuteNonQuery();
                                    }
                                    ((ASPxGridView)sender).CancelEdit();
                                    #endregion

                                }
                                else { }

                                #endregion
                            }
                            #endregion
                        }
                        break;
                }

                break;
        }
    }
    protected void gvwGroupCheckList_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        string smode = "imbissue";
        dynamic xgvwData = ((ASPxGridView)sender).GetRowValues(e.VisibleIndex, "SGROUPID", "GOCL_CACTIVE");
        ASPxGridView gvwItemCheckList = ((ASPxGridView)sender).FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "gvwItemCheckList") as ASPxGridView;
        if (gvwItemCheckList != null)
        {

            DataView dvCheckLists = (DataView)sdsCheckLists.Select(DataSourceSelectArguments.Empty);

            // ((ASPxPageControl)gvw.FindEditFormTemplateControl("pageControl")).TabPages[0].Text = "ปัญหาการขนส่ง";
            if (Request.Params["__CALLBACKPARAM"].EndsWith("imbIssueProd;") || ((ASPxTextBox)gvw.FindEditFormTemplateControl("txtMode")).Text.Equals("imbissueprod"))
            {//เป้นการตรวจสภาพสินค้า
                smode = "imbissueprod";
                dvCheckLists = (DataView)sdsCheckListsProd.Select(DataSourceSelectArguments.Empty);
                //((ASPxPageControl)gvw.FindEditFormTemplateControl("pageControl")).TabPages[0].Text = "ปัญหาผลิตภัณฑ์";
            }
            //((ASPxPageControl)gvw.FindEditFormTemplateControl("pageControl")).TabPages[0].Text = ((ASPxTextBox)gvw.FindEditFormTemplateControl("txtMode")).Text.Equals("imbissueprod") ? "ปัญหาผลิตภัณฑ์" : "ปัญหาการขนส่ง";
            DataRow[] drCheckLists = dvCheckLists.ToTable().Select("SGROUPID='" + xgvwData[0] + "'");
            DataTable dtCheckLists = drCheckLists.CopyToDataTable<DataRow>();

            gvwItemCheckList.DataSource = dtCheckLists;
            gvwItemCheckList.DataBind();
            if (Session["IssueData"] != null)
            {
                string[] ArrayIssueData = Session["IssueData"].ToString().Length > 0 ? Session["IssueData"].ToString().Split('$') : ("").Split('$');
                string[] ArrayTruckData = ArrayIssueData.Length > 0 ? ArrayIssueData[1].Split('^') : ("").Split('^');
                if (ArrayTruckData.Length > 0)
                    BindCheckListData(gvwItemCheckList, "" + ArrayTruckData[0], "" + ArrayTruckData[1], smode);
            }

            dvCheckLists.Dispose();
            dtCheckLists.Dispose();
        }
    }
    //UploadControl
    protected void UploadControl_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        try
        {
            e.CallbackData = SavePostedFiles(e.UploadedFile);
        }
        catch (Exception ex)
        {
            e.IsValid = false;
            e.ErrorText = ex.Message;
        }
    }
    #endregion
    #region Functions
    protected bool ReducePoint(string REDUCETYPE, string PROCESSID, params string[] sArrayParams)
    {//SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT
        bool IsReduce = true;
        TREDUCEPOINT repoint = new TREDUCEPOINT(this.Page, connection);
        repoint.NREDUCEID = "";
        //repoint.DREDUCE = "";
        repoint.CACTIVE = "1";
        repoint.SREDUCEBY = "" + Session["UserID"];
        repoint.SREDUCETYPE = REDUCETYPE;
        repoint.SPROCESSID = PROCESSID;
        repoint.SREDUCENAME = "" + sArrayParams[0];
        repoint.SCHECKLISTID = "" + sArrayParams[1];
        repoint.STOPICID = "" + sArrayParams[2];
        repoint.SVERSIONLIST = "" + sArrayParams[3];
        repoint.NPOINT = "" + sArrayParams[4];
        repoint.SREFERENCEID = "" + sArrayParams[5];
        repoint.SCONTRACTID = "" + sArrayParams[6];
        repoint.SHEADID = "" + sArrayParams[7];
        repoint.SHEADREGISTERNO = "" + sArrayParams[8];
        repoint.STRAILERID = "" + sArrayParams[9];
        repoint.STRAILERREGISTERNO = "" + sArrayParams[10];

        repoint.Insert();
        return IsReduce;
    }
    void BindData()
    {

        Cache.Remove(sds.CacheKeyDependency);
        Cache[sds.CacheKeyDependency] = new object();
        sds.Select(new System.Web.UI.DataSourceSelectArguments());
        string strsql = @"SELECT rownum SKEYID,V_PLANSCHEDULE.SDELIVERYNO ,V_PLANSCHEDULE.NPLANID ,V_PLANSCHEDULE.SPLANLISTID ,V_PLANSCHEDULE.NDROP ,V_PLANSCHEDULE.NVALUE ,V_PLANSCHEDULE.SSHIPTO ,V_PLANSCHEDULE.SPRODTYPE ,V_PLANSCHEDULE.SPRODNAME
,V_PLANSCHEDULE.DDELIVERY,V_PLANSCHEDULE.STIMEWINDOW,V_PLANSCHEDULE.CFIFO ,V_PLANSCHEDULE.STERMINALID  ,V_PLANSCHEDULE.DEPO_ID,V_PLANSCHEDULE.STRUCKID
,CASE WHEN  NVL(V_PLANSCHEDULE.SHEADNO,'-')='-' THEN V_PLANSCHEDULE.SHEADREGISTERNO ELSE V_PLANSCHEDULE.SHEADNO END SHEADREGISTERNO
,CASE WHEN  NVL(V_PLANSCHEDULE.STRAILERNO,'-')='-' THEN V_PLANSCHEDULE.STRAILERREGISTERNO ELSE V_PLANSCHEDULE.STRAILERNO END STRAILERREGISTERNO
,V_PLANSCHEDULE.SVENDORID ,V_PLANSCHEDULE.DPLAN  ,V_PLANSCHEDULE.CCONFIRM V_PLANSCHEDULE_CCONFIRM
,V_PLANSCHEDULE.CCHECKTRUCKA,NVL(V_PLANSCHEDULE.CCOMMIT,'1') CCONFIRM,V_PLANSCHEDULE.CCHECKTRUCKB,V_PLANSCHEDULE.CCHECKPRODUCT
,TRCK_HEAD.STRUCKID SHEADID
,TRCK_HEAD.STRAILERID STRAILID
,CASE WHEN NVL(TRCK_HEAD.CHOLD,'0')='1' THEN 'HOLD' 
ELSE (
    CASE WHEN NVL(TRCK_HEAD.CACCIDENT,'0')='1' THEN (CASE WHEN NVL(TRCK_HEAD.CSTATUS,'0')='1' THEN 'HOLD' 
    ELSE (
            CASE WHEN NVL(V_CHECKTRUCK.CMA,'0')='2' THEN 'HOLD'
            ELSE 
            (
                CASE WHEN NVL(V_CHECKTRUCK.CMA,'0')='1' THEN 'EDIT'||NVL(V_CHECKTRUCK.NDAY_MA,'0')||'DAY'  ELSE 'OK' END
            ) END
    ) END ) 
    ELSE ( CASE WHEN NVL(V_CHECKTRUCK.CMA,'0')='2' THEN 'HOLD'
            ELSE 
            (
                CASE WHEN NVL(V_CHECKTRUCK.CMA,'0')='1' THEN 'EDIT'||NVL(V_CHECKTRUCK.NDAY_MA,'0')||'DAY'  ELSE 'OK' END
            ) END)
    END
) END sStatus 
,'1' CPASSED , SYSDATE DARRIVE ,'' SHIPMENT_NO
,NVL(TRCK_HEAD.CHOLD,'0') CHOLD ,NVL(TRCK_HEAD.CACCIDENT,'0') CACCIDENT ,NVL(TRCK_HEAD.CSTATUS,'0') CSTATUS
,NVL(V_CHECKTRUCK.CCLEAN,'0') CCLEAN,NVL(V_CHECKTRUCK.CMA,'0') CMA ,NVL(V_CHECKTRUCK.NDAY_MA,'0') NDAY_MA ,NVL(V_CHECKTRUCK.COK,'0') COK ,NVL(V_CHECKTRUCK.ICMA,'0') ICMA ,NVL(V_CHECKTRUCK.CEDITED,'0')  CEDITED
FROM (
      SELECT LPAD(PLNLST.SDELIVERYNO,10,'0') SDELIVERYNO ,PLNLST.NPLANID ,PLNLST.SPLANLISTID ,PLNLST.NDROP ,PLNLST.NVALUE ,PLNLST.SSHIPTO ,PLNLST.SPRODTYPE ,PLNLST.SPRODNAME
   ,PLN.DDELIVERY ,PLN.STIMEWINDOW ,PLN.CFIFO ,PLN.STERMINALID ,PLN.STRUCKID, PLN.SHEADNO,PLN.SHEADREGISTERNO ,PLN.STRAILERNO ,PLN.STRAILERREGISTERNO ,PLN.SVENDORID ,PLN.DPLAN ,PLN.CCONFIRM
    ,CASE WHEN NVL(PLN.SHEADNO,'#')='#' THEN PLN.SHEADREGISTERNO ELSE PLN.SHEADNO END HEADREGISTERNO 
    ,CASE WHEN NVL(PLN.STRAILERNO,'#')='#' THEN PLN.STRAILERREGISTERNO ELSE PLN.STRAILERNO END TRAILERREGISTERNO
   ,NVL(plnlst.CCHECKTRUCKA,'0') CCHECKTRUCKA,NVL(plnlst.CCHECKTRUCKB,'0') CCHECKTRUCKB ,NVL(plnlst.CCHECKPRODUCT,'0') CCHECKPRODUCT,PLNLST.CCOMMIT
   ,DELI.SHIP_TO,CUST.DEPO_ID
    FROM TPlanScheduleList plnlst
    LEFT JOIN TPLANSCHEDULE pln ON PLN.NPLANID = PLNLST.NPLANID
    LEFT JOIN TDELIVERY DELI ON PLNLST.SDELIVERYNO=DELI.DELIVERY_NO
    LEFT JOIN TCUSTOMER CUST ON CUST.SHIP_TO =DELI.SHIP_TO
    WHERE PLN.CACTIVE='1'
    AND plnlst.CACTIVE = '1'
    AND NVL(CCONFIRM,'0') = '1' AND NVL(plnlst.CCHECKTRUCKA,'0') = '1'
) V_PLANSCHEDULE
LEFT JOIN TTRUCK TRCK_HEAD ON REPLACE(REPLACE(REPLACE(TRCK_HEAD.SHEADREGISTERNO,'-',''),'.',''),' ','')= REPLACE(REPLACE(REPLACE(V_PLANSCHEDULE.SHEADREGISTERNO,'-',''),'.',''),' ','')
LEFT JOIN (
    SELECT CHKTRCK.SCONTRACTID ,CHKTRCK.STRUCKID ,CHKTRCK.SHEADERREGISTERNO ,CHKTRCK.STRAILERREGISTERNO ,CHKTRCK.CCLEAN ,CHKTRCK.CMA  ,CHKTRCK.NDAY_MA ,CHKTRCK.COK
    , NVL(ICHKTRCK.CEDITED,'0') CEDITED ,ICHKTRCK.CMA ICMA --,ICHKTRCK.DBEGIN_LIST ,ICHKTRCK.DEND_LIST
    FROM TCHECKTRUCK CHKTRCK
    LEFT JOIN TCHECKTRUCKITEM ICHKTRCK ON CHKTRCK.SCHECKID=ICHKTRCK.SCHECKID
    WHERE NVL(CCLEAN,'0')='0' AND NVL(COK,'0')='0' AND  NVL(ICHKTRCK.CEDITED,'0')='0' AND SCHECKLISTID!='0'
    GROUP BY CHKTRCK.SCONTRACTID ,CHKTRCK.STRUCKID ,CHKTRCK.SHEADERREGISTERNO ,CHKTRCK.STRAILERREGISTERNO ,CHKTRCK.CCLEAN ,CHKTRCK.CMA  ,CHKTRCK.NDAY_MA ,CHKTRCK.COK, NVL(ICHKTRCK.CEDITED,'0'),ICHKTRCK.CMA 
) V_CHECKTRUCK ON TRCK_HEAD.STRUCKID=V_CHECKTRUCK.STRUCKID
--LEFT JOIN TTRUCK TRCK_TRAIL ON REPLACE(REPLACE(REPLACE(TRCK_TRAIL.STRAILERREGISTERNO,'-',''),'.',''),' ','')= REPLACE(REPLACE(REPLACE(V_PLANSCHEDULE.STRAILERREGISTERNO,'-',''),'.',''),' ','')
WHERE 1=1 AND V_PLANSCHEDULE.SDELIVERYNO LIKE '008%'
AND (V_PLANSCHEDULE.SDELIVERYNO||(CASE WHEN  NVL(V_PLANSCHEDULE.SHEADNO,'-')='-' THEN V_PLANSCHEDULE.SHEADREGISTERNO ELSE V_PLANSCHEDULE.SHEADNO END)||(CASE WHEN  NVL(V_PLANSCHEDULE.STRAILERNO,'-')='-' THEN V_PLANSCHEDULE.STRAILERREGISTERNO ELSE V_PLANSCHEDULE.STRAILERNO END)) LIKE '%'||NVL(:S_KEYWORD,(V_PLANSCHEDULE.SDELIVERYNO||(CASE WHEN  NVL(V_PLANSCHEDULE.SHEADNO,'-')='-' THEN V_PLANSCHEDULE.SHEADREGISTERNO ELSE V_PLANSCHEDULE.SHEADNO END)||(CASE WHEN  NVL(V_PLANSCHEDULE.STRAILERNO,'-')='-' THEN V_PLANSCHEDULE.STRAILERREGISTERNO ELSE V_PLANSCHEDULE.STRAILERNO END)))||'%' 
AND V_PLANSCHEDULE.DDELIVERY BETWEEN TO_DATE(:S_BEGIN,'DD/MM/YYYY  HH24:MI:SS') AND TO_DATE(:S_END,'DD/MM/YYYY  HH24:MI:SS') 
{0}", scondition = "";
        scondition += (cboTerminal.Value + "" != "") ? " AND V_PLANSCHEDULE.SSHIPTO LIKE'%" + CommonFunction.ReplaceInjection(cboTerminal.Value + "") + "'" : "";
        scondition += (cboTimeWindow.Value + "" != "") ? " AND  V_PLANSCHEDULE.STIMEWINDOW = '" + CommonFunction.ReplaceInjection(cboTimeWindow.Value + "") + "'" : "";

        sds.SelectCommand = string.Format(strsql, scondition);
        sds.SelectParameters.Clear();
        sds.SelectParameters.Add(":S_KEYWORD", txtSearch.Text.Trim());
        sds.SelectParameters.Add(":S_BEGIN", dteEnd.Date.ToString("dd/MM/yyyy 00:00:00", new CultureInfo("en-US")));
        sds.SelectParameters.Add(":S_END", dteEnd.Date.ToString("dd/MM/yyyy 23:59:59", new CultureInfo("en-US")));

        sds.DataBind();
        gvw.DataBind();
        lblConfirmCar.Text = ((DataTable)((DataView)sds.Select(DataSourceSelectArguments.Empty)).ToTable()).Select("CCHECKTRUCKB='0'").Length + "";

    }
    protected void BackUpData(int visibleindex, ASPxGridView gvw)
    {
        dynamic values = gvw.GetRowValues(visibleindex, "STRUCKID", "SHEADID", "SHEADREGISTERNO", "STRAILID", "STRAILERREGISTERNO", "CMA", "ICMA", "CPASSED", "CCONFIRM", "SDELIVERYNO", "DDELIVERY", "STIMEWINDOW", "NPLANID", "SPLANLISTID", "NDROP");

        //string[] ArrayIssueData = (Session["IssueData"] + "").Split('$')[1].Replace("#", "").Split('^');
        DataTable dtData = PrepareDataTable("ss_data", "" /*+ values[10]*/, "" /*+ ArrayIssueData[0]*/, "SCONTRACTID", "DDATE", "STRUCKID", "SHEADID", "SHEADREGISTERNO", "STRAILERID", "STRAILERREGISTERNO", "CPASSED", "CSEND", "SDELIVERYNO", "DDELIVERY", "STIMEWINDOW", "NPLANID", "SPLANLISTID", "NDROP", "CCHECKTRUCKA");
        DataRow dr;
        int csend = 0;
        for (int rows = 0; rows < gvw.VisibleRowCount; rows++)
        {
            GridViewDataColumn EventCol = (GridViewDataColumn)gvw.Columns["#"];
            GridViewDataColumn IssueCol = (GridViewDataColumn)gvw.Columns["ใบตรวจสภาพรถ"];
            ASPxButton imbconfirm = gvw.FindRowCellTemplateControl(rows, EventCol, "imbconfirm") as ASPxButton;
            ASPxButton imbcancel = gvw.FindRowCellTemplateControl(rows, EventCol, "imbcancel") as ASPxButton;
            ASPxTextBox txtconfirm = gvw.FindRowCellTemplateControl(rows, EventCol, "txtconfirm") as ASPxTextBox;
            ASPxTextBox txtPassed = gvw.FindRowCellTemplateControl(rows, IssueCol, "txtPassed") as ASPxTextBox;

            string val = "" + txtconfirm.Value;
            dynamic dataGVWRows = gvw.GetRowValues(rows, "STRUCKID", "SHEADID", "SHEADREGISTERNO", "STRAILID", "CCONFIRM", "CPASSED", "SDELIVERYNO", "DDELIVERY", "STIMEWINDOW", "NPLANID", "SPLANLISTID", "NDROP", "CCHECKTRUCKA");
            dr = dtData.NewRow();
            dr["SCONTRACTID"] = "";
            dr["DDATE"] = "";
            dr["STRUCKID"] = "";
            dr["STRAILERREGISTERNO"] = "";
            dr["SHEADID"] = "" + dataGVWRows[1];
            dr["SHEADREGISTERNO"] = "" + dataGVWRows[2];
            dr["STRAILERID"] = "" + dataGVWRows[3];
            dr["SDELIVERYNO"] = "" + dataGVWRows[6];
            dr["DDELIVERY"] = "" + dataGVWRows[7];
            dr["STIMEWINDOW"] = "" + dataGVWRows[8];
            dr["NPLANID"] = "" + dataGVWRows[9];
            dr["SPLANLISTID"] = "" + dataGVWRows[10];
            dr["NDROP"] = "" + dataGVWRows[11];
            dr["CPASSED"] = (val == "1") ? "" + txtPassed.Value : "0";//ถ้าไม่สามารถยืนยันรถได้ จะตีเป็นตรวจสถาพไม่ผ่านโดยอัตโนมัติ
            dr["CCHECKTRUCKA"] = val;
            csend += ((val == "1") ? 1 : 0);
            dtData.Rows.Add(dr);
        }
        Session["ss_data"] = dtData;
    }
    protected DataTable PrepareDataTable(string ss_name, string sdate, string contractid, params string[] fields)
    {
        DataTable dtData = new DataTable();
        if (Session[ss_name] + "" != "")
        {
            dtData = (DataTable)Session[ss_name];
            foreach (DataRow drDel in dtData.Select("DDATE='" + sdate + "' AND SCONTRACTID='" + contractid + "'"))
            {
                int idx = dtData.Rows.IndexOf(drDel);
                dtData.Rows[idx].Delete();
            }
        }
        else
        {
            foreach (string field_type in fields)
            {
                dtData.Columns.Add(field_type + "");
            }
        }
        return dtData;
    }
    protected void BindCheckListData(ASPxGridView gvw, string SCONTRACTID, string STRUCKID, string mode)
    {
        string qry = @"SELECT CHKTRCK.*, ICHKTRCK.*
,ICHKTRCK.CMA ICMA,ICHKTRCK.NDAY_MA INDAY_MA 
FROM (
    SELECT distinct * FROM TCHECKTRUCK WHERE 1=1 AND STRUCKID='" + STRUCKID + @"'
)CHKTRCK
LEFT JOIN (
    SELECT TCHECKTRUCKITEM.*, TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME SCREATENAME
    FROM TCHECKTRUCKITEM LEFT JOIN TUser ON TCHECKTRUCKITEM.SCREATE=TUSER.SUID
    WHERE 1=1 --AND NVL(CCHECKED,'0')='0'
    and TCHECKTRUCKITEM.SCHECKID IN(SELECT distinct SCHECKID FROM TCHECKTRUCK WHERE 1=1 AND STRUCKID='" + STRUCKID + @"')
)ICHKTRCK ON CHKTRCK.SCHECKID=ICHKTRCK.SCHECKID
WHERE 1=1 --AND NVL(COK,'0')='0'";

        if (mode == "imbissueprod")
        {
            qry = @"SELECT CHKTRCK.*, ICHKTRCK.*
,'0' ICMA,0 INDAY_MA ,SYSDATE DEND_LIST ,SYSDATE DBEGIN_LIST
FROM (
    SELECT DISTINCT * FROM TCHECKPRODUCT WHERE 1=1 AND STRUCKID='" + STRUCKID + @"'
)CHKTRCK
LEFT JOIN (
    SELECT TCHECKPRODUCTITEM.*, TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME SCREATENAME
    FROM TCHECKPRODUCTITEM LEFT JOIN TUser ON TCHECKPRODUCTITEM.SCREATE=TUSER.SUID
    WHERE 1=1 --AND NVL(CCHECKED,'0')='0'
    and TCHECKPRODUCTITEM.SCHECKID IN(SELECT distinct SCHECKID FROM TCHECKPRODUCT WHERE 1=1 AND STRUCKID='" + STRUCKID + @"')
)ICHKTRCK ON CHKTRCK.SCHECKID=ICHKTRCK.SCHECKID
WHERE 1=1 --AND NVL(COK,'0')='0' ";
        }
        DataTable dtCheckData = CommonFunction.Get_Data(connection, qry);
        for (int idxgvw = 0; idxgvw < gvw.VisibleRowCount; idxgvw++)
        {
            dynamic ArrayRowValues = gvw.GetRowValues(idxgvw, "SCHECKLISTID", "SCHECKLISTNAME", "STOPICID", "SVERSION", "SVERSIONLIST", "NLIST", "NDAY_MA", "NPOINT", "CCUT", "CBAN", "STYPECHECKLISTID");
            ASPxTextBox txtSCHECKLISTID = gvw.FindRowCellTemplateControl(idxgvw, (GridViewDataColumn)gvw.Columns[0], "txtSCHECKLISTID") as ASPxTextBox;
            ASPxCheckBox cbxSCHECKLISTID = gvw.FindRowCellTemplateControl(idxgvw, (GridViewDataColumn)gvw.Columns[0], "cbxSCHECKLISTID") as ASPxCheckBox;
            ASPxTextBox txtIsChecked = gvw.FindRowCellTemplateControl(idxgvw, (GridViewDataColumn)gvw.Columns[0], "txtIsChecked") as ASPxTextBox;
            ASPxLabel lblCHOLD = gvw.FindRowCellTemplateControl(idxgvw, (GridViewDataColumn)gvw.Columns[0], "lblCHOLD") as ASPxLabel;
            ASPxLabel lblMAnDay = gvw.FindRowCellTemplateControl(idxgvw, (GridViewDataColumn)gvw.Columns[0], "lblMAnDay") as ASPxLabel;
            Label lblCheckBy = gvw.FindRowCellTemplateControl(idxgvw, (GridViewDataColumn)gvw.Columns[0], "lblCheckBy") as Label;
            ASPxHyperLink lnkEdited = gvw.FindRowCellTemplateControl(idxgvw, (GridViewDataColumn)gvw.Columns[0], "lnkEdited") as ASPxHyperLink;
            string CBAN = "", CCUT = "", NDAY_MA = "", ReportBy = "", SCHECKID = ""; int HasCheckList = 0;

            foreach (DataRow drCheckData in dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "' AND STYPECHECKLISTID='" + ArrayRowValues[10] + "'"))
            {
                HasCheckList = 1;
                SCHECKID = drCheckData["SCHECKID"] + "";
                ReportBy = drCheckData["SCREATENAME"] + "";
                break;
            }
            CBAN = dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "' AND ICMA='2'").Length > 0 ? "1" : "0";
            CCUT = dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "' AND ICMA='1'").Length > 0 ? "1" : "0";
            DataRow[] drBan = dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "' AND ICMA='2'", "DEND_LIST ,DBEGIN_LIST");
            DataRow[] drNDAYMA = dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "' AND ICMA='1'", "DEND_LIST ,DBEGIN_LIST");
            NDAY_MA = drNDAYMA.Length > 0 ? "" + drNDAYMA[0]["INDAY_MA"] : "0";
            if (HasCheckList > 0)//(dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "'").Length > 0)
            {
                string SCHECKLISTID = ((CBAN == "1") ? "2" : ((CCUT == "1") ? "1" : "0"));
                txtSCHECKLISTID.Text = ((CBAN == "1") ? "2" : ((CCUT == "1") ? "1" : "0"));
                lblCHOLD.ClientVisible = (SCHECKLISTID == "2") ? true : false;
                lblCHOLD.Text = "ห้ามวิ่ง" + (drBan.Length > 0 ? "(เมื่อ " + Convert.ToDateTime("" + drBan[0]["DEND_LIST"]).ToString("dd MMM yyyy") + " )" : "");
                lblMAnDay.ClientVisible = (SCHECKLISTID == "1") ? true : false;
                lblMAnDay.Text = (NDAY_MA != "") ? "แก้ไขภายใน " + NDAY_MA + " วัน" + (drNDAYMA.Length > 0 ? "(ภายใน " + Convert.ToDateTime("" + drNDAYMA[0]["DEND_LIST"]).ToString("dd MMM yyyy") + " )" : "") : "";
            }
            cbxSCHECKLISTID.Checked = (HasCheckList > 0) ? true : false;
            cbxSCHECKLISTID.ClientEnabled = !cbxSCHECKLISTID.Checked;
            txtIsChecked.Text = (HasCheckList > 0) ? "1" : "0";
            if (cbxSCHECKLISTID.Checked)
            {
                lblCheckBy.Text = "<img title='" + ReportBy + "' src='images/ic_owner.gif' />";
                lnkEdited.Text = "<img title='คลิีกเพื่อปรับสถานะเป็นตรวจสอบเรียบร้อย' alt='คลิีกเพื่อปรับสถานะเป็นตรวจสอบเรียบร้อย' src='images/btnLoop1.gif' />";
                lnkEdited.ClientSideEvents.Click = "function(s,e){UpdateCheckListItem('" + lnkEdited.ClientID + "','" + SCONTRACTID + "','" + STRUCKID + "','" + ArrayRowValues[0] + "','" + SCHECKID + "');}";
            }
            lblCheckBy.Visible = cbxSCHECKLISTID.Checked;
            lnkEdited.Visible = cbxSCHECKLISTID.Checked;


        }
        dtCheckData.Dispose();
    }
    string SavePostedFiles(UploadedFile uploadedFile)
    {
        string mode = "" + Session["UploadMode"];
        string PathUploadDirectory = (mode.Equals("imbissueprod") ? UploadDirectory.Replace("EvidenceOfChecks", "EvidenceOfProducts") : UploadDirectory) + "/" + "";
        dynamic MasterData = gvw.GetRowValues(gvw.EditingRowVisibleIndex, "STRUCKID", "SHEADID", "SHEADREGISTERNO", "STRAILID", "STRAILERREGISTERNO", "CMA", "ICMA", "CPASSED", "CCONFIRM", "SDELIVERYNO", "DDELIVERY", "STIMEWINDOW", "NPLANID", "SPLANLISTID", "NDROP");
        string IssueData = "" + Session["IssueData"];
        /*
         0$58
         * TR4901412
         * กท.74-9080
         * 
         * #
        */
        string UploadDir = (mode.Equals("imbissueprod") ? UploadDirectory.Replace("EvidenceOfChecks", "EvidenceOfProducts") : UploadDirectory);
        string[] ArrayIssueData = IssueData.Split('^');
        PathUploadDirectory = ArrayIssueData[0].Remove(0, ArrayIssueData[0].Split('$')[0].Length + 1) + "/" + ArrayIssueData[1] + "/" + Convert.ToDateTime("" + MasterData[10]).ToString("MMddyyyy", new CultureInfo("en-US")) + "/";

        if (!uploadedFile.IsValid)
            return string.Empty;

        FileInfo fileInfo = new FileInfo(uploadedFile.FileName);
        if (!Directory.Exists(UploadDir + PathUploadDirectory)) Directory.CreateDirectory(MapPath(UploadDir + PathUploadDirectory));
        string resFileName = MapPath(UploadDir + PathUploadDirectory) + fileInfo.Name;

        if (File.Exists(resFileName))
        {
            string sPath = Path.GetDirectoryName(resFileName)
                , sFileName = Path.GetFileNameWithoutExtension(resFileName)
                , sFileType = Path.GetExtension(resFileName);
            int nExists = Directory.GetFiles(Path.GetDirectoryName(resFileName), "*" + Path.GetFileName(resFileName)).Length;
            sPath = sPath + "\\" + "(" + nExists + ")" + sFileName + sFileType;
            File.Copy(resFileName, sPath, true);
        }

        string fileLabel = fileInfo.Name;
        string fileLength = uploadedFile.ContentLength / 1024 + "K";
        #region temp Data Attechment
        {//temp Data Attechment 
            DataTable dtAttechment = new DataTable();
            #region prepaire temp data Attechment storage
            if (Session["DataAttechment"] == null)
            {
                dtAttechment.Columns.Add("SCHECKID", typeof(string));
                dtAttechment.Columns.Add("NATTACHMENT", typeof(string));
                dtAttechment.Columns.Add("STRUCKID", typeof(string));
                dtAttechment.Columns.Add("SCONTRACTID", typeof(string));
                dtAttechment.Columns.Add("SATTACHTYPEID", typeof(string));
                dtAttechment.Columns.Add("SPATH", typeof(string));
                dtAttechment.Columns.Add("SFILE", typeof(string));
                dtAttechment.Columns.Add("SSYSFILE", typeof(string));
                dtAttechment.Columns.Add("DCREATE", typeof(string));
                dtAttechment.Columns.Add("SCREATE", typeof(string));
                dtAttechment.Columns.Add("DUPDATE", typeof(string));
                dtAttechment.Columns.Add("SUPDATE", typeof(string));
                dtAttechment.Columns.Add("cRowFlag", typeof(string));
            }
            else
            {
                dtAttechment = (DataTable)Session["DataAttechment"];
                foreach (DataRow drRemove in dtAttechment.Select("cRowFlag='Page' AND SFILE='" + fileInfo.Name + "'")) dtAttechment.Rows[dtAttechment.Rows.IndexOf(drRemove)].Delete();

            }
            #endregion
            DataRow drAttechment;

            drAttechment = dtAttechment.NewRow();
            drAttechment["SCHECKID"] = "";
            drAttechment["NATTACHMENT"] = dtAttechment.Rows.Count + 1;
            drAttechment["SCONTRACTID"] = "" + ArrayIssueData[0].Remove(0, ArrayIssueData[0].Split('$')[0].Length + 1);
            drAttechment["STRUCKID"] = "" + ArrayIssueData[1];
            drAttechment["SATTACHTYPEID"] = mode.Equals("imbissueprod") ? "2" : "1";
            drAttechment["SPATH"] = "" + UploadDir + PathUploadDirectory;
            drAttechment["SFILE"] = "" + fileInfo.Name;
            drAttechment["SSYSFILE"] = "" + fileInfo.Name;
            drAttechment["DCREATE"] = "" + Convert.ToDateTime("" + MasterData[10]).ToString("MM/dd/yyyy", new CultureInfo("en-US"));
            drAttechment["SCREATE"] = "" + Session["SVDID"];
            drAttechment["DUPDATE"] = "";
            drAttechment["SUPDATE"] = "";
            drAttechment["cRowFlag"] = "Page";

            dtAttechment.Rows.Add(drAttechment);
            Session["DataAttechment"] = dtAttechment;

        }
        #endregion

        uploadedFile.SaveAs(resFileName);

        return string.Format("{0} ({1})|{2}#{3}", fileLabel, fileLength, fileInfo.Name, (UploadDir + PathUploadDirectory).Remove(0, 2));
    }
    #endregion

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}
