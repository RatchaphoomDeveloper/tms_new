﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="Admin_VisitForm_Item.aspx.cs" Inherits="Admin_VisitForm_Item" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <style type="text/css">
        .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"] {
            margin-left: 0px;
        }

        .checkbox label, .radio label {
            padding-right: 10px;
        }

        .required.control-label:after {
            content: "*";
            color: red;
        }

        .form-horizontal .control-label.text-left {
            text-align: left;
            left: 0px;
        }

        .GridColorHeader th {
            text-align: inherit !important;
            align-content: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-info" style="margin-top: 20px;">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>ข้อมูลคำถาม
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <div class="form-group form-horizontal row">
                                <label for="ddlGroupName" class="col-md-2 control-label">ชื่อหมวด/กลุ่ม</label>
                                <div class="col-md-3">
                                    <asp:DropDownList ID="ddlGroupName" runat="server" CssClass="form-control marginTp">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group form-horizontal row">
                                <label for="rdoOptionGroupStatus" class="col-md-2 control-label">สถานะการใช้งาน</label>
                                <div class="col-md-3 radio">
                                    <asp:RadioButtonList ID="rdoOptionGroupStatus" ClientIDMode="Static" runat="server" RepeatColumns="2" CellPadding="2" CellSpacing="2" RepeatLayout="Flow">
                                        <asp:ListItem Value="1" Selected="True">&nbsp;&nbsp;&nbsp;&nbsp;ใช้งาน</asp:ListItem>
                                        <asp:ListItem Value="0">&nbsp;&nbsp;&nbsp;&nbsp;ไม่ใช้งาน</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="form-group form-horizontal row">
                                <label for="txtItemName" class="col-md-2 control-label">หัวข้อคำถาม</label>
                                <div class="col-md-2">
                                    <asp:TextBox ID="txtItemName" runat="server" ClientIDMode="Static" CssClass="form-control input-md" MaxLength="500"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group form-horizontal row">
                                <label for="txtItemPoint" class="col-md-2 control-label">คะแนนคำนวณ</label>
                                <div class="col-md-2">
                                    <asp:TextBox ID="txtItemPoint" runat="server" ClientIDMode="Static" CssClass="form-control input-md" MaxLength="7"></asp:TextBox>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-info" style="margin-top: 20px;">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>รายการคำถาม
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:GridView ID="grvMainGroup" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                HorizontalAlign="Center" AutoGenerateColumns="false"   DataKeyNames="NGROUPID,NTYPEVISITFORMID"
                                EmptyDataText="[ ไม่มีข้อมูล ]" AllowPaging="True">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="10px" ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center" HeaderText="ลำดับ">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkActive" runat="server" Checked="false" Enabled="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageEdit" runat="server" ImageUrl="~/Images/document.gif"
                                                Width="16px" Height="16px" BorderWidth="0" Style="cursor: pointer" CommandName="RowSelect" CausesValidation="False" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                <PagerStyle CssClass="pagination-ys" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

