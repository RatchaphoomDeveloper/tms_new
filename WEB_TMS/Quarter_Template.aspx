﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="Quarter_Template.aspx.cs" Inherits="Quarter_Template" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
    <div class="form-horizontal">
        <div class="form-group">
            </div>
        <div class="form-group">
            </div>
        <div class="form-group">
            <center>
                <asp:Button Text="บันทึก"  ID="btnSave" OnClick="btnSave_Click" runat="server" CssClass="btn btn-default" />
            </center>
            </div>
        <h4 class="text-center">เอกสารแจ้งผลการประเมินการทำงานขนส่ง ประจำไตรมาส</h4>
        <div class="row">
            <div class="form-group">
            <div class="col-md-4">
            </div>
            <div  class="col-md-1 text-right">
                Font Name
                </div>
                <div  class="col-md-2">
               <asp:TextBox runat="server" ID="txtFont" CssClass="form-control" />
                </div>
                
                
            </div>
            <div class="col-md-4">
            </div>
                </div>
        <div class="row">
            <div class="form-group">
            <div class="col-md-4">
            </div>
            <div  class="col-md-1  text-right">
                Font Size
                </div>
                <div  class="col-md-2">
                <asp:TextBox runat="server" ID="txtSize" CssClass="form-control" />
                </div>
            <div class="col-md-4">
            </div>
                </div>
        </div>
        <div class="row">
            <div class="form-group">
            <div class="col-md-2">
            </div>
            <div class="">
            </div>
            <div class="col-md-2">
            </div>
                </div>
        </div>
        <div class="row">
            <div class="form-group">
            <div class="col-md-2">
            </div>
            <div class="">
                <dx:ASPxHtmlEditor ID="txtBody" style="width: 980px;" runat="server" >
                    <SettingsImageUpload UploadImageFolder="~/UploadFile">
                    </SettingsImageUpload>
                </dx:ASPxHtmlEditor>
            </div>
            <div class="col-md-2">
            </div>
                </div>
        </div>
        <br />
        <h4 class="text-center">email แจ้งผลการประเมิน ประจำไตรมาส</h4>
        <div class="row">
            <div class="form-group">
            <div class="col-md-2">
            </div>
            <div class="">
                <dx:ASPxHtmlEditor ID="txtEmail" style="width: 980px;" runat="server">
                    <SettingsImageUpload UploadImageFolder="~/UploadFile">
                    </SettingsImageUpload>
                </dx:ASPxHtmlEditor>
            </div>
            <div class="col-md-2">
            </div>
                </div>
        </div>
        </div>
    </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" Runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" Runat="Server">
</asp:Content>

