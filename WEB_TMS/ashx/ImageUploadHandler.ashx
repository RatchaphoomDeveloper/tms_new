﻿<%@ WebHandler Language="C#" Class="ImageUploadHandler" %>
/* Class="plupload.ImageUploadHandler"
using System;
using System.Web;

public class ImageUploadHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        context.Response.Write("Hello World");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}*/
using System;
using System.Drawing;
using System.IO;
using System.Collections.Generic;
using System.Web;
using Westwind.plUpload;
using System.Linq;


//namespace plupload
//{

/// <summary>
/// This class is an application level implementation of an uploader
/// 
/// This uploader subclasses plUploadFileHandler which downloads files
/// into a temporary folder (~/tempuploads) and then resizes the image,
/// renames it and copies it a final destination (~/UploadedImages)
/// 
/// This handler also deletes old files in both of those folders
/// just to keep the size of this demo reasonable.
/// </summary>
/// 




public class ImageUploadHandler : plUploadFileHandler, System.Web.SessionState.IRequiresSessionState
{
    //KM_Entities db = new KM_Entities();
    const string ImageStoragePath = "~/UploadFile/UploadedImagesCAR";
    public static int ImageHeight = 600;
    string strConn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;


    public ImageUploadHandler()
    {
        // Normally you'd set these values from config values
        FileUploadPhysicalPath = "~/UploadFile/temp/tempuploads";
        MaxUploadSize = 2000000;
        AllowedExtensions = ".jpg,.jpeg,.png,.gif,.bmp";
    }

    protected override void OnUploadCompleted(string fileName)
    {
        var Server = Context.Server;

        // Physical Path is auto-transformed
        var path = FileUploadPhysicalPath;
        var fullUploadedFileName = Path.Combine(path, fileName);

        // Typically you'd want to ensure that the filename is unique
        // Some ID from the database to correlate - here I use a static img_ prefix
        string[] nameFile = fileName.Split('.');
        string generatedFilename = "img_" + DateTime.Now.ToString("ddmmyyyyHHmmssfff") + "." + nameFile[nameFile.Length - 1];
        //string generatedFilename = "img" + "." + nameFile[1];

        string sUserID = HttpContext.Current.Session["UserID"] + "";
        string REQ_ID = HttpContext.Current.Session["REQ_ID"] + "";
        int NVERSION = int.Parse(HttpContext.Current.Session["NVERSION"] + "") + 1;
        string SPATH = HttpContext.Current.Session["SPATH"] + "/";


        string imagePath = Server.MapPath(ImageStoragePath);

        imagePath = (imagePath + "/" + REQ_ID).Replace("/", "\\");

        //if (!Directory.Exists(Server.MapPath("./") + PathTemp.Replace("/", "\\")))
        //{
        //    Directory.CreateDirectory(Server.MapPath("./") + PathTemp.Replace("/", "\\"));
        //}
        if (!Directory.Exists(Server.MapPath("./") + imagePath.Replace("/", "\\")))
        {
            Directory.CreateDirectory(imagePath.Replace("/", "\\"));
        }

        string QUERYINS = @"INSERT INTO TBL_CAR_IMAGE (CARIMAGE_ID,REQUEST_ID, FILE_NAME, FILE_SYSNAME, FILE_PATH, ISACTIVE_FLAG, CREATE_CODE, CREATE_DATE,  NVERSION) 
                            VALUES ( '" + CommonFunction.ReplaceInjection(GenerateID()) + "','" + CommonFunction.ReplaceInjection(REQ_ID) + "','" + CommonFunction.ReplaceInjection(fileName) + "', '" + CommonFunction.ReplaceInjection(generatedFilename) + "', '" + SPATH + "', 'Y', '" + CommonFunction.ReplaceInjection(sUserID) + "', SYSDATE, " + CommonFunction.ReplaceInjection(NVERSION + "") + ")";

        AddTODB(QUERYINS);

        try
        {
            // resize the image and write out in final image folder
            ResizeImage(fullUploadedFileName, Path.Combine(imagePath, generatedFilename), ImageHeight);

            // delete the temp file
            File.Delete(fullUploadedFileName);
        }
        catch (Exception ex)
        {
            WriteErrorResponse("Unable to write out uploaded file: " + ex.Message);
            return;
        }

        string relativePath = VirtualPathUtility.ToAbsolute(ImageStoragePath + "/" + REQ_ID);
        string finalImageUrl = relativePath + "/" + generatedFilename;


        // return just a string that contains the url path to the file
        WriteUploadCompletedMessage(finalImageUrl);



    }

    private void AddTODB(string strQuery)
    {
        using (System.Data.OracleClient.OracleConnection con = new System.Data.OracleClient.OracleConnection(strConn))
        {

            if (con.State == System.Data.ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }

            using (System.Data.OracleClient.OracleCommand com = new System.Data.OracleClient.OracleCommand(strQuery, con))
            {
                com.ExecuteNonQuery();
            }

            con.Close();

        }
    }

    protected override bool OnUploadStarted(int chunk, int chunks, string name)
    {
        // time out files after 15 minutes - temporary upload files
        DeleteTimedoutFiles(Path.Combine(FileUploadPhysicalPath, "*.*"), 900);

        // clean out final image folder too
        DeleteTimedoutFiles(Path.Combine(Context.Server.MapPath(ImageStoragePath), "*.*"), 900);

        return base.OnUploadStarted(chunk, chunks, name);
    }

    // these aren't needed in this example and with files in general
    // use these to stream data into some alternate data source
    // when directly inheriting from the base handler

    //protected override bool  OnUploadChunk(Stream chunkStream, int chunk, int chunks, string fileName)
    //{
    //     return base.OnUploadChunk(chunkStream, chunk, chunks, fileName);
    //}

    //protected override bool OnUploadChunkStarted(int chunk, int chunks, string fileName)
    //{
    //    return true;
    //}




    #region Sample Helpers
    /// <summary>
    /// Deletes files based on a file spec and a given timeout.
    /// This routine is useful for cleaning up temp files in 
    /// Web applications.
    /// </summary>
    /// <param name="filespec">A filespec that includes path and/or wildcards to select files</param>
    /// <param name="seconds">The timeout - if files are older than this timeout they are deleted</param>
    public static void DeleteTimedoutFiles(string filespec, int seconds)
    {
        string path = Path.GetDirectoryName(filespec);
        string spec = Path.GetFileName(filespec);
        string[] files = Directory.GetFiles(path, spec);

        foreach (string file in files)
        {
            try
            {
                if (File.GetLastWriteTimeUtc(file) < DateTime.UtcNow.AddSeconds(seconds * -1))
                    File.Delete(file);
            }
            catch { }  // ignore locked files
        }
    }

    /// <summary>
    /// Creates a resized bitmap from an existing image on disk. Resizes the image by 
    /// creating an aspect ratio safe image. Image is sized to the larger size of width
    /// height and then smaller size is adjusted by aspect ratio.
    /// 
    /// Image is returned as Bitmap - call Dispose() on the returned Bitmap object
    /// </summary>
    /// <param name="filename"></param>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <returns>Bitmap or null</returns>
    public static bool ResizeImage(string filename, string outputFilename,
                                   int height)
    {
        Bitmap bmpOut = null;

        try
        {
            Bitmap bmp = new Bitmap(filename);
            System.Drawing.Imaging.ImageFormat format = bmp.RawFormat;

            decimal ratio;
            int newWidth = 0;
            int newHeight = 0;

            //*** If the image is smaller than a thumbnail just return it
            if (bmp.Height < height)
            {
                if (outputFilename != filename)
                    bmp.Save(outputFilename);
                bmp.Dispose();
                return true;
            }

            ratio = (decimal)height / bmp.Height;
            newHeight = height;
            newWidth = Convert.ToInt32(bmp.Width * ratio);


            bmpOut = new Bitmap(newWidth, newHeight);
            Graphics g = Graphics.FromImage(bmpOut);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.FillRectangle(Brushes.White, 0, 0, newWidth, newHeight);
            g.DrawImage(bmp, 0, 0, newWidth, newHeight);

            bmp.Dispose();

            bmpOut.Save(outputFilename, format);
            bmpOut.Dispose();
        }
        catch (Exception ex)
        {
            var msg = ex.GetBaseException();
            return false;
        }

        return true;
    }

    #endregion
    private string GenerateID()
    {
        string _ID = "";
        string REQ_ID = HttpContext.Current.Session["REQ_ID"] + "";
        string NVER = "SELECT CARIMAGE_ID FROM TBL_CAR_IMAGE WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(REQ_ID) + "' ORDER BY CARIMAGE_ID DESC";
        System.Data.DataTable dt = CommonFunction.Get_Data(strConn, NVER);

        if (dt.Rows.Count > 0)
        {
            _ID = (int.Parse(dt.Rows[0]["CARIMAGE_ID"] + "") + 1) + "";
        }
        else
        {
            _ID = "1";
        }
        return _ID;
    }

}
//}