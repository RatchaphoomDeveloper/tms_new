﻿<%@ WebHandler Language="C#" Class="RemoveFileOnServer" %>

using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public class RemoveFileOnServer : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        string shtml = "0", trid = "", SCONTRACTID = "", STRUCKID = "", SCHECKID = "", NATTACHMENT = "",
           DATA = CommonFunction.ReplaceInjection(context.Request["data"]);
        //%5E
        string[] data = (DATA.Length > 0) ? DATA.Replace("%5E", "$").Split('$') : ("").Split('$');//contractid + '^' + truckid + '^' + checklistid + '^' + scheckid
        if (data.Length > 0)
        {
            //trid = CommonFunction.ReplaceInjection("" + data[0]);
            SCONTRACTID = CommonFunction.ReplaceInjection("" + data[0]);
            STRUCKID = CommonFunction.ReplaceInjection("" + data[1]);
            SCHECKID = CommonFunction.ReplaceInjection("" + data[2]);
            NATTACHMENT = CommonFunction.ReplaceInjection("" + data[3]);
        }
        int result = -1;
        //
        DataTable dtATTACHMENT = new DataTable();
        using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            dtATTACHMENT = CommonFunction.Get_Data(con, "SELECT * FROM TCHECKTRUCKATTACHMENT WHERE SCHECKID='" + SCHECKID + "' AND NATTACHMENT='" + NATTACHMENT + "' AND SCONTRACTID='" + SCONTRACTID + "' AND STRUCKID='" + STRUCKID + "'");

            OracleCommand oraCmd = new OracleCommand();
            oraCmd.Connection = con;
            if (con.State == ConnectionState.Closed) con.Open();
            oraCmd.CommandType = CommandType.Text;
            oraCmd.CommandText = "DELETE TCHECKTRUCKATTACHMENT WHERE SCHECKID='" + SCHECKID + "' AND NATTACHMENT='" + NATTACHMENT + "' AND SCONTRACTID='" + SCONTRACTID + "' AND STRUCKID='" + STRUCKID + "' ";
            //oraCmd.Parameters.Add("S_CHECKID", OracleType.VarChar).Value = (SCHECKID);
            //oraCmd.Parameters.Add("N_ATTACHMENT", OracleType.VarChar).Value = (NATTACHMENT);
            //oraCmd.Parameters.Add("S_CONTRACTID", OracleType.VarChar).Value = (SCONTRACTID);
            //oraCmd.Parameters.Add("S_TRUCKID", OracleType.VarChar).Value = (STRUCKID);
            result = oraCmd.ExecuteNonQuery();

            if (dtATTACHMENT.Rows.Count > 0)
            {
                foreach (DataRow drAttachment in dtATTACHMENT.Rows)
                {
                    string resFileName = "" + HttpContext.Current.Server.MapPath("" + drAttachment["SPATH"]) + drAttachment["SSYSFILE"];
                    if (File.Exists(resFileName))
                    {
                        File.Delete(resFileName);
                    }
                }
            }
        }
        shtml = (result > 0) ? "1" : "0";
        context.Response.Expires = -1;
        context.Response.ContentType = "text/plain";//return type
        context.Response.ContentEncoding = Encoding.UTF8;
        context.Response.Write(shtml);//return value
        context.Response.End();
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}