﻿<%@ WebHandler Language="C#" Class="GetCarRegis" %>

using System;
using System.Web;

public class GetCarRegis : IHttpHandler
{

    public System.Data.OracleClient.OracleConnection _conn = new System.Data.OracleClient.OracleConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString);
    //string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    public void ProcessRequest(HttpContext context)
    {
        string _mode = CommonFunction.ReplaceInjection(context.Request.QueryString["VenID"]);

        string Query = @"SELECT SHEADID , SHEADREGISTERNO,STRAILERREGISTERNO,CASE  WHEN  NVL(SHEADREGISTERNO,'xxx') <> 'xxx' THEN '/'||STRAILERREGISTERNO ELSE ' ' END as tail1 FROM
(
    SELECT ROW_NUMBER()OVER(ORDER BY TUC.SHEADID) AS RN ,TUC.SHEADID,TUC.SHEADREGISTERNO,TUC.STRAILERREGISTERNO FROM  TCONTRACT CON
    INNER JOIN
    (
        SELECT TCF.SCONTRACTID,TCFL.SHEADID,TCFL.SHEADREGISTERNO,TCFL.STRAILERREGISTERNO FROM TTRUCKCONFIRM TCF
        INNER JOIN 
        (
            SELECT TRKC.NCONFIRMID,TRKC.SHEADID,TRKH.SHEADREGISTERNO,TRKT.SHEADREGISTERNO as STRAILERREGISTERNO FROM TTRUCKCONFIRMLIST TRKC
            INNER JOIN TTRUCK TRKH
            ON TRKH.STRUCKID = TRKC.SHEADID
            INNER JOIN TTRUCK TRKT
            ON TRKT.STRUCKID = TRKC.STRAILERID
            WHERE TRKC.CCONFIRM = '1'
            GROUP BY TRKC.NCONFIRMID,TRKC.SHEADID,TRKH.SHEADREGISTERNO,TRKT.SHEADREGISTERNO
        ) TCFL
        ON TCFL.NCONFIRMID = TCF.NCONFIRMID AND TCF.CCONFIRM = '1'
        GROUP BY TCF.SCONTRACTID,TCFL.SHEADID,TCFL.SHEADREGISTERNO,TCFL.STRAILERREGISTERNO
    )TUC
    ON TUC.SCONTRACTID = CON.SCONTRACTID
    WHERE CON.SVENDORID = '" + CommonFunction.ReplaceInjection(_mode) + @"' AND CON.CACTIVE = 'Y' 
    GROUP BY  TUC.SHEADID,TUC.SHEADREGISTERNO,TUC.STRAILERREGISTERNO
)";
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}