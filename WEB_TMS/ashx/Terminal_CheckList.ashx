﻿<%@ WebHandler Language="C#" Class="Terminal_CheckList" %>
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

public class Terminal_CheckList : IHttpHandler {


    public void ProcessRequest(HttpContext context)
    {

        string shtml = "0", SCONTRACTID = "", STRUCKID = "", SCHECKID = "", SCHECKLISTID = "",
            DATA = CommonFunction.ReplaceInjection(context.Request["data"]);
        //%5E
        string[] data = (DATA.Length > 0) ? DATA.Replace("%5E", "$").Split('$') : ("").Split('$');//contractid + '^' + truckid + '^' + checklistid + '^' + scheckid
        if (data.Length > 0)
        {
            SCONTRACTID = CommonFunction.ReplaceInjection("" + data[0]);
            STRUCKID = CommonFunction.ReplaceInjection("" + data[1]);
            SCHECKLISTID = CommonFunction.ReplaceInjection("" + data[2]);
            SCHECKID = CommonFunction.ReplaceInjection("" + data[3]);
        }
        int result = -1;
        using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        { /*
            if (con.State == ConnectionState.Closed) con.Open();
            OracleCommand ora_cmd = new OracleCommand("UPDATE TCHECKTRUCKITEM SET CEDITED='1' WHERE SCHECKID=:S_CHECKID AND SCHECKLISTID=:S_CHECKLISTID ", con); 
            ora_cmd.Parameters.Add(":S_CHECKID", OracleType.Number).Value = int.Parse(SCHECKID); 
            ora_cmd.Parameters.Add(":S_CHECKLISTID", OracleType.Number).Value = int.Parse(SCHECKLISTID);
            result = ora_cmd.ExecuteNonQuery();
            */
            OracleCommand oraCmd = new OracleCommand();
            oraCmd.Connection = con;
            if (con.State == ConnectionState.Closed) con.Open();
            oraCmd.CommandType = CommandType.StoredProcedure;
            oraCmd.CommandText = "UTCHECKTRUCKITEM_COK";
            oraCmd.Parameters.Add("S_CHECKID", OracleType.Number).Value = int.Parse(SCHECKID);
            oraCmd.Parameters.Add("S_CHECKLISTID", OracleType.Number).Value = int.Parse(SCHECKLISTID);

            oraCmd.ExecuteNonQuery();
        } 
        shtml = (result == -1) ? "0" : "1";
        context.Response.Expires = -1;
        context.Response.ContentType = "text/plain";//return type
        context.Response.ContentEncoding = Encoding.UTF8;
        context.Response.Write(shtml);//return value
        context.Response.End();
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}