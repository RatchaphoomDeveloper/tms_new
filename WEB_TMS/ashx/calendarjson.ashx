﻿<%@ WebHandler Language="C#" Class="calendarjson" %>


using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Web;

public class calendarjson : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    string conn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    public void ProcessRequest(HttpContext context)
    {
        //Here is where you populate List<CalendarEvent> _cal with your source events

        //Instead, I'll load fake data for Events 1

        ////รหัส;typeหน้า;ความจุ;SEAL_HIT
        //  //00001;01;170000;1
        //  string[] Para = (Session["sCalendar"] + "").Split(';');

        string sMonth = context.Request.QueryString["sMonth"] + "";
        string sYear = context.Request.QueryString["sYear"] + "";
        string eMonth = context.Request.QueryString["eMonth"] + "";
        string eYear = context.Request.QueryString["eYear"] + "";
        string sProcess = CommonFunction.ReplaceInjection(context.Request.QueryString["sProcess"] + "");

        int temp;
        int nMonth = int.TryParse(sMonth, out temp) ? temp : 0;
        int nYear = int.TryParse(sYear, out temp) ? temp : 0;
        int neMonth = int.TryParse(eMonth, out temp) ? temp : 0;
        int neYear = int.TryParse(eYear, out temp) ? temp : 0;

        int Month;
        int Year;

        if (nYear < neYear && neMonth == 0) //ถ้าเป็นเดือน ธันวาของปีที่แล้ว
        {
            Month = 12;
            Year = nYear;
        }
        else
        {
            Month = neMonth;
            Year = neYear;
        }


        string[] Para = (context.Session["sCalendar"] + "").Split(';');
        if (Para.Length >= 4)
        {



            string sResult = "";
            switch (sProcess)
            {
                case "1":

                    sResult = GenInputOnmCalendar(Month, Year, Para[2], Para[3]);
                    break;

                case "2":
                    sResult = GenInputOnmCalendarDetail(Month, Year, Para[2], Para[3]);
                    break;


            }


            context.Response.ContentType = "application/json";
            context.Response.Write(sResult);
            context.Response.End();

        }



        if (sProcess == "3")
        {
            string sResult = sResult = GenInputOnmCalendarDetailOnly(Month, Year, "0", "0");
            context.Response.ContentType = "application/json";
            context.Response.Write(sResult);
            context.Response.End();
        }
    }


    //ปฎิทิน
    private string GenInputOnmCalendar(int nMonth, int nYear, string sCapacity, string SEAL_HIT)
    {
        int temp;
        int nSEAL_HIT = int.TryParse(SEAL_HIT, out temp) ? temp : 0;
        int nCapacity = int.TryParse(sCapacity, out temp) ? temp : 0;
        string condition = "";
        //ถ้าเป็น ตีซิล ไม่ต้องเช็ค รถใหญ่รถเล็ก CASE WHEN {2} = 0 THEN 1 ELSE 0 END   // 0 คือไม่ ตีซิล

        //  SEAL_HIT != 3  เพิ่มเติมเอกสารอื่นๆ
        string sql = "SELECT MIN(NVL(BEGIN_CAP,0)) As  NMIN,MAX(NVL(END_CAP,0)) As NMAX,STYPECAP  FROM TBL_CONDITIONS WHERE NVL(ACTIVE_FLAG,'Y') = 'Y' GROUP BY STYPECAP ORDER BY STYPECAP";
        System.Data.DataTable dtt = CommonFunction.Get_Data(conn, sql);


        System.Data.DataRow[] drDetailS = dtt.Select("STYPECAP = '1'");
        System.Data.DataRow[] drDetailL = dtt.Select("STYPECAP = '2'");

        string sMINS = "-1";
        string sMAXS = "-1";

        //รถเล็ก
        if (drDetailS.Length > 0)
        {
            sMINS = drDetailS[0]["NMIN"] + "";
            sMAXS = drDetailS[0]["NMAX"] + "";
        }

        string sMINL = "-1";
        string sMAXL = "-1";

        //รถใหญ่
        if (drDetailL.Length > 0)
        {
            sMINL = drDetailL[0]["NMIN"] + "";
            sMAXL = drDetailL[0]["NMAX"] + "";
        }

        //CAR_AMOUNT1 = รถใหญ่
        //CAR_AMOUNT2 = รถเล็ก
        //nSEAL_HIT = ตีซีล
        if (nSEAL_HIT > 0)
        {
            condition = "o.CAR_AMOUNT1 >= o.CURRENT_CAR_AMOUNT1 AND o.SEAL_HIT >= o.CURRENT_SEAL_HIT AND o.CAR_AMOUNT1 >= o.CURRENT_CAR_AMOUNT1 AND to_number(o.CAR_AMOUNT1)+to_number(o.CAR_AMOUNT2)+to_number(o.SEAL_HIT) >= to_number(o.CURRENT_CAR_AMOUNT1)+to_number(o.CURRENT_CAR_AMOUNT2)+to_number(o.CURRENT_SEAL_HIT)";
        }
        else if (nCapacity >= decimal.Parse(sMINL))
        {
            condition = "o.CAR_AMOUNT1 > o.CURRENT_CAR_AMOUNT1 AND o.SEAL_HIT >= o.CURRENT_SEAL_HIT AND o.CAR_AMOUNT2 >= o.CURRENT_CAR_AMOUNT2 AND to_number(o.CAR_AMOUNT1)+to_number(o.CAR_AMOUNT2)+to_number(o.SEAL_HIT) > to_number(o.CURRENT_CAR_AMOUNT1)+to_number(o.CURRENT_CAR_AMOUNT2)+to_number(o.CURRENT_SEAL_HIT)";
        }
        else if (nCapacity <= decimal.Parse(sMAXS))
        {
            condition = "o.CAR_AMOUNT1 >= o.CURRENT_CAR_AMOUNT1 AND o.SEAL_HIT >= o.CURRENT_SEAL_HIT AND o.CAR_AMOUNT2 > o.CURRENT_CAR_AMOUNT2 AND to_number(o.CAR_AMOUNT1)+to_number(o.CAR_AMOUNT2)+to_number(o.SEAL_HIT) > to_number(o.CURRENT_CAR_AMOUNT1)+to_number(o.CURRENT_CAR_AMOUNT2)+to_number(o.CURRENT_SEAL_HIT)";
        }
        string sqlQuery = @"SELECT COUNT(*) FROM (

SELECT
    m.CAR_AMOUNT as CAR_AMOUNT1,  (SELECT COUNT(*)  FROM TBL_CALENDAR_BOOKING WHERE SEAL_HIT != 3 AND SEAL_HIT = 0  AND NCAPACITY BETWEEN {3} AND  {4} AND TRUNC(DBOOKING) = TO_DATE('{1}','fmdd/mm/yyyy') ) AS CURRENT_CAR_AMOUNT1 ,  
     ml.CAR_AMOUNT as CAR_AMOUNT2,    (SELECT COUNT(*) FROM TBL_CALENDAR_BOOKING WHERE SEAL_HIT != 3 AND SEAL_HIT = 0  AND NCAPACITY BETWEEN {5} AND {6} AND TRUNC(DBOOKING) = TO_DATE('{1}','fmdd/mm/yyyy') ) AS CURRENT_CAR_AMOUNT2,
    m.SEAL_HIT ,NVL( (SELECT SUM(NVL(SEAL_HIT,0)) FROM TBL_CALENDAR_BOOKING WHERE SEAL_HIT != 3 AND TRUNC(DBOOKING) = TO_DATE('{1}','fmdd/mm/yyyy') ),0) + {2}  AS CURRENT_SEAL_HIT

FROM TBL_CONDITIONS m
INNER JOIN TBL_CONDITIONS ml ON M.NROWADD = ml.NROWADD AND NVL(ml.ACTIVE_FLAG,'Y') = 'Y' AND ML.CONDITIONS_ID != M.CONDITIONS_ID
WHERE NVL(m.ACTIVE_FLAG,'Y') = 'Y'
AND {0}  BETWEEN m.BEGIN_CAP AND m.END_CAP

) o
WHERE 
" + condition + "";
        //WHERE
        //   o.CAR_AMOUNT1 >=  o.CURRENT_CAR_AMOUNT1
        //   AND 
        //     o.CAR_AMOUNT2 >= o.CURRENT_CAR_AMOUNT2
        //     AND
        //    o.SEAL_HIT >=  o.CURRENT_SEAL_HIT


        string evt = "";


        #region set data to calendar
        #region ใช้งาน และ set up

        System.Data.DataTable tCheckHoliday = new System.Data.DataTable();

        int MaxDate = DateTime.DaysInMonth(nYear, nMonth);
        string _SMM = (nMonth - 1) + "";
        string _SYY = nYear + "";

        for (int i = 1; i <= MaxDate; i++)
        {

            string sCheckHoliday = "";

            //เช็ควันหยุด
            bool bCheckStopDate = false;
            bool bCheckHolidayDate = false;
            DateTime date1;
            DateTime date = DateTime.TryParse(i + "/" + nMonth + "/" + (nYear + 543), out date1) ? date1 : DateTime.Now;
            if ((int)date.DayOfWeek == 6 || (int)date.DayOfWeek == 0)
            {
                bCheckStopDate = true;
            }
            else
            {
                tCheckHoliday = CommonFunction.Get_Data(conn, "SELECT SHOLIDAY FROM LSTHOLIDAY_MV WHERE TRUNC(DHOLIDAY) = TO_DATE('" + date.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) + "','dd/MM/yyyy')");
                if ((tCheckHoliday.Rows.Count) > 0)
                {
                    sCheckHoliday = tCheckHoliday.Rows[0]["SHOLIDAY"] + "";
                    bCheckHolidayDate = true;
                }
            }


            string sValue = nSEAL_HIT != 3 ? CommonFunction.Get_Value(conn, string.Format(sqlQuery, nCapacity.ToString(), i + "/" + nMonth + "/" + nYear, nSEAL_HIT.ToString(), sMINL, sMAXL, sMINS, sMAXS)) : "3";



            //ถ้า sValue = 0 คือจองเต็มแล้ว
            string sSetButton = "";
            if (!bCheckStopDate)
            {

                string sDate = date.ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US"));

                if (!bCheckHolidayDate)
                {

                    sSetButton = sValue != "0" ? "<button type=button class=positive  style=width:100%;height:40px  onclick=SetDataCalendar('" + (i + "/" + nMonth + "/" + (nYear + 543)) + "')><img src=fullcalendar/images/tick.png />จองคิว</button>" : "<span style=font-family:Tahoma,Arial,Verdana,sans-serif;font-size:14px;text-align:center;color:red; ><img src=fullcalendar/images/cross.png />เต็มแล้ว</span>";

                }
                else
                {

                    sSetButton = "<span style=font-family:Tahoma,Arial,Verdana,sans-serif;font-size:14px;text-align:center;color:red; ><img src=fullcalendar/images/cross.png />" + sCheckHoliday + "</span>";

                }
                evt += @",{""title"": ""<div align=center style=vertical-align:middle; class=revtSet>" + sSetButton + @"</div>"",""start"":  """ + sDate + @""",""allDay"":true}";


            }
        }

        #endregion
        #endregion




        return (evt.Length > 0 ? "[" + evt.Remove(0, 1) + "]" : "") + "";
    }


    //ปฎิทิน
    private string GenInputOnmCalendarDetail(int nMonth, int nYear, string sCapacity, string SEAL_HIT)
    {
        int temp;
        int nSEAL_HIT = int.TryParse(SEAL_HIT, out temp) ? temp : 0;
        int nCapacity = int.TryParse(sCapacity, out temp) ? temp : 0;
        string condition = "";

        string sql = "SELECT MIN(NVL(BEGIN_CAP,0)) As  NMIN,MAX(NVL(END_CAP,0)) As NMAX,STYPECAP  FROM TBL_CONDITIONS WHERE NVL(ACTIVE_FLAG,'Y') = 'Y' GROUP BY STYPECAP ORDER BY STYPECAP";
        System.Data.DataTable dtt = CommonFunction.Get_Data(conn, sql);


        System.Data.DataRow[] drDetailS = dtt.Select("STYPECAP = '1'");
        System.Data.DataRow[] drDetailL = dtt.Select("STYPECAP = '2'");

        string sMINS = "-1";
        string sMAXS = "-1";

        //รถเล็ก
        if (drDetailS.Length > 0)
        {
            sMINS = drDetailS[0]["NMIN"] + "";
            sMAXS = drDetailS[0]["NMAX"] + "";
        }

        string sMINL = "-1";
        string sMAXL = "-1";

        //รถใหญ่
        if (drDetailL.Length > 0)
        {
            sMINL = drDetailL[0]["NMIN"] + "";
            sMAXL = drDetailL[0]["NMAX"] + "";
        }
        if (nSEAL_HIT > 0)
        {
            condition = "o.CAR_AMOUNT1 >= o.CURRENT_CAR_AMOUNT1 AND o.SEAL_HIT >= o.CURRENT_SEAL_HIT AND o.CAR_AMOUNT1 >= o.CURRENT_CAR_AMOUNT1 AND to_number(o.CAR_AMOUNT1)+to_number(o.CAR_AMOUNT2)+to_number(o.SEAL_HIT) >= to_number(o.CURRENT_CAR_AMOUNT1)+to_number(o.CURRENT_CAR_AMOUNT2)+to_number(o.CURRENT_SEAL_HIT)";
        }
        else if (nCapacity >= decimal.Parse(sMINL))
        {
            condition = "o.CAR_AMOUNT1 > o.CURRENT_CAR_AMOUNT1 AND o.SEAL_HIT >= o.CURRENT_SEAL_HIT AND o.CAR_AMOUNT2 >= o.CURRENT_CAR_AMOUNT2 AND to_number(o.CAR_AMOUNT1)+to_number(o.CAR_AMOUNT2)+to_number(o.SEAL_HIT) > to_number(o.CURRENT_CAR_AMOUNT1)+to_number(o.CURRENT_CAR_AMOUNT2)+to_number(o.CURRENT_SEAL_HIT)";
        }
        else if (nCapacity <= decimal.Parse(sMAXS))
        {
            condition = "o.CAR_AMOUNT1 >= o.CURRENT_CAR_AMOUNT1 AND o.SEAL_HIT >= o.CURRENT_SEAL_HIT AND o.CAR_AMOUNT2 > o.CURRENT_CAR_AMOUNT2 AND to_number(o.CAR_AMOUNT1)+to_number(o.CAR_AMOUNT2)+to_number(o.SEAL_HIT) > to_number(o.CURRENT_CAR_AMOUNT1)+to_number(o.CURRENT_CAR_AMOUNT2)+to_number(o.CURRENT_SEAL_HIT)";
        }
        //ถ้าเป็น ตีซิล ไม่ต้องเช็ค รถใหญ่รถเล็ก CASE WHEN {2} = 0 THEN 1 ELSE 0 END   // 0 คือไม่ ตีซิล

        string sqlQuery1 = @"SELECT COUNT(*) FROM (

SELECT
    m.CAR_AMOUNT as CAR_AMOUNT1,  (SELECT COUNT(*)  FROM TBL_CALENDAR_BOOKING WHERE SEAL_HIT != 3 AND SEAL_HIT = 0  AND NCAPACITY BETWEEN {3} AND  {4} AND TRUNC(DBOOKING) = TO_DATE('{1}','fmdd/mm/yyyy') ) AS CURRENT_CAR_AMOUNT1 ,  
     ml.CAR_AMOUNT as CAR_AMOUNT2,    (SELECT COUNT(*) FROM TBL_CALENDAR_BOOKING WHERE SEAL_HIT != 3 AND SEAL_HIT = 0  AND NCAPACITY BETWEEN {5} AND {6} AND TRUNC(DBOOKING) = TO_DATE('{1}','fmdd/mm/yyyy') ) AS CURRENT_CAR_AMOUNT2,
    m.SEAL_HIT ,NVL( (SELECT SUM(NVL(SEAL_HIT,0)) FROM TBL_CALENDAR_BOOKING WHERE SEAL_HIT != 3 AND TRUNC(DBOOKING) = TO_DATE('{1}','fmdd/mm/yyyy') ),0) + {2}  AS CURRENT_SEAL_HIT

FROM TBL_CONDITIONS m
INNER JOIN TBL_CONDITIONS ml ON M.NROWADD = ml.NROWADD AND NVL(ml.ACTIVE_FLAG,'Y') = 'Y' AND ML.CONDITIONS_ID != M.CONDITIONS_ID
WHERE NVL(m.ACTIVE_FLAG,'Y') = 'Y'
AND {0}  BETWEEN m.BEGIN_CAP AND m.END_CAP

) o
WHERE 
" + condition + "";
        //WHERE
        //   o.CAR_AMOUNT1 >=  o.CURRENT_CAR_AMOUNT1
        //   AND 
        //     o.CAR_AMOUNT2 >= o.CURRENT_CAR_AMOUNT2
        //     AND
        //    o.SEAL_HIT >=  o.CURRENT_SEAL_HIT


        string sqlQuery = string.Format(@"SELECT SUM(NVL( SEAL_HIT,0) ) AS SEALHIT, 
SUM(NVL(
      CASE WHEN NCAPACITY >= {0} AND NCAPACITY <= {1} AND SEAL_HIT = 0  THEN 1 ELSE 0 END  ,0)) AS NCARS, 
SUM(NVL(
     CASE WHEN  NCAPACITY >= {2} AND NCAPACITY <= {3} AND SEAL_HIT = 0  THEN 1 ELSE 0 END ,0 )) AS NCARL ,
DBOOKING ,to_char(DBOOKING, 'dd') AS SDAY  FROM TBL_CALENDAR_BOOKING  
WHERE SEAL_HIT != 3 AND to_char(DBOOKING, 'mm') = '{4}'  AND to_char(DBOOKING, 'yyyy') = '{5}'
GROUP BY DBOOKING ORDER BY DBOOKING 
", sMINS, sMAXS, sMINL, sMAXL, nMonth.ToString().PadLeft(2, '0'), nYear.ToString());


        string evt = "";


        #region set data to calendar
        #region ใช้งาน และ set up

        System.Data.DataTable dttData1 = new System.Data.DataTable();
        dttData1 = CommonFunction.Get_Data(conn, sqlQuery);
        System.Data.DataTable tCheckHoliday = new System.Data.DataTable();

        int MaxDate = DateTime.DaysInMonth(nYear, nMonth);
        string _SMM = (nMonth - 1) + "";
        string _SYY = nYear + "";
        string sResult = "";
        for (int i = 1; i <= MaxDate; i++)
        {

            sResult = "";
            DateTime date1;
            DateTime date = DateTime.TryParse(i + "/" + nMonth + "/" + (nYear + 543), out date1) ? date1 : DateTime.Now;


            string sCheckHoliday = "";

            //เช็ควันหยุด
            bool bCheckStopDate = false;
            bool bCheckHolidayDate = false;

            if ((int)date.DayOfWeek == 6 || (int)date.DayOfWeek == 0)
            {
                bCheckStopDate = true;
            }
            else
            {
                tCheckHoliday = CommonFunction.Get_Data(conn, "SELECT SHOLIDAY FROM LSTHOLIDAY_MV WHERE TRUNC(DHOLIDAY) = TO_DATE('" + date.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) + "','dd/MM/yyyy')");
                if ((tCheckHoliday.Rows.Count) > 0)
                {
                    sCheckHoliday = tCheckHoliday.Rows[0]["SHOLIDAY"] + "";
                    bCheckHolidayDate = true;
                }
            }



            if (!bCheckStopDate && !bCheckHolidayDate)
            {

                System.Data.DataRow[] drRow = dttData1.Select("SDAY = '" + i.ToString().PadLeft(2, '0') + "'");


                if (drRow.Length > 0)
                {


                    sResult += ("" + drRow[0]["NCARL"]) != "0" ? "รถใหญ่ " + drRow[0]["NCARL"] + " คัน <br/>" : "";
                    sResult += ("" + drRow[0]["NCARS"]) != "0" ? "รถเล็ก " + drRow[0]["NCARS"] + " คัน <br/>" : "";
                    sResult += ("" + drRow[0]["SEALHIT"]) != "0" ? "<span style=color:#ff9933!important;font-weight:bold!important; >อื่นๆ " + drRow[0]["SEALHIT"] + " คัน <br/></span>" : "";


                }



                string sValue = nSEAL_HIT != 3 ? CommonFunction.Get_Value(conn, string.Format(sqlQuery1, nCapacity.ToString(), i + "/" + nMonth + "/" + nYear, nSEAL_HIT.ToString(), sMINL, sMAXL, sMINS, sMAXS)) : "3";

                sResult += (sValue != "0" ? "<button type=button class=positive  style=width:100%;height:30px  onclick=SetDataCalendar('" + (i + "/" + nMonth + "/" + (nYear + 543)) + "')><img src=fullcalendar/images/tick.png />จัดคิว</button>" : "");

            }

            string sDate = date.ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US"));

            evt += @",{""title"": ""<div align=center style=vertical-align:middle; class=revtSet><span style=font-family:Tahoma,Arial,Verdana,sans-serif;font-size:14px;text-align:left;color:black; >" + sResult + @"</span></div>"",""start"":  """ + sDate + @""",""allDay"":true}";



        }

        #endregion
        #endregion




        return (evt.Length > 0 ? "[" + evt.Remove(0, 1) + "]" : "") + "";
    }



    //ปฎิทิน
    private string GenInputOnmCalendarDetailOnly(int nMonth, int nYear, string sCapacity, string SEAL_HIT)
    {
        int temp;


        string sql = "SELECT MIN(NVL(BEGIN_CAP,0)) As  NMIN,MAX(NVL(END_CAP,0)) As NMAX,STYPECAP  FROM TBL_CONDITIONS WHERE NVL(ACTIVE_FLAG,'Y') = 'Y' GROUP BY STYPECAP ORDER BY STYPECAP";
        System.Data.DataTable dtt = CommonFunction.Get_Data(conn, sql);


        System.Data.DataRow[] drDetailS = dtt.Select("STYPECAP = '1'");
        System.Data.DataRow[] drDetailL = dtt.Select("STYPECAP = '2'");

        string sMINS = "-1";
        string sMAXS = "-1";

        //รถเล็ก
        if (drDetailS.Length > 0)
        {
            sMINS = drDetailS[0]["NMIN"] + "";
            sMAXS = drDetailS[0]["NMAX"] + "";
        }

        string sMINL = "-1";
        string sMAXL = "-1";

        //รถใหญ่
        if (drDetailL.Length > 0)
        {
            sMINL = drDetailL[0]["NMIN"] + "";
            sMAXL = drDetailL[0]["NMAX"] + "";
        }

        string sqlQuery = string.Format(@"SELECT SUM(NVL( SEAL_HIT,0) ) AS SEALHIT, 
SUM(NVL(
      CASE WHEN NCAPACITY >= {0} AND SEAL_HIT = 0 AND NCAPACITY <= {1} THEN 1 ELSE 0 END ,0)) AS NCARS, 
SUM(NVL(
     CASE WHEN  NCAPACITY >= {2} AND SEAL_HIT = 0 AND NCAPACITY <= {3} THEN 1 ELSE 0 END ,0 )) AS NCARL ,
DBOOKING ,to_char(DBOOKING, 'dd') AS SDAY  FROM TBL_CALENDAR_BOOKING  
WHERE SEAL_HIT != 3 AND to_char(DBOOKING, 'mm') = '{4}'  AND to_char(DBOOKING, 'yyyy') = '{5}'
GROUP BY DBOOKING ORDER BY DBOOKING 
", sMINS, sMAXS, sMINL, sMAXL, nMonth.ToString().PadLeft(2, '0'), nYear.ToString());


        string evt = "";


        #region set data to calendar
        #region ใช้งาน และ set up

        System.Data.DataTable dttData1 = new System.Data.DataTable();
        dttData1 = CommonFunction.Get_Data(conn, sqlQuery);
        System.Data.DataTable tCheckHoliday = new System.Data.DataTable();

        int MaxDate = DateTime.DaysInMonth(nYear, nMonth);
        string _SMM = (nMonth - 1) + "";
        string _SYY = nYear + "";
        string sResult = "";
        for (int i = 1; i <= MaxDate; i++)
        {

            sResult = "";
            DateTime date1;
            DateTime date = DateTime.TryParse(i + "/" + nMonth + "/" + (nYear + 543), out date1) ? date1 : DateTime.Now;

            System.Data.DataRow[] drRow = dttData1.Select("SDAY = '" + i.ToString().PadLeft(2, '0') + "'");


            if (drRow.Length > 0)
            {


                sResult += ("" + drRow[0]["NCARL"]) != "0" ? "รถใหญ่ " + drRow[0]["NCARL"] + " คัน <br/>" : "";
                sResult += ("" + drRow[0]["NCARS"]) != "0" ? "รถเล็ก " + drRow[0]["NCARS"] + " คัน <br/>" : "";
                sResult += ("" + drRow[0]["SEALHIT"]) != "0" ? "<span style=color:#ff9933!important;font-weight:bold!important; >อื่นๆ " + drRow[0]["SEALHIT"] + " คัน <br/></span>" : "";


            }

            string sDate = date.ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US"));

            evt += @",{""title"": ""<div align=center style=vertical-align:middle; class=revtSet><span style=font-family:Tahoma,Arial,Verdana,sans-serif;font-size:14px;text-align:left;color:black; >" + sResult + @"</span></div>"",""start"":  """ + sDate + @""",""allDay"":true}";



        }

        #endregion
        #endregion




        return (evt.Length > 0 ? "[" + evt.Remove(0, 1) + "]" : "") + "";
    }


    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}
