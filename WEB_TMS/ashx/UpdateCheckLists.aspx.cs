﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Globalization;
using System.Data;
using System.Data.OracleClient;

public partial class ashx_UpdateCheckLists : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           //  string str = "" + Request.QueryString["data"];
           string str = "" + Request["data"];//Request.QueryString["data"];Request["data"]
            string shtml = "0", SCONTRACTID = "", STRUCKID = "", SCHECKID = "", SCHECKLISTID = "", DATA = CommonFunction.ReplaceInjection(str);
            string[] data = (DATA.Length > 0) ? DATA.Replace("%5E", "$").Split('$') : ("").Split('$');//contractid + '^' + truckid + '^' + checklistid + '^' + scheckid
            if (data.Length > 0)
            {
                SCONTRACTID = CommonFunction.ReplaceInjection("" + data[0]);
                STRUCKID = CommonFunction.ReplaceInjection("" + data[1]);
                SCHECKLISTID = CommonFunction.ReplaceInjection("" + data[2]);
                SCHECKID = CommonFunction.ReplaceInjection("" + data[3]);
            }
            int result = -1;
            using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
            {
                ////OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString);
                if (con.State == ConnectionState.Closed) con.Open();
                OracleCommand ora_cmd = new OracleCommand("UPDATE TCHECKTRUCKITEM SET CEDITED='1' WHERE SCHECKID=" + SCHECKID + " AND SCHECKLISTID=" + SCHECKLISTID + " ", con);
                /*ora_cmd.Parameters.Add(":S_CHECKID", OracleType.Number).Value = int.Parse(SCHECKID); 
                ora_cmd.Parameters.Add(":S_CHECKLISTID", OracleType.Number).Value = int.Parse(SCHECKLISTID);*/
                result = ora_cmd.ExecuteNonQuery();
            }
            Response.Write("[" + result + "]");
        }
    }
}