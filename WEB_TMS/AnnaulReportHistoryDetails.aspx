﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="AnnaulReportHistoryDetails.aspx.cs" Inherits="AnnaulReportHistoryDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"] {
            margin-left: 0px;
        }

        .checkbox label, .radio label {
            padding-right: 10px;
            padding-left: 15px;
        }

        .required.control-label:after {
            content: "*";
            color: red;
        }

        .form-horizontal .control-label.text-left {
            text-align: left;
            left: 0px;
        }

        .GridColorHeader th {
            text-align: inherit !important;
            align-content: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="grvMain" EventName="PageIndexChanging" />
            <asp:PostBackTrigger ControlID="btnExportPdf" />
            <asp:PostBackTrigger ControlID="btnClose" />
        </Triggers>
        <ContentTemplate>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>ประวัติการเรียกดูเอกสาร
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body" style="padding-bottom: 0px;">
                            <div class="form-group form-horizontal row">
                                <label for="<%=lblYear.ClientID %>" class="col-md-1 control-label">ปี</label>
                                <div style="text-align: left" class="col-md-1 control-label">
                                    <asp:Label ID="lblYear" runat="server" Text="2016"></asp:Label>
                                </div>
                                <label for="<%=lblVendorName.ClientID %>" class="col-md-2 control-label">บริษัทผู้ขนส่ง</label>
                                <div style="text-align: left" class="col-md-3 control-label">
                                    <asp:Label ID="lblVendorName" runat="server" Text="บริษัทผู้ขนส่ง"></asp:Label>
                                </div>
                                <div style="text-align: left" class="col-md-2">
                                <asp:Button ID="btnClose" runat="server" Text="ปิด" UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-warning" OnClick="btnClose_Click" Style="width: 80px" />
                                    </div>

                            </div>
                            <asp:GridView ID="grvMain" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                HorizontalAlign="Center" AutoGenerateColumns="false" OnRowCommand="grvMain_RowCommand" OnRowDataBound="grvMain_RowDataBound" OnPageIndexChanging="grvMain_PageIndexChanging" DataKeyNames="ID"
                                EmptyDataText="[ ไม่มีข้อมูล ]" AllowPaging="True" OnDataBound="grvMain_DataBound">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="10px" ItemStyle-CssClass="center" HeaderText="ลำดับ" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" Width="10px" />
                                        <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ชื่อ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="StaticCol" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblSFIRSTNAME" runat="server" Text='<%# Eval("SFIRSTNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="นามสกุล" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="StaticCol" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblSLASTNAME" runat="server" Text='<%# Eval("SLASTNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="USER" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="StaticCol" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblSUSERNAME" runat="server" Text='<%# Eval("SUSERNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="เลขที่เอกสาร" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Eval("DOCNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="วันที่" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <ItemStyle HorizontalAlign="Center" Width="250px" />
                                        <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblSEND_TO_VENDOR_DATE" runat="server" Text='<%# Eval("DOWNLOAD_DATE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Document" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" Visible="false">
                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                        <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImagePDF" runat="server" ImageUrl="~/Images/ic_pdf2.gif" Width="16px" Height="16px" BorderWidth="0" Style="cursor: pointer" CausesValidation="False" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                <PagerStyle CssClass="pagination-ys" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div style="display: none;">
                    <asp:Button ID="btnExportPdf" runat="server" OnClick="btnExportPdf_Click" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="display: none;">
        <asp:Button ID="btnVendorDownload" runat="server" Text="Export Excel" CssClass="btn btn-hover btn-info" OnClick="btnVendorDownload_Click" />

    </div>
    <script type="text/javascript">
        function clickDownload() {
            $("#<%=btnVendorDownload.ClientID%>").click();
        }


    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

