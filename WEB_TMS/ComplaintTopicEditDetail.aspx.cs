﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class ComplaintTopicEditDetail : PageBase
{
    #region + View State +
    private int TopicID
    {
        get
        {
            if ((int)ViewState["TopicID"] != null)
                return (int)ViewState["TopicID"];
            else
                return 0;
        }
        set
        {
            ViewState["TopicID"] = value;
        }
    }

    private DataTable dtComplainType
    {
        get
        {
            if ((DataTable)ViewState["dtComplainType"] != null)
                return (DataTable)ViewState["dtComplainType"];
            else
                return null;
        }
        set
        {
            ViewState["dtComplainType"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["TOPIC_ID"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                TopicID = int.Parse(decryptedValue);
                ddlTopic.SelectedValue = TopicID.ToString();

                DataTable dtTopic = TopicBLL.Instance.TopicSelectAllBLL(" AND TOPIC_ID = " + TopicID.ToString());
                radType.SelectedValue = dtTopic.Rows[0]["TYPE_VALUE"].ToString();
                radTopicStatus.SelectedValue = dtTopic.Rows[0]["ISACTIVE_VALUE"].ToString();

                this.LoadTopic();
                this.LoadComplainType();
                //this.LoadRequireField();
                this.LoadAttachFile();
            }

            radType.Enabled = false;
            ddlTopic.Enabled = false;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadTopic()
    {
        try
        {
            DataTable dtTopic = TopicBLL.Instance.TopicSelectAllBLL(" AND TYPE = '" + radType.SelectedValue + "'");
            DropDownListHelper.BindDropDownList(ref ddlTopic, dtTopic, "TOPIC_ID", "TOPIC_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadComplainType()
    {
        try
        {
            dtComplainType = ComplainTypeBLL.Instance.ComplainTypeSelectAllBLL(" AND M_TOPIC.TOPIC_ID = '" + TopicID + "'");
            DropDownListHelper.BindDropDownList(ref ddlComplainType, dtComplainType, "COMPLAIN_TYPE_ID", "COMPLAIN_TYPE_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    //private void LoadRequireField()
    //{
    //    try
    //    {
    //        this.GenDropDownRequire(ddlReqTopic);
    //        this.GenDropDownRequire(ddlReqOrganiz);
    //        this.GenDropDownRequire(ddlReqComplainType);
    //        this.GenDropDownRequire(ddlReqVendor);
    //        this.GenDropDownRequire(ddlReqContract);
    //        this.GenDropDownRequire(ddlReqDelivery);
    //        this.GenDropDownRequire(ddlReqHeaderRegist);
    //        this.GenDropDownRequire(ddlReqPersonalNo);
    //        this.GenDropDownRequire(ddlReqComplainAddress);
    //        this.GenDropDownRequire(ddlReqDateTran);
    //        this.GenDropDownRequire(ddlReqTotalCar);
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}

    private void GenDropDownRequire(DropDownList ddl)
    {
        try
        {
            DataTable dtType = new DataTable();
            dtType.Columns.Add("TYPE_VALUE");
            dtType.Columns.Add("TYPE_DESCRIPTION");
            dtType.Rows.Add("1", "REQUIRE");
            dtType.Rows.Add("2", "OPTIONAL");
            dtType.Rows.Add("3", "DISABLE");
            DropDownListHelper.BindDropDownList(ref ddl, dtType, "TYPE_VALUE", "TYPE_DESCRIPTION", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadAttachFile()
    {
        try
        {

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            int ComplainID = 0;
            int ComplainActive = 0;

            if (ddlComplainType.SelectedIndex > 0)
            {
                ComplainID = int.Parse(ddlComplainType.SelectedValue);
                ComplainActive = int.Parse(radComplainStatus.SelectedValue);
            }

            TopicBLL.Instance.TopicUpdateBLL(int.Parse(ddlTopic.SelectedValue), int.Parse(radTopicStatus.SelectedValue), ComplainID, ddlComplainType.SelectedItem.ToString(), ComplainActive, int.Parse(Session["UserID"].ToString()), 0, 0);
            alertSuccess("บันทึกข้อมูลเรียบร้อย", "ComplaintTopic.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ComplaintTopic.aspx");
    }

    protected void ddlComplainType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlComplainType.SelectedIndex == 0)
                radComplainStatus.SelectedValue = null;
            else
                radComplainStatus.SelectedValue = dtComplainType.Rows[ddlComplainType.SelectedIndex]["COMPLAIN_ACTIVE"].ToString();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
}