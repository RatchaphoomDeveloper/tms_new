﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using System.Linq;
using System.Drawing;
using System.Data.OracleClient;
using System.Configuration;
using System.Web.Configuration;
using EmailHelper;

public partial class ProductTagItemAddEdit : PageBase
{
    DataSet ds;
    string sqlCon = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    #region + View State +
    private DataTable dtKPI
    {
        get
        {
            if ((DataTable)ViewState["dtKPI"] != null)
                return (DataTable)ViewState["dtKPI"];
            else
                return null;
        }
        set
        {
            ViewState["dtKPI"] = value;
        }
    }

    private DataTable dt
    {
        get
        {
            if ((DataTable)ViewState["dt"] != null)
                return (DataTable)ViewState["dt"];
            else
                return null;
        }
        set
        {
            ViewState["dt"] = value;
        }
    }

    private string OldPassWord
    {
        get
        {
            if ((string)ViewState["OldPassWord"] != null)
                return (string)ViewState["OldPassWord"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["OldPassWord"] = value;
        }
    }

    private string DefaultPassWord
    {
        get
        {
            if ((string)ViewState["DefaultPassWord"] != null)
                return (string)ViewState["DefaultPassWord"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["DefaultPassWord"] = value;
        }
    }

    private string PassWord
    {
        get
        {
            if ((string)ViewState["PassWord"] != null)
                return (string)ViewState["PassWord"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["PassWord"] = value;
        }
    }

    private string ActionType
    {
        get
        {
            if ((string)ViewState["ActionType"] != null)
                return (string)ViewState["ActionType"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["ActionType"] = value;
        }
    }

    private string ActionKey
    {
        get
        {
            if ((string)ViewState["ActionKey"] != null)
                return (string)ViewState["ActionKey"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["ActionKey"] = value;
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            this.LoadProducTagHead();
            this.LoadStatus();
            this.CheckQueryString();
            this.GetData();
            this.Enabled();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadProducTagHead()
    {
        try
        {
            DataTable dtProducTagHead = ProductTagBLL.Instance.ProductTagHeadSelectBLL(" AND CACTIVE = 1");
            DropDownListHelper.BindDropDownList(ref ddlProductTagHead, dtProducTagHead, "KM_TAG_LVL1_ID", "KM_TAG_LVL1_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void CheckQueryString()
    {
        try
        {
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                ActionType = Encoding.UTF8.GetString(MachineKey.Decode(Request.QueryString["type"], MachineKeyProtection.All));
                ActionKey = Encoding.UTF8.GetString(MachineKey.Decode(Request.QueryString["id"], MachineKeyProtection.All));
            }
            else
                Response.Redirect("../Other/NotAuthorize.aspx");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void Enabled()
    {

    }

    private void GetData()
    {
        if (string.Equals(ActionType, Mode.Add.ToString()))
        {
            cmdSave.Visible = true;
        }
        else if (string.Equals(ActionType, Mode.View.ToString()))
        {
            dt = ProductTagBLL.Instance.ProductTagItemSelectBLL(" AND KM_TAG_LVL2_ID = " + ActionKey);
            if (dt.Rows.Count > 0)
            {
                ddlProductTagHead.SelectedValue = dt.Rows[0]["KM_TAG_LVL1_ID"].ToString();
                ddlStatus.SelectedValue = dt.Rows[0]["CACTIVE"].ToString();
                txtProductTagItem.Text = dt.Rows[0]["KM_TAG_LVL2_NAME"].ToString();
                ddlProductTagHead.Enabled = false;
                txtProductTagItem.Enabled = false;
                ddlStatus.Enabled = false;
                cmdSave.Visible = false;
            }
        }
        else
        {
            dt = ProductTagBLL.Instance.ProductTagItemSelectBLL(" AND KM_TAG_LVL2_ID = " + ActionKey);
            if (dt.Rows.Count > 0)
            {
                ddlProductTagHead.SelectedValue = dt.Rows[0]["KM_TAG_LVL1_ID"].ToString();
                ddlStatus.SelectedValue = dt.Rows[0]["CACTIVE"].ToString();
                txtProductTagItem.Text = dt.Rows[0]["KM_TAG_LVL2_NAME"].ToString();
                cmdSave.Visible = true;
            }
        }
    }

    private void LoadStatus()
    {
        try
        {
            DataTable dtStatus = UserBLL.Instance.StatusSelectBLL(" AND IS_ACTIVE = 1 AND STATUS_TYPE = 'DEPARTMENT_STATUS'");
            DropDownListHelper.BindDropDownList(ref ddlStatus, dtStatus, "STATUS_VALUE", "STATUS_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void mpConfirmSave_ClickOK(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSave();
            DataTable dtResult = ProductTagBLL.Instance.ProductTagItemInsertBLL(int.Parse(ActionKey), txtProductTagItem.Text.Trim(), int.Parse(ddlProductTagHead.SelectedValue), int.Parse(ddlStatus.SelectedValue), int.Parse(Session["UserID"].ToString()));
            if (dtResult.Rows.Count > 0)
                alertSuccess("บันทึกผลสำเร็จ", "ProductTagItem.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateSave()
    {
        try
        {
            if (ddlStatus.SelectedIndex == 0)
                throw new Exception("กรุณาเลือกสถานะ");

            if (ddlProductTagHead.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก ProductTagHead");

            if (string.Equals(txtProductTagItem.Text.Trim(), ""))
                throw new Exception("กรุณากรอก ProductTagItem");

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void mpConfirmCancel_ClickOK(object sender, EventArgs e)
    {
        Response.Redirect("ProductTagItem.aspx");
    }
}