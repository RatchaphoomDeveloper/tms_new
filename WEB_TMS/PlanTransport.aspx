﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="PlanTransport.aspx.cs" Inherits="PlanTransport" %>

<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript">

        function CheckNumber(s, e) {

            if (s.GetValue() != null && s.GetValue() != "") {
                var values = s.GetValue().replace(/,/g, '');
                if ($.isNumeric(values)) {
                }
                else {
                    s.SetValue("");
                }
            }
        }
    </script>
    <style type="text/css">
        .Poop .dxcpLoadingPanelWithContent_Aqua {
            left: 360px !important;
        }
        /*  .dxeHLC
        {
            display: none;
        }
        .dxeLTM
        {
            display: none;
        }*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" OnCallback="xcpn_Callback"
        ClientInstanceName="xcpn" CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents BeginCallback="function (s, e) {}" EndCallback="function(s,e){ inEndRequestHandler();setdatepicker();eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen);  inEndRequestHandler(); s.cpRedirectOpen = undefined; }"></ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" cellpadding="3" cellspacing="1">
                    <tr>
                        <td>
                            <table id="Table1" runat="server" cellpadding="3" cellspacing="1" width="100%">
                                <tr>
                                    <td width="10%">วันที่รับงาน :
                                    </td>
                                    <td width="20%">
                                        <asp:TextBox  ID="edtAssign" runat="server" CssClass="edtAssign datepicker"  />
                                    </td>
                                    <td width="10%" align="right">คลังต้นทาง :
                                    </td>
                                    <td width="25%">
                                        <dx:ASPxComboBox ID="cboSelect" ClientInstanceName="cboSelect" runat="server" Width="100%" TextFormatString="{1}" ValueField="STERMINALID" IncrementalFilteringMode="Contains">
                                            <Columns>
                                                <dx:ListBoxColumn Caption="รหัสคลัง" FieldName="STERMINALID" />
                                                <dx:ListBoxColumn Caption="ชื่อคลัง" FieldName="SABBREVIATION" />
                                            </Columns>
                                        </dx:ASPxComboBox>

                                    </td>
                                    <td width="10%" align="right">เลขที่สัญญา:
                                    </td>
                                    <td width="25%">
                                        <%-- <dx:ASPxComboBox runat="server" CallbackPageSize="30" EnableCallbackMode="True" ValueField="SCONTRACTID"
                                            TextFormatString="{1}" Width="100%" Height="25px" ClientInstanceName="cboSCONTRACT"
                                            CssClass="dxeLineBreakFix" SkinID="xcbbATC" ID="cboSCONTRACT" OnItemRequestedByValue="cboSCONTRACT_OnItemRequestedByValueSQL"
                                            OnItemsRequestedByFilterCondition="cboSCONTRACT_ItemsRequestedByFilterConditionSQL">
                                            <Columns>
                                                <dx:ListBoxColumn FieldName="SCONTRACTID" Width="20%" Caption="#"></dx:ListBoxColumn>
                                                <dx:ListBoxColumn FieldName="SCONTRACTNO" Width="80%" Caption="สัญญา"></dx:ListBoxColumn>
                                            </Columns>
                                            <ItemStyle Wrap="True"></ItemStyle>
                                        </dx:ASPxComboBox>--%>
                                        <dx:ASPxComboBox runat="server" ValueField="SCONTRACTID" TextField="SCONTRACTNO"
                                            ClientInstanceName="cboSCONTRACT" CssClass="dxeLineBreakFix" ID="cboSCONTRACT"
                                            Width="100%">
                                        </dx:ASPxComboBox>
                                        <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                            ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"></asp:SqlDataSource>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Delivery No :
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox runat="server" ID="txtDNO" Width="100%">
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td align="right">ทะเบียนรถ:
                                    </td>
                                    <td>
                                        <dx:ASPxComboBox ID="cboCarregis" runat="server" ClientInstanceName="cboCarregis"
                                            ItemStyle-Wrap="True"
                                            SkinID="xcbbATC" TextFormatString="{0} {1}" ValueField="STRUCKID" Width="300px"
                                            CssClass="dxeLineBreakFix" Height="25">
                                            <Columns>
                                                <dx:ListBoxColumn FieldName="SHEADREGISTERNO" Width="110px" Caption="ทะเบียนหัว"></dx:ListBoxColumn>
                                                <dx:ListBoxColumn FieldName="STRAILERREGISTERNO" Width="110px" Caption="ทะเบียนหาง"></dx:ListBoxColumn>
                                            </Columns>
                                            <ItemStyle Wrap="True"></ItemStyle>
                                            <%--             <ValidationSettings ErrorDisplayMode="ImageWithTooltip" Display="Dynamic" RequiredField-IsRequired="true"
                                                RequiredField-ErrorText="ระบุทะเบียรถ" ValidationGroup="add">
                                                <RequiredField IsRequired="True" ErrorText="ระบุบริษัท"></RequiredField>
                                            </ValidationSettings>--%>
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td colspan="2" align="right">
                                        <dx:ASPxButton runat="server" ID="btnSearch" AutoPostBack="false" SkinID="_search" CssClass="dxeLineBreakFix">
                                            <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('Search');}" />
                                        </dx:ASPxButton>
                                        <dx:ASPxButton runat="server" ID="brnClear" AutoPostBack="false" Text="Clear" CssClass="dxeLineBreakFix">
                                            <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('Clear');}" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="right"></td>
                                    <td colspan="2" align="right"></td>
                                    <td colspan="1" align="right"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxPageControl ID="pageControl" ClientInstanceName="pageControl" runat="server"
                                EnableCallBacks="false" Width="100%" ActiveTabIndex="0">
                                <ClientSideEvents ActiveTabChanged="
                                    function(s, e) {
                                    xcpn.PerformCallback('Search;' + e.tab.index);
	                                }" />
                                <TabPages>
                                    <dx:TabPage Text="งานขนส่งภายในวัน" Visible="true">
                                        <ContentCollection>
                                            <dx:ContentControl ID="ctctrl1" runat="server">
                                                <dx:ASPxButton runat="server" ID="ASPxButton1" AutoPostBack="false" Text="Refresh" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('Search');}" />
                                                </dx:ASPxButton>
                                                <dx:ASPxLabel Text="งานขนส่งภายในวันทั้งหมด " ID="ASPxLabel1" runat="server" />
                                                <dx:ASPxLabel Text="" ID="lblPlanAll" runat="server" />
                                                <dx:ASPxLabel Text=" รายการ" ID="ASPxLabel2" runat="server" />
                                                <dx:ASPxLabel Text="จัดแผนแล้ว " ID="ASPxLabel3" runat="server" ForeColor="Red" />
                                                <dx:ASPxLabel ClientInstanceName="lblPlan" Text="" ID="lblPlan" ForeColor="Red" runat="server" />
                                                <dx:ASPxLabel Text="&nbsp;รายการ" ID="ASPxLabel5" runat="server" ForeColor="Red" />

                                                <dx:ASPxGridView runat="server" ID="gvw" ClientInstanceName="gvw" AutoGenerateColumns="false" KeyFieldName="ORDERID"
                                                    Width="100%">
                                                    <ClientSideEvents EndCallback="function(s,e){ eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}"></ClientSideEvents>
                                                    <SettingsPager Mode="ShowPager">
                                                    </SettingsPager>
                                                    <Columns>
                                                        <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <HeaderTemplate>
                                                                <dx:ASPxLabel ID="lblHead" runat="server" Text="งานขนส่งภายในวัน">
                                                                </dx:ASPxLabel>
                                                            </HeaderTemplate>
                                                            <Columns>
                                                                <dx:GridViewDataDateColumn Caption="วันที่รับงาน" FieldName="DDELIVERY" HeaderStyle-HorizontalAlign="Center" PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataColumn Caption="เที่ยวที่รับงาน" FieldName="SWINDOWTIMENAME" HeaderStyle-HorizontalAlign="Center" Width="8%">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>

                                                                <dx:GridViewDataColumn Caption="Delivery No." FieldName="SDELIVERYNO" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="8%">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="lblDo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"SDELIVERYNO") %>'
                                                                            Cursor="pointer" Font-Underline="true">
                                                                            <ClientSideEvents Click="function(s,e){ var inx = s.name.substring(s.name.lastIndexOf('_')+1,s.name.length); txtDeliveryNo.SetText(inx);  txtSelectgvw.SetText('1'); PopDetail.Show(); }" />
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="คลังต้นทาง" FieldName="STERMINALNAME_FROM" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ปลายทาง" FieldName="STERMINALNAME" HeaderStyle-HorizontalAlign="Center" Width="18%">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataTextColumn Caption="ปริมาณ" FieldName="NVALUE" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                    <PropertiesTextEdit DisplayFormatString="{0:n0}">
                                                                    </PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn Caption="วันที่ถึง<br/>ปลายทาง" FieldName="LATEDATE" HeaderStyle-HorizontalAlign="Center" PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataDateColumn>

                                                                <dx:GridViewDataColumn Caption="เวลาที่ถึง<br/>ปลายทาง" FieldName="LATETIME" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="100px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataTextColumn Caption="เวลาที่รถ<br/>ต้องถึงคลัง" HeaderStyle-HorizontalAlign="Center">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="lblDATETIMEEXPECT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"DATETIMEEXPECT") %>'>
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="คาดว่ารถจะ<br/>ถึงคลังต้นทาง" HeaderStyle-HorizontalAlign="Center">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="txtLate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"DATETIMETOTERMINAL") %>'>
                                                                        </dx:ASPxLabel>
                                                                        <dx:ASPxTextBox ID="txtLateTextBox" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"DATETIMETOTERMINAL") %>' ClientVisible="false"></dx:ASPxTextBox>
                                                                        <dx:ASPxLabel ID="txtLateBefore" runat="server" Text="" ClientVisible="false">
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataColumn Caption="Drop No." HeaderStyle-HorizontalAlign="Center" Width="4%">
                                                                    <DataItemTemplate>
                                                                        <%--  <dx:ASPxTextBox runat="server" ID="txtLoop" Text='<%# DataBinder.Eval(Container.DataItem,"ROUND") %>'
                                                        RightToLeft="True" ClientEnabled='<%#  Eval("CANCEL").ToString() == "0"  ? false :true  %>'>
                                                        <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                    </dx:ASPxTextBox>--%>
                                                                        <dx:ASPxTextBox runat="server" ID="txtLoop" Text='<%# DataBinder.Eval(Container.DataItem,"ROUND") %>'
                                                                            RightToLeft="True" ToolTip="กรณีเป็น Muti Drop ให้ใส่เลข 1,2,3,....">
                                                                            <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                        </dx:ASPxTextBox>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ทะเบียนรถ" FieldName="SABBREVIATION" HeaderStyle-HorizontalAlign="Center">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxComboBox ID="gvwcboCarregis" runat="server"
                                                                            ItemStyle-Wrap="True"
                                                                            SkinID="xcbbATC" TextFormatString="{0} {1}" ValueField="STRUCKID" Width="155px"
                                                                            CssClass="dxeLineBreakFix" Height="25" IncrementalFilteringDelay="500">
                                                                            <Columns>
                                                                                <dx:ListBoxColumn FieldName="STRUCKID" Width="80px" Caption="รหัส" Visible="false"></dx:ListBoxColumn>
                                                                                <dx:ListBoxColumn FieldName="SHEADREGISTERNO" Width="100px" Caption="ทะเบียนหัว"></dx:ListBoxColumn>
                                                                                <dx:ListBoxColumn FieldName="STRAILERREGISTERNO" Width="100px" Caption="ทะเบียนหาง"></dx:ListBoxColumn>
                                                                            </Columns>
                                                                            <ItemStyle Wrap="True"></ItemStyle>
                                                                            <%--<ValidationSettings ErrorDisplayMode="ImageWithTooltip" Display="Dynamic" RequiredField-IsRequired="true"
                                                            RequiredField-ErrorText="ระบุทะเบียรถ" ValidationGroup="add">
                                                            <RequiredField IsRequired="True" ErrorText="ระบุบริษัท"></RequiredField>
                                                        </ValidationSettings>--%>
                                                                            <%--<ClientSideEvents ButtonClick="function (s, e) { alert('xx');  }" />--%>
                                                                        </dx:ASPxComboBox>

                                                                        <dx:ASPxCallback ID="xcpnCheckLate" runat="server" HideContentOnCallback="True" OnCallback="xcpnCheckLate_Callback">
                                                                            <ClientSideEvents BeginCallback="function(s,e){ LoadingPanel.Show();}" EndCallback="function(s,e){ LoadingPanel.Hide();}" />
                                                                        </dx:ASPxCallback>
                                                                        <%----%>
                                                                        <dx:ASPxCallback ID="xcpnOutBound" runat="server" HideContentOnCallback="True" ClientInstanceName="xcpnOutBound"
                                                                            OnCallback="xcpnOutBound_Callback">
                                                                            <%-- <ClientSideEvents CallbackComplete="function(s,e){  if(e.result != '') {  if(e.result == 'S'){ alert('ss'); dxInfo('แจ้งให้ทราบ','การจัดแผนผ่าน');  }else{ dxError('พบข้อผิดพลาด',e.result);  }} }" EndCallback="function(s,e){    }">
                                                        </ClientSideEvents>--%>
                                                                            <ClientSideEvents BeginCallback="function(s,e){ LoadingPanel.Show();}" EndCallback="function(s,e){ LoadingPanel.Hide();}" />
                                                                        </dx:ASPxCallback>

                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="จัดแผน" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxButton ID="btnConfirm" runat="server" Text="ยืนยัน" CausesValidation="False" AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton ID="btnCancel" runat="server" Text="ยกเลิก" CausesValidation="False" AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                                        </dx:ASPxButton>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="สถานะ" FieldName="STATUS" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel runat="server" ID="lblStatus" Text='<%# DataBinder.Eval(Container.DataItem,"STATUS") %>'>
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <%-- <dx:GridViewDataColumn>
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtSTRUCKID" Text='<%# DataBinder.Eval(Container.DataItem,"STRUCKID") %>' Width="100px">
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                            </dx:GridViewDataColumn>--%>
                                                                <dx:GridViewDataColumn FieldName="ORDERID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="STRUCKID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="NWINDOWTIMEID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="SVENDORID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="STERMINALID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="SCONTRACTID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="SCONTRACTNO" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="SHIP_TO" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="ORDERTYPE" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="CANCEL" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="DATE_CREATE" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="FULLNAME" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="SEMAIL" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <%--   <dx:GridViewDataColumn Caption=" " HeaderStyle-HorizontalAlign="Center" Width="13%">
                                                <DataItemTemplate>
                                                    <dx:ASPxButton runat="server" ID="btnPaln" Text="จัดแผน" AutoPostBack="false">
                                                        <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('save;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                    </dx:ASPxButton>
                                                </DataItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </dx:GridViewDataColumn>--%>
                                                            </Columns>
                                                        </dx:GridViewBandColumn>
                                                    </Columns>
                                                </dx:ASPxGridView>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>
                                    <dx:TabPage Text="งานขนส่งล่วงหน้า" Visible="true">
                                        <ContentCollection>
                                            <dx:ContentControl ID="ContentControl1" runat="server">
                                                <dx:ASPxButton runat="server" ID="ASPxButton2" AutoPostBack="false" Text="Refresh" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('Search');}" />
                                                </dx:ASPxButton>
                                                <dx:ASPxLabel Text="งานขนส่งล่วงหน้าทั้งหมด " ID="ASPxLabel4" runat="server" />
                                                <dx:ASPxLabel Text="" ID="lblPlanAll2" runat="server" />
                                                <dx:ASPxLabel Text=" รายการ" ID="ASPxLabel7" runat="server" />
                                                <dx:ASPxLabel Text="จัดแผนแล้ว " ID="ASPxLabel8" runat="server" ForeColor="Red" />
                                                <dx:ASPxLabel Text="" ID="lblPlan2" ClientInstanceName="lblPlan2" ForeColor="Red" runat="server" />
                                                <dx:ASPxLabel Text="&nbsp;รายการ" ID="ASPxLabel10" runat="server" ForeColor="Red" />
                                                <dx:ASPxGridView runat="server" ID="gvw2" ClientInstanceName="gvw2" AutoGenerateColumns="false" KeyFieldName="ORDERID"
                                                    Width="100%">
                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}"></ClientSideEvents>
                                                    <Columns>
                                                        <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <HeaderTemplate>
                                                                <dx:ASPxLabel ID="lblHead" runat="server" Text="งานขนส่งล่วงหน้า">
                                                                </dx:ASPxLabel>
                                                            </HeaderTemplate>
                                                            <Columns>
                                                                <dx:GridViewDataDateColumn Caption="วันที่รับงาน" FieldName="DDELIVERY" HeaderStyle-HorizontalAlign="Center" PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataColumn Caption="เที่ยวที่รับงาน" FieldName="SWINDOWTIMENAME" HeaderStyle-HorizontalAlign="Center" Width="8%">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>

                                                                <dx:GridViewDataColumn Caption="Delivery No." FieldName="SDELIVERYNO" HeaderStyle-HorizontalAlign="Center">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="lblDo2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"SDELIVERYNO") %>'
                                                                            Cursor="pointer" Font-Underline="true">
                                                                            <ClientSideEvents Click="function(s,e){ var inx = s.name.substring(s.name.lastIndexOf('_')+1,s.name.length); txtDeliveryNo.SetText(inx);  txtSelectgvw.SetText('2'); PopDetail.Show(); }" />
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="คลังต้นทาง" FieldName="STERMINALNAME_FROM" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ปลายทาง" FieldName="STERMINALNAME" HeaderStyle-HorizontalAlign="Center" Width="18%">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataTextColumn Caption="ปริมาณ" FieldName="NVALUE" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                    <PropertiesTextEdit DisplayFormatString="{0:n0}">
                                                                    </PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn Caption="วันที่ถึง<br/>ปลายทาง" FieldName="LATEDATE" HeaderStyle-HorizontalAlign="Center" PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataDateColumn>

                                                                <dx:GridViewDataColumn Caption="เวลาที่ถึง<br/>ปลายทาง" FieldName="LATETIME" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="100px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataTextColumn Caption="เวลาที่รถ<br/>ต้องถึงคลัง" HeaderStyle-HorizontalAlign="Center">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="lblDATETIMEEXPECT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"DATETIMEEXPECT") %>'>
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="คาดว่ารถจะ<br/>ถึงคลังต้นทาง" HeaderStyle-HorizontalAlign="Center">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="txtLate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"DATETIMETOTERMINAL") %>'>
                                                                        </dx:ASPxLabel>
                                                                        <dx:ASPxTextBox ID="txtLateTextBox" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"DATETIMETOTERMINAL") %>' ClientVisible="false"></dx:ASPxTextBox>
                                                                        <dx:ASPxLabel ID="txtLateBefore" runat="server" Text="" ClientVisible="false">
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataColumn Caption="Drop No." HeaderStyle-HorizontalAlign="Center" Width="4%">
                                                                    <DataItemTemplate>
                                                                        <%-- <dx:ASPxTextBox runat="server" ID="txtLoop" Text='<%# DataBinder.Eval(Container.DataItem,"ROUND") %>'
                                                        RightToLeft="True" ClientEnabled='<%#  Eval("CANCEL").ToString() == "0"  ? false :true  %>'>
                                                        <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                    </dx:ASPxTextBox>--%>
                                                                        <dx:ASPxTextBox runat="server" ID="txtLoop" Text='<%# DataBinder.Eval(Container.DataItem,"ROUND") %>' ToolTip="กรณีเป็น Muti Drop ให้ใส่เลข 1,2,3,...."
                                                                            RightToLeft="True">
                                                                            <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                                        </dx:ASPxTextBox>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ทะเบียนรถ" FieldName="SABBREVIATION" HeaderStyle-HorizontalAlign="Center">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxComboBox ID="gvwcboCarregis" runat="server"
                                                                            ItemStyle-Wrap="True"
                                                                            SkinID="xcbbATC" TextFormatString="{0} {1}" ValueField="STRUCKID" Width="155px"
                                                                            CssClass="dxeLineBreakFix" Height="25" IncrementalFilteringDelay="500">
                                                                            <Columns>
                                                                                <dx:ListBoxColumn FieldName="STRUCKID" Width="80px" Caption="รหัส" Visible="false"></dx:ListBoxColumn>
                                                                                <dx:ListBoxColumn FieldName="SHEADREGISTERNO" Width="100px" Caption="ทะเบียนหัว"></dx:ListBoxColumn>
                                                                                <dx:ListBoxColumn FieldName="STRAILERREGISTERNO" Width="100px" Caption="ทะเบียนหาง"></dx:ListBoxColumn>
                                                                            </Columns>
                                                                            <ItemStyle Wrap="True"></ItemStyle>
                                                                            <%--<ValidationSettings ErrorDisplayMode="ImageWithTooltip" Display="Dynamic" RequiredField-IsRequired="true"
                                                            RequiredField-ErrorText="ระบุทะเบียรถ" ValidationGroup="add">
                                                            <RequiredField IsRequired="True" ErrorText="ระบุบริษัท"></RequiredField>
                                                        </ValidationSettings>--%>
                                                                            <%--<ClientSideEvents ButtonClick="function (s, e) { alert('xx');  }" />--%>
                                                                        </dx:ASPxComboBox>
                                                                        <dx:ASPxTextBox runat="server" ID="txtLate" Text='' ClientVisible="false">
                                                                        </dx:ASPxTextBox>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>

                                                                <dx:GridViewDataColumn Caption="จัดแผน" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxButton ID="btnConfirm" runat="server" Text="ยืนยัน" CssClass="dxeLineBreakFix" CausesValidation="False" AutoPostBack="false">
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton ID="btnCancel" runat="server" Text="ยกเลิก" CssClass="dxeLineBreakFix" CausesValidation="False" AutoPostBack="false">
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxCallback ID="xcpnCheckLate" runat="server" HideContentOnCallback="True" OnCallback="xcpnCheckLate_Callback" >
                                                                            <ClientSideEvents BeginCallback="function(s,e){ LoadingPanel.Show();}" EndCallback="function(s,e){ LoadingPanel.Hide();}" />
                                                                        </dx:ASPxCallback>
                                                                        <%--OnCallback="xcpnCheckLate_Callback"--%>
                                                                        <dx:ASPxCallback ID="xcpnOutBound" runat="server" HideContentOnCallback="False"
                                                                            ClientInstanceName="xcpnOutBound2" OnCallback="xcpnOutBound_Callback">
                                                                            <%--     <ClientSideEvents CallbackComplete="function(s,e){  if(e.result != '') {  if(e.result == 'S'){ alert('ss'); dxInfo('แจ้งให้ทราบ','การจัดแผนผ่าน');  }else{ dxError('พบข้อผิดพลาด',e.result);  }} }" EndCallback="function(s,e){    }">
                                                        </ClientSideEvents>--%>
                                                                            <ClientSideEvents BeginCallback="function(s,e){ LoadingPanel.Show();}" EndCallback="function(s,e){ LoadingPanel.Hide();}" />
                                                                        </dx:ASPxCallback>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="สถานะ" FieldName="STATUS" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel runat="server" ID="lblStatus" Text='<%# DataBinder.Eval(Container.DataItem,"STATUS") %>'>
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="ORDERID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="STRUCKID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="NWINDOWTIMEID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="SVENDORID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="STERMINALID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="SCONTRACTID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="SCONTRACTNO" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="SHIP_TO" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="ORDERTYPE" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="CANCEL" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="DATE_CREATE" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="FULLNAME" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="SEMAIL" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                            </Columns>
                                                        </dx:GridViewBandColumn>
                                                    </Columns>
                                                </dx:ASPxGridView>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>
                                    <dx:TabPage Text="งานขนส่งที่เกินระยะเวลาจัดแผน" Visible="true">
                                        <ContentCollection>
                                            <dx:ContentControl ID="ContentControl2" runat="server">
                                                <dx:ASPxGridView runat="server" ID="gvw3" ClientInstanceName="gvw3" AutoGenerateColumns="false"
                                                    Width="100%">
                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}"></ClientSideEvents>
                                                    <Columns>
                                                        <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <HeaderTemplate>
                                                                <dx:ASPxLabel ID="lblHead" runat="server" Text="งานขนส่งที่เกินระยะเวลาจัดแผน">
                                                                </dx:ASPxLabel>
                                                            </HeaderTemplate>
                                                            <Columns>
                                                                <dx:GridViewDataDateColumn Caption="วันที่รับงาน" FieldName="DDELIVERY" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="8%" PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataColumn Caption="กะขนส่ง" FieldName="SWINDOWTIMENAME" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="14%" Settings-AllowAutoFilter="False">
                                                                    <Settings AllowAutoFilter="False"></Settings>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="Delivery No." FieldName="SDELIVERYNO" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="8%">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="lblDo2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"SDELIVERYNO") %>'
                                                                            Cursor="pointer" Font-Underline="true">
                                                                            <ClientSideEvents Click="function(s,e){ var inx = s.name.substring(s.name.lastIndexOf('_')+1,s.name.length); txtDeliveryNo.SetText(inx);  txtSelectgvw.SetText('3'); PopDetail.Show(); }" />
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ต้นทาง" FieldName="STERMINALNAME_FROM" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="14%" Settings-AllowAutoFilter="False">
                                                                    <Settings AllowAutoFilter="False"></Settings>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ปลายทาง" FieldName="STERMINALNAME" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="14%" Settings-AllowAutoFilter="False">
                                                                    <Settings AllowAutoFilter="False"></Settings>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataTextColumn Caption="ปริมาณ" FieldName="NVALUE" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="8%" Settings-AllowAutoFilter="False">
                                                                    <Settings AllowAutoFilter="False"></Settings>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                    <PropertiesTextEdit DisplayFormatString="{0:n0}">
                                                                    </PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <%--  <dx:GridViewDataColumn Caption="Drop" HeaderStyle-HorizontalAlign="Center" Width="8%">
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtLoop2" Text='<%# DataBinder.Eval(Container.DataItem,"ROUND") %>'
                                                        RightToLeft="True">
                                                        <ClientSideEvents ValueChanged="function (s,e){ CheckNumber(s,e); }" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="ทะเบียนรถ" FieldName="SABBREVIATION" HeaderStyle-HorizontalAlign="Center"
                                                Width="13%">
                                                <DataItemTemplate>
                                                   <dx:ASPxComboBox ID="gvwcboCarregis2" runat="server" CallbackPageSize="30" ItemStyle-Wrap="True"
                                                        EnableCallbackMode="True" OnItemRequestedByValue="cboCarregisXc_OnItemRequestedByValueSQL"
                                                        OnItemsRequestedByFilterCondition="cboCarregisXc_ItemsRequestedByFilterConditionSQL"
                                                        SkinID="xcbbATC" TextFormatString="{1} {2}" ValueField="SHEADID" Width="180px"
                                                        CssClass="dxeLineBreakFix" Height="25" IncrementalFilteringDelay="500">
                                                        <Columns>
                                                            <dx:ListBoxColumn FieldName="SHEADID" Width="80px" Caption="รหัส"></dx:ListBoxColumn>
                                                            <dx:ListBoxColumn FieldName="SHEADREGISTERNO" Width="100px" Caption="ทะเบียนหัว">
                                                            </dx:ListBoxColumn>
                                                            <dx:ListBoxColumn FieldName="STRAILERREGISTERNO" Width="100px" Caption="ทะเบียนหาง">
                                                            </dx:ListBoxColumn>
                                                        </Columns>
                                                        <ItemStyle Wrap="True"></ItemStyle>
                                                    </dx:ASPxComboBox>
                                                    <dx:ASPxCallback ID="xcpnOutBound2" runat="server" HideContentOnCallback="False"
                                                        ClientInstanceName="xcpnOutBound2" OnCallback="xcpnOutBound2_Callback">
                                                        <ClientSideEvents BeginCallback="function(s,e){ LoadingPanel.Show();}" EndCallback="function(s,e){ LoadingPanel.Hide();}" />
                                                    </dx:ASPxCallback>
                                                </DataItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="สถานะ" FieldName="STATUS" HeaderStyle-HorizontalAlign="Center"
                                                Width="10%" CellStyle-HorizontalAlign="Center">
                                                <DataItemTemplate>
                                                    <dx:ASPxLabel runat="server" ID="lblStatus" Text='<%# DataBinder.Eval(Container.DataItem,"STATUS") %>'>
                                                    </dx:ASPxLabel>
                                                </DataItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>--%>
                                                                <dx:GridViewDataColumn FieldName="ORDERID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="STRUCKID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="NWINDOWTIMEID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="SVENDORID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="STERMINALID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="SCONTRACTID" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="SCONTRACTNO" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="SHIP_TO" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="ORDERTYPE" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="CANCEL" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="DATE_CREATE" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="FULLNAME" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn FieldName="SEMAIL" Visible="false">
                                                                </dx:GridViewDataColumn>
                                                            </Columns>
                                                        </dx:GridViewBandColumn>
                                                    </Columns>
                                                    <Settings ShowFilterRow="True" />
                                                </dx:ASPxGridView>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>
                                </TabPages>
                            </dx:ASPxPageControl>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxPopupControl runat="server" ID="PopDetail" ClientInstanceName="PopDetail"
                                CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                                HeaderText="รายละเอียด" AllowDragging="True" PopupAnimationType="None" EnableViewState="False"
                                Width="800px" AutoUpdatePosition="true" CssClass="Poop">
                                <ClientSideEvents PopUp="function(s,e){ xcpnPopup.PerformCallback('ListData'); }"
                                    Closing="function(s,e){ lblDoNo.SetClientVisible(false);  gvwPop.SetClientVisible(false); lblDONODATA.SetClientVisible(false);}" />
                                <ContentCollection>
                                    <dx:PopupControlContentControl>
                                        <dx:ASPxCallbackPanel ID="xcpnPopup" runat="server" HideContentOnCallback="False"
                                            OnCallback="xcpnPopup_Callback" ClientInstanceName="xcpnPopup" CausesValidation="False"
                                            OnLoad="xcpnPopup_Load" LoadingDivStyle-CssClass="SS">
                                            <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}"></ClientSideEvents>
                                            <LoadingDivStyle CssClass="SS">
                                            </LoadingDivStyle>
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <table width="100%" cellpadding="3" cellspacing="1">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxLabel runat="server" ID="lblDoNo" ClientInstanceName="lblDoNo" ClientVisible="false">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxGridView runat="server" ID="gvwPop" Width="100%" ClientInstanceName="gvwPop"
                                                                    ClientVisible="false">
                                                                    <Columns>
                                                                        <dx:GridViewDataColumn Caption="ลำดับ" Width="10%" HeaderStyle-HorizontalAlign="Center"
                                                                            CellStyle-HorizontalAlign="Center">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn Caption="ชื่อผลิตภัณฑ์" FieldName="MATERIAL_NAME" Width="60%"
                                                                            HeaderStyle-HorizontalAlign="Center">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ปริมาณ" FieldName="QTY_TRAN" Width="15%" HeaderStyle-HorizontalAlign="Center"
                                                                            CellStyle-HorizontalAlign="Center">
                                                                            <PropertiesTextEdit DisplayFormatString="{0:n0}">
                                                                            </PropertiesTextEdit>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="วันที่ขนส่ง" FieldName="DEV_DATE" Width="15%"
                                                                            HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
                                                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                                                            </PropertiesTextEdit>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                </dx:ASPxGridView>
                                                            </td>
                                                        </tr>
                                                        <tr align="center">
                                                            <td>
                                                                <dx:ASPxLabel runat="server" ID="lblDONODATA" ClientInstanceName="lblDONODATA" Text="ไม่พบข้อมูล"
                                                                    ForeColor="Red" ClientVisible="false">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxCallbackPanel>
                                    </dx:PopupControlContentControl>
                                </ContentCollection>
                            </dx:ASPxPopupControl>
                            <dx:ASPxTextBox runat="server" ID="txtDeliveryNo" ClientInstanceName="txtDeliveryNo"
                                ClientVisible="false">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtSelectgvw" ClientInstanceName="txtSelectgvw"
                                ClientVisible="false">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>

    <script type="text/javascript">        

        setdatepicker();

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            setdatepicker();
        }
        function setdatepicker() {
            $(document).keypress(function (e) {
                if (e.which == 13) {
                    return false;
                }
            });

            //alert('test');
           
            $('.edtAssign').parent().click(function () {
                var date = new Date();
                var dates = new Date();
                date.setDate(date.getDate() - 3);
                $("#<%=edtAssign.ClientID %>").parent('.input-group.date').datepicker('update').setStartDate(date);
                //$(this).find('input').parent('.input-group.date').datepicker('update').setStartDate(date);
               
                $("#<%=edtAssign.ClientID %>").parent('.input-group.date').datepicker('update').setEndDate(dates);
            });
                       
        }

    </script>
    <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel">
    </dx:ASPxLoadingPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
