﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="vendor_HomeAlert.aspx.cs" Inherits="vendor_HomeAlert" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript">
        function ListData(Value) {
            // alert(Value);
            txtDate.SetText(Value);
            xcpnCalendar.PerformCallback('ListData');
        }
    </script>
    <style type="text/css">
        radius
        {
            border-left: 10px;
            border-right-radius: 10px;
            border-bottom-radius: 10px;
            border-top--radius: 10px;
        }
        
        .RED
        {
            color: Red;
        }
        
        .DEFALUT
        {
            color: #03527c;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load" LoadingPanelStyle-HorizontalAlign="Center" LoadingPanelStyle-VerticalAlign="Middle">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <table width="100%" border="0" cellpadding="2" cellspacing="1">
                    <tr>
                        <td width="71%" valign="top">
                            <table width="100%" border="0" cellspacing="2" cellpadding="5">
                                <tr>
                                    <td width="50%" align="left"><img src="images/ic_cst_calendar.gif" width="16" height="13"
                                    alt="" /> แจ้งเตือนรถหมดอายุวัดน้ำ
                                        <dx:ASPxTextBox ID="lblVendorID" runat="server" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td width="50%" align="right">
                                        <dx:ASPxComboBox ID="cbStatus" runat="server">
                                            <ClientSideEvents ValueChanged="function(s,e){xcpn.PerformCallback('change');}" />
                                        </dx:ASPxComboBox>
                                        <asp:SqlDataSource ID="sdsStatusReq" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                            ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                            SelectCommand="SELECT '0' as ID ,'- สถานะทั้งหมด -' as NAME ,-1 as NORDER FROM DUAL
UNION
SELECT '00' as ID ,'ยังไม่ยื่นคำขอ' as NAME,0 as NORDER FROM DUAL
UNION
SELECT STATUSREQ_ID as ID, STATUSREQ_NAME,NORDER as NAME
 FROM TBL_STATUSREQ
WHERE ISACTIVE_FLAG = 'Y' 
ORDER BY NORDER ASC

"></asp:SqlDataSource>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="29%" valign="top"></td>
                    </tr>
                    <tr>
                        <td width="71%" valign="top">
                            <%--<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><img src="images/spacer.GIF" width="250px" height="1px" /> </td>
                                </tr>
                            </table>--%>
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" Style="margin-top: 0px"
                                ClientInstanceName="gvw" Width="100%" KeyFieldName="STRUCKID" SkinID="_gvw" OnAfterPerformCallback="gvw_AfterPerformCallback">
                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = &#39;&#39;;if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}">
                                </ClientSideEvents>
                                <Columns>
                                    <dx:GridViewDataColumn Caption="ที่" ShowInCustomizationForm="True" Width="5%">
                                        <DataItemTemplate>
                                            <label id="lblNo" runat="server">
                                                <%# (Container.ItemIndex + 1) %></label>.</DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ทะเบียนรถหัว/ท้าย" Width="20%" FieldName="RAILERREGISTERNUMBER">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel Text='<%# Eval("RAILERREGISTERNUMBER") %>' runat="server" ID="xlbldCarConFirm" />
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="สถานะ" Width="14%" FieldName="STATUSNAME">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel Text='<%# Eval("STATUSNAME").ToString() ==""?"ยังไม่ยื่นคำขอ":Eval("STATUSNAME").ToString() %>'
                                                runat="server" ID="xlbldCarConFirm" CssClass='<%# Eval("COLOR").ToString() %>' />
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="หมดอายุวัดน้ำ" HeaderStyle-HorizontalAlign="Center"
                                        Width="10%" FieldName="DWATEREXPIRE">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel Text='<%# Eval("DWATEREXPIRE", "{0:dd/MM/yyyy}") %>' runat="server"
                                                ID="xlbldCarConFirm" />
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ยื่นคำขอ" HeaderStyle-HorizontalAlign="Center" Width="10%"
                                        FieldName="REQUEST_DATE">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel Text='<%# Eval("REQUEST_DATE").ToString()==""?"-":Eval("REQUEST_DATE", "{0:dd/MM/yyyy}").ToString()%>'
                                                runat="server" ID="xlbldPlanConfirm" />
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ขอนัดหมาย" HeaderStyle-HorizontalAlign="Center" Width="10%"
                                        FieldName="APPOINTMENT_DATE">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel Text='<%# Eval("APPOINTMENT_DATE").ToString()==""?"-":Eval("APPOINTMENT_DATE", "{0:dd/MM/yyyy}").ToString()%>'
                                                runat="server" ID="xlbldPlanConfirm" />
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="วันนัดหมาย" HeaderStyle-HorizontalAlign="Center"
                                        Width="10%" FieldName="ACCEPT_DATE">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel Text='<%# Eval("ACCEPT_DATE").ToString()==""?"-":Eval("ACCEPT_DATE", "{0:dd/MM/yyyy}").ToString() %>'
                                                runat="server" ID="xlbsupdate" />
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn CellStyle-Cursor="hand" Caption="" Width="5%">
                                        <DataItemTemplate>
                                            <%--   <dx:ASPxButton ID="imbView" runat="server" SkinID="_edit" CausesValidation="False">
                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                        </dx:ASPxButton>--%>
                                            <dx:ASPxButton ID="btnView" runat="server" AutoPostBack="false" EnableTheming="False"
                                                EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer" ClientVisible='<%# Eval("LOCK_REQ").ToString()=="N"? false : true %>'>
                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('View;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                <Image Url="~/Images/ic_search.gif" Height="16" Width="16">
                                                </Image>
                                            </dx:ASPxButton>
                                             <dx:ASPxButton ID="ASPxButton1" runat="server" AutoPostBack="false" EnableTheming="False"
                                                EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer" ClientVisible='<%# Eval("LOCK_REQ").ToString()!="N"? false : true %>'>
                                                <ClientSideEvents Click="function(s,e){ dxWarning('แจ้งเตือน', 'ระบบได้ทำการล็อค <br> เนื่องจากท่านไม่ได้แนบใบเทียบแป้นใน 30 วัน'); }" />
                                                <Image Url="~/Images/lock.png" Height="18" Width="18">
                                                </Image>
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand" HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="สถานะ" VisibleIndex="1" FieldName="STATUSID" Visible="False">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="รหัสคำขอ" VisibleIndex="1" FieldName="REQUEST_ID"
                                        Visible="False">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="รหัสรถหัว" VisibleIndex="1" FieldName="VEH_ID" Visible="False">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="รหัสรถหัว" VisibleIndex="1" FieldName="TU_ID" Visible="False">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ประเภทรถ" VisibleIndex="1" FieldName="SCARTYPEID"
                                        Visible="False">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <%-- <dx:GridViewDataTextColumn Caption="สถานะรถ" VisibleIndex="1" FieldName="STATUSCAR" Visible="False">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataTextColumn>--%>
                                </Columns>
                                <SettingsBehavior AllowSort="true" />
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <table>
                                <tr>
                                    <td align="right" width="60%">&nbsp; </td>
                                    <td align="right">&nbsp; </td>
                                    <td align="right">&nbsp; </td>
                                    <td align="right">&nbsp; </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                <tr>
                                    <td><img src="images/calendar.png" width="16" height="16" alt="" /> คำขออื่นๆ </td>
                                    <td align="right">
                                        <dx:ASPxButton ID="btnRequest" ClientInstanceName="btnRequest" runat="server" Text=" เพิ่มคำขออื่นๆ "
                                            OnClick="btnRequest_Click">
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0">
                                <tr>
                                    <td>
                                        <dx:ASPxGridView ID="gvw2" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                            Style="margin-top: 0px" ClientInstanceName="gvw2" Width="100%" KeyFieldName="STRUCKID"
                                            SkinID="_gvw" OnAfterPerformCallback="gvw2_AfterPerformCallback">
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" Width="5%">
                                                    <DataItemTemplate>
                                                        <label id="lblNo" runat="server">
                                                            <%# (Container.ItemIndex + 1) %></label></DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ทะเบียนรถหัว/ท้าย" HeaderStyle-HorizontalAlign="Center"
                                                    Width="23%" FieldName="REGISNO">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel Text='<%# Eval("REGISNO") %>' runat="server" ID="xlbldCarConFirm" />
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="สถานะ" HeaderStyle-HorizontalAlign="Center" Width="14%"
                                                    FieldName="STATUSREQ_NAME">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel Text='<%# Eval("STATUSREQ_NAME") %>' runat="server" ID="xlbldCarConFirm"
                                                            CssClass='<%# Eval("COLOR").ToString() %>' />
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ประเภทคำขอ" HeaderStyle-HorizontalAlign="Center"
                                                    Width="20%" FieldName="REQTYPE_NAME">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel Text='<%# Eval("REQTYPE_NAME") %>' runat="server" ID="xlbldCarConFirm" />
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ยื่นคำขอ" HeaderStyle-HorizontalAlign="Center"
                                                    Width="10%" FieldName="REQUESTDATE">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel Text='<%# Eval("REQUESTDATE").ToString()==""?"-":Eval("REQUESTDATE", "{0:dd/MM/yyyy}").ToString() %>'
                                                            runat="server" ID="xlbldCarConFirm" />
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ขอนัดหมาย" HeaderStyle-HorizontalAlign="Center"
                                                    Width="10%" FieldName="APPDATE">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel Text='<%# Eval("APPDATE").ToString()==""?"-":Eval("APPDATE", "{0:dd/MM/yyyy}").ToString() %>'
                                                            runat="server" ID="xlbsupdate" />
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="วันนัดหมาย" HeaderStyle-HorizontalAlign="Center"
                                                    Width="10%" FieldName="ACCEPTDATE">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel Text='<%# Eval("ACCEPTDATE").ToString()==""?"-":Eval("ACCEPTDATE", "{0:dd/MM/yyyy}").ToString() %>'
                                                            runat="server" ID="xlbsupdate" />
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataColumn CellStyle-Cursor="hand" Caption="" Width="5%">
                                                    <DataItemTemplate>
                                                        <%--   <dx:ASPxButton ID="imbView" runat="server" SkinID="_edit" CausesValidation="False">
                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                        </dx:ASPxButton>--%>
                                                        <dx:ASPxButton ID="btnView" runat="server" AutoPostBack="false" EnableTheming="False"
                                                            EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"  ClientVisible='<%# Eval("LOCK_REQ").ToString()=="N"? false : true %>'>
                                                            <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('View2;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                            <Image Url="~/Images/ic_search.gif" Height="16" Width="16">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                          <dx:ASPxButton ID="ASPxButton1" runat="server" AutoPostBack="false" EnableTheming="False"
                                                EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer" ClientVisible='<%# Eval("LOCK_REQ").ToString()!="N"? false : true %>'>
                                                <ClientSideEvents Click="function(s,e){ dxWarning('แจ้งเตือน', 'ระบบได้ทำการล็อค <br> เนื่องจากท่านไม่ได้แนบใบเทียบแป้นใน 30 วัน'); }" />
                                                <Image Url="~/Images/lock.png" Height="18" Width="18">
                                                </Image>
                                            </dx:ASPxButton>
                                                    </DataItemTemplate>
                                                    <CellStyle Cursor="hand" HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataTextColumn Caption="สถานะ" VisibleIndex="1" FieldName="STATUSREQ_ID"
                                                    Visible="False">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="รหัสคำขอ" VisibleIndex="1" FieldName="REQUEST_ID"
                                                    Visible="False">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="รหัสรถหัว" VisibleIndex="1" FieldName="VEH_ID"
                                                    Visible="False">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="รหัสรถหาง" VisibleIndex="1" FieldName="TU_ID"
                                                    Visible="False">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="รหัสประเภทคำขอ" VisibleIndex="1" FieldName="REQTYPE_ID"
                                                    Visible="False">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataTextColumn>
                                                <%-- <dx:GridViewDataTextColumn Caption="สถานะรถ" VisibleIndex="1" FieldName="STATUSCAR" Visible="False">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataTextColumn>--%>
                                            </Columns>
                                            <SettingsPager AlwaysShowPager="True">
                                            </SettingsPager>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="29%" valign="top">
                            <dx:ASPxCallbackPanel ID="xcpnCalendar" runat="server" OnCallback="xcpnCalendar_Callback"
                                ClientInstanceName="xcpnCalendar" CausesValidation="False" OnLoad="xcpnCalendar_Load">
                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}">
                                </ClientSideEvents>
                                <PanelCollection>
                                    <dx:PanelContent ID="PanelContent1" runat="server">
                                        <table width="100%" border="0" cellpadding="3" cellspacing="1" style="background-color: #C5D9F1">
                                            <tr>
                                                <td align="center" bgcolor="#7ACCC8" class="style30">ปฏิทินสถานะการหมดอายุวัดน้ำ
                                                    <dx:ASPxTextBox runat="server" ID="txtDate" ClientInstanceName="txtDate" ClientVisible="false">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox runat="server" ID="txtMonth" ClientVisible="false">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox runat="server" ID="txtYear" ClientVisible="false">
                                                    </dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                                        <tr>
                                                            <td width="10%">
                                                                <dx:ASPxButton runat="server" ID="btnPrev" AutoPostBack="false" BackgroundImage-ImageUrl="images/icKPI01.png"
                                                                    Width="24" Height="24" EnableTheming="false" EnableDefaultAppearance="false"
                                                                    SkinID="dd" Cursor="pointer">
                                                                    <ClientSideEvents Click="function(){xcpnCalendar.PerformCallback('Prev');}" />
                                                                    <BackgroundImage ImageUrl="images/icKPI01.png"></BackgroundImage>
                                                                </dx:ASPxButton>
                                                            </td>
                                                            <td width="79%" align="center">
                                                                <dx:ASPxLabel runat="server" ID="lblCalendardate">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td width="11%">
                                                                <dx:ASPxButton runat="server" ID="btnNext" AutoPostBack="false" BackgroundImage-ImageUrl="images/icKPI02.png"
                                                                    Width="24" Height="24" EnableTheming="false" EnableDefaultAppearance="false"
                                                                    SkinID="dd" Cursor="pointer">
                                                                    <ClientSideEvents Click="function(){xcpnCalendar.PerformCallback('Next');}" />
                                                                    <BackgroundImage ImageUrl="images/icKPI02.png"></BackgroundImage>
                                                                </dx:ASPxButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Literal runat="server" ID="ltrCalendar"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <dx:ASPxGridView ID="gvwDataCar" ClientInstanceName="gvwDataCar" runat="server" AutoGenerateColumns="false"
                                                        Width="100%" OnAfterPerformCallback="gvwDataCar_AfterPerformCallback">
                                                        <Columns>
                                                            <dx:GridViewDataColumn Caption="">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxImage runat="server" ID="img4" ImageUrl='<%# Eval("IMG") %>'>
                                                                    </dx:ASPxImage>
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="รายละเอียด" FieldName="DETAIL" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            </dx:GridViewDataColumn>
                                                        </Columns>
                                                    </dx:ASPxGridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" valign="top"><img src="images/em.gif" width="16" height="16" alt="" />
                                                    รถที่หมดอายุวัดน้ำแล้ว ณ วันที่ปัจจุบัน</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" valign="top"><img src="images/green.png" width="16" height="16" alt="" />
                                                    งานที่ผู้ขนส่งดำเนินการแล้ว</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" valign="top"><img src="images/red.png" width="16" height="16" alt="" />
                                                    งานที่รอการจัดการคำขอจากผู้ขนส่งทั้งหมด </td>
                                            </tr>
                                        </table>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxCallbackPanel>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
