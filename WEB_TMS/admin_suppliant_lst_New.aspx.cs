﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Text;
using System.IO;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTabControl;
using System.Configuration;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using EmailHelper;
using TMS_BLL.Transaction.Appeal;
using TMS_BLL.Transaction.Accident;
using System.Web.Security;
using GemBox.Spreadsheet;
using System.Drawing;
using System.Web.UI.HtmlControls;

public partial class admin_suppliant_lst_New : PageBase
{
    #region Member
    DataSet ds;
    DataTable dt;
    DataRow dr;
    string FieldType = "SUPPLIANT", str = string.Empty;

    public enum eType
    {
        All = 0, //
        SurpriseCheck = 1, // ตรวจสภาพรถ
        Accident = 2, //อุบัติเหตุ
        Complain = 3, //เรื่องร้องเรียน
        ConfirmContracts = 4, //ยืนยันรถตามสัญญา
        PlanTransport = 5, //ยืนยันรถตามแผน
        ReportMonthly = 6 //รายงานประจำเดือน

    }
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            SetText();
            ViewState["DataField"] = GetField(FieldType);
            lblHeader.Text = SetField((DataTable)ViewState["DataField"], false);
            SetDropDownList();
            SetGridView(ref gvData, (DataTable)ViewState["DataField"]);
            lblSearchResult.Text = string.Format("ผลการค้นหา", "0");
            SetVisible();
            this.AssignAuthen();
        }
    }
    #endregion
    private void AssignAuthen()
    {
        try
        {
            if (!CanWrite)
            {
                gvData.Columns[1].Visible = false;
                //aAdd.Visible = false;
            }

            if (!CanRead)
                gvData.Columns[0].Visible = false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region SetDropDownList
    private void SetDropDownList()
    {
        //SetRadioButtonListIs_Active(ref rblCACTIVE, "IS_ACTIVE", false);
    }
    #endregion

    #region SetVisible
    private void SetVisible()
    {
        //if (!string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
        //{
        //    divType.Visible = false;
        //}
        //else if (!string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup2.ToString()))
        //{
        //    divType.Visible = false;
        //}
        //else 
        //{
        //    divType.Visible = true;
        //}
        if (Session["AppealDoc"] != null)
        {
            txtSEARCH.Enabled = false;
            txtStartDate.Enabled = false;
            txtEndDate.Enabled = false;
            ddlSTATUS.Enabled = false;
            btnSearch_Click(null, null);
        }
        //if (VendorID == -1)
        //{
        //    aAdd.Visible = false;
        //    ddlVENDOR_CODE_SEARCH.Enabled = true;
        //    divEmploy.Visible = true;
        //}
        //else
        //{
        //    ddlVENDOR_CODE_SEARCH.Enabled = false;
        //    ddlVENDOR_CODE_SEARCH.SelectedValue = VendorCode;
        //    aAdd.Visible = true;
        //    divEmploy.Visible = false;
        //}
        //if (hidType.Value == "REQUEST")
        //{
        //    divEmploy.Visible = false;
        //}
    }
    #endregion

    #region SetText
    private void SetText()
    {
        btnClear.Text = "เคลียร์";
        btnSearch.Text = "ค้นหา";
        //aAdd.InnerText = Resources.Add;
        //btnExport.Text = Resources.Export;


        txtStartDate.Text = DateTime.Now.Date.AddMonths(-1).ToString("dd/MM/yyyy");
        txtEndDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
    }
    #endregion

    #region btnSearch_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string StartDate = string.Empty, EndDate = string.Empty;
            if (!string.IsNullOrEmpty(txtStartDate.Text.Trim()))
            {
                StartDate = txtStartDate.Text.Trim() + " 00:00:00";
            }
            if (!string.IsNullOrEmpty(txtEndDate.Text.Trim()))
            {
                EndDate = txtEndDate.Text.Trim() + " 23:59:59";
            }

            if (!string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
            {
                string type = "";
                if (ddlType.SelectedValue == "1") 
                {
                    type = "011";
                    dt = AppealBLL.Instance.AppealSelect(txtSEARCH.Text.Trim(), StartDate, EndDate, hidDocID.Value, ddlSTATUS.SelectedValue, hideAppealID.Value, type);
                }
                else if (ddlType.SelectedValue == "2")
                {
                    type = "090";
                    dt = AppealBLL.Instance.AppealSelect(txtSEARCH.Text.Trim(), StartDate, EndDate, hidDocID.Value, ddlSTATUS.SelectedValue, hideAppealID.Value, type);
                }
                else if (ddlType.SelectedValue == "3")
                {
                    type = "080";
                    dt = AppealBLL.Instance.AppealSelect(txtSEARCH.Text.Trim(), StartDate, EndDate, hidDocID.Value, ddlSTATUS.SelectedValue, hideAppealID.Value, type);
                }
                else if (ddlType.SelectedValue == "4")
                {
                    type = "010";
                    dt = AppealBLL.Instance.AppealSelect(txtSEARCH.Text.Trim(), StartDate, EndDate, hidDocID.Value, ddlSTATUS.SelectedValue, hideAppealID.Value, type);
                }
                else if (ddlType.SelectedValue == "5")
                {
                    type = "020";
                    dt = AppealBLL.Instance.AppealSelect(txtSEARCH.Text.Trim(), StartDate, EndDate, hidDocID.Value, ddlSTATUS.SelectedValue, hideAppealID.Value, type);
                }
                else if (ddlType.SelectedValue == "6")
                {
                    type = "100";
                    dt = AppealBLL.Instance.AppealSelect(txtSEARCH.Text.Trim(), StartDate, EndDate, hidDocID.Value, ddlSTATUS.SelectedValue, hideAppealID.Value, type);
                }
                else if (ddlType.SelectedValue == "0")
                {
                    type = "";
                    dt = AppealBLL.Instance.AppealSelect(txtSEARCH.Text.Trim(), StartDate, EndDate, hidDocID.Value, ddlSTATUS.SelectedValue, hideAppealID.Value, type);
                }
            }
            else
            {
                if (ddlType.SelectedIndex <= 0)
                {
                    throw new Exception("กรุณาเลือกประเภท");
                }
                dt = AppealBLL.Instance.AppealVendorSelect(txtSEARCH.Text.Trim(), StartDate, EndDate, hidDocID.Value, ddlSTATUS.SelectedValue, hideAppealID.Value, ddlType.SelectedValue, Session["SVDID"] + string.Empty);
                //if (ddlType.SelectedValue == (int)eType.SurpriseCheck + string.Empty)
                //{

                //}
                //else if (ddlType.SelectedValue == (int)eType.Accident + string.Empty)
                //{

                //}
                //else if (ddlType.SelectedValue == (int)eType.Complain + string.Empty)
                //{
                //    dt = AppealBLL.Instance.AppealComplainSelectBLL(Session["SVDID"].ToString(), this.GetConditionSearchComplain());
                //}
                //else if (ddlType.SelectedValue == (int)eType.ConfirmContracts + string.Empty)
                //{

                //}
                //else if (ddlType.SelectedValue == (int)eType.PlanTransport + string.Empty)
                //{

                //}
                //else if (ddlType.SelectedValue == (int)eType.ReportMonthly + string.Empty)
                //{

                //}

            }

            gvData.DataSource = dt;
            gvData.DataBind();
            lblSearchResult.Text = string.Format("ผลการค้นหา {0} รายการ", dt.Rows.Count);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private string GetConditionSearchComplain()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (Session["AppealDoc"] != null)
                return sb.Append(" AND TAPPEAL.SREFERENCEID ='" + Session["AppealDoc"].ToString() + "'").ToString();
            else
                sb.Append(" AND TO_CHAR(TCOMPLAIN.COMPLAIN_DATE,'yyyyMMdd', 'NLS_DATE_LANGUAGE = ENGLISH') >= '" + DateTime.ParseExact(txtStartDate.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "' AND TO_CHAR(TCOMPLAIN.COMPLAIN_DATE,'yyyyMMdd', 'NLS_DATE_LANGUAGE = ENGLISH') <= '" + DateTime.ParseExact(txtEndDate.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "'");
            //DDELIVERY

            if (hideAppealID.Value != "")
                sb.Append(" AND TAPPEAL.SAPPEALID = '" + hideAppealID.Value + "'");
            else if (ddlSTATUS.SelectedValue != null)
            {
                if (string.Equals(ddlSTATUS.SelectedValue, "1"))
                    sb.Append(" AND (TAPPEAL.CSTATUS IN ('0','1','2'))");
                else if (string.Equals(ddlSTATUS.SelectedValue, "2"))
                    sb.Append(" AND TAPPEAL.CSTATUS = '3'");
                else if (string.Equals(ddlSTATUS.SelectedValue, "3"))
                    sb.Append(" AND TAPPEAL.SAPPEALID IS NULL");
            }

            if (!string.Equals(hidDocID.Value, string.Empty))
                sb.Append(" AND TCOMPLAIN.DOC_ID = '" + hidDocID.Value + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region btnClear_Click
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            txtSEARCH.Text = string.Empty;
            txtStartDate.Text = string.Empty;
            txtEndDate.Text = string.Empty;
            ddlSTATUS.ClearSelection();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    #endregion

    #region btnExport_Click
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            btnSearch_Click(null, null);
            ExportExcel(FieldType, string.Empty, dt, 1, 0);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion

    #region gvData_RowDataBound
    protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;

            CheckBox cbCACTIVE = (CheckBox)e.Row.FindControl("cbCACTIVE");
            HtmlAnchor aEdit = (HtmlAnchor)e.Row.FindControl("aEdit");
            HtmlAnchor aView = (HtmlAnchor)e.Row.FindControl("aView");
            if (cbCACTIVE != null)
            {
                if (drv.Row.Table.Columns.Contains("CACTIVE"))
                {
                    cbCACTIVE.Checked = drv["CACTIVE"] + string.Empty == (int)IS_ACTIVE_STATUS.Active + string.Empty;
                }
            }
            if (aEdit != null)
            {
                if (drv.Row.Table.Columns.Contains("SAPPEALID"))
                {
                    str = Mode.Edit + "&" + drv["SAPPEALID"];
                }
                if (!string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
                {
                    //mode  & SAPPEALID & STATUS
                    if (drv.Row.Table.Columns.Contains("CHKSTATUS"))
                    {
                        str += "&" + drv["CHKSTATUS"];
                    }
                    aEdit.HRef = "admin_suppliant_PTT_Add.aspx?str=" + EncodeQueryString(str);
                }
                else
                {
                    //mode & SAPPEALID & V_ID กรณณี Vendor
                    if (drv.Row.Table.Columns.Contains("V_ID"))
                    {
                        str += "&" + drv["V_ID"];
                    }
                    if (drv.Row.Table.Columns.Contains("ITYPE"))
                    {
                        str += "&" + drv["ITYPE"];
                    }
                    aEdit.HRef = "admin_suppliant_Add.aspx?str=" + EncodeQueryString(str);
                }

            }
            if (aView != null)
            {
                if (drv.Row.Table.Columns.Contains("SAPPEALID"))
                {
                    str = Mode.View + "&" + drv["SAPPEALID"];
                }
                if (!string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
                {
                    //mode  & SAPPEALID & STATUS
                    if (drv.Row.Table.Columns.Contains("CHKSTATUS"))
                    {
                        str += "&" + drv["CHKSTATUS"];
                    }

                    aView.HRef = "admin_suppliant_PTT_Add.aspx?str=" + EncodeQueryString(str);
                }
                else
                {
                    //Type & SAPPEALID & ID1

                    if (drv.Row.Table.Columns.Contains("V_ID"))
                    {
                        str += "&" + drv["V_ID"];
                    }
                    if (drv.Row.Table.Columns.Contains("ITYPE"))
                    {
                        str += "&" + drv["ITYPE"];
                    }
                    aView.HRef = "admin_suppliant_Add.aspx?str=" + EncodeQueryString(str);
                }
            }
        }
    }
    #endregion

    #region gvData_PageIndexChanging
    protected void gvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvData.PageIndex = e.NewPageIndex;
        btnSearch_Click(null, null);

    }
    #endregion
}
