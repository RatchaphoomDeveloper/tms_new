﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using System.Linq;
using System.Drawing;
using System.Data.OracleClient;
using System.Configuration;
using System.Web.Configuration;
using EmailHelper;

public partial class UserAddEdit : PageBase
{
    DataSet ds;
    string sqlCon = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    #region + View State +

    private DataTable dtSoldTo
    {
        get
        {
            if ((DataTable)ViewState["dtSoldTo"] != null)
                return (DataTable)ViewState["dtSoldTo"];
            else
                return null;
        }
        set
        {
            ViewState["dtSoldTo"] = value;
        }
    }
    private string ShipID
    {
        get
        {
            if ((string)ViewState["ShipID"] != null)
                return (string)ViewState["ShipID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["ShipID"] = value;
        }
    }

    private string SoldID
    {
        get
        {
            if ((string)ViewState["SoldID"] != null)
                return (string)ViewState["SoldID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["SoldID"] = value;
        }
    }

    private DataTable dtKPI
    {
        get
        {
            if ((DataTable)ViewState["dtKPI"] != null)
                return (DataTable)ViewState["dtKPI"];
            else
                return null;
        }
        set
        {
            ViewState["dtKPI"] = value;
        }
    }

    private DataTable dt
    {
        get
        {
            if ((DataTable)ViewState["dt"] != null)
                return (DataTable)ViewState["dt"];
            else
                return null;
        }
        set
        {
            ViewState["dt"] = value;
        }
    }
    private DataTable dtCGroup
    {
        get
        {
            if ((DataTable)ViewState["dtCGroup"] != null)
                return (DataTable)ViewState["dtCGroup"];
            else
                return null;
        }
        set
        {
            ViewState["dtCGroup"] = value;
        }
    }
    private DataTable dtCheck
    {
        get
        {
            if ((DataTable)ViewState["dtCheck"] != null)
                return (DataTable)ViewState["dtCheck"];
            else
                return null;
        }
        set
        {
            ViewState["dtCheck"] = value;
        }
    }


    private string OldPassWord
    {
        get
        {
            if ((string)ViewState["OldPassWord"] != null)
                return (string)ViewState["OldPassWord"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["OldPassWord"] = value;
        }
    }

    private string DefaultPassWord
    {
        get
        {
            if ((string)ViewState["DefaultPassWord"] != null)
                return (string)ViewState["DefaultPassWord"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["DefaultPassWord"] = value;
        }
    }

    private string PassWord
    {
        get
        {
            if ((string)ViewState["PassWord"] != null)
                return (string)ViewState["PassWord"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["PassWord"] = value;
        }
    }

    private string ActionType
    {
        get
        {
            if ((string)ViewState["ActionType"] != null)
                return (string)ViewState["ActionType"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["ActionType"] = value;
        }
    }

    private string ActionKey
    {
        get
        {
            if ((string)ViewState["ActionKey"] != null)
                return (string)ViewState["ActionKey"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["ActionKey"] = value;
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            this.LoadUserGroup();
            this.LoadStatus();
            this.LoadTeam();
            this.LoadSaleDist();
            this.LoadDepartment();
            this.CheckQueryString();
            this.GetData();
            this.Enabled();

            dgvSearch.DataSource = new DataTable();
            dgvSearch.DataBind();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void CheckQueryString()
    {
        try
        {
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                ActionType = Encoding.UTF8.GetString(MachineKey.Decode(Request.QueryString["type"], MachineKeyProtection.All));
                ActionKey = Encoding.UTF8.GetString(MachineKey.Decode(Request.QueryString["id"], MachineKeyProtection.All));
            }
            else
                Response.Redirect("../Other/NotAuthorize.aspx");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadDepartment()
    {
        try
        {
            DataTable dtDepartment = DepartmentBLL.Instance.DepartmentSelectBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlDepartment, dtDepartment, "DEPARTMENT_ID", "DEPARTMENT_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void LoadDivision(string Department)
    {
        try
        {
            DataTable dtDivision = DivisionBLL.Instance.DivisionSelectBLL(Department);
            DropDownListHelper.BindDropDownList(ref ddlDivision, dtDivision, "DIVISION_ID", "DIVISION_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void GetData()
    {
        if (string.Equals(ActionType, Mode.Add.ToString()))
        {
            cmdSave.Visible = true;
        }
        else if (string.Equals(ActionType, Mode.View.ToString()))
        {
            dt = UserBLL.Instance.UserSelectBLL(" AND SUID = '" + ActionKey + "' ");
            if (dt.Rows.Count > 0)
            {
                ddlUserGroup.SelectedValue = dt.Rows[0]["USERGROUP_ID"].ToString();
                hCGroup.Value = dt.Rows[0]["CGROUP"].ToString();
                txtFirstName.Text = dt.Rows[0]["SFIRSTNAME"].ToString();
                txtLastName.Text = dt.Rows[0]["SLASTNAME"].ToString();
                txtUsername.Text = dt.Rows[0]["SUSERNAME"].ToString();
                if (string.Equals(ddlUserGroup.SelectedValue, "0") || string.Equals(ddlUserGroup.SelectedValue, "6"))
                {
                    txtPassword.Attributes["value"] = dt.Rows[0]["SPASSWORD"].ToString();
                    hidOldPassword.Value = dt.Rows[0]["SPASSWORD"].ToString();
                    OldPassWord = dt.Rows[0]["SPASSWORD"].ToString();
                }
                else
                {
                    txtPassword.Attributes["value"] = string.Empty;
                    hidOldPassword.Value = string.Empty;
                    OldPassWord = string.Empty;
                }
                txtPosition.Text = dt.Rows[0]["SPOSITION"].ToString();
                txtEmail.Text = dt.Rows[0]["SEMAIL"].ToString();
                txtTelephone.Text = dt.Rows[0]["STEL"].ToString();
                ddlUserGroup_SelectedIndexChanged(null, null);
                ddlStatus.SelectedValue = dt.Rows[0]["CACTIVE"].ToString();
                //if (string.Equals(hCGroup.Value, "0") || string.Equals(hCGroup.Value, "6"))
                //{
                    ddlVendorID.SelectedValue = dt.Rows[0]["SVENDORID"].ToString();
                //}
                //else
                //{
                //    ddlVendorID.SelectedValue = string.Empty;
                //}
                ddlDepartment.SelectedValue = dt.Rows[0]["DEPARTMENT_ID"].ToString();
                ddlDepartment_SelectedIndexChanged(null, null);
                ddlDivision.SelectedValue = dt.Rows[0]["DIVISION_ID"].ToString();
                ddlTeam.SelectedValue = dt.Rows[0]["IS_CONTRACT"].ToString();
                ddlSaleDist.SelectedValue = dt.Rows[0]["SALE_DISTRICT"].ToString();
            }
            //Util.EnableControls(divDetail);
        }
        else
        {
            dt = UserBLL.Instance.UserSelectBLL(" AND SUID = '" + ActionKey + "' ");
            if (dt.Rows.Count > 0)
            {
                ddlUserGroup.SelectedValue = dt.Rows[0]["USERGROUP_ID"].ToString();
                hCGroup.Value = dt.Rows[0]["CGROUP"].ToString();
                txtFirstName.Text = dt.Rows[0]["SFIRSTNAME"].ToString();
                txtLastName.Text = dt.Rows[0]["SLASTNAME"].ToString();
                txtUsername.Text = dt.Rows[0]["SUSERNAME"].ToString();
                if (string.Equals(hCGroup.Value, "0") || string.Equals(hCGroup.Value, "6"))
                {
                    txtPassword.Attributes["value"] = dt.Rows[0]["SPASSWORD"].ToString();
                    hidOldPassword.Value = dt.Rows[0]["SPASSWORD"].ToString();
                    OldPassWord = dt.Rows[0]["SPASSWORD"].ToString();
                }
                else
                {
                    txtPassword.Attributes["value"] = string.Empty;
                    hidOldPassword.Value = string.Empty;
                    OldPassWord = string.Empty;
                }
                txtPosition.Text = dt.Rows[0]["SPOSITION"].ToString();
                txtEmail.Text = dt.Rows[0]["SEMAIL"].ToString();
                txtTelephone.Text = dt.Rows[0]["STEL"].ToString();

                //----
                ddlUserGroup_SelectedIndexChanged(null, null);
                //----
                ddlStatus.SelectedValue = dt.Rows[0]["CACTIVE"].ToString();
                if (string.Equals(hCGroup.Value, "0") || string.Equals(hCGroup.Value, "6"))
                {
                    ddlVendorID.SelectedValue = dt.Rows[0]["SVENDORID"].ToString();
                }
                else
                {
                    ddlVendorID.SelectedValue = string.Empty;
                }
                ddlDepartment.SelectedValue = dt.Rows[0]["DEPARTMENT_ID"].ToString();
                ddlDepartment_SelectedIndexChanged(null, null);
                ddlDivision.SelectedValue = dt.Rows[0]["DIVISION_ID"].ToString();
                ddlTeam.SelectedValue = dt.Rows[0]["IS_CONTRACT"].ToString();
            }
        }
    }

    private void Enabled()
    {
        if (string.Equals(ActionType, Mode.View.ToString()))
        {
            txtFirstName.Enabled = false;
            txtLastName.Enabled = false;
            txtUsername.Enabled = false;
            txtPassword.Enabled = false;
            txtPosition.Enabled = false;
            txtEmail.Enabled = false;
            txtTelephone.Enabled = false;
            ddlUserGroup.Enabled = false;
            ddlStatus.Enabled = false;
            ddlVendorID.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlDivision.Enabled = false;
            ddlTeam.Enabled = false;
            ddlSaleDist.Enabled = false;
            cmdSave.Visible = false;
        }
        else if (string.Equals(ActionType, Mode.Edit.ToString()))
        {
            txtUsername.Enabled = false;
            if (string.Equals(hCGroup.Value, "0") || string.Equals(hCGroup.Value, "6"))
            {
                linkForget.Visible = true;
            }
            else
            {
                linkForget.Visible = false;
            }
            //txtPassword.Enabled = false;
            cmdSave.Visible = true;
        }
    }

    private void LoadUserGroup()
    {
        try
        {
            DataTable dtUserGroup = UserGroupBLL.Instance.UserGroupSelectBLL(string.Empty);
            //DropDownListHelper.BindDropDownList(ref ddlUserGroup, dtUserGroup, "IS_ADMIN", "USERGROUP_NAME", true);
            DropDownListHelper.BindDropDownList(ref ddlUserGroup, dtUserGroup, "USERGROUP_ID", "USERGROUP_NAME", true);

            ddlVendorID.Visible = true;
            txtShipTo.Visible = false;
            aShipTo.Visible = false;
            aShipToClear.Visible = false;

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadForgetGroup()
    {
        try
        {
            DataTable dtForgetGroup = UserGroupBLL.Instance.UserGroupSelectBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlForgetGroup, dtForgetGroup, "IS_ADMIN", "USERGROUP_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadTeam()
    {
        try
        {
            DataTable dtTeam = UserBLL.Instance.StatusSelectBLL(" AND IS_ACTIVE = 1 AND STATUS_TYPE = 'TEAM_STATUS'");
            DropDownListHelper.BindDropDownList(ref ddlTeam, dtTeam, "STATUS_VALUE", "STATUS_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void LoadSaleDist()
    {
        try
        {
            DataTable dtSaleDist = UserBLL.Instance.SaleDistSelectBLL("");
            DropDownListHelper.BindDropDownList(ref ddlSaleDist, dtSaleDist, "SALES_DISTRICT", "DISTRICT_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadStatus()
    {
        try
        {
            DataTable dtStatus = UserBLL.Instance.StatusSelectBLL(" AND IS_ACTIVE = 1 AND STATUS_TYPE = 'USER_STATUS'");
            DropDownListHelper.BindDropDownList(ref ddlStatus, dtStatus, "STATUS_VALUE", "STATUS_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void ddlUserGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlUserGroup.SelectedIndex > 0)
        {
            dtCGroup = UserBLL.Instance.getCGroupFromUserGroupID(" AND USERGROUP_ID = " + ddlUserGroup.SelectedValue.ToString());
            string CGroup = "";
            if (dtCGroup.Rows.Count > 0)
            {
                CGroup = dtCGroup.Rows[0]["IS_ADMIN"].ToString();
                hCGroup.Value = CGroup;
            }
            if (CGroup != "")
            {
                string Condition = string.Empty;
                switch (CGroup)
                {
                    case "0":
                        Condition = "SELECT SVENDORID AS VENDOR_CODE, SVENDORNAME AS VENDOR_ABBR FROM (SELECT v.SVENDORID, VS.SVENDORNAME, ROW_NUMBER()OVER(ORDER BY VS.SVENDORNAME ) AS RN FROM  TVENDOR v INNER JOIN TVENDOR_SAP vs ON V.SVENDORID = VS.SVENDORID WHERE v.CACTIVE = '1')";
                        //ผู้ขนส่ง
                        break;

                    case "1":
                        Condition = "SELECT UNITCODE AS VENDOR_CODE, UNITNAME AS VENDOR_ABBR FROM (SELECT UNITCODE, UNITNAME, ROW_NUMBER()OVER(ORDER BY UNITNAME) AS RN FROM TUNIT )";
                        //พนักงาน ปตท
                        break;

                    case "2":
                        //Condition = "SELECT STERMINALID AS VENDOR_CODE,SABBREVIATION AS VENDOR_ABBR FROM (SELECT T.STERMINALID, TS.STERMINALNAME, T.SABBREVIATION, T.CSTATUS, T.CACTIVE, ROW_NUMBER()OVER(ORDER BY T.STERMINALID) AS RN FROM TTERMINAL T INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID WHERE (T.STERMINALID LIKE '5%' OR T.STERMINALID LIKE '8%')) WHERE CACTIVE = '1' AND CSTATUS = '1' AND sabbreviation IS NOT NULL";
                        Condition = "SELECT STERMINALID AS VENDOR_CODE,SABBREVIATION AS VENDOR_ABBR FROM (SELECT T.STERMINALID, TS.STERMINALNAME, T.SABBREVIATION, T.CSTATUS, T.CACTIVE, ROW_NUMBER()OVER(ORDER BY T.STERMINALID) AS RN FROM TTERMINAL T INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID WHERE (T.STERMINALID LIKE 'H%' OR T.STERMINALID LIKE 'K%')) WHERE CACTIVE = '1' AND CSTATUS = '1' AND sabbreviation IS NOT NULL";
                        //พนักงานคลัง
                        break;

                    case "3":
                        Condition = "SELECT UNITCODE AS VENDOR_CODE, UNITNAME AS VENDOR_ABBR FROM (SELECT UNITCODE, UNITNAME, ROW_NUMBER()OVER(ORDER BY UNITNAME) AS RN FROM TUNIT )";
                        //พนักงาน ปตท
                        break;

                    case "4":
                        Condition = "SELECT UNITCODE AS VENDOR_CODE, UNITNAME AS VENDOR_ABBR FROM (SELECT UNITCODE, UNITNAME, ROW_NUMBER()OVER(ORDER BY UNITNAME) AS RN FROM TUNIT )";
                        //พนักงาน ปตท
                        break;

                    case "5":
                        Condition = "SELECT UNITCODE AS VENDOR_CODE, UNITNAME AS VENDOR_ABBR FROM (SELECT UNITCODE, UNITNAME, ROW_NUMBER()OVER(ORDER BY UNITNAME) AS RN FROM TUNIT )";
                        //พนักงาน ปตท
                        break;

                    case "6":
                        Condition = "SELECT DISTINCT SOLD_ID AS VENDOR_CODE,SOLD_ID||'-'||SOLD_NAME AS VENDOR_ABBR FROM M_SOLD_TO";
                        //ลูกค้า ปตท
                        break;

                }

                if (!string.Equals(CGroup, "6"))
                {
                    DataTable dtVendorID = UserGroupBLL.Instance.DepartmentSelectBLL(Condition);
                    DropDownListHelper.BindDropDownList(ref ddlVendorID, dtVendorID, "VENDOR_CODE", "VENDOR_ABBR", true);
                    ddlVendorID.Visible = true;
                    txtShipTo.Visible = false;
                    aShipTo.Visible = false;
                    aShipToClear.Visible = false;
                }
                else
                {
                    ddlVendorID.Visible = false;
                    txtShipTo.Visible = true;
                    aShipTo.Visible = true;
                    aShipToClear.Visible = true;
                }
                ddlVendorID.Enabled = true;
                //if (string.Equals(CGroup, "1") || string.Equals(CGroup, "2") || string.Equals(CGroup, "3") || string.Equals(CGroup, "4") || string.Equals(CGroup, "5"))
                if (!string.Equals(CGroup, "0") && !string.Equals(CGroup, "6"))
                {
                    txtPassword.Enabled = false;
                    ddlDepartment.Enabled = true;
                    ddlDivision.Enabled = false;
                    //ddlVendorID.Enabled = false;
                    if (string.Equals(CGroup, "3"))
                    {
                        ddlTeam.Enabled = false;
                        ddlTeam.ClearSelection();
                    }
                    else
                    {
                        ddlTeam.Enabled = true;
                        ddlTeam.ClearSelection();
                    }
                    ddlVendorID.ClearSelection();
                    ddlDepartment.ClearSelection();
                    ddlDivision.ClearSelection();
                }
                else
                {
                    if (string.Equals(CGroup, "0") || string.Equals(CGroup, "6"))
                    {
                        txtPassword.Enabled = true;
                        ddlTeam.Enabled = false;
                        ddlTeam.ClearSelection();
                    }
                    else
                    {
                        txtPassword.Enabled = false;
                        ddlTeam.Enabled = true;
                        ddlTeam.ClearSelection();
                        ddlVendorID.Enabled = true;
                    }
                    ddlDepartment.Enabled = false;
                    ddlDivision.Enabled = false;
                    ddlVendorID.Enabled = true;
                    ddlVendorID.ClearSelection();
                    ddlDepartment.ClearSelection();
                    ddlDivision.ClearSelection();
                }
            }
            else
            {
                ddlVendorID.Enabled = false;
                ddlDepartment.Enabled = false;
                ddlDivision.Enabled = false;
                ddlVendorID.ClearSelection();
                ddlDepartment.ClearSelection();
                ddlDivision.ClearSelection();
            }

            if (string.Equals(ddlUserGroup.SelectedValue.ToString(), "101"))
            {
                ddlSaleDist.Enabled = true;
            }
            else
            {
                ddlSaleDist.Enabled = false;
            }
        }

    }

    protected void aShipTo_ServerClick(object sender, EventArgs e)
    {
        this.GetSoldTo();
    }

    private void GetSoldTo()
    {
        try
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowOTP", "$('#ShowOTP').modal();", true);
            updOTP.Update();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void aShipToClear_ServerClick(object sender, EventArgs e)
    {
        txtShipTo.Text = string.Empty;
        ShipID = String.Empty;
        SoldID = String.Empty;
    }

    protected void dgvSearch_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        try
        {
            ImageButton imgButton = e.CommandSource as ImageButton;
            if (imgButton != null)
            {
                int Index = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;

                if (string.Equals(e.CommandName, "ChooseData"))
                {
                    SoldID = dgvSearch.DataKeys[Index]["SOLD_ID"].ToString();
                    ShipID = dgvSearch.DataKeys[Index]["SHIP_ID"].ToString();

                    //txtSoldTo.Text = dgvSearch.DataKeys[Index]["SOLD_TO"].ToString();
                    txtShipTo.Text = dgvSearch.DataKeys[Index]["SHIP_NAME"].ToString();
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "<script type='text/javascript'>$('#ShowOTP').modal('hide');</script>", false);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "ShowOTP().Hide();", true);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.Equals(txtInput.Value.Trim(), string.Empty))
                throw new Exception(string.Format("คำค้นหา"));

            if (txtInput.Value.Trim().Length < 5)
                throw new Exception("คำค้นหา ต้องมีความยาวอย่างน้อย 5 ตัวอักษร");

            dtSoldTo = UserBLL.Instance.SoldToSelectBLL("%" + txtInput.Value.Trim() + "%");
            GridViewHelper.BindGridView(ref dgvSearch, dtSoldTo, false);
            updOTP.Update();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void mpConfirmSave_ClickOK(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSave();
            DataTable dtResult = UserBLL.Instance.UserInsertBLL(int.Parse(ActionKey), int.Parse(hCGroup.Value), int.Parse(hCGroup.Value) == 6 ? ShipID : ddlVendorID.SelectedValue, txtFirstName.Text, txtLastName.Text, txtPosition.Text, txtTelephone.Text, txtUsername.Text, PassWord, OldPassWord, txtEmail.Text, int.Parse(ddlStatus.SelectedValue), int.Parse(ddlTeam.SelectedValue.Trim() == "" ? "0" : ddlTeam.SelectedValue), int.Parse(Session["UserID"].ToString()), int.Parse(ddlDepartment.SelectedValue.Trim() == "" ? "0" : ddlDepartment.SelectedValue), int.Parse(ddlDivision.SelectedValue.Trim() == "" ? "0" : ddlDivision.SelectedValue), int.Parse(ddlUserGroup.SelectedValue.Trim() == "" ? "0" : ddlUserGroup.SelectedValue), ddlSaleDist.SelectedValue.Trim());
            if (dtResult.Rows.Count > 0)
                alertSuccess("บันทึกผลสำเร็จ", "User.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateSave()
    {
        try
        {

            if (ddlUserGroup.SelectedIndex < 1)
                throw new Exception("กรุณาเลือกกลุ่มผู้ใช้งาน");

            if (!string.Equals(hCGroup.Value, "6"))
            {
                if (ddlVendorID.SelectedIndex < 1)
                    throw new Exception("กรุณาเลือกหน่วยงาน");
            }
            else
            {
                if (string.Equals(txtShipTo.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาเลือก ShipTo/SoldTo");
            }

            if (!string.Equals(hCGroup.Value, "0") && !string.Equals(hCGroup.Value, "6"))
            {
                if (ddlDepartment.SelectedIndex < 1)
                    throw new Exception("กรุณาเลือก Department");

                if (ddlDivision.SelectedIndex < 1)
                    throw new Exception("กรุณาเลือก Division");
            }

            if (ddlStatus.SelectedIndex < 1)
                throw new Exception("กรุณาเลือกสถานะผู้ใช้งาน");

            if (!string.Equals(hCGroup.Value, "0") && !string.Equals(hCGroup.Value, "3") && !string.Equals(hCGroup.Value, "6"))
            {
                if (ddlTeam.SelectedIndex < 1)
                    throw new Exception("กรุณาเลือกหน่วยงาน(ทีม)");
            }

            if (string.Equals(txtFirstName.Text.Trim(), string.Empty))
                throw new Exception("กรุณากรอกชื่อ");

            if (string.Equals(txtLastName.Text.Trim(), string.Empty))
                throw new Exception("กรุณากรอกนามสกุล");

            if (string.Equals(txtTelephone.Text.Trim(), string.Empty))
                throw new Exception("กรุณากรอกหมายเลขโทรศัพท์");

            if (string.Equals(txtEmail.Text.Trim(), string.Empty))
                throw new Exception("กรุณากรอกอีเมล์");

            if (string.Equals(txtUsername.Text.Trim(), string.Empty))
                throw new Exception("กรุณากรอก Username");

            if (string.Equals(hCGroup.Value, "0") || string.Equals(hCGroup.Value, "6"))
            {
                if (string.Equals(txtPassword.Text.Trim(), string.Empty))
                    throw new Exception("กรุณากรอก Password");
            }

            if (string.Equals(txtPosition.Text.Trim(), string.Empty))
                throw new Exception("กรุณากรอกตำแหน่ง");

            if (string.Equals(ActionType, Mode.Add.ToString()))
            {
                dtCheck = UserBLL.Instance.UserSelectBLL(" AND SUSERNAME = '" + txtUsername.Text.Trim() + "' ");
                if (dtCheck.Rows.Count > 0)
                {
                    throw new Exception("มีผู้ใช้ Username นี้ในระบบแล้ว");
                }
                PassWord = STCrypt.encryptMD5(txtPassword.Text);
            }
            else
            {
                if (!string.Equals(hCGroup.Value, "0") && !string.Equals(hCGroup.Value, "6"))
                {
                    PassWord = string.Empty;
                    OldPassWord = string.Empty;
                }

                if (hidOldPassword.Value == txtPassword.Text)
                {
                    PassWord = hidOldPassword.Value;
                    OldPassWord = string.Empty;
                }
                else
                {
                    PassWord = STCrypt.encryptMD5(txtPassword.Text);
                    OldPassWord = hidOldPassword.Value;
                }
            }
            if (string.Equals(ddlUserGroup.SelectedValue.Trim(), "101"))
                if (string.Equals(ddlSaleDist.SelectedValue.Trim(), ""))
                    throw new Exception("กรุณาเลือกพื้นที่รับผิดชอบ");


        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void mpConfirmCancel_ClickOK(object sender, EventArgs e)
    {
        Response.Redirect("User.aspx");
    }
    protected void txtPassword_TextChanged(object sender, EventArgs e)
    {
        txtPassword.Attributes["value"] = txtPassword.Text;
    }
    protected void cmdReset_Click(object sender, EventArgs e)
    {
        SentEmail(txtForgetUser.Text, ddlForgetDepartment.SelectedValue, txtForgetEmail.Text);
    }
    protected void linkForget_Click(object sender, EventArgs e)
    {
        try
        {
            this.LoadForgetGroup();
            ddlForgetGroup.Text = hCGroup.Value;
            this.ddlForgetGroup_SelectedIndexChanged(null, null);
            ddlForgetDepartment.Text = ddlVendorID.SelectedValue;
            txtForgetUser.Text = txtUsername.Text;
            txtForgetEmail.Text = txtEmail.Text;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "<script type='text/javascript'>$('#ShowEvent').modal();</script>", false);
            updShowEvent.Update();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void ddlForgetGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        string Condition = string.Empty;
        switch (hCGroup.Value)
        {
            case "0":
                Condition = "SELECT SVENDORID AS VENDOR_CODE, SVENDORNAME AS VENDOR_ABBR FROM (SELECT v.SVENDORID, VS.SVENDORNAME, ROW_NUMBER()OVER(ORDER BY VS.SVENDORNAME ) AS RN FROM  TVENDOR v INNER JOIN TVENDOR_SAP vs ON V.SVENDORID = VS.SVENDORID WHERE v.CACTIVE = '1')";
                //ผู้ขนส่ง
                break;

            case "1":
                Condition = "SELECT UNITCODE AS VENDOR_CODE, UNITNAME AS VENDOR_ABBR FROM (SELECT UNITCODE, UNITNAME, ROW_NUMBER()OVER(ORDER BY UNITNAME) AS RN FROM TUNIT )";
                //พนักงาน ปตท
                break;

            case "2":
                //Condition = "SELECT STERMINALID AS VENDOR_CODE,SABBREVIATION AS VENDOR_ABBR FROM (SELECT T.STERMINALID, TS.STERMINALNAME, T.SABBREVIATION, T.CSTATUS, T.CACTIVE, ROW_NUMBER()OVER(ORDER BY T.STERMINALID) AS RN FROM TTERMINAL T INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID WHERE (T.STERMINALID LIKE '5%' OR T.STERMINALID LIKE '8%')) WHERE CACTIVE = '1' AND CSTATUS = '1' AND sabbreviation IS NOT NULL";
                Condition = "SELECT STERMINALID AS VENDOR_CODE,SABBREVIATION AS VENDOR_ABBR FROM (SELECT T.STERMINALID, TS.STERMINALNAME, T.SABBREVIATION, T.CSTATUS, T.CACTIVE, ROW_NUMBER()OVER(ORDER BY T.STERMINALID) AS RN FROM TTERMINAL T INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID WHERE (T.STERMINALID LIKE 'H%' OR T.STERMINALID LIKE 'K%')) WHERE CACTIVE = '1' AND CSTATUS = '1' AND sabbreviation IS NOT NULL";
                //พนักงานคลัง
                break;

            case "3":
                Condition = "SELECT UNITCODE AS VENDOR_CODE, UNITNAME AS VENDOR_ABBR FROM (SELECT UNITCODE, UNITNAME, ROW_NUMBER()OVER(ORDER BY UNITNAME) AS RN FROM TUNIT )";
                //พนักงาน ปตท
                break;

            case "4":
                Condition = "SELECT UNITCODE AS VENDOR_CODE, UNITNAME AS VENDOR_ABBR FROM (SELECT UNITCODE, UNITNAME, ROW_NUMBER()OVER(ORDER BY UNITNAME) AS RN FROM TUNIT )";
                //พนักงาน ปตท
                break;

            case "5":
                Condition = "SELECT UNITCODE AS VENDOR_CODE, UNITNAME AS VENDOR_ABBR FROM (SELECT UNITCODE, UNITNAME, ROW_NUMBER()OVER(ORDER BY UNITNAME) AS RN FROM TUNIT )";
                //พนักงาน ปตท
                break;

            case "6":
                Condition = "SELECT DISTINCT SOLD_ID AS VENDOR_CODE,SOLD_ID||'-'||SOLD_NAME AS VENDOR_ABBR FROM M_SOLD_TO";
                //ลูกค้า ปตท
                break;
        }
        DataTable dtForgetDepartment = UserGroupBLL.Instance.DepartmentSelectBLL(Condition);
        DropDownListHelper.BindDropDownList(ref ddlForgetDepartment, dtForgetDepartment, "VENDOR_CODE", "VENDOR_ABBR", true);
        ddlForgetDepartment.Enabled = true;
    }

    private void SentEmail(string Username, string Department, string Email)
    {
        //function sendmail username and new password 
        DataTable dt1 = new DataTable();
        DataTable dt2 = new DataTable();
        DataTable dt3 = new DataTable();
        dt1 = CommonFunction.Get_Data(sqlCon, "SELECT SEMAIL FROM TUSER  WHERE SVENDORID = '" + Department + "'");
        if (dt1.Rows.Count > 0)
        {
            dt2 = CommonFunction.Get_Data(sqlCon, "SELECT SEMAIL FROM TUSER  WHERE SVENDORID = '" + Department + "' AND SUSERNAME = '" + Username + "'");
            if (dt2.Rows.Count > 0)
            {
                dt3 = CommonFunction.Get_Data(sqlCon, "SELECT SEMAIL,SFIRSTNAME||' '||SLASTNAME AS FULL_NAME FROM TUSER  WHERE SVENDORID = '" + Department + "' AND SUSERNAME = '" + Username + "' AND SEMAIL = '" + Email + "'");
                if (dt3.Rows.Count > 0)
                {

                    using (OracleConnection con = new OracleConnection(sqlCon))
                    {
                        con.Open();
                        DataTable dtPW = CommonFunction.Get_Data(sqlCon, "select FN_GENERATE_PASSWORD as PASSWORD from dual");
                        DefaultPassWord = dtPW.Rows[0]["PASSWORD"].ToString().Trim();
                        string sql = "UPDATE TUSER SET SPASSWORD = '" + STCrypt.encryptMD5(DefaultPassWord) + "' WHERE SUSERNAME = '" + Username + "'";
                        using (OracleCommand com = new OracleCommand(sql, con))
                        {
                            com.ExecuteNonQuery();
                        }
                    }

                    string str = @"<table>
                                <tr><td colspan='2'>เรียน {0}</td></tr>
                                <tr><td colspan='2'>Username และ Password ของท่านคือ</td></tr>
                                <tr><td>Username :</td><td>{1}</td></tr>
                                <tr><td>Password :</td><td>{2}</td></tr>
                                </table>";
                    string _from = ConfigurationManager.AppSettings["SystemMail"].ToString();
                    string _to = dt3.Rows[0][0] + "";

                    if (ConfigurationManager.AppSettings["usemail"].ToString() == "0")
                    {
                        _from = ConfigurationManager.AppSettings["demoMailSend"].ToString();
                        _to = ConfigurationManager.AppSettings["demoMailRecv"].ToString();
                    }

                    //CommonFunction.SendMail(_from, _to, "แจ้ง Username และ Password", string.Format(str, dt.Rows[0][0] + "", _Username, _DefaultPassword), "");
                    MailService.SendMail(_to, "แจ้ง Username และ Password ระบบ TMS", string.Format(str, dt3.Rows[0][1] + "", Username, DefaultPassWord));

                    alertSuccess(Resources.CommonResource.Msg_Alert_Title_Complete + "ระบบได้ส่ง Username และ Password ไปยังที่อยู่ Email ของท่านแล้ว กรุณาตรวจสอบตามที่อยู่ Email ของท่าน");
                }
                else
                {
                    //dt3
                    alertSuccess(Resources.CommonResource.Msg_Alert_Title_Error + "Email ของท่านไม่ตรงกับขอมูลที่มีในระบบ");
                }
            }
            else
            {
                //dt2
                alertSuccess(Resources.CommonResource.Msg_Alert_Title_Error + "Username ของท่านไม่ตรงกับขอมูลที่มีในระบบ");
            }
        }
        else
        {
            //dt1
            alertSuccess(Resources.CommonResource.Msg_Alert_Title_Error + "หน่วยงาน ของท่านไม่ตรงกับขอมูลที่มีในระบบ");
        }
    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlDepartment.SelectedIndex > 0)
            {
                this.LoadDivision(" AND DEPARTMENT_ID = " + ddlDepartment.SelectedValue.Trim());
                ddlDivision.Enabled = true;
            }
            else
            {
                DropDownListHelper.BindDropDownList(ref ddlDivision, null, "DIVISION_ID", "DIVISION_NAME", true);
                ddlDivision.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
}