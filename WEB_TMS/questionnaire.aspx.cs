﻿using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using TMS_Entity;
using OfficeOpenXml;
using System.Data.OracleClient;
using System.Web.Configuration;

public partial class questionnaire : PageBase
{
    string sqlConn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    #region " Private Calss "
    private class SUMTScore
    {
        private string ColIndex { get; set; }
        private double SumScore { get; set; }

        SUMTScore()
        {
            SumScore = 0.00;
        }
    }
    #endregion " Private Class "
    string GroupPermission = "01";
    #region " Prop "
    protected DataTable SearhData
    {
        get { return (DataTable)ViewState[this.ToString() + "SearhData"]; }
        set { ViewState[this.ToString() + "SearhData"] = value; }
    }

    protected UserProfile user_profile
    {
        get { return (UserProfile)ViewState[this.ToString() + "user_profile"]; }
        set { ViewState[this.ToString() + "user_profile"] = value; }
    }

    protected TQUESTIONNAIRE SearchCriteria
    {
        get { return (TQUESTIONNAIRE)ViewState[this.ToString() + "SearchCriteria"]; }
        set { ViewState[this.ToString() + "SearchCriteria"] = value; }
    }

    protected DataTable VendorDDL
    {
        get { return (DataTable)ViewState[this.ToString() + "VendorDDL"]; }
        set { ViewState[this.ToString() + "VendorDDL"] = value; }
    }

    protected decimal PercentYear
    {
        get { return (decimal)ViewState[this.ToString() + "PercentYear"]; }
        set { ViewState[this.ToString() + "PercentYear"] = value; }
    }

    #endregion " Prop "

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            user_profile = SessionUtility.GetUserProfileSession();
            CheckPermission();
            InitForm();
            LoadMain();
            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
                btnExport.Enabled = false;
                btnCompare.Enabled = false;
            }
            if (!CanWrite)
            {
                btnFormList.Enabled = false;
                btnFormConfig.Enabled = false;
                btnAdd.Enabled = false;
                btnEdit.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    public void InitForm()
    {

        PercentYear = 0;
        int StartYear = 2015;
        int cYear = DateTime.Today.Year;
        int endYear = cYear + 4;
        int index = 0;
        for (int i = StartYear; i <= endYear; i++)
        {
            ddlYearSearch.Items.Insert(index, new ListItem((i).ToString(), (i).ToString()));
        }


        ddlYearSearch.ClearSelection();
        // เลือกปีปัจจุบัน
        ListItem sel = ddlYearSearch.Items.FindByValue(cYear.ToString());
        if (sel != null) sel.Selected = true;
        else ddlYearSearch.SelectedIndex = 0;
        LoadVendor();
    }

    public void LoadVendor()
    {
        try
        {
            VendorDDL = ComplainBLL.Instance.VendorSelectBLL();
            DataRow dr = VendorDDL.NewRow();
            dr["SABBREVIATION"] = "";
            VendorDDL.Rows.InsertAt(dr, 0);
            ddlVendorSearch.DataSource = VendorDDL;
            ddlVendorSearch.DataBind();
            if (user_profile.CGROUP != GROUP_PERMISSION.UNIT)
            {
                ddlVendorSearch.ClearSelection();
                ListItem lst = ddlVendorSearch.Items.FindByValue(user_profile.SVDID);
                if (lst != null) lst.Selected = true;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }
    public void CheckPermission()
    {
        if (Session["UserID"] == null)
        {
            GotoDefault();
            return;
        }

        //if (!GroupPermission.Contains(user_profile.CGROUP))
        //{
        //    GotoDefault();
        //    return;
        //}

        if (user_profile.CGROUP != GROUP_PERMISSION.UNIT)
        {
            btnFormList.Visible = false;
            btnFormConfig.Visible = false;
            btnAdd.Visible = false;

            ddlVendorSearch.Enabled = false;
        }

    }
    public void RegisterPostClick()
    {
        foreach (GridViewRow row in grvMain.Rows)
        {
            if (row.RowType != DataControlRowType.DataRow) continue;

            ImageButton ImageEdit = row.Cells[grvMain.Columns.Count - 1].FindControl("ImageEdit") as ImageButton;
            ImageButton imgView = row.Cells[grvMain.Columns.Count - 2].FindControl("imgView") as ImageButton;
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (ImageEdit != null) scriptManager.RegisterPostBackControl(ImageEdit);
            if (imgView != null) scriptManager.RegisterPostBackControl(imgView);
        }
    }

    public void GotoDefault()
    {
        Response.Redirect("default.aspx");
    }
    protected void grvMain_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "RowSelect")
        {
            int? rowIndex = grvMain.Columns.Count - 1;
            int _keyNQUESTIONNAIREID = int.Parse(grvMain.DataKeys[rowIndex.Value]["NQUESTIONNAIREID"].ToString());
            if (_keyNQUESTIONNAIREID != null)
            {
                Session["EditNQUESTIONNAIREID"] = _keyNQUESTIONNAIREID;
                Response.Redirect("questionnaire_form.aspx");
            }
        }
        else if (e.CommandName == "View")
        {
            int? rowIndex = grvMain.Columns.Count - 2;
            int _keyNQUESTIONNAIREID = int.Parse(grvMain.DataKeys[rowIndex.Value]["NQUESTIONNAIREID"].ToString());
            if (_keyNQUESTIONNAIREID != null)
            {
                Session["ViewNQUESTIONNAIREID"] = _keyNQUESTIONNAIREID;
                Response.Redirect("questionnaire_form.aspx");
            }
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        LoadMain();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        InitForm();
        LoadMain();
    }
    public void LoadMain()
    {
        try
        {
            string _err = string.Empty;
            SearchCriteria = new TQUESTIONNAIRE() { NYEAR = string.Empty, SVENDORID = string.Empty };
            if (ddlYearSearch.SelectedItem != null) SearchCriteria.NYEAR = ddlYearSearch.SelectedItem.Value;
            if (ddlVendorSearch.SelectedItem != null) SearchCriteria.SVENDORID = ddlVendorSearch.SelectedItem.Value;
            bool OnlyComplete = user_profile.CGROUP != GROUP_PERMISSION.UNIT ? true : false;
            SearhData = new QuestionnaireBLL().GetListScoreByGroup(ref _err, SearchCriteria, OnlyComplete);
            try { if (SearhData != null) SearhData.Columns.Remove("ROWNUM"); }
            catch { }

            // Clear Column 
            if (grvMain.Columns.Count > 7)
            {
                var DelCol = new List<DataControlField>();

                foreach (DataControlField dtCol in grvMain.Columns)
                {
                    if (dtCol.HeaderStyle.CssClass != "StaticCol")
                    {
                        DelCol.Add(dtCol);
                    }
                }

                foreach (DataControlField dtCol in DelCol)
                {
                    grvMain.Columns.Remove(dtCol);
                }

                grvMain.DataSource = null;
                grvMain.DataBind();
            }

            if (SearhData.Rows.Count > 0)
            {
                if (SearhData.Columns.Count > 4)
                {
                    int insertStartWith = 2;
                    for (int i = 0; i < SearhData.Columns.Count - 7; i++)
                    {
                        TemplateField col = new TemplateField();
                        col.HeaderText = SearhData.Columns[i].ColumnName.Replace(Environment.NewLine, "").Replace("\r", "").Replace("\n", "").Replace("\t", "").Trim();
                        grvMain.Columns.Insert(insertStartWith, col);
                        insertStartWith++;
                    }

                    TemplateField colPercent = new TemplateField();
                    PercentYear = decimal.Parse(SearhData.Rows[0]["PERCENT"].ToString());
                    colPercent.HeaderText = PercentYear.ToString("0.##") + "%";
                    grvMain.Columns.Insert(insertStartWith + 1, colPercent);
                }
            }
            grvMain.DataSource = SearhData;
            grvMain.DataBind();

        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }

    protected void grvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                if (SearhData.Rows.Count > 0)
                {
                    if (SearhData.Columns.Count > 4)
                    {
                        Label lblFAv = new Label();
                        lblFAv.ID = "lblFAv";
                        lblFAv.Text = "Average";
                        e.Row.Cells[1].Controls.Add(lblFAv);
                        e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[1].Font.Bold = true;

                        int NumOfCalColumn = SearhData.Columns.Count - 5;
                        int insertStartWith = 2;
                        double TTotal = 0.00;
                        DataRow[] foundRows = SearhData.Select(" NTYPEVISITFORMID > 0 ");
                        decimal CountRow = 0;
                        if (foundRows != null)
                        {
                            CountRow = foundRows.Length;
                        }

                        for (int i = 0; i < SearhData.Columns.Count - 7; i++)
                        {
                            Label lbl = new Label();
                            double SumO = (double)(SearhData.AsEnumerable().Where(y => y.Field<decimal>(i) != 0).Sum(x => x.Field<decimal>(i)));
                            if (SumO != 0)
                            {
                                SumO = SumO / (double)CountRow;
                            }
                            TTotal += SumO;
                            lbl.Text = SumO.ToString("0.00");
                            e.Row.Cells[insertStartWith].Controls.Add(lbl);
                            e.Row.Cells[insertStartWith].HorizontalAlign = HorizontalAlign.Center;
                            insertStartWith++;
                        }

                        //Total
                        Label lblTotal = new Label();
                        lblTotal.Text = TTotal.ToString("0.00");
                        e.Row.Cells[insertStartWith].Controls.Add(lblTotal);
                        e.Row.Cells[insertStartWith].HorizontalAlign = HorizontalAlign.Center;
                        insertStartWith++;

                        Label lblPercent = new Label();
                        lblPercent.Text = ((decimal.Parse(TTotal.ToString()) / 100) * PercentYear).ToString("0.00");
                        e.Row.Cells[insertStartWith].Controls.Add(lblPercent);
                        e.Row.Cells[insertStartWith].HorizontalAlign = HorizontalAlign.Center;
                    }
                }

            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label DataItemIndex = new Label();
                DataItemIndex.ID = "DataItemIndex";
                DataItemIndex.Text = ((grvMain.PageSize * grvMain.PageIndex) + (e.Row.RowIndex + 1)).ToString("0");
                e.Row.Cells[0].Controls.Add(DataItemIndex);

                Label lblSVENDORNAME = new Label();
                string SVENDORNAME = DataBinder.Eval(e.Row.DataItem, "SVENDORNAME").ToString();
                lblSVENDORNAME.Text = SVENDORNAME;
                e.Row.Cells[1].Controls.Add(lblSVENDORNAME);
                int insertStartWith = 2;
                if (SearhData.Rows.Count > 0)
                {
                    if (SearhData.Columns.Count > 4)
                    {
                        for (int i = 0; i < SearhData.Columns.Count - 7; i++)
                        {
                            Label lbl = new Label();
                            lbl.Text = ((decimal)SearhData.Rows[e.Row.RowIndex][i]).ToString("0.00");
                            e.Row.Cells[insertStartWith].Controls.Add(lbl);
                            e.Row.Cells[insertStartWith].HorizontalAlign = HorizontalAlign.Center;
                            insertStartWith++;
                        }
                    }
                }

                Label lblSUM_SCORE = new Label();
                decimal SUM_SCORE = (decimal)DataBinder.Eval(e.Row.DataItem, "TOTAL");
                lblSUM_SCORE.Text = SUM_SCORE.ToString("0.00");
                e.Row.Cells[insertStartWith].Controls.Add(lblSUM_SCORE);
                insertStartWith++;

                Label lblPercent = new Label();
                decimal PERCENT = (decimal.Parse(SUM_SCORE.ToString()) / 100) * PercentYear;
                lblPercent.Text = PERCENT.ToString("0.00");
                e.Row.Cells[insertStartWith].Controls.Add(lblPercent);
                e.Row.Cells[insertStartWith].HorizontalAlign = HorizontalAlign.Center;
                insertStartWith++;

                Label lblDCREATE = new Label();
                string SDCHECK = DataBinder.Eval(e.Row.DataItem, "SDCHECK").ToString();
                lblDCREATE.Text = SDCHECK;
                e.Row.Cells[insertStartWith].Controls.Add(lblDCREATE);
                insertStartWith++;

                if (user_profile.CGROUP == GROUP_PERMISSION.UNIT)
                {
                    string ID = DataBinder.Eval(e.Row.DataItem, "NQUESTIONNAIREID").ToString().Trim();
                    CheckBox chkIsView = new CheckBox() { Enabled = false };
                    string IS_VENDOR_VIEW = DataBinder.Eval(e.Row.DataItem, "IS_VENDOR_VIEW").ToString().Trim();
                    chkIsView.Checked = IS_VENDOR_VIEW == "1" ? true : false;
                    e.Row.Cells[insertStartWith].Controls.Add(chkIsView);
                    insertStartWith++;

                    HtmlImage imgView = new HtmlImage() { Src = "~/Images/btnSearch.png", Width = 16, Height = 16 };
                    imgView.Attributes.Add("onclick", "gotoView(\'" + ID + "\');");
                    imgView.Attributes.Add("Style", "cursor: pointer");
                    e.Row.Cells[insertStartWith].Controls.Add(imgView);
                    insertStartWith++;
                    
                    HtmlImage imgEdit = new HtmlImage() { Src = "~/Images/document.gif", Width = 16, Height = 16 };
                    if (CanWrite)
                    {
                        imgEdit.Attributes.Add("onclick", "gotoEdit(\'" + ID + "\');");
                        imgEdit.Attributes.Add("Style", "cursor: pointer");
                    }
                    else
                    {
                        imgEdit.Attributes.Add("onclick", "javascript:void(0);");
                        imgEdit.Attributes.Add("Style", "cursor: not-allowed");
                    }
                    
                    e.Row.Cells[insertStartWith].Controls.Add(imgEdit);
                    insertStartWith++;
                                        
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }
    protected void btnFormConfig_Click(object sender, EventArgs e)
    {
        grvMain.DataSource = SearhData;
        grvMain.DataBind();
    }
    protected void grvMain_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvMain.PageIndex = e.NewPageIndex;
        LoadMain();
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        string Id = hdnNum.Value;
        if (Id == "0") return;
        Button btn = sender as Button;
        if (btn.ID == "btnView")
        {
            Session["ViewNQUESTIONNAIREID"] = Id;
        }
        else
        {
            Session["EditNQUESTIONNAIREID"] = Id;
        }
        Response.Redirect("questionnaire_form.aspx");
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    private void ExportToExcel()
    {
        try
        {
            string _err = string.Empty;
            if (ddlYearSearch.SelectedItem != null) SearchCriteria.NYEAR = ddlYearSearch.SelectedItem.Value;
            if (ddlVendorSearch.SelectedItem != null) SearchCriteria.SVENDORID = ddlVendorSearch.SelectedItem.Value;
            bool OnlyComplete = user_profile.CGROUP != GROUP_PERMISSION.UNIT ? true : false;
            DataTable dt = new QuestionnaireBLL().GetListScoreByGroup(ref _err, SearchCriteria, OnlyComplete);

            dt.Columns["ROWNUM"].SetOrdinal(0);
            dt.Columns["SVENDORNAME"].SetOrdinal(1);
            DataRow newRow = dt.NewRow();
            newRow["SVENDORNAME"] = "Average";
            int NumOfCalColumn = dt.Columns.Count - 5;
            int insertStartWith = 2;
            double TTotal = 0.00;
            DataRow[] foundRows = dt.Select(" NTYPEVISITFORMID > 0 ");
            decimal CountRow = 0;
            if (foundRows != null)
            {
                CountRow = foundRows.Length;
            }
            for (int i = insertStartWith; i < dt.Columns.Count - 6; i++)
            {
                double SumO = (double)(dt.AsEnumerable().Where(y => y.Field<decimal>(i) != 0).Sum(x => x.Field<decimal>(i)));
                if (SumO != 0)
                {
                    SumO = SumO / (double)CountRow;
                }
                newRow[insertStartWith] = SumO.ToString("0.00");
                TTotal += SumO;
                insertStartWith++;
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                decimal SUM_SCORE = decimal.Parse(dt.Rows[i]["TOTAL"].ToString());
                decimal PERCENT = (decimal.Parse(SUM_SCORE.ToString()) / 100) * PercentYear;
                dt.Rows[i]["PERCENT"] = PERCENT.ToString("0.00");
            }


            newRow[insertStartWith] = TTotal.ToString("0.00");
            insertStartWith++;
            newRow[insertStartWith] = ((decimal.Parse(TTotal.ToString()) / 100) * PercentYear).ToString("0.00");

            dt.Rows.Add(newRow);

            dt.Columns.Remove("NQUESTIONNAIREID");
            dt.Columns.Remove("NTYPEVISITFORMID");
            dt.Columns.Remove("IS_VENDOR_VIEW");
            dt.Columns["SVENDORNAME"].ColumnName = "บริษัทผู้ขนส่ง";
            dt.Columns["SDCHECK"].ColumnName = "วันที่ตรวจสอบ";
            dt.Columns["ROWNUM"].ColumnName = "ลำดับ";
            dt.Columns["PERCENT"].ColumnName = PercentYear.ToString() + "%";
            using (ExcelPackage pck = new ExcelPackage())
            {
                var ws = pck.Workbook.Worksheets.Add("Audit");

                ws.Cells["A1"].LoadFromDataTable(dt, true);
                Response.Clear();
                Response.BinaryWrite(pck.GetAsByteArray());
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=report_audit.xlsx");
                Response.End();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }


    }


    protected void grvMain_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {

            for (int i = grvMain.Columns.Count - 1; i >= grvMain.Columns.Count - 3; i--)
                e.Row.Cells[i].Visible = user_profile.CGROUP != GROUP_PERMISSION.UNIT ? false : true;

        }
        catch (Exception)
        {

        }
    }
}