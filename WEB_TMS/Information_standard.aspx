﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="Information_standard.aspx.cs" Inherits="Information_standard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
  <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <script type="text/javascript">

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" OnCallback="xcpn_Callback"
        ClientInstanceName="xcpn" CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <dx:ASPxRoundPanel ID="rpnInformation" ClientInstanceName="rpn" runat="server" Width="980px"
                    HeaderText="ข้อมูลตัวคูณมาตรฐาน">
                    <PanelCollection>
                        <dx:PanelContent runat="server" ID="arp">
                            <table id="Table1" runat="server" width="100%">
                                <tr>
                                    <td>
                                        <table width="100%" runat="server">
                                            <tr>
                                                <td width="20%">จัดการมาตราฐานสำหรับรถขนส่ง :</td>
                                                <td width="80%">
                                                    <dx:ASPxComboBox runat="server" DataSourceID="sds" ID="cboTruckType" TextField="SCARTYPENAME"
                                                        ValueField="SCARTYPEID">
                                                        <ClientSideEvents SelectedIndexChanged="function (s, e) { xcpn.PerformCallback('chagevalidate'); }" />
                                                    </dx:ASPxComboBox>
                                                    <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                        SelectCommand="SELECT &quot;SCARTYPEID&quot;, &quot;SCARTYPENAME&quot; FROM &quot;TTRUCKTYPEINFORMATION&quot;">
                                                    </asp:SqlDataSource>
                                                    <dx:ASPxTextBox runat="server" ID="txtchkEditgrid" ClientVisible="false">
                                                    </dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="Table2" runat="server" width="100%">
                                            <tr>
                                                <td width="9%">ค้นหาคลัง :</td>
                                                <td width="91%">
                                                    <dx:ASPxTextBox runat="server" ID="txtSearch" NullText="รหัสคลัง,คลัง" CssClass="dxeLineBreakFix"
                                                        Width="300px">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search'); }"></ClientSideEvents>
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="btnCancelSearch" runat="server" SkinID="_cancelsearch" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('CancelSearch'); }">
                                                        </ClientSideEvents>
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="false" SkinID="_gvw"
                                                        OnAfterPerformCallback="gvw_AfterPerformCallback" ClientInstanceName="gvw" OnInit="gvwSum_Init"
                                                        KeyFieldName="STERMINALID" EnableRowsCache="false" OnRowUpdating="gvw_RowUpdating">
                                                        <Columns>
                                                            <dx:GridViewDataColumn Caption="รหัสคลัง" FieldName="STERMINALID" Width="3%" HeaderStyle-HorizontalAlign="Center"
                                                                CellStyle-HorizontalAlign="Center" EditCellStyle-HorizontalAlign="Center">
                                                                <EditItemTemplate>
                                                                    <dx:ASPxLabel runat="server" ID="lblSTERMINALID" Text='<%# DataBinder.Eval(Container.DataItem,"STERMINALID") %>'>
                                                                    </dx:ASPxLabel>
                                                                </EditItemTemplate>
<EditCellStyle HorizontalAlign="Center"></EditCellStyle>
                                                                <DataItemTemplate>
                                                                    <dx:ASPxHyperLink runat="server" ID="hplSVENDORID" Text='<%# Eval("STERMINALID") %>'
                                                                        Cursor="pointer">
                                                                        <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('viewHistoryInformation;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    </dx:ASPxHyperLink>
                                                                </DataItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="คลัง" FieldName="SABBREVIATION" Width="15%" HeaderStyle-HorizontalAlign="Center"
                                                                CellStyle-HorizontalAlign="Left">
                                                                <EditItemTemplate>
                                                                    <dx:ASPxLabel runat="server" ID="lblSABBREVIATION" Text='<%# DataBinder.Eval(Container.DataItem,"SABBREVIATION") %>'>
                                                                    </dx:ASPxLabel>
                                                                </EditItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Left">
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <%--   <dx:GridViewCommandColumn Width="7%" Caption=" ">
                                                                <EditButton Visible="true">
                                                                </EditButton>
                                                            </dx:GridViewCommandColumn>--%>
                                                            <dx:GridViewDataColumn Width="5%" Caption="การจัดการ"  HeaderStyle-HorizontalAlign="Center"
                                                                CellStyle-HorizontalAlign="Center">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxButton runat="server" ID="edt" SkinID="_edit">
                                                                        <ClientSideEvents Click="function(s,e){ gvw.PerformCallback('Startedit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    </dx:ASPxButton>
                                                                </DataItemTemplate>
                                                                <EditItemTemplate>
                                                                    <dx:ASPxButton runat="server" ID="edt" SkinID="_update" AutoPostBack="false" CssClass="dxeLineBreakFix" >
                                                                        <ClientSideEvents Click="function(s,e){if(!ASPxClientEdit.ValidateGroup('add')) return false;  gvw.PerformCallback('Updateedit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    </dx:ASPxButton>
                                                                    <dx:ASPxButton runat="server" ID="ASPxButton1" SkinID="_cancelth" AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                                        <ClientSideEvents Click="function(s,e){gvw.CancelEdit(); }" />
                                                                    </dx:ASPxButton>
                                                                </EditItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="CHECKIN" Visible="false">
                                                            </dx:GridViewDataColumn>
                                                        <%--    <dx:GridViewDataColumn FieldName="SCARTYPEID" Visible="false">
                                                            </dx:GridViewDataColumn>--%>
                                                        </Columns>
                                                        <SettingsPager Mode="ShowAllRecords">
                                                        </SettingsPager>
                                                        <SettingsEditing Mode="Inline" />
                                                         <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
        </ClientSideEvents>
                                                    </dx:ASPxGridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <font color="red">หมายเหตุ</font> กรณีที่ไม่มีการขนส่งให้ระบุเป็นช่องว่าง
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
