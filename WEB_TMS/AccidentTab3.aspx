﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="AccidentTab3.aspx.cs" Culture="en-US" UICulture="en-US" Inherits="AccidentTab3" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">

    <asp:Panel runat="server" ID="plTab3">
        <div class="form-horizontal">
            <ul class="nav nav-tabs" id="tabtest">
                <li class="" id="liTab1" runat="server"><a href="#TabGeneral" runat="server" id="GeneralTab1">แจ้งเรื่องอุบัติเหตุ</a></li>
                <li class="" id="liTab2" runat="server"><a href="#TabGeneral2" runat="server" id="GeneralTab2">รายงานเบื้องต้น</a></li>
                <li class="active" id="liTab3" runat="server"><a href="#TabGeneral3" data-toggle="tab" aria-expanded="true" runat="server" id="GeneralTab3">รายงานวิเคราะห์สาเหตุ</a></li>
                <li class="" id="liTab4" visible="false" runat="server"><a href="#TabGeneral4" runat="server" id="GeneralTab4">รายงานผลการพิจารณา</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="TabGeneral">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse8" id="acollapse8">ความเสียหาย(ประเมินโดยคณะกรรมการสอบสวนอุบัติเหตุร้ายแรง)</a>
                            <asp:HiddenField runat="server" ID="hidcollapse8" />
                        </div>
                        <div id="collapse8" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row form-group ">
                                    <div class="col-md-12">
                                        <h5>
                                            <asp:Label Text="" ID="lblType" runat="server" /></h5>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse1" id="acollapse1">ข้อมูลการเกิดอุบัติเหตุ</a><asp:HiddenField runat="server" ID="hidcollapse1" />
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row form-group">
                                    <label class="col-md-3 control-label">Accident ID</label>
                                    <div class="col-md-3">
                                        <asp:TextBox runat="server" ID="txtAccidentID" CssClass="form-control" ReadOnly="true" Text="Generate by System" />
                                    </div>

                                </div>
                                <div class="row form-group">
                                    <label class="col-md-3 control-label">วันที่-เวลาเกิดเหตุ</label>
                                    <div class="col-md-3">
                                        <asp:TextBox runat="server" ID="txtAccidentDate" CssClass="form-control datetimepicker" ReadOnly="true" />
                                    </div>
                                    <label class="col-md-2 control-label">วันที่-เวลาที่แจ้งเรื่องในระบบ</label>
                                    <div class="col-md-3">
                                        <asp:TextBox runat="server" ID="txtAccidentDateSystem" CssClass="form-control datetimepicker" ReadOnly="true" />
                                    </div>

                                </div>
                                <div class="row form-group">
                                    <label class="col-md-3 control-label">ผขส. แจ้งเหตุให้ ปตท.</label>
                                    <div class="col-md-3">
                                        <div class="col-md-4 PaddingLeftRight0">
                                            <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblREPORTER" Enabled="false">
                                                <asp:ListItem Text="No" Value="0" />
                                                <asp:ListItem Text="Yes" Value="1" Selected="True" />
                                            </asp:RadioButtonList>
                                        </div>
                                        <div class="col-md-8 PaddingLeftRight0">
                                            <asp:TextBox runat="server" ID="txtAccidentDatePtt" CssClass="form-control datetimepicker" ReadOnly="true" />
                                        </div>

                                    </div>
                                    <label class="col-md-2 control-label">ชื่อผู้แจ้ง(ผู้ขนส่ง)</label>
                                    <div class="col-md-3">
                                        <asp:TextBox runat="server" ID="txtAccidentName" CssClass="form-control" ReadOnly="true" />
                                    </div>

                                </div>
                                <div class="row form-group">
                                    <label class="col-md-3 control-label">วันที่-เวลาจัดทำอุบัติเหตุเบื้องต้น</label>
                                    <div class="col-md-3">
                                        <asp:TextBox runat="server" ID="txtDatePrimary" CssClass="form-control datetimepicker" ReadOnly="true" />
                                    </div>
                                    <label class="col-md-2 control-label">ผขส. ส่งรายงานวิเคราะห์สาเหตุ</label>
                                    <div class="col-md-3">
                                        <asp:TextBox runat="server" CssClass="form-control datetimepicker" ID="txtDateAnalysis" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-3 control-label">วันที่-เวลา เจ้าหน้าที่ ปง.อนุมัติเอกสาร</label>
                                    <div class="col-md-3">
                                        <asp:TextBox runat="server" CssClass="form-control datetimepicker" ID="txtApproveDate" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse3" id="acollapse3">เอกสารหลักฐานประกอบการพิจารณา</a><asp:HiddenField runat="server" ID="hidcollapse3" />
                        </div>
                        <div id="collapse3" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <asp:GridView ID="dgvUploadFile" runat="server" CssClass="table table-striped table-bordered" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader"
                                            HorizontalAlign="Center" AutoGenerateColumns="false"
                                            CellPadding="4" GridLines="None" DataKeyNames="UPLOAD_ID,FULLPATH" OnRowDeleting="dgvUploadFile_RowDeleting"
                                            OnRowUpdating="dgvUploadFile_RowUpdating" ForeColor="#333333" OnRowDataBound="dgvUploadFile_RowDataBound" EmptyDataText="[ ไม่มีข้อมูล ]">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                                <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                                <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                                <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                                <asp:BoundField DataField="FULLPATH" Visible="false" />
                                                <asp:TemplateField HeaderText="STATUS">
                                                    <ItemTemplate>
                                                        <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblStatus">
                                                            <asp:ListItem Text="&nbsp;ผ่าน&nbsp;&nbsp;" Value="0" />
                                                            <asp:ListItem Text="&nbsp;ไม่ผ่าน" Value="1" />

                                                        </asp:RadioButtonList>
                                                        <asp:HiddenField runat="server" ID="hidUPLOAD_ID" Value='<%# Eval("UPLOAD_ID") %>' />
                                                        <asp:HiddenField runat="server" ID="hidUPLOAD_NAME" Value='<%# Eval("UPLOAD_NAME") %>' />
                                                        <asp:HiddenField runat="server" ID="hidFILENAME_SYSTEM" Value='<%# Eval("FILENAME_SYSTEM") %>' />
                                                        <asp:HiddenField runat="server" ID="hidFILENAME_USER" Value='<%# Eval("FILENAME_USER") %>' />
                                                        <asp:HiddenField runat="server" ID="hidFULLPATH" Value='<%# Eval("FULLPATH") %>' />
                                                        <asp:HiddenField runat="server" ID="hidSTATUS" Value='<%# Eval("STATUS") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="หมายเหตุ">
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtRemark" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                            Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                                    Height="25px" Style="cursor: pointer" CommandName="Delete" Visible="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>

                                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>

                                        </asp:GridView>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="row form-group">
                                        <label class="col-md-4 control-label"></label>
                                        <div class="col-md-4 text-center">
                                            <input id="btnApprove" type="button" value="อนุมัติ" data-toggle="modal" data-target="#ModalConfirmBeforeApprove" class="btn btn-md bth-hover btn-info" />
                                            <input id="btnEdit" type="button" value="แก้ไขเอกสาร" data-toggle="modal" data-target="#ModalConfirmEdit" class="btn btn-md bth-hover btn-info" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse2" id="acollapse2">ประมาณการความเสียหาย</a><asp:HiddenField runat="server" ID="hidcollapse2" />
                        </div>
                        <div id="collapse2" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row form-group">
                                    <label class="col col-md-5 control-label">ประมาณการความเสียหาย (บาท)</label>
                                    <div class="col-md-3">
                                        <asp:TextBox runat="server" CssClass="form-control number" ID="txtDAMAGE" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse4" id="acollapse4">สรุปเหตุการณ์รถขนส่งเกิดอุบัติเหตุโดย&nbsp;ปตท.</a><asp:HiddenField runat="server" ID="hidcollapse4" />
                        </div>
                        <div id="collapse4" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row form-group">
                                    <label class="col col-md-2 control-label">การประเมินความผิดของผู้ขนส่ง</label>
                                    <div class="col-md-3">
                                        <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblEVALUATION">
                                            <asp:ListItem Text="&nbsp;ฝ่ายถูก&nbsp;&nbsp;" Value="0" />
                                            <asp:ListItem Text="&nbsp;ฝ่ายผิด" Value="1" />
                                            <asp:ListItem Text="&nbsp;ประมาทร่วม" Value="2" />
                                        </asp:RadioButtonList>
                                    </div>
                                    <div class="col-md-7">
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtEVALUATION_DETAIL"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col col-md-2 control-label">อุบัติเหตุที่เกิดขึ้นเข้าข่าย</label>
                                    <div class="col-md-3">
                                        <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblSERIOUS">
                                            <asp:ListItem Text="&nbsp;ร้ายแรง&nbsp;&nbsp;" Value="0" />
                                            <asp:ListItem Text="&nbsp;ไม่ร้ายแรง" Value="1" />
                                        </asp:RadioButtonList>
                                    </div>
                                    <div class="col-md-7">
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtSERIOUS_DETAIL"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col col-md-2 control-label">1.ชีวิต(พขร.)</label>
                                    <div class="col-md-3">
                                        <asp:RadioButtonList runat="server" ID="rblLIFE">
                                            <asp:ListItem Text="&nbsp;ไม่มีผลกระทบ&nbsp;&nbsp;" Value="4" />
                                            <asp:ListItem Text="&nbsp;บาดเจ็บเล็กน้อย&nbsp;&nbsp;" Value="0" />
                                            <asp:ListItem Text="&nbsp;รักษาทางการแพทย์&nbsp;แต่ไม่ต้องหยุดงาน" Value="1" />
                                            <asp:ListItem Text="&nbsp;หยุดงาน&nbsp;หรือ&nbsp;ต้องเปลี่ยนงาน" Value="2" />
                                            <asp:ListItem Text="&nbsp;ทุพลภาพหรือเสียชีวิต" Value="3" />
                                        </asp:RadioButtonList>
                                    </div>
                                    <label class="col col-md-2 control-label">2.ทรัพย์สินเสียหาย</label>
                                    <div class="col-md-3">
                                        <asp:RadioButtonList runat="server" ID="rblPROPERTY">
                                            <asp:ListItem Text="&nbsp;ไม่มีผลกระทบ&nbsp;&nbsp;" Value="4" />
                                            <asp:ListItem Text="&nbsp;เสียหายน้อยมาก&nbsp(ไม่เกิน&nbsp50,000&nbspบาท)&nbsp;&nbsp;" Value="0" />
                                            <asp:ListItem Text="&nbsp;เสียหายปานกลาง&nbsp;(ไม่เกิน 250,000&nbsp;บาท)" Value="1" />
                                            <asp:ListItem Text="&nbsp;เสียหายมาก&nbsp;(ไม่เกิน 1,000,000&nbsp;บาท)" Value="2" />
                                            <asp:ListItem Text="&nbsp;เสียหายมาก&nbsp;(มากกว่า 1,000,000&nbsp;บาท)" Value="3" />
                                        </asp:RadioButtonList>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="row form-group">
                                    <label class="col col-md-2 control-label">3.สิ่งแวดล้อม</label>
                                    <div class="col-md-3">
                                        <asp:RadioButtonList runat="server" ID="rblENVIRONMENT">
                                            <asp:ListItem Text="&nbsp;ไม่มีผลกระทบ&nbsp;&nbsp;" Value="4" />
                                            <asp:ListItem Text="&nbsp;มีผลกระทบเล็กน้อย" Value="0" />
                                            <asp:ListItem Text="&nbsp;มีผลกระทบปานกลาง" Value="1" />
                                            <asp:ListItem Text="&nbsp;มีผลกระทบรุนแรง" Value="2" />
                                            <asp:ListItem Text="&nbsp;มีผลกระทบรุนแรงมาก" Value="3" />
                                        </asp:RadioButtonList>
                                    </div>
                                    <label class="col col-md-2 control-label">4.ภาพลักษณ์องค์กร</label>
                                    <div class="col-md-3">
                                        <asp:RadioButtonList runat="server" ID="rblCORPORATE">
                                            <asp:ListItem Text="&nbsp;ไม่มีผลกระทบ&nbsp;&nbsp;" Value="4" />
                                            <asp:ListItem Text="&nbsp;อาจจะทำให้เกิดผลกระทบ" Value="0" />
                                            <asp:ListItem Text="&nbsp;มีผลกระทบทางอ้อม" Value="1" />
                                            <asp:ListItem Text="&nbsp;มีผลกระทบโดยตรง&nbsp;บริหารจัดการได้" Value="2" />
                                            <asp:ListItem Text="&nbsp;มีผลกระทบโดยตรง&nbsp;ไม่สามารถบริหารจัดการได้" Value="3" />
                                        </asp:RadioButtonList>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="row form-group">
                                    <label class="col col-md-5 control-label">ปริมาณน้ำมันเสียหายหรือสูญหายเนื่องจากอุบัติเหตุ(ลิตร)</label>
                                    <div class="col-md-7">
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtOil" />
                                    </div>
                                </div>
                                <hr />
                                <div class="row form-group">
                                    <label class="col col-md-12">การวิเคราะห์สาเหตุของการเกิดอุบัติเหตุ (Root Cause Analysis) </label>
                                </div>
                                <div class="row form-group">
                                    <label class="col col-md-2 control-label">1. เกิดจากคน (Personal Factors) </label>
                                    <div class="col-md-4 checkbox">
                                        <asp:CheckBoxList runat="server" ID="cblPERSONALFACTORS">
                                            <asp:ListItem Text="ความรู้ไม่เพียงพอ/ ไม่ชำนาญพอ" Value="0" />
                                            <asp:ListItem Text="ฝ่าฝืนกฎหมาย/ข้อบังคับของบริษัท" Value="1" />
                                            <asp:ListItem Text="ประมาท/ไม่ปฏิบัติตามหลักการขับขี่อย่างปลอดภัย" Value="2" />
                                            <asp:ListItem Text="เมื่อยล้า" Value="3" />
                                            <asp:ListItem Text="อื่นๆ (อธิบาย)" Value="4" />
                                        </asp:CheckBoxList>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtPERSONALFACTORS" />
                                    </div>
                                    <label class="col col-md-2 control-label">2. เกิดจากเส้นทางการขนส่ง (Route Hazardous) </label>
                                    <div class="col-md-4 checkbox">
                                        <asp:CheckBoxList runat="server" ID="cblROUTEHAZARDOUS">
                                            <asp:ListItem Text="ขาดการประเมินความเสี่ยงเส้นทางการขนส่ง" Value="0" />
                                            <asp:ListItem Text="จุดจอดที่มีอยู่ไม่เหมาะสม" Value="1" />
                                            <asp:ListItem Text="ขาดการสื่อความในเรื่องของจุดเสี่ยง/จุดจอด" Value="2" />
                                            <asp:ListItem Text="อื่นๆ (อธิบาย)" Value="3" />

                                        </asp:CheckBoxList>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtROUTEHAZARDOUS" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col col-md-2 control-label">3. เกิดจากสภาพรถขนส่ง (Truck)</label>
                                    <div class="col-md-4 checkbox">
                                        <asp:CheckBoxList runat="server" ID="cblTRUCK">
                                            <asp:ListItem Text="รถขนส่งไม่ได้ตามมาตรฐานรถขนส่งผลิตภัณฑ์ปิโตรเลียม" Value="0" />
                                            <asp:ListItem Text="ขาดการตรวจความพร้อมของรถขนส่งก่อนรับผลิตภัณฑ์" Value="1" />
                                            <asp:ListItem Text="ประมาท/ไม่ปฏิบัติตามหลักการขับขี่อย่างปลอดภัย" Value="2" />
                                            <asp:ListItem Text="ขาดการบำรุงรักษา" Value="3" />
                                            <asp:ListItem Text="อื่นๆ (อธิบาย)" Value="4" />
                                        </asp:CheckBoxList>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtTRUCK" />
                                    </div>
                                    <label class="col col-md-2 control-label">4. เกิดจากสภาพแวดล้อม (Environment)</label>
                                    <div class="col-md-4 checkbox">
                                        <asp:CheckBoxList runat="server" ID="cblENVIRONMENTS">
                                            <asp:ListItem Text="ฝนตก / หมอกลง " Value="0" />
                                            <asp:ListItem Text="ความมืด/ไม่มีแสงไฟส่องสว่าง" Value="1" />
                                            <asp:ListItem Text="บุคคลที่ 3 วิ่งตัดหน้ากระชั้นชิด" Value="2" />
                                            <asp:ListItem Text="อื่นๆ (อธิบาย)" Value="3" />

                                        </asp:CheckBoxList>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtENVIRONMENTS" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col col-md-2 control-label">5. เกิดจากนโยบายบริษัท (Company Policy)</label>
                                    <div class="col-md-4 checkbox">
                                        <asp:CheckBoxList runat="server" ID="cblCOMPANYPOLICY">
                                            <asp:ListItem Text="ชั่วโมงการทำงาน/การพักผ่อนของพนักงานรับรถไม่เหมาะสม" Value="0" />
                                            <asp:ListItem Text="ขาดการตรวจสอบการปฏิบัติงานของพนักงานขับรถ" Value="1" />
                                            <asp:ListItem Text="อื่นๆ (อธิบาย)" Value="2" />
                                        </asp:CheckBoxList>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtCOMPANYPOLICY" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-4 control-label"></label>
                                    <div class="col-md-4  text-center">
                                        <input id="btnSave" type="button" value="บันทึก" data-toggle="modal" data-target="#ModalConfirmBeforeSave" class="btn btn-md bth-hover btn-info" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

    <uc1:ModelPopup runat="server" ID="mpSave" IDModel="ModalConfirmBeforeApprove" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpSaveSend_ClickOK" TextTitle="ยืนยันการอนุมัติ" TextDetail="คุณต้องการอนุมัติใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpEdit" IDModel="ModalConfirmEdit" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpEdit_ClickOK" TextTitle="ยืนยันการแก้ไขเอกสาร" TextDetail="คุณต้องการแก้ไขเอกสารใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpSaveSend" IDModel="ModalConfirmBeforeSave" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpSave_ClickOK" TextTitle="ยืนยันบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
    <asp:HiddenField ID="hidID" runat="server" />
    <asp:HiddenField ID="hidCactive" runat="server" />
    <asp:HiddenField ID="hidCGROUP" runat="server" />
    <asp:HiddenField ID="hidSEMPLOYEEID" runat="server" />
    <asp:HiddenField ID="hidPERS_CODE" runat="server" />
    <asp:HiddenField ID="hidSVENDORID" runat="server" />
    <asp:HiddenField ID="hidDRIVER_NO" runat="server" />
    <script type="text/javascript">
        function SetEnabledControlByID(id) {
            if (id == '6') {
                $('#btnApprove,#btnEdit').prop('disabled', false);
            }
            else if (id == '8') {
                $('#btnApprove,#btnEdit').prop('disabled', true);
            }
            else {
                $('#btnApprove,#btnEdit').prop('disabled', true);
            }
            var last = $('#<%= hidcollapse1.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse1").removeClass('in');
            }
            last = $('#<%= hidcollapse2.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse2").removeClass('in');
            }
            last = $('#<%= hidcollapse3.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse3").removeClass('in');
            }
            last = $('#<%= hidcollapse4.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse4").removeClass('in');
            }

        }
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(EndRequestHandler);
        $(document).ready(function () {

            fcollapse();
        });


        function EndRequestHandler(sender, args) {
            fcollapse();
        }
        function fcollapse() {
            $("#acollapse1").on('click', function () {
                var active = $("#collapse1").attr('class');
                //console.log(active);
                $('#<%= hidcollapse1.ClientID %>').val(active);
            });
            $("#acollapse2").on('click', function () {
                var active = $("#collapse2").attr('class');
                $('#<%= hidcollapse2.ClientID %>').val(active);
            });
            $("#acollapse3").on('click', function () {
                var active = $("#collapse3").attr('class');
                $('#<%= hidcollapse3.ClientID %>').val(active);
            });
            $("#acollapse4").on('click', function () {
                var active = $("#collapse4").attr('class');
                $('#<%= hidcollapse4.ClientID %>').val(active);
            });

        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

