﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxGridView;

public partial class admin_ChkList_add : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bool chkurl = false;
            Session["CheckPermission"] = null;
            string AddEdit = "";

            if (Session["cPermission"] != null)
            {
                string[] url = (Session["cPermission"] + "").Split('|');
                string[] chkpermision;
                bool sbreak = false;

                foreach (string inurl in url)
                {
                    chkpermision = inurl.Split(';');
                    if (chkpermision[0] == "37")
                    {
                        switch (chkpermision[1])
                        {
                            case "0":
                                chkurl = false;

                                break;
                            case "1":
                                chkurl = true;

                                break;

                            case "2":
                                chkurl = true;
                                AddEdit = "1";
                                break;
                        }
                        sbreak = true;
                    }

                    if (sbreak == true) break;
                }
            }

            if (chkurl == false)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            }

            Session["CheckPermission"] = AddEdit;

            var dt1 = new List<DT1>();
            Session["dt1"] = dt1;

            Session["NTYPEVISITFORMID"] = null;

            dteDateStart1.Value = DateTime.Now;

            if (Session["oNTYPEVISITFORMID"] != null)
            {
                Session["NTYPEVISITFORMID"] = Session["oNTYPEVISITFORMID"];
                listData();
            }
            else
            {

                dt1.Add(new DT1
                {
                    dtsID = 1,
                    dtsScoreName = "",
                    dtcScore = ""
                });

                sgvw.DataSource = dt1;
                sgvw.DataBind();

                Session["dt1"] = dt1;
            }

            SetYear();
        }

    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        lblDateUpdate.Text = DateTime.Now.ToString("dd/MM/yyyy HH.mm น.");
        lblUpdateBy.Text = CommonFunction.Get_Value(sql, "SELECT  SFIRSTNAME || ' ' || SLASTNAME AS FULLNAME FROM TUSER WHERE SUID = '" + Session["UserID"] + "" + "'");
    }

    private void SetYear()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = CommonFunction.Get_Data(sql, "select to_char(sysdate, 'YYYY') as cyear from dual");
            for (int i = 0; i < 5; i++)
            {
                int Year = int.Parse(dt.Rows[0]["cyear"].ToString()) - i;
                cmbYear.Items.Add(Year.ToString(), Year);
            }
            cmbYear.SelectedIndex = 0;

        }
        catch (Exception)
        {

            throw;
        }
    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        if ("" + Session["CheckPermission"] == "1")
        {
            switch (paras[0])
            {
                case "Save1":

                    if (Session["oNTYPEVISITFORMID"] == null)
                    {
                        //insert
                        /*int? chkValue = int.Parse(CommonFunction.Get_Value(sql, String.Format("SELECT count(*) FROM TTYPEOFVISITFORM WHERE CACTIVE = '1' and YEAR = {0}", cmbYear.Value)).ToString());
                        if (chkValue > 0)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้กรุณา InActive รายการเก่าก่อน!');");
                            return;
                        }

                        /// Check  รายการซ้ำ  (1 ปีมีได้ 1 แบบสอบถาม )

                        chkValue = int.Parse(CommonFunction.Get_Value(sql, string.Format("SELECT count(*) FROM TTYPEOFVISITFORM WHERE YEAR = {0}", cmbYear.Value)).ToString());
                        if (chkValue > 0)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้ เนื่องจากจำกัด 1 ปี มีได้ 1 แบบสอบถาม ');");
                            return;
                        }*/

                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();
                            string GenIDForm = CommonFunction.Gen_ID(con, "SELECT NTYPEVISITFORMID FROM (SELECT NTYPEVISITFORMID FROM TTYPEOFVISITFORM ORDER BY NTYPEVISITFORMID DESC) WHERE ROWNUM <= 1");
                            string strsql = "INSERT INTO TTYPEOFVISITFORM(NTYPEVISITFORMID,STYPEVISITFORMNAME,DUSE,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE) Values (:NTYPEVISITFORMID,:STYPEVISITFORMNAME,:DUSE,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CACTIVE)";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {

                                com.Parameters.Clear();
                                com.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = GenIDForm;
                                com.Parameters.Add(":STYPEVISITFORMNAME", OracleType.VarChar).Value = txtExam1.Text;
                                com.Parameters.Add(":DUSE", OracleType.DateTime).Value = dteDateStart1.Value;
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = rblStatus1.Value;
                                com.ExecuteNonQuery();
                                /// ค่าใน Radio
                                string strsql1 = "INSERT INTO TTYPEOFVISITFORMLIST(NTYPEVISITLISTID,NTYPEVISITFORMID,STYPEVISITLISTNAME,NTYPEVISITLISTSCORE) VALUES (:NTYPEVISITLISTID,:NTYPEVISITFORMID,:STYPEVISITLISTNAME,:NTYPEVISITLISTSCORE)";
                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                {
                                    for (int i = 0; i <= sgvw.VisibleRowCount - 1; i++)
                                    {
                                        string txtScoreName = ((ASPxTextBox)sgvw.FindRowCellTemplateControl(i, null, "txtScoreName")).Text + "";
                                        string txtScore = ((ASPxTextBox)sgvw.FindRowCellTemplateControl(i, null, "txtScore")).Text + "";


                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":NTYPEVISITLISTID", OracleType.Number).Value = i + 1;
                                        com1.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = GenIDForm;
                                        com1.Parameters.Add(":STYPEVISITLISTNAME", OracleType.VarChar).Value = txtScoreName;
                                        com1.Parameters.Add(":NTYPEVISITLISTSCORE", OracleType.Number).Value = txtScore;
                                        com1.ExecuteNonQuery();

                                    }
                                }

                            }
                            Session["NTYPEVISITFORMID"] = GenIDForm;

                        }
                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','บันทึกข้อมูลเรียบร้อย');");
                        LogUser("37", "I", "บันทึกข้อมูลหน้า แบบฟอร์มประเมินสถานประกอบการ", Session["NTYPEVISITFORMID"] + "");

                    }
                    else
                    {    //update

                        //int? chkValue = int.Parse(CommonFunction.Get_Value(sql, String.Format("SELECT count(*) FROM TTYPEOFVISITFORM WHERE CACTIVE = '1' and YEAR = {0} and NTYPEVISITFORMID != {1}", cmbYear.Value, Session["oNTYPEVISITFORMID"])).ToString());
                        //if (chkValue > 0)
                        //{
                        //    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้กรุณา InActive รายการเก่าก่อน!');");
                        //    return;
                        //}

                        /// Check  รายการซ้ำ  (1 ปีมีได้ 1 แบบสอบถาม )

                        int? chkValue = int.Parse(CommonFunction.Get_Value(sql, string.Format("SELECT count(*) FROM TTYPEOFVISITFORM WHERE YEAR = {0}  and NTYPEVISITFORMID != {1} ", cmbYear.Value, Session["oNTYPEVISITFORMID"])).ToString());
                        if (chkValue > 0)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้ เนื่องจากจำกัด 1 ปี มีได้ 1 แบบสอบถาม ');");
                            return;
                        }

                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();
                            string strsql = "UPDATE TTYPEOFVISITFORM SET STYPEVISITFORMNAME = :STYPEVISITFORMNAME,DUSE = :DUSE,DUPDATE = SYSDATE,SUPDATE = :SUPDATE,CACTIVE = :CACTIVE ,YEAR= :YEAR WHERE NTYPEVISITFORMID = :NTYPEVISITFORMID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {

                                com.Parameters.Clear();
                                com.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = Session["oNTYPEVISITFORMID"];
                                com.Parameters.Add(":STYPEVISITFORMNAME", OracleType.VarChar).Value = txtExam1.Text;
                                com.Parameters.Add(":DUSE", OracleType.DateTime).Value = dteDateStart1.Value;
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = rblStatus1.Value;
                                com.Parameters.Add(":YEAR", OracleType.Number).Value = cmbYear.Value;
                                com.ExecuteNonQuery();

                                string strsqlDel = "DELETE FROM TTYPEOFVISITFORMLIST WHERE NTYPEVISITFORMID = " + Session["oNTYPEVISITFORMID"] + "";

                                using (OracleCommand com2 = new OracleCommand(strsqlDel, con))
                                {
                                    com2.ExecuteNonQuery();
                                }

                                string strsql1 = "INSERT INTO TTYPEOFVISITFORMLIST(NTYPEVISITLISTID,NTYPEVISITFORMID,STYPEVISITLISTNAME,NTYPEVISITLISTSCORE) VALUES (:NTYPEVISITLISTID,:NTYPEVISITFORMID,:STYPEVISITLISTNAME,:NTYPEVISITLISTSCORE)";
                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                {
                                    for (int i = 0; i <= sgvw.VisibleRowCount - 1; i++)
                                    {
                                        string txtScoreName = ((ASPxTextBox)sgvw.FindRowCellTemplateControl(i, null, "txtScoreName")).Text + "";
                                        string txtScore = ((ASPxTextBox)sgvw.FindRowCellTemplateControl(i, null, "txtScore")).Text + "";

                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":NTYPEVISITLISTID", OracleType.Number).Value = i + 1;
                                        com1.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = Session["oNTYPEVISITFORMID"];
                                        com1.Parameters.Add(":STYPEVISITLISTNAME", OracleType.VarChar).Value = txtScoreName;
                                        com1.Parameters.Add(":NTYPEVISITLISTSCORE", OracleType.Number).Value = txtScore;
                                        com1.ExecuteNonQuery();
                                    }
                                }

                            }

                        }
                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','บันทึกข้อมูลเรียบร้อย');");
                        LogUser("37", "E", "แก้ไขข้อมูลหน้า แบบฟอร์มประเมินสถานประกอบการ", Session["oNTYPEVISITFORMID"] + "");

                    }
                    break;

                case "Save2":


                    if (Session["NTYPEVISITFORMID"] != null)
                    {
                        //insert
                        if (String.IsNullOrEmpty(txtGroupID.Text))
                        {

                            using (OracleConnection con = new OracleConnection(sql))
                            {
                                con.Open();
                                string GenIDForm = CommonFunction.Gen_ID(con, "SELECT NGROUPID FROM (SELECT NGROUPID FROM TGROUPOFVISITFORM ORDER BY  NGROUPID DESC) WHERE ROWNUM <= 1");
                                string strsql = "INSERT INTO TGROUPOFVISITFORM(NGROUPID,NTYPEVISITFORMID,SGROUPNAME,CACTIVE,SCREATE,DCREATE,SUPDATE,DUPDATE,NNO) Values (:NGROUPID,:NTYPEVISITFORMID,:SGROUPNAME,:CACTIVE,:SCREATE,SYSDATE,:SUPDATE,SYSDATE,:NNO)";
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {

                                    com.Parameters.Clear();
                                    com.Parameters.Add(":NGROUPID", OracleType.Number).Value = GenIDForm;
                                    com.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = Session["NTYPEVISITFORMID"] + "";
                                    com.Parameters.Add(":SGROUPNAME", OracleType.VarChar).Value = txtProb.Text;
                                    com.Parameters.Add(":CACTIVE", OracleType.Char).Value = rblStatus2.Value;
                                    com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":NNO", OracleType.Number).Value = txtRunNumber.Text;
                                    com.ExecuteNonQuery();

                                }
                            }
                        }
                        else
                        {
                            //update
                            using (OracleConnection con = new OracleConnection(sql))
                            {
                                con.Open();
                                string strsql = "UPDATE TGROUPOFVISITFORM SET SGROUPNAME = :SGROUPNAME,CACTIVE = :CACTIVE,SUPDATE = :SUPDATE,DUPDATE = SYSDATE,NNO = :NNO WHERE NGROUPID = :NGROUPID AND NTYPEVISITFORMID = :NTYPEVISITFORMID";
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {

                                    com.Parameters.Clear();
                                    com.Parameters.Add(":NGROUPID", OracleType.Number).Value = txtGroupID.Text;
                                    com.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = Session["oNTYPEVISITFORMID"] + "";
                                    com.Parameters.Add(":SGROUPNAME", OracleType.VarChar).Value = txtProb.Text;
                                    com.Parameters.Add(":CACTIVE", OracleType.Char).Value = rblStatus2.Value;
                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":NNO", OracleType.Number).Value = txtRunNumber.Text;
                                    com.ExecuteNonQuery();

                                }
                            }
                        }

                    }
                    ClearGroup();
                    gvw.DataBind();
                    AutoNumber();
                    break;

                case "Save3":

                    //insert
                    if (Session["NTYPEVISITFORMID"] != null)
                    {
                        if (String.IsNullOrEmpty(txtVisitformID.Text))
                        {
                            using (OracleConnection con = new OracleConnection(sql))
                            {
                                con.Open();

                                string GenIDForm = CommonFunction.Gen_ID(con, "SELECT NVISITFORMID FROM (SELECT NVISITFORMID FROM TVISITFORM ORDER BY  NVISITFORMID DESC) WHERE ROWNUM <= 1");
                                string strsql = "INSERT INTO TVISITFORM(NVISITFORMID,NTYPEVISITFORMID,NGROUPID,SVISITFORMNAME,NWEIGHT,CACTIVE,SCREATE,DCREATE,SUPDATE,DUPDATE) Values (:NVISITFORMID,:NTYPEVISITFORMID,:NGROUPID,:SVISITFORMNAME,:NWEIGHT,:CACTIVE,:SCREATE,SYSDATE,:SUPDATE,SYSDATE)";
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {

                                    com.Parameters.Clear();

                                    com.Parameters.Add(":NVISITFORMID", OracleType.Number).Value = GenIDForm;
                                    com.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = Session["NTYPEVISITFORMID"] + "";
                                    com.Parameters.Add(":NGROUPID", OracleType.Number).Value = cboGroup.Value;
                                    com.Parameters.Add(":SVISITFORMNAME", OracleType.VarChar).Value = txtTopicQ.Text;
                                    com.Parameters.Add(":CACTIVE", OracleType.Char).Value = rblStatus3.Value;
                                    com.Parameters.Add(":NWEIGHT", OracleType.Number).Value = txtWeight.Text;
                                    com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.ExecuteNonQuery();

                                }
                            }
                        }
                        else
                        {
                            //update

                            using (OracleConnection con = new OracleConnection(sql))
                            {
                                con.Open();
                                string strsql = "UPDATE TVISITFORM SET NGROUPID = :NGROUPID,SVISITFORMNAME = :SVISITFORMNAME,NWEIGHT = :NWEIGHT,CACTIVE = :CACTIVE,SUPDATE = :SUPDATE,DUPDATE = SYSDATE WHERE NVISITFORMID = :NVISITFORMID AND NTYPEVISITFORMID = :NTYPEVISITFORMID";
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {

                                    com.Parameters.Clear();
                                    com.Parameters.Add(":NVISITFORMID", OracleType.Number).Value = txtVisitformID.Text;
                                    com.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = Session["NTYPEVISITFORMID"] + "";
                                    com.Parameters.Add(":NGROUPID", OracleType.Number).Value = cboGroup.Value;
                                    com.Parameters.Add(":SVISITFORMNAME", OracleType.VarChar).Value = txtTopicQ.Text;
                                    com.Parameters.Add(":CACTIVE", OracleType.Char).Value = rblStatus3.Value;
                                    com.Parameters.Add(":NWEIGHT", OracleType.Number).Value = txtWeight.Text;
                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.ExecuteNonQuery();

                                }
                            }

                        }

                    }
                    ClearVisiForm();
                    gvwTopicQ.DataBind();
                    break;

                case "edit1":
                    int index = int.Parse(paras[1]);
                    txtRunNumber.Text = gvw.GetRowValues(index, "NNO") + "";
                    txtGroupID.Text = gvw.GetRowValues(index, "NGROUPID") + "";
                    txtProb.Text = gvw.GetRowValues(index, "SGROUPNAME") + "";
                    rblStatus2.Value = gvw.GetRowValues(index, "CACTIVE") + "";
                    break;

                case "delete1":
                    if (txtDelIndex1.Text != "")
                    {
                        string NGROUPID = gvw.GetRowValues(int.Parse(txtDelIndex1.Text), "NGROUPID") + "";

                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();
                            string strDel = "DELETE FROM TGROUPOFVISITFORM WHERE NGROUPID = " + NGROUPID;
                            using (OracleCommand com = new OracleCommand(strDel, con))
                            {
                                com.ExecuteNonQuery();
                            }
                        }
                        gvw.DataBind();
                    }
                    break;

                case "edit2":
                    int index1 = int.Parse(paras[1]);
                    cboGroup.Value = gvwTopicQ.GetRowValues(index1, "NGROUPID") + "";
                    txtTopicQ.Text = gvwTopicQ.GetRowValues(index1, "SVISITFORMNAME") + "";
                    txtVisitformID.Text = gvwTopicQ.GetRowValues(index1, "NVISITFORMID") + "";
                    rblStatus3.Value = gvwTopicQ.GetRowValues(index1, "CACTIVE") + "";
                    txtWeight.Text = gvwTopicQ.GetRowValues(index1, "NWEIGHT") + "";
                    break;

                case "delete2":
                    if (txtDelIndex1.Text != "")
                    {
                        int indexD2 = int.Parse(txtDelIndex1.Text);
                        string NVISITFORMID = gvwTopicQ.GetRowValues(indexD2, "NVISITFORMID") + "";

                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();
                            string strDel = "DELETE FROM TVISITFORM WHERE NVISITFORMID = " + NVISITFORMID;
                            using (OracleCommand com = new OracleCommand(strDel, con))
                            {
                                com.ExecuteNonQuery();
                            }
                        }
                        gvwTopicQ.DataBind();
                    }
                    break;

                case "AddClick":
                    var sdt1 = (List<DT1>)Session["dt1"];

                    if (sdt1.Count > 0)
                    {
                        int id = sdt1.Count;
                        sdt1.Clear();

                        int indexgvw = sgvw.VisibleRowCount - 1;

                        for (int i = 0; i <= indexgvw; i++)
                        {
                            sdt1.Add(new DT1
                            {
                                dtsID = Convert.ToInt32(sgvw.GetRowValues(i, "dtsID")),
                                dtsScoreName = ((ASPxTextBox)sgvw.FindRowCellTemplateControl(i, null, "txtScoreName")).Text + "",
                                dtcScore = ((ASPxTextBox)sgvw.FindRowCellTemplateControl(i, null, "txtScore")).Text + ""
                            });
                        }
                    }

                    int? maxQ = (sdt1.Count > 0) ? sdt1.Select(s => s.dtsID).OrderByDescending(o => o).First() + 1 : 1;

                    sdt1.Add(new DT1
                    {
                        dtsID = maxQ,
                        dtsScoreName = "",
                        dtcScore = ""
                    });

                    sgvw.DataSource = sdt1;
                    sgvw.DataBind();

                    Session["dt1"] = sdt1;
                    break;

                case "deleteList":

                    if (sgvw.VisibleRowCount > 1)
                    {
                        var delsdt1 = (List<DT1>)Session["dt1"];
                        delsdt1.RemoveAt(Convert.ToInt32(paras[1]));

                        sgvw.DataSource = delsdt1;
                        sgvw.DataBind();

                        Session["dt1"] = delsdt1;
                    }

                    break;
            }
        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }

    }

    void listData()
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();

        dt = CommonFunction.Get_Data(sql, "SELECT V.STYPEVISITFORMNAME,V.DUSE,V.CACTIVE FROM TTYPEOFVISITFORM v WHERE v.NTYPEVISITFORMID = " + Session["NTYPEVISITFORMID"] + "");
        dt1 = CommonFunction.Get_Data(sql, "SELECT VS.NTYPEVISITLISTID,VS.STYPEVISITLISTNAME,VS.NTYPEVISITLISTSCORE FROM TTYPEOFVISITFORMLIST vs WHERE vs.NTYPEVISITFORMID = " + Session["NTYPEVISITFORMID"] + " ORDER BY VS.NTYPEVISITLISTID");

        if (dt.Rows.Count > 0 && dt1.Rows.Count > 0)
        {
            txtExam1.Text = dt.Rows[0]["STYPEVISITFORMNAME"] + "";
            dteDateStart1.Date = Convert.ToDateTime(dt.Rows[0]["DUSE"] + "");
            rblStatus1.Value = dt.Rows[0]["CACTIVE"] + "";

            List<DT1> data2 = new List<DT1>(dt1.Rows.Count);

            foreach (DataRow dr1 in dt1.Rows)
            {
                data2.Add(new DT1
                {
                    dtsID = Convert.ToInt32(dr1["NTYPEVISITLISTID"] + ""),
                    dtsScoreName = dr1["STYPEVISITLISTNAME"] + "",
                    dtcScore = dr1["NTYPEVISITLISTSCORE"] + ""
                });
            }

            txtSave.Text = "1";

            sgvw.DataSource = data2;
            sgvw.DataBind();

            Session["dt1"] = data2;
        }

        else
        {
            Response.Redirect("admin_VisitForm_lst.aspx");
        }
    }

    void AutoNumber()
    {
        txtRunNumber.Text = (gvw.VisibleRowCount + 1) + "";
    }

    void ClearGroup()
    {
        txtRunNumber.Text = "";
        txtGroupID.Text = "";
        txtProb.Text = "";
        rblStatus2.SelectedIndex = 0;
    }

    void ClearVisiForm()
    {
        txtTopicQ.Text = "";
        txtVisitformID.Text = "";
        rblStatus3.SelectedIndex = 0;
    }

    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    [Serializable]
    struct DT1
    {
        public int? dtsID { get; set; }
        public string dtsScoreName { get; set; }
        public string dtcScore { get; set; }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}