﻿using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Accident;

public partial class KPI_SUMMARY : PageBase
{
    DataTable dt;
    DataSet ds;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDrowDownList();

            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["str"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                string[] QueryString = decryptedValue.Split('&');

                ddlYear.SelectedValue = QueryString[0];
            }
            //btnSearch_Click(null, null);

            if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
            {
                btnViewMonth.Visible = true;
                btnSearch_Click(null, null);
            }
        }
    }
    #endregion

    #region DrowDownList
    private void SetDrowDownList()
    {
        for (int i = 2015; i < DateTime.Now.Year + 1; i++)
        {
            ddlYear.Items.Add(new ListItem()
            {
                Text = i + string.Empty,
                Value = i + string.Empty
            });
        }
        ddlYear.SelectedValue = DateTime.Now.ToString("yyyy", new CultureInfo("en-US"));
        //ViewState["DataMonth"] = KPI2BLL.Instance.MonthSelect();
        //ddlMonth.DataTextField = "NAME_TH";
        //ddlMonth.DataValueField = "MONTH_ID";
        //ddlMonth.DataSource = ViewState["DataMonth"];
        //ddlMonth.DataBind();
        //ddlMonth.Items.Insert(0, new ListItem()
        //{
        //    Text = "--เลือก--",
        //    Value = "0"
        //});

        ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
        ddlVendor.DataTextField = "SABBREVIATION";
        ddlVendor.DataValueField = "SVENDORID";
        ddlVendor.DataSource = ViewState["DataVendor"];
        ddlVendor.DataBind();
        ddlVendor.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = ""
        });
        if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
        {
            ddlVendor.SelectedValue = Session["SVDID"].ToString();
            ddlVendor.Enabled = false;
            ddlVendor_SelectedIndexChanged(null, null);
        }

        ddlContract.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlContract.SelectedIndex = 0;

        //ViewState["DataStatus"] = KPI2BLL.Instance.KPIStatus("KPI");
        //ddlStatus.DataTextField = "STATUS_NAME";
        //ddlStatus.DataValueField = "STATUS_VALUE";
        //ddlStatus.DataSource = ViewState["DataStatus"];
        //ddlStatus.DataBind();
        //ddlStatus.Items.Insert(0, new ListItem()
        //{
        //    Text = "--เลือก--",
        //    Value = "0"
        //});

    }

    #region ddlVendor_SelectedIndexChanged
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlVendor.SelectedIndex > 0)
            {

                dt = AccidentBLL.Instance.ContractSelect(ddlVendor.SelectedValue);
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", true);
            }
            else
            {
                dt = new DataTable();
                dt.Columns.Add("SCONTRACTID", typeof(string));
                dt.Columns.Add("SCONTRACTNO", typeof(string));
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", true);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region ddlContract_SelectedIndexChanged
    protected void ddlContract_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlContract.SelectedIndex > 0)
            {
                //dt = AccidentBLL.Instance.GroupSelect(ddlContract.SelectedValue);
                //if (dt.Rows.Count > 0)
                //{
                //    DataRow dr = dt.Rows[0];
                //    ddlWorkGroup.SelectedValue = dr["WORKGROUPID"] + string.Empty;
                //    ddlWorkGroup_SelectedIndexChanged(null, null);
                //    ddlGroup.SelectedValue = dr["GROUPID"] + string.Empty;
                //}
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion
    #endregion

    #region Btn_Cilck
    #region btnSearch_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            GetData();
            gvKPI.DataSource = dt;
            gvKPI.DataBind();
            lblItem.Text = dt.Rows.Count + string.Empty;
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion

    #region GetData
    private void GetData()
    {
        bool IsPTT = false;
        if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
            IsPTT = false;
        else
            IsPTT = true;

        dt = KPI2BLL.Instance.KPISummarySelect(ddlYear.SelectedValue, ddlVendor.SelectedValue, ddlContract.SelectedValue);

        dt.Columns.Add("KPI_YEAR");

        KPI_Helper Helper = new KPI_Helper();
        decimal KPIYear = 0;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            KPIYear = Helper.GetKPIYear(ddlYear.SelectedValue, dt.Rows[i]["SVENDORID"].ToString(), int.Parse(dt.Rows[i]["SCONTRACTID"].ToString()), IsPTT);
            dt.Rows[i]["KPI_YEAR"] = (KPIYear == 0) ? string.Empty : KPIYear.ToString("0");
        }
    }
    #endregion

    #region btnClear_Click
    protected void btnClear_Click(object sender, EventArgs e)
    {

        ddlVendor.SelectedIndex = 0;
        ddlVendor_SelectedIndexChanged(null, null);
        //ddlMonth.SelectedIndex = 0;
        ddlYear.SelectedValue = DateTime.Now.Year + string.Empty;
        lblItem.Text = "0";
        //ddlStatus.SelectedIndex = 0;
    }
    #endregion

    #region btnExport_Click
    //protected void btnExport_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        GetData();
    //        dt.Columns.Remove("SYS_TIME");
    //        dt.Columns.Remove("REPORT_PTT");
    //        dt.Columns.Remove("REPORTER");
    //        dt.Columns.Remove("VENDOR_REPORT_TIME");
    //        dt.Columns.Remove("SEND_CONSIDER");
    //        dt.Columns.Remove("PTT_APPROVE");
    //        dt.Columns.Remove("INFORMER_NAME");
    //        dt.Columns.Remove("CACTIVE");
    //        dt.Columns.Remove("CREATE_DATE");
    //        dt.Columns.Remove("CREATE_BY");
    //        dt.Columns.Remove("UPDATE_DATE");
    //        dt.Columns.Remove("UPDATE_BY");
    //        dt.Columns.Remove("ACCIDENTTYPE_ID");
    //        dt.Columns.Remove("SHIPMENT_NO");
    //        dt.Columns.Remove("STRUCKID");
    //        dt.Columns.Remove("SVENDORID");
    //        dt.Columns.Remove("GROUPID");
    //        dt.Columns.Remove("SCONTRACTID");
    //        dt.Columns.Remove("SEMPLOYEEID");
    //        dt.Columns.Remove("STERMINALID");
    //        dt.Columns.Remove("LOCATIONS");
    //        dt.Columns.Remove("GPS");
    //        dt.Columns.Remove("PRODUCT_ID");
    //        dt.Columns.Remove("WORKGROUPNAME");
    //        dt.Columns.Remove("TERMINALNAME");
    //        SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
    //        ExcelFile workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/Admin/AccidentFormat.xlsx"));
    //        ExcelWorksheet worksheet = workbook.Worksheets["Accident"];
    //        worksheet.InsertDataTable(dt, new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

    //        string Path = this.CheckPath();
    //        string FileName = "Accident_Report_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xlsx";

    //        workbook.Save(Path + "\\" + FileName);
    //        this.DownloadFile(Path, FileName);
    //    }
    //    catch (Exception ex)
    //    {
    //        alertFail(ex.Message);
    //    }
    //}

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #endregion

    #region gvKPI_RowUpdating
    protected void gvKPI_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strData = gvKPI.DataKeys[e.RowIndex]["YEAR"] + "&" + gvKPI.DataKeys[e.RowIndex]["SCONTRACTID"] + "&" + gvKPI.DataKeys[e.RowIndex]["SVENDORID"];
        byte[] plaintextBytes = Encoding.UTF8.GetBytes(strData);
        string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

        Page.ClientScript.RegisterStartupScript(
   this.GetType(), "OpenWindow", "window.open('KPI_Summary_Detail.aspx?str=" + encryptedValue + "' ,'_blank');", true);
    }
    #endregion

    #region gvKPI_PageIndexChanging
    protected void gvKPI_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvKPI.PageIndex = e.NewPageIndex;
        btnSearch_Click(null, null);
    }
    #endregion

    #region gvKPI_RowDataBound
    protected void gvKPI_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dv = (DataRowView)e.Row.DataItem;
            if (dv != null)
            {
                CheckBox cbResult = (CheckBox)e.Row.FindControl("cbResult");
                if (dv["STATUS_ID"] + string.Empty == "2")
                {
                    cbResult.Enabled = true;
                }
                else
                {
                    cbResult.Enabled = false;
                }
            }
        }
    }
    #endregion

    protected void btnViewMonth_Click(object sender, EventArgs e)
    {
        try
        {
            var plaintextBytes = Encoding.UTF8.GetBytes(string.Empty + "&" + ddlYear.SelectedValue + "&" + DateTime.Now.Month.ToString() + "&" + ddlContract.SelectedValue + "&" + string.Empty);
            var encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

            FormHelper.OpenForm("KPI_Manual.aspx?str=" + encryptedValue, Page);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
}