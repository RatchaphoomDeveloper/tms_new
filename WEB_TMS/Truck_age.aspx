﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    EnableViewState="true" CodeFile="Truck_age.aspx.cs" Inherits="Truck_age"
    StylesheetTheme="Aqua" %>

<%@ Register Src="~/UserControl/cButtonDelete.ascx" TagPrefix="uc2" TagName="cButtonDelete" %>
<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIE" runat="Server">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=11" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .grvHeaderFlag {
            text-align: center !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="form-horizontal">
            <div class="tab-pane fade active in" id="TabGeneral">
                <br />
                <br />
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <script language="javascript" type="text/javascript">
                            function CheckNumeric(e) {

                                if (window.event) // IE 
                                {
                                    if ((e.keyCode < 48 || e.keyCode > 57) & e.keyCode != 8) {
                                        event.returnValue = false;
                                        return false;

                                    }
                                }
                                else { // Fire Fox
                                    if ((e.which < 48 || e.which > 57) & e.which != 8) {
                                        e.preventDefault();
                                        return false;

                                    }
                                }
                            }

                        </script>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i>
                                <asp:Label ID="lblHeaderTab1" runat="server" Text="เงื่อนไขค้นหา"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-2" style="text-align: right">
                                                <asp:Label ID="lblWorkGroup" runat="server" Text="กลุ่มงาน"></asp:Label>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:DropDownList ID="ddlWorkGroup" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlWorkGroup_SelectedIndexChanged"></asp:DropDownList>
                                            </div>
                                             <div class="col-md-2" style="text-align: right">
                                                <asp:Label ID="lblContract" runat="server" Text="เลขที่สัญญา"></asp:Label>
                                            </div>
                                            <div class="col-md-3" style="text-align: left">
                                                <asp:DropDownList ID="ddlContract" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlContract_SelectedIndexChanged"></asp:DropDownList>
                                            </div>
                                            <div class="col-md-2">
                                                &nbsp;
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2" style="text-align: right">
                                                <asp:Label ID="lblVendor" runat="server" Text="ผู้ขนส่ง"></asp:Label>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged"></asp:DropDownList>
                                            </div>
                                           <div class="col-md-2" style="text-align: right">
                                                <asp:Label ID="lblLicense" runat="server" Text="ทะเบียนรถ"></asp:Label>
                                            </div>
                                            <div class="col-md-3" style="text-align: left">
                                                <asp:TextBox ID="txtLicense" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-md-2">
                                                &nbsp;
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2" style="text-align: right">
                                                <asp:Label ID="lblTruckAge" runat="server" Text="อายุรถ"></asp:Label>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:DropDownList ID="ddlTruckAge" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="-- เลือกข้อมูล --"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="ไม่เกิน 1 ปี"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="1 ปี - ไม่เกิน 2 ปี"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="2 ปี - ไม่เกิน 3 ปี"></asp:ListItem>
                                                    <asp:ListItem Value="4" Text="3 ปี - ไม่เกิน 4 ปี"></asp:ListItem>
                                                    <asp:ListItem Value="5" Text="4 ปี - ไม่เกิน 5 ปี"></asp:ListItem>
                                                    <asp:ListItem Value="6" Text="5 ปี - ไม่เกิน 6 ปี"></asp:ListItem>
                                                    <asp:ListItem Value="7" Text="6 ปี - ไม่เกิน 7 ปี"></asp:ListItem>
                                                    <asp:ListItem Value="8" Text="7 ปี - ไม่เกิน 8 ปี"></asp:ListItem>
                                                    <asp:ListItem Value="9" Text="8 ปี ขึ้นไป"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-2" style="text-align: right">
                                                <asp:Label ID="lblDocumentType" runat="server" Text="ประเภทเอกสาร"></asp:Label>
                                            </div>
                                            <div class="col-md-3" style="text-align: left">
                                                 <asp:DropDownList ID="ddlDocumentType" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="" Text="-- เลือกข้อมูล --"></asp:ListItem>
                                                    <asp:ListItem Value="-1" Text="วันที่จดทะเบียนกับขนส่ง"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="เอกสารใบเทียบแป้น"></asp:ListItem>
                                                    <asp:ListItem Value="41" Text="เอกสารใบวัดน้ำ"></asp:ListItem>
                                                    <asp:ListItem Value="344" Text="ธพ.น.2"></asp:ListItem>
                                                    <asp:ListItem Value="343" Text="ธพ.น.3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-2">
                                                &nbsp;
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-2" style="text-align: right">
                                                <asp:Label ID="lbl" runat="server" Text="หมดอายุในอีก(วัน)"></asp:Label>
                                            </div>
                                            <div class="col-md-3" style="text-align: left">
                                                <asp:TextBox ID="TxtTruckExpire" runat="server" CssClass="form-control" placeholder="รับเฉพาะตัวเลข" onkeypress="CheckNumeric(event);"></asp:TextBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: right">
                                                <asp:Label ID="lblIsExpire" runat="server" Text="หมดอายุแล้ว"></asp:Label>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:DropDownList ID="ddlIsExpire" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="" Text="-- เลือกข้อมูล --"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="ยังไม่หมดอายุ"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="หมดอายุ"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-2">
                                                &nbsp;
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-2" style="text-align: right">
                                                <asp:Label ID="lblDateStart" runat="server" Text="วันที่เริ่มต้นเอกสาร"></asp:Label>
                                            </div>
                                            <div class="col-md-3">
                                               <asp:TextBox ID="txtDateStart" runat="server" CssClass="datepicker"></asp:TextBox>
                                            </div>
                                            <div class="col-md-2" style="text-align: right">
                                                <asp:Label ID="lblDateStop" runat="server" Text="วันที่หมดอายุเอกสาร"></asp:Label>
                                            </div>
                                            <div class="col-md-3" style="text-align: left">
                                                <asp:TextBox ID="txtDateEnd" runat="server" CssClass="datepicker"></asp:TextBox>
                                            </div>
                                            <div class="col-md-2">
                                                &nbsp;
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-2" style="text-align: right">
                                                <asp:Label ID="lblPermission" runat="server" Text="การอนุญาตใช้งาน"></asp:Label>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:DropDownList ID="ddlPermission" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            
                                            <div class="col-md-2">
                                                &nbsp;
                                            </div>
                                        </div>

                                        <br />
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer" style="text-align: right">
                                <asp:Button ID="cmdSearch" runat="server" CssClass="btn btn-success" Text="ค้นหา" UseSubmitBehavior="false" OnClick="cmdSearch_Click" />
                                <asp:Button ID="cmdExport" runat="server" CssClass="btn btn-success" Text="Export" UseSubmitBehavior="false" OnClick="cmdExport_Click" />
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i>
                                <asp:Label ID="Label1" runat="server" Text="สรุปอายุเอกสารรถขนส่ง"></asp:Label>
                            </div>

                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <asp:GridView ID="dgvTruck" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                        HorizontalAlign="Center" AutoGenerateColumns="false" OnPreRender="dgvTruck_PreRender"
                                        EmptyDataText="[ ไม่มีข้อมูล ]" AllowPaging="True" OnRowDataBound="dgvTruck_RowDataBound"
                                        OnPageIndexChanging="dgvTruck_PageIndexChanging" DataKeyNames="STRUCKID">
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <Columns>
                                            <asp:BoundField DataField="VENDOR_NAME" HeaderText="ผู้ขนส่ง"></asp:BoundField>
                                            <asp:BoundField DataField="SHEADREGISTERNO" HeaderText="ทะเบียนหัว"></asp:BoundField>
                                            <asp:BoundField DataField="STRAILERREGISTERNO" HeaderText="ทะเบียนหาง"></asp:BoundField>
                                            <asp:BoundField DataField="STATUS_NAME" HeaderText="สถานะ"></asp:BoundField>
                                            <asp:BoundField DataField="CONTRACT_NO" HeaderText="เลขที่สัญญา"></asp:BoundField>
                                            <asp:BoundField DataField="TRUCK_TYPE_NAME" HeaderText="ประเภทรถ"></asp:BoundField>
                                            <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทเอกสาร"></asp:BoundField>
                                            <asp:BoundField DataField="DOC_NUMBER" HeaderText="หมายเลขเอกสาร"></asp:BoundField>
                                            <asp:BoundField DataField="DOC_YEAR" HeaderText="ปี"></asp:BoundField>
                                            <asp:BoundField DataField="START_DATE" HeaderText="วันที่เริ่มต้นเอกสาร"></asp:BoundField>
                                            <asp:BoundField DataField="STOP_DATE" HeaderText="วันที่สิ้นสุดเอกสาร"></asp:BoundField>
                                            <asp:BoundField DataField="IS_EXPIRE" HeaderText="" Visible="false"></asp:BoundField>
                                            <asp:BoundField DataField="DREGISTER" HeaderText="ตรวจสอบอายุ"></asp:BoundField>
                                        </Columns>
                                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>

                                        <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                        <PagerStyle CssClass="pagination-ys" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="cmdExport" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
