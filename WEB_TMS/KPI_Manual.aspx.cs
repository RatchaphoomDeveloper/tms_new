﻿using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Accident;

public partial class KPI_Manual : PageBase
{
    #region + View State +
    private DataTable dt
    {
        get
        {
            if ((DataTable)ViewState["dt"] != null)
                return (DataTable)ViewState["dt"];
            else
                return null;
        }
        set
        {
            ViewState["dt"] = value;
        }
    }
    #endregion

    DataSet ds;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDrowDownList();
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["str"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                string[] QueryString = decryptedValue.Split('&');

                ddlYear.SelectedValue = QueryString[1];
                ddlMonth.SelectedValue = QueryString[2];
                ddlVendor.SelectedValue = QueryString[4];
                ddlVendor_SelectedIndexChanged(null, null);
                ddlContract.SelectedValue = QueryString[3];
            }
            btnSearch_Click(null, null);
            if (Session["chkurl"] != null && Session["chkurl"] == "1")
            {
                btnImport.Visible = false;
            }
            this.AssignAuthen();
        }
        if (dt != null)
            this.SetColor(ref gvKPI, dt);
    }
    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
                btnSearch.CssClass = "btn btn-md bth-hover btn-info";
                
            }
            if (!CanWrite)
            {
                btnImport.Enabled = false;
                btnImport.CssClass = "btn btn-md bth-hover btn-info";
                gvKPI.Columns[6].Visible = false;
                
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region DrowDownList
    private void SetDrowDownList()
    {
        for (int i = 2015; i < DateTime.Now.Year + 1; i++)
        {
            ddlYear.Items.Add(new ListItem()
            {
                Text = i + string.Empty,
                Value = i + string.Empty
            });
        }
        ddlYear.SelectedValue = DateTime.Now.ToString("yyyy", new CultureInfo("en-US"));

        ViewState["DataMonth"] = KPI2BLL.Instance.MonthSelect();
        ddlMonth.DataTextField = "NAME_TH";
        ddlMonth.DataValueField = "MONTH_ID";
        ddlMonth.DataSource = ViewState["DataMonth"];
        ddlMonth.DataBind();
        ddlMonth.SelectedValue = DateTime.Now.Month.ToString();
        //ddlMonth.Items.Insert(0, new ListItem()
        //{
        //    Text = "--เลือก--",
        //    Value = "0"
        //});

        ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
        ddlVendor.DataTextField = "SABBREVIATION";
        ddlVendor.DataValueField = "SVENDORID";
        ddlVendor.DataSource = ViewState["DataVendor"];
        ddlVendor.DataBind();
        ddlVendor.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlContract.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlContract.SelectedIndex = 0;

        ViewState["DataStatus"] = KPI2BLL.Instance.KPIStatus("KPI");
        ddlStatus.DataTextField = "STATUS_NAME";
        ddlStatus.DataValueField = "STATUS_VALUE";
        ddlStatus.DataSource = ViewState["DataStatus"];
        ddlStatus.DataBind();
        ddlStatus.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "-1"
        });

        if (Session["CGROUP"] != null && string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
        {
            lblVendorRemark.Text = " *ข้อมูล KPI จะแสดงเฉพาะ สัญญาที่แจ้งผลประเมินแล้วเท่านั้น";
            ddlVendor.SelectedValue = Session["SVDID"].ToString();
            ddlVendor.Enabled = false;
            ddlVendor_SelectedIndexChanged(null, null);
            ddlStatus.SelectedValue = "3";
            ddlStatus.Enabled = false;
        }
    }

    #region ddlVendor_SelectedIndexChanged
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlVendor.SelectedIndex > 0)
            {

                dt = AccidentBLL.Instance.ContractSelect(ddlVendor.SelectedValue);
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", true);
            }
            else
            {
                dt = new DataTable();
                dt.Columns.Add("SCONTRACTID", typeof(string));
                dt.Columns.Add("SCONTRACTNO", typeof(string));
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", true);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region ddlContract_SelectedIndexChanged
    protected void ddlContract_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlContract.SelectedIndex > 0)
            {
                //dt = AccidentBLL.Instance.GroupSelect(ddlContract.SelectedValue);
                //if (dt.Rows.Count > 0)
                //{
                //    DataRow dr = dt.Rows[0];
                //    ddlWorkGroup.SelectedValue = dr["WORKGROUPID"] + string.Empty;
                //    ddlWorkGroup_SelectedIndexChanged(null, null);
                //    ddlGroup.SelectedValue = dr["GROUPID"] + string.Empty;
                //}
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion
    #endregion

    #region Btn_Cilck
    #region btnSearch_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            GetData();
            DataRow[] drs = dt.Select("STATUS_ID= " + ddlStatus.SelectedValue + " OR " + ddlStatus.SelectedValue + "=-1");
            if (drs.Any())
            {
                dt = drs.CopyToDataTable();
            }
            else
            {
                dt.Clear();
            }
            gvKPI.DataSource = dt;
            gvKPI.DataBind();

            if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
            {
                btnImport.Visible = false;

                LinkButton lnk;
                for (int i = 0; i < gvKPI.Rows.Count; i++)
                {
                    lnk = (LinkButton)gvKPI.Rows[i].FindControl("lnkChooseEmp");
                    if (lnk != null)
                        lnk.Text = "ดูผลประเมิน KPI";
                }
            }
            //if (!CanWrite){
            //    LinkButton lnk;
            //    for (int i = 0; i < gvKPI.Rows.Count; i++)
            //    {
            //        lnk = (LinkButton)gvKPI.Rows[i].FindControl("lnkChooseEmp");
            //        if (lnk != null)
            //        {
            //           lnk.Enabled = false;
            //        }
            //    }
            //}

            lblItem.Text = ddlYear.SelectedValue;

            this.SetColor(ref gvKPI, dt);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }

    public void SetColor(ref GridView dgvSource, DataTable dataSource)
    {
        try
        {
            string ColorName = string.Empty;
            var converter = System.ComponentModel.TypeDescriptor.GetConverter(typeof(Color));
            Label lbl = new Label();
            for (int i = 0; i < dgvSource.Rows.Count; i++)
            {
                lbl = (Label)dgvSource.Rows[i].FindControl("lblRowColor");
                if (lbl != null)
                {
                    if (!string.Equals(lbl.Text.Replace("&nbsp;", string.Empty), string.Empty))
                    {
                        ColorName = lbl.Text;
                        //dgvSource.Rows[i].ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblRowNumber");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblVendorName");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblContractNo");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblStatusName");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region GetData
    private void GetData()
    {
        ds = KPI2BLL.Instance.KPIScoreSelect(ddlYear.SelectedValue, ddlMonth.SelectedValue, ddlVendor.SelectedValue, ddlContract.SelectedValue);

        if (ds.Tables.Count > 0)
        {
            dt = ds.Tables["CONTRACT"];
            if (dt != null && dt.Rows.Count > 0)
            {
                DataTable dtKPI = ds.Tables["KPI"];
                if (dtKPI != null && dtKPI.Rows.Count > 0)
                {
                    for (int i = 0; i < dtKPI.Rows.Count; i++)
                    {
                        DataRow[] drs = dt.Select("SCONTRACTID = " + dtKPI.Rows[i]["SCONTRACTID"]);
                        if (drs.Any())
                        {
                            DataRow dr = drs[0];
                            dr["STATUS_ID"] = dtKPI.Rows[i]["STATUS_ID"];
                            dr["STATUS_NAME"] = dtKPI.Rows[i]["STATUS_NAME"];
                            dr["KPI_HEADER_ID"] = dtKPI.Rows[i]["KPI_HEADER_ID"];
                            dr["ROW_COLOR"] = dtKPI.Rows[i]["ROW_COLOR"];
                        }
                    }
                }
            }
        }
    }
    #endregion

    #region btnClear_Click
    protected void btnClear_Click(object sender, EventArgs e)
    {

        ddlVendor.SelectedIndex = 0;
        ddlVendor_SelectedIndexChanged(null, null);
        ddlMonth.SelectedIndex = 0;
        ddlYear.SelectedValue = DateTime.Now.Year + string.Empty;
        lblItem.Text = "0";
    }
    #endregion

    #region btnExport_Click
    //protected void btnExport_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        GetData();
    //        dt.Columns.Remove("SYS_TIME");
    //        dt.Columns.Remove("REPORT_PTT");
    //        dt.Columns.Remove("REPORTER");
    //        dt.Columns.Remove("VENDOR_REPORT_TIME");
    //        dt.Columns.Remove("SEND_CONSIDER");
    //        dt.Columns.Remove("PTT_APPROVE");
    //        dt.Columns.Remove("INFORMER_NAME");
    //        dt.Columns.Remove("CACTIVE");
    //        dt.Columns.Remove("CREATE_DATE");
    //        dt.Columns.Remove("CREATE_BY");
    //        dt.Columns.Remove("UPDATE_DATE");
    //        dt.Columns.Remove("UPDATE_BY");
    //        dt.Columns.Remove("ACCIDENTTYPE_ID");
    //        dt.Columns.Remove("SHIPMENT_NO");
    //        dt.Columns.Remove("STRUCKID");
    //        dt.Columns.Remove("SVENDORID");
    //        dt.Columns.Remove("GROUPID");
    //        dt.Columns.Remove("SCONTRACTID");
    //        dt.Columns.Remove("SEMPLOYEEID");
    //        dt.Columns.Remove("STERMINALID");
    //        dt.Columns.Remove("LOCATIONS");
    //        dt.Columns.Remove("GPS");
    //        dt.Columns.Remove("PRODUCT_ID");
    //        dt.Columns.Remove("WORKGROUPNAME");
    //        dt.Columns.Remove("TERMINALNAME");
    //        SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
    //        ExcelFile workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/Admin/AccidentFormat.xlsx"));
    //        ExcelWorksheet worksheet = workbook.Worksheets["Accident"];
    //        worksheet.InsertDataTable(dt, new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

    //        string Path = this.CheckPath();
    //        string FileName = "Accident_Report_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xlsx";

    //        workbook.Save(Path + "\\" + FileName);
    //        this.DownloadFile(Path, FileName);
    //    }
    //    catch (Exception ex)
    //    {
    //        alertFail(ex.Message);
    //    }
    //}

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    #endregion

    #region gvKPI_RowUpdating
    protected void gvKPI_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strData = gvKPI.DataKeys[e.RowIndex]["KPI_HEADER_ID"] + "&" + gvKPI.DataKeys[e.RowIndex]["YEAR"] + "&" + gvKPI.DataKeys[e.RowIndex]["MONTH_ID"] + "&" + gvKPI.DataKeys[e.RowIndex]["SCONTRACTID"] + "&" + gvKPI.DataKeys[e.RowIndex]["SVENDORID"];
        byte[] plaintextBytes = Encoding.UTF8.GetBytes(strData);
        string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

        Page.ClientScript.RegisterStartupScript(
   this.GetType(), "OpenWindow", "window.open('KPI_Manual_AddEdit.aspx?str=" + encryptedValue + "','_blank');", true);
    }
    #endregion

    #region gvKPI_PageIndexChanging
    protected void gvKPI_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvKPI.PageIndex = e.NewPageIndex;
        btnSearch_Click(null, null);
    }
    #endregion

    protected void btnImport_Click(object sender, EventArgs e)
    {
        Response.Redirect("KPI_Import.aspx");
    }
}