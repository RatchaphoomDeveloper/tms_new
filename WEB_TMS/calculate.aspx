﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="calculate.aspx.cs" Inherits="calculate" %>

<%@ Register Src="DocumentCharges.ascx" TagName="UserControl" TagPrefix="myUsr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <%--คำนวนค่า --%>
    <script type="text/javascript">
        function Calculate() {

            var sRow = txtgvwrow.GetValue();
            txtstoploop.SetText('N');
            for (var i = 0; i < sRow; i++) {

                var Vs = gvw.GetRowValues(i, 'SID;SERVICECHARGE', OnGetRowValues);
                if (txtstoploop.GetValue() == 'Y') {
                    return false;
                }
            }

        }



        function OnGetRowValues(values) {
            var Cause = cboCause_CIN.GetValue();
            var sID = rblCapacity_CIN.GetValue() + rblCarspecies_CIN.GetValue() + rblCartype_CIN.GetValue();
            if (Cause == "00002") {
                sID = rblCapacity_CIN.GetValue() + "0" + rblCartype_CIN.GetValue();
            }

            //alert(sID);


            var CV = values.toString();
            var _item = CV.split(',');
            var PanValue = "0";



            if (sID == _item[0]) {
                //หาค่าติดแป้นเพิ่ม
                PanValue = txtSetpan_CIN.GetValue() * txtpan_CIN.GetValue();
                Result = format(parseInt(_item[1]) + parseInt(CheckNullInJAVA(PanValue)));

                lblTotal_CIN.SetText(Result);
                txtstoploop.SetText('Y');
            }

        }

        function Calculate2() {
            var Result = "0";
            var PanValue = "0";
            var CarSpecies = rblCarspecies_CIN.GetValue();
            var rblCartype = rblCartype_CIN.GetValue();
            var rblCapacity = rblCapacity_CIN.GetValue();

            var ReqType = cboReqType_CIN.GetValue();
            var Cause = cboCause_CIN.GetValue();
            //            alert(ReqType);
            //            alert(Cause);
            if (ReqType == "01") {
                if (Cause == "00001") {
                    //เช็คว่าคือรถประเภทไหน รถใหม่หรือรถเก่า
                    if (CarSpecies == "0") {

                        //เช็คว่าปริมาณน้ำเกิน 16000 หรือ ไม่เกิน 16000
                        if (rblCapacity == "0") {
                            if (rblCartype == "0") {
                                Result = "2500";
                            }
                            else if (rblCartype == "1") {
                                Result = "3750";
                            }
                            else {
                                Result = "5000";
                            }
                        }
                        else {
                            if (rblCartype == "0") {
                                Result = "5500";
                            }
                            else if (rblCartype == "1") {
                                Result = "8250";
                            }
                            else {
                                Result = "11000";
                            }
                        }

                    }
                    else {

                        //เช็คว่าปริมาณน้ำเกิน 16000 หรือ ไม่เกิน 16000
                        if (rblCapacity == "0") {
                            if (rblCartype == "0") {
                                Result = "5000";
                            }
                            else if (rblCartype == "1") {
                                Result = "5000";
                            }
                            else {
                                Result = "5000";
                            }
                        }
                        else {
                            if (rblCartype == "0") {
                                Result = "11000";
                            }
                            else if (rblCartype == "1") {
                                Result = "11000";
                            }
                            else {
                                Result = "11000";
                            }
                        }

                    }

                }
                else if (Cause == "00002" || Cause == "00003" || Cause == "00004" || Cause == "00005") {

                    if (Cause == "00002") {
                        //เช็คกรณีที่รถเกิดอุบัตติเหตุ 
                        var sCaseAc = rblAccident_CIN.GetValue();
                        if (sCaseAc == "0") {
                            Result = "1000";
                        }
                        else {
                            //เช็คว่าปริมาณน้ำเกิน 16000 หรือ ไม่เกิน 16000
                            if (rblCapacity == "0") {
                                if (rblCartype == "0") {
                                    Result = "5000";
                                }
                                else if (rblCartype == "1") {
                                    Result = "5000";
                                }
                                else {
                                    Result = "5000";
                                }
                            }
                            else {
                                if (rblCartype == "0") {
                                    Result = "11000";
                                }
                                else if (rblCartype == "1") {
                                    Result = "11000";
                                }
                                else {
                                    Result = "11000";
                                }
                            }
                        }
                    }
                    else {
                        //กรณีรถาอยุเกิน 8 ปี กรณีเปลี่ยนเข้าของ กรณีสลับหัวหาง
                        //เช็คว่าปริมาณน้ำเกิน 16000 หรือ ไม่เกิน 16000
                        if (rblCapacity == "0") {
                            if (rblCartype == "0") {
                                Result = "5000";
                            }
                            else if (rblCartype == "1") {
                                Result = "5000";
                            }
                            else {
                                Result = "5000";
                            }
                        }
                        else {
                            if (rblCartype == "0") {
                                Result = "11000";
                            }
                            else if (rblCartype == "1") {
                                Result = "11000";
                            }
                            else {
                                Result = "11000";
                            }
                        }
                    }


                }


                //หาค่าติดแป้นเพิ่ม
                PanValue = txtSetpan_CIN.GetValue() * txtpan_CIN.GetValue();
                Result = parseInt(Result) + parseInt(CheckNullInJAVA(PanValue));

            }
            else if (ReqType == "02") {
                if (Cause == "00006") {
                    Result = "1000";
                }
                else if (Cause == "00007") {
                    Result = "1000";
                }
                else if (Cause == "00008") {
                    Result = "1000";
                }
                else if (Cause == "00009") {
                    Result = "1000";
                }

            }
            else if (ReqType == "03") {
                Result = "500";
            }
            else if (ReqType == "04") {
                Result = "500";
            }
            //alert(Result);



            lblTotal_CIN.SetText(Result);
        }

    </script>
    <script type="text/javascript">
        //กรณีอุบัตติเหตุ วัดน้ำใหม่
        function Acident(Value) {

            if (Value == "0") {

                $("table #cph_Main_xcpn_trCartype").hide();
                $("table #cph_Main_xcpn_trCapacity").hide();
                $("table #cph_Main_xcpn_trSetpan").hide();
                txtSetpan_CIN.SetText('');
                lblTotal_CIN.SetText(format(1000));
            }
            else {
                $("table #cph_Main_xcpn_trCartype").show();
                $("table #cph_Main_xcpn_trCapacity").show();
                $("table #cph_Main_xcpn_trSetpan").show();
                lblTotal_CIN.SetText('-');
            }

        }


    </script>
    <%--ฟังชั่นแปลงข้อมูลต่างๆ --%>
    <script type="text/javascript">
        function CheckNullInJAVA(Value) {
            var Result = "0";

            if (Value) {
                Result = Value;

            }
            return Result;
        }

        function format(num) {
            var sValue = num.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
            var result = "";
            var chkZeroZero = sValue.split(".");
            if (chkZeroZero[1] == "00") {
                result = sValue.replace('.00', '');
            }
            else {
                result = sValue;
            }


            return result;
        }
    </script>
    <%--ฟังชั่นการซ่อนหรือ Distextbox --%>
    <script type="text/javascript">
        function SetPanEnable(StatusCheck) {
            if (StatusCheck == "Checked") {

                txtSetpan_CIN.SetEnabled(true);
                Calculate();
            }
            else {
                txtSetpan_CIN.SetEnabled(false);
                txtSetpan_CIN.SetText('');
                Calculate();
            }
        }

       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" OnCallback="xcpn_Callback"
        ClientInstanceName="xcpn" CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" border="0" cellpadding="5" cellspacing="1">
                    <tr>
                        <td height="35" colspan="3" align="right">
                        <myUsr:UserControl ID="myUserControl" runat="server"></myUsr:UserControl>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="0" cellpadding="5" cellspacing="1" width="100%">
                                <tr>
                                    <td width="30%" align="center" bgcolor="#B9EAEF">ประเภทคำขอ</td>
                                    <td width="70%">
                                        <dx:ASPxComboBox runat="server" ID="cboReqType" ClientInstanceName="cboReqType_CIN"
                                            DataSourceID="sdsReqType" TextField="REQTYPE_NAME" ValueField="REQTYPE_ID">
                                            <ClientSideEvents ValueChanged="function(s,e){  xcpn.PerformCallback('ChangeReqType;'+s.GetValue());  }" />
                                            <ValidationSettings ValidationGroup="Cal" RequiredField-IsRequired="true" RequiredField-ErrorText="ระบุประเภทคำขอ"
                                                Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                                <RequiredField IsRequired="True" ErrorText="ระบุประเภทคำขอ"></RequiredField>
                                            </ValidationSettings>
                                        </dx:ASPxComboBox>
                                        <asp:SqlDataSource ID="sdsReqType" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                            ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                            SelectCommand="SELECT &quot;REQTYPE_ID&quot;, &quot;REQTYPE_NAME&quot;, &quot;ISACTIVE_FLAG&quot; FROM &quot;TBL_REQTYPE&quot; WHERE (&quot;ISACTIVE_FLAG&quot; = :ISACTIVE_FLAG)">
                                            <SelectParameters>
                                                <asp:Parameter DefaultValue="Y" Name="ISACTIVE_FLAG" Type="String" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                                <tr runat="server" id="trCause">
                                    <td align="center" bgcolor="#B9EAEF">สาเหตุ</td>
                                    <td>
                                        <dx:ASPxComboBox runat="server" ID="cboCause" ClientInstanceName="cboCause_CIN" TextField="CAUSE_NAME"
                                            ValueField="CAUSE_ID">
                                            <ClientSideEvents ValueChanged="function (s, e) { xcpn.PerformCallback('ChangeCause'); }">
                                            </ClientSideEvents>
                                            <ValidationSettings ValidationGroup="Cal" RequiredField-IsRequired="true" RequiredField-ErrorText="ระบุสาเหตุ"
                                                Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                                <RequiredField IsRequired="True" ErrorText="ระบุสาเหตุ"></RequiredField>
                                            </ValidationSettings>
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr id="tracc" runat="server">
                                    <td align="center" bgcolor="#B9EAEF">ระดับความรุนแรง / เสียหาย</td>
                                    <td>
                                        <dx:ASPxRadioButtonList runat="server" ID="rblAccident" Border-BorderWidth="0" ClientInstanceName="rblAccident_CIN">
                                            <Items>
                                                <dx:ListEditItem Text="กรณีถัง / ชัชซีย์ไม่ชำรุด การแก้ไขไม่กระทบความจุถัง / ความเอียงรถ"
                                                    Value="0" />
                                                <dx:ListEditItem Text="ถังชำรุด / ชัชซีย์เสียหาย การแก้ไข กระทบความจุถัง / ความเอียงของรถ"
                                                    Value="1" />
                                            </Items>
                                            <ClientSideEvents ValueChanged="function (s, e) { Acident(s.GetValue()); }"></ClientSideEvents>
                                            <Border BorderWidth="0px"></Border>
                                        </dx:ASPxRadioButtonList>
                                    </td>
                                </tr>
                                <tr runat="server" id="trCarspecies">
                                    <td align="center" bgcolor="#B9EAEF" width="15%">ชนิดรถ</td>
                                    <td width="57%">
                                        <dx:ASPxRadioButtonList runat="server" ID="rblCarspecies" RepeatDirection="Horizontal"
                                            ClientInstanceName="rblCarspecies_CIN" SkinID="rblStatus">
                                            <Items>
                                                <dx:ListEditItem Text="รถใหม่ (ไม่มีประวัติวัดน้ำ)" Value="1" />
                                                <dx:ListEditItem Text="รถเก่า" Value="0" />
                                            </Items>
                                            <ClientSideEvents ValueChanged="function(){ Calculate(); }" />
                                        </dx:ASPxRadioButtonList>
                                    </td>
                                </tr>
                                <tr runat="server" id="trCartype">
                                    <td align="center" bgcolor="#B9EAEF">ประเภทรถ</td>
                                    <td>
                                        <dx:ASPxRadioButtonList runat="server" ID="rblCartype" RepeatDirection="Horizontal"
                                            ClientInstanceName="rblCartype_CIN" SkinID="rblStatus" DataSourceID="sdsCar"
                                            TextField="CARCATE_NAME" ValueField="CARCATE_ID">
                                            <%-- <Items>
                                    <dx:ListEditItem Text="รถลูกค้า" Value="0" />
                                    <dx:ListEditItem Text="รถรับจ้างขนส่ง" Value="1" />
                                    <dx:ListEditItem Text="รถอื่นๆ" Value="2" />
                                </Items>--%>
                                            <ClientSideEvents ValueChanged="function(){ Calculate(); }" />
                                        </dx:ASPxRadioButtonList>
                                        <asp:SqlDataSource ID="sdsCar" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                            ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                            SelectCommand="SELECT CARCATE_ID,CARCATE_NAME, ISACTIVE_FLAG, ORDER_CAR FROM TBL_CARCATE WHERE (ISACTIVE_FLAG = :ISACTIVE_FLAG) ORDER BY ORDER_CAR ASC">
                                            <SelectParameters>
                                                <asp:Parameter DefaultValue="Y" Name="ISACTIVE_FLAG" Type="String" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                                <tr runat="server" id="trCapacity">
                                    <td align="center" bgcolor="#B9EAEF">ขนาดความจุ</td>
                                    <td>
                                        <dx:ASPxRadioButtonList runat="server" ID="rblCapacity" RepeatDirection="Horizontal"
                                            ClientInstanceName="rblCapacity_CIN" SkinID="rblStatus" DataSourceID="sdsCapacity"
                                            TextField="detail" ValueField="NROWADD">
                                            <%-- <Items>
                                    <dx:ListEditItem Text="ไม่เกิน 16,000 ลิตร" Value="0" />
                                    <dx:ListEditItem Text="เกิน 16,000 ลิตร" Value="1" />
                                </Items>--%>
                                            <ClientSideEvents ValueChanged="function(){ Calculate(); }" />
                                        </dx:ASPxRadioButtonList>
                                        <asp:SqlDataSource ID="sdsCapacity" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                            ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                            SelectCommand="
SELECT  SERVICE_ID, REQTYPE_ID, 
TO_CHAR(START_CAPACITY, '9,999,999,999') ||' - '||TO_CHAR(END_CAPACITY, '9,999,999,999')||' '||UNIT as detail, 
 NROWADD FROM TBL_SERVICECOST
WHERE ISACTIVE_FLAG = 'Y' AND SERVICE_ID = '00001'
GROUP BY  SERVICE_ID, REQTYPE_ID, START_CAPACITY, END_CAPACITY, NROWADD,UNIT
ORDER BY NROWADD"></asp:SqlDataSource>
                                    </td>
                                </tr>
                                <tr runat="server" id="trSetpan">
                                    <td align="center" bgcolor="#B9EAEF">การติดแป้นเพิ่ม</td>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td width="22%">
                                                    <dx:ASPxCheckBox runat="server" ID="chbSetpan" ClientInstanceName="chbSetpan_CIN"
                                                        Text="ติดเพิ่มจำนวน" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents CheckedChanged="function(s,e){SetPanEnable(s.GetCheckState());}" />
                                                    </dx:ASPxCheckBox>
                                                </td>
                                                <td width="20%">
                                                    <dx:ASPxTextBox runat="server" ID="txtSetpan" ClientInstanceName="txtSetpan_CIN"
                                                        CssClass="dxeLineBreakFix" Width="95%">
                                                        <ValidationSettings Display="dynamic" ErrorDisplayMode="imagewithtooltip" ValidationGroup="cal">
                                                            <RegularExpression ErrorText="ระบุตัวเลขเท่านั้น และค่าที่กำหนดต้องไม่เกิน 30" ValidationExpression="^[0-9]{0,3}$" />
                                                        </ValidationSettings>
                                                        <ClientSideEvents Validation="function(s,e) { if ( s.GetValue() ) { if (s.GetValue() > 30) {$( '.dxEditors_edtError_Aqua' ).attr({ alt: '', title: 'ระบุตัวเลขเท่านั้น และค่าที่กำหนดต้องไม่เกิน 30'}); e.isValid = false; }else {Calculate();  } } else {Calculate();} }" />
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td width="56%">&nbsp;
                                                    <dx:ASPxLabel runat="server" ID="lblSetpanUnit" Text="แป้น" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="display: none;">
                                    <td></td>
                                    <td>
                                        <dx:ASPxGridView runat="server" ID="gvw" SettingsPager-Mode="ShowAllRecords" ClientInstanceName="gvw"
                                            KeyFieldName="SID">
                                            <SettingsPager Mode="ShowAllRecords">
                                            </SettingsPager>
                                        </dx:ASPxGridView>
                                        <dx:ASPxTextBox runat="server" ID="txtgvwrow" ClientInstanceName="txtgvwrow" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxTextBox runat="server" ID="txtstoploop" ClientInstanceName="txtstoploop"
                                            ClientVisible="false">
                                        </dx:ASPxTextBox>
                                        txtpan_CIN
                                        <dx:ASPxTextBox runat="server" ID="txtpan" ClientInstanceName="txtpan_CIN" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="28%" valign="top">
                            <%--  <table width="100%" border="0" align="center" cellpadding="3" cellspacing="1" style="position: relative;
                                z-index: 1000;">--%>
                            <table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td align="center" bgcolor="#B9EAEF">
                                        <dx:ASPxLabel runat="server" ID="lblMaihate">
                                        </dx:ASPxLabel>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td height="35" align="center" valign="middle" bgcolor="#FFFFCC"><strong class="active">
                                        <dx:ASPxLabel runat="server" ID="lblTotal" ClientInstanceName="lblTotal_CIN">
                                        </dx:ASPxLabel>
                                    </strong>&nbsp;บาท </td>
                                </tr>
                                <tr valign="top">
                                    <td align="center" valign="middle" bgcolor="#FFFFCC">*หมายเหตุ เป็นค่าบริการประมาณการ
                                    </td>
                                </tr>
                                <tr valign="top" style="display: none;">
                                    <td height="35" align="center" valign="middle" bgcolor="#FFFFCC">
                                        <dx:ASPxButton runat="server" ID="btnCalculate" Text="คำนวนค่าธรรมเนียม" Width="70%"
                                            AutoPostBack="false" ValidationGroup="Cal">
                                            <ClientSideEvents Click="function(s,e){ if(!ASPxClientEdit.ValidateGroup('Cal')) return false; Calculate();}" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
