﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMS_BLL.Master;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TMS_BLL.Transaction.Complain;
using System.Text;
using EmailHelper;
using System.Globalization;
using System.Web.Security;

public partial class Vehicle_detail : PageBase
{
    #region ViewState    
    public string VendorId
    {
        get
        {
            if (ViewState["VendorId"] != null)
                return (string)ViewState["VendorId"];
            else
                return "";
        }
        set
        {
            ViewState["VendorId"] = value;
        }
    }

    private string SpotStatus
    {
        get
        {
            if ((string)ViewState["SpotStatus"] != null)
                return (string)ViewState["SpotStatus"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["SpotStatus"] = value;
        }
    }
    public string CGROUP
    {
        get
        {
            if (ViewState["CGROUP"] != null)
                return (string)ViewState["CGROUP"];
            else
                return "";
        }
        set
        {
            ViewState["CGROUP"] = value;
        }
    }
    public string STRUCKID
    {
        get
        {
            if (ViewState["STRUCKID"] != null)
                return (string)ViewState["STRUCKID"];
            else
                return "";
        }
        set
        {
            ViewState["STRUCKID"] = value;
        }
    }
    #endregion
    private string CartypeId;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        this.Culture = "en-US";
        this.UICulture = "en-US";
        //Get User expire
        if (Session["UserID"] == null || Session["UserID"] + "" == "")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return;
            
        }
        else
        {
            VendorId = Session["SVDID"].ToString();
            CGROUP = Session["CGROUP"].ToString();
        }

            
        if (!IsPostBack)
        {

            
            
            #region BindData
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["str"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                string str = decryptedValue;
                decryptedBytes = MachineKey.Decode(Request.QueryString["STTRUCKID"], MachineKeyProtection.All);
                decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                STRUCKID = decryptedValue;
                decryptedBytes = MachineKey.Decode(Request.QueryString["CarTypeID"], MachineKeyProtection.All);
                decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                Session["SCARTYPEID"] = decryptedValue;
                CartypeId = string.Equals(Session["SCARTYPEID"], "0") ? "0" : "1";
                Rdo_TypeCar.SelectedValue =CartypeId ;
                Rdo_TypeCar.Enabled = false;

                if (STRUCKID != "") { 
                }
                DataTable dt = VehicleBLL.Instance.LoadDataSelectEdit(STRUCKID);
                VendorId = dt.Rows[0]["STRANSPORTID"].ToString();
                this.initalFrom(CGROUP);
                this.ChoseTypeCar(CartypeId, dt.Rows[0]["SHEADREGISTERNO"].ToString(), dt.Rows[0]["STRAILERREGISTERNO"].ToString());

                if (dt.Rows[0]["CSPACIALCONTRAC"].ToString() == "Y")
                    SpotStatus = "1";
                else
                    SpotStatus = "0";

                this.BindDataInForm(STRUCKID, Rdo_TypeCar.SelectedIndex.ToString(),dt);
                
            }
            #endregion
            
        }
    }

    private void BindDataInForm(string STRUCKID,string TypeCar,DataTable dt)
    {
        
        switch (TypeCar)
        {
            case "0":
                ddlVendorTruck.SelectedValue = dt.Rows[0]["STRANSPORTID"].ToString();
                rdo_contratType.SelectedValue = dt.Rows[0]["CARCATE_ID"].ToString();
                ddl_truck.SelectedValue = dt.Rows[0]["SHEADREGISTERNO"].ToString();
                Truck_contract.SelectedValue = dt.Rows[0]["TMPCONTRACTID"].ToString();
                VEH_TEXT.Text = dt.Rows[0]["VEH_TEXT"].ToString();
                break;
            case "1":
                ddlvendorSemiTruck.SelectedValue = dt.Rows[0]["STRANSPORTID"].ToString();
                rdo_contratType1.SelectedValue = dt.Rows[0]["CARCATE_ID"].ToString();
                truck_head.SelectedValue = dt.Rows[0]["SHEADREGISTERNO"].ToString();
                Truck_Semi.SelectedValue = dt.Rows[0]["STRAILERREGISTERNO"].ToString();
                TruckSemi_contract.SelectedValue =  dt.Rows[0]["TMPCONTRACTID"].ToString();
                semi_veh_text.Text = dt.Rows[0]["VEH_TEXT"].ToString();
                break;
            
        }     


    }
    #region EVENT     
    protected void ChoseTypeCar(string CartypeId, string SHEADREGISTERNO, string STRAILERREGISTERNO)
    {
        DataTable LoadDataContrat = VehicleBLL.Instance.LoadDataContrat(CGROUP, VendorId, SpotStatus);
        switch (CartypeId)
        {
            case "0":
                DataTable dt_truck = VehicleBLL.Instance.LoadDataTruckVehicle(VendorId, SHEADREGISTERNO);
                DropDownListHelper.BindDropDownList(ref ddl_truck, dt_truck, "SHEADREGISTERNO", "SHEADREGISTERNO", false);
                DropDownListHelper.BindDropDownList(ref Truck_contract, LoadDataContrat, "SCONTRACTID", "SCONTRACTNO", false);  
                divtruck.Visible = true;
                divSemi.Visible = false;
                break;
            case "1":
                DataTable dt_headcar = VehicleBLL.Instance.LoadDataHeadVehicle(VendorId,SHEADREGISTERNO);
                DataTable dt_semi = VehicleBLL.Instance.LoadDataSemiVehicle(VendorId, STRAILERREGISTERNO);
                DropDownListHelper.BindDropDownList(ref truck_head, dt_headcar, "SHEADREGISTERNO", "SHEADREGISTERNO", false);
                DropDownListHelper.BindDropDownList(ref Truck_Semi, dt_semi, "SHEADREGISTERNO", "SHEADREGISTERNO", false);
                DropDownListHelper.BindDropDownList(ref TruckSemi_contract, LoadDataContrat, "SCONTRACTID", "SCONTRACTNO", false);
                divSemi.Visible = true;
                divtruck.Visible = false;
                break;
            default:
                divtruck.Visible = false;
                divSemi.Visible = false;
                break;
        }
        
        
    }

    protected void ddlTruckSelectedValue(object sender,EventArgs e)
    {
        this.initalTruck(ddlVendorTruck.SelectedValue);
        
    }

    protected void ddlSemiSelectValue(object sender, EventArgs e)
    {
        this.initalTruck(ddlvendorSemiTruck.SelectedValue);
    }

    protected void initalTruck(string value)
    {
        DataTable LoadDataContrat = VehicleBLL.Instance.LoadDataContrat(CGROUP, value, SpotStatus);
        DataTable dt_classgrp = VehicleBLL.Instance.LoadDataClassgrp();
        switch (Rdo_TypeCar.SelectedIndex)
        {
            case 0:
                DataTable dt_truck = VehicleBLL.Instance.LoadDataTruck(value, SpotStatus);
                DropDownListHelper.BindDropDownList(ref ddl_truck, dt_truck, "STRUCKID", "SHEADREGISTERNO", true);
                DropDownListHelper.BindDropDownList(ref Truck_contract, LoadDataContrat, "SCONTRACTID", "SCONTRACTNO", true);                                
                divtruck.Visible = true;
                divSemi.Visible = false;
                break;
            case 1:
                DataTable dt_headcar = VehicleBLL.Instance.LoadDataHeadCar(value, SpotStatus);
                DataTable dt_semi = VehicleBLL.Instance.LoadDataSemi(value, SpotStatus);
                DropDownListHelper.BindDropDownList(ref truck_head, dt_headcar, "STRUCKID", "SHEADREGISTERNO", true);
                DropDownListHelper.BindDropDownList(ref Truck_Semi, dt_semi, "STRUCKID", "SHEADREGISTERNO", true);
                DropDownListHelper.BindDropDownList(ref TruckSemi_contract, LoadDataContrat, "SCONTRACTID", "SCONTRACTNO", false);
                divSemi.Visible = true;
                divtruck.Visible = false;
                break;
            default:
                divtruck.Visible = false;
                divSemi.Visible = false;
                break;
        }
    }
    protected void TruckVeh_Text(object sender, EventArgs e)
    {
        DataTable LoadGetVeh_Text = VehicleBLL.Instance.LoadGetVeh_Text(ddl_truck.SelectedValue);
        int totalcapacity=0 ;
        string veh_text ="" ;
        for (int i = 0; i < LoadGetVeh_Text.Rows.Count; i++)
			{
			    totalcapacity +=int.Parse(LoadGetVeh_Text.Rows[i]["NCAPACITY"].ToString());
                veh_text+=LoadGetVeh_Text.Rows[i]["VEH_TEXT"].ToString() +",";
			}
        veh_text = veh_text.TrimEnd(',');
        VEH_TEXT.Text = "SU-" + totalcapacity + "(" + veh_text + ")";
    }
    #region คำนวณ VEH_TEXT Semi
    
    
    protected void SemitTruckVeh_Text(object sender, EventArgs e)
    {
        DataTable LoadGetVeh_Text = VehicleBLL.Instance.LoadGetVeh_Text(Truck_Semi.SelectedValue);
        int totalcapacity=0;
        string veh_text = "";
        for (int i = 0; i < LoadGetVeh_Text.Rows.Count; i++)
        {
            totalcapacity += int.Parse(LoadGetVeh_Text.Rows[i]["NCAPACITY"].ToString());
            veh_text += LoadGetVeh_Text.Rows[i]["VEH_TEXT"].ToString() + ",";
        }
        veh_text = veh_text.TrimEnd(',');
        semi_veh_text.Text = "SM-" + totalcapacity + "(" + veh_text + ")";
    }
    #endregion
    

    #endregion
    #region InitalData
    private void initalFrom(string CGROUP)
    {
        DataTable dt_vehicle_car = VehicleBLL.Instance.LoadDataVehicleCar();
        string condition = VendorId;
        DataTable dt_vendor = VendorBLL.Instance.SelectName(condition);
        switch (CGROUP)
        {
            case "1":
                
                DropDownListHelper.BindDropDownList(ref ddlVendorTruck, dt_vendor, "SVENDORID", "SABBREVIATION", true);
                DropDownListHelper.BindDropDownList(ref ddlvendorSemiTruck, dt_vendor, "SVENDORID", "SABBREVIATION", false);
                DropDownListHelper.BindDropDownList(ref rdo_contratType, dt_vehicle_car, "config_value", "config_name", true);
                DropDownListHelper.BindDropDownList(ref rdo_contratType1, dt_vehicle_car, "config_value", "config_name", false);  
            break;
            default:
                
                DropDownListHelper.BindDropDownList(ref ddlVendorTruck, dt_vendor, "SVENDORID", "SABBREVIATION", true);
                DropDownListHelper.BindDropDownList(ref ddlvendorSemiTruck, dt_vendor, "SVENDORID", "SABBREVIATION", false);
                DropDownListHelper.BindDropDownList(ref rdo_contratType, dt_vehicle_car, "config_value", "config_name", true);
                DropDownListHelper.BindDropDownList(ref rdo_contratType1, dt_vehicle_car, "config_value", "config_name", false);
                break;
        }
        
    }
    #endregion
    #region Micellouse
        private Control FindControlRecursive(Control rootControl, string controlID)
        {
            if (rootControl.ID == controlID) return rootControl;

            foreach (Control controlToSearch in rootControl.Controls)
            {
                Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
                if (controlToReturn != null) return controlToReturn;
            }
            return null;
        }
        private string GetConditionVendor()
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                if (ddlVendorTruck.SelectedIndex > 0)
                    sb.Append(" AND TCONTRACT.SVENDORID = '" + Truck_contract.SelectedValue + "'");

                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    #endregion
    
    
    
        #region ResponseMenu
        protected void ResponseMenu(object sender, EventArgs e)
        {
            if (string.Equals(CGROUP.ToString(), "1"))
            {
                Response.Redirect("approve_pk.aspx");
            }
            else
            {
                Response.Redirect("vendor_request.aspx");

            }        
        }
        #endregion
}
    
