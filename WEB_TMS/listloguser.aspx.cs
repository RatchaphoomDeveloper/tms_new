﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using DevExpress.Web.ASPxEditors;
using System.Globalization;

public partial class listloguser : System.Web.UI.Page
{
    string connection = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        cboUserGroup.ItemRequestedByValue += new ListEditItemRequestedByValueEventHandler(cboUserGroup_ItemRequestedByValue);

        if (!IsPostBack)
        {
            dateEditStart.Date = DateTime.Today;
            dateEditEnd.Date = DateTime.Today;
        }
    }

    void cboUserGroup_ItemRequestedByValue(object source, ListEditItemRequestedByValueEventArgs e)
    {
        //throw new NotImplementedException();
    }

    protected void cboSearchNameAndOrgUser_ItemsRequestedByFilterCondition(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        listUserName.SelectCommand = @"SELECT USER_NAME ,ORG ,SUID FROM (SELECT TUSR.SUID,  TUSR.SFIRSTNAME||' '||TUSR.SLASTNAME AS USER_NAME,TUNITCODE.SABBREVIATION AS ORG , ROW_NUMBER()OVER(ORDER BY SUID) AS RN 
                                        FROM TUSER TUSR
                                            LEFT JOIN (
                                            SELECT SVENDORID , SABBREVIATION FROM TVENDOR
                                            UNION ALL
                                            SELECT STERMINALID , SABBREVIATION  FROM TTERMINAL
                                            UNION ALL
                                            SELECT UNITCODE ,UNITNAME FROM TUNIT
                                        ) TUNITCODE ON TUSR.SVENDORID = TUNITCODE.SVENDORID
                                        WHERE TUSR.SFIRSTNAME||' '||TUSR.SLASTNAME || TUNITCODE.SABBREVIATION LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex 
                                        ORDER BY USER_NAME";

        listUserName.SelectParameters.Clear();
        listUserName.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        listUserName.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        listUserName.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = listUserName;
        comboBox.DataBind();
    }

    protected void cboMenu_ItemsRequestedByFilterCondition(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        string[] menu = { "ข้อมูลGRADE","ข้อมูลTIMEWINDOW","ข้อมูลTQUESTIONNAIRE","ข้อมูลTTOPIC","ข้อมูลTVISITFORM","ข้อมูลTVISITOR","ข้อมูลTVISITORITEM","ข้อมูลTVISITORUSER",
                            "ข้อมูลกลุ่มแบบฟอร์มตรวจสอบ","ข้อมูลกลุ่มแบบฟอร์มประเมินผู้ประกอบการ","ข้อมูลกักรถคลังปลายทาง","ข้อมูลการลงคิวงาน","ข้อมูลข้อร้องเรียน","ข้อมูลจัดแผน","ข้อมูลตรวจสภาพรถ",
                            "ข้อมูลตรวจสินค้า","ข้อมูลแบบฟอร์มการตรวจสอบ","ข้อมูลประเภทแบบปอร์มประเมินผูประกอบการ","ข้อมูลประเภทแบบฟอร์มตรวจสอบ","ข้อมูลปัญหาอื่นๆ","ข้อมูลยืนยันรถตามสัญญา",
                            "ข้อมูลรายการปัญหาอื่นๆ","ข้อมูลรายการยืนยันรถตามสัญญา","ข้อมูลวันหยุด","ข้อมูลหัวข้อประเมินผล","ข้อมูลอุทธรณ์","ข้อมูลอุบัติเหตุ" };
        List<String> list = new List<String>();
        foreach (var item in menu)
        {
            list.Add(item);
        }
        comboBox.DataSource = list.Select(s => new { Text = "" + s, Value = "" + s });
        comboBox.DataBind();
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (e.Parameter == "Search")
        {
            bindData();
        }

    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        //Page_Load ทำงานก่อน xcpn_Load
        bindData();
    }

    public void bindData()
    {
        string dateStart = "";
        string dateEnd = "";
        string where = "";
        string menu = "";
        string searchNameAndOrgUser = cboSearchNameAndOrgUser.Value + "";
        string userGroup = cboUserGroup.Value + "";
        string searchMenu = cboSearchMenu.Value + "";

        if (dateEditStart.Value != null && dateEditEnd.Value != null)
        {
            dateStart = dateEditStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            dateEnd = dateEditEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
        }

        #region switch case menu
        switch (searchMenu)
        {
            case "ข้อมูลGRADE": menu = "ข้อมูลGRADE"; break;
            case "ข้อมูลTIMEWINDOW": menu = "ข้อมูลTIMEWINDOW"; break;
            case "ข้อมูลTQUESTIONNAIRE": menu = "ข้อมูลTQUESTIONNAIRE"; break;
            case "ข้อมูลTTOPIC": menu = "ข้อมูลTTOPIC"; break;
            case "ข้อมูลTVISITFORM": menu = "ข้อมูลTVISITFORM"; break;
            case "ข้อมูลTVISITOR": menu = "ข้อมูลTVISITOR"; break;
            case "ข้อมูลTVISITORITEM": menu = "ข้อมูลTVISITORITEM"; break;
            case "ข้อมูลTVISITORUSER": menu = "ข้อมูลTVISITORUSER"; break;
            case "ข้อมูลกลุ่มแบบฟอร์มตรวจสอบ": menu = "ข้อมูลกลุ่มแบบฟอร์มตรวจสอบ"; break;
            case "ข้อมูลกลุ่มแบบฟอร์มประเมินผู้ประกอบการ": menu = "ข้อมูลกลุ่มแบบฟอร์มประเมินผู้ประกอบการ"; break;
            case "ข้อมูลกักรถคลังปลายทาง": menu = "ข้อมูลกักรถคลังปลายทาง"; break;
            case "ข้อมูลการลงคิวงาน": menu = "ข้อมูลการลงคิวงาน"; break;
            case "ข้อมูลข้อร้องเรียน": menu = "ข้อมูลข้อร้องเรียน"; break;
            case "ข้อมูลจัดแผน": menu = "ข้อมูลจัดแผน"; break;
            case "ข้อมูลตรวจสภาพรถ": menu = "ข้อมูลตรวจสภาพรถ"; break;
            case "ข้อมูลตรวจสินค้า": menu = "ข้อมูลตรวจสินค้า"; break;
            case "ข้อมูลแบบฟอร์มการตรวจสอบ": menu = "ข้อมูลแบบฟอร์มการตรวจสอบ"; break;
            case "ข้อมูลประเภทแบบปอร์มประเมินผูประกอบการ": menu = "ข้อมูลประเภทแบบปอร์มประเมินผูประกอบการ"; break;
            case "ข้อมูลประเภทแบบฟอร์มตรวจสอบ": menu = "ข้อมูลประเภทแบบฟอร์มตรวจสอบ"; break;
            case "ข้อมูลปัญหาอื่นๆ": menu = "ข้อมูลปัญหาอื่นๆ"; break;
            case "ข้อมูลยืนยันรถตามสัญญา": menu = "ข้อมูลยืนยันรถตามสัญญา"; break;
            case "ข้อมูลรายการปัญหาอื่นๆ": menu = "ข้อมูลรายการปัญหาอื่นๆ"; break;
            case "ข้อมูลรายการยืนยันรถตามสัญญา": menu = "ข้อมูลรายการยืนยันรถตามสัญญา"; break;
            case "ข้อมูลวันหยุด": menu = "ข้อมูลวันหยุด"; break;
            case "ข้อมูลหัวข้อประเมินผล": menu = "ข้อมูลหัวข้อประเมินผล"; break;
            case "ข้อมูลอุทธรณ์": menu = "ข้อมูลอุทธรณ์"; break;
            case "ข้อมูลอุบัติเหตุ": menu = "ข้อมูลอุบัติเหตุ"; break;
        }
        #endregion

        if (dateEditStart.Value == null && dateEditEnd.Value == null)//(dateStart == "" && dateEnd == "")
        {
            if (userGroup == "1")
            {
                where = "WHERE TEVENTLOG.ผู้ใช้งาน||','||TUNITCODE.SABBREVIATION = NVL('" + searchNameAndOrgUser + "',TEVENTLOG.ผู้ใช้งาน||','||TUNITCODE.SABBREVIATION) AND TUSR.CGROUP = 0 AND TEVENTLOG.เมนู = NVL('" + menu + "',TEVENTLOG.เมนู)";
            }
            else if (userGroup == "2")
            {
                where = "WHERE TEVENTLOG.ผู้ใช้งาน||','||TUNITCODE.SABBREVIATION = NVL('" + searchNameAndOrgUser + "',TEVENTLOG.ผู้ใช้งาน||','||TUNITCODE.SABBREVIATION) AND TUSR.CGROUP = 1 AND TEVENTLOG.เมนู = NVL('" + menu + "',TEVENTLOG.เมนู)";
            }
            else if (userGroup == "3")
            {
                where = "WHERE TEVENTLOG.ผู้ใช้งาน||','||TUNITCODE.SABBREVIATION = NVL('" + searchNameAndOrgUser + "',TEVENTLOG.ผู้ใช้งาน||','||TUNITCODE.SABBREVIATION) AND TUSR.CGROUP = 2 AND TEVENTLOG.เมนู = NVL('" + menu + "',TEVENTLOG.เมนู)";
            }
            else
            {
                where = "WHERE TEVENTLOG.ผู้ใช้งาน||','||TUNITCODE.SABBREVIATION = NVL('" + searchNameAndOrgUser + "',TEVENTLOG.ผู้ใช้งาน||','||TUNITCODE.SABBREVIATION) AND TEVENTLOG.เมนู = NVL('" + menu + "',TEVENTLOG.เมนู)";
            }
        }
        else
        {

            if (userGroup == "1")
            {
                where = "WHERE TO_DATE(TEVENTLOG.วันที่,'DD/MM/YYYY') BETWEEN NVL(TO_DATE('" + dateStart + "','DD/MM/YYYY'),TO_DATE(SYSDATE,'DD/MM/YYYY')) AND NVL(TO_DATE('" + dateEnd + "','DD/MM/YYYY'),TO_DATE(SYSDATE,'DD/MM/YYYY')) AND TEVENTLOG.ผู้ใช้งาน||','||TUNITCODE.SABBREVIATION = NVL('" + searchNameAndOrgUser + "',TEVENTLOG.ผู้ใช้งาน||','||TUNITCODE.SABBREVIATION) AND TUSR.CGROUP = 0 AND TEVENTLOG.เมนู = NVL('" + menu + "',TEVENTLOG.เมนู)";
            }
            else if (userGroup == "2")
            {
                where = "WHERE TO_DATE(TEVENTLOG.วันที่,'DD/MM/YYYY') BETWEEN NVL(TO_DATE('" + dateStart + "','DD/MM/YYYY'),TO_DATE(SYSDATE,'DD/MM/YYYY')) AND NVL(TO_DATE('" + dateEnd + "','DD/MM/YYYY'),TO_DATE(SYSDATE,'DD/MM/YYYY')) AND TEVENTLOG.ผู้ใช้งาน||','||TUNITCODE.SABBREVIATION = NVL('" + searchNameAndOrgUser + "',TEVENTLOG.ผู้ใช้งาน||','||TUNITCODE.SABBREVIATION) AND TUSR.CGROUP = 1 AND TEVENTLOG.เมนู = NVL('" + menu + "',TEVENTLOG.เมนู)";
            }
            else if (userGroup == "3")
            {
                where = "WHERE TO_DATE(TEVENTLOG.วันที่,'DD/MM/YYYY') BETWEEN NVL(TO_DATE('" + dateStart + "','DD/MM/YYYY'),TO_DATE(SYSDATE,'DD/MM/YYYY')) AND NVL(TO_DATE('" + dateEnd + "','DD/MM/YYYY'),TO_DATE(SYSDATE,'DD/MM/YYYY')) AND TEVENTLOG.ผู้ใช้งาน||','||TUNITCODE.SABBREVIATION = NVL('" + searchNameAndOrgUser + "',TEVENTLOG.ผู้ใช้งาน||','||TUNITCODE.SABBREVIATION) AND TUSR.CGROUP = 2 AND TEVENTLOG.เมนู = NVL('" + menu + "',TEVENTLOG.เมนู)";
            }
            else
            {
                where = "WHERE TO_DATE(TEVENTLOG.วันที่,'DD/MM/YYYY') BETWEEN NVL(TO_DATE('" + dateStart + "','DD/MM/YYYY'),TO_DATE(SYSDATE,'DD/MM/YYYY')) AND NVL(TO_DATE('" + dateEnd + "','DD/MM/YYYY'),TO_DATE(SYSDATE,'DD/MM/YYYY')) AND TEVENTLOG.ผู้ใช้งาน||','||TUNITCODE.SABBREVIATION = NVL('" + searchNameAndOrgUser + "',TEVENTLOG.ผู้ใช้งาน||','||TUNITCODE.SABBREVIATION) AND TEVENTLOG.เมนู = NVL('" + menu + "',TEVENTLOG.เมนู)";
            }
        }

        #region sqlQueryLogUser
        string sqlQueryLogUser = @"SELECT ROWNUM ARRANGE, TEVENTLOG.เมนู MENU, TEVENTLOG.วันที่ DATES, TEVENTLOG.กลุ่มผู้ใช้งาน USER_GROUP, TEVENTLOG.ผู้ใช้งาน USER_ACTIVE, TUNITCODE.SABBREVIATION ORG, TEVENTLOG.การทำงาน WORKING
                        FROM TUSER TUSR
                        LEFT JOIN (
                            SELECT SVENDORID , SABBREVIATION FROM TVENDOR
                            UNION ALL
                            SELECT STERMINALID , SABBREVIATION  FROM TTERMINAL
                            UNION ALL
                            SELECT UNITCODE ,UNITNAME FROM TUNIT
                        ) TUNITCODE ON TUSR.SVENDORID = TUNITCODE.SVENDORID
                        LEFT JOIN(
                        --------------------------------------------------------------------------1.-------TACCIDENT
                        SELECT 'ข้อมูลอุบัติเหตุ' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลอุบัติเหตุ'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TACCIDENT.SCREATE SCREATE,MAX(TACCIDENT.DCREATE) DCREATE,
                                    'USER ID : '||TACCIDENT.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TACCIDENT TACCIDENT
                                    LEFT JOIN TUSER TUSERCRE ON TACCIDENT.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TACCIDENT.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TACCIDENT.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TACCIDENT.SUPDATE SUPDATE,MAX(TACCIDENT.DUPDATE) DUPDATE,
                                    'USER ID : '||TACCIDENT.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TACCIDENT TACCIDENT
                                    LEFT JOIN TUSER TUSERCRE ON TACCIDENT.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TACCIDENT.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TACCIDENT.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        --WHERE TO_DATE(LATEST_DATE,'DD/MM/YYYY') = TO_DATE(NVL(LATEST_DATE,''),'DD/MM/YYYY')
                        --WHERE LATEST_DATE = NVL(TO_DATE(LATEST_DATE,'DD/MM/YYYY'),'2') 
                        ----------------------------------------------------------------------1.------END TACCIDENT
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------2.----------LSTHOLIDAY
                        SELECT 'ข้อมูลวันหยุด' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลวันหยุด'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, LSTHOLIDAY.SCREATE SCREATE,MAX(LSTHOLIDAY.DCREATE) DCREATE,
                                    'USER ID : '||LSTHOLIDAY.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM LSTHOLIDAY LSTHOLIDAY
                                    LEFT JOIN TUSER TUSERCRE ON LSTHOLIDAY.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON LSTHOLIDAY.SUPDATE = TUSERUPD.SUID
                                    GROUP BY LSTHOLIDAY.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, LSTHOLIDAY.SUPDATE SUPDATE,MAX(LSTHOLIDAY.DUPDATE) DUPDATE,
                                    'USER ID : '||LSTHOLIDAY.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM LSTHOLIDAY LSTHOLIDAY
                                    LEFT JOIN TUSER TUSERCRE ON LSTHOLIDAY.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON LSTHOLIDAY.SUPDATE = TUSERUPD.SUID
                                    GROUP BY LSTHOLIDAY.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ------------------------------------------------------------------------2.----END LSTHOLIDAY
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------3.--------- LSTMULTIPLEIMPACT
                        ---------------------------------------------------------------------3.-------END LSTMULTIPLEIMPACT
                        --UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------4.--------- TAPPEAL
                        SELECT 'ข้อมูลอุทธรณ์' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลอุทธรณ์'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TAPPEAL.SCREATE SCREATE,MAX(TAPPEAL.DCREATE) DCREATE,
                                    'USER ID : '||TAPPEAL.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TAPPEAL TAPPEAL
                                    LEFT JOIN TUSER TUSERCRE ON TAPPEAL.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TAPPEAL.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TAPPEAL.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TAPPEAL.SUPDATE SUPDATE,MAX(TAPPEAL.DUPDATE) DUPDATE,
                                    'USER ID : '||TAPPEAL.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TAPPEAL TAPPEAL
                                    LEFT JOIN TUSER TUSERCRE ON TAPPEAL.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TAPPEAL.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TAPPEAL.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------4.-------END TAPPEAL
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------5.--------- TCARBAN
                        SELECT 'ข้อมูลกักรถคลังปลายทาง' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลกักรถคลังปลายทาง'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TCARBAN.SCREATE SCREATE,MAX(TCARBAN.DCREATE) DCREATE,
                                    'USER ID : '||TCARBAN.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TCARBAN TCARBAN
                                    LEFT JOIN TUSER TUSERCRE ON TCARBAN.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TCARBAN.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TCARBAN.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TCARBAN.SUPDATE SUPDATE,MAX(TCARBAN.DUPDATE) DUPDATE,
                                    'USER ID : '||TCARBAN.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TCARBAN TCARBAN
                                    LEFT JOIN TUSER TUSERCRE ON TCARBAN.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TCARBAN.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TCARBAN.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------5.-------END TCARBAN
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------6.--------- TCATEGORY
                        SELECT 'ข้อมูลหัวข้อประเมินผล' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลหัวข้อประเมินผล'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TCATEGORY.SCREATE SCREATE,MAX(TCATEGORY.DCREATE) DCREATE,
                                    'USER ID : '||TCATEGORY.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TCATEGORY TCATEGORY
                                    LEFT JOIN TUSER TUSERCRE ON TCATEGORY.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TCATEGORY.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TCATEGORY.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TCATEGORY.SUPDATE SUPDATE,MAX(TCATEGORY.DUPDATE) DUPDATE,
                                    'USER ID : '||TCATEGORY.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TCATEGORY TCATEGORY
                                    LEFT JOIN TUSER TUSERCRE ON TCATEGORY.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TCATEGORY.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TCATEGORY.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------6.-------END TCATEGORY
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------7.--------- TCHECKLIST
                        SELECT 'ข้อมูลแบบฟอร์มการตรวจสอบ' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลแบบฟอร์มการตรวจสอบ'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TCHECKLIST.SCREATE SCREATE,MAX(TCHECKLIST.DCREATE) DCREATE,
                                    'USER ID : '||TCHECKLIST.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TCHECKLIST TCHECKLIST
                                    LEFT JOIN TUSER TUSERCRE ON TCHECKLIST.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TCHECKLIST.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TCHECKLIST.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TCHECKLIST.SUPDATE SUPDATE,MAX(TCHECKLIST.DUPDATE) DUPDATE,
                                    'USER ID : '||TCHECKLIST.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TCHECKLIST TCHECKLIST
                                    LEFT JOIN TUSER TUSERCRE ON TCHECKLIST.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TCHECKLIST.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TCHECKLIST.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------7.-------END TCHECKLIST
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------8.--------- TCHECKPRODUCT
                        SELECT 'ข้อมูลตรวจสินค้า' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลตรวจสินค้า'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TCHECKPRODUCT.SCREATE SCREATE,MAX(TCHECKPRODUCT.DCREATE) DCREATE,
                                    'USER ID : '||TCHECKPRODUCT.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TCHECKPRODUCT TCHECKPRODUCT
                                    LEFT JOIN TUSER TUSERCRE ON TCHECKPRODUCT.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TCHECKPRODUCT.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TCHECKPRODUCT.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TCHECKPRODUCT.SUPDATE SUPDATE,MAX(TCHECKPRODUCT.DUPDATE) DUPDATE,
                                    'USER ID : '||TCHECKPRODUCT.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TCHECKPRODUCT TCHECKPRODUCT
                                    LEFT JOIN TUSER TUSERCRE ON TCHECKPRODUCT.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TCHECKPRODUCT.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TCHECKPRODUCT.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------8.-------END TCHECKPRODUCT
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------9.--------- TCHECKPRODUCTITEM
                        -----------------------------------------------------------------------9.-------END TCHECKPRODUCTITEM
                        --UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------10.--------- TCHECKTRUCK
                        SELECT 'ข้อมูลตรวจสภาพรถ' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลตรวจสภาพรถ'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TCHECKTRUCK.SCREATE SCREATE,MAX(TCHECKTRUCK.DCREATE) DCREATE,
                                    'USER ID : '||TCHECKTRUCK.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TCHECKTRUCK TCHECKTRUCK
                                    LEFT JOIN TUSER TUSERCRE ON TCHECKTRUCK.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TCHECKTRUCK.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TCHECKTRUCK.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TCHECKTRUCK.SUPDATE SUPDATE,MAX(TCHECKTRUCK.DUPDATE) DUPDATE,
                                    'USER ID : '||TCHECKTRUCK.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TCHECKTRUCK TCHECKTRUCK
                                    LEFT JOIN TUSER TUSERCRE ON TCHECKTRUCK.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TCHECKTRUCK.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TCHECKTRUCK.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------10.-------END TCHECKTRUCK
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------11.--------- TCHECKTRUCKATTACHMENT
                        -----------------------------------------------------------------------11.-------END TCHECKTRUCKATTACHMENT
                        --UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------12.--------- TCHECKTRUCKITEM
                        ---------------------------------------------------------------------12.-------END TCHECKTRUCKITEM
                        --UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------13--------- TCOMPLAIN
                        SELECT 'ข้อมูลข้อร้องเรียน' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลข้อร้องเรียน'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TCOMPLAIN.SCREATE SCREATE,MAX(TCOMPLAIN.DCREATE) DCREATE,
                                    'USER ID : '||TCOMPLAIN.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TCOMPLAIN TCOMPLAIN
                                    LEFT JOIN TUSER TUSERCRE ON TCOMPLAIN.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TCOMPLAIN.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TCOMPLAIN.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TCOMPLAIN.SUPDATE SUPDATE,MAX(TCOMPLAIN.DUPDATE) DUPDATE,
                                    'USER ID : '||TCOMPLAIN.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TCOMPLAIN TCOMPLAIN
                                    LEFT JOIN TUSER TUSERCRE ON TCOMPLAIN.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TCOMPLAIN.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TCOMPLAIN.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------13.-------END TCOMPLAIN
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------14.--------- TCONTRACT
                        -----------------------------------------------------------------------14.-------END TCONTRACT
                        --UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------15.--------- TCONTRACT_GUARANTEES
                        -----------------------------------------------------------------------15.-------END TCONTRACT_GUARANTEES
                        --UNION ALL -----------------------------------------------------------------------------------
                        -------------------------------------------------------------------------16.--------- TCONTRACT_TERMINAL
                        -----------------------------------------------------------------------16.-------END TCONTRACT_TERMINAL
                        --UNION ALL -----------------------------------------------------------------------------------
                        -------------------------------------------------------------------------17.--------- TCONTRACT_TRUCK
                        ---------------------------------------------------------------------17.-------END TCONTRACT_TRUCK
                        --UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------18.--------- TCUSTOMER
                        -----------------------------------------------------------------------18.-------END TCUSTOMER
                        --UNION ALL -----------------------------------------------------------------------------------
                        -------------------------------------------------------------------------19.--------- TEMPLOYEE
                        -----------------------------------------------------------------------19.-------END TEMPLOYEE
                        --UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------20.--------- TFIFO
                        SELECT 'ข้อมูลการลงคิวงาน' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลการลงคิวงาน'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TFIFO.SCREATE SCREATE,MAX(TFIFO.DCREATE) DCREATE,
                                    'USER ID : '||TFIFO.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TFIFO TFIFO
                                    LEFT JOIN TUSER TUSERCRE ON TFIFO.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TFIFO.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TFIFO.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1 
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS

                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------20.-------END TFIFO
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------21.--------- TGRADE
                        SELECT 'ข้อมูลGRADE' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลGRADE'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TGRADE.SCREATE SCREATE,MAX(TGRADE.DCREATE) DCREATE,
                                    'USER ID : '||TGRADE.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TGRADE TGRADE
                                    LEFT JOIN TUSER TUSERCRE ON TGRADE.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TGRADE.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TGRADE.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TGRADE.SUPDATE SUPDATE,MAX(TGRADE.DUPDATE) DUPDATE,
                                    'USER ID : '||TGRADE.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TGRADE TGRADE
                                    LEFT JOIN TUSER TUSERCRE ON TGRADE.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TGRADE.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TGRADE.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------21.-------END TGRADE
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------22.--------- TGROUPOFCHECKLIST
                        SELECT 'ข้อมูลกลุ่มแบบฟอร์มตรวจสอบ' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลกลุ่มแบบฟอร์มตรวจสอบ'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TGROUPOFCHECKLIST.SCREATE SCREATE,MAX(TGROUPOFCHECKLIST.DCREATE) DCREATE,
                                    'USER ID : '||TGROUPOFCHECKLIST.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TGROUPOFCHECKLIST TGROUPOFCHECKLIST
                                    LEFT JOIN TUSER TUSERCRE ON TGROUPOFCHECKLIST.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TGROUPOFCHECKLIST.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TGROUPOFCHECKLIST.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TGROUPOFCHECKLIST.SUPDATE SUPDATE,MAX(TGROUPOFCHECKLIST.DUPDATE) DUPDATE,
                                    'USER ID : '||TGROUPOFCHECKLIST.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TGROUPOFCHECKLIST TGROUPOFCHECKLIST
                                    LEFT JOIN TUSER TUSERCRE ON TGROUPOFCHECKLIST.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TGROUPOFCHECKLIST.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TGROUPOFCHECKLIST.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------22.-------END TGROUPOFCHECKLIST
                        UNION ALL -------------------------------------------------------------------------------------
                        -----------------------------------------------------------------------23.--------- TGROUPOFVISITFORM
                        SELECT 'ข้อมูลกลุ่มแบบฟอร์มประเมินผู้ประกอบการ' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลกลุ่มแบบฟอร์มประเมินผู้ประกอบการ'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(  
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TGROUPOFVISITFORM.SCREATE SCREATE,MAX(TGROUPOFVISITFORM.DCREATE) DCREATE,
                                    'USER ID : '||TGROUPOFVISITFORM.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TGROUPOFVISITFORM TGROUPOFVISITFORM
                                    LEFT JOIN TUSER TUSERCRE ON TGROUPOFVISITFORM.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TGROUPOFVISITFORM.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TGROUPOFVISITFORM.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TGROUPOFVISITFORM.SUPDATE SUPDATE,MAX(TGROUPOFVISITFORM.DUPDATE) DUPDATE,
                                    'USER ID : '||TGROUPOFVISITFORM.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TGROUPOFVISITFORM TGROUPOFVISITFORM
                                    LEFT JOIN TUSER TUSERCRE ON TGROUPOFVISITFORM.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TGROUPOFVISITFORM.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TGROUPOFVISITFORM.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------23.-------END TGROUPOFVISITFORM
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------24.--------- TLOSS
                        -----------------------------------------------------------------------24.-------END TLOSS
                        --UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------25.--------- TPLANSCHEDULE
                        SELECT 'ข้อมูลจัดแผน' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลจัดแผน'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TPLANSCHEDULE.SCREATE SCREATE,MAX(TPLANSCHEDULE.DCREATE) DCREATE,
                                    'USER ID : '||TPLANSCHEDULE.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TPLANSCHEDULE TPLANSCHEDULE
                                    LEFT JOIN TUSER TUSERCRE ON TPLANSCHEDULE.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TPLANSCHEDULE.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TPLANSCHEDULE.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TPLANSCHEDULE.SUPDATE SUPDATE,MAX(TPLANSCHEDULE.DUPDATE) DUPDATE,
                                    'USER ID : '||TPLANSCHEDULE.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TPLANSCHEDULE TPLANSCHEDULE
                                    LEFT JOIN TUSER TUSERCRE ON TPLANSCHEDULE.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TPLANSCHEDULE.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TPLANSCHEDULE.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------25.-------END TPLANSCHEDULE
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------26.--------- TPROBLEM
                        SELECT 'ข้อมูลปัญหาอื่นๆ' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลปัญหาอื่นๆ'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TPROBLEM.SCREATE SCREATE,MAX(TPROBLEM.DCREATE) DCREATE,
                                    'USER ID : '||TPROBLEM.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TPROBLEM TPROBLEM
                                    LEFT JOIN TUSER TUSERCRE ON TPROBLEM.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TPROBLEM.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TPROBLEM.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TPROBLEM.SUPDATE SUPDATE,MAX(TPROBLEM.DUPDATE) DUPDATE,
                                    'USER ID : '||TPROBLEM.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TPROBLEM TPROBLEM
                                    LEFT JOIN TUSER TUSERCRE ON TPROBLEM.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TPROBLEM.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TPROBLEM.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------26.-------END TPROBLEM
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------27.--------- TPROBLEMLIST
                        SELECT 'ข้อมูลรายการปัญหาอื่นๆ' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลรายการปัญหาอื่นๆ'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TPROBLEMLIST.SCREATE SCREATE,MAX(TPROBLEMLIST.DCREATE) DCREATE,
                                    'USER ID : '||TPROBLEMLIST.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TPROBLEMLIST TPROBLEMLIST
                                    LEFT JOIN TUSER TUSERCRE ON TPROBLEMLIST.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TPROBLEMLIST.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TPROBLEMLIST.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TPROBLEMLIST.SUPDATE SUPDATE,MAX(TPROBLEMLIST.DUPDATE) DUPDATE,
                                    'USER ID : '||TPROBLEMLIST.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TPROBLEMLIST TPROBLEMLIST
                                    LEFT JOIN TUSER TUSERCRE ON TPROBLEMLIST.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TPROBLEMLIST.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TPROBLEMLIST.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------27.-------END TPROBLEMLIST
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------28.--------- TQUESTIONNAIRE
                        SELECT 'ข้อมูลTQUESTIONNAIRE' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลTQUESTIONNAIRE'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TQUESTIONNAIRE.SCREATE SCREATE,MAX(TQUESTIONNAIRE.DCREATE) DCREATE,
                                    'USER ID : '||TQUESTIONNAIRE.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TQUESTIONNAIRE TQUESTIONNAIRE
                                    LEFT JOIN TUSER TUSERCRE ON TQUESTIONNAIRE.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TQUESTIONNAIRE.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TQUESTIONNAIRE.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TQUESTIONNAIRE.SUPDATE SUPDATE,MAX(TQUESTIONNAIRE.DUPDATE) DUPDATE,
                                    'USER ID : '||TQUESTIONNAIRE.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TQUESTIONNAIRE TQUESTIONNAIRE
                                    LEFT JOIN TUSER TUSERCRE ON TQUESTIONNAIRE.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TQUESTIONNAIRE.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TQUESTIONNAIRE.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------28.-------END TQUESTIONNAIRE
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------29.--------- TSURPRISECHECKHISTORY
                        -----------------------------------------------------------------------29.-------END TSURPRISECHECKHISTORY
                        --UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------30.--------- TTERMINAL
                        -----------------------------------------------------------------------30.-------END TTERMINAL
                        --UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------31.--------- TTOPIC
                        SELECT 'ข้อมูลTTOPIC' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลTTOPIC'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TTOPIC.SCREATE SCREATE,MAX(TTOPIC.DCREATE) DCREATE,
                                    'USER ID : '||TTOPIC.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TTOPIC TTOPIC
                                    LEFT JOIN TUSER TUSERCRE ON TTOPIC.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TTOPIC.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TTOPIC.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TTOPIC.SUPDATE SUPDATE,MAX(TTOPIC.DUPDATE) DUPDATE,
                                    'USER ID : '||TTOPIC.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TTOPIC TTOPIC
                                    LEFT JOIN TUSER TUSERCRE ON TTOPIC.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TTOPIC.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TTOPIC.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------31.-------END TTOPIC
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------32.--------- TTRUCK
                        -----------------------------------------------------------------------32.-------END TTRUCK
                        --UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------33.--------- TTRUCKCONFIRM
                        SELECT 'ข้อมูลยืนยันรถตามสัญญา' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลยืนยันรถตามสัญญา'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TTRUCKCONFIRM.SCREATE SCREATE,MAX(TTRUCKCONFIRM.DCREATE) DCREATE,
                                    'USER ID : '||TTRUCKCONFIRM.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TTRUCKCONFIRM TTRUCKCONFIRM
                                    LEFT JOIN TUSER TUSERCRE ON TTRUCKCONFIRM.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TTRUCKCONFIRM.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TTRUCKCONFIRM.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TTRUCKCONFIRM.SUPDATE SUPDATE,MAX(TTRUCKCONFIRM.DUPDATE) DUPDATE,
                                    'USER ID : '||TTRUCKCONFIRM.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TTRUCKCONFIRM TTRUCKCONFIRM
                                    LEFT JOIN TUSER TUSERCRE ON TTRUCKCONFIRM.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TTRUCKCONFIRM.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TTRUCKCONFIRM.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------33.-------END TTRUCKCONFIRM
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------34.--------- TTRUCKCONFIRMLIST
                        SELECT 'ข้อมูลรายการยืนยันรถตามสัญญา' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลรายการยืนยันรถตามสัญญา'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TTRUCKCONFIRMLIST.SCREATE SCREATE,MAX(TTRUCKCONFIRMLIST.DCREATE) DCREATE,
                                    'USER ID : '||TTRUCKCONFIRMLIST.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TTRUCKCONFIRMLIST TTRUCKCONFIRMLIST
                                    LEFT JOIN TUSER TUSERCRE ON TTRUCKCONFIRMLIST.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TTRUCKCONFIRMLIST.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TTRUCKCONFIRMLIST.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TTRUCKCONFIRMLIST.SUPDATE SUPDATE,MAX(TTRUCKCONFIRMLIST.DUPDATE) DUPDATE,
                                    'USER ID : '||TTRUCKCONFIRMLIST.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TTRUCKCONFIRMLIST TTRUCKCONFIRMLIST
                                    LEFT JOIN TUSER TUSERCRE ON TTRUCKCONFIRMLIST.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TTRUCKCONFIRMLIST.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TTRUCKCONFIRMLIST.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------34-------END TTRUCKCONFIRMLIST
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------35.--------- TTRUCK_COMPART
                        -----------------------------------------------------------------------35.-------END TTRUCK_COMPART
                        --UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------36.--------- TTYPEOFCHECKLIST
                        SELECT 'ข้อมูลประเภทแบบฟอร์มตรวจสอบ' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลประเภทแบบฟอร์มตรวจสอบ'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT 'ข้อมูลประเภทแบบฟอร์มตรวจสอบ' TABLE_NAME, STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TTYPEOFCHECKLIST.SCREATE SCREATE,MAX(TTYPEOFCHECKLIST.DCREATE) DCREATE,
                                    'USER ID : '||TTYPEOFCHECKLIST.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TTYPEOFCHECKLIST TTYPEOFCHECKLIST
                                    LEFT JOIN TUSER TUSERCRE ON TTYPEOFCHECKLIST.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TTYPEOFCHECKLIST.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TTYPEOFCHECKLIST.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TTYPEOFCHECKLIST.SUPDATE SUPDATE,MAX(TTYPEOFCHECKLIST.DUPDATE) DUPDATE,
                                    'USER ID : '||TTYPEOFCHECKLIST.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TTYPEOFCHECKLIST TTYPEOFCHECKLIST
                                    LEFT JOIN TUSER TUSERCRE ON TTYPEOFCHECKLIST.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TTYPEOFCHECKLIST.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TTYPEOFCHECKLIST.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------36.-------END TTYPEOFCHECKLIST
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------37.--------- TTYPEOFVISITFORM
                        SELECT 'ข้อมูลประเภทแบบปอร์มประเมินผูประกอบการ' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลประเภทแบบปอร์มประเมินผูประกอบการ'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TTYPEOFVISITFORM.SCREATE SCREATE,MAX(TTYPEOFVISITFORM.DCREATE) DCREATE,
                                    'USER ID : '||TTYPEOFVISITFORM.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TTYPEOFVISITFORM TTYPEOFVISITFORM
                                    LEFT JOIN TUSER TUSERCRE ON TTYPEOFVISITFORM.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TTYPEOFVISITFORM.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TTYPEOFVISITFORM.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TTYPEOFVISITFORM.SUPDATE SUPDATE,MAX(TTYPEOFVISITFORM.DUPDATE) DUPDATE,
                                    'USER ID : '||TTYPEOFVISITFORM.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TTYPEOFVISITFORM TTYPEOFVISITFORM
                                    LEFT JOIN TUSER TUSERCRE ON TTYPEOFVISITFORM.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TTYPEOFVISITFORM.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TTYPEOFVISITFORM.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------37.-------END TTYPEOFVISITFORM
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------38.--------- TUNIT
                        -----------------------------------------------------------------------38.-------END TUNIT
                        --UNION ALL -----------------------------------------------------------------------------------
                        -------------------------------------------------------------------------39.--------- TVENDOR
                        -----------------------------------------------------------------------39.-------END TVENDOR
                        --UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------40.--------- TVISITFORM
                        SELECT 'ข้อมูลTVISITFORM' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลTVISITFORM'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TVISITFORM.SCREATE SCREATE,MAX(TVISITFORM.DCREATE) DCREATE,
                                    'USER ID : '||TVISITFORM.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TVISITFORM TVISITFORM
                                    LEFT JOIN TUSER TUSERCRE ON TVISITFORM.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TVISITFORM.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TVISITFORM.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TVISITFORM.SUPDATE SUPDATE,MAX(TVISITFORM.DUPDATE) DUPDATE,
                                    'USER ID : '||TVISITFORM.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TVISITFORM TVISITFORM
                                    LEFT JOIN TUSER TUSERCRE ON TVISITFORM.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TVISITFORM.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TVISITFORM.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------40.-------END TVISITFORM
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------41.--------- TVISITOR
                        SELECT 'ข้อมูลTVISITOR' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลTVISITOR'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TVISITOR.SCREATE SCREATE,MAX(TVISITOR.DCREATE) DCREATE,
                                    'USER ID : '||TVISITOR.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TVISITOR TVISITOR
                                    LEFT JOIN TUSER TUSERCRE ON TVISITOR.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TVISITOR.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TVISITOR.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TVISITOR.SUPDATE SUPDATE,MAX(TVISITOR.DUPDATE) DUPDATE,
                                    'USER ID : '||TVISITOR.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TVISITOR TVISITOR
                                    LEFT JOIN TUSER TUSERCRE ON TVISITOR.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TVISITOR.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TVISITOR.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------41.-------END TVISITOR
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------42.--------- TVISITORITEM
                        SELECT 'ข้อมูลTVISITORITEM' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลTVISITORITEM'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TVISITORITEM.SCREATE SCREATE,MAX(TVISITORITEM.DCREATE) DCREATE,
                                    'USER ID : '||TVISITORITEM.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TVISITORITEM TVISITORITEM
                                    LEFT JOIN TUSER TUSERCRE ON TVISITORITEM.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TVISITORITEM.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TVISITORITEM.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TVISITORITEM.SUPDATE SUPDATE,MAX(TVISITORITEM.DUPDATE) DUPDATE,
                                    'USER ID : '||TVISITORITEM.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TVISITORITEM TVISITORITEM
                                    LEFT JOIN TUSER TUSERCRE ON TVISITORITEM.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TVISITORITEM.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TVISITORITEM.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------42.-------END TVISITORITEM
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------43.--------- TVISITORUSER
                        SELECT 'ข้อมูลTVISITORUSER' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลTVISITORUSER'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TVISITORUSER.SCREATE SCREATE,MAX(TVISITORUSER.DCREATE) DCREATE,
                                    'USER ID : '||TVISITORUSER.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TVISITORUSER TVISITORUSER
                                    LEFT JOIN TUSER TUSERCRE ON TVISITORUSER.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TVISITORUSER.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TVISITORUSER.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TVISITORUSER.SUPDATE SUPDATE,MAX(TVISITORUSER.DUPDATE) DUPDATE,
                                    'USER ID : '||TVISITORUSER.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TVISITORUSER TVISITORUSER
                                    LEFT JOIN TUSER TUSERCRE ON TVISITORUSER.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TVISITORUSER.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TVISITORUSER.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------43.-------END TVISITORUSER
                        UNION ALL -----------------------------------------------------------------------------------
                        -----------------------------------------------------------------------44.--------- TWINDOWTIME
                        SELECT 'ข้อมูลTIMEWINDOW' เมนู, LATEST_DATE วันที่, TUSER.SUID, TUSER.SUID||'-'||TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME ผู้ใช้งาน, TUSER.SPOSITION หน่วยงาน, CASE TUSER.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END กลุ่มผู้ใช้งาน, CASE WHEN STATUS IS NOT NULL THEN 'ข้อมูลTIMEWINDOW'||'-->'||STATUS ELSE '' END การทำงาน
                        FROM TUSER TUSER LEFT JOIN(
                            SELECT STATUS,
                            CASE WHEN STATUS = 'เพิ่ม' THEN DCREATE ELSE DCREATE
                            END LATEST_DATE, GROUP_USER_ID, UNITS,SCREATE
                            FROM(
                                SELECT STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'เพิ่ม' STATUS, TWINDOWTIME.SCREATE SCREATE,MAX(TWINDOWTIME.DCREATE) DCREATE,
                                    'USER ID : '||TWINDOWTIME.SCREATE||' - '||TUSERCRE.SFIRSTNAME||' '||TUSERCRE.SLASTNAME ACTIVE_USER, TUSERCRE.CGROUP GROUP_USER_ID,
                                    CASE TUSERCRE.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERCRE.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERCRE.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERCRE.SVENDORID)
                                    END UNITS
                                    FROM TWINDOWTIME TWINDOWTIME
                                    LEFT JOIN TUSER TUSERCRE ON TWINDOWTIME.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TWINDOWTIME.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TWINDOWTIME.SCREATE, TUSERCRE.SFIRSTNAME, TUSERCRE.SLASTNAME, TUSERCRE.CGROUP, TUSERCRE.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SCREATE, DCREATE, ACTIVE_USER, GROUP_USER_ID, UNITS
        
                                UNION ALL --------------------------------------------------------------------------------
        
                                SELECT STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                                FROM (
                                    SELECT 'แก้ไข' STATUS, TWINDOWTIME.SUPDATE SUPDATE,MAX(TWINDOWTIME.DUPDATE) DUPDATE,
                                    'USER ID : '||TWINDOWTIME.SUPDATE||' - '||TUSERUPD.SFIRSTNAME||' '||TUSERUPD.SLASTNAME ACTIVE_USER, TUSERUPD.CGROUP GROUP_USER_ID,
                                    CASE TUSERUPD.CGROUP WHEN '0' THEN (SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = TUSERUPD.SVENDORID)
                                        WHEN '1' THEN (SELECT UNITABBR FROM TUNIT WHERE UNITCODE = TUSERUPD.SVENDORID)
                                        WHEN '2' THEN (SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID = TUSERUPD.SVENDORID)
                                    END UNITS
                                    FROM TWINDOWTIME TWINDOWTIME
                                    LEFT JOIN TUSER TUSERCRE ON TWINDOWTIME.SCREATE = TUSERCRE.SUID
                                    LEFT JOIN TUSER TUSERUPD ON TWINDOWTIME.SUPDATE = TUSERUPD.SUID
                                    GROUP BY TWINDOWTIME.SUPDATE, TUSERUPD.SFIRSTNAME, TUSERUPD.SLASTNAME, TUSERUPD.CGROUP, TUSERUPD.SVENDORID
                                )
                                WHERE 1=1
                                GROUP BY STATUS, SUPDATE, DUPDATE, ACTIVE_USER, GROUP_USER_ID, UNITS
                            )
                            WHERE 1=1
                            GROUP BY STATUS, GROUP_USER_ID, UNITS, DCREATE,SCREATE
                        )CHECK_USER ON TUSER.SUID = CHECK_USER.SCREATE
                        ---------------------------------------------------------------------44.-------END TWINDOWTIME
                        )TEVENTLOG ON TUSR.SUID = TEVENTLOG.SUID " + where;
        #endregion

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(connection, sqlQueryLogUser);
        gvwLogUser.DataSource = dt;
        gvwLogUser.DataBind();
    }

}
