﻿<%@ Page Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="Requesting.aspx.cs"
    Inherits="Requesting" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" ClientInstanceName="xcpn" OnCallback="xcpn_Callback">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent>
                <dx:ASPxTextBox ID="txtREQ_ID" runat="Server" ClientVisible="false">
                </dx:ASPxTextBox>
                <table width="100%" border="0" cellpadding="5" cellspacing="1">
                   <%-- <tr>
                        <td colspan="4" align="right">
                            <table border="0" cellspacing="1" cellpadding="1">
                                <tr>
                                    <td>
                                        <dx:ASPxButton ID="btnViewHisCar" runat="Server" Text="ดูประวัติรถ" AutoPostBack="false">
                                            <ClientSideEvents Click="function(s,e){ if(xcpn.InCallback()) return; else xcpn.PerformCallback('VIEWCARHIS;'); }" />
                                        </dx:ASPxButton>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btnServiceHis" runat="Server" Text="ประวัติการรับบริการ" AutoPostBack="false">
                                            <ClientSideEvents Click="function(s,e){ if(xcpn.InCallback()) return; else xcpn.PerformCallback('VIEWSERVICEHIS;'); }" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>--%>
                    <tr>
                        <td bgcolor="#B9EAEF">วันที่ยืนคำขอ</td>
                        <td>
                            <dx:ASPxLabel ID="lblDReq" runat="Server">
                            </dx:ASPxLabel>
                        </td>
                        <td bgcolor="#B9EAEF">วันที่หมดอายุวัดน้ำ</td>
                        <td>
                            <dx:ASPxLabel ID="lblDExpWat" runat="Server">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td width="18%" bgcolor="#B9EAEF">วันที่นัดหมาย</td>
                        <td width="32%">
                            <dx:ASPxLabel ID="lblDAppoint" runat="Server">
                            </dx:ASPxLabel>
                        </td>
                        <td bgcolor="#B9EAEF">สาเหตุ</td>
                        <td>
                            <dx:ASPxLabel ID="lblCause" runat="Server">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#B9EAEF">ประเภทคำขอ</td>
                        <td align="left">
                            <dx:ASPxLabel ID="lblReqType" runat="Server">
                            </dx:ASPxLabel>
                        </td>
                        <td bgcolor="#B9EAEF">ประเภทรถ</td>
                        <td>
                            <dx:ASPxLabel ID="lblCarType" runat="Server">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#B9EAEF">ทะเบียนรถ</td>
                        <td>
                            <dx:ASPxLabel ID="lblVeh_No" runat="Server">
                            </dx:ASPxLabel>
                        </td>
                        <td bgcolor="#B9EAEF">บริษัทขนส่ง</td>
                        <td>
                            <dx:ASPxLabel ID="lblVendor" runat="Server">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><img src="images/credit.png" width="16" height="16" alt="" />รายละเอียดค่าธรรมเนียม
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" bgcolor="#6AC6DF">
                            <table width="100%" border="0" cellpadding="5" cellspacing="1">
                                <tr>
                                    <td width="38%" valign="top" bgcolor="#EAEAEA">ค่าธรรมเนียมบริการ</td>
                                    <td width="62%" bgcolor="#FFFFFF">
                                        <dx:ASPxLabel ID="lblFee" runat="Server">
                                        </dx:ASPxLabel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr runat="server" id="trDoc1">
                        <td colspan="4"><img src="images/cv.png" width="16" height="16" alt="" /> แนบไฟล์เอกสารสำคัญ
                        </td>
                    </tr>
                    <tr runat="server" id="trDoc2">
                        <td colspan="4">
                            <dx:ASPxLabel ID="lblAttachFile" runat="Server" EncodeHtml="false" EnableTheming="false"
                                EnableDefaultAppearance="false">
                            </dx:ASPxLabel>
                            <dx:ASPxGridView ID="gvwdoc" runat="server" AutoGenerateColumns="false" Width="100%"
                                Border-BorderWidth="0px" Border-BorderStyle="None" Border-BorderColor="White">
                                <Columns>
                                        <dx:GridViewDataColumn Caption="ประเภท" FieldName="DOC_DESCRIPTION" CellStyle-BackColor="#b9eaef"
                                        CellStyle-Border-BorderColor="White" Width="22%">
                                        <CellStyle BackColor="#B9EAEF">
                                            <Border BorderColor="White"></Border>
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="คอนโทรนดูเอกสาร" CellStyle-Border-BorderColor="White"
                                        Width="38%">
                                        <DataItemTemplate>
                                            <table width="96%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td align="right" width="50px">
                                                        <dx:ASPxButton runat="server" ID="btnpdf" AutoPostBack="false" CssClass="dxeLineBreakFix"
                                                            EnableDefaultAppearance="false" EnableTheming="false" Cursor="pointer" SkinID="dd">
                                                            <Image Url="Images/ic_pdf2.gif" Height="17px" Width="17px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td align="left">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxLabel runat="server" ID="lblFileName" Text='<%# DataBinder.Eval(Container.DataItem,"FILE_NAME") %>'>
                                                                    </dx:ASPxLabel>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxButton ID="btnView" runat="server" AutoPostBack="False" CssClass="dxeLineBreakFix"
                                                                        Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                                        Width="25px">
                                                                        <Image Height="20px" Url="Images/view1.png" Width="20px">
                                                                        </Image>
                                                                    </dx:ASPxButton>
                                                                    <dx:ASPxTextBox runat="server" ID="txtFilePath" Text='<%# DataBinder.Eval(Container.DataItem,"FILE_PATH") %>'
                                                                        ClientVisible="false">
                                                                    </dx:ASPxTextBox>
                                                                    <dx:ASPxTextBox runat="server" ID="txtFileName" Text='<%# DataBinder.Eval(Container.DataItem,"FILE_SYSNAME") %>'
                                                                        ClientVisible="false">
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </DataItemTemplate>
                                        <CellStyle>
                                            <Border BorderColor="White"></Border>
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ListRdl" CellStyle-Border-BorderColor="White" CellStyle-HorizontalAlign="Right"
                                        Width="50%">
                                        <DataItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <dx:ASPxImage runat="server" ID="img4" ImageUrl='<%# Eval("CONSIDER").ToString() == "Y" ? "images/action_check.png": "images/action_delete.png" %>'>
                                                        </dx:ASPxImage>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxLabel ID="lblstacheck" runat="server" Text='<%# Eval("CONSIDER").ToString() == "Y" ? "ผ่าน": "ไม่ผ่าน" %>'>
                                                        </dx:ASPxLabel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Left">
                                            <Border BorderColor="White"></Border>
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="DOC_ID" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="CONSIDER" Visible="false">
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <Settings ShowColumnHeaders="false" />
                                <Border BorderStyle="None" BorderColor="White" BorderWidth="0px"></Border>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td height="40" colspan="4" align="center">
                            <dx:ASPxButton ID="btnPrintDoc" runat="Server" Text="พิมพ์สำเนาเอกสาร" OnClick="btnPrintDoc_Click">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><img src="images/i_newtopic.gif" width="16" height="16" alt="" /> ผลการขอสำเนาเอกสารการตรวจสอบปริมาตรถังน้ำมัน
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%" border="0" cellpadding="3" cellspacing="2">
                                <tr>
                                    <td width="22%" valign="top" bgcolor="#B9EAEF">สถานะดำเนินการ</td>
                                    <td width="78%">
                                        <dx:ASPxRadioButtonList ID="rblStatus" runat="Server" ClientInstanceName="rblStatus"
                                            RepeatDirection="Horizontal" Border-BorderWidth="0px" Paddings-Padding="0px">
                                            <Paddings Padding="0px"></Paddings>
                                            <Items>
                                                <dx:ListEditItem Value="10" Text="ดำเนินการเรียบร้อย" />
                                                <dx:ListEditItem Value="" Text="ยังไม่ดำเนินการ" Selected="true" />
                                            </Items>
                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                ValidationGroup="submit">
                                                <ErrorFrameStyle ForeColor="Red">
                                                </ErrorFrameStyle>
                                                <RequiredField ErrorText="กรุณาระบุสถานะดำเนินการ" IsRequired="True" />
                                            </ValidationSettings>
                                            <Border BorderWidth="0px"></Border>
                                        </dx:ASPxRadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" bgcolor="#B9EAEF">&nbsp;</td>
                                    <td>
                                        <table border="0" cellspacing="1" cellpadding="1">
                                            <tr>
                                                <td>
                                                    <dx:ASPxButton ID="btnSubmit" runat="Server" Text="บันทึกผล" AutoPostBack="false"
                                                        ValidationGroup="submit">
                                                        <ClientSideEvents Click="function(s,e){ if(!ASPxClientEdit.ValidateGroup('submit')) return false; dxConfirm('แจ้งเตือน','ท่านต้องการบันทึกเพื่อเปลี่ยนแปลงข้อมูลนี้ ใช่หรือไม่ ?',function(s,e){ dxPopupConfirm.Hide(); if(rblStatus.GetValue()=='10'){ if(xcpn.InCallback()) return; else xcpn.PerformCallback('SUBMIT;'); }else window.location='ApproveRequest.aspx'; } ,function(s,e){ dxPopupConfirm.Hide(); }); }" />
                                                    </dx:ASPxButton>
                                                </td>
                                                <td>
                                                    <dx:ASPxButton ID="btnBack" runat="Server" Text="ย้อนกลับ" AutoPostBack="false">
                                                        <ClientSideEvents Click="function(s,e){ window.location='approve_mv.aspx'; }" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><img src="images/comment.png" width="16" height="16" alt="" /> หมายเหตุ
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <dx:ASPxGridView ID="gvwRemark" runat="Server" ClientInstanceName="gvwRemark" SkinID="_gvw"
                                Width="100%" Style="margin: 0px;">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="วันที่-เวลา" FieldName="REMARK_DATE" Width="17%">
                                        <PropertiesTextEdit DisplayFormatString="{0:dd/MM/yyyy} - {0:HH:mm} น.">
                                        </PropertiesTextEdit>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                 <dx:GridViewDataColumn Caption="ผู้บันทึก" FieldName="SNAME" Width="24%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn Caption="ขั้นตอน" FieldName="SDESCRIPTION" Width="25%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="หมายเหตุ" FieldName="REMARKS" Width="35%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsPager Mode="ShowAllRecords">
                                </SettingsPager>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">&nbsp;</td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
