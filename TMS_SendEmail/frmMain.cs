﻿using System;
using System.Data;
using System.Windows.Forms;
using TMS_BLL.Transaction.SendEmail;
using TMS_BLL.Master;
using EmailHelper;
using TMS_BLL.Transaction.Complain;
using System.Threading;

namespace TMS_SendEmail
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitialForm();
        }

        private void InitialForm()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.SendEmail();

                //notifyIcon1.Icon = TMS_SendEmail.Properties.Resources.mail;
                //dtpExecuteTime.Text = Properties.Settings.Default.ExecuteTime;

                //lblEnvironment.Text = string.Format(lblEnvironment.Text, Properties.Settings.Default.Environment);
                //lblVersion.Text = string.Format(lblVersion.Text, Assembly.GetEntryAssembly().GetName().Version.ToString());
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                Thread.Sleep(2000);
                this.Cursor = Cursors.Default;
                Application.Exit();
            }
        }

        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            this.Show();
            WindowState = FormWindowState.Normal;
            this.BringToFront();
        }

        private void frmMain_Shown(object sender, EventArgs e)
        {
            //WindowState = FormWindowState.Minimized;
            //Hide();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                Properties.Settings.Default["ExecuteTime"] = dtpExecuteTime.Text;
                Properties.Settings.Default.Save();

                MessageBox.Show("Save Success...");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (string.Equals(DateTime.Now.ToString("HH:mm"), dtpExecuteTime.Text))
                    this.SendEmail();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmdSendNow_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการส่งอีเมล์ทันที ใช่หรือไม่", "ยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    this.SendEmail();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SendEmail()
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                int TemplateID;
                DataTable dtEmail;

                TemplateID = 3;                                                                     //รายงานประจำเดือน
                dtEmail = SendEmailBLL.Instance.SendEmailReportSelectBLL(TemplateID, 1);
                this.SendEmailReport(dtEmail);

                TemplateID = 4;                                                                     //รายงานประจำ 3 เดือน
                dtEmail = SendEmailBLL.Instance.SendEmailReportSelectBLL(TemplateID, 2);
                this.SendEmailReport(dtEmail);

                TemplateID = 5;                                                                     //รายงานประจำ 6 เดือน
                dtEmail = SendEmailBLL.Instance.SendEmailReportSelectBLL(TemplateID, 3);
                this.SendEmailReport(dtEmail);

                TemplateID = 21;                                                                     //แจ้งเตือน ปตท ให้ตรวจรายงาน
                dtEmail = SendEmailBLL.Instance.SendEmailReportSelectBLL(TemplateID, 0);
                if (dtEmail.Rows.Count > 0)
                    this.SendWarningPTT(TemplateID);
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void SendEmailReport(DataTable dtEmail)
        {
            try
            {
                for (int i = 0; i < dtEmail.Rows.Count; i++)
                {
                    //MailService.SendMail(dtEmail.Rows[i]["EMAIL"].ToString() + ", zsakchai.p@pttict.com", dtTemplate.Rows[0]["SUBJECT"].ToString(), Body);
                    MailService.SendMail(dtEmail.Rows[i]["EMAIL"].ToString(), dtEmail.Rows[0]["SUBJECT"].ToString(), dtEmail.Rows[0]["BODY"].ToString(), "", "VendorEmployeeAdd", "EMAIL");
                }
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
            }
        }

        private void SendWarningPTT(int TemplateID)
        {
            try
            {
                DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
                if (dtTemplate.Rows.Count > 0)
                {
                    string Email = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, "80000368", 0, string.Empty, false, true);

                    string Body = dtTemplate.Rows[0]["BODY"].ToString();

                    MailService.SendMail(Email, dtTemplate.Rows[0]["SUBJECT"].ToString(), Body, "", "ReportWarning", "EMAIL");
                }
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
            }
        }
    }
}