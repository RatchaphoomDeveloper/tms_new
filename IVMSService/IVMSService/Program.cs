﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace IVMSService
{
    class Program
    {
        static HttpClient client = new HttpClient();

        static void Main(string[] args)
        {

        }
        static void InsertContractSub(string ID, string CARRIER_CODE, string CONTRACTS_NUM, string CONTRACT_ID)
        {
            var YourModel = new { ID = "1", CARRIER_CODE = "0010010063", CONTRACTS_NUM = "ปตท./ขปน./ร.1/31/2558", CONTRACT_ID = "2" };
            using (var client = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(YourModel);
                var stringContent = new StringContent(json, Encoding.UTF8,
                            "application/json");
                var model = client
                                   .PostAsync("http://hermes.pttdigital.com/tms_contract_sub/InsertContractSub", stringContent)
                                   .Result;
                if (model.StatusCode == HttpStatusCode.OK)
                {
                    //ผ่าน
                }

            }
        }

        static void UpdateContract(string ID, string GROUPNAME)
        {
            var YourModel = new { ID = "1", GROUPNAME = "0010010063" };
            using (var client = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(YourModel);
                var stringContent = new StringContent(json, Encoding.UTF8,
                            "application/json");
                var model = client
                                   .PostAsync("http://hermes.pttdigital.com/tms_contract/UpdateContract", stringContent)
                                   .Result;
                if (model.StatusCode == HttpStatusCode.OK)
                {
                    //ผ่าน
                }

            }
        }

        static void TruckUpdateContract(string CHASSIS, string LICENSE_PLATE, string LICENSE_PLATE_BACK, string CONTRACT_NUM, string CONTRACT_UPDATE, string CONTRACT_ENDING, string CARIER_CODE, string CONTRACT_NAME, string TU_NAME, string TU_MODEL, string TRUCK_CATEGORY)
        {
            var YourModel = new { CHASSIS = "1", LICENSE_PLATE = "0010010063", LICENSE_PLATE_BACK = "1", CONTRACT_NUM = "0010010063", CONTRACT_UPDATE = "1", CONTRACT_ENDING = "0010010063", CARIER_CODE = "1", CONTRACT_NAME = "0010010063", TU_NAME = "1", TU_MODEL = "0010010063", TRUCK_CATEGORY = "0010010063" };
            using (var client = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(YourModel);
                var stringContent = new StringContent(json, Encoding.UTF8,
                            "application/json");
                var model = client
                                   .PostAsync("http://hermes.pttdigital.com/tms_truck/TruckUpdateContract", stringContent)
                                   .Result;
                if (model.StatusCode == HttpStatusCode.OK)
                {
                    //ผ่าน
                }

            }
        }

        static void VehicleStatus(string LICENSE, string CARIERCODE, string CONTRACTNUM, string SPLANTCODE)
        {
            var YourModel = new { LICENSE = "1", CARIERCODE = "0010010063", CONTRACTNUM = "1", SPLANTCODE = "0010010063" };
            using (var client = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(YourModel);
                var stringContent = new StringContent(json, Encoding.UTF8,
                            "application/json");
                var model = client
                                   .PostAsync("http://hermes.pttdigital.com/tms_vehiclestatus/VehicleStatus", stringContent)
                                   .Result;
                if (model.StatusCode == HttpStatusCode.OK)
                {
                    //ผ่าน
                }

            }
        }

        static void InsertDriver(string ID, string PERSONAL_ID, string FIRST_NAME, string LAST_NAME, string DRIVERSTATUS, string CARIER_CODE, string PICTUREPATH, string UPDATEDATETIME)
        {
            var YourModel = new { ID = "1", PERSONAL_ID = "0010010063", FIRST_NAME = "1", LAST_NAME = "0010010063", DRIVERSTATUS = "1", CARIER_CODE = "0010010063", PICTUREPATH = "1", UPDATEDATETIME = "0010010063" };
            using (var client = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(YourModel);
                var stringContent = new StringContent(json, Encoding.UTF8,
                            "application/json");
                var model = client
                                   .PostAsync("http://hermes.pttdigital.com/tms_driver/InsertDriver", stringContent)
                                   .Result;
                if (model.StatusCode == HttpStatusCode.OK)
                {
                    //ผ่าน
                }

            }
        }

    }
}
