﻿using System;
using System.Data;

namespace TMS.Entity.Transaction.Truck
{
    [Serializable]
    public class TruckEntity
    {
        private int? _SYSTEM_ID;
        public int? SYSTEM_ID
        { get { return _SYSTEM_ID; } set { _SYSTEM_ID = value; } }


        private int? _USER_ID;
        public int? USER_ID
        { get { return _USER_ID; } set { _USER_ID = value; } }


        private int? _TRUCK_TEMP_ID;
        public int? TRUCK_TEMP_ID
        { get { return _TRUCK_TEMP_ID; } set { _TRUCK_TEMP_ID = value; } }


        private int? _TRUCK_TYPE_ID;
        public int? TRUCK_TYPE_ID
        { get { return _TRUCK_TYPE_ID; } set { _TRUCK_TYPE_ID = value; } }


        private int? _IS_LICENSE;
        public int? IS_LICENSE
        { get { return _IS_LICENSE; } set { _IS_LICENSE = value; } }


        private string _LICENSE;
        public string LICENSE
        { get { return _LICENSE; } set { _LICENSE = value; } }


        private string _VECHICLE_TEXT;
        public string VECHICLE_TEXT
        { get { return _VECHICLE_TEXT; } set { _VECHICLE_TEXT = value; } }


        private int? _VECHICLE_ROUTE_ID;
        public int? VECHICLE_ROUTE_ID
        { get { return _VECHICLE_ROUTE_ID; } set { _VECHICLE_ROUTE_ID = value; } }


        private int? _TRANS_MODE_ID;
        public int? TRANS_MODE_ID
        { get { return _TRANS_MODE_ID; } set { _TRANS_MODE_ID = value; } }


        private int? _TU_TYPE_ID;
        public int? TU_TYPE_ID
        { get { return _TU_TYPE_ID; } set { _TU_TYPE_ID = value; } }


        private string _CHASSIS;
        public string CHASSIS
        { get { return _CHASSIS; } set { _CHASSIS = value; } }


        private int? _PHYSICAL_TYPE_ID;
        public int? PHYSICAL_TYPE_ID
        { get { return _PHYSICAL_TYPE_ID; } set { _PHYSICAL_TYPE_ID = value; } }


        private string _REGISTER_DELIVERY_DATE;
        public string REGISTER_DELIVERY_DATE
        { get { return _REGISTER_DELIVERY_DATE; } set { _REGISTER_DELIVERY_DATE = value; } }


        private string _ENGINE_NUMBER;
        public string ENGINE_NUMBER
        { get { return _ENGINE_NUMBER; } set { _ENGINE_NUMBER = value; } }


        private int? _VENDOR_ID;
        public int? VENDOR_ID
        { get { return _VENDOR_ID; } set { _VENDOR_ID = value; } }


        private int? _BRAND_ID;
        public int? BRAND_ID
        { get { return _BRAND_ID; } set { _BRAND_ID = value; } }


        private string _ENGINE_POWER;
        public string ENGINE_POWER
        { get { return _ENGINE_POWER; } set { _ENGINE_POWER = value; } }


        private int? _WHEEL_ID;
        public int? WHEEL_ID
        { get { return _WHEEL_ID; } set { _WHEEL_ID = value; } }


        private int? _FUEL_TYPE_ID;
        public int? FUEL_TYPE_ID
        { get { return _FUEL_TYPE_ID; } set { _FUEL_TYPE_ID = value; } }


        private int? _SHAFT_ID;
        public int? SHAFT_ID
        { get { return _SHAFT_ID; } set { _SHAFT_ID = value; } }


        private int? _FUEL_CAPACITY_ID;
        public int? FUEL_CAPACITY_ID
        { get { return _FUEL_CAPACITY_ID; } set { _FUEL_CAPACITY_ID = value; } }


        private int? _VIBRATE_ID;
        public int? VIBRATE_ID
        { get { return _VIBRATE_ID; } set { _VIBRATE_ID = value; } }


        private string _OWNER_SHIP;
        public string OWNER_SHIP
        { get { return _OWNER_SHIP; } set { _OWNER_SHIP = value; } }


        private int? _VENDOR_TANK_ID;
        public int? VENDOR_TANK_ID
        { get { return _VENDOR_TANK_ID; } set { _VENDOR_TANK_ID = value; } }


        private int? _VALVE_TYPE_ID;
        public int? VALVE_TYPE_ID
        { get { return _VALVE_TYPE_ID; } set { _VALVE_TYPE_ID = value; } }


        private int? _PRESSURE_ID;
        public int? PRESSURE_ID
        { get { return _PRESSURE_ID; } set { _PRESSURE_ID = value; } }


        private int? _MATERIAL_ID;
        public int? MATERIAL_ID
        { get { return _MATERIAL_ID; } set { _MATERIAL_ID = value; } }


        private int? _PUMP_TYPE_ID;
        public int? PUMP_TYPE_ID
        { get { return _PUMP_TYPE_ID; } set { _PUMP_TYPE_ID = value; } }


        private int? _SHAFT_WEIGHT_ID;
        public int? SHAFT_WEIGHT_ID
        { get { return _SHAFT_WEIGHT_ID; } set { _SHAFT_WEIGHT_ID = value; } }


        private int? _INSURANCE_ID;
        public int? INSURANCE_ID
        { get { return _INSURANCE_ID; } set { _INSURANCE_ID = value; } }


        private int? _INSURANCE_TYPE_ID;
        public int? INSURANCE_TYPE_ID
        { get { return _INSURANCE_TYPE_ID; } set { _INSURANCE_TYPE_ID = value; } }


        private string _INSURANCE_DATE_START;
        public string INSURANCE_DATE_START
        { get { return _INSURANCE_DATE_START; } set { _INSURANCE_DATE_START = value; } }


        private string _INSURANCE_DATE_END;
        public string INSURANCE_DATE_END
        { get { return _INSURANCE_DATE_END; } set { _INSURANCE_DATE_END = value; } }


        private int? _PROTECT_TYPE_ID;
        public int? PROTECT_TYPE_ID
        { get { return _PROTECT_TYPE_ID; } set { _PROTECT_TYPE_ID = value; } }


        private int? _FILL_TYPE_ID;
        public int? FILL_TYPE_ID
        { get { return _FILL_TYPE_ID; } set { _FILL_TYPE_ID = value; } }


        private int? _INLET_ID;
        public int? INLET_ID
        { get { return _INLET_ID; } set { _INLET_ID = value; } }


        private int? _INLET_LEVEL_ID;
        public int? INLET_LEVEL_ID
        { get { return _INLET_LEVEL_ID; } set { _INLET_LEVEL_ID = value; } }


        private int? _STATUS_ID_GPS;
        public int? STATUS_ID_GPS
        { get { return _STATUS_ID_GPS; } set { _STATUS_ID_GPS = value; } }


        private int? _STATUS_ID_CCTV;
        public int? STATUS_ID_CCTV
        { get { return _STATUS_ID_CCTV; } set { _STATUS_ID_CCTV = value; } }


        private int? _VENDOR_IVMS_ID;
        public int? VENDOR_IVMS_ID
        { get { return _VENDOR_IVMS_ID; } set { _VENDOR_IVMS_ID = value; } }


        private string _MV_POSITION;
        public string MV_POSITION
        { get { return _MV_POSITION; } set { _MV_POSITION = value; } }


        private string _MV_CODE;
        public string MV_CODE
        { get { return _MV_CODE; } set { _MV_CODE = value; } }


        private string _MV_DATE_FIRST;
        public string MV_DATE_FIRST
        { get { return _MV_DATE_FIRST; } set { _MV_DATE_FIRST = value; } }


        private string _MV_DATE_LASTED;
        public string MV_DATE_LASTED
        { get { return _MV_DATE_LASTED; } set { _MV_DATE_LASTED = value; } }


        private string _MV_DATE_NEXT;
        public string MV_DATE_NEXT
        { get { return _MV_DATE_NEXT; } set { _MV_DATE_NEXT = value; } }


        private int? _TRANSACTION_TYPE;
        public int? TRANSACTION_TYPE
        { get { return _TRANSACTION_TYPE; } set { _TRANSACTION_TYPE = value; } }


        private int? _STATUS_ID;
        public int? STATUS_ID
        { get { return _STATUS_ID; } set { _STATUS_ID = value; } }


        private int? _TRUCK_ID;
        public int? TRUCK_ID
        { get { return _TRUCK_ID; } set { _TRUCK_ID = value; } }

        private decimal? _INSURANCE_COST;
        public decimal? INSURANCE_COST
        { get { return _INSURANCE_COST; } set { _INSURANCE_COST = value; } }

        private decimal? _INSURANCE_PREMIUM;
        public decimal? INSURANCE_PREMIUM
        { get { return _INSURANCE_PREMIUM; } set { _INSURANCE_PREMIUM = value; } }

        private string _LICENSE_TRANSPORTNO;
        public string LICENSE_TRANSPORTNO
        { get { return _LICENSE_TRANSPORTNO; } set { _LICENSE_TRANSPORTNO = value; } }

        private string _LICENSE_TRANSPORT_DATE_START;
        public string LICENSE_TRANSPORT_DATE_START
        { get { return _LICENSE_TRANSPORT_DATE_START; } set { _LICENSE_TRANSPORT_DATE_START = value; } }

        private string _LICENSE_TRANSPORT_DATE_END;
        public string LICENSE_TRANSPORT_DATE_END
        { get { return _LICENSE_TRANSPORT_DATE_END; } set { _LICENSE_TRANSPORT_DATE_END = value; } }

    }
}