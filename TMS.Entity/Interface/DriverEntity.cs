﻿using System;

namespace TMS.Entity.Interface
{
    [Serializable]
    public class DriverEntity
    {
        private int? _DRIVER_ID;
        public int? DRIVER_ID
        { get { return _DRIVER_ID; } set { _DRIVER_ID = value; } }

        private string _DRIVER_CODE;
        public string DRIVER_CODE
        { get { return _DRIVER_CODE; } set { _DRIVER_CODE = value; } }

        private string _PERSONAL_CODE;
        public string PERSONAL_CODE
        { get { return _PERSONAL_CODE; } set { _PERSONAL_CODE = value; } }

        private string _CARRIER;
        public string CARRIER
        { get { return _CARRIER; } set { _CARRIER = value; } }

        private string _FIRSTNAME;
        public string FIRSTNAME
        { get { return _FIRSTNAME; } set { _FIRSTNAME = value; } }

        private string _LASTNAME;
        public string LASTNAME
        { get { return _LASTNAME; } set { _LASTNAME = value; } }

        private string _STATUS;
        public string STATUS
        { get { return _STATUS; } set { _STATUS = value; } }

        private string _LICENSE_NO;
        public string LICENSE_NO
        { get { return _LICENSE_NO; } set { _LICENSE_NO = value; } }

        private string _LICENSE_FROM;
        public string LICENSE_FROM
        { get { return _LICENSE_FROM; } set { _LICENSE_FROM = value; } }

        private string _LICENSE_TO;
        public string LICENSE_TO
        { get { return _LICENSE_TO; } set { _LICENSE_TO = value; } }

        private string _TELEPHONE_1;
        public string TELEPHONE_1
        { get { return _TELEPHONE_1; } set { _TELEPHONE_1 = value; } }

        private string _TELEPHONE_2;
        public string TELEPHONE_2
        { get { return _TELEPHONE_2; } set { _TELEPHONE_2 = value; } }
    }
}