﻿using System;
using System.Data;

namespace TMS.Entity.Interface
{
    [Serializable]
    public class TUEntity
    {
        private string _EXPIRE_DAT;
        public string EXPIRE_DAT
        { get { return _EXPIRE_DAT; } set { _EXPIRE_DAT = value; } }

        private string _LANGUAGE;
        public string LANGUAGE
        { get { return _LANGUAGE; } set { _LANGUAGE = value; } }

        private string _L_NUMBER;
        public string L_NUMBER
        { get { return _L_NUMBER; } set { _L_NUMBER = value; } }

        private string _TU_ID;
        public string TU_ID
        { get { return _TU_ID; } set { _TU_ID = value; } }

        private string _TU_MAXWGT;
        public string TU_MAXWGT
        { get { return _TU_MAXWGT; } set { _TU_MAXWGT = value; } }

        private string _TU_NUMBER;
        public string TU_NUMBER
        { get { return _TU_NUMBER; } set { _TU_NUMBER = value; } }

        private string _TU_STATUS;
        public string TU_STATUS
        { get { return _TU_STATUS; } set { _TU_STATUS = value; } }

        private string _TU_TEXT;
        public string TU_TEXT
        { get { return _TU_TEXT; } set { _TU_TEXT = value; } }

        private string _TU_TYPE;
        public string TU_TYPE
        { get { return _TU_TYPE; } set { _TU_TYPE = value; } }

        private string _TU_UNLWGT;
        public string TU_UNLWGT
        { get { return _TU_UNLWGT; } set { _TU_UNLWGT = value; } }

        private string _VOL_UOM;
        public string VOL_UOM
        { get { return _VOL_UOM; } set { _VOL_UOM = value; } }

        private string _WGT_UOM;
        public string WGT_UOM
        { get { return _WGT_UOM; } set { _WGT_UOM = value; } }

        private string _ZSTART_DAT;
        public string ZSTART_DAT
        { get { return _ZSTART_DAT; } set { _ZSTART_DAT = value; } }

        private DataTable _CMP_ITEM;
        public DataTable CMP_ITEM
        { get { return _CMP_ITEM; } set { _CMP_ITEM = value; } }
    }
}