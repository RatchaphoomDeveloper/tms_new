﻿using System;
using System.Data;

namespace TMS.Entity.Interface
{
    [Serializable]
    public class VehicleEntity
    {
        private string _CARRIER;
        public string CARRIER
        { get { return _CARRIER; } set { _CARRIER = value; } }

        private string _CLASS_GRP;
        public string CLASS_GRP
        { get { return _CLASS_GRP; } set { _CLASS_GRP = value; } }

        private string _DEPOT;
        public string DEPOT
        { get { return _DEPOT; } set { _DEPOT = value; } }

        private string _LANGUAGE;
        public string LANGUAGE
        { get { return _LANGUAGE; } set { _LANGUAGE = value; } }

        private string _REG_DATE;
        public string REG_DATE
        { get { return _REG_DATE; } set { _REG_DATE = value; } }

        private string _ROUTE;
        public string ROUTE
        { get { return _ROUTE; } set { _ROUTE = value; } }

        private string _TPPOINT;
        public string TPPOINT
        { get { return _TPPOINT; } set { _TPPOINT = value; } }

        private string _VEHICLE;
        public string VEHICLE
        { get { return _VEHICLE; } set { _VEHICLE = value; } }

        private string _VEH_ID;
        public string VEH_ID
        { get { return _VEH_ID; } set { _VEH_ID = value; } }

        private string _VEH_STATUS;
        public string VEH_STATUS
        { get { return _VEH_STATUS; } set { _VEH_STATUS = value; } }

        private string _VEH_TEXT;
        public string VEH_TEXT
        { get { return _VEH_TEXT; } set { _VEH_TEXT = value; } }

        private string _VEH_TYPE;
        public string VEH_TYPE
        { get { return _VEH_TYPE; } set { _VEH_TYPE = value; } }

        private string _VHCLSIGN;
        public string VHCLSIGN
        { get { return _VHCLSIGN; } set { _VHCLSIGN = value; } }

        private DataTable _TU_ITEM;
        public DataTable TU_ITEM
        { get { return _TU_ITEM; } set { _TU_ITEM = value; } }
    }
}