﻿using System;
using System.Data;
using System.Windows.Forms;
using TMS_BLL.Transaction.Complain;
using System.Threading;
using TMS_BLL.Transaction.AutoStatus;
using TMS_BLL.Master;
using EmailHelper;
using System.Text;
using System.Web.UI;
using System.Web.Security;
using System.Globalization;

namespace TMS_DRIVER_JOB
{
    public partial class frmMain : Form
    {
        DataTable dtDriverExpire;
        public static int VendorEmpDocExpired_Alert = 24;//แจ้งเตือนข้อมูล พขร. ใกล้หมดอายุ 
        public static int VendorEmpDocExpired_InActive = 25;//แจ้งเตือนข้อมูล พขร. หมดอายุ และระงับ) 
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitialForm();
        }

        private void InitialForm()
        {
            try
            {
                VendorBLL.Instance.ErrorLogInsert("1", "Start Job", "สำเร็จ", "TMSDriverJob");
                this.DriverCardExpire();              //อีเมล์แจ้งเตือน เอกสารหมดอายุ 
                this.DriverCardExpireAndInactive();   //อีเมล์แจ้งเตือน เอกสารหมดอายุ และ ระงับ พขร. 
                VendorBLL.Instance.ErrorLogInsert("1", "Stop Job", "สำเร็จ", "TMSDriverJob");
            }
            catch (Exception ex)
            {
                VendorBLL.Instance.ErrorLogInsert("0", "Start Job Fail", ex.Message, "TMSDriverJob");
            }
            finally
            {
                Thread.Sleep(2000);
                Application.Exit();
            }
        }

        private void DriverCardExpire()
        {
            try
            {
                dtDriverExpire = UserBLL.Instance.DocExpireBLL(VendorEmpDocExpired_Alert);
                for (int i = 0; i < dtDriverExpire.Rows.Count; i++)
                {
                    sentEmailToVendor(dtDriverExpire, VendorEmpDocExpired_Alert, i);
                    VendorBLL.Instance.ErrorLogInsert("1", "เตือนเอกสารหมดอายุ", "สำเร็จ ส่งอีเมลไป " + dtDriverExpire.Rows[i]["VENDOR_EMAIL"].ToString(), "TMSDriverJob");
                }
            }
            catch (Exception ex)
            {
                VendorBLL.Instance.ErrorLogInsert("0", "ล้มเหลว เตือนเอกสารหมดอายุ", ex.Message, "TMSDriverJob");
                // throw new Exception(ex.Message);                
            }
        }

        private void DriverCardExpireAndInactive()
        {
            try
            {
                dtDriverExpire = UserBLL.Instance.DocExpireAndInactiveBLL(VendorEmpDocExpired_InActive);
                for (int i = 0; i < dtDriverExpire.Rows.Count; i++)
                {
                    //Step1 Update EmpStatus to Inactive (All)                    
                    updateDriverStatus(dtDriverExpire);
                    //Step2 Send Email
                    sentEmailToVendor(dtDriverExpire, VendorEmpDocExpired_InActive, i);
                    VendorBLL.Instance.ErrorLogInsert("1", "เตือนเอกสารหมดอายุ และระงับ", "สำเร็จ ส่งอีเมลไป " + dtDriverExpire.Rows[i]["VENDOR_EMAIL"].ToString(), "TMSDriverJob");

                }
            }
            catch (Exception ex)
            {
                VendorBLL.Instance.ErrorLogInsert("0", "เตือนเอกสารหมดอายุ และระงับ", ex.Message, "TMSDriverJob");
                //throw new Exception(ex.Message);
            }
        }

        private void updateDriverStatus(DataTable dtDriver)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string result = string.Empty;
                string getUploadName = "";
                string[] resultCheck;
                dtDriver.Columns.Add("STATUS");
                dtDriver.Columns.Add("MESSAGE");
                for (int i = 0; i < dtDriver.Rows.Count; i++)
                {
                    string DriverStatus = string.Empty;
                    DriverStatus = "1";//ระงับ พขร
                    try
                    {
                        SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                        {
                            DRIVER_CODE = dtDriver.Rows[i]["DRIVER_CODE"].ToString(),
                            PERS_CODE = dtDriver.Rows[i]["PERS_CODE"].ToString(),
                            LICENSE_NO = dtDriver.Rows[i]["LICENSE_NO"].ToString(),
                            LICENSENOE_FROM = DateTime.Parse(dtDriver.Rows[i]["VALID_FROM"] + "").ToString("dd/MM/yyyy"),
                            LICENSENO_TO = DateTime.Parse(dtDriver.Rows[i]["VALID_TO"] + "").ToString("dd/MM/yyyy"),
                            Phone_1 = (string.Equals(dtDriver.Rows[i]["STEL"].ToString(), string.Empty)) ? "-" : dtDriver.Rows[i]["STEL"].ToString(),
                            Phone_2 = (string.Equals(dtDriver.Rows[i]["STEL2"].ToString(), string.Empty)) ? "-" : dtDriver.Rows[i]["STEL2"].ToString(),
                            FIRST_NAME = dtDriver.Rows[i]["FIRST_NAME"].ToString(),
                            LAST_NAME = dtDriver.Rows[i]["LAST_NAME"].ToString(),
                            CARRIER = dtDriver.Rows[i]["STRANS_ID"].ToString(),
                            DRV_STATUS = DriverStatus

                            //DRIVER_CODE = "5500006195",
                            //PERS_CODE = "1251288843327",
                            //LICENSE_NO = "สบ.00227/53",
                            //LICENSENOE_FROM = "25/01/2016",
                            //LICENSENO_TO = "25/02/2017",
                            //Phone_1 = "088",
                            //Phone_2 = "089",
                            //FIRST_NAME = "sakchai",
                            //LAST_NAME = "weaweawe",
                            //CARRIER = "0010001778",
                            //DRV_STATUS = "1"
                        };

                        result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();

                        resultCheck = result.Split(':');

                        dtDriver.Rows[i]["STATUS"] = resultCheck[0];
                        if (string.Equals(dtDriver.Rows[i]["STATUS"].ToString(), "Y"))
                            dtDriver.Rows[i]["MESSAGE"] = string.Empty;
                        else
                            dtDriver.Rows[i]["MESSAGE"] = resultCheck[1];

                        //Update TMS
                        string Detail = "";
                        bool isRes, isRes2 = false;
                        isRes = VendorBLL.Instance.VendorUpdateInActiveBySemployeeid(dtDriver.Rows[i]["SEMPLOYEEID"].ToString() + string.Empty, "0");
                        isRes2 = VendorBLL.Instance.VendorUpdateSAPInActiveBySemployeeid(dtDriver.Rows[i]["SEMPLOYEEID"].ToString() + string.Empty, "1");

                        //Log
                        if (isRes)
                        {
                            Detail += "Updated Inactive = 0 <br/>";
                        }
                        if (isRes2)
                        {
                            Detail += "Updated DRVStatus = 1 <br/>";
                        }
                        getUploadName = dtDriver.Rows[i]["UPLOAD_NAME"].ToString();
                        Detail += "พขร. เอกสาร " + getUploadName + " หมดอายุ<br/>";
                        Detail += "สถานะการทำงาน : ระงับ";
                        VendorBLL.Instance.LogInsert(Detail, dtDriver.Rows[i]["PERS_CODE"].ToString(), null, "0", "0", 441, dtDriver.Rows[i]["STRANS_ID"].ToString(), 0);

                        VendorBLL.Instance.ErrorLogInsert("1", "เตือนเอกสารหมดอายุ และระงับ", "สำเร็จ อัพเดทข้อมูล พขร. " + dtDriver.Rows[i]["PERS_CODE"].ToString(), "TMSDriverJob");
                    }
                    catch (Exception ex)
                    {
                        dtDriver.Rows[i]["STATUS"] = "N";
                        dtDriver.Rows[i]["MESSAGE"] = ex.Message;
                        VendorBLL.Instance.ErrorLogInsert("0", "เตือนเอกสารหมดอายุ และระงับ", ex.Message, "TMSDriverJob");
                    }

                }

            }
            catch (Exception ex)
            {

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void sentEmailToVendor(DataTable dtDriverExpire, int templateID, int i)
        {
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectAllBLL(" AND M_EMAIL_TEMPLATE.TEMPLATE_ID = " + templateID);
            DataTable dtGetCCEmail = VendorBLL.Instance.GetSpecialCC_TeamStatusEmailBLL();
            DataTable dtComplainEmail = new DataTable();
            if (dtTemplate.Rows.Count > 0)
            {
                // string EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, 0, "", false, false);
                //string getVendorEmail = dtDriverExpire.Rows[i]["VENDOR_EMAIL"].ToString(); 

                //เพิ่มให้ระบบส่งเมลเพิ่มไปยัง pairuch.k@pttplc.com(พจ รข.) - ขอจากคุณซัน 24 Jan 2018 
                string EmailList = dtDriverExpire.Rows[i]["VENDOR_EMAIL"].ToString();

                //string EmailList = "zmaliwan.p@pttdigital.com";
                //string EmailList = "zsupachai.p@pttdigital.com";
                string EmailCC = string.Empty;//"nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,THANYAVIT.K@PTTOR.COM";
                //for (int cci = 0; cci < dtGetCCEmail.Rows.Count; cci++)
                //{
                //    EmailCC += dtGetCCEmail.Rows[cci]["CCEMAIL"].ToString() + ",";
                //}

                // string test2 = dtGetCCEmail.Rows[0]["CCEMAIL"].ToString();

                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                //Subject = Subject.Replace("{Month}", dtDriverExpire.Rows[i]["checkMONTH"].ToString());

                Body = Body.Replace("{EmpName}", dtDriverExpire.Rows[i]["FULLNAME"].ToString());
                Body = Body.Replace("{DocType}", dtDriverExpire.Rows[i]["UPLOAD_NAME"].ToString());
                Body = Body.Replace("{Vendor}", dtDriverExpire.Rows[i]["VENDOR_NAME"].ToString());
                Body = Body.Replace("{DayLeft}", dtDriverExpire.Rows[i]["DAYLEFT"].ToString());
                string getDateExpire = Convert.ToDateTime(dtDriverExpire.Rows[i]["EXPIRE_DATE"]).ToString("dd MMMM yyyy", new CultureInfo("th-TH", true));
                Body = Body.Replace("{ExpiredDate}", getDateExpire);

                Body = Body.Replace("{Month}", dtDriverExpire.Rows[i]["checkMONTH"].ToString());

                MailService.SendMail(EmailList, Subject, Body, EmailCC, "VendorEmployeeAdd", "EMAIL");
            }
        }
        public static string GetClickHere(string URL)
        {
            return "<a href=\"" + URL + "\">คลิกที่นี่</a>";
        }


    }
}

