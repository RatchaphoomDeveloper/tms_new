﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMS_D_CreateDriver_SYNC_OUT_SI;
using TMS_D_UpdateDriver_SYNC_OUT_SI;
using System.Data.OracleClient;
//using System.Web.Configuration;
using System.Configuration;
using System.Globalization;
using System.Data;
/// <summary>
/// Summary description for SAP_Create_Driver
/// </summary>
public class SAP_Create_Driver
{
    public SAP_Create_Driver()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #region Data
    private string fDRIVERCODE;
    private string fPERSCODE;
    private string fPERSCODE_FROM;
    private string fPERSCODE_TO;
    private string fCARRIER;
    private string fFIRST_NAME;
    private string fLAST_NAME;
    private string fDRVSTATUS;
    private string fLICENSENO;
    private string fLICENSENOE_FROM;
    private string fLICENSENO_TO;
    private string fPhone_1;
    private string fPhone_2;
    #endregion
    #region Property
    public string DRIVER_CODE
    {
        get { return this.fDRIVERCODE; }
        set { this.fDRIVERCODE = DBNull(value, ""); }
    }
    public string PERS_CODE
    {
        get { return this.fPERSCODE; }
        set { this.fPERSCODE = DBNull(value, ""); }
    }
    public string PERSCODE_FROM
    {
        get { return this.fPERSCODE_FROM; }
        set { this.fPERSCODE_FROM = DBNull(value, ""); }
    }
    public string PERSCODE_TO
    {
        get { return this.fPERSCODE_TO; }
        set { this.fPERSCODE_TO = DBNull(value, ""); }
    }
    public string CARRIER
    {
        get { return this.fCARRIER; }
        set { this.fCARRIER = DBNull(value, ""); }
    }
    public string FIRST_NAME
    {
        get { return this.fFIRST_NAME; }
        set { this.fFIRST_NAME = DBNull(value, ""); }
    }
    public string LAST_NAME
    {
        get { return this.fLAST_NAME; }
        set { this.fLAST_NAME = DBNull(value, ""); }
    }
    public string DRV_STATUS
    {
        get { return this.fDRVSTATUS; }
        set { this.fDRVSTATUS = DBNull(value, ""); }
    }
    public string LICENSE_NO
    {
        get { return this.fLICENSENO; }
        set { this.fLICENSENO = DBNull(value, ""); }
    }
    public string LICENSENOE_FROM
    {
        get { return this.fLICENSENOE_FROM; }
        set { this.fLICENSENOE_FROM = DBNull(value, ""); }
    }
    public string LICENSENO_TO
    {
        get { return this.fLICENSENO_TO; }
        set { this.fLICENSENO_TO = DBNull(value, ""); }
    }
    public string Phone_1
    {
        get { return this.fPhone_1; }
        set { this.fPhone_1 = DBNull(value, ""); }
    }
    public string Phone_2
    {
        get { return this.fPhone_2; }
        set { this.fPhone_2 = DBNull(value, ""); }
    }
    #endregion
    #region Method
    private string DBNull(string _data, string _defVal)
    {//#!
        return _data + "" == "" ? _defVal : _data;
    }
    public string CRT_Driver_SYNC_OUT_SI()
    {
        string result = ""; string Msg = "", sErr = "";
        bool IsUpdated = false;
        bool IsCreated = false;
        using (OracleConnection connection = new OracleConnection(WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            using (var _SIClient = new CreateDriver_SYNC_OUT_SIClient("HTTP_Port4"))
            {
                _SIClient.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
                _SIClient.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";

                //LICENSE_NO = "กพ.00036/47";
                //LICENSENOE_FROM = "2527-01-01";
                //LICENSENO_TO = "2530-01-31";
                //Phone_1 = "0851530000";
                //Phone_2 = "0850000000";
                //FIRST_NAME = "FIRST_NAME";
                //LAST_NAME = "LAST_NAME";
                //PERS_CODE = "1820100001261";
                //CARRIER = "PHTDPRT";

                #region Drv LICENSE
                int Driver_Items = 0; int idx_Items = 0;
                Driver_Items += (LICENSE_NO != "") ? 1 : 0;
                Driver_Items += (Phone_1 != "") ? 1 : 0;
                Driver_Items += (Phone_2 != "") ? 1 : 0;

                var Driver_License = new CreateDriver_Src_Req_DTItem[Driver_Items];
                var _DTItem = new CreateDriver_Src_Req_DTItem();
                if (LICENSE_NO.Trim() != "")
                {
                    _DTItem = new CreateDriver_Src_Req_DTItem();
                    _DTItem.LICENSENO = LICENSE_NO;
                    _DTItem.LICENSETYP = "Z001";
                    _DTItem.VALID_FROM = _DTItem.VALID_TO = "1900-01-01";
                    if (LICENSENOE_FROM != "")
                    {
                        //_DTItem.VALID_FROM = Convert.ToDateTime(LICENSENOE_FROM).ToString("yyyy-MM-dd", new CultureInfo("en-US"));
                        if (LICENSENOE_FROM != null)
                        {
                            DateTime? dtime = new DateTime(1900, 01, 01);
                            dtime = new PageBase().ConvertToDateNull(LICENSENOE_FROM);
                            if (dtime == null)
                            {
                                dtime = new DateTime(1900, 01, 01);
                            }
                            _DTItem.VALID_FROM = dtime.Value.ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            _DTItem.VALID_FROM = "1900-01-01";
                        }
                    }
                    else
                    {
                        _DTItem.VALID_FROM = "1900-01-01";
                    }
                    if (LICENSENO_TO != "")
                    {
                        // _DTItem.VALID_TO = Convert.ToDateTime(LICENSENO_TO).ToString("yyyy-MM-dd", new CultureInfo("en-US"));
                        if (LICENSENO_TO != null)
                        {
                            DateTime? dtime = new DateTime(1900, 01, 01);
                            dtime = new PageBase().ConvertToDateNull(LICENSENO_TO);
                            if (dtime == null)
                            {
                                dtime = new DateTime(1900, 01, 01);
                            }
                            _DTItem.VALID_TO = dtime.Value.ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            _DTItem.VALID_TO = "1900-01-01";
                        }
                    }
                    else
                    {
                        _DTItem.VALID_TO = "1900-01-01";
                    }
                    Driver_License[idx_Items] = _DTItem;
                    idx_Items++;
                }
                else { sErr += "กรุณาระบุรหัสใบขับขี่"; }

                //if (Phone_1 != "")
                //{
                //    _DTItem = new CreateDriver_Src_Req_DTItem();
                //    _DTItem.LICENSENO = Phone_1;
                //    _DTItem.LICENSETYP = "Z002";
                //    _DTItem.VALID_FROM = "1900-01-01"; _DTItem.VALID_TO = "1900-01-01";
                //    Driver_License[idx_Items] = _DTItem;
                //    idx_Items++;
                //}
                //if (Phone_2 != "")
                //{
                //    _DTItem = new CreateDriver_Src_Req_DTItem();
                //    _DTItem.LICENSENO = Phone_2;
                //    _DTItem.LICENSETYP = "Z002";
                //    _DTItem.VALID_FROM = "1900-01-01"; _DTItem.VALID_TO = "1900-01-01";
                //    Driver_License[idx_Items] = _DTItem;
                //    idx_Items++;
                //}
                #endregion

                var _ReqDate = new CreateDriver_Src_Req_DT();
                _ReqDate.DRIVERCODE = "";
                _ReqDate.FIRST_NAME = FIRST_NAME;
                _ReqDate.LAST_NAME = LAST_NAME;
                _ReqDate.PERSCODE = PERS_CODE;
                _ReqDate.CARRIER = CARRIER;
                _ReqDate.DRVSTATUS = " ";
                if (Driver_License.Length > 0)
                {
                    _ReqDate.GT_DRIVER_LINCENSE = Driver_License;
                }

                var Driver = _SIClient.CreateDriver_SYNC_OUT_SI(_ReqDate);
                int Driver_SAP = Driver.GT_RETURN_DR.Length > 0 ? (Driver.GT_RETURN_DR.Length - 1) : 0;

                for (int i = 0; i <= Driver_SAP; i++)
                {
                    IsCreated = !Driver.GT_RETURN_DR[i].MSGTYP.Equals("E");
                    if (Driver.GT_RETURN_DR[i].MSGTYP.Equals("E"))
                    {
                        //int nExists = 0;
                        Msg += Driver.GT_RETURN_DR[i].MESSAGE;
                        //foreach (var Return in Driver.GT_RETURN_DR)
                        //{
                        //    nExists += ((Return.MESSAGE != "มีข้อมูลพนักงานขับรถในระบบแล้ว") ? 0 : 1);
                        //    Msg += "," + Return.MESSAGE;
                        //}
                        //if (nExists > 0)
                        //{//มีข้อมูลพนักงานขับรถในระบบแล้ว
                        //    //UDP_Driver_SYNC_OUT_SI();
                        //}
                    }
                    else
                    {//Driver 4700000222 has been created
                        Msg = Driver.GT_RETURN_DR[i].MESSAGE.ToLower().Replace("has been created", "").Replace("driver", "").Trim();
                        //เช็คว่าถ้าผ่านให้ จะมีบางกรณีที่ส่ง alert ว่ามี Licenno แล้ว จึงต้องทำการเลือก array ตัวสุดท่าย ซึ่งจะเป็นข้อความ Driver 4700000222 has been created มาเพื่อนำไอดีไปแอดข้อมูล
                        if (Msg.Contains("has been created"))
                        {

                        }
                        else
                        {
                            Msg = Driver.GT_RETURN_DR[i].MESSAGE.ToLower().Replace("has been created", "").Replace("driver", "").Trim();
                        }
                    }
                }
            }
        }

        return (IsCreated ? "Y" : "N") + ":" + Msg;
    }
    public string UDP_Driver_SYNC_OUT_SI()
    {
        string result = ""; string Msg = "", sErr = "";
        bool IsUpdated = false;
        using (var _SIClient = new UpdateDriver_SYNC_OUT_SIClient("HTTP_Port5"))
        {
            _SIClient.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
            _SIClient.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";

            if (DRIVER_CODE != "")
            {
                string[] CHKDB = HasDriver(DRIVER_CODE, PERS_CODE);


                #region Drv LICENSE
                int Driver_Items = 0; int idx_Items = 0;
                Driver_Items += (LICENSE_NO != "") ? 1 : 0;
                Driver_Items += (Phone_1 != "") ? 1 : 0;
                Driver_Items += (Phone_2 != "") ? 1 : 0;

                var Driver_License = new UpdateDriver_Src_Req_DTItem[Driver_Items];
                var _DTItem = new UpdateDriver_Src_Req_DTItem();
                if (LICENSE_NO != "")
                {
                    _DTItem = new UpdateDriver_Src_Req_DTItem();
                    _DTItem.LICENSENO = LICENSE_NO;
                    _DTItem.LICENSETYP = "Z001";
                    if (LICENSENOE_FROM != "")
                    {
                        if (LICENSENOE_FROM != null)
                        {
                            DateTime? dtime = new DateTime(1900, 01, 01);
                            dtime = new PageBase().ConvertToDateNull(LICENSENOE_FROM);
                            if (dtime == null)
                            {
                                dtime = new DateTime(1900, 01, 01);
                            }
                            _DTItem.VALID_FROM = dtime.Value.ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            _DTItem.VALID_FROM = "1900-01-01";
                        }
                    }
                    else
                    {
                        _DTItem.VALID_FROM = "1900-01-01";
                    }
                    if (LICENSENO_TO != "")
                    {
                        if (LICENSENO_TO != null)
                        {
                            DateTime? dtime = new DateTime(1900, 01, 01);
                            dtime = new PageBase().ConvertToDateNull(LICENSENO_TO);
                            if (dtime == null)
                            {
                                dtime = new DateTime(1900, 01, 01);
                            }
                            _DTItem.VALID_TO = dtime.Value.ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            _DTItem.VALID_TO = "1900-01-01";
                        }
                    }
                    else
                    {
                        _DTItem.VALID_TO = "1900-01-01";
                    }
                    Driver_License[idx_Items] = _DTItem;
                    idx_Items++;
                }
                else { sErr += "กรุณาระบุรหัสใบขับขี่"; }

                //if (Phone_1 != "")
                //{
                //    _DTItem = new UpdateDriver_Src_Req_DTItem();
                //    _DTItem.LICENSENO = Phone_1;
                //    _DTItem.LICENSETYP = "Z002";
                //    _DTItem.VALID_FROM = "1900-01-01"; _DTItem.VALID_TO = "1900-01-01";
                //    Driver_License[idx_Items] = _DTItem;
                //    idx_Items++;
                //}
                //if (Phone_2 != "")
                //{
                //    _DTItem = new UpdateDriver_Src_Req_DTItem();
                //    _DTItem.LICENSENO = Phone_2;
                //    _DTItem.LICENSETYP = "Z002";
                //    _DTItem.VALID_FROM = "1900-01-01"; _DTItem.VALID_TO = "1900-01-01";
                //    Driver_License[idx_Items] = _DTItem;
                //    idx_Items++;
                //}
                #endregion
                var _ReqDate = new UpdateDriver_Src_Req_DT();
                _ReqDate.DRIVERCODE = DRIVER_CODE;
                _ReqDate.FIRST_NAME = FIRST_NAME;
                _ReqDate.LAST_NAME = LAST_NAME;
                _ReqDate.PERSCODE = PERS_CODE;
                _ReqDate.CARRIER = CARRIER;
                _ReqDate.DRVSTATUS = DRV_STATUS;
                //_ReqDate.DRVSTATUS = " ";
                if (Driver_License.Length > 0)
                {
                    _ReqDate.GT_DRIVER_LINCENSE = Driver_License;
                }

                var Driver = _SIClient.UpdateDriver_SYNC_OUT_SI(_ReqDate);
                int Driver_SAP = Driver.GT_RETURN_DR.Length > 0 ? (Driver.GT_RETURN_DR.Length - 1) : 0;

                for (int i = 0; i <= Driver_SAP; i++)
                {

                    IsUpdated = !Driver.GT_RETURN_DR[i].MSGTYP.Equals("E");
                    if (Driver.GT_RETURN_DR[i].MSGTYP.Equals("E"))
                    {
                        Msg += "," + Driver.GT_RETURN_DR[i].MESSAGE;
                        //int nExists = 0;
                        //foreach (var Return in Driver.GT_RETURN_DR)
                        //{
                        //    //nExists += ((Return.MESSAGE != "มีข้อมูลพนักงานขับรถในระบบแล้ว") ? 0 : 1);
                        //    Msg += "," + Return.MESSAGE;
                        //}
                    }
                    else
                    {//Driver 4700000222 has been created
                        if (Driver.GT_RETURN_DR[i].MSGTYP.Equals("S"))
                        {
                            Msg += Driver.GT_RETURN_DR[i].MESSAGE.ToLower().Replace("has been changed", "").Replace("driver", "").Trim();
                        }
                    }
                }

            }
            else { sErr += "ไม่พบรหัสพนักงานในระบบ"; }
        }
        return (IsUpdated ? "Y" : "N") + ":" + Msg;
    }
    public string[] HasDriver(string drv_id, string pe_id)
    {
        string[] sResult = { "N", "" };
        using (OracleConnection connection = new OracleConnection(WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable dt = new DataTable();
            new OracleDataAdapter(@"SELECT * FROM TEMPLOYEE_SAP WHERE 1=1 AND (PERS_CODE='" + pe_id + "') ORDER BY DUPDATE DESC", connection).Fill(dt);
            if (dt.Rows.Count > 0)
            {
                string sMsg = "";
                foreach (DataRow _dr in dt.Rows)
                {
                    sMsg += (_dr["PERS_CODE"] + "" == pe_id) ? _dr["SEMPLOYEEID"] + "" : "";
                }
                sResult = new string[] { "Y", sMsg };
            }
        }
        return sResult;
    }
    #endregion
}