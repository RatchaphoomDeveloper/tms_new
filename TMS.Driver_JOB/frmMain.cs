﻿using System;
using System.Data;
using System.Threading;
using System.Windows.Forms;
using System.Linq;
using EmailHelper;
using TMS_BLL.Master;
//using TMS.Business.Master;
using TMS.Entity.Interface;
using EmailHelper;
using TMS_BLL.Transaction.Complain;
using System.Configuration;
//using TMS_DAL.ConnectionDAL;
//using TMS.SAPHelper.Interface;

namespace TMS.Driver_JOB
{
    public partial class frmMain : Form
    {
        DataTable dtCard;
        public static int VendorEmployeeDocumentExpired = 121;//แจ้งเรื่องอายุเอกสารหมดอายุ
        public frmMain()
        {
            InitializeComponent();
            
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitialForm();
        }

        private void InitialForm()
        {
            this.Cursor = Cursors.WaitCursor;

            try
            {
                this.DriverCardExpire();                    //อีเมล์แจ้งเตือน เอกสารหมดอายุ 
                this.DriverCardExpireAndInactive();             //อีเมล์แจ้งเตือน เอกสารหมดอายุ และ ระงับ พขร. 
            }
            catch (Exception ex)
            {

            }
            finally
            {
                Thread.Sleep(2000);
                this.Cursor = Cursors.Default;
                Application.Exit();
            }
        }


        private void DriverCardExpire()
        {
            try
            {
                dtCard = UserBLL.Instance.DocExpireBLL(VendorEmployeeDocumentExpired);
                for (int i = 0; i < dtCard.Rows.Count; i++)
                {
                    //ConfigValue.EmailAccident7
                    //  public static int VendorEmployeeDocumentExpired = 121;//แจ้งเรื่องอายุเอกสารหมดอายุ
                    DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(VendorEmployeeDocumentExpired);
                    DataTable dtComplainEmail = new DataTable();
                    if (dtTemplate.Rows.Count > 0)
                    {
                        //MailService.SendMail(9, 2, string.Empty, string.Empty, dtCard);
                        string getname = dtTemplate.Rows[0]["STATUS"].ToString();
                        string EmailList = "zsupachai.p@pttdigital.com"; //ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, 0, "", false, false);
                        //string EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dr["SVENDORID"] + string.Empty, false, false);
                        string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                        string Body = dtTemplate.Rows[0]["BODY"].ToString();
                        //Subject = Subject.Replace("{ACCID}", accID);
                        //Body = Body.Replace("{ACCID}", accID);
                         MailService.SendMail(EmailList, Subject, Body);
                         //MailService.SendMail("suphakit.k@pttplc.com" + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), Session["SVDID"].ToString(), true, false), Subject, Body);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void DriverCardExpireAndInactive()
        {
            try
            {
                dtCard = UserBLL.Instance.DocExpireAndInactiveBLL(VendorEmployeeDocumentExpired);
                for (int i = 0; i < dtCard.Rows.Count; i++)
                 {
                    //Step1 Update Status (All)
                     string sMsg = ""; 
                    string QUERY = "SELECT * FROM SAP_DRIVER_MASTER_DATA WHERE PERS_CODE = '" + dtCard.Rows[0]["EMP_ID"].ToString() + "'";
                     DataTable dtSap = UserBLL.Instance.GetQueryDataBLL(QUERY);

                    SAP_Create_Driver veh_driver = new SAP_Create_Driver();
                    veh_driver.DRIVER_CODE = dtSap.Rows.Count > 0 ? dtSap.Rows[0]["DRIVER_CODE"] + "" : EMPSAPID;

                    //veh_driver.PERS_CODE = Percode;
                    //veh_driver.CARRIER = vendor;
                    //veh_driver.FIRST_NAME = txtName.Text.Trim();
                    //veh_driver.LAST_NAME = txtSurName.Text.Trim();
                    veh_driver.DRV_STATUS = "1";
                    //veh_driver.LICENSE_NO = drivelicence;
                    //veh_driver.Phone_1 = tel;
                    //veh_driver.Phone_2 = tel2;
                    //veh_driver.LICENSENOE_FROM = dedtstartlicence.Text;
                    //veh_driver.LICENSENO_TO = dedtEndlicence.Text;
                    sMsg = veh_driver.CRT_Driver_SYNC_OUT_SI();
                     //Step2 Send Email
                     DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(VendorEmployeeDocumentExpired);
                     DataTable dtComplainEmail = new DataTable();
                     if (dtTemplate.Rows.Count > 0)
                     {
                         // MailService.SendMail(9, 2, string.Empty, string.Empty, dtCard);
                         // string EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, 0, "", false, false);
                         string EmailList = "zsupachai.p@pttdigital.com,"; 
                         string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                         string Body = dtTemplate.Rows[0]["BODY"].ToString();
                          MailService.SendMail(EmailList, Subject, Body);
                     }
                 }
            }
            catch (Exception ex)
             {
                    throw new Exception(ex.Message);
            }
        }

        private DriverEntity GenerateEntity(int i)
        {
            try
            {
                 DriverEntity driverEntity = new DriverEntity
                {
                    DRIVER_ID = int.Parse(dtCard.Rows[i]["DRIVER_ID"] + string.Empty),
                    DRIVER_CODE = (dtCard.Rows[i]["DRIVER_CODE"] + string.Empty).Trim(),
                    PERSONAL_CODE = (dtCard.Rows[i]["CARD_NO"] + string.Empty).Trim().Replace("-", ""),
                    CARRIER = (dtCard.Rows[i]["VENDOR_CODE"] + string.Empty).Trim(),
                    FIRSTNAME = (dtCard.Rows[i]["FIRSTNAME"] + string.Empty).Trim(),
                    LASTNAME = (dtCard.Rows[i]["LASTNAME"] + string.Empty).Trim(),
                    STATUS = "1",
                    LICENSE_NO = (dtCard.Rows[i]["LICENSE"] + string.Empty).Trim(),
                    LICENSE_FROM = (dtCard.Rows[i]["LICENSE_DATE_ISSUE"] + string.Empty).Trim(),
                    LICENSE_TO = (dtCard.Rows[i]["LICENSE_DATE_EXPIRE"] + string.Empty).Trim(),
                    TELEPHONE_1 = (dtCard.Rows[i]["TELEPHONE_1"] + string.Empty).Trim(),
                    TELEPHONE_2 = (dtCard.Rows[i]["TELEPHONE_2"] + string.Empty).Trim()
                };

                return driverEntity;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        
    }
}
