﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TMS_TestWebService.ServiceReference1 {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference1.iTerminalServiceSoap")]
    public interface iTerminalServiceSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/UpdateMAP_DO", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        TMS_TestWebService.ServiceReference1.Response UpdateMAP_DO(TMS_TestWebService.ServiceReference1.MapDOInput MapDO);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetCurrentDate", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.DateTime GetCurrentDate();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/DriverRegister_SYNC_IN", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        TMS_TestWebService.ServiceReference1.STATUS DriverRegister_SYNC_IN(string DEPOT, string VEHICLE_HEAD, string VEHICLE_TAIL, string ITEM_NO, string SPERSONALNO, string SEMPLOYEENAME, string CREATEDATE, string SVENDORID, string DRIVER_NO);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/DeleteQueue", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        TMS_TestWebService.ServiceReference1.Response DeleteQueue(string Depot, string vehicle_no, string drver_no);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3056.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class MapDOInput : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string tu_noField;
        
        private string outboundField;
        
        private int time_WindowField;
        
        private string vehnoField;
        
        private string statusField;
        
        private string depotField;
        
        private string driver_noField;
        
        private string plan_idField;
        
        private string nameField;
        
        private string surnameField;
        
        private string user_typeField;
        
        private System.DateTime plant_dateField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string Tu_no {
            get {
                return this.tu_noField;
            }
            set {
                this.tu_noField = value;
                this.RaisePropertyChanged("Tu_no");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string Outbound {
            get {
                return this.outboundField;
            }
            set {
                this.outboundField = value;
                this.RaisePropertyChanged("Outbound");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public int Time_Window {
            get {
                return this.time_WindowField;
            }
            set {
                this.time_WindowField = value;
                this.RaisePropertyChanged("Time_Window");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string Vehno {
            get {
                return this.vehnoField;
            }
            set {
                this.vehnoField = value;
                this.RaisePropertyChanged("Vehno");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string Status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
                this.RaisePropertyChanged("Status");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public string Depot {
            get {
                return this.depotField;
            }
            set {
                this.depotField = value;
                this.RaisePropertyChanged("Depot");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=6)]
        public string Driver_no {
            get {
                return this.driver_noField;
            }
            set {
                this.driver_noField = value;
                this.RaisePropertyChanged("Driver_no");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=7)]
        public string Plan_id {
            get {
                return this.plan_idField;
            }
            set {
                this.plan_idField = value;
                this.RaisePropertyChanged("Plan_id");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=8)]
        public string Name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
                this.RaisePropertyChanged("Name");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=9)]
        public string Surname {
            get {
                return this.surnameField;
            }
            set {
                this.surnameField = value;
                this.RaisePropertyChanged("Surname");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=10)]
        public string User_type {
            get {
                return this.user_typeField;
            }
            set {
                this.user_typeField = value;
                this.RaisePropertyChanged("User_type");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=11)]
        public System.DateTime Plant_date {
            get {
                return this.plant_dateField;
            }
            set {
                this.plant_dateField = value;
                this.RaisePropertyChanged("Plant_date");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3056.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class STATUS : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string idField;
        
        private string rEMARKField;
        
        private string cSTATUSField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string ID {
            get {
                return this.idField;
            }
            set {
                this.idField = value;
                this.RaisePropertyChanged("ID");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string REMARK {
            get {
                return this.rEMARKField;
            }
            set {
                this.rEMARKField = value;
                this.RaisePropertyChanged("REMARK");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string CSTATUS {
            get {
                return this.cSTATUSField;
            }
            set {
                this.cSTATUSField = value;
                this.RaisePropertyChanged("CSTATUS");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3056.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class Response : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string response_codeField;
        
        private string response_descriptionField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string Response_code {
            get {
                return this.response_codeField;
            }
            set {
                this.response_codeField = value;
                this.RaisePropertyChanged("Response_code");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string Response_description {
            get {
                return this.response_descriptionField;
            }
            set {
                this.response_descriptionField = value;
                this.RaisePropertyChanged("Response_description");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface iTerminalServiceSoapChannel : TMS_TestWebService.ServiceReference1.iTerminalServiceSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class iTerminalServiceSoapClient : System.ServiceModel.ClientBase<TMS_TestWebService.ServiceReference1.iTerminalServiceSoap>, TMS_TestWebService.ServiceReference1.iTerminalServiceSoap {
        
        public iTerminalServiceSoapClient() {
        }
        
        public iTerminalServiceSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public iTerminalServiceSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public iTerminalServiceSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public iTerminalServiceSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public TMS_TestWebService.ServiceReference1.Response UpdateMAP_DO(TMS_TestWebService.ServiceReference1.MapDOInput MapDO) {
            return base.Channel.UpdateMAP_DO(MapDO);
        }
        
        public System.DateTime GetCurrentDate() {
            return base.Channel.GetCurrentDate();
        }
        
        public TMS_TestWebService.ServiceReference1.STATUS DriverRegister_SYNC_IN(string DEPOT, string VEHICLE_HEAD, string VEHICLE_TAIL, string ITEM_NO, string SPERSONALNO, string SEMPLOYEENAME, string CREATEDATE, string SVENDORID, string DRIVER_NO) {
            return base.Channel.DriverRegister_SYNC_IN(DEPOT, VEHICLE_HEAD, VEHICLE_TAIL, ITEM_NO, SPERSONALNO, SEMPLOYEENAME, CREATEDATE, SVENDORID, DRIVER_NO);
        }
        
        public TMS_TestWebService.ServiceReference1.Response DeleteQueue(string Depot, string vehicle_no, string drver_no) {
            return base.Channel.DeleteQueue(Depot, vehicle_no, drver_no);
        }
    }
}
