﻿using TMS.Entity.Interface;
using TMS.SAPHelper.Utility;
using TMS.SAPHelper.WS_VEH_UPDATE;
using System;
using System.Configuration;
using System.Data;

namespace TMS.SAPHelper.Interface
{
    public class Vehical
    {
        #region + Vehicle +
        //public static DataTable CreateVeh(VehicleEntity vehEntity, int CreateBy)
        //{
        //    #region + Example +
        //    //DataTable dtItem = new DataTable();
        //    //dtItem.Columns.Add("VEHICLE");
        //    //dtItem.Columns.Add("SEQ_NMBR");
        //    //dtItem.Columns.Add("TU_NUMBER");

        //    //dtItem.Rows.Add("กท.60-0711", "1", "กท.60-0711");

        //    //VehicleEntity vehEntity = new VehicleEntity
        //    //{
        //    //    CARRIER = "0010001144",
        //    //    CLASS_GRP = "G100",
        //    //    DEPOT = "5102",
        //    //    LANGUAGE = " ",
        //    //    REG_DATE = "2016-12-01",
        //    //    ROUTE = "",
        //    //    TPPOINT = "",
        //    //    VEHICLE = "กท.60-0711",
        //    //    VEH_ID = "YV2JSGOCBCS906186",
        //    //    VEH_STATUS = " ",
        //    //    VEH_TEXT = "SU20000(LPG)",
        //    //    VEH_TYPE = "A130",
        //    //    VHCLSIGN = "",
        //    //    TU_ITEM = dtItem
        //    //};

        //    //DataTable dtResult = LPG.SAPHelper.Interface.Vehical.CreateVeh(vehEntity, UserID);
        //    #endregion

        //    int HeaderID = 0;

        //    try
        //    {
        //        DataTable dtLog = InterfaceTruckBLL.Instance.InterfaceVehSelectBLL(" AND SAP_STATUS = 'S' AND LEG_STATUS = 'S' AND VEH_ID = '" + vehEntity.VEH_ID + "'");
        //        if (dtLog.Rows.Count > 0)
        //        {
        //            vehEntity.VEH_ID = dtLog.Rows[0]["VEH_ID"].ToString();

        //            return UpdateVeh(vehEntity, CreateBy);
        //        }
        //        else
        //        {
        //            VehicleCreate_Sync_Out_SIRequest req = new VehicleCreate_Sync_Out_SIRequest();

        //            using (VehicleCreate_Sync_Out_SIClient CreateVeh = new VehicleCreate_Sync_Out_SIClient("HTTP_Port4"))
        //            {
        //                int ItemCount = vehEntity.TU_ITEM.Rows.Count;

        //                WS_VEH_CREATE.ZO4V2_OIGVTU[] Items = new WS_VEH_CREATE.ZO4V2_OIGVTU[ItemCount];
        //                WS_VEH_CREATE.ZO4V2_RETURN[] Return = new WS_VEH_CREATE.ZO4V2_RETURN[50];

        //                //SAP Login
        //                CreateVeh.ClientCredentials.UserName.UserName = GetConfig.SAP_Username;
        //                CreateVeh.ClientCredentials.UserName.Password = GetConfig.SAP_Password;

        //                DataTable dtResult = new DataTable();
        //                dtResult.Columns.Add("STATUS");
        //                dtResult.Columns.Add("MESSAGE");
        //                dtResult.Columns.Add("DRIVER_CODE");
        //                dtResult.Columns.Add("MSGID");
        //                dtResult.Columns.Add("TCODE");
        //                dtResult.Columns.Add("TU_NUMBER");

        //                //Item
        //                int Count = 0;
        //                WS_VEH_CREATE.ZO4V2_OIGVTU Item = new WS_VEH_CREATE.ZO4V2_OIGVTU();

        //                for (int i = 0; i < ItemCount; i++)
        //                {
        //                    Item = new WS_VEH_CREATE.ZO4V2_OIGVTU();
        //                    Item.VEHICLE = vehEntity.TU_ITEM.Rows[i]["VEHICLE"].ToString();
        //                    Item.SEQ_NMBR = vehEntity.TU_ITEM.Rows[i]["SEQ_NMBR"].ToString();
        //                    Item.TU_NUMBER = vehEntity.TU_ITEM.Rows[i]["TU_NUMBER"].ToString();

        //                    Items[Count++] = Item;
        //                }

        //                HeaderID = InterfaceTruckBLL.Instance.InterfaceInsertBLL(vehEntity, CreateBy);

        //                CreateVeh.VehicleCreate_Sync_Out_SI(vehEntity.CARRIER
        //                    , vehEntity.CLASS_GRP
        //                    , vehEntity.DEPOT
        //                    , vehEntity.LANGUAGE
        //                    , vehEntity.REG_DATE
        //                    , vehEntity.ROUTE
        //                    , vehEntity.TPPOINT
        //                    , vehEntity.VEHICLE
        //                    , vehEntity.VEH_ID
        //                    , vehEntity.VEH_STATUS
        //                    , vehEntity.VEH_TEXT
        //                    , vehEntity.VEH_TYPE
        //                    , vehEntity.VHCLSIGN
        //                    , ref Return
        //                    , ref Items);

        //                if (Return.Length > 0)
        //                {
        //                    int Row = 0;
        //                    if (Return.Length > 1)
        //                        Row = 1;

        //                    dtResult.Rows.Add(Return[Row].MSGTYP, Return[Row].MESSAGE, string.Empty, Return[Row].MSGID, Return[Row].TCODE, Return[Row].VEHICLE);
        //                    InterfaceTruckBLL.Instance.InterfaceUpdateHeaderBLL(HeaderID, 3, Return[Row].MSGTYP, "S", Return[Row].MESSAGE, Return[Row].MSGID, Return[Row].TCODE, Return[Row].VEHICLE, CreateBy);
        //                }

        //                return dtResult;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        InterfaceTruckBLL.Instance.InterfaceUpdateHeaderBLL(HeaderID, 3, string.Empty, "E", ex.Message, string.Empty, string.Empty, string.Empty, CreateBy);
        //        throw new Exception(ex.Message);
        //    }
        //}

        public static DataTable UpdateVeh(VehicleEntity vehEntity, int CreateBy)
        {
            //int HeaderID = 0;
            bool IsUpdated = true;

            try
            {
                UpdateVehicle_SYNC_OUT_SIRequest req = new UpdateVehicle_SYNC_OUT_SIRequest();

                using (UpdateVehicle_SYNC_OUT_SIClient UpdateVeh = new UpdateVehicle_SYNC_OUT_SIClient("HTTP_Port"))
                {
                    string Msg = "", sErr = "";
                    int ItemCount = vehEntity.TU_ITEM.Rows.Count;

                    TMS.SAPHelper.WS_VEH_UPDATE.UpdateVehicle_Src_Req_DTItem[] Items = new TMS.SAPHelper.WS_VEH_UPDATE.UpdateVehicle_Src_Req_DTItem[ItemCount];
                    TMS.SAPHelper.WS_VEH_UPDATE.UpdateVehicle_SYNC_OUT_SIResponse[] Return = new TMS.SAPHelper.WS_VEH_UPDATE.UpdateVehicle_SYNC_OUT_SIResponse[50];

                    //SAP Login
                    UpdateVeh.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"].ToString();
                    UpdateVeh.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"].ToString();

                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("STATUS");
                    dtResult.Columns.Add("MESSAGE");

                    //Item
                    int Count = 0;
                    TMS.SAPHelper.WS_VEH_UPDATE.UpdateVehicle_Src_Req_DTItem Item = new TMS.SAPHelper.WS_VEH_UPDATE.UpdateVehicle_Src_Req_DTItem();

                    for (int i = 0; i < ItemCount; i++)
                    {
                        Item = new TMS.SAPHelper.WS_VEH_UPDATE.UpdateVehicle_Src_Req_DTItem();
                        Item.VEHICLE = vehEntity.TU_ITEM.Rows[i]["VEHICLE"].ToString();
                        Item.SEQ_NMBR = vehEntity.TU_ITEM.Rows[i]["SEQ_NMBR"].ToString();
                        Item.TU_NUMBER = vehEntity.TU_ITEM.Rows[i]["TU_NUMBER"].ToString();

                        Items[Count++] = Item;
                    }

                    //HeaderID = InterfaceTruckBLL.Instance.InterfaceInsertBLL(vehEntity, CreateBy);

                    var Vechicle = new UpdateVehicle_Src_Req_DT()
                    {
                        CARRIER = vehEntity.CARRIER,
                        CLASS_GRP = vehEntity.CLASS_GRP,
                        DEPOT = vehEntity.DEPOT,
                        REG_DATE = vehEntity.REG_DATE,
                        ROUTE = vehEntity.ROUTE,
                        TPPOINT = vehEntity.TPPOINT,
                        VEHICLE = vehEntity.VEHICLE,
                        VEH_ID = vehEntity.VEH_ID,
                        VEH_STATUS = vehEntity.VEH_STATUS,
                        VEH_TEXT = vehEntity.VEH_TEXT,
                        VHCLSIGN = vehEntity.VHCLSIGN,
                        TU_ITEM = Items
                    };

                    try
                    {
                        var Res_VEH = UpdateVeh.UpdateVehicle_SYNC_OUT_SI(Vechicle);
                        int Res_VEH_Row = Res_VEH.GT_RETURN.Length > 0 ? (Res_VEH.GT_RETURN.Length - 1) : 0;
                        for (int i = 0; i <= Res_VEH_Row; i++)
                        {
                            IsUpdated = !Res_VEH.GT_RETURN[i].MSGTYP.Equals("E");
                            if (Res_VEH.GT_RETURN[i].MSGTYP.Equals("E"))
                            {
                                Msg += "," + Res_VEH.GT_RETURN[i].MESSAGE;
                            }
                            dtResult.Rows.Add(Res_VEH.GT_RETURN[i].MSGTYP, Msg);
                        }
                    }
                    catch (Exception ex)
                    {
                        IsUpdated = false;
                        Msg = "," + ex.Message.ToString();
                        dtResult.Rows.Add("E", Msg);
                    }
                                
                    return dtResult;
                }
            }
            catch (Exception ex)
            {
                //InterfaceTruckBLL.Instance.InterfaceUpdateHeaderBLL(HeaderID, 3, string.Empty, "E", ex.Message, string.Empty, string.Empty, string.Empty, CreateBy);
                throw new Exception(ex.Message);
            }
        }

        #endregion
    }
}