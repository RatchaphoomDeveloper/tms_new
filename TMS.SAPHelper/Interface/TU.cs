﻿using TMS.Entity.Interface;
using TMS.SAPHelper.Utility;
using TMS.SAPHelper.WS_TU_UPDATE;
using System;
using System.Configuration;
using System.Data;
using TMS.SAPHelper;

namespace TMS.SAPHelper.Interface
{
    public class TU
    {
        #region + TU +
        //public static DataTable CreateTU(TUEntity tuEntity, int CreateBy)
        //{
        //    #region + Example +
        //    //DataTable dtItem = new DataTable();
        //    //dtItem.Columns.Add("TU_NUMBER");
        //    //dtItem.Columns.Add("COM_NUMBER");
        //    //dtItem.Columns.Add("SEQ_NMBR");
        //    //dtItem.Columns.Add("CMP_MAXVOL");
        //    //dtItem.Columns.Add("LOAD_SEQ");

        //    //dtItem.Rows.Add("ฮณ-0007", "1", "1", "2000", "0");

        //    //TUEntity tuEntity = new TUEntity
        //    //{
        //    //    EXPIRE_DAT = "2017-01-01",
        //    //    LANGUAGE = "TH",
        //    //    L_NUMBER = "1",
        //    //    TU_ID = "123456789",
        //    //    TU_MAXWGT = "2000",
        //    //    TU_NUMBER = "ฮณ-0007",
        //    //    TU_STATUS = " ",
        //    //    TU_TEXT = "SU2000(LPG)",
        //    //    TU_TYPE = "A130",
        //    //    TU_UNLWGT = "2000",
        //    //    VOL_UOM = "L",
        //    //    WGT_UOM = "KG",
        //    //    ZSTART_DAT = "2016-01-01",
        //    //    CMP_ITEM = dtItem
        //    //};

        //    //DataTable dtResult = LPG.SAPHelper.Interface.CreateTU(tuEntity, UserID);
        //    #endregion

        //    int HeaderID = 0;

        //    try
        //    {
        //        DataTable dtLog = InterfaceTruckBLL.Instance.InterfaceTUSelectBLL(" AND SAP_STATUS = 'S' AND LEG_STATUS = 'S' AND TU_ID = '" + tuEntity.TU_ID + "'");
        //        if (dtLog.Rows.Count > 0)
        //        {
        //            tuEntity.TU_ID = dtLog.Rows[0]["TU_ID"].ToString();

        //            return UpdateTU(tuEntity, CreateBy);
        //        }
        //        else
        //        {
        //            TransportUnitCreate_Sync_Out_SIRequest req = new TransportUnitCreate_Sync_Out_SIRequest();

        //            using (TransportUnitCreate_Sync_Out_SIClient CreateTU = new TransportUnitCreate_Sync_Out_SIClient("HTTP_Port2"))
        //            {
        //                int ItemCount = tuEntity.CMP_ITEM.Rows.Count;

        //                WS_TU_CREATE.Z04C2_OIGCC[] Items = new WS_TU_CREATE.Z04C2_OIGCC[ItemCount];
        //                WS_TU_CREATE.ZO4C2_RETURN[] Return = new WS_TU_CREATE.ZO4C2_RETURN[50];

        //                //SAP Login
        //                CreateTU.ClientCredentials.UserName.UserName = GetConfig.SAP_Username;
        //                CreateTU.ClientCredentials.UserName.Password = GetConfig.SAP_Password;

        //                DataTable dtResult = new DataTable();
        //                dtResult.Columns.Add("STATUS");
        //                dtResult.Columns.Add("MESSAGE");
        //                dtResult.Columns.Add("DRIVER_CODE");
        //                dtResult.Columns.Add("MSGID");
        //                dtResult.Columns.Add("TCODE");
        //                dtResult.Columns.Add("TU_NUMBER");

        //                //Item
        //                int Count = 0;
        //                WS_TU_CREATE.Z04C2_OIGCC Item = new WS_TU_CREATE.Z04C2_OIGCC();

        //                for (int i = 0; i < ItemCount; i++)
        //                {
        //                    Item = new WS_TU_CREATE.Z04C2_OIGCC();
        //                    Item.TU_NUMBER = tuEntity.CMP_ITEM.Rows[i]["TU_NUMBER"].ToString();
        //                    Item.COM_NUMBER = Convert.ToByte(tuEntity.CMP_ITEM.Rows[i]["COM_NUMBER"].ToString());
        //                    Item.SEQ_NMBR = tuEntity.CMP_ITEM.Rows[i]["SEQ_NMBR"].ToString();
        //                    Item.CMP_MAXVOL = tuEntity.CMP_ITEM.Rows[i]["CMP_MAXVOL"].ToString();
        //                    Item.LOAD_SEQ = tuEntity.CMP_ITEM.Rows[i]["LOAD_SEQ"].ToString();

        //                    Items[Count++] = Item;
        //                }

        //                HeaderID = InterfaceTruckBLL.Instance.InterfaceInsertBLL(tuEntity, CreateBy);

        //                CreateTU.TransportUnitCreate_Sync_Out_SI(tuEntity.EXPIRE_DAT
        //                    , tuEntity.LANGUAGE
        //                    , tuEntity.L_NUMBER
        //                    , tuEntity.TU_ID
        //                    , tuEntity.TU_MAXWGT
        //                    , tuEntity.TU_NUMBER
        //                    , tuEntity.TU_STATUS
        //                    , tuEntity.TU_TEXT
        //                    , tuEntity.TU_TYPE
        //                    , tuEntity.TU_UNLWGT
        //                    , tuEntity.VOL_UOM
        //                    , tuEntity.WGT_UOM
        //                    , tuEntity.ZSTART_DAT
        //                    , ref Items
        //                    , ref Return
        //                    );

        //                if (Return.Length > 0)
        //                {
        //                    int Row = 0;
        //                    if (Return.Length > 1)
        //                        Row = 1;

        //                    dtResult.Rows.Add(Return[Row].MSGTYP, Return[Row].MESSAGE, string.Empty, Return[Row].MSGID, Return[Row].TCODE, Return[Row].TU_NUMBER);
        //                    InterfaceTruckBLL.Instance.InterfaceUpdateHeaderBLL(HeaderID, 2, Return[Row].MSGTYP, "S", Return[Row].MESSAGE, Return[Row].MSGID, Return[Row].TCODE, Return[Row].TU_NUMBER, CreateBy);
        //                }

        //                return dtResult;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        InterfaceTruckBLL.Instance.InterfaceUpdateHeaderBLL(HeaderID, 3, string.Empty, "E", ex.Message, string.Empty, string.Empty, string.Empty, CreateBy);
        //        throw new Exception(ex.Message);
        //    }
        //}

        public static DataTable UpdateTU(TUEntity tuEntity, int CreateBy)
        {
            //int HeaderID = 0;

            try
            {
                UpdateTU_SYNC_OUT_SIRequest req = new UpdateTU_SYNC_OUT_SIRequest();

                using (UpdateTU_SYNC_OUT_SIClient UpdateTU = new UpdateTU_SYNC_OUT_SIClient("HTTP_Port1"))
                {
                    int ItemCount = tuEntity.CMP_ITEM.Rows.Count;

                    TMS.SAPHelper.WS_TU_UPDATE.UpdateTU_Src_Req_DTItem[] Items = new TMS.SAPHelper.WS_TU_UPDATE.UpdateTU_Src_Req_DTItem[ItemCount];
                    TMS.SAPHelper.WS_TU_UPDATE.UpdateTU_SYNC_OUT_SIResponse[] Return = new TMS.SAPHelper.WS_TU_UPDATE.UpdateTU_SYNC_OUT_SIResponse[50];

                    //SAP Login
                    UpdateTU.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"].ToString();
                    UpdateTU.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"].ToString();

                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("STATUS");
                    dtResult.Columns.Add("MESSAGE");
                    dtResult.Columns.Add("DRIVER_CODE");
                    dtResult.Columns.Add("MSGID");
                    dtResult.Columns.Add("TCODE");
                    dtResult.Columns.Add("TU_NUMBER");

                    //Item
                    int Count = 0;
                    TMS.SAPHelper.WS_TU_UPDATE.UpdateTU_Src_Req_DTItem Item = new TMS.SAPHelper.WS_TU_UPDATE.UpdateTU_Src_Req_DTItem();

                    for (int i = 0; i < ItemCount; i++)
                    {
                        //Item = new TMS.SAPHelper.WS_TU_UPDATE.UpdateTU_Src_Req_DTItem();
                        //Item.TU_NUMBER = tuEntity.CMP_ITEM.Rows[i]["TU_NUMBER"].ToString();
                        //Item.COM_NUMBER = Convert.ToByte(tuEntity.CMP_ITEM.Rows[i]["COM_NUMBER"].ToString());
                        //Item.SEQ_NMBR = tuEntity.CMP_ITEM.Rows[i]["SEQ_NMBR"].ToString();
                        //Item.CMP_MAXVOL = tuEntity.CMP_ITEM.Rows[i]["CMP_MAXVOL"].ToString();
                        //Item.LOAD_SEQ = tuEntity.CMP_ITEM.Rows[i]["LOAD_SEQ"].ToString();

                        //Items[Count++] = Item;
                    }

                    //HeaderID = InterfaceTruckBLL.Instance.InterfaceInsertBLL(tuEntity, CreateBy);
                    var trailer = new UpdateTU_Src_Req_DT()
                    {
                        TU_NUMBER = tuEntity.TU_NUMBER,
                        TU_ID = tuEntity.TU_ID,
                        ZSTART_DAT = tuEntity.ZSTART_DAT,
                        VOL_UOM = "#!",
                        TU_UNLWGT = tuEntity.TU_UNLWGT,
                        TU_STATUS = tuEntity.TU_STATUS,
                        TU_TEXT = tuEntity.TU_TEXT,
                        TU_MAXWGT = tuEntity.TU_MAXWGT,
                        L_NUMBER = tuEntity.L_NUMBER,
                        EXPIRE_DAT = tuEntity.EXPIRE_DAT,

                        CMP_ITEM = Items
                    };

                    //UpdateTU.TransportUnitUpdate_Sync_Out_SI(tuEntity.EXPIRE_DAT
                    //    , tuEntity.L_NUMBER
                    //    , tuEntity.TU_ID
                    //    , tuEntity.TU_MAXWGT
                    //    , tuEntity.TU_NUMBER
                    //    , tuEntity.TU_STATUS
                    //    , tuEntity.TU_TEXT
                    //    , tuEntity.TU_UNLWGT
                    //    , SAP_Config.SAPNotUpdateValue
                    //    , tuEntity.ZSTART_DAT
                        
                    //    , ref Items
                    //    , ref Return
                    //    );

                    if (Return.Length > 0)
                    {
                        int Row = 0;
                        if (Return.Length > 1)
                            Row = 1;

                        //dtResult.Rows.Add(Return[Row].MSGTYP, Return[Row].MESSAGE, string.Empty, Return[Row].MSGID, Return[Row].TCODE, Return[Row].TU_NUMBER);
                        //InterfaceTruckBLL.Instance.InterfaceUpdateHeaderBLL(HeaderID, 2, Return[Row].MSGTYP, "S", Return[Row].MESSAGE, Return[Row].MSGID, Return[Row].TCODE, Return[Row].TU_NUMBER, CreateBy);
                    }

                    return dtResult;
                }
            }
            catch (Exception ex)
            {
                //InterfaceTruckBLL.Instance.InterfaceUpdateHeaderBLL(HeaderID, 3, string.Empty, "E", ex.Message, string.Empty, string.Empty, string.Empty, CreateBy);
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}