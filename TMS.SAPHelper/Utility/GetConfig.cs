﻿using System.Configuration;

namespace TMS.SAPHelper.Utility
{
    public static class GetConfig
    {
        public static string SAP_Username = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"].ToString();
        public static string SAP_Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"].ToString();
    }
}