﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS_Entity
{
    [Serializable]
    public class UserProfile
    {
        public string USER_ID { get; set; }
        public string USERNAME { get; set; }
         public int TITLE_ID { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string SYSTEM_NAME { get; set; }
        public int SYSTEM_ID { get; set; }
        public string LOGO_NAME { get; set; }
        public string SVDID { get; set; }
        public string CGROUP { get; set; }
        public string UserGroup { get; set; }
        public string CHKCHANGEPASSWORD { get; set; }
    }
}
