﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TMS_Entity
{
    public class ParameterEntity
    {
        private string _sKey;
        private string _sValue;

        public ParameterEntity(string key, string value)
        {
            _sKey = key;
            _sValue = value;
        }

        public string sKey
        {
            get
            {
                return _sKey;
            }
            set
            {
                _sKey = value;
            }
        }
        public string sValue
        {
            get
            {
                return _sValue;
            }
            set
            {
                _sValue = value;
            }
        }
    }


    //public class ParameterEntity
    //{
    //}
}
