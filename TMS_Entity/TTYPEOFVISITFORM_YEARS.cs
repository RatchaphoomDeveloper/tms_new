﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TMS_Entity
{
    [Serializable]
    public class TTYPEOFVISITFORM_YEARS
    {
        public decimal? ID { get; set; }
        public decimal? YEAR { get; set; }
        public decimal? NTYPEVISITFORMID { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        public string CREATE_USR { get; set; }
        public string UPDATE_USR { get; set; }
        public string IS_ACTIVE { get; set; }
        public decimal? PERCENT { get; set; }
    }
}
