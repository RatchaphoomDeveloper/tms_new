﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// Add By ORN.K
    /// </remarks>
    public class DataModelHelper
    {
        //INTERFACE
        public static void CreateIDataTable<T>(ref DataTable datatable)
        {
            Type entityType = typeof(T);
            DataTable table = new DataTable(entityType.Name);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            foreach (PropertyDescriptor prop in properties)
            {
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            datatable = table;
            return;
        }

        //DATA
        public static void CreateList<T>(ref List<T> item, DataTable dataTable, bool fristSelect = false)
        {
            try
            {
                item = new List<T>();

                if (item.GetType().IsGenericType && item is IEnumerable == false) throw new Exception();
                Type ItemType = item.GetType().GetGenericArguments()[0];

                int rowSel = dataTable.Rows.Count;
                List<T> ObjectList = new List<T>();
                if (fristSelect) rowSel = 1;
                for (int i = 0; i < rowSel; i++)
                {
                    T TempObject = (T)Activator.CreateInstance<T>();
                    for (int j = 0; j < dataTable.Columns.Count; j++)
                    {
                        try
                        {
                            string colName = Convert.ToString(dataTable.Columns[j]);
                            PropertyInfo prop = ItemType.GetProperty(colName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                            if (prop == null) continue;

                            object value = dataTable.Rows[i][colName];

                            if (IsNullableType(prop.PropertyType))
                            {
                                if (value == DBNull.Value)
                                    value = null;
                                prop.SetValue(TempObject, Convert.ChangeType(value, prop.PropertyType), null);

                                //prop.SetValue(TempObject, dataTable.Rows[i][colName], null);
                            }
                            else
                            {
                                prop.SetValue(TempObject, Convert.ChangeType(dataTable.Rows[i][colName], prop.PropertyType), null);
                            }
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }


                    }
                    ObjectList.Add(TempObject);
                }
                item = ObjectList;
                return;
            }
            catch (Exception ex)
            {
                item = null;
                throw ex;
            }
        }

        public static void CreateListN<T>(ref List<T> item, DataTable dataTable)
        {
            try
            {
                item = new List<T>();

                if (item.GetType().IsGenericType && item is IEnumerable == false) throw new Exception();
                Type ItemType = item.GetType().GetGenericArguments()[0];


                List<T> ObjectList = new List<T>();

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    T TempObject = (T)Activator.CreateInstance<T>();
                    for (int j = 0; j < dataTable.Columns.Count; j++)
                    {
                        try
                        {
                            string colName = Convert.ToString(dataTable.Columns[j]);
                            PropertyInfo prop = ItemType.GetProperty(colName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                            if (prop == null) continue;

                            object value = dataTable.Rows[i][colName];

                            if (IsNullableType(prop.PropertyType))
                            {
                                if (value == DBNull.Value)
                                    value = null;
                                try
                                {
                                    value = System.Convert.ChangeType(dataTable.Rows[i][colName],
                                        Nullable.GetUnderlyingType(prop.PropertyType));
                                }
                                catch (InvalidCastException)
                                {
                                    return;
                                }
                                prop.SetValue(TempObject, value, null);
                            }
                            else
                            {
                               prop.SetValue(TempObject, Convert.ChangeType(dataTable.Rows[i][colName], prop.PropertyType), null);
                            }
                        }
                        catch (Exception)
                        {
                            continue;
                        }
                    }
                    ObjectList.Add(TempObject);
                }
                item = ObjectList;
                return;
            }
            catch (Exception ex)
            {
                item = null;
                throw ex;
            }
        }

        public static void SetValue(object inputObject, string propertyName, object propertyVal)
        {
            //find out the type
            Type type = inputObject.GetType();

            //get the property information based on the type
            System.Reflection.PropertyInfo propertyInfo = type.GetProperty(propertyName);

            //find the property type
            Type propertyType = propertyInfo.PropertyType;

            //Convert.ChangeType does not handle conversion to nullable types
            //if the property type is nullable, we need to get the underlying type of the property
            var targetType = IsNullableType(propertyInfo.PropertyType) ? Nullable.GetUnderlyingType(propertyInfo.PropertyType) : propertyInfo.PropertyType;

            //Returns an System.Object with the specified System.Type and whose value is
            //equivalent to the specified object.
            propertyVal = Convert.ChangeType(propertyVal, targetType);

            //Set the value of the property
            propertyInfo.SetValue(inputObject, propertyVal, null);

        }
        public static T ChangeType<T>(object value)
        {
            var t = typeof(T);

            if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null)
                {
                    return default(T);
                }

                t = Nullable.GetUnderlyingType(t);
            }

            return (T)Convert.ChangeType(value, t);
        }
        /// <summary>
        /// Function for Check Nullable Type
        /// </summary>
        /// <param name="propertyType"></param>
        /// <returns></returns>
        private static bool IsNullableType(Type propertyType)
        {
            return (propertyType.IsGenericType) && (object.ReferenceEquals(propertyType.GetGenericTypeDefinition(), typeof(Nullable<>)));
        }

        public static void CreateObject<T>(ref T item, DataTable dataTable)
        {
            try
            {
                item = (T)Activator.CreateInstance<T>();

                if (item.GetType().IsGenericType && item is IEnumerable) throw new Exception();

                Type type = item.GetType();

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    for (int j = 0; j < dataTable.Columns.Count; j++)
                    {
                        string colName = Convert.ToString(dataTable.Columns[j]);
                        PropertyInfo prop = type.GetProperty(colName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                        if (prop == null) continue;

                        object value = dataTable.Rows[i][colName];

                        if (IsNullableType(prop.PropertyType))
                        {
                            if (value == DBNull.Value)
                                value = null;

                            prop.SetValue(item, value, null);

                            //prop.SetValue(TempObject, dataTable.Rows[i][colName], null);
                        }
                        else
                        {
                            prop.SetValue(item, Convert.ChangeType(dataTable.Rows[0][colName], prop.PropertyType), null);
                        }

                    }

                }
                return;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string CheckNull(object T)
        {
            string result;

            if (T == null)
            {
                result = string.Empty;
            }
            else
            {
                result = T.ToString();
            }

            return result;
        }
    }
}
