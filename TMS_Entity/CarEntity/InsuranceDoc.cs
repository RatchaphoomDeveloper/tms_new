﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TMS_Entity.CarEntity
{
    [Serializable]
    public class InsuranceDoc
    {
        //public string GenID { get; set; }
        public string TRUCKID { get; set; }
        public DataTable dtUpload { get; set; }
        public string SessionUserId { get; set; }
    }
}
