﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TMS_Entity.CarEntity
{
    [Serializable]
    public class Truck
    {
        public int TypeCar { get; set; }
        public string STRUCK { get; set; }
        public string STRANSPORTID { get; set; }        
        public string txtHRegNo { get; set; }
        public string cmbHsHolder { get; set; }
        public string cmbHRoute { get; set; }
        public string txtHsEngine { get; set; }
        public string cmbHTru_Cate { get; set; }
        public string cboHLoadMethod { get; set; }
        public string txtHsModel { get; set; }
        public string txtHsChasis { get; set; }
        public string cboHsVibration { get; set; }
        public string wheel { get; set; }
        public string cboRMaterialOfPressure { get; set; }
        public string txtRnShaftDriven { get; set; }
        public string txtRwightDriven { get; set; }
        public int? txtRnWeight { get; set; }
        public string cboHPumpPower_type { get; set; }
        public string txtVEH_Volume { get; set; }
        public string cboHGPSProvider { get; set; }
        public string rblRValveType { get; set; }
        public string rblRFuelType { get; set; }
        public string nslot { get; set; }
        public string sessionId { get; set; }
        
    }
}
