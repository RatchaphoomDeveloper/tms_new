﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TMS_Entity.CarEntity
{
    [Serializable]
    public class Insurance
    {
        public string STRUCKID { get; set; }
        public string company { get; set; }
        public string Detail { get; set; }
    }
}
