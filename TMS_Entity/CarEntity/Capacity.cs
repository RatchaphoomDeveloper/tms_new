﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TMS_Entity.CarEntity
{
    [Serializable]
    public class Capacity
    {
        public string STRUCK { get; set; }
        public DataTable capacity { get; set; }
        public string SessionUserId { get; set; }
    }
}
