﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TMS_Entity.CarEntity
{
    [Serializable]
    public class SemiTruck
    {
        public int TypeCar{get;set;}
        public string STRUCK{get;set;}
        public string STRANSPORTID { get; set; }
        public string txtSemiNo { get; set; }        
        public string cmbSemiHolder{get;set;}
        public string cmbCarSemiSap{get;set;}
        public string cboHLoadMethod{get;set;}
        public string txtSemiChasis{get;set;}
        public string txtSemiShaftDriven{get;set;}
        public string SemiWheel{get;set;}
        public string rblSemiPumpPower{get;set;}
        public string txtSemiWeight{get;set;}
        public string nslot { get; set; }
        public string sessionId{get;set;}
    }
}
