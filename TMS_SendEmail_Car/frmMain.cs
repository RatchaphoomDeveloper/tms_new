﻿using System;
using System.Data;
using System.Windows.Forms;
using TMS_BLL.Transaction.SendEmail;
using TMS_BLL.Master;
using EmailHelper;
using TMS_BLL.Transaction.Complain;
using System.Threading;
using TMS_SendEmail_Car;

namespace TMS_SendEmail
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitialForm();
        }

        private void InitialForm()
        {
            try
            {
                this.SendEmail();
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                Thread.Sleep(2000);
                Application.Exit();
            }
        }

        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            this.Show();
            WindowState = FormWindowState.Normal;
            this.BringToFront();
        }

        private void frmMain_Shown(object sender, EventArgs e)
        {
            //WindowState = FormWindowState.Minimized;
            //Hide();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                TMS_SendEmail_Car.Properties.Settings.Default["ExecuteTime"] = dtpExecuteTime.Text;
                TMS_SendEmail_Car.Properties.Settings.Default.Save();

                MessageBox.Show("Save Success...");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (string.Equals(DateTime.Now.ToString("HH:mm"), dtpExecuteTime.Text))
                    this.SendEmail();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmdSendNow_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการส่งอีเมล์ทันที ใช่หรือไม่", "ยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    this.SendEmail();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SendEmail()
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                int TemplateID;
                DataTable dtEmail;

                TemplateID = 42;                                                                     //เตือนให้ผู้ขนส่งยืนยันรถ
                dtEmail = SendEmailBLL.Instance.SendEmailCarSelectBLL(TemplateID);
                this.SendEmailReport(dtEmail);
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void SendEmailReport(DataTable dtEmail)
        {
            try
            {
                string Subject = string.Empty;
                string Body = string.Empty;
                string Link = string.Empty;

                if (string.Equals(ConfigValue.Environment, "DEV"))
                    Link = "https://hq-srvtst-s08/tms_dup/vendor_confirm_contracts.aspx";
                else if (string.Equals(ConfigValue.Environment, "PRD"))
                    Link = "https://ptttms.pttplc.com/TMS/vendor_confirm_contracts.aspx";

                for (int i = 0; i < dtEmail.Rows.Count; i++)
                {
                    //MailService.SendMail(dtEmail.Rows[i]["EMAIL"].ToString() + ", zsakchai.p@pttict.com", dtTemplate.Rows[0]["SUBJECT"].ToString(), Body);
                    //MailService.SendMail("zsakchai.p@pttict.com,," + dtEmail.Rows[i]["EMAIL"].ToString(), dtEmail.Rows[0]["SUBJECT"].ToString(), dtEmail.Rows[0]["BODY"].ToString());

                    Subject = dtEmail.Rows[i]["SUBJECT"].ToString();
                    Subject = Subject.Replace("{VENDOR}", dtEmail.Rows[i]["SABBREVIATION"].ToString());

                    Body = dtEmail.Rows[i]["BODY"].ToString();
                    Body = Body.Replace("{VENDOR}", dtEmail.Rows[i]["SABBREVIATION"].ToString());
                    Body = Body.Replace("{CONTRACT}", dtEmail.Rows[i]["SCONTRACTNO"].ToString());
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    Body = "<div style=\"font-size:20px; font-family:Angsana New\">" + Body + "</div>";

                    MailService.SendMail(dtEmail.Rows[i]["EMAIL"].ToString(), Subject, Body, "", "VendorEmployeeAdd", "EMAIL");
                }
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
            }
        }
    }
}