﻿
namespace TMS_SendEmail_Car
{
    public static class ConfigValue
    {
        public static string Environment = "PRD";

        public static string GetClickHere(string URL)
        {
            return "<a href=\"" + URL + "\">คลิกที่นี่</a>";
        }
    }
}