﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
    public class RequireBLL
    {
        #region MenuSelect
        public DataTable MenuSelect()
        {
            try
            {
                return RequireDAL.Instance.MenuSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RequireLevelSelect
        public DataTable RequireLevelSelect(string FIELD_TYPE)
        {
            try
            {
                return RequireDAL.Instance.RequireLevelSelect(FIELD_TYPE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RequireLevelDataSelect
        public DataTable RequireLevelDataSelect(string MREQUIRELEVELID, string PARENTID)
        {
            try
            {
                return RequireDAL.Instance.RequireLevelDataSelect(MREQUIRELEVELID, PARENTID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RequireSelect
        public DataTable RequireSelect(string Level1, string Level2, string Level3, string Level4, string MREQUIRELEVELIDLEVEL1, string MREQUIRELEVELIDLEVEL2, string MREQUIRELEVELIDLEVEL3, string MREQUIRELEVELIDLEVEL4, string FIELD_TYPE, int LevelCount)
        {
            try
            {
                return RequireDAL.Instance.RequireSelect(Level1, Level2, Level3, Level4, MREQUIRELEVELIDLEVEL1, MREQUIRELEVELIDLEVEL2, MREQUIRELEVELIDLEVEL3, MREQUIRELEVELIDLEVEL4, FIELD_TYPE, LevelCount);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RequireFileSelect
        public DataTable RequireFileSelect(string Level1, string Level2, string Level3, string Level4, string MREQUIRELEVELIDLEVEL1, string MREQUIRELEVELIDLEVEL2, string MREQUIRELEVELIDLEVEL3, string MREQUIRELEVELIDLEVEL4, string FIELD_TYPE, int LevelCount)
        {
            try
            {
                return RequireDAL.Instance.RequireFileSelect(Level1, Level2, Level3, Level4, MREQUIRELEVELIDLEVEL1, MREQUIRELEVELIDLEVEL2, MREQUIRELEVELIDLEVEL3, MREQUIRELEVELIDLEVEL4, FIELD_TYPE, LevelCount);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RequireSave
        public bool RequireSave(string M_COMPLAIN_REQUIRE_FIELD, string ISACTIVE, string REQUIRE_TYPE)
        {
            try
            {
                return RequireDAL.Instance.RequireSave(M_COMPLAIN_REQUIRE_FIELD, ISACTIVE, REQUIRE_TYPE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RequireFileSave
        public bool RequireFileSave(string REQUEST_ID, string ISACTIVE, string UPLOAD_NAME, string EXTENTION, string MAX_FILE_SIZE, string UPLOAD_ID)
        {
            try
            {
                return RequireDAL.Instance.RequireFileSave(REQUEST_ID, ISACTIVE, UPLOAD_NAME, EXTENTION, MAX_FILE_SIZE, UPLOAD_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RequireDelete
        public bool RequireDelete(string M_COMPLAIN_REQUIRE_FIELD)
        {
            try
            {
                return RequireDAL.Instance.RequireDelete(M_COMPLAIN_REQUIRE_FIELD);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region FieldSelect
        public DataTable FieldSelect(string I_FIELD_TYPE)
        {
            try
            {
                return RequireDAL.Instance.FieldSelect(I_FIELD_TYPE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        public DataTable RequireTemplateSelectBLL(string FieldType)
        {
            try
            {
                return RequireDAL.Instance.RequireSelectTemplateDAL(FieldType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #region RequireFieldSelect
        public DataTable RequireFieldSelect(string I_FIELD_TYPE, string I_FIELD_REFERENCE)
        {
            try
            {
                return RequireDAL.Instance.RequireFieldSelect(I_FIELD_TYPE, I_FIELD_REFERENCE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        public void RequireInsertBLL(DataTable dtFieldFinal, DataTable dtFileFinal, string ComplainTypeID, DataTable dtComplainConfig, DataTable dtMonthlyReportConfig)
        {
            try
            {
                RequireDAL.Instance.RequireInsertDAL(dtFieldFinal, dtFileFinal, ComplainTypeID, dtComplainConfig, dtMonthlyReportConfig);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static RequireBLL _instance;
        public static RequireBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new RequireBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
