﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TMS_DAL.Master;
using System.Web.UI.WebControls;
using TMS_Entity.CarEntity;
using TMS_DAL.Transaction.TruckDoc;

namespace TMS_BLL.Master
{
    public class CarBLL
    {
        #region LoadData
        public DataTable LoadInitalData()
        {
            try
            {
                return CarDAL.Instance.initalData();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public DataTable LoadDataRoute()
        {
            try
            {
                return CarDAL.Instance.InitalDataRoute();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable LoadDataCarSap()
        {
            try
            {
                return CarDAL.Instance.InitalCarTypeSap();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable LoadDataCartype()
        {
            try
            {
                return CarDAL.Instance.InitalCarType();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public string LoadGetTRUCKID()
        {
            try
            {
                return CarDAL.Instance.GenTRUKCID();
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public string LoadGetGenId()
        {
            try
            {
                return CarDAL.Instance.GenidDoc();
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public DataTable LoadBrand()
        {
            try
            {
                return CarDAL.Instance.InitalBand();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public DataTable LoadGps()
        {
            try
            {
                return CarDAL.Instance.InitalGps();
            }
            catch (Exception)
            {

                throw;
            }
        }
        //public DataTable 
        #endregion
        #region แสดงข้อมูล
        #region แสดงข้อมูล Header
        public DataTable LoadDataHeader(string STRUCKID)
        {
            try
            {
                return CarDAL.Instance.SelectDataHeader(STRUCKID);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region แสดงข้อมูลประกัน
        public DataTable LoadDataInsurance(string STRUCKID)
        {
            try
            {
                return CarDAL.Instance.SelectDataInsurance(STRUCKID);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region แสดงข้อมูลช่องเติม
        public DataTable LoadDataCompart(string STRUCKID)
        {
            try
            {
                return CarDAL.Instance.SelectDataCompart(STRUCKID);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region แสดงBlacklist
        public DataTable initalBlacklist(string SHEADREGISTERNO)
        {
            try
            {
                return CarDAL.Instance.DataBlacklist(SHEADREGISTERNO);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region หาจำนวนBlcklist
        public string initalRowBlacklist(string SHEADREGISTERNO)
        {
            try
            {
                return CarDAL.Instance.DataRowBlacklist(SHEADREGISTERNO);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region หางไปค้นหัว
        public string FindHeadBySemi(string SHEADREGISTERNO)
        {
            return CarDAL.Instance.SelectFindHeadBySemi(SHEADREGISTERNO);
        }

        #endregion
        #region LoadVendorName
        public string LoadVendorName(string VendorId)
        {
            if (CarDAL.Instance.DataVendorName(VendorId).Rows.Count > 0)
            {
                return CarDAL.Instance.DataVendorName(VendorId).Rows[0]["SABBREVIATION"].ToString();
            }
            else
            {
                return "";
            }
        }
        #endregion
        #region GetClassGrp //contract_add.aspx
        public DataTable GetGenClassgrp(string SHEADREGISTERNO)
        {
            try
            {
                return CarDAL.Instance.GetClassGrp(SHEADREGISTERNO);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        #endregion

        public DataTable CreateClassgrp(string SHEADREGISTERNO, string DOCUMENT)
        {
            try
            {
                return CarDAL.Instance.CreateClassgrp(SHEADREGISTERNO, DOCUMENT);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckAgeExpireBLL(int Template_ID)
        {
            try
            {
                return CarDAL.Instance.TruckAgeExpireDAL(Template_ID);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckAgeExpireSysdateBLL(int Template_ID)
        {
            try
            {
                return CarDAL.Instance.TruckAgeExpireSysdateDAL(Template_ID);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckDocExpireBLL(int Template_ID)
        {
            try
            {
                return CarDAL.Instance.TruckDocExpireDAL(Template_ID);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckDocExpireSysdateBLL(int Template_ID)
        {
            try
            {
                return CarDAL.Instance.TruckDocExpireSysdateDAL(Template_ID);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public DataTable UpdateTruckAgeExpireBLL(DataTable ttruck)
        {
            try
            {
                return CarDAL.Instance.UpdateTruckAgeExpireDAL(ttruck);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public DataTable UpdateTruckExpireBLL(string struckid)
        {
            try
            {
                return CarDAL.Instance.UpdateTruckExpireDAL(struckid);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        #endregion
        #region บันทึกข้อมูลรถ
        public DataTable TruckSave(string CarType, DataTable Truck, DataTable Doc, DataTable DocTPN, DataTable Insurance, DataTable Oilcapacity, string SpotStatus)
        {
            try
            {
                return CarDAL.Instance.SaveDataTruck(CarType, Truck, Doc, DocTPN, Insurance, Oilcapacity, SpotStatus);
            }
            catch (Exception)
            {

                throw;
            }

        }
        #endregion
        #region Update ทั้งหมด
        public void UpdateApprove(string TTRUCKID)
        {
            try
            {
                CarDAL.Instance.UpdatateApprove(TTRUCKID);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public void TruckCancelBLL(string TTRUCKID)
        {
            try
            {
                CarDAL.Instance.TruckCancelDAL(TTRUCKID);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #region บันทึกcomment
        public void SaveComment(string TTRUCKID, string COMMENT)
        {
            try
            {
                CarDAL.Instance.SaveComment(TTRUCKID, COMMENT);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
        #region Update รถเดี่ยว
        public DataTable UpdateTruck(string TTRUCKID, string CarType, DataTable Truck, DataTable Doc, DataTable DocTPN, DataTable Insurance, DataTable Oilcapacity, string SpotStatus)
        {
            try
            {
                return CarDAL.Instance.UpdateDataTruck(TTRUCKID, CarType, Truck, Doc, DocTPN, Insurance, Oilcapacity, SpotStatus);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        #endregion




        #endregion
        #region เอกสารDoc
        public DataTable CheckDocTruck(string STTRUCKID)
        {
            try
            {
                return TruckDocDAL.Instance.CheckDocComplate(STTRUCKID);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region Check กดอนุมัติก่อนอีเมลล์
        public DataTable CheckalterEmail(string STRUCKID)
        {
            return CarDAL.Instance.CheckAlterEmal(STRUCKID);
        }
        #endregion
        #region UpDate Classgrp
        public void UpdateClassGrp(string Classgrp, string SHEADREGISTERNO)
        {
            try
            {
                CarDAL.Instance.UpDateClassgrp(Classgrp, SHEADREGISTERNO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region UpdateNTotal
        public void UpdateNTotal(string TtruckId, int nTotal)
        {
            try
            {
                CarDAL.Instance.UpdateNTotal(TtruckId, nTotal);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region + Instance +
        private static CarBLL _instance;
        public static CarBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new CarBLL();
                //}
                return _instance;
            }
        }
        #endregion

        public DataTable DeleteTruckBLL(string TRUCKID)
        {
            try
            {
                return CarDAL.Instance.DeleteTruckDAL(TRUCKID);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckAgeSelect(string Condition)
        {
            try
            {
                return CarDAL.Instance.TruckAgeSelect(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetDocMVBLL(string TruckID)
        {
            try
            {
                return CarDAL.Instance.GetDocMVDAL(TruckID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

}
