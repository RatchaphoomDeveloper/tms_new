﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TMS_DAL.Master;
using System.Web.UI.WebControls;
using TMS_Entity.CarEntity;
using TMS_DAL.Transaction.TruckDoc;
namespace TMS_BLL.Master
{
    public class LOG
    {
        private DataTable DataLog { get; set; }
        public string regisNo { get; set; }
        public string headder { get; set; }
        public string regisno { get; set; }
        public string user_id { get; set; }
        public string newdata { get; set; }
        public string olddata { get; set; }
        
        private DataTable DataToDatatable(string headder, string regisno, string user_id, string newdata, string olddata)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("HEADDER");
            dt.Columns.Add("REGISNO");
            dt.Columns.Add("USER_ID");
            dt.Columns.Add("NEWDATA");
            dt.Columns.Add("OLDDATA");
            dt.Rows.Add(
                headder,
                regisno,
                user_id,
                newdata,
                olddata
                );
            this.DataLog = dt;
            return DataLog;
        }
        #region เพิ่มข้อมุลLog ตอนสร้าง
        public void SaveLog(string headder, string regisno, string user_id, string newdata, string olddata)
        {
            try
            {
                this.DataToDatatable(headder,  regisno,  user_id,  newdata,  olddata);
                TMS_DAL.Master.TruckLog.Instance.SaveLog(this.DataLog);    
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message); 
            }
        }

        #endregion
        #region GetOldTruck
            public DataTable GetOldDataTruck(string regisno)
            {
                return TruckLog.Instance.GetOldDataTruck(regisno);
            }
        #endregion
        #region GetOldTruckInsurance
            public DataTable GetOldDataTruckInsurance(string STTRUCKID)
        {
            return TruckLog.Instance.GetOldDataTruckInsurance(STTRUCKID);
        }
        #endregion
        #region GetOldCapacity
            public DataTable GetOldDataCapacity(string STTRUCKID)
            {
                return TruckLog.Instance.GetOldDataCapacity(STTRUCKID);
            }
        #endregion
        #region Getทะเบียนรถโดย Id
            public DataTable GetRegisNoByTruckId(string STTRUCKID)
        {
            return TruckLog.Instance.GetRegisNoByTruckId(STTRUCKID);
        }
        #endregion           
        
        #region + Instance +
        private static LOG _instance;
    public static LOG Instance
    {
        get
        {
            //if (_instance == null)
            //{
                _instance = new LOG();
            //}
            return _instance;
        }
    }
    #endregion

        
    }

}
