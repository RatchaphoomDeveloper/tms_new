﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL;

namespace TMS_BLL.Master
{
    public class ConfirmTruckBLL
    {
        #region + Instance +
        private static ConfirmTruckBLL _instance;
        public static ConfirmTruckBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new ConfirmTruckBLL();
                //}
                return _instance;
            }
        }
        #endregion

        #region PTTConfirmTruckSelect
        public DataSet PTTConfirmTruckSelect(string I_DATE, string I_STERMINALID, string I_SCONTRACTID, string I_SVENDORID, string I_CSTANBY, string I_GROUPID, string I_CCONFIRM)
        {
            try
            {
                return ConfirmTruckDAL.Instance.PTTConfirmTruckSelect(I_DATE, I_STERMINALID, I_SCONTRACTID, I_SVENDORID, I_CSTANBY, I_GROUPID, I_CCONFIRM);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region PTTConfirmTruckDetailSelect
        public DataSet PTTConfirmTruckDetailSelect(string I_DATE, string I_STERMINALID, string I_SCONTRACTID, string I_TEXTSEARCH, string I_CSTANBY, string I_CAR_STATUS_ID, string I_CCONFIRM, bool I_ISVENDOR)
        {
            try
            {
                return ConfirmTruckDAL.Instance.PTTConfirmTruckDetailSelect(I_DATE, I_STERMINALID, I_SCONTRACTID, I_TEXTSEARCH, I_CSTANBY, I_CAR_STATUS_ID, I_CCONFIRM, I_ISVENDOR);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region PTTConfirmTruckDetailCheckIVMSSelect
        public DataSet PTTConfirmTruckDetailCheckIVMSSelect(string I_SPLANTCODE, string I_LICENSE)
        {
            try
            {
                return ConfirmTruckDAL.Instance.PTTConfirmTruckDetailCheckIVMSSelect(I_SPLANTCODE, I_LICENSE);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region PTTConfirmTruckDetailCheckIVMSSelect
        public DataTable PTTConfirmTruckDetailCheckIVMSSelect(string ConnectionString, string License, string splantcode)
        {
            try
            {
                return ConfirmTruckDAL.Instance.PTTConfirmTruckDetailCheckIVMSSelect(ConnectionString, License, splantcode);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region PTTConfirmTruckDetailCheckIVMSSelect
        public DataTable PTTConfirmTruckDetailCheckIVMSSelect(string ConnectionString, DataTable dt)
        {
            try
            {
                return ConfirmTruckDAL.Instance.PTTConfirmTruckDetailCheckIVMSSelect(ConnectionString, dt);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region PTTConfirmTruckLockUpdate
        public bool PTTConfirmTruckLockUpdate(string I_NCONFIRMID, string I_USERID, string I_PTT_LOCK)
        {
            try
            {
                return ConfirmTruckDAL.Instance.PTTConfirmTruckLockUpdate(I_NCONFIRMID, I_USERID, I_PTT_LOCK);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region TruckByVendorSelect
        public DataTable TruckByVendorSelect(string I_VENDORCODE)
        {
            try
            {
                return ConfirmTruckDAL.Instance.TruckByVendorSelect(I_VENDORCODE);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}
