﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
	public class MaintainBLL
	{
		#region MaintainSelect
		public DataTable MaintainSelect()
        {
            try
            {
				return MaintainDAL.Instance.MaintainSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

		#region MaintainSave
		public DataTable MaintainSave(DataTable MaintainDT, string I_USERID)
        {

            try
            {
				return MaintainDAL.Instance.MaintainSave(I_USERID, MaintainDT);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region + Instance +
		private static MaintainBLL _instance;
		public static MaintainBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
				_instance = new MaintainBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
