﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
    public class ContractBLL
    {

        #region TTERMINALSelect
        public DataTable TTERMINALSelect()
        {
            try
            {
                return ContractDAL.Instance.TTERMINALSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region TransportTypeSelect
        public DataTable TransportTypeSelect()
        {
            try
            {
                return ContractDAL.Instance.TransportTypeSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractTypeSelect
        public DataTable ContractTypeSelect()
        {
            try
            {
                return ContractDAL.Instance.ContractTypeSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ProductTypeSelect
        public DataTable ProductTypeSelect()
        {
            try
            {
                return ContractDAL.Instance.ProductTypeSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GUARANTEETYPESelect
        public DataTable GUARANTEETYPESelect()
        {
            try
            {
                return ContractDAL.Instance.GUARANTEETYPESelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VEHSelectByPagesize
        public DataTable VEHSelectByPagesize(string fillter, int startIndex, int endIndex, string STRANSPORTID)
        {
            try
            {
                return ContractDAL.Instance.VEHSelectByPagesize(fillter, startIndex, endIndex, STRANSPORTID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region MContractSelectByPagesize
        public DataTable MContractSelectByPagesize(string fillter, int startIndex, int endIndex)
        {
            try
            {
                return ContractDAL.Instance.MContractSelectByPagesize(fillter, startIndex, endIndex);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region TUSelect
        public DataTable TUSelect(string VEH_ID)
        {
            try
            {
                return ContractDAL.Instance.TUSelect(VEH_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractSelect
        public DataSet ContractSelect(string I_CONTRACT_ID)
        {
            try
            {
                return ContractDAL.Instance.ContractSelect(I_CONTRACT_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractListSelect
        public DataTable ContractListSelect(string IP_CONTRACTNO, string IP_VENDORID, string IP_CACTIVE)
        {
            try
            {
                return ContractDAL.Instance.ContractListSelect(IP_CONTRACTNO, IP_VENDORID, IP_CACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ContractListSelect2(string IP_CONTRACTNO, string IP_VENDORID, string IP_CACTIVE, string SpotStatus)
        {
            try
            {
                return ContractDAL.Instance.ContractListSelect2(IP_CONTRACTNO, IP_VENDORID, IP_CACTIVE, SpotStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ConTractSelectByVendor
        public DataTable ConTractSelectByVendor(string SVENDORID = "", string CONTRACTNO = "")
        {
            try
            {
                return ContractDAL.Instance.ConTractSelectByVendor(SVENDORID, CONTRACTNO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ConTractDetailSelect
        public DataTable ConTractDetailSelect(string Condition = "")
        {
            try
            {
                return ContractDAL.Instance.ConTractDetailSelect(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractSave
        public DataTable ContractSave(string I_CONTRACTID, string I_MODIFY_TYPE,
string I_CONTRACTTYPE,
string I_CONTRACTNO,
string I_CONTRACTNONEW,
string I_VENDOR,
string I_STARTDATE,
string I_ENDDATE,
string I_CONTRACTTRUCK,
string I_REMARK,
string I_CACTIVE,
string I_USERID,
string I_PROCUREMENT,
string I_MAINPLANT,
string I_PLNT,
string I_RENEWFROM,
            string I_GROUPSID, string I_NTRUCKCONTRACT,
                                             string I_NTRUCKRESERVE,
DataTable I_GUARANTEE,
DataTable I_CONTRACTPLANT,
DataTable I_GPRODUCT,
DataTable I_TRUCK, DataTable dtUpload, string flag)
        {

            try
            {
                return ContractDAL.Instance.ContractSave(I_CONTRACTID, I_MODIFY_TYPE,
 I_CONTRACTTYPE,
 I_CONTRACTNO,
 I_CONTRACTNONEW,
 I_VENDOR,
 I_STARTDATE,
 I_ENDDATE,
 I_CONTRACTTRUCK,
 I_REMARK,
 I_CACTIVE,
 I_USERID,
 I_PROCUREMENT,
 I_MAINPLANT,
 I_PLNT,
 I_RENEWFROM,
 I_GROUPSID, I_NTRUCKCONTRACT,
                                                I_NTRUCKRESERVE,
 I_GUARANTEE,
 I_CONTRACTPLANT,
 I_GPRODUCT,
 I_TRUCK, dtUpload, flag);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CheckTruckInContract
        public bool CheckTruckInContract(string STRUCKID)
        {
            try
            {
                return ContractDAL.Instance.CheckTruckInContract(STRUCKID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        public DataTable TruckContractExpireBLL()
        {
            try
            {
                return ContractDAL.Instance.TruckContractExpireDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable DriverContractExpireBLL()
        {
            try
            {
                return ContractDAL.Instance.DriverContractExpireDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UpdateContractExpireBLL()
        {
            try
            {
                ContractDAL.Instance.UpdateContractExpireDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SpotDeliverySelectDAL(string Condition)
        {
            try
            {
                return ContractDAL.Instance.SpotDeliverySelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region AfterContractSelect
        public DataTable AfterContractSelect(string I_STRUCKID, string I_CONTRACT_ID)
        {
            try
            {
                return ContractDAL.Instance.AfterContractSelect(I_STRUCKID, I_CONTRACT_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region + Instance +
        private static ContractBLL _instance;
        public static ContractBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new ContractBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
