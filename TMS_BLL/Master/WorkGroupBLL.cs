﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
    public class WorkGroupBLL
    {
        #region WorkGroupSelect
        public DataTable WorkGroupSelect(string NAME, string CACTIVE)
        {
            try
            {
                return WorkGroupDAL.Instance.WorkGroupSelect(NAME, CACTIVE);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region WorkGroupCheckValidate
        public bool WorkGroupCheckValidate(int ID, string NAME)
        {
            try
            {
                return WorkGroupDAL.Instance.WorkGroupCheckValidate(ID, NAME);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region WorkGroupInsert
        public bool WorkGroupInsert(string NAME, string CACTIVE)
        {
            try
            {
                return WorkGroupDAL.Instance.WorkGroupInsert(NAME, CACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region WorkGroupUpdate
        public bool WorkGroupUpdate(int ID, string NAME, string CACTIVE)
        {
            try
            {
                return WorkGroupDAL.Instance.WorkGroupUpdate(ID, NAME, CACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion


        #region GroupSelect
        public DataTable GroupSelect(int WORKGROUPID, string NAME, string CACTIVE)
        {
            try
            {
                return WorkGroupDAL.Instance.GroupSelect(WORKGROUPID,NAME, CACTIVE);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GroupCheckValidate
        public bool GroupCheckValidate(int ID, string NAME)
        {
            try
            {
                return WorkGroupDAL.Instance.GroupCheckValidate(ID, NAME);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GroupInsert
        public bool GroupInsert(int WORKGROUPID, string NAME, string CACTIVE)
        {
            try
            {
                return WorkGroupDAL.Instance.GroupInsert(WORKGROUPID, NAME, CACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region GroupUpdate
        public bool GroupUpdate(int ID,int WORKGROUPID, string NAME, string CACTIVE)
        {
            try
            {
                return WorkGroupDAL.Instance.GroupUpdate(ID,WORKGROUPID, NAME, CACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region GroupSelect
        public DataTable VEHICLE_STATUS()
        {
            try
            {
                return WorkGroupDAL.Instance.VEHICLE_STATUS();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region + Instance +
        private static WorkGroupBLL _instance;
        public static WorkGroupBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new WorkGroupBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
