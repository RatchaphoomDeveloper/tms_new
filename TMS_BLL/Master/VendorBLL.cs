﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
    public class VendorBLL
    {
        #region PositionSelect
        public DataTable PositionSelect()
        {
            try
            {
                return VendorDAL.Instance.PositionSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable PositionSelectAllBLL(string Condition)
        {
            try
            {
                return VendorDAL.Instance.PositionSelectAllDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region TitleSelect
        public DataTable TitleSelect()
        {
            try
            {
                return VendorDAL.Instance.TitleSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region StatusCancelSelect
        public DataTable StatusCancelSelect()
        {
            try
            {
                return VendorDAL.Instance.StatusCancelSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region StatusSelect
        public DataTable StatusSelect()
        {
            try
            {
                return VendorDAL.Instance.StatusSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region SelectName
        public DataTable SelectName(string condition)
        {
            try
            {
                if (string.IsNullOrEmpty(condition))
                {
                    return VendorBLL.Instance.TVendorSapSelect();
                }
                else
                {
                    return VendorDAL.Instance.SelectName(condition);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectVendorName(string condition)
        {
            try
            {
                return VendorBLL.Instance.TVendorSapSelect(condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorSelect
        public DataTable VendorSelect(string SEMPLOYEEID)
        {
            try
            {
                return VendorDAL.Instance.VendorSelect(SEMPLOYEEID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable DriverSelect(string CardID)
        {
            try
            {
                return VendorDAL.Instance.DriverSelect(CardID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable DriverSelectWebBLL(string CardID)
        {
            try
            {
                return VendorDAL.Instance.DriverSelectWebDAL(CardID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region LoadNameVendor
        public DataTable LoadNameVendor(string Condition)
        {
            return VendorDAL.Instance.InitalNameVendor(Condition);
        }
        #endregion

        #region VendorList
        public DataTable VendorList(string Condition)
        {
            try
            {
                return VendorDAL.Instance.VendorList(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorWebList
        public DataTable VendorWebList(string Condition)
        {
            try
            {
                return VendorDAL.Instance.VendorWebList(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorWebServiceList
        public DataTable VendorWebServiceList(string Condition)
        {
            try
            {
                return VendorDAL.Instance.VendorWebServiceList(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorGetByPersCode
        public DataTable VendorGetByPersCode(string PersCode)
        {
            try
            {
                return VendorDAL.Instance.VendorGetByPersCode(PersCode);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorP4Select
        public DataTable VendorP4Select(string SEMPLOYEEID, string REQID)
        {
            try
            {
                return VendorDAL.Instance.VendorP4Select(SEMPLOYEEID, REQID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorRequestFile
        public DataTable VendorRequestFile(string PERSON_TYPE)
        {
            try
            {
                return VendorDAL.Instance.VendorRequestFile(PERSON_TYPE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable VendorRequestFile_Doc(string PERSON_TYPE, string UPLOAD_TYPE_IND)
        {
            try
            {
                return VendorDAL.Instance.VendorRequestFile_Doc(PERSON_TYPE, UPLOAD_TYPE_IND);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region EmployeeHistorySelect
        public DataTable EmployeeHistorySelect(string PERS_CODE)
        {
            try
            {
                return VendorDAL.Instance.EmployeeHistorySelect(PERS_CODE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region TVendorSapSelect
        public DataTable TVendorSapSelect()
        {
            try
            {
                return VendorDAL.Instance.TVendorSapSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable TVendorSapSelect(string CONDITION)
        {
            try
            {
                return VendorDAL.Instance.TVendorSapSelect(CONDITION);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region oldvender

        #region VendorSave
        public DataTable VendorSave(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, DataTable dtUpload)
        {
            try
            {
                return VendorDAL.Instance.VendorSave(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, dtUpload);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorInsert
        public DataTable VendorInsert(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, string I_EMPSAPID, DataTable dtUpload)
        {
            try
            {
                return VendorDAL.Instance.VendorInsert(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, I_EMPSAPID, dtUpload);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorP4Insert
        public DataTable VendorP4Insert(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID,
            string I_EMPSAPID, DataTable dtUpload)
        {
            try
            {
                return VendorDAL.Instance.VendorP4Insert(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, I_EMPSAPID, dtUpload);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorSaveChangeVendor
        public DataTable VendorSaveChangeVendor(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, DataTable dtUpload)
        {
            try
            {
                return VendorDAL.Instance.VendorSaveChangeVendor(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, dtUpload);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorCheckBeforeSave
        public DataTable VendorCheckBeforeSave(string I_PERS_CODE)
        {
            try
            {
                return VendorDAL.Instance.VendorCheckBeforeSave(I_PERS_CODE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorUpdate
        public DataTable VendorUpdate(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, string I_EMPSAPID, DataTable dtUpload)
        {
            try
            {
                return VendorDAL.Instance.VendorUpdate(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, I_EMPSAPID, dtUpload);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorP4Update
        public DataTable VendorP4Update(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, string I_REQ_ID,
            string I_EMPSAPID, DataTable dtUpload)
        {
            try
            {
                return VendorDAL.Instance.VendorP4Update(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, I_REQ_ID, I_EMPSAPID, dtUpload);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorUpdateVendor
        public DataTable VendorUpdateVendor(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, DataTable dtUpload)
        {
            try
            {
                return VendorDAL.Instance.VendorUpdateVendor(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, dtUpload);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorUpdateInuseBySemployeeid
        public bool VendorUpdateInuseBySemployeeid(string I_SEMPLOYEEID,
                                             string I_INUSE)
        {
            try
            {
                return VendorDAL.Instance.VendorUpdateInuseBySemployeeid(I_SEMPLOYEEID,
                                              I_INUSE);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorUpdateInActiveBySemployeeid
        public bool VendorUpdateInActiveBySemployeeid(string I_SEMPLOYEEID, string I_CACTIVE)
        {
            try
            {
                return VendorDAL.Instance.VendorUpdateInactiveBySemployeeid(I_SEMPLOYEEID, I_CACTIVE);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorUpdateSAPInActiveBySemployeeid
        public bool VendorUpdateSAPInActiveBySemployeeid(string I_SEMPLOYEEID, string I_DRVStatus)
        {
            try
            {
                return VendorDAL.Instance.VendorUpdateSAPInactiveBySemployeeid(I_SEMPLOYEEID, I_DRVStatus);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region EMPSAPIDUpdate
        public DataTable EMPSAPIDUpdate(string I_SEMPLOYEEID,
                                             string I_PERS_CODE,
                                            string I_EMPSAPID)
        {
            try
            {
                return VendorDAL.Instance.EMPSAPIDUpdate(I_SEMPLOYEEID,
                                              I_PERS_CODE, I_EMPSAPID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorPKEditOrApprove
        public DataTable VendorPKEditOrApprove(string I_SUPDATE, string I_SDESCRIPTION, string I_REQ_ID, string I_STATUS)
        {
            try
            {
                return VendorDAL.Instance.VendorPKEditOrApprove(I_SUPDATE, I_SDESCRIPTION, I_REQ_ID, I_STATUS);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorPKSaveDraft
        public DataTable VendorPKSaveDraft(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, string I_REQ_ID, DataTable dtUpload)
        {
            try
            {
                return VendorDAL.Instance.VendorPKSaveDraft(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, I_REQ_ID, dtUpload);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorPKSaveApprove
        public DataTable VendorPKSaveApprove(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, string I_REQ_ID, string I_EMPSAPID, DataTable dtUpload)
        {
            try
            {
                return VendorDAL.Instance.VendorPKSaveApprove(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, I_REQ_ID, I_EMPSAPID, dtUpload);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetSpecialCC_TeamStatusEmailBLL
        public DataTable GetSpecialCC_TeamStatusEmailBLL()
        {
            try
            {
                return VendorDAL.Instance.GetSpecialCC_TeamStatusEmailDAL();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion


        #endregion

        #region newvender

        #region VendorSave
        public DataTable VendorSave2(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                return VendorDAL.Instance.VendorSave2(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, dtUpload, dtUpload2);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorInsert2
        public DataTable VendorInsert2(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, string I_EMPSAPID, DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                return VendorDAL.Instance.VendorInsert2(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, I_EMPSAPID, dtUpload, dtUpload2);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable VendorInsert3(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, string I_EMPSAPID, DataTable dtUpload, DataTable dtUpload2, string SpotStatus, int ContractID)
        {
            try
            {
                return VendorDAL.Instance.VendorInsert3(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, I_EMPSAPID, dtUpload, dtUpload2, SpotStatus, ContractID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorP4Insert2
        public DataTable VendorP4Insert2(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID,
            string I_EMPSAPID, DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                return VendorDAL.Instance.VendorP4Insert2(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, I_EMPSAPID, dtUpload, dtUpload2);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable VendorP4Insert3(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID,
            string I_EMPSAPID, DataTable dtUpload, DataTable dtUpload2, string SpotStatus, int ContractID)
        {
            try
            {
                return VendorDAL.Instance.VendorP4Insert3(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, I_EMPSAPID, dtUpload, dtUpload2, SpotStatus, ContractID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorSaveChangeVendor2
        public DataTable VendorSaveChangeVendor2(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                return VendorDAL.Instance.VendorSaveChangeVendor2(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, dtUpload, dtUpload2);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorUpdate2
        public DataTable VendorUpdate2(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, string I_EMPSAPID, DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                return VendorDAL.Instance.VendorUpdate2(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, I_EMPSAPID, dtUpload, dtUpload2);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorP4Update2
        public DataTable VendorP4Update2(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, string I_REQ_ID,
            string I_EMPSAPID, DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                return VendorDAL.Instance.VendorP4Update2(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, I_REQ_ID, I_EMPSAPID, dtUpload, dtUpload2);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable VendorP4Update3(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, string I_REQ_ID,
            string I_EMPSAPID, DataTable dtUpload, DataTable dtUpload2, string SpotStatus, int ContractID)
        {
            try
            {
                return VendorDAL.Instance.VendorP4Update3(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, I_REQ_ID, I_EMPSAPID, dtUpload, dtUpload2, SpotStatus, ContractID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorUpdateVendor2
        public DataTable VendorUpdateVendor2(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                return VendorDAL.Instance.VendorUpdateVendor2(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, dtUpload, dtUpload2);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorPKSaveDraft2
        public DataTable VendorPKSaveDraft2(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, string I_REQ_ID, DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                return VendorDAL.Instance.VendorPKSaveDraft2(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, I_REQ_ID, dtUpload, dtUpload2);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable VendorPKSaveDraft3(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, string I_REQ_ID, DataTable dtUpload, DataTable dtUpload2, string SpotStatus, int ContractID)
        {
            try
            {
                return VendorDAL.Instance.VendorPKSaveDraft3(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, I_REQ_ID, dtUpload, dtUpload2, SpotStatus, ContractID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorPKSaveApprove2
        public DataTable VendorPKSaveApprove2(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, string I_REQ_ID, string I_EMPSAPID, DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                return VendorDAL.Instance.VendorPKSaveApprove2(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, I_REQ_ID, I_EMPSAPID, dtUpload, dtUpload2);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable VendorPKSaveApprove3(string I_SEMPLOYEEID,
                                              string I_FNAME,
                                              string I_LNAME,
                                              DateTime? I_DCREATE,
                                              string I_SUPDATE,
                                              string I_SCREATE,
                                              string I_PERS_CODE,
                                              string I_CARRIER,
                                              string I_LICENSE_NO,
                                              string I_DRVSTATUS,
                                              string I_STRANS_ID,
                                              int? I_SEMPTPYE,
                                              DateTime? I_DBIRTHDATE,
                                              string I_STEL,
                                              string I_STEL2,
                                              string I_SMAIL,
                                              string I_SMAIL2,
                                              DateTime? I_PERSONEL_BEGIN,
                                              DateTime? I_PERSONEL_EXPIRE,
                                              string I_CACTIVE,
                                              string I_CAUSEOVER,
                                              string I_SFILENAME,
                                              string I_SSYSFILENAME,
                                              string I_SPATH,
                                              string I_SDRIVERNO,
                                              DateTime? I_DDRIVEBEGIN,
                                              DateTime? I_DDRIVEEXPIRE,
                                              string I_SPERSONELNO,
                                              string I_BANSTATUS,
                                              string I_CAUSESAP,
                                              string I_CANCELSTATUS,
                                              string I_CAUSESAPCANCEL,
                                              string I_CAUSESAPCOMMIT,
                                              DateTime? I_WORKDATE
            , string I_NOTFILL, string I_TNAME, string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
            string I_STERMINALID, string I_REQ_ID, string I_EMPSAPID, DataTable dtUpload, DataTable dtUpload2, string SpotStatus, int ContractID)
        {
            try
            {
                return VendorDAL.Instance.VendorPKSaveApprove3(I_SEMPLOYEEID,
                                              I_FNAME,
                                              I_LNAME,
                                              I_DCREATE,
                                              I_SUPDATE,
                                              I_SCREATE,
                                              I_PERS_CODE,
                                              I_CARRIER,
                                              I_LICENSE_NO,
                                              I_DRVSTATUS,
                                              I_STRANS_ID,
                                              I_SEMPTPYE,
                                              I_DBIRTHDATE,
                                              I_STEL,
                                              I_STEL2,
                                              I_SMAIL,
                                              I_SMAIL2,
                                              I_PERSONEL_BEGIN,
                                              I_PERSONEL_EXPIRE,
                                              I_CACTIVE,
                                              I_CAUSEOVER,
                                              I_SFILENAME,
                                              I_SSYSFILENAME,
                                              I_SPATH,
                                              I_SDRIVERNO,
                                              I_DDRIVEBEGIN,
                                              I_DDRIVEEXPIRE,
                                              I_SPERSONELNO,
                                              I_BANSTATUS,
                                              I_CAUSESAP,
                                              I_CANCELSTATUS,
                                              I_CAUSESAPCANCEL,
                                              I_CAUSESAPCOMMIT,
                                              I_WORKDATE, I_NOTFILL, I_TNAME, I_NUMACCIDENT,
                                             I_ACCIDENTSTARTDATE,
                                             I_ACCIDENTENDDATE,
                                             I_SETTLETO, I_STERMINALID, I_REQ_ID, I_EMPSAPID, dtUpload, dtUpload2, SpotStatus, ContractID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #endregion

        #region VendorSelectBLL
        public DataTable VendorSelectBLL(string Condition)
        {
            try
            {
                return VendorDAL.Instance.VendorSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region LogInsert
        public bool LogInsert(string DETAIL, string PERS_CODE, DateTime? DATE_END, string STATUS, string DRIVER_STATUS_CHANGE_TO, int CREATE_BY, string SVENDORID, int CUSSCORE_TOTAL_DISABLE)
        {
            try
            {
                return VendorDAL.Instance.LogInsert(DETAIL, PERS_CODE, DATE_END, STATUS, DRIVER_STATUS_CHANGE_TO, CREATE_BY, SVENDORID, CUSSCORE_TOTAL_DISABLE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool ErrorLogInsert(string STATUS, string TYPE, string DETAIL, string CREATE_BY)
        {
            try
            {
                return VendorDAL.Instance.driverJobLogInsert(STATUS, TYPE, DETAIL, CREATE_BY);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region + Instance +
        private static VendorBLL _instance;
        public static VendorBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new VendorBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }

}
