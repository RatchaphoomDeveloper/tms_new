﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Common;
using TMS_DAL.Master;
using TMS_Entity;

namespace TMS_BLL.Master
{
    public class TVENDOR_SAP
    {
        public string SVENDORID { get; set; }
        public string SVENDORNAME { get; set; }
    }
    [Serializable]
    public class TQUESTIONNAIRELIST
    {
        public int? NQUESTIONNAIREID { get; set; }
        public int? NVISITFORMID { get; set; }
        public int? NGROUPID { get; set; }
        public int? NTYPEVISITFORMID { get; set; }
        public int? NVALUE { get; set; }
        public string SREMARK { get; set; }
        public int? NVALUEITEM { get; set; }
    }

    [Serializable]
    public class HeaderQuestionnaire
    {
        public int NGROUPID { get; set; }
        public string SGROUPNAME { get; set; }
    }
    [Serializable]
    public class RadioValueList
    {
        public int NTYPEVISITLISTSCORE { get; set; }
        public string STYPEVISITLISTNAME { get; set; }
        public string CONFIG_VALUE { get; set; }
    }
    /// <summary>
    /// เก็บ Form ที่ต้องกรอก
    /// </summary>
    [Serializable]
    public class FORMQUESTIONNAIRE
    {
        public int? NVISITFORMID { get; set; }
        public int? NGROUPID { get; set; }
        public string SVISITFORMNAME { get; set; }
        public string STYPEVISITFORMNAME { get; set; }
        public int? NTYPEVISITFORMID { get; set; }
        public int? YEAR { get; set; }
        public DateTime? DCHECK { get; set; }
        public int? SVENDORID { get; set; }
        public int? NQUESTIONNAIREID { get; set; }
        public string SADDRESS { get; set; }
        public string SREMARK { get; set; }
        public int? NVALUE { get; set; }
        public int? NNO { get; set; }
        public decimal? NWEIGHT { get; set; }
    }

    [Serializable]
    public class Questionnaire
    {
        public int? NQUESTIONNAIREID { get; set; }
        public string SVENDORID { get; set; }

        public DateTime? DCHECK { get; set; }
        public string SDCHECK { get; set; }
        public string SADDRESS { get; set; }
        public string SREMARK { get; set; }

        public DateTime? DCREATE { get; set; }
        public string SDCREATE { get; set; }

        public string SCREATE { get; set; }
        public string SDUPDATE { get; set; }

        public DateTime? DUPDATE { get; set; }
        public string SUPDATE { get; set; }

        public decimal? NVALUE { get; set; }
        public int? NNO { get; set; }
        public string YEAR { get; set; }
        public int? NTYPEVISITFORMID { get; set; }
        public List<TQUESTIONNAIRE_AS> LST_TQUESTIONNAIRE_AS { get; set; }
        public List<TQUESTIONNAIRE_BY> LST_TQUESTIONNAIRE_BY { get; set; }
        public List<F_UPLOAD> LST_FILE_UPLOAD { get; set; }
        public List<TQUESTIONNAIRELIST> LST_TQUESTIONNAIRELIST { get; set; }
        public DataTable DT_QUESTIONNAIRELIST { get; set; }
        public DataTable DT_TEAM { get; set; }
        public List<TQUESTIONNAIRE_TEAM> LST_TEAM { get; set; }
        public string IS_VENDOR_VIEW { get; set; }
    }
    [Serializable]
    public class TQUESTIONNAIRE
    {
        public decimal? NQUESTIONNAIREID { get; set; }
        public string SVENDORID { get; set; }
        public string SVENDORNAME { get; set; }
        public string SDCHECK { get; set; }
        public DateTime? DCHECK { get; set; }
        public string SADDRESS { get; set; }
        public string SREMARK { get; set; }
        public DateTime? DCREATE { get; set; }
        public string SCREATE { get; set; }
        public DateTime? DUPDATE { get; set; }
        public string SUPDATE { get; set; }
        public int? NVALUE { get; set; }
        public int? NNO { get; set; }
        public int? NTYPEVISITFORMID { get; set; }
        public string NYEAR { get; set; }
        public string NGROUP { get; set; }
    }
    [Serializable]
    public class F_UPLOAD
    {
        public decimal? ID { get; set; }
        public decimal? UPLOAD_ID { get; set; }
        public string UPLOAD_NAME { get; set; }
        public string FILENAME_SYSTEM { get; set; }
        public string FILENAME_USER { get; set; }
        public string FULLPATH { get; set; }
        public decimal? REF_INT { get; set; }
        public string REF_STR { get; set; }
    }
    /// <summary>
    /// รายชื่อคณะผู้ตรวจ
    /// </summary>
    [Serializable]
    public class TQUESTIONNAIRE_BY : TQUESTIONNAIRE_AS
    {

    }

    [Serializable]
    public class TQUESTIONNAIRE_TEAM
    {
        public decimal? QTEAM_ID { get; set; }
        public decimal? NQUESTIONNAIREID { get; set; }
        public string SNAME { get; set; }
        public decimal? TEAM_INDEX { get; set; }
        public string DCREATE { get; set; }
        public string SCREATE { get; set; }
        public string TEAM_TYPE { get; set; }
        public string TYPE_NAME { get; set; }
    }
    /// <summary>
    /// รายชื่อผู้รับการตรวจ 
    /// </summary> 
    [Serializable]
    public class TQUESTIONNAIRE_AS
    {
        public int? NQUESTIONNAIREID { get; set; }
        public int? NID { get; set; }
        public string SNAME { get; set; }
        public string DCREATE { get; set; }
        public string SCREATE { get; set; }
        public int? NINDEX { get; set; }
    }

    [Serializable]
    public class TTYPEOFVISITFORM
    {
        public int? NTYPEVISITFORMID { get; set; }
        public string STYPEVISITFORMNAME { get; set; }
        public string SDCREATE { get; set; }
        public string SCREATE { get; set; }
        public string SDUPDATE { get; set; }
        public string SUPDATE { get; set; }
        public string CACTIVE { get; set; }
        public string REFDOCID { get; set; }
        public string SHOWINLIST { get; set; }
        /// <summary>
        /// Radio Level
        /// </summary>
        public List<TTYPEOFVISITFORMLIST> LstTYPEOFVISITFORMLIST { get; set; }
        public DataTable DT_TGROUPOFVISITFORM { get; set; }
        public DataTable DT_GROUPDLL { get; set; }
        public DataTable DT_ITEMS { get; set; }
    }
    [Serializable]
    public class TTYPEOFVISITFORMLIST
    {
        public decimal? NTYPEVISITLISTID { get; set; }
        public decimal? NTYPEVISITFORMID { get; set; }
        public string STYPEVISITLISTNAME { get; set; }
        public decimal? NTYPEVISITLISTSCORE { get; set; }
        public string CONFIG_VALUE { get; set; }
        public string CACTIVE { get; set; }
    }
    [Serializable]
    public class SearhTTYPEOFVISITFORM
    {
        public string STYPEVISITFORMNAME { get; set; }
        public string SDUPDATE_START { get; set; }
        public string SDUPDATE_END { get; set; }
        public string CACTIVE { get; set; }
        public string SHOWINLIST { get; set; }
    }

    [Serializable]
    public class SearchFormConfig
    {
        public string STYPEVISITFORMNAME { get; set; }
        /// <summary>
        /// สถานะแบบฟอร์ม
        /// </summary>
        public string CACTIVE { get; set; }
        /// <summary>
        /// สถานะปีที่ใช้งาน
        /// </summary>
        public string Y_CACTIVE { get; set; }
        /// <summary>
        /// ปีที่ผูกกับแบบฟอร์ม
        /// </summary>
        public string YEAR { get; set; }

        public string ID { get; set; }
    }
    /// <summary>
    /// รายการคำถาม
    /// </summary>
    [Serializable]
    public class VisitForm
    {
        public decimal? NVISITFORMID { get; set; }
        public decimal? NTYPEVISITFORMID { get; set; }
        public decimal? NGROUPID { get; set; }
        public string SVISITFORMNAME { get; set; }
        public string CACTIVE { get; set; }
        public string SCREATE { get; set; }
        public string SUPDATE { get; set; }
        public string NWEIGHT { get; set; }
        public string ITEM_INDEX { get; set; }
        public string FORM_CODE { get; set; }
    }
    public class GroupOfVisitform
    {
        public string NGROUPID { get; set; }
        public string NTYPEVISITFORMID { get; set; }
        public string SGROUPNAME { get; set; }
        public string CACTIVE { get; set; }
        public string SCREATE { get; set; }
        public string SUPDATE { get; set; }
        public string NNO { get; set; }
    }
    public enum USER_PERMISSION
    {
        VIEW = 0,
        DISABLE = 1,
        ADD_EDIT = 2
    }

    public enum PAGE_ACTION
    {
        VIEW = 0,
        EDIT = 1,
        ADD = 2
    }

    [Serializable]
    public class VendorQuestionnaireBYYear
    {
        /// <summary>
        /// Form ที่ประเมิน
        /// </summary>
        public int NTYPEVISITFORMID { get; set; }
        /// <summary>
        /// ID ครั้งที่ประเมิน
        /// </summary>
        public int NQUESTIONNAIREID { get; set; }
        public string SVENDORID { get; set; }
        public DateTime? DCHECK { get; set; }
        public string SADDRESS { get; set; }
        public string SREMARK { get; set; }
        public decimal? NVALUE { get; set; }
        public string SVENDORNAME { get; set; }
        /// <summary>
        /// ครั้งที่
        /// </summary>
        public int? NNO { get; set; }
        /// <summary>
        /// ปีที่ประเมิน
        /// </summary>
        public string YEAR { get; set; }
    }

    public class QuestionnaireBLL
    {
        #region " ข้อมูลแบบฟอร์มการทำงาน admin_VisitForm_add_.aspx"
        /// <summary>
        /// ข้อมูลระดับคะแนน 
        /// </summary>
        /// <param name="_err"></param>
        /// <param name="NTYPEVISITFORMID"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        public List<TTYPEOFVISITFORMLIST> GetTTYPEOFVISITFORMLIST(ref string _err, int NTYPEVISITFORMID)
        {
            var result = new List<TTYPEOFVISITFORMLIST>();
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"SELECT t.NTYPEVISITLISTID,
                  t.NTYPEVISITFORMID,
                  c.CONFIG_NAME as STYPEVISITLISTNAME,
                  t.NTYPEVISITLISTSCORE,
                  t.CONFIG_VALUE,
                  t.CACTIVE
                FROM TTYPEOFVISITFORMLIST t
                INNER JOIN C_CONFIG c
                ON c.CONFIG_TYPE   ='AUDIT_LEVEL'
                AND c.CONFIG_VALUE = t.CONFIG_VALUE and t.NTYPEVISITFORMID =:NTYPEVISITFORMID"));
                paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", NTYPEVISITFORMID.ToString()));
                DataTable dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                Utility.DataModelHelper.CreateListN<TTYPEOFVISITFORMLIST>(ref result, dt);
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }
        public bool CheckDuplicateFormName(ref string _err, int? NTYPEVISITFORMID, string STYPEVISITFORMNAME)
        {
            bool result = false;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string _conn = string.Empty;
                if (NTYPEVISITFORMID > 0)
                {
                    _conn = " and NTYPEVISITFORMID!=:NTYPEVISITFORMID";
                    paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", NTYPEVISITFORMID.ToString()));
                }

                _sb.Append(System.String.Format(@"select count(*) from TTYPEOFVISITFORM  where 1=1 {0} and STYPEVISITFORMNAME=:STYPEVISITFORMNAME ", _conn));
                paramList.Add(new ParameterEntity(":STYPEVISITFORMNAME", STYPEVISITFORMNAME.ToString()));
                DataTable dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                result = int.Parse(dt.Rows[0][0].ToString()) > 0 ? true : false;
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        public bool CheckDuplicateRadio(ref string _err, int NTYPEVISITFORMID, string CONFIG_VALUE)
        {
            bool result = false;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"select count(*) from TTYPEOFVISITFORMLIST where CONFIG_VALUE=:CONFIG_VALUE and NTYPEVISITFORMID=:NTYPEVISITFORMID"));
                paramList.Add(new ParameterEntity(":CONFIG_VALUE", CONFIG_VALUE.ToString()));
                paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", NTYPEVISITFORMID.ToString()));
                DataTable dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                result = int.Parse(dt.Rows[0][0].ToString()) > 0 ? true : false;
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
        /// <summary>
        /// ข้อมูลชื่อหมวด/กลุ่ม 
        /// </summary>
        /// <param name="_err"></param>
        /// <param name="NTYPEVISITFORMID"></param>
        /// <returns></returns>
        public DataTable GetTGROUPOFVISITFORM(ref string _err, int NTYPEVISITFORMID, bool OnlyIsActive = false)
        {
            DataTable result = null;
            try
            {
                string _conn = string.Empty;
                if (OnlyIsActive)
                {
                    _conn = " and CACTIVE ='1' ";
                }
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"SELECT NGROUPID,
                  NTYPEVISITFORMID, SGROUPNAME, CACTIVE, SCREATE, DCREATE, SUPDATE, DUPDATE, NNO,GROUP_INDEX
                FROM TGROUPOFVISITFORM where NTYPEVISITFORMID=:NTYPEVISITFORMID {0} order by GROUP_INDEX asc", _conn));
                paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", NTYPEVISITFORMID.ToString()));
                result = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public DataTable GetITEM_VISITFORM(ref string _err, int NTYPEVISITFORMID, int? groupId = 0, bool OnlyInGroupActive = true)
        {
            DataTable result = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                string _condition = string.Empty;
                if (groupId > 0)
                {
                    _condition = String.Format(@" and F.NGROUPID ={0}", groupId.ToString());
                }

                if (OnlyInGroupActive)
                {
                    _condition += String.Format(@" and G.CACTIVE ='1'");
                }

                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"SELECT F.*, G.GROUP_INDEX, G.SGROUPNAME from TVISITFORM F 
                INNER JOIN TGROUPOFVISITFORM G ON G.NGROUPID=F.NGROUPID where F.NTYPEVISITFORMID=:NTYPEVISITFORMID {0} order by G.GROUP_INDEX,TO_NUMBER(F.FORM_CODE, '99D99') asc", _condition));
                paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", NTYPEVISITFORMID.ToString()));
                result = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public bool MarkTimeStamp(ref string _err, int formID)
        {
            bool result = false;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                if (formID > 0)
                {
                    _sb.Append(System.String.Format(@"UPDATE TTYPEOFVISITFORM SET DUPDATE=sysdate where NTYPEVISITFORMID={0}", formID));
                    result = new cmdCommonDAL().ExecuteNonQuery(_sb.ToString(), paramList, out _err);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public bool IsDupFormCode(ref string _err, VisitForm itm)
        {
            bool result = false;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                if (!itm.NVISITFORMID.HasValue) itm.NVISITFORMID = 0;
                _sb.Append(System.String.Format(@" select count(*) from TVISITFORM where FORM_CODE=:FORM_CODE and  NVISITFORMID<>:NVISITFORMID and NGROUPID=:NGROUPID and  NTYPEVISITFORMID=:NTYPEVISITFORMID "));
                paramList.Add(new ParameterEntity(":FORM_CODE", itm.FORM_CODE.ToString()));
                paramList.Add(new ParameterEntity(":NVISITFORMID", itm.NVISITFORMID.ToString()));
                paramList.Add(new ParameterEntity(":NGROUPID", itm.NGROUPID.ToString()));
                paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", itm.NTYPEVISITFORMID.ToString()));
                DataTable dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                result = dt.Rows[0][0].ToString() == "0" ? false : true;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        public bool ActionItemForm(ref string _err, VisitForm itm)
        {
            bool result = false;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();

                result = IsDupFormCode(ref _err, itm);
                if (result)
                {
                    _err = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากรหัสคำถามนี้มีในระบบแล้ว";
                    return !result;
                }

                if (!itm.NVISITFORMID.HasValue) itm.NVISITFORMID = 0;
                if (itm.NVISITFORMID > 0)
                {
                    _sb.Append(System.String.Format(@" UPDATE TVISITFORM SET SVISITFORMNAME=:SVISITFORMNAME, CACTIVE=:CACTIVE, SUPDATE=:SUPDATE,  DUPDATE=sysdate, NWEIGHT=:NWEIGHT,FORM_CODE=:FORM_CODE WHERE NVISITFORMID=:NVISITFORMID"));
                    paramList.Add(new ParameterEntity(":SVISITFORMNAME", itm.SVISITFORMNAME.ToString()));
                    paramList.Add(new ParameterEntity(":CACTIVE", itm.CACTIVE.ToString()));
                    paramList.Add(new ParameterEntity(":SUPDATE", itm.SUPDATE.ToString()));
                    paramList.Add(new ParameterEntity(":NWEIGHT", itm.NWEIGHT.ToString()));
                    paramList.Add(new ParameterEntity(":FORM_CODE", itm.FORM_CODE.ToString()));
                    paramList.Add(new ParameterEntity(":NVISITFORMID", itm.NVISITFORMID.ToString()));
                }
                else
                {
                    int? refId = GetItemRefID(ref _err);
                    _sb.Append(System.String.Format(@"INSERT INTO TVISITFORM ( NVISITFORMID, NTYPEVISITFORMID, NGROUPID, SVISITFORMNAME,  CACTIVE, SCREATE, DCREATE, SUPDATE, DUPDATE, NWEIGHT,  ITEM_INDEX, FORM_CODE ) VALUES (:NVISITFORMID, :NTYPEVISITFORMID, :NGROUPID, :SVISITFORMNAME,  :CACTIVE, :SCREATE, sysdate, :SUPDATE, sysdate, :NWEIGHT, ( select decode(MAX(ITEM_INDEX),null,0,MAX(ITEM_INDEX))+1 as NINDEX  from TVISITFORM where NGROUPID=:NGROUPID ) ,:FORM_CODE) "));
                    paramList.Add(new ParameterEntity(":NVISITFORMID", refId.ToString()));
                    paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", itm.NTYPEVISITFORMID.ToString()));
                    paramList.Add(new ParameterEntity(":NGROUPID", itm.NGROUPID.ToString()));
                    paramList.Add(new ParameterEntity(":SVISITFORMNAME", itm.SVISITFORMNAME.ToString()));
                    paramList.Add(new ParameterEntity(":CACTIVE", itm.CACTIVE.ToString()));
                    paramList.Add(new ParameterEntity(":SCREATE", itm.SCREATE.ToString()));
                    paramList.Add(new ParameterEntity(":SUPDATE", itm.SUPDATE.ToString()));
                    paramList.Add(new ParameterEntity(":NWEIGHT", itm.NWEIGHT.ToString()));
                    paramList.Add(new ParameterEntity(":FORM_CODE", itm.FORM_CODE.ToString()));
                }
                result = new cmdCommonDAL().ExecuteNonQuery(_sb.ToString(), paramList, out _err);
                if (result) result = this.MarkTimeStamp(ref _err, int.Parse(itm.NTYPEVISITFORMID.ToString()));
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        public bool ActionGroupForm(ref string _err, GroupOfVisitform itm)
        {
            bool result = false;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                if (int.Parse(itm.NGROUPID) > 0) //Edit
                {
                    _sb.Append(System.String.Format(@" UPDATE TGROUPOFVISITFORM SET 
                      SGROUPNAME=:SGROUPNAME, CACTIVE=:CACTIVE,  SUPDATE=:SUPDATE, DUPDATE=sysdate  where NGROUPID=:NGROUPID"));
                    paramList.Add(new ParameterEntity(":SGROUPNAME", itm.SGROUPNAME.ToString()));
                    paramList.Add(new ParameterEntity(":CACTIVE", itm.CACTIVE.ToString()));
                    paramList.Add(new ParameterEntity(":SUPDATE", itm.SUPDATE.ToString()));
                    paramList.Add(new ParameterEntity(":NGROUPID", itm.NGROUPID.ToString()));
                }
                else
                {
                    int? refId = GetGroupRefID(ref _err);
                    _sb.Append(System.String.Format(@" INSERT INTO TGROUPOFVISITFORM
                  ( NGROUPID, NTYPEVISITFORMID,  SGROUPNAME, CACTIVE, SCREATE, DCREATE, SUPDATE, DUPDATE,  NNO, GROUP_INDEX )
                  VALUES
                  ( :NGROUPID, :NTYPEVISITFORMID, :SGROUPNAME, :CACTIVE, :SCREATE, SYSDATE, :SUPDATE, SYSDATE, 0 ,( select decode(MAX(GROUP_INDEX),null,0,MAX(GROUP_INDEX))+1 as NINDEX  from TGROUPOFVISITFORM where NTYPEVISITFORMID=:NTYPEVISITFORMID ))"));

                    paramList.Add(new ParameterEntity(":NGROUPID", refId.ToString()));
                    paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", itm.NTYPEVISITFORMID.ToString()));
                    paramList.Add(new ParameterEntity(":SGROUPNAME", itm.SGROUPNAME.ToString()));
                    paramList.Add(new ParameterEntity(":CACTIVE", itm.CACTIVE.ToString()));
                    paramList.Add(new ParameterEntity(":SCREATE", itm.SCREATE.ToString()));
                    paramList.Add(new ParameterEntity(":SUPDATE", itm.SUPDATE.ToString()));

                }
                result = new cmdCommonDAL().ExecuteNonQuery(_sb.ToString(), paramList, out _err);
                if (result) result = this.MarkTimeStamp(ref _err, int.Parse(itm.NTYPEVISITFORMID.ToString()));
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }


        public int? InsertForm(ref string _err, UserProfile user, TTYPEOFVISITFORM form, string _conn)
        {
            int? result = 0;
            try
            {

                result = GetTYPEOFVISITFORMID(ref _err);
                StringBuilder _sb = new StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                if (form.NTYPEVISITFORMID > 0)
                {

                    _sb.Append(System.String.Format(@"INSERT INTO TTYPEOFVISITFORM (NTYPEVISITFORMID,STYPEVISITFORMNAME,DCREATE,SCREATE,SHOWINLIST) VALUES (
                :REFID ,:STYPEVISITFORMNAME,SYSTIMESTAMP,:SCREATE,:SHOWINLIST) "));
                    paramList.Add(new ParameterEntity(":REFID", result.ToString()));
                    paramList.Add(new ParameterEntity(":STYPEVISITFORMNAME", form.STYPEVISITFORMNAME.ToString()));
                    paramList.Add(new ParameterEntity(":SCREATE", user.USER_ID.ToString()));
                    paramList.Add(new ParameterEntity(":SHOWINLIST", form.SHOWINLIST.ToString()));
                    bool resultNonQuery = new cmdCommonDAL().ExecuteNonQuery(_sb.ToString(), paramList, out _err);
                }

            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        /// <summary>
        ///  ข้อมูลแบบฟอร์ม 
        /// </summary>
        /// <param name="_err"></param>
        /// <param name="NTYPEVISITFORMID"></param>
        /// <returns></returns>
        public TTYPEOFVISITFORM getTTYPEOFVISITFORM(ref string _err, int? NTYPEVISITFORMID)
        {
            var result = new TTYPEOFVISITFORM();
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"SELECT NTYPEVISITFORMID, STYPEVISITFORMNAME, DUSE, DCREATE, SCREATE, DUPDATE, SUPDATE,  CACTIVE, REFDOCID, SHOWINLIST FROM TTYPEOFVISITFORM where NTYPEVISITFORMID=:NTYPEVISITFORMID"));
                paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", NTYPEVISITFORMID.ToString()));
                DataTable dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                if (dt != null)
                {
                    result.NTYPEVISITFORMID = int.Parse(dt.Rows[0]["NTYPEVISITFORMID"].ToString());
                    result.CACTIVE = dt.Rows[0]["CACTIVE"].ToString();
                    result.SHOWINLIST = dt.Rows[0]["SHOWINLIST"].ToString();
                    result.STYPEVISITFORMNAME = dt.Rows[0]["STYPEVISITFORMNAME"].ToString();
                    result.SUPDATE = dt.Rows[0]["SUPDATE"].ToString();
                    result.LstTYPEOFVISITFORMLIST = GetTTYPEOFVISITFORMLIST(ref _err, int.Parse(result.NTYPEVISITFORMID.ToString()));
                    result.DT_TGROUPOFVISITFORM = GetTGROUPOFVISITFORM(ref _err, int.Parse(result.NTYPEVISITFORMID.ToString()));
                    result.DT_GROUPDLL = GetTGROUPOFVISITFORM(ref _err, int.Parse(result.NTYPEVISITFORMID.ToString()), true);
                    result.DT_ITEMS = GetITEM_VISITFORM(ref _err, int.Parse(result.NTYPEVISITFORMID.ToString()));
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        #endregion " ข้อมูลแบบฟอร์มการทำงาน admin_VisitForm_add_.aspx"

        #region " ข้อมูลผลประเมินการบริหารงาน questionnaire.aspx"

        private class ToppicHeader
        {
            public string GroupName { get; set; }
            public string GroupId { get; set; }
        }
        public bool CopyForm(ref string _err, int NTYPEVISITFORMID)
        {
            bool result = true;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                int DocId = GetTTYPEOFVISITFORM_RefID(ref _err);

            }
            catch (Exception)
            {
                result = false;
                throw;
            }
            return result;
        }
        public DataTable GetScoreVendorInForm(ref string _err, TQUESTIONNAIRE search)
        {
            DataTable result = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string _condition = string.Empty;
                #region " SQL "
                _sb.Append(System.String.Format(@"SELECT DECODE (TSCORE.SUM_SCORE,0,0, ROUND(TSCORE.SUM_SCORE/
                  (SELECT MAX(NTYPEVISITLISTSCORE)
                  FROM TTYPEOFVISITFORMLIST M
                  WHERE M.NTYPEVISITFORMID = TSCORE.NTYPEVISITFORMID
                  AND CACTIVE              ='1'
                  ),2)) AS TSUM_SCORE ,
                  TSCORE.*
                FROM
                  (SELECT DECODE(SUM_SCORE,'',0,SUM_SCORE) AS SUM_SCORE ,
                 DECODE(
                  (SELECT SUM(NWEIGHT) AS NWEIGHT FROM TVISITFORM WHERE NGROUPID =G.NGROUPID
                  AND NTYPEVISITFORMID                                           =G.NTYPEVISITFORMID
                  AND CACTIVE                                                    ='1'
                  ),'',0,
                  (SELECT SUM(NWEIGHT) AS NWEIGHT
                  FROM TVISITFORM
                  WHERE NGROUPID      =G.NGROUPID
                  AND NTYPEVISITFORMID=G.NTYPEVISITFORMID
                  AND CACTIVE         ='1'
                  )) AS NWEIGHT,
                  G.*,
                  SC.NQUESTIONNAIREID,
                  (SELECT COUNT(NGROUPID)
                  FROM TVISITFORM
                  WHERE NGROUPID      =G.NGROUPID
                  AND NTYPEVISITFORMID=G.NTYPEVISITFORMID
                  AND CACTIVE         ='1'
                  ) AS ITMGRUP
                FROM
                  ( SELECT DISTINCT TG.NGROUPID,
                    TG.NTYPEVISITFORMID,
                    TG.SGROUPNAME
                  FROM TGROUPOFVISITFORM TG
                  WHERE TG.NTYPEVISITFORMID IN
                    (SELECT TY.NTYPEVISITFORMID
                    FROM TTYPEOFVISITFORM_YEARS TY
                    WHERE TY.IS_ACTIVE ='1'
                    AND TY.YEAR        =:NYEAR
                    )
                  AND TG.CACTIVE ='1'
                  ORDER BY TG.NGROUPID ASC
                  ) G
                LEFT JOIN
                  (SELECT SUM( RESULT.QITEM_SCORE) AS SUM_SCORE ,
                    SUM(NWEIGHT)                   AS NWEIGHT,
                    RESULT.SVENDORNAME,
                    RESULT.SVENDORID,
                    RESULT.STYPEVISITFORMNAME,
                    RESULT.NTYPEVISITFORMID,
                    RESULT.NGROUPID,
                    RESULT.SGROUPNAME,
                    RESULT.NYEAR,
                    TO_CHAR(RESULT.DCHECK,'DD/MM/YYYY') AS DCHECK,
                    RESULT.NQUESTIONNAIREID
                  FROM
                    (SELECT QT.*,
                      V.SVENDORNAME,
                      F.STYPEVISITFORMNAME,
                      F.NWEIGHT,
                      rdo.NTYPEVISITLISTSCORE*F.NWEIGHT AS QITEM_SCORE ,
                      Q.SVENDORID,
                      Q.NYEAR,
                      Q.DCHECK,
                      F.SGROUPNAME
                    FROM TQUESTIONNAIRELIST QT
                    INNER JOIN
                      (SELECT TF.*,
                        TTF.STYPEVISITFORMNAME,
                        TG.SGROUPNAME
                      FROM TTYPEOFVISITFORM_YEARS TY
                      INNER JOIN TVISITFORM TF
                      ON TF.NTYPEVISITFORMID = TY.NTYPEVISITFORMID
                      AND TY.IS_ACTIVE       ='1'
                      AND TY.YEAR            =:NYEAR
                      INNER JOIN TTYPEOFVISITFORM TTF
                      ON TTF.NTYPEVISITFORMID = TF.NTYPEVISITFORMID
                      INNER JOIN TGROUPOFVISITFORM TG
                      ON TF.NGROUPID            = TG.NGROUPID
                      AND TG.CACTIVE            ='1'
                      AND TF.CACTIVE               ='1'
                      ) F ON F.NTYPEVISITFORMID = QT.NTYPEVISITFORMID
                    AND F.NVISITFORMID          = QT.NVISITFORMID
                    /*AND F.CACTIVE               ='1'*/
                    INNER JOIN TTYPEOFVISITFORMLIST rdo
                    ON rdo.NTYPEVISITFORMID = QT.NTYPEVISITFORMID
                    AND rdo.CONFIG_VALUE    = QT.NVALUEITEM AND  rdo.CACTIVE               ='1'
                    INNER JOIN TQUESTIONNAIRE Q
                    ON Q.NQUESTIONNAIREID   = QT.NQUESTIONNAIREID
                    AND QT.NTYPEVISITFORMID = Q.NTYPEVISITFORMID
                    AND Q.NYEAR             =:NYEAR
                    INNER JOIN TVENDOR_SAP V
                    ON V.SVENDORID  = Q.SVENDORID
                    AND V.SVENDORID =:SVENDORID
                    ) RESULT
                  GROUP BY RESULT.SVENDORNAME,
                    RESULT.STYPEVISITFORMNAME,
                    RESULT.NTYPEVISITFORMID,
                    RESULT.NYEAR,
                    TO_CHAR(RESULT.DCHECK,'DD/MM/YYYY'),
                    RESULT.NQUESTIONNAIREID,
                    RESULT.SGROUPNAME,
                    RESULT.NGROUPID,
                    RESULT.SVENDORID
                  ) SC ON SC.NGROUPID = G.NGROUPID ) TSCORE "));
                #endregion " SQL "
                paramList.Add(new ParameterEntity(":SVENDORID", search.SVENDORID.ToString()));
                paramList.Add(new ParameterEntity(":NYEAR", search.NYEAR.ToString()));
                result = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public DataTable GetListScoreByYear(ref string _err, int? START_YEAR, int? END_YEAR, string SVENDORID, bool ISVendor, bool showVENDORID = false)
        {
            DataTable result = new DataTable("RESULT");
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string _condition = string.Empty;
                if (!string.IsNullOrEmpty(SVENDORID))
                {
                    _condition = string.Format(" and Q.SVENDORID='{0}'", SVENDORID);
                }

                int? colYear = START_YEAR - END_YEAR;
                result.Columns.Add("ROWNUM", typeof(String));
                result.Columns.Add("SVENDORNAME", typeof(String));
                result.Columns.Add("SVENDORID", typeof(String));
                

                for (int? i = START_YEAR; i <= END_YEAR; i++)
                {
                    result.Columns.Add(i.ToString(), typeof(String));
                }

                if (ISVendor)
                {
                    _condition +=string.Format(" and Q.IS_VENDOR_VIEW='1'");
                }

                #region " SQL "
                string sql = string.Format(@"SELECT DECODE (SUM_SCORE,0,0,ROUND(SUM_SCORE/
                        (SELECT MAX(NTYPEVISITLISTSCORE)
                        FROM TTYPEOFVISITFORMLIST M
                        WHERE M.NTYPEVISITFORMID = TSCORE.NTYPEVISITFORMID
                        AND CACTIVE              ='1'
                        ),2)) as SUM_SCORE,
                        SVENDORID,
                        SVENDORNAME,
                        STYPEVISITFORMNAME,
                        NTYPEVISITFORMID,
                        NYEAR,
                        DCHECK,NQUESTIONNAIREID
                    FROM (SELECT SUM( RESULT.QITEM_SCORE) AS SUM_SCORE ,
                  RESULT.SVENDORNAME,
                  RESULT.SVENDORID,
                  RESULT.STYPEVISITFORMNAME,
                  RESULT.NTYPEVISITFORMID,
                  RESULT.NYEAR,
                  TO_CHAR(RESULT.DCHECK,'DD/MM/YYYY') AS DCHECK,
                  RESULT.NQUESTIONNAIREID
                FROM
                  (SELECT QT.*,
                    V.SVENDORNAME,
                    F.STYPEVISITFORMNAME,
                    F.NWEIGHT,
                    rdo.NTYPEVISITLISTSCORE*F.NWEIGHT AS QITEM_SCORE ,
                    Q.SVENDORID,
                    Q.NYEAR,
                    Q.DCHECK
                  FROM TQUESTIONNAIRELIST QT
                  INNER JOIN
                    (SELECT DISTINCT TF.*,
                      TY.YEAR,
                      TTF.STYPEVISITFORMNAME
                    FROM TTYPEOFVISITFORM_YEARS TY
                    INNER JOIN TVISITFORM TF
                    ON TF.NTYPEVISITFORMID           = TY.NTYPEVISITFORMID
                    AND TF.CACTIVE               ='1'
                    AND TY.IS_ACTIVE                 ='1'
                    AND TO_NUMBER(TY.YEAR , '9999') >= TO_NUMBER(:START_YEAR, '9999')
                    AND TO_NUMBER(TY.YEAR , '9999') <=TO_NUMBER(:END_YEAR, '9999')
                    INNER JOIN TTYPEOFVISITFORM TTF
                    ON TTF.NTYPEVISITFORMID = TF.NTYPEVISITFORMID
                    WHERE TF.NGROUPID      IN
                      (SELECT NGROUPID
                      FROM TGROUPOFVISITFORM TG
                      WHERE TG.NTYPEVISITFORMID = TTF.NTYPEVISITFORMID
                      AND TG.CACTIVE            ='1'
                      )
                    ) F ON F.NTYPEVISITFORMID = QT.NTYPEVISITFORMID
                  AND F.NVISITFORMID          = QT.NVISITFORMID
                  /*AND F.CACTIVE               ='1'*/
                  INNER JOIN TTYPEOFVISITFORMLIST rdo
                  ON rdo.NTYPEVISITFORMID = QT.NTYPEVISITFORMID
                  AND rdo.CONFIG_VALUE    = QT.NVALUEITEM AND  rdo.CACTIVE               ='1'
                  INNER JOIN TQUESTIONNAIRE Q
                  ON Q.NQUESTIONNAIREID            = QT.NQUESTIONNAIREID
                  AND QT.NTYPEVISITFORMID          = Q.NTYPEVISITFORMID
                  AND TO_NUMBER(Q.NYEAR , '9999') >= TO_NUMBER(:START_YEAR, '9999')
                  AND TO_NUMBER(Q.NYEAR , '9999') <=TO_NUMBER(:END_YEAR, '9999') {0}
                  AND F.YEAR                      = Q.NYEAR
                  INNER JOIN TVENDOR_SAP V
                  ON V.SVENDORID = Q.SVENDORID
                  ) RESULT
                GROUP BY RESULT.SVENDORNAME,
                  RESULT.STYPEVISITFORMNAME,
                  RESULT.NTYPEVISITFORMID,
                  RESULT.NYEAR,
                  TO_CHAR(RESULT.DCHECK,'DD/MM/YYYY'),
                  RESULT.NQUESTIONNAIREID,
                  RESULT.SVENDORID) TSCORE", _condition);
                #endregion " SQL "
                paramList.Add(new ParameterEntity(":START_YEAR", START_YEAR.ToString()));
                paramList.Add(new ParameterEntity(":END_YEAR", END_YEAR.ToString()));
                DataTable DtAllScoreByVendor = new cmdCommonDAL().ExecuteAdaptor(sql, paramList, out _err);
                if (DtAllScoreByVendor.Rows.Count > 0)// มีการกรอกคะแนนแล้ว
                {
                    DataView view = new DataView(DtAllScoreByVendor);
                    DataTable distinctValues = view.ToTable(true, "SVENDORID", "SVENDORNAME");
                    int rownum = 1;
                    string YEAR = string.Empty;
                    string VENDORID = string.Empty;
                    foreach (DataRow q in distinctValues.Rows)
                    {
                        VENDORID = q["SVENDORID"].ToString();
                        DataRow resultRow = result.NewRow();
                        resultRow["ROWNUM"] = rownum.ToString();
                        resultRow["SVENDORNAME"] = q["SVENDORNAME"];
                        resultRow["SVENDORID"] = q["SVENDORID"];
                        for (int? i = START_YEAR; i <= END_YEAR; i++)
                        {
                            YEAR = i.ToString();
                            string expression = string.Format(" NYEAR='{0}' and SVENDORID={1}", YEAR, VENDORID);
                            DataRow[] foundRows = null;
                            foundRows = DtAllScoreByVendor.Select(expression);
                            if (foundRows.Length > 0) 
                                resultRow[YEAR] = foundRows[0]["SUM_SCORE"].ToString();                            
                            else 
                                resultRow[YEAR] = 0;
                            
                        }
                        result.Rows.Add(resultRow);
                        rownum++;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            if (!showVENDORID) result.Columns.Remove("SVENDORID"); 

            return result;
        }

        public bool ISQueationComplete(ref string _err, TQUESTIONNAIRE search)
        {
            bool result = false;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string _condition = string.Empty;
                if (!string.IsNullOrEmpty(search.SVENDORID))
                {
                    _condition = string.Format(" and Q.SVENDORID='{0}'", search.SVENDORID);
                }

                #region " sql "
                _sb.Append(System.String.Format(@""));
                paramList.Add(new ParameterEntity(":NYEAR", search.NYEAR.ToString()));
                DataTable dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                #endregion " sql "
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        public DataTable GetListYearForInputForm(ref string _err)
        {
            DataTable result = new DataTable();
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@" SELECT *  FROM TTYPEOFVISITFORM_YEARS where IS_ACTIVE='1'"));
                result = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        public DataTable GetListScoreByGroup(ref string _err, TQUESTIONNAIRE search, bool ISVendor)
        {
            DataTable result = new DataTable("RESULT");
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string _condition = string.Empty;

                int colToppic = 0;
                #region " sql Get Topic "
                _sb.Append(System.String.Format(@"SELECT DISTINCT TG.NGROUPID, TG.NTYPEVISITFORMID, TG.SGROUPNAME
                  FROM TGROUPOFVISITFORM TG WHERE TG.NTYPEVISITFORMID IN (SELECT TY.NTYPEVISITFORMID  FROM TTYPEOFVISITFORM_YEARS TY WHERE TY.IS_ACTIVE ='1' AND TY.YEAR =:NYEAR)
                  AND TG.CACTIVE ='1' ORDER BY TG.NGROUPID ASC"));
                #endregion " sql Get Topic"
                paramList.Add(new ParameterEntity(":NYEAR", search.NYEAR.ToString()));
                DataTable DtTopTic = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                if (DtTopTic.Rows.Count > 0)
                {
                    List<ToppicHeader> lstToppic = new List<ToppicHeader>();
                    List<TQUESTIONNAIRE> LstQuestionnaire = new List<TQUESTIONNAIRE>();
                    colToppic = DtTopTic.Rows.Count;
                    /// Init Column
                    foreach (DataRow dtRow in DtTopTic.Rows)
                    {
                        string SGROUPNAME = dtRow["SGROUPNAME"].ToString().Replace(Environment.NewLine, "").Replace("\r", "").Replace("\n", "").Replace("\t", "").Trim();
                        result.Columns.Add(SGROUPNAME, typeof(decimal));
                        lstToppic.Add(new ToppicHeader()
                        {
                            GroupId = dtRow["NGROUPID"].ToString(),
                            GroupName = SGROUPNAME
                        });
                    }
                    result.Columns.Add("ROWNUM", typeof(String));
                    result.Columns.Add("SVENDORNAME", typeof(String));
                    result.Columns.Add("TOTAL", typeof(decimal));
                    result.Columns.Add("PERCENT", typeof(decimal));
                    result.Columns.Add("SDCHECK", typeof(String));
                    result.Columns.Add("NQUESTIONNAIREID", typeof(decimal));
                    result.Columns.Add("NTYPEVISITFORMID", typeof(decimal));
                    result.Columns.Add("IS_VENDOR_VIEW", typeof(String));

                    /// Get Vender from QUESTIONNAIRE
                    if (!string.IsNullOrEmpty(search.SVENDORID))
                    {
                        _condition +=string.Format(" and Q.SVENDORID='{0}'", search.SVENDORID);
                    }

                    if (ISVendor)
                    {
                        _condition +=string.Format(" and Q.IS_VENDOR_VIEW='1'");
                    }

                    string sql = string.Format(@"SELECT A.SVENDORID,to_char(DCHECK,'DD/MM/YYYY') as SDCHECK,
                      A.NQUESTIONNAIREID,
                      A.NYEAR,
                      A.NTYPEVISITFORMID,A.IS_VENDOR_VIEW ,
                      V.SVENDORNAME, (SELECT PERCENT FROM TTYPEOFVISITFORM_YEARS  WHERE IS_ACTIVE ='1'  AND YEAR =:NYEAR) as PERCENT
                    FROM
                      (SELECT Q.DCHECK,
                        Q.SVENDORID,
                        Q.NQUESTIONNAIREID,
                        Q.NYEAR,
                        Q.NTYPEVISITFORMID , decode(Q.IS_VENDOR_VIEW,'',0,Q.IS_VENDOR_VIEW) as IS_VENDOR_VIEW
                      FROM TQUESTIONNAIRE Q
                      INNER JOIN TVENDOR_SAP V
                      ON V.SVENDORID          = Q.SVENDORID
                      AND Q.NYEAR             = :NYEAR {0}
                      AND Q.NTYPEVISITFORMID IS NULL
                      UNION ALL
                      SELECT Q.DCHECK,
                        Q.SVENDORID,
                        Q.NQUESTIONNAIREID,
                        Q.NYEAR,
                        Q.NTYPEVISITFORMID , decode(Q.IS_VENDOR_VIEW,'',0,Q.IS_VENDOR_VIEW) as IS_VENDOR_VIEW
                      FROM TQUESTIONNAIRE Q
                      WHERE Q.NYEAR           = :NYEAR
                      AND Q.NTYPEVISITFORMID IN
                        (SELECT NTYPEVISITFORMID
                        FROM TTYPEOFVISITFORM_YEARS TY
                        WHERE TY.IS_ACTIVE ='1'
                        AND TY.YEAR        =:NYEAR
                        ) {0}
                      ) A
                    INNER JOIN TVENDOR_SAP V
                    ON V.SVENDORID = A.SVENDORID
                    AND V.CACTIVE  ='1'", _condition);

                    DataTable DtVendorList = new cmdCommonDAL().ExecuteAdaptor(sql, paramList, out _err);
                    if (DtVendorList.Rows.Count > 0)
                    {
                        #region " SQl "
                        sql = String.Format(@"SELECT DECODE (SUM_SCORE,0,0, ROUND(SUM_SCORE/
                            (SELECT MAX(NTYPEVISITLISTSCORE)
                            FROM TTYPEOFVISITFORMLIST M
                            WHERE M.NTYPEVISITFORMID = TSCORE.NTYPEVISITFORMID
                            AND CACTIVE              ='1'
                            ),2)) as SUM_SCORE,
                            SVENDORID,
                            SVENDORNAME,
                            STYPEVISITFORMNAME,
                            NTYPEVISITFORMID,
                            NGROUPID,SGROUPNAME
                            NYEAR,
                            DCHECK,NQUESTIONNAIREID
                        FROM
                        (SELECT SUM( RESULT.QITEM_SCORE) AS SUM_SCORE ,
                          RESULT.SVENDORNAME, RESULT.SVENDORID,
                          RESULT.STYPEVISITFORMNAME, RESULT.NTYPEVISITFORMID,
                          RESULT.NGROUPID, RESULT.SGROUPNAME,
                          RESULT.NYEAR, TO_CHAR(RESULT.DCHECK,'DD/MM/YYYY') AS DCHECK,
                          RESULT.NQUESTIONNAIREID
                        FROM
                          (SELECT QT.*,
                            V.SVENDORNAME, F.STYPEVISITFORMNAME,
                            F.NWEIGHT, rdo.NTYPEVISITLISTSCORE*F.NWEIGHT AS QITEM_SCORE ,
                            Q.SVENDORID, Q.NYEAR,
                            Q.DCHECK, F.SGROUPNAME
                          FROM TQUESTIONNAIRELIST QT
                          INNER JOIN
                            (SELECT TF.*, TTF.STYPEVISITFORMNAME, TG.SGROUPNAME
                            FROM TTYPEOFVISITFORM_YEARS TY
                            INNER JOIN TVISITFORM TF
                            ON TF.NTYPEVISITFORMID = TY.NTYPEVISITFORMID
                            AND TY.IS_ACTIVE       ='1'
                            AND TY.YEAR            =:NYEAR
                            INNER JOIN TTYPEOFVISITFORM TTF
                            ON TTF.NTYPEVISITFORMID = TF.NTYPEVISITFORMID
                            INNER JOIN TGROUPOFVISITFORM TG
                            ON TF.NGROUPID            = TG.NGROUPID
                            AND TG.CACTIVE            ='1'
                            ) F ON F.NTYPEVISITFORMID = QT.NTYPEVISITFORMID
                          AND F.NVISITFORMID          = QT.NVISITFORMID
                          AND F.CACTIVE               ='1'
                          INNER JOIN TTYPEOFVISITFORMLIST rdo
                          ON rdo.NTYPEVISITFORMID = QT.NTYPEVISITFORMID
                          AND rdo.CONFIG_VALUE    = QT.NVALUEITEM AND  rdo.CACTIVE               ='1'
                          INNER JOIN TQUESTIONNAIRE Q
                          ON Q.NQUESTIONNAIREID   = QT.NQUESTIONNAIREID
                          AND QT.NTYPEVISITFORMID = Q.NTYPEVISITFORMID
                          AND Q.NYEAR             =:NYEAR {0}
                          INNER JOIN TVENDOR_SAP V
                          ON V.SVENDORID = Q.SVENDORID
                          ) RESULT
                        GROUP BY RESULT.SVENDORNAME,
                          RESULT.STYPEVISITFORMNAME,
                          RESULT.NTYPEVISITFORMID,
                          RESULT.NYEAR,
                          TO_CHAR(RESULT.DCHECK,'DD/MM/YYYY'),
                          RESULT.NQUESTIONNAIREID,
                          RESULT.SGROUPNAME,
                          RESULT.NGROUPID,
                          RESULT.SVENDORID) TSCORE", _condition);
                        #endregion " sql "
                        DataTable DtAllScoreByVendor = new cmdCommonDAL().ExecuteAdaptor(sql, paramList, out _err);
                        if (DtAllScoreByVendor.Rows.Count > 0)// มีการกรอกคะแนนแล้ว
                        {
                            string VENDORID = string.Empty;
                            int rownum = 1;
                            foreach (DataRow q in DtVendorList.Rows)// ex 15
                            {
                                VENDORID = q["SVENDORID"].ToString();
                                DataRow resultRow = result.NewRow();
                                double Total = 0.00;
                                resultRow["ROWNUM"] = rownum.ToString();
                                resultRow["NQUESTIONNAIREID"] = decimal.Parse(q["NQUESTIONNAIREID"].ToString());
                                resultRow["NTYPEVISITFORMID"] = string.IsNullOrEmpty(q["NTYPEVISITFORMID"].ToString()) ? 0 : decimal.Parse(q["NTYPEVISITFORMID"].ToString());
                                resultRow["SVENDORNAME"] = q["SVENDORNAME"];
                                resultRow["SDCHECK"] = q["SDCHECK"].ToString();
                                foreach (var toppic in lstToppic)
                                {
                                    string _keyNGROUPID = toppic.GroupId;
                                    string expression = string.Format(" NGROUPID={0} and SVENDORID={1}", _keyNGROUPID, VENDORID);
                                    DataRow[] foundRows = null;
                                    foundRows = DtAllScoreByVendor.Select(expression);
                                    if (foundRows.Length > 0)
                                    {
                                        resultRow[toppic.GroupName] = (decimal)foundRows[0]["SUM_SCORE"];
                                        Total += double.Parse(foundRows[0]["SUM_SCORE"].ToString());
                                    }
                                    else
                                    {
                                        resultRow[toppic.GroupName] = 0;
                                    }
                                }
                                resultRow["TOTAL"] = Total;
                                resultRow["PERCENT"] = q["PERCENT"].ToString();
                                resultRow["IS_VENDOR_VIEW"] = q["IS_VENDOR_VIEW"].ToString();
                                result.Rows.Add(resultRow);
                                rownum++;
                            }
                        }
                        else
                        {
                            string VENDORID = string.Empty;
                            int rownum = 1;
                            foreach (DataRow q in DtVendorList.Rows)
                            {
                                VENDORID = q["SVENDORID"].ToString();
                                DataRow resultRowNull = result.NewRow();
                                double Total = 0.00;
                                resultRowNull["ROWNUM"] = rownum.ToString();
                                resultRowNull["NQUESTIONNAIREID"] = decimal.Parse(q["NQUESTIONNAIREID"].ToString());
                                resultRowNull["NTYPEVISITFORMID"] = string.IsNullOrEmpty(q["NTYPEVISITFORMID"].ToString()) ? 0 : decimal.Parse(q["NTYPEVISITFORMID"].ToString());
                                resultRowNull["SVENDORNAME"] = q["SVENDORNAME"];
                                resultRowNull["SDCHECK"] = q["SDCHECK"].ToString();
                                resultRowNull["TOTAL"] = Total;
                                resultRowNull["PERCENT"] = q["PERCENT"].ToString();
                                resultRowNull["IS_VENDOR_VIEW"] = q["IS_VENDOR_VIEW"].ToString();
                                foreach (var toppic in lstToppic)
                                {
                                    resultRowNull[toppic.GroupName] = 0;
                                }
                                rownum++;

                                result.Rows.Add(resultRowNull);
                            }

                        }

                    }
                    else
                    {

                        return result;

                    }
                }
                else return result;
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        public DataTable GetListScoreByVendor(ref string _err, TQUESTIONNAIRE search)
        {
            DataTable result = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string _condition = string.Empty;
                if (!string.IsNullOrEmpty(search.SVENDORID))
                {
                    _condition = string.Format(" and Q.SVENDORID='{0}'", search.SVENDORID);
                }

                #region " sql "
                _sb.Append(System.String.Format(@"SELECT 0 AS SUM_SCORE,
                  V.SVENDORNAME,
                  '-' AS STYPEVISITFORMNAME ,
                  0 AS NTYPEVISITFORMID,
                  Q.NYEAR,
                  TO_CHAR(Q.DCHECK,'DD/MM/YYYY') AS DCHECK,
                  Q.NQUESTIONNAIREID
                FROM TQUESTIONNAIRE Q
                INNER JOIN TVENDOR_SAP V
                ON V.SVENDORID            = Q.SVENDORID
                WHERE Q.NTYPEVISITFORMID IS NULL
                AND Q.NYEAR               =:NYEAR
                UNION ALL
                SELECT SUM( RESULT.QITEM_SCORE) AS SUM_SCORE ,
                  RESULT.SVENDORNAME,
                  RESULT.STYPEVISITFORMNAME,
                  RESULT.NTYPEVISITFORMID,
                  RESULT.NYEAR,
                  TO_CHAR(RESULT.DCHECK,'DD/MM/YYYY') AS DCHECK,
                  RESULT.NQUESTIONNAIREID
                FROM
                  (SELECT QT.*,
                    V.SVENDORNAME,
                    F.STYPEVISITFORMNAME,
                    F.NWEIGHT,
                    rdo.NTYPEVISITLISTSCORE*F.NWEIGHT AS QITEM_SCORE ,
                    Q.SVENDORID,
                    Q.NYEAR,
                    Q.DCHECK
                  FROM TQUESTIONNAIRELIST QT
                  INNER JOIN
                    (SELECT TF.*,
                      TTF.STYPEVISITFORMNAME
                    FROM TTYPEOFVISITFORM_YEARS TY
                    INNER JOIN TVISITFORM TF
                    ON TF.NTYPEVISITFORMID = TY.NTYPEVISITFORMID
                    AND TY.IS_ACTIVE       ='1'
                    AND TY.YEAR            =:NYEAR
                    INNER JOIN TTYPEOFVISITFORM TTF
                    ON TTF.NTYPEVISITFORMID   = TF.NTYPEVISITFORMID
                    ) F ON F.NTYPEVISITFORMID = QT.NTYPEVISITFORMID
                  AND F.NVISITFORMID          = QT.NVISITFORMID
                  /*AND F.CACTIVE               ='1'*/
                  INNER JOIN TTYPEOFVISITFORMLIST rdo
                  ON rdo.NTYPEVISITFORMID = QT.NTYPEVISITFORMID
                  AND rdo.CONFIG_VALUE    = QT.NVALUEITEM AND  rdo.CACTIVE               ='1'
                  INNER JOIN TQUESTIONNAIRE Q
                  ON Q.NQUESTIONNAIREID   = QT.NQUESTIONNAIREID
                  AND QT.NTYPEVISITFORMID = Q.NTYPEVISITFORMID
                  AND Q.NYEAR             =:NYEAR
                  INNER JOIN TVENDOR_SAP V
                  ON V.SVENDORID = Q.SVENDORID {0}
                  ) RESULT
                GROUP BY RESULT.SVENDORNAME,
                  RESULT.STYPEVISITFORMNAME,
                  RESULT.NTYPEVISITFORMID,
                  RESULT.NYEAR,
                  TO_CHAR(RESULT.DCHECK,'DD/MM/YYYY'),
                  RESULT.NQUESTIONNAIREID
                ", _condition));
                #endregion " sql "
                paramList.Add(new ParameterEntity(":NYEAR", search.NYEAR.ToString()));
                result = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        #endregion " ข้อมูลผลประเมินการบริหารงาน questionnaire.aspx "
        public USER_PERMISSION CheckPermission(ref string _err, string _userId, string _page_name)
        {
            USER_PERMISSION result = USER_PERMISSION.DISABLE;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"SELECT DISTINCT M.SMENULINK ,
                  P.SMENUID, MAX(P.CPERMISSION) as MCPERMISSION,
                  P.SUID
                FROM TPERMISSION P
                INNER JOIN TMENU M
                ON M.SMENUID           =P.SMENUID
                WHERE M.SMENULINK     IS NOT NULL
                AND P.SUID             =:SUID
                AND UPPER(M.SMENULINK) = UPPER(:SMENULINK)
                group by  M.SMENULINK ,  P.SMENUID, P.SUID
                ORDER BY M.SMENULINK DESC"));
                paramList.Add(new ParameterEntity(":SUID", _userId.ToString()));
                paramList.Add(new ParameterEntity(":SMENULINK", _page_name.ToString()));
                DataTable dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                if (dt.Rows.Count > 0) result = (USER_PERMISSION)Enum.Parse(typeof(USER_PERMISSION), dt.Rows[0]["MCPERMISSION"].ToString());
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// รายชื่อผู้รับการตรวจ 
        /// </summary>
        /// <param name="_err"></param>
        /// <param name="NQUESTIONNAIREID"></param>
        /// <returns></returns>
        public List<TQUESTIONNAIRE_AS> GetQAS(ref string _err, int? NQUESTIONNAIREID)
        {
            var result = new List<TQUESTIONNAIRE_AS>();
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                if (NQUESTIONNAIREID > 0)
                {
                    _sb.Append(System.String.Format(@"SELECT cast(NQUESTIONNAIREID as INT) as NQUESTIONNAIREID ,cast(NID as INT) as NID,SNAME,to_char(DCREATE,'DD/MM/YYYY') as DCREATE,  SCREATE , cast(NINDEX as INT) as NINDEX from TQUESTIONNAIRE_AS where NQUESTIONNAIREID=:NQUESTIONNAIREID order by  cast(NINDEX as INT) asc"));
                    paramList.Add(new ParameterEntity(":NQUESTIONNAIREID", NQUESTIONNAIREID.ToString()));
                    DataTable dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                    Utility.DataModelHelper.CreateListN<TQUESTIONNAIRE_AS>(ref result, dt);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public List<TQUESTIONNAIRE_TEAM> GetQTEAM(ref string _err, int? NQUESTIONNAIREID)
        {
            var result = new List<TQUESTIONNAIRE_TEAM>();
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                if (NQUESTIONNAIREID > 0)
                {
                    _sb.Append(System.String.Format(@"SELECT CAST(NQUESTIONNAIREID AS INT) AS NQUESTIONNAIREID ,
  QTEAM_ID,
  SNAME,
  TO_CHAR(DCREATE,'DD/MM/YYYY') AS DCREATE,
  SCREATE,
  TEAM_INDEX,c.CONFIG_NAME as TYPE_NAME ,TEAM_TYPE
FROM TQUESTIONNAIRE_TEAM T inner join c_config  c on c.CONFIG_VALUE = T.TEAM_TYPE  and c.CONFIG_TYPE='AUDIT_TEAM_TYPE'
where NQUESTIONNAIREID=:NQUESTIONNAIREID order by cast(TEAM_INDEX as INT) asc "));
                    paramList.Add(new ParameterEntity(":NQUESTIONNAIREID", NQUESTIONNAIREID.ToString()));
                    DataTable dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                    Utility.DataModelHelper.CreateListN<TQUESTIONNAIRE_TEAM>(ref result, dt);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// รายชื่อคณะผู้ตรวจ
        /// </summary>
        /// <param name="_err"></param>
        /// <param name="NQUESTIONNAIREID"></param>
        /// <returns></returns>
        public List<TQUESTIONNAIRE_BY> GetQBY(ref string _err, int? NQUESTIONNAIREID)
        {
            var result = new List<TQUESTIONNAIRE_BY>();
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                if (NQUESTIONNAIREID > 0)
                {
                    _sb.Append(System.String.Format(@"SELECT cast(NQUESTIONNAIREID as INT) as NQUESTIONNAIREID ,cast(NID as INT) as NID ,SNAME,to_char(DCREATE,'DD/MM/YYYY') as DCREATE,  SCREATE, cast(NINDEX as INT) as NINDEX from TQUESTIONNAIRE_BY where NQUESTIONNAIREID=:NQUESTIONNAIREID order by cast(NINDEX as INT) asc "));
                    paramList.Add(new ParameterEntity(":NQUESTIONNAIREID", NQUESTIONNAIREID.ToString()));
                    DataTable dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                    Utility.DataModelHelper.CreateListN<TQUESTIONNAIRE_BY>(ref result, dt);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        public List<RadioValueList> GetRadio(ref string _err, int? NTYPEVISITFORMID, bool OnlyActive = false)
        {
            var result = new List<RadioValueList>();
            DataTable dt = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string _condition = string.Empty;
                if (OnlyActive)
                {
                    _condition = " and TVL.CACTIVE='1'";
                }
                if (NTYPEVISITFORMID > 0)
                {
                    _sb.Append(System.String.Format(@"select TVL.NTYPEVISITLISTSCORE ,
                      TVL.STYPEVISITLISTNAME,
                      TVL.CONFIG_VALUE from TTYPEOFVISITFORMLIST TVL
                    where NTYPEVISITFORMID =:NTYPEVISITFORMID AND TVL.CACTIVE           ='1' {0}
                    and TVL.CONFIG_VALUE IN (SELECT  CONFIG_VALUE from C_CONFIG where CONFIG_TYPE ='AUDIT_LEVEL'  AND CACTIVE ='1' )
                    ORDER BY TVL.CONFIG_VALUE", _condition));
                    paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", NTYPEVISITFORMID.ToString()));
                    dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                    Utility.DataModelHelper.CreateList<RadioValueList>(ref result, dt);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
        public DataTable GetTQUESTIONNAIRELIST(ref string _err, int? NQUESTIONNAIREID)
        {
            DataTable result = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"SELECT NQUESTIONNAIREID,NVISITFORMID,NGROUPID,NTYPEVISITFORMID,
NVALUE,SREMARK,NVALUEITEM FROM TQUESTIONNAIRELIST WHERE NQUESTIONNAIREID = '{0}'", NQUESTIONNAIREID));
                result = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        public List<FORMQUESTIONNAIRE> GetQuestionnaireForm(ref string _err, int? NTYPEVISITFORMID, int? NGROUPID, string SVENDORID, int? NQUESTIONNAIREID)
        {
            List<FORMQUESTIONNAIRE> result = new List<FORMQUESTIONNAIRE>();
            DataTable dt = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"select distinct A.* from (select G.SGROUPNAME,FF.* from (SELECT f.SVISITFORMNAME,f.NVISITFORMID,
  f.NGROUPID,
  f.NWEIGHT,T.NTYPEVISITFORMID
FROM TVISITFORM f
INNER JOIN  TTYPEOFVISITFORM T
ON T.NTYPEVISITFORMID = f.NTYPEVISITFORMID
where f.NTYPEVISITFORMID =:NTYPEVISITFORMID
and f.NTYPEVISITFORMID=:NTYPEVISITFORMID and f.NGROUPID =:NGROUPID ) ff
INNER JOIN TGROUPOFVISITFORM G on G.NGROUPID = ff.NGROUPID AND G.NTYPEVISITFORMID =ff.NTYPEVISITFORMID
) A inner JOIN ( SELECT qfrm.SVENDORID,
  qfrm.NQUESTIONNAIREID,
  qfrm.DCHECK,
  qfrm.NNO,
  qfrm.SREMARK,
  qfrm.SADDRESS,
  qfrm.NVALUE as SUMALL,
  qlst.NVISITFORMID,
  qlst.NGROUPID,
  qlst.NVALUE  AS NVALUE,
  qlst.SREMARK AS SREMARK_ITEM,
  qlst.NVALUEITEM,
  qlst.NTYPEVISITFORMID
FROM TQUESTIONNAIRE qfrm
INNER JOIN TQUESTIONNAIRELIST qlst
ON qfrm.NQUESTIONNAIREID = qlst.NQUESTIONNAIREID 
where SVENDORID=:SVENDORID and qfrm.NQUESTIONNAIREID=:NQUESTIONNAIREID and qlst.NGROUPID =:NGROUPID 
) Q ON Q.NTYPEVISITFORMID = A.NTYPEVISITFORMID and A.NGROUPID = Q.NGROUPID and Q.NTYPEVISITFORMID = A.NTYPEVISITFORMID"));

                paramList.Add(new ParameterEntity(":NQUESTIONNAIREID", NQUESTIONNAIREID.ToString()));
                paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", NTYPEVISITFORMID.ToString()));
                paramList.Add(new ParameterEntity(":NGROUPID", NGROUPID.ToString()));
                paramList.Add(new ParameterEntity(":SVENDORID", SVENDORID.ToString()));
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        public DataTable GetFormName(ref string _err)
        {
            DataTable dt = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"SELECT * from TTYPEOFVISITFORM f where f.cactive = 1"));
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {
                throw;
            }

            return dt;
        }
        public List<F_UPLOAD> GetUploadFile(ref string _err, int? NQUESTIONNAIREID)
        {
            List<F_UPLOAD> result = new List<F_UPLOAD>();
            try
            {
                if (NQUESTIONNAIREID > 0)
                {
                    _err = string.Empty;
                    var _sb = new System.Text.StringBuilder();
                    List<ParameterEntity> paramList = new List<ParameterEntity>();
                    _sb.Append(System.String.Format(@"select f.ID, f.FILENAME_SYSTEM,f.FILENAME_USER,f.FULLPATH ,f.REF_INT,f.REF_STR,f.UPLOAD_ID,UPLOAD_NAME from f_upload  f INNER JOIN M_UPLOAD_TYPE ON f.UPLOAD_ID = M_UPLOAD_TYPE.UPLOAD_ID where f.ref_int =:NQUESTIONNAIREID and f.ISACTIVE = 1 and REF_STR='AUDIT_FILE'"));
                    paramList.Add(new ParameterEntity(":NQUESTIONNAIREID", NQUESTIONNAIREID.ToString()));
                    DataTable dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                    Utility.DataModelHelper.CreateListN<F_UPLOAD>(ref result, dt);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
        public Questionnaire GetQuestionnaire(ref string _err, int? NQUESTIONNAIREID)
        {
            Questionnaire result = new Questionnaire();

            try
            {
                if (NQUESTIONNAIREID > 0)
                {
                    _err = string.Empty;
                    TQUESTIONNAIRE QUESTIONNAIRE = new Master.TQUESTIONNAIRE();
                    var _sb = new System.Text.StringBuilder();
                    List<ParameterEntity> paramList = new List<ParameterEntity>();
                    _sb.Append(System.String.Format(@"SELECT NQUESTIONNAIREID,SVENDORID, 
                   DCHECK,SADDRESS,SREMARK,to_char(DCREATE, 'DD/MM/YYYY') as SDCREATE ,SCREATE ,to_char(DUPDATE, 'DD/MM/YYYY') as SDUPDATE, to_char(DCHECK, 'DD/MM/YYYY') as SDCHECK,
                   SUPDATE,NVALUE,NNO,NTYPEVISITFORMID ,NYEAR,decode(IS_VENDOR_VIEW,'',0,IS_VENDOR_VIEW) as IS_VENDOR_VIEW from TQUESTIONNAIRE Q where Q.NQUESTIONNAIREID ='{0}'", NQUESTIONNAIREID.ToString()));
                    DataTable dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                    result.LST_TQUESTIONNAIRE_BY = GetQBY(ref _err, NQUESTIONNAIREID);
                    result.LST_TQUESTIONNAIRE_AS = GetQAS(ref _err, NQUESTIONNAIREID);
                    result.LST_FILE_UPLOAD = GetUploadFile(ref _err, NQUESTIONNAIREID);
                    result.DT_QUESTIONNAIRELIST = GetTQUESTIONNAIRELIST(ref _err, NQUESTIONNAIREID);
                    result.LST_TEAM = GetQTEAM(ref _err, NQUESTIONNAIREID);
                    result.SDCHECK = dt.Rows[0]["SDCHECK"].ToString();
                    result.SDCREATE = dt.Rows[0]["SDCREATE"].ToString();
                    result.SDUPDATE = dt.Rows[0]["SDUPDATE"].ToString();
                    result.NNO = int.Parse(dt.Rows[0]["NNO"].ToString());
                    result.NQUESTIONNAIREID = string.IsNullOrEmpty(dt.Rows[0]["NQUESTIONNAIREID"].ToString()) ? 0 : int.Parse(dt.Rows[0]["NQUESTIONNAIREID"].ToString());
                    result.NTYPEVISITFORMID = string.IsNullOrEmpty(dt.Rows[0]["NTYPEVISITFORMID"].ToString()) ? 0 : int.Parse(dt.Rows[0]["NTYPEVISITFORMID"].ToString());
                    result.NVALUE = ParseDecimal(dt.Rows[0]["NVALUE"].ToString());
                    result.SADDRESS = dt.Rows[0]["SADDRESS"].ToString();
                    result.SCREATE = dt.Rows[0]["SCREATE"].ToString();
                    result.SREMARK = dt.Rows[0]["SREMARK"].ToString();
                    result.SUPDATE = dt.Rows[0]["SUPDATE"].ToString();
                    result.SVENDORID = dt.Rows[0]["SVENDORID"].ToString();
                    result.YEAR = dt.Rows[0]["NYEAR"].ToString();
                    result.IS_VENDOR_VIEW = dt.Rows[0]["IS_VENDOR_VIEW"].ToString();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
        public List<VendorQuestionnaireBYYear> GetVendorQuestionnaireBYYear(ref string _err, string YEAR, string SVENDORID)
        {
            List<VendorQuestionnaireBYYear> result = new List<VendorQuestionnaireBYYear>();
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@" SELECT q.NQUESTIONNAIREID,
                  q.SVENDORID, q.DCHECK,
                  q.SADDRESS, q.SREMARK,
                  q.NVALUE, VS.SVENDORNAME,q.NNO
                FROM TQUESTIONNAIRE q
                LEFT JOIN TVENDOR_SAP vs
                ON Q.SVENDORID               = VS.SVENDORID
                WHERE q.SVENDORID            = :SVENDORID
                AND TO_CHAR(q.DCHECK,'YYYY') = :DCHECK"));
                paramList.Add(new ParameterEntity(":SVENDORID", SVENDORID.ToString()));
                paramList.Add(new ParameterEntity(":DCHECK", YEAR.ToString()));
                DataTable dt = null;
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                Utility.DataModelHelper.CreateList<VendorQuestionnaireBYYear>(ref result, dt);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        public List<HeaderQuestionnaire> GetHeaderQ(ref string _err, int? NTYPEVISITFORMID)
        {

            List<HeaderQuestionnaire> result = new List<HeaderQuestionnaire>();
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"SELECT GV.NGROUPID ,
                  GV.NNO || '. ' || GV.SGROUPNAME AS SGROUPNAME
                FROM TTYPEOFVISITFORM tv
                INNER JOIN TGROUPOFVISITFORM gv
                ON TV.NTYPEVISITFORMID    = GV.NTYPEVISITFORMID
                WHERE TV.NTYPEVISITFORMID = :NTYPEVISITFORMID
                ORDER BY GV.NNO"));
                paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", NTYPEVISITFORMID.ToString()));
                DataTable dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                Utility.DataModelHelper.CreateList<HeaderQuestionnaire>(ref result, dt);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        public DataTable GetDetailFormQ(ref string _err, int NGROUPID)
        {
            DataTable dt = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"SELECT NVISITFORMID,
                  DECODE(FORM_CODE,'','-',FORM_CODE) AS FORM_CODE,
                  SVISITFORMNAME                     AS SVISITFORMNAME,
                  NVL(NWEIGHT,0)                     AS NWEIGHT,
                  NGROUPID,
                  NTYPEVISITFORMID
                FROM
                  (SELECT NVISITFORMID,
                    FORM_CODE,
                    SVISITFORMNAME AS SVISITFORMNAME,
                    NVL(NWEIGHT,0) AS NWEIGHT,
                    NGROUPID,
                    NTYPEVISITFORMID
                  FROM TVISITFORM
                  WHERE (NGROUPID = :oNGROUPID)
                  AND (CACTIVE    = '1')
                  ORDER BY TO_NUMBER(DECODE(FORM_CODE,'',0,FORM_CODE), '99D99'),
                    NVISITFORMID ASC
                  ) A"));
                paramList.Add(new ParameterEntity(":oNGROUPID", NGROUPID.ToString()));
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {
                throw;
            }
            return dt;
        }

        public string GetFormNameNOTAcitveForView(ref string _err, string NTYPEVISITFORMID)
        {
            string result = string.Empty;
            try
            {
                DataTable dt = null;
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"SELECT f.STYPEVISITFORMNAME AS FORMNAME FROM TTYPEOFVISITFORM f
                                                where f.NTYPEVISITFORMID =:NTYPEVISITFORMID"));
                paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", NTYPEVISITFORMID));
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                if (dt.Rows.Count > 0)
                {
                    result = dt.Rows[0][0].ToString();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public TVENDOR_SAP GetVendorByID(ref string _err, string vId)
        {
            var result = new TVENDOR_SAP();
            var lst = new List<TVENDOR_SAP>();
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"select ven.SVENDORID, ven.SABBREVIATION as SVENDORNAME FROM TVendor ven
                LEFT JOIN TVENDOR_SAP vensap
                ON ven.SVENDORID          = vensap.SVENDORID
                WHERE ven.SVENDORID = :SVENDORID
                AND ven.INUSE             = '1'
                AND NVL(ven.CACTIVE,'Y') != '0'  "));
                paramList.Add(new ParameterEntity(":SVENDORID", vId));
                DataTable dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                if (dt != null)
                {
                    Utility.DataModelHelper.CreateList<TVENDOR_SAP>(ref lst, dt, true);
                    result = lst[0];
                }

            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        public DataTable GetVendorNotEntryQuestion(ref string _err, string year = "")
        {
            DataTable dt = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string _condition = string.Empty;

                if (!string.IsNullOrEmpty(year))
                {
                    _condition = string.Format(" and y.year = '{0}'", year);
                }

                _sb.Append(System.String.Format(@"SELECT ven.DUPDATE,
                  ven.SVENDORID,
                  ven.SABBREVIATION,
                  vensap.SNO
                  ||' '
                  || vensap.SDISTRICT
                  ||' '
                  || vensap.SREGION
                  ||' '
                  || vensap.SPROVINCE
                  ||' '
                  || vensap.SPROVINCECODE AS Address ,
                  ven.STEL ,
                  CASE ven.CACTIVE
                    WHEN '0'
                    THEN 'ไม่อนุญาตให้ใช้งาน'
                    ELSE ' '
                  END AS CACTIVE ,
                  ven.CHECKIN ,
                  ven.NOTFILL,
                  ven.INUSE
                FROM TVendor ven
                LEFT JOIN TVENDOR_SAP vensap
                ON ven.SVENDORID          = vensap.SVENDORID
                WHERE 1                   =1
                AND ven.INUSE             = '1'
                AND NVL(ven.CACTIVE,'Y') != '0'
                AND ven.SVENDORID NOT    IN
                  (SELECT SVENDORID
                  FROM TQUESTIONNAIRE Q
                  WHERE Q.NYEAR           =:NYEAR
                  AND Q.NTYPEVISITFORMID IN
                    (SELECT NTYPEVISITFORMID FROM TTYPEOFVISITFORM_YEARS Y WHERE Y.IS_ACTIVE='1' and Y.YEAR =:NYEAR
                    )
                  )  AND ven.SVENDORID NOT    IN (SELECT SVENDORID
                  FROM TQUESTIONNAIRE Q
                  WHERE Q.NYEAR           =:NYEAR AND Q.NTYPEVISITFORMID is null)
                ORDER BY ven.INUSE DESC NULLS LAST,
                  ven.DUPDATE DESC NULLS LAST"));
                paramList.Add(new ParameterEntity(":NYEAR", year));
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {
                throw;
            }
            return dt;
        }
        public DataTable GetQUESTIONNAIRE_TEAM_TYPE(ref string _err)
        {
            DataTable dt = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"SELECT * FROM C_CONFIG WHERE CONFIG_TYPE='AUDIT_TEAM_TYPE'"));
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {
                throw;
            }
            return dt;
        }
        /// <summary>
        /// สำหรับ Dropdown ในการเลือกแบบสอบถามสำหรับกรอก
        /// </summary>
        /// <param name="_err"></param>
        /// <returns></returns>
        public DataTable GetFormNameForInput(ref string _err, string year = "")
        {
            DataTable dt = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string _condition = string.Empty;

                if (!string.IsNullOrEmpty(year))
                {
                    _condition = string.Format(" and y.year = '{0}'", year);
                }
                _sb.Append(System.String.Format(@"SELECT 'ปี '|| y.YEAR ||' : '|| f.STYPEVISITFORMNAME AS FORMNAME,  y.YEAR ||' : ' || f.NTYPEVISITFORMID as VAL,y.NTYPEVISITFORMID ,f.STYPEVISITFORMNAME FROM TTYPEOFVISITFORM_YEARS y
                  INNER JOIN TTYPEOFVISITFORM f ON f.NTYPEVISITFORMID = y.NTYPEVISITFORMID and f.CACTIVE='1' and y.IS_ACTIVE='1' {0}", _condition));
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {
                throw;
            }
            return dt;
        }
        public DataTable GetConfig(ref string _err, SearchFormConfig _search)
        {
            string _condition = string.Empty;
            DataTable dt = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                if (!string.IsNullOrEmpty(_search.STYPEVISITFORMNAME))
                {
                    _condition += " AND upper(f.STYPEVISITFORMNAME) LIKE '%' ||upper(:FORMNAME) ||'%' ";
                    paramList.Add(new ParameterEntity(":FORMNAME", _search.STYPEVISITFORMNAME.ToString()));
                }

                if (!string.IsNullOrEmpty(_search.ID))
                {
                    _condition += " AND ID=:ID ";
                    paramList.Add(new ParameterEntity(":ID", _search.ID.ToString()));
                }

                if (!string.IsNullOrEmpty(_search.YEAR))
                {
                    _condition += " AND y.YEAR=:YEAR ";
                    paramList.Add(new ParameterEntity(":YEAR", _search.YEAR.ToString()));
                }

                if (!string.IsNullOrEmpty(_search.Y_CACTIVE))
                {
                    _condition += " AND IS_ACTIVE=:IS_ACTIVE ";
                    paramList.Add(new ParameterEntity(":IS_ACTIVE", _search.Y_CACTIVE.ToString()));
                }

                _sb.Append(System.String.Format(@"SELECT y.YEAR,y.PERCENT,
                  y.NTYPEVISITFORMID,
                  DECODE(y.UPDATE_DATE,NULL,'-',TO_CHAR(y.UPDATE_DATE,'DD/MM/YYYY')) AS UPDATE_DATE,
                  ID,
                  TO_CHAR(y.CREATE_DATE,'DD/MM/YYYY')                        AS CREATE_DATE,
                  f.CACTIVE                                                  AS CACTIVE ,
                  DECODE(f.STYPEVISITFORMNAME,NULL,'-',f.STYPEVISITFORMNAME) AS FORMNAME,
                  y.IS_ACTIVE                                                AS Y_CACTIVE
                FROM TTYPEOFVISITFORM_YEARS y
                LEFT JOIN TTYPEOFVISITFORM f
                ON f.NTYPEVISITFORMID = y.NTYPEVISITFORMID
                WHERE 1=1 {0}  ", _condition));
                _sb.Append(" order by y.YEAR desc");
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {

                throw;
            }

            return dt;
        }
        public DataTable GetConfig(ref string _err, int? Id = 0)
        {
            Id = Id.HasValue ? Id : 0;
            DataTable dt = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"SELECT y.YEAR,y.NTYPEVISITFORMID, decode(y.UPDATE_DATE,null,'-',TO_CHAR(y.UPDATE_DATE,'DD/MM/YYYY')) as UPDATE_DATE, ID,
TO_CHAR(y.CREATE_DATE,'DD/MM/YYYY')                                AS CREATE_DATE,
                                                  f.CACTIVE            AS ACTIVE ,
                                                  decode(f.STYPEVISITFORMNAME,null,'-',f.STYPEVISITFORMNAME) AS FORMNAME
                                                FROM TTYPEOFVISITFORM_YEARS y
                                                left JOIN TTYPEOFVISITFORM f
                                                ON f.NTYPEVISITFORMID = y.NTYPEVISITFORMID "));
                if (Id > 0)
                {
                    _sb.Append(System.String.Format(@" where y.ID =:ID"));
                    paramList.Add(new ParameterEntity(":ID", Id.ToString()));
                }
                _sb.Append(" order by y.YEAR desc");
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {

                throw;
            }

            return dt;
        }

        public bool IsDuplicateQuestion(ref string _err, string NYEAR, string SVENDORID, int? QDocId = 0)
        {
            bool result = false;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@" SELECT count(*)
FROM TQUESTIONNAIRE where NYEAR=:NYEAR AND  SVENDORID=:SVENDORID and NQUESTIONNAIREID<>{0}", QDocId));
                paramList.Add(new ParameterEntity(":NYEAR", NYEAR.ToString()));
                paramList.Add(new ParameterEntity(":SVENDORID", SVENDORID.ToString()));
                int? count = new cmdCommonDAL().GetCount(_sb.ToString(), paramList, out _err);
                if (count > 0)
                {
                    return true;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
        /// <summary>
        /// เช็คข้อมูลว่ามีการกรอกแบบสอบถามไปหรือยัง
        /// </summary>
        /// <param name="_err"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool IsInUse(ref String _err, TTYPEOFVISITFORM_YEARS item)
        {
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                if ((item.ID.HasValue) && (item.YEAR.HasValue))
                {
                    _sb.Append(System.String.Format(@" Select count(*) C from TQUESTIONNAIRELIST where year=:YEAR and NTYPEVISITFORMID=:NTYPEVISITFORMID"));
                    paramList.Add(new ParameterEntity(":YEAR", item.YEAR.ToString()));
                    paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", item.NTYPEVISITFORMID.ToString()));
                    int? count = new cmdCommonDAL().GetCount(_sb.ToString(), paramList, out _err);
                    if (count > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteConfig(ref String _err, UserProfile user, int? Id)
        {
            try
            {
                if (Id.HasValue)
                {
                    var _sb = new System.Text.StringBuilder();
                    List<ParameterEntity> paramList = new List<ParameterEntity>();
                    _sb.Append(System.String.Format(@" delete from TTYPEOFVISITFORM_YEARS where ID =:ID"));
                    paramList.Add(new ParameterEntity(":ID", Id.ToString()));
                    bool result = new cmdCommonDAL().ExecuteNonQuery(_sb.ToString(), paramList, out _err);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public double GetTotalScore(ref String _err, TQUESTIONNAIRE search)
        {
            double result = 0.00;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string _condition = string.Empty;
                if (!string.IsNullOrEmpty(search.SVENDORID))
                {
                    _condition = string.Format(" and Q.SVENDORID='{0}'", search.SVENDORID);
                }
                if (search.NTYPEVISITFORMID.HasValue)
                {
                    _condition += string.Format(" and Q.NTYPEVISITFORMID='{0}'", search.NTYPEVISITFORMID);
                }

                if (!string.IsNullOrEmpty(search.NGROUP))
                {
                    _condition += string.Format(" and F.NGROUPID<>'{0}'", search.NGROUP);
                }
                #region " sql "
                _sb.Append(System.String.Format(@" SELECT DECODE (SUM_SCORE,0,0, ROUND(SUM_SCORE/
                        (SELECT MAX(NTYPEVISITLISTSCORE)
                        FROM TTYPEOFVISITFORMLIST M
                        WHERE M.NTYPEVISITFORMID = TSCORE.NTYPEVISITFORMID
                        AND CACTIVE              ='1'
                        ),2)) as SUM_SCORE, 
                        SVENDORNAME,
                        STYPEVISITFORMNAME,
                        NTYPEVISITFORMID,
                        NYEAR,
                        DCHECK,NQUESTIONNAIREID
                    FROM ( SELECT SUM( RESULT.QITEM_SCORE) AS SUM_SCORE ,
                  RESULT.SVENDORNAME,
                  RESULT.STYPEVISITFORMNAME,
                  RESULT.NTYPEVISITFORMID,
                  RESULT.NYEAR,
                  TO_CHAR(RESULT.DCHECK,'DD/MM/YYYY') AS DCHECK,
                  RESULT.NQUESTIONNAIREID
                FROM
                  (SELECT QT.*,
                    V.SVENDORNAME,
                    F.STYPEVISITFORMNAME,
                    F.NWEIGHT,
                    rdo.NTYPEVISITLISTSCORE*F.NWEIGHT AS QITEM_SCORE ,
                    Q.SVENDORID,
                    Q.NYEAR,
                    Q.DCHECK
                  FROM TQUESTIONNAIRELIST QT
                  INNER JOIN
                    (SELECT TF.*,
                      TTF.STYPEVISITFORMNAME
                    FROM TTYPEOFVISITFORM_YEARS TY
                    INNER JOIN TVISITFORM TF
                    ON TF.NTYPEVISITFORMID = TY.NTYPEVISITFORMID
                    AND TY.IS_ACTIVE       ='1'
                    AND TY.YEAR            =:NYEAR
                    INNER JOIN TTYPEOFVISITFORM TTF
                    ON TTF.NTYPEVISITFORMID   = TF.NTYPEVISITFORMID
                    ) F ON F.NTYPEVISITFORMID = QT.NTYPEVISITFORMID
                  AND F.NVISITFORMID          = QT.NVISITFORMID
                  /*AND F.CACTIVE               ='1'*/
                  INNER JOIN TTYPEOFVISITFORMLIST rdo
                  ON rdo.NTYPEVISITFORMID = QT.NTYPEVISITFORMID
                  AND rdo.CONFIG_VALUE    = QT.NVALUEITEM
                  AND  rdo.CACTIVE               ='1'
                  INNER JOIN TQUESTIONNAIRE Q
                  ON Q.NQUESTIONNAIREID   = QT.NQUESTIONNAIREID
                  AND QT.NTYPEVISITFORMID = Q.NTYPEVISITFORMID
                  AND Q.NYEAR             =:NYEAR
                  INNER JOIN TVENDOR_SAP V
                  ON V.SVENDORID = Q.SVENDORID {0}
                  ) RESULT
                GROUP BY RESULT.SVENDORNAME,
                  RESULT.STYPEVISITFORMNAME,
                  RESULT.NTYPEVISITFORMID,
                  RESULT.NYEAR,
                  TO_CHAR(RESULT.DCHECK,'DD/MM/YYYY'),
                  RESULT.NQUESTIONNAIREID ) TSCORE ", _condition));
                #endregion " sql "
                paramList.Add(new ParameterEntity(":NYEAR", search.NYEAR.ToString()));
                DataTable dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0) result = double.Parse(dt.Rows[0]["SUM_SCORE"].ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }


        public void AddEditConfig(ref String _err, TTYPEOFVISITFORM_YEARS item, UserProfile user)
        {
            try
            {
                item.ID = item.ID.HasValue ? item.ID : 0;
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();

                paramList.Clear();
                /// Check ว่า Form นี้มีการใช้งานกับปีอื่นอยู่หรือยัง
                paramList.Add(new ParameterEntity(":YEAR", item.YEAR.ToString()));
                paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", item.NTYPEVISITFORMID.ToString()));
                paramList.Add(new ParameterEntity(":ID", item.ID.ToString()));
                DataTable dt = new cmdCommonDAL().ExecuteAdaptor("Select * from TTYPEOFVISITFORM_YEARS where YEAR=:YEAR and NTYPEVISITFORMID=:NTYPEVISITFORMID and ID<>:ID", paramList, out _err);
                if (dt.Rows.Count > 0)
                {
                    _err = String.Format("ไม่สามารถตั้งค่าแบบฟอร์มซ้ำได้ เนื่องจากมีการตั้งค่าแบบฟอร์มนี้สำหรับปี {0} เรียบร้อยแล้ว", item.YEAR.ToString());
                    return;
                }

                if (item.IS_ACTIVE == "1")
                {
                    //Check ว่ามีการ Active Form ของปีนี้ในรายการอื่นๆแล้วหรือยัง
                    paramList.Clear();
                    paramList.Add(new ParameterEntity(":YEAR", item.YEAR.ToString()));
                    paramList.Add(new ParameterEntity(":ID", item.ID.ToString()));
                    dt = new cmdCommonDAL().ExecuteAdaptor("Select * from TTYPEOFVISITFORM_YEARS where YEAR=:YEAR and IS_ACTIVE='1' and ID<>:ID", paramList, out _err);
                    if (dt.Rows.Count > 0)
                    {
                        _err = String.Format("ไม่สามารถใช้งานแบบฟอร์มนี้ได้ เนื่องจากปี {0} มีแบบฟอร์มอื่นใช้งานอยู่ <br/> ต้อง Inactive แบบฟอร์มอื่นก่อน", item.YEAR.ToString());
                        return;
                    }
                }

                if (item.ID > 0)  // Edit
                {
                    _sb.Append(System.String.Format(@"UPDATE TTYPEOFVISITFORM_YEARS SET IS_ACTIVE=:IS_ACTIVE,PERCENT=:PERCENT,
                    YEAR=:YEAR, UPDATE_DATE=sysdate, NTYPEVISITFORMID=:NTYPEVISITFORMID,UPDATE_USR=:UPDATE_USR Where ID =:ID "));
                    paramList.Clear();
                    paramList.Add(new ParameterEntity(":IS_ACTIVE", item.IS_ACTIVE.ToString()));
                    paramList.Add(new ParameterEntity(":PERCENT", item.PERCENT.ToString()));
                    paramList.Add(new ParameterEntity(":YEAR", item.YEAR.ToString()));
                    paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", item.NTYPEVISITFORMID.ToString()));
                    paramList.Add(new ParameterEntity(":UPDATE_USR", user.USER_ID.ToString()));
                    paramList.Add(new ParameterEntity(":ID", item.ID.ToString()));
                }
                else
                {
                    int? refId = GetConfigRefID(ref _err);
                    paramList.Clear();
                    _sb.Append(System.String.Format(@"INSERT INTO TTYPEOFVISITFORM_YEARS (ID,YEAR,NTYPEVISITFORMID,CREATE_DATE,CREATE_USR,UPDATE_DATE,UPDATE_USR,IS_ACTIVE,PERCENT) VALUES (:ID,:YEAR,:NTYPEVISITFORMID,SYSTIMESTAMP,:CREATE_USR,sysdate,:CREATE_USR,:IS_ACTIVE,:PERCENT)"));
                    paramList.Add(new ParameterEntity(":ID", refId.ToString()));
                    paramList.Add(new ParameterEntity(":YEAR", item.YEAR.ToString()));
                    paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", item.NTYPEVISITFORMID.ToString()));
                    paramList.Add(new ParameterEntity(":CREATE_USR", user.USER_ID.ToString()));
                    paramList.Add(new ParameterEntity(":IS_ACTIVE", item.IS_ACTIVE.ToString()));
                    paramList.Add(new ParameterEntity(":PERCENT", item.PERCENT.ToString()));
                }

                bool result = new cmdCommonDAL().ExecuteNonQuery(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// เช็คว่าแบบฟอร์มที่จะแก้ไขมีการใช้งานไปใหรือยัง
        /// </summary>
        /// <returns></returns>
        public bool IsFormInUse(ref string _err, string year, int formID)
        {
            bool result = false;
            try
            {
                DataTable dt = null;
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"select count(*) from TQUESTIONNAIRE Q where Q.NYEAR=:YEAR and NTYPEVISITFORMID=:NTYPEVISITFORMID"));
                paramList.Add(new ParameterEntity(":YEAR", year));
                paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", formID.ToString()));
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                int? count_result = new cmdCommonDAL().GetCount(_sb.ToString(), paramList, out _err);
                if (count_result > 0) result = true;
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        public int? GetConfigRefID(ref String _err)
        {
            int? result = 0;
            try
            {
                DataTable dt = null;
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"select TTYPEOFVISITFORM_YEARS_SEQ.NEXTVAL from dual"));
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                result = int.Parse(dt.Rows[0][0].ToString());
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
        public int? GetItemRefID(ref String _err)
        {
            int? result = 0;
            try
            {
                DataTable dt = null;
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"select TVISITFORM_SEQ.NEXTVAL from dual"));
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                result = int.Parse(dt.Rows[0][0].ToString());
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
        public int? GetGroupRefID(ref String _err)
        {
            int? result = 0;
            try
            {
                DataTable dt = null;
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"select TGROUPOFVISITFORM_SEQ.NEXTVAL from dual"));
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                result = int.Parse(dt.Rows[0][0].ToString());
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public int? GetTYPEOFVISITFORMID(ref String _err)
        {

            int? result = 0;
            try
            {
                DataTable dt = null;
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"select TTYPEOFVISITFORM_SEQ.NEXTVAL from dual"));
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                result = int.Parse(dt.Rows[0][0].ToString());
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public int? GetTeamID(ref String _err)
        {
            int? result = 0;
            try
            {
                DataTable dt = null;
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"select TQUESTIONNAIRE_TEAM_SEQ.nextval from dual"));
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                result = int.Parse(dt.Rows[0][0].ToString());
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
        public int? GetDocID(ref String _err)
        {
            int? result = 0;
            try
            {
                DataTable dt = null;
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"select TQUESTIONNAIRE_SEQ.nextval from dual"));
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                result = int.Parse(dt.Rows[0][0].ToString());
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        #region " New Visit Form "

        public bool EditVisitForm(ref String _err, UserProfile user, TTYPEOFVISITFORM item)
        {
            bool result = true;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"UPDATE TTYPEOFVISITFORM  SET STYPEVISITFORMNAME=:STYPEVISITFORMNAME,SHOWINLIST=:SHOWINLIST ,DUPDATE=SYSTIMESTAMP,SUPDATE=:SUPDATE
                  WHERE NTYPEVISITFORMID=:NTYPEVISITFORMID"));
                paramList.Add(new ParameterEntity(":STYPEVISITFORMNAME", item.STYPEVISITFORMNAME.ToString()));
                paramList.Add(new ParameterEntity(":SHOWINLIST", item.SHOWINLIST.ToString()));
                paramList.Add(new ParameterEntity(":SUPDATE", user.USER_ID.ToString()));
                paramList.Add(new ParameterEntity(":NTYPEVISITFORMID", item.NTYPEVISITFORMID.ToString()));

                bool resultNonQuery = new cmdCommonDAL().ExecuteNonQuery(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {
                result = false;

                throw;
            }
            return result;
        }
        public DataTable GetTTYPEOFVISITFORM(ref string _err, SearhTTYPEOFVISITFORM item)
        {
            DataTable result = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string _condition = string.Empty;
                if (!string.IsNullOrEmpty(item.SDUPDATE_START))
                {
                    _condition += " and t.DUPDATE >= to_date(:SDUPDATE_START || ' 00.00.00','DD/MM/YYYY hh24.mi.ss')";
                    paramList.Add(new ParameterEntity(":SDUPDATE_START", item.SDUPDATE_START.ToString()));
                }

                if (!string.IsNullOrEmpty(item.SDUPDATE_END))
                {
                    _condition += " and t.DUPDATE <= to_date(:SDUPDATE_END || ' 23.59.59','DD/MM/YYYY hh24.mi.ss')";
                    paramList.Add(new ParameterEntity(":SDUPDATE_END", item.SDUPDATE_END.ToString()));
                }

                if (!string.IsNullOrEmpty(item.STYPEVISITFORMNAME))
                {
                    _condition += " and t.STYPEVISITFORMNAME LIKE '%' || :STYPEVISITFORMNAME ||'%'";
                    paramList.Add(new ParameterEntity(":STYPEVISITFORMNAME", item.STYPEVISITFORMNAME.ToString()));
                }

                if (!string.IsNullOrEmpty(item.SHOWINLIST))
                {
                    _condition += " and t.SHOWINLIST =:SHOWINLIST";
                    paramList.Add(new ParameterEntity(":SHOWINLIST", item.SHOWINLIST.ToString()));
                }
                if (!string.IsNullOrEmpty(item.CACTIVE))
                {
                    _condition += " and t.CACTIVE =:CACTIVE";
                    paramList.Add(new ParameterEntity(":CACTIVE", item.CACTIVE.ToString()));
                }


                _sb.Append(System.String.Format(@"select A.NTYPEVISITFORMID,A.STYPEVISITFORMNAME ,A.NG ,A.DCREATE , decode(A.DUPDATE,null,'-',A.DUPDATE) as DUPDATE ,A.CACTIVE,count(NVISITFORMID) as NL 
                from (select t.NTYPEVISITFORMID,t.STYPEVISITFORMNAME,t.CACTIVE,
                to_char(t.DUPDATE,'DD/MM/YYYY') as DUPDATE, to_char(t.DCREATE,'DD/MM/YYYY') as DCREATE
                ,count(NGROUPID) as NG
                from TTYPEOFVISITFORM t
                left join  TGROUPOFVISITFORM g on g.NTYPEVISITFORMID = t.NTYPEVISITFORMID
                    Where 1=1 {0}
                group by  t.NTYPEVISITFORMID,t.STYPEVISITFORMNAME ,
                to_char(t.DUPDATE,'DD/MM/YYYY') , to_char(t.DCREATE,'DD/MM/YYYY') ,t.CACTIVE
                ) A left join TVISITFORM F on F.NTYPEVISITFORMID = A.NTYPEVISITFORMID
                group by A.NTYPEVISITFORMID,A.STYPEVISITFORMNAME ,A.NG,A.DCREATE , decode(A.DUPDATE,null,'-',A.DUPDATE),A.CACTIVE", _condition));

                result = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);

            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public DataTable GetDropDownRadioFromConfig(ref string _err)
        {
            DataTable result = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"SELECT CONFIG_ID,CONFIG_TYPE,CONFIG_NAME,CONFIG_VALUE from C_CONFIG where CONFIG_TYPE ='AUDIT_LEVEL'"));
                result = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
        public int InsertVisitForm(ref String _err, UserProfile user, TTYPEOFVISITFORM item)
        {
            int result = 0;
            try
            {
                int refId = GetTTYPEOFVISITFORM_RefID(ref _err);
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"INSERT INTO TTYPEOFVISITFORM (NTYPEVISITFORMID,STYPEVISITFORMNAME,DCREATE,SCREATE,SHOWINLIST) VALUES (
                :REFID ,:STYPEVISITFORMNAME,SYSTIMESTAMP,:SCREATE,:SHOWINLIST) "));
                paramList.Add(new ParameterEntity(":REFID", refId.ToString()));
                paramList.Add(new ParameterEntity(":STYPEVISITFORMNAME", item.STYPEVISITFORMNAME.ToString()));
                paramList.Add(new ParameterEntity(":SCREATE", user.USER_ID.ToString()));
                paramList.Add(new ParameterEntity(":SHOWINLIST", item.SHOWINLIST.ToString()));
                bool resultNonQuery = new cmdCommonDAL().ExecuteNonQuery(_sb.ToString(), paramList, out _err);

                #region " ref doc "
                /*_sb.Append(System.String.Format(@"INSERT INTO TTYPEOFVISITFORM (NTYPEVISITFORMID,STYPEVISITFORMNAME,DCREATE,SCREATE,REFDOCID,SHOWINLIST) VALUES (
                :REFID ,:STYPEVISITFORMNAME,SYSTIMESTAMP,:SCREATE,{0},:SHOWINLIST) ", @"(SELECT to_char(SUBSTR(
                  (SELECT DECODE(MAX(REFDOCID),NULL,'100000',MAX(REFDOCID)) AS mid
                  FROM TTYPEOFVISITFORM
                  WHERE DECODE(REPLACE(TRANSLATE(REFDOCID,'1234567890','##########'),'#'),NULL,'NUMBER','NON NUMER') = 'NUMBER'
                  AND LENGTH(REFDOCID)                                                                               =6
                  ) ,1,1)) ||
                to_char(
                SUBSTR(
                  (SELECT DECODE(MAX(REFDOCID),NULL,'100001',MAX(REFDOCID)+1) AS mid
                  FROM TTYPEOFVISITFORM
                  WHERE DECODE(REPLACE(TRANSLATE(REFDOCID,'1234567890','##########'),'#'),NULL,'NUMBER','NON NUMER') = 'NUMBER'
                  AND LENGTH(REFDOCID)                                                                               =6
                  ) ,-5)) as mid
                FROM dual)"));*/
                #endregion " ref doc "
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public int GetTTYPEOFVISITFORM_RefID(ref String _err)
        {
            int result = 0;
            try
            {
                DataTable dt = null;
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"SELECT TTYPEOFVISITFORM_SEQ.NEXTVAL from dual"));
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                result = int.Parse(dt.Rows[0][0].ToString());
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
        #endregion " New Visit Form "

        #region + Instance +
        private static QuestionnaireBLL _instance;
        public static QuestionnaireBLL Instance
        {
            get
            {
                _instance = new QuestionnaireBLL();
                return _instance;
            }
        }
        #endregion

        #region "  Convert Type "
        public static decimal? ConvertToDecimal(String str)
        {
            decimal result;
            bool no_error = decimal.TryParse(str, out result);
            return no_error ? result : new Nullable<decimal>();
        }

        public static short? ConvertToShort(String str)
        {
            short result;
            bool no_error = short.TryParse(str, out result);
            return no_error ? result : new Nullable<short>();
        }

        public static int? ConvertToInt(String str)
        {
            int result;
            bool no_error = int.TryParse(str, out result);
            return no_error ? result : new Nullable<int>();
        }

        public static decimal? ParseDecimal(String str)
        {
            string result = str;

            if (str.IndexOf(",") != -1)
            {
                result = result.Trim().Replace(",", string.Empty);
            }

            return ConvertToDecimal(result);
        }

        #region "Decimal Format"

        public static string FormatDecimal(decimal d)
        {
            return d.ToString("#,##0.00");
        }

        public static string FormatDecimal(decimal? d)
        {
            string result = string.Empty;

            if (d.HasValue)
            {
                result = d.Value.ToString("#,##0.00");
            }

            return result;
        }

        #endregion


        public static string CheckNull(object T)
        {
            string result;

            if (T == null)
            {
                result = string.Empty;
            }
            else
            {
                result = T.ToString();
            }

            return result;
        }
        #endregion " Convert Type "
    }

}
