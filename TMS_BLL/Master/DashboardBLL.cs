﻿using System;
using System.Data;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
    public class DashboardBLL
    {
        public DataTable DocStatusSelectBLL()
        {
            try
            {
                return DashboardDAL.Instance.DriverLogSelectDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static DashboardBLL _instance;
        public static DashboardBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new DashboardBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}