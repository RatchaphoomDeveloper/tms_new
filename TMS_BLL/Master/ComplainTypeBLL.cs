﻿using System;
using TMS_DAL.Master;
using System.Data;

namespace TMS_BLL.Master
{
    public class ComplainTypeBLL
    {
        ComplainTypeDAL complainTypeDAL = new ComplainTypeDAL();

        public DataTable ComplainTypeSelect(string Condition)
        {
            try
            {
                return complainTypeDAL.ComplainTypeSelect(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainTypeSelectAllBLL(string Condition)
        {
            try
            {
                return complainTypeDAL.ComplainTypeSelectAllDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static ComplainTypeBLL _instance;
        public static ComplainTypeBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new ComplainTypeBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}