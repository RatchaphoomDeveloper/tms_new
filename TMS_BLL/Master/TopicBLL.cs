﻿using System;
using TMS_DAL.Master;
using System.Data;

namespace TMS_BLL.Master
{
    public class TopicBLL
    {
        TopicDAL topicDAL = new TopicDAL();

        public DataTable TopicSelect(string Condition)
        {
            try
            {
                return topicDAL.TopicSelect(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region TopicSelect
        public DataTable TopicSelect(string TOPIC_NAME, string CACTIVE, string TYPE)
        {
            try
            {
                return TopicDAL.Instance.TopicSelect( TOPIC_NAME,  CACTIVE, TYPE);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region TopicCheckValidate
        public bool TopicCheckValidate(int TOPIC_ID, string TOPIC_NAME, string TYPE)
        {
            try
            {
                return TopicDAL.Instance.TopicCheckValidate( TOPIC_ID,  TOPIC_NAME,  TYPE);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region TopicInsert
        public bool TopicInsert(string TOPIC_NAME, string CACTIVE, string TYPE, string USERID)
        {
            try
            {
                return TopicDAL.Instance.TopicInsert( TOPIC_NAME,  CACTIVE,  TYPE,  USERID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region TopicUpdate
        public bool TopicUpdate(int TOPIC_ID, string TOPIC_NAME, string CACTIVE, string TYPE, string USERID)
        {
            try
            {
                return TopicDAL.Instance.TopicUpdate( TOPIC_ID,  TOPIC_NAME,  CACTIVE,  TYPE,  USERID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion


        #region ComplainTypeSelect
        public DataTable ComplainTypeSelect(int WORKGROUPID, string NAME, string CACTIVE)
        {
            try
            {
                return TopicDAL.Instance.ComplainTypeSelect(WORKGROUPID, NAME, CACTIVE);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ComplainTypeCheckValidate
        public bool ComplainTypeCheckValidate(int ID, string NAME)
        {
            try
            {
                return TopicDAL.Instance.ComplainTypeCheckValidate(ID, NAME);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ComplainTypeInsert
        public bool ComplainTypeInsert(int TOPIC_ID, string COMPLAIN_TYPE_NAME, string CACTIVE, string LOCK_DRIVER, string IS_ADD_MULTIPLE_CAR, string USERID)
        {
            try
            {
                return TopicDAL.Instance.ComplainTypeInsert( TOPIC_ID,  COMPLAIN_TYPE_NAME,  CACTIVE,  LOCK_DRIVER,  IS_ADD_MULTIPLE_CAR,  USERID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region ComplainTypeUpdate
        public bool ComplainTypeUpdate(int COMPLAIN_TYPE_ID, int TOPIC_ID, string COMPLAIN_TYPE_NAME, string CACTIVE, string LOCK_DRIVER, string IS_ADD_MULTIPLE_CAR, string USERID)
        {
            try
            {
                return TopicDAL.Instance.ComplainTypeUpdate( COMPLAIN_TYPE_ID,  TOPIC_ID,  COMPLAIN_TYPE_NAME,  CACTIVE,  LOCK_DRIVER,  IS_ADD_MULTIPLE_CAR,  USERID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        public DataTable TopicSelectAllBLL(string Condition)
        {
            try
            {
                return topicDAL.TopicSelectAllDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void TopicAddBLL(string TopicName, int IsActive, int UserID)
        {
            try
            {
                topicDAL.TopicAddDAL(TopicName, IsActive, UserID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void TopicAddDetailBLL(int TopicID, string ComplainTypeName, int IsActive, int UserID, int LockDriver, int IsAddMultipleCar)
        {
            try
            {
                topicDAL.TopicAddDetailDAL(TopicID, ComplainTypeName, IsActive, UserID, LockDriver, IsAddMultipleCar);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void TopicUpdateBLL(int TopicID, int Topic_Active, int ComplainID, string ComplainTypeName, int Complain_Active, int UserID, int LockDriver, int IsAddMultipleCar)
        {
            try
            {
                topicDAL.TopicUpdateDAL(TopicID, Topic_Active, ComplainID, ComplainTypeName, Complain_Active, UserID, LockDriver, IsAddMultipleCar);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static TopicBLL _instance;
        public static TopicBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new TopicBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}