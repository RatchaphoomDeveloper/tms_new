﻿using System;
using System.Data;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
    public class DocStatusBLL
    {
        public DataTable DocStatusSelectBLL()
        {
            try
            {
                return DocStatusDAL.Instance.DocStatusSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static DocStatusBLL _instance;
        public static DocStatusBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new DocStatusBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}