﻿using System;
using System.Data;
using TMS_DAL.Master;


namespace TMS_BLL.Master
{
    public class ProductTagBLL
    {

        public DataTable ProductTagHeadSelectBLL(string Condition)
        {
            try
            {
                return ProductTagDAL.Instance.ProductTagHeadSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ProductTagItemSelectBLL(string Condition)
        {
            try
            {
                return ProductTagDAL.Instance.ProductTagItemSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ProductTagHeadInsertBLL(int KM_TAG_LVL1_ID, string KM_TAG_LVL1_NAME, int CACTIVE, int CREATER)
        {
            try
            {
                return ProductTagDAL.Instance.ProductTagHeadInsertDAL(KM_TAG_LVL1_ID, KM_TAG_LVL1_NAME, CACTIVE, CREATER);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ProductTagItemInsertBLL(int KM_TAG_LVL2_ID, string KM_TAG_LVL2_NAME, int KM_TAG_LVL1_ID, int CACTIVE, int CREATER)
        {
            try
            {
                return ProductTagDAL.Instance.ProductTagItemInsertDAL(KM_TAG_LVL2_ID, KM_TAG_LVL2_NAME, KM_TAG_LVL1_ID, CACTIVE, CREATER);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static ProductTagBLL _instance;
        public static ProductTagBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new ProductTagBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
