﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
    public class KnowledgeTypeBLL
    {
        public DataTable KnowledgeTypeSelectBLL(string Condition)
        {
            try
            {
                return KnowledgeTypeDAL.Instance.KnowledgeSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KnowledgeTypeInsertBLL(int DEPARTMENT_ID, string DEPARTMENT_NAME, int CACTIVE, int CREATER)
        {
            try
            {
                return KnowledgeTypeDAL.Instance.KnowledgeInsertDAL(DEPARTMENT_ID, DEPARTMENT_NAME, CACTIVE, CREATER);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static KnowledgeTypeBLL _instance;
        public static KnowledgeTypeBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new KnowledgeTypeBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
