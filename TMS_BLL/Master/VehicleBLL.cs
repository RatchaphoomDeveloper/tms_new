﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMS_DAL.Master;
using System.Data;

namespace TMS_BLL.Master
{
    public class VehicleBLL
    {
        public DataTable LoadDataInital()
        {
            try
            {
                return VehicleDAL.Instance.InitalDataInital();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable LoadDataHeadCar(string VendorId, string SpotStatus)
        {
            try
            {
                return VehicleDAL.Instance.InitalDataHeadCar(VendorId, SpotStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable LoadDataContrat(string CGROUP, string VendorId, string SpotStatus)
        {
            try
            {
                return VehicleDAL.Instance.InitalDataContrat(CGROUP, VendorId, SpotStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable LoadDataSelectEdit(string STRUCKID)
        {
            try
            {
                return VehicleDAL.Instance.InitalSelectEdit(STRUCKID);
            }
            catch (Exception)
            {

                throw;
            }
        }
        //เช็ครถคันนั้นเคยส่งข้อมูล Sap หรือไม่
        public DataTable HaveVehicle(string SHEADREGISTERNO)
        {
            try
            {
                return VehicleDAL.Instance.HaveVehicle(SHEADREGISTERNO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable LoadDataClassgrp()
        {
            try
            {
                return VehicleDAL.Instance.InitalDataClassgrp();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GENERATE_DOC_MV(string I_DOC_ID, string I_SCREATE)
        {
            try
            {
                return VehicleDAL.Instance.GENERATE_DOC_MVDAL(I_DOC_ID,I_SCREATE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CHECKTBL_REQ_BY_TTRUCKBLL(string I_ID)
        {
            try
            {
                return VehicleDAL.Instance.CHECKTBL_REQ_BY_TTRUCKDAL(I_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable LoadDataTruck(string VendorId, string SpotStatus)
        {
            try
            {
                return VehicleDAL.Instance.InitalDataTruck(VendorId, SpotStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable LoadDataSemi(string VendorId, string SpotStatus)
        {
            try
            {
                return VehicleDAL.Instance.InitalDataSemi(VendorId, SpotStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //โหลดรถเดี่ยวแล้วมาแสดง
        public DataTable LoadDataTruckVehicle(string VendorId, string SHEADREGISTERNO)
        {
            try
            {
                return VehicleDAL.Instance.InitalDataTruckVehicle(VendorId, SHEADREGISTERNO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //โหลดหัวMapแล้วมาแสดง
        public DataTable LoadDataHeadVehicle(string VendorId, string SHEADREGISTERNO)
        {
            try
            {
                return VehicleDAL.Instance.InitalDataHeadVehicle(VendorId, SHEADREGISTERNO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //โหลดหางลางมาสลับหางลาก
        public DataTable LoadDataSemiVehicle(string VendorId, string SHEADREGISTERNO)
        {
            try
            {
                return VehicleDAL.Instance.InitalDataSemiVehicle(VendorId, SHEADREGISTERNO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #region โหลดVehicle สำหรับ Ptt
        //โหลดรถเดี่ยวแล้วมาแสดง
        public DataTable LoadDataTruckVehicleCancel(string VendorId, string SHEADREGISTERNO)
        {
            try
            {
                return VehicleDAL.Instance.InitalDataTruckVehicleCancel(VendorId, SHEADREGISTERNO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //โหลดหัวMapแล้วมาแสดง
        public DataTable LoadDataHeadVehicleCancel(string VendorId, string SHEADREGISTERNO)
        {
            try
            {
                return VehicleDAL.Instance.InitalDataHeadVehicleCancel(VendorId, SHEADREGISTERNO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //โหลดหางลางมาสลับหางลาก
        public DataTable LoadDataSemiVehicleCancel(string VendorId, string SHEADREGISTERNO)
        {
            try
            {
                return VehicleDAL.Instance.InitalDataSemiVehicleCancel(VendorId, SHEADREGISTERNO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        public DataTable LoadDataVehicleCar()
        {
            try
            {
                return VehicleDAL.Instance.InitalDataVehicleCar();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable LoadGetVeh_Text(string STTRUCKID)
        {
            try
            {
                return VehicleDAL.Instance.GetVeh_text(STTRUCKID);
            }
            catch (Exception ex)
            {
                
                throw new Exception(ex.Message);
            }
        }
        public DataTable GENERATE_REQ_MV(string ReqType_ID)
        {
            try
            {
                return VehicleDAL.Instance.GENERATE_REQ_MVDAL(ReqType_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region SAVEDATE
        public DataTable SaveVehicle(string Cartype, DataTable DatatoDataToDatatable, string SpotStatus, string ClassGroup, int ContractID)
        {
            try
            {
                return VehicleDAL.Instance.VehicleInsert(Cartype, DatatoDataToDatatable, SpotStatus, ClassGroup, ContractID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                
            }
        }
        #endregion
        #region CHANGE VEHICLE 
        public DataTable ChangeVehicle(string Cartype, DataTable DatatoDataToDatatable)
        {
            try
            {
                return VehicleDAL.Instance.VehicleChange(Cartype, DatatoDataToDatatable);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
        }
        #endregion
        #region EDITDATA
        public DataTable EditVehicle(string Cartype, DataTable DatatoDataToDatatable, string SpotStatus, string ClassGroup, int ContractID)
        {
            try
            {
                return VehicleDAL.Instance.VehicleEdit(Cartype, DatatoDataToDatatable, SpotStatus, ClassGroup, ContractID);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        #endregion
        #region Status
        public DataTable StatusTMS(DataTable Datastatus,DataTable DataBlacklist)
        {
            return VehicleDAL.Instance.SetStatusTms(Datastatus, DataBlacklist);
        }
        #endregion
        #region + Instance +
        private static VehicleBLL _instance;
        public static VehicleBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new VehicleBLL();
                //}
                return _instance;
            }
        }
        #endregion

        public DataTable ClassGroupSelectBLL(string Condition)
        {
            return VehicleDAL.Instance.ClassGroupSelectDAL(Condition);
        }
    }
}
