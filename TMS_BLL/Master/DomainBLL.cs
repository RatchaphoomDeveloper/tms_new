﻿using System;
using System.Data;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
    public class DomainBLL
    {
        public DataTable DomainSelectBLL(string Condition)
        {
            try
            {
                return DomainDAL.Instance.DomainSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static DomainBLL _instance;
        public static DomainBLL Instance
        {
            get
            {
                _instance = new DomainBLL();
                return _instance;
            }
        }
        #endregion
    }
}