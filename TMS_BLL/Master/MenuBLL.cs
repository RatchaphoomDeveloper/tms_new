﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
    public class MenuBLL
    {
        #region MenuSelect
        public DataTable MenuSelect(string PRM)
        {
            try
            {
                return MenuDAL.Instance.MenuSelect(PRM);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable MenuSelect2(string cGroup)
        {
            try
            {
                return MenuDAL.Instance.MenuSelect2(cGroup);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region + Instance +
        private static MenuBLL _instance;
        public static MenuBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new MenuBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
