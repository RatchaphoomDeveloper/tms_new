﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
   
    public class KPIBLL
    {
        #region + Instance +
        private static KPIBLL _instance;
        public static KPIBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new KPIBLL();
                //}
                return _instance;
            }
        }
        #endregion       
        #region SelectData
        
        public DataTable GetFormReport()
        {
            try
            {
                return KPIDAL.Instance.SetFormReport();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public DataTable GetVendorContract(string conditon)
        {
            return KPIDAL.Instance.SetVendorContrart(conditon);
        }
        public DataTable GetReportById(string condition)
        {
            return KPIDAL.Instance.SetReportById(condition);
        }
        public DataTable GetAllContract()
        {
            return KPIDAL.Instance.SetAllContract();
        }
        #region ConfigKPI ค้นหา รายงาน
        public DataTable GetKpiReport(string Year)
        {
            return KPIDAL.Instance.SetKpiReport(Year);
        }
        #region ค้นหาข้อมูลที่เคยบันทึกไว้แล้ว
        public DataTable GetSelectYearConfig(string Year)
        {
            try
            {
                return KPIDAL.Instance.SetSelectYearConfig(Year);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion
        #region Select รายละเอียด KPI แบบ Manual
        public DataTable GetLoadFromKPIByYearMonth(string Year, string Month ,string Contractid)
        {
            return KPIDAL.Instance.SetLoadFromKPIByYearMonth(Year, Month,Contractid);
        }
	    #endregion
        

        #endregion
        #region SaveData
        #region Save หน้าสร้างหัวข้อ
        public DataTable GetSaveDataKPI(DataTable kpi_eva_head, DataTable kpi_eva_edtail)
        {
            return KPIDAL.Instance.SetSaveDataKPI(kpi_eva_head, kpi_eva_edtail);
        }
        #endregion        
        #region Save หน้าconfig
        public DataTable GetSaveConfig(string kpi_eva_year, DataTable GetKpiReport)
        {
            try
            {
                return KPIDAL.Instance.SetSaveConfig(kpi_eva_year, GetKpiReport);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion
        #region EditData
        public DataTable GetEditDataKPI(string kpi_eva_head_id,DataTable kpi_eva_head, DataTable kpi_eva_edtail)
        {
            return KPIDAL.Instance.SetEditDataKPI(kpi_eva_head_id,kpi_eva_head, kpi_eva_edtail);
        }
        #endregion
        public DataTable GetKpiScoreInsert(DataTable score_delete,DataTable DtDataScore)
        {
            return KPIDAL.Instance.SetKpiScoreInsert(score_delete,DtDataScore);
        }

        public DataTable KPISelectBLL()
        {
            try
            {
                return KPIDAL.Instance.KPISelectDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPISelectForEditBLL(int FormulaID)
        {
            try
            {
                return KPIDAL.Instance.KPISelectForEditDAL(FormulaID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void KPISaveBLL(int FormulaID, string VariableA, string ValueA, string VariableB, string ValueB, int CreateBy)
        {
            try
            {
                KPIDAL.Instance.KPISaveDAL(FormulaID, VariableA, ValueA, VariableB, ValueB, CreateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void KPISaveTransactionBLL(string Year, int MonthID, string VendorID, int ContractID, DataTable dtData, int StatusID, int CreateBy)
        {
            try
            {
                KPIDAL.Instance.KPISaveTransactionDAL(Year, MonthID, VendorID, ContractID, dtData, StatusID, CreateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void KPISaveTransactionBatchBLL(string Year, int MonthID, int TopicHeaderID, DataTable dtData, int CreateBy)
        {
            try
            {
                KPIDAL.Instance.KPISaveTransactionBatchDAL(Year, MonthID, TopicHeaderID, dtData, CreateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPIEvaluationSelectBLL(string Condition)
        {
            try
            {
                return KPIDAL.Instance.KPIEvaluationSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable LoadMethodBLL(string Condition)
        {
            try
            {
                return KPIDAL.Instance.LoadMethodDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable LoadFormulaBLL(string Condition)
        {
            try
            {
                return KPIDAL.Instance.LoadFormulaDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void KPIFormSaveBLL(int TopicHeaderID, string FormName, string Detail, int IsActive, DataTable dtTopic, int CreateBy)
        {
            try
            {
                KPIDAL.Instance.KPIFormSaveDAL(TopicHeaderID, FormName, Detail, IsActive, dtTopic, CreateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPIFormSelectBLL(int TopicHeaderID)
        {
            try
            {
                return KPIDAL.Instance.KPIFormSelectDAL(TopicHeaderID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPIMappingFormSelectBLL(string Year, int MonthID, string VendorID, int ContractID, string Condition)
        {
            try
            {
                return KPIDAL.Instance.KPIMappingFormSelectDAL(Year, MonthID, VendorID, ContractID, Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPIMappingFormSelect2BLL(string Year, int MonthID, string VendorID, int ContractID, int TopicHeaderID, bool IsPTT)
        {
            try
            {
                return KPIDAL.Instance.KPIMappingFormSelect2DAL(Year, MonthID, VendorID, ContractID, TopicHeaderID, IsPTT);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPIFindSumBLL(string Year, string VendorID, int ContractID, int MonthID, int FormulaID, int TopicHeaderID, int TopicDetailID)
        {
            try
            {
                return KPIDAL.Instance.KPIFindSumDAL(Year, VendorID, ContractID, MonthID, FormulaID, TopicHeaderID, TopicDetailID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPISelectListHeaderBLL(string Year)
        {
            try
            {
                return KPIDAL.Instance.KPISelectListHeaderDAL(Year);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPISelectListDetailBLL(int TopicHeaderID)
        {
            try
            {
                return KPIDAL.Instance.KPISelectListDetailDAL(TopicHeaderID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPIExportSelectBLL()
        {
            try
            {
                return KPIDAL.Instance.KPIExportSelectDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPILastUpdateSelectBLL(string Type, string Year, int MonthID)
        {
            try
            {
                return KPIDAL.Instance.KPILastUpdateSelectDAL(Type, Year, MonthID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPIExportFormSelectBLL(int TopicHeaderID)
        {
            try
            {
                return KPIDAL.Instance.KPIExportFormSelectDAL(TopicHeaderID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPICheckBLL(int TopicHeaderID)
        {
            try
            {
                return KPIDAL.Instance.KPICheckDAL(TopicHeaderID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string GetTotalTruckBLL(string Year, int MonthID, int ContractID)
        {
            try
            {
                DataTable dt = KPIDAL.Instance.GetTotalTruckDAL(Year, MonthID, ContractID);
                if (dt.Rows.Count > 0)
                    return dt.Rows[0]["TOTAL_TRUCK"].ToString();

                return "0";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string GetTotalAccidentBLL(string Year, int MonthID, int ContractID, string VendorID, string Condition)
        {
            try
            {
                DataTable dt = KPIDAL.Instance.GetTotalAccidentDAL(Year, MonthID, ContractID, VendorID, Condition);
                if (dt.Rows.Count > 0)
                    return dt.Rows[0]["TOTAL"].ToString();

                return "0";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetTotalIVMSBLL(string Year, int MonthID, int ContractID)
        {
            try
            {
                DataTable dt = KPIDAL.Instance.GetTotalIVMSDAL(Year, MonthID, ContractID);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetShipmentCostDataBLL(string Year, int MonthID, int ContractID)
        {
            try
            {
                DataTable dt = KPIDAL.Instance.GetShipmentCostDataDAL(Year, MonthID, ContractID);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}