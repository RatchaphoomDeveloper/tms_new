﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMS_DAL.Master;
using System.Data;

namespace TMS_BLL.Master
{
    public class UserGroupBLL
    {
        public DataTable UserGroupSelectBLL(string Condition)
        {
            try
            {
                return UserGroupDAL.Instance.UserGroupSelectDAL(Condition);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public DataTable DepartmentSelectBLL(string Condition)
        {
            try
            {
                return UserGroupDAL.Instance.DepartmentSelectDAL(Condition);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static UserGroupBLL _instance;
        public static UserGroupBLL Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new UserGroupBLL();
                }
                return _instance;
            }
        }
        #endregion
    }
}
