﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
   
    public class KPI2BLL
    {
        #region + Instance +
        private static KPI2BLL _instance;
        public static KPI2BLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new KPI2BLL();
                //}
                return _instance;
            }
        }
        #endregion       
        
        #region KPIScoreSelect
        public DataSet KPIScoreSelect(string I_YEAR, string I_MONTH, string I_SVENDORID, string I_SCONTRACTID)
        {
            try
            {
                return KPI2DAL.Instance.KPIScoreSelect(I_YEAR, I_MONTH, I_SVENDORID, I_SCONTRACTID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region MonthSelect
        public DataTable MonthSelect()
        {
            try
            {
                return KPI2DAL.Instance.MonthSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region KPIMappingFormSelect
        public DataSet KPIMappingFormSelect(string I_YEAR)
        {
            try
            {
                return KPI2DAL.Instance.KPIMappingFormSelect(I_YEAR);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CheckKPIMappingForm
        public DataTable CheckKPIMappingForm(string I_WHERE)
        {
            try
            {
                return KPI2DAL.Instance.CheckKPIMappingForm(I_WHERE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region SaveKPIMappingForm
        public DataTable SaveKPIMappingForm(string I_YEAR, string I_USERID, DataSet dsData, DataSet dsDataCheck)
        {
            try
            {
                return KPI2DAL.Instance.SaveKPIMappingForm(I_YEAR, I_USERID, dsData,dsDataCheck);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region KPIStatus
        public DataTable KPIStatus(string I_TYPE)
        {
            try
            {
                return KPI2DAL.Instance.KPIStatus(I_TYPE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region KPIResultSelect
        public DataTable KPIResultSelect(string I_YEAR, string I_MONTH, string I_VENDORID, string I_CONTRACTID, string I_STATUS)
        {
            try
            {
                return KPI2DAL.Instance.KPIResultSelect(I_YEAR, I_MONTH, I_VENDORID, I_CONTRACTID, I_STATUS);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region UpdateKPIStatus
        public bool UpdateKPIStatus(string I_USERID, int I_STATUS_ID, DataSet ds)
        {
            try
            {
                return KPI2DAL.Instance.UpdateKPIStatus(I_USERID, I_STATUS_ID, ds);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region KPISummarySelect
        public DataTable KPISummarySelect(string I_YEAR, string I_VENDORID, string I_CONTRACTID)
        {
            try
            {
                return KPI2DAL.Instance.KPISummarySelect(I_YEAR, I_VENDORID, I_CONTRACTID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region KPIIndexSelect
        public DataSet KPIIndexSelect(string I_YEAR)
        {
            try
            {
                return KPI2DAL.Instance.KPIIndexSelect(I_YEAR);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}