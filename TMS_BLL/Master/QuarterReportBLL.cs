﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Master;
namespace TMS_BLL.Master
{
    public class QuarterReportBLL
    {
        
        #region + Instance +
        private static QuarterReportBLL _instance;
        public static QuarterReportBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new QuarterReportBLL();
                //}
                return _instance;
            }
        }
        #endregion

        #region GetData
        public DataTable GetData(string I_YEAR, string I_SVENDORID, string I_SPROCESSID, int I_FIX)
        {
            try
            {
                return QuarterReportDAL.Instance.GetData(I_YEAR, I_SVENDORID, I_SPROCESSID, I_FIX);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetDataVendorConfig
        public DataTable GetDataVendorConfig(string I_YEAR, string I_SVENDORID, string I_SPROCESSID, int I_FIX)
        {
            try
            {
                return QuarterReportDAL.Instance.GetDataVendorConfig(I_YEAR, I_SVENDORID, I_SPROCESSID, I_FIX);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetVendor
        public DataTable GetVendor(string condition)
        {
            try
            {
                return QuarterReportDAL.Instance.GetVendor(condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Save
        public bool Save(string I_ID, string I_YEAR, string I_QUARTER, string I_SVENDORID
            , string I_DOCNO, string I_DATESTR, string I_USERID, DataTable I_DATA)
        {
            try
            {
                return QuarterReportDAL.Instance.Save(I_ID, I_YEAR, I_QUARTER, I_SVENDORID
            , I_DOCNO, I_DATESTR, I_USERID, I_DATA);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region SaveLog
        public bool SaveLog(string I_YEARS, string I_QUARTER, string I_SVENDORID, string I_LOGVERSION, string I_USERID)
        {
            try
            {
                return QuarterReportDAL.Instance.SaveLog(I_YEARS, I_QUARTER, I_SVENDORID, I_LOGVERSION, I_USERID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Config Quarter
        #region ConfigQuarterSave
        public bool ConfigQuarterSave(string I_ID, string I_YEARS, string I_QUARTER, string I_USERID, DataTable dtTitle)
        {
            try
            {
                return QuarterReportDAL.Instance.ConfigQuarterSave(I_ID, I_YEARS, I_QUARTER, I_USERID, dtTitle);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ConfigQuarterSelect
        public DataTable ConfigQuarterSelect(string I_YEARS, string I_QUARTER)
        {
            try
            {
                return QuarterReportDAL.Instance.ConfigQuarterSelect(I_YEARS, I_QUARTER);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

        #region GetSPROCESSID
        public DataTable GetSPROCESSID()
        {
            try
            {
                return QuarterReportDAL.Instance.GetSPROCESSID();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetDataQuarterByVendor
        public DataTable GetDataQuarterByVendor(string I_YEAR, string I_SVENDORID, string I_QUARTER, string I_CACTIVE)
        {
            try
            {
                return QuarterReportDAL.Instance.GetDataQuarterByVendor(I_YEAR, I_SVENDORID, I_QUARTER, I_CACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetDataQuarterLog
        public DataTable GetDataQuarterLog(string I_YEAR, string I_SVENDORID, string I_QUARTER, string I_LOGVERSION)
        {
            try
            {
                return QuarterReportDAL.Instance.GetDataQuarterLog(I_YEAR, I_SVENDORID, I_QUARTER, I_LOGVERSION);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}
