﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
    public class DivisionBLL
    {
        public DataTable DivisionSelectBLL(string Condition)
        {
            try
            {
                return DivisionDAL.Instance.DivisionSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable DivisionInsertBLL(int DIVISION_ID, string DIVISION_NAME, string DOC_PREFIX, int DEPARTMENT_ID, int CACTIVE, int CREATER)
        {
            try
            {
                return DivisionDAL.Instance.DivisionInsertDAL(DIVISION_ID, DIVISION_NAME, DOC_PREFIX, DEPARTMENT_ID, CACTIVE, CREATER);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static DivisionBLL _instance;
        public static DivisionBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new DivisionBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
