﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Common;
using TMS_DAL.Master;
using TMS_Entity;

namespace TMS_BLL.Master
{
    [Serializable]
    public class T_ANNUAL_REPORT
    {
        public string ID { get; set; }
        public string YEAR { get; set; }
        public string VENDORID { get; set; }
        public string STATUS_ID { get; set; }
        public string CREATE_DATE { get; set; }
        public string CREATE_BY { get; set; }
        public string UPDATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public string SEND_TO_VENDOR_BY { get; set; }
        public string SEND_TO_VENDOR_DATE { get; set; }
        public string DOCNO { get; set; }
        public string DOC_DATE { get; set; }
        public string CONTRACT_ID { get; set; }
        public string CONTRACT_NO { get; set; }
        public string GRADE { get; set; }
        public string POINT { get; set; }
        public List<F_UPLOAD> LST_FILE_UPLOAD { get; set; }
        public DataTable dtAnnualDetail { get; set; }
    }

    [Serializable]
    public class FinalGade
    {
        public string YEAR { get; set; }
        public string VENDORID { get; set; }
        public string GROUPID { get; set; }
        public string GROUPNAME { get; set; }
        public string SCONTRACTNO { get; set; }
        public string SCONTRACTID { get; set; }

        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public string Quarter { get; set; }
        public string Month { get; set; }
        /// <summary>
        /// หัวข้อการตัดคะแนน
        /// </summary>
        public string Toppic { get; set; }
        /// <summary>
        /// ชื่อ พขร. / ID พขร.
        /// </summary>
        public string EMPLOYEE { get; set; }
        /// <summary>
        /// ทะเบียนรถ หัวหรือหางก็ได้
        /// </summary>
        public string CARREGISTERNO { get; set; }
        /// <summary>
        /// หัวข้อ
        /// </summary>
        public List<string> LstPROCESSID { get; set; }
        /// <summary>
        /// สถานะเอกสาร
        /// </summary>
        public List<string> LstStatus { get; set; }
    }

    public class FinalGradeBLL
    {

        public FinalGradeBLL()
        {

        }

        public int? GetDocID(ref String _err)
        {
            int? result = 0;
            try
            {
                DataTable dt = null;
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                _sb.Append(System.String.Format(@"select T_ANNUAL_REPORT_SEQ.nextval from dual"));
                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                result = int.Parse(dt.Rows[0][0].ToString());
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public DataTable GetReport(ref string _err, T_ANNUAL_REPORT search)
        {
            DataTable result = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string _condition = string.Empty;
                paramList.Add(new ParameterEntity(":YEAR", search.YEAR.ToString()));
                paramList.Add(new ParameterEntity(":VENDOR_ID", search.VENDORID.ToString()));
                if (!string.IsNullOrEmpty(search.ID))
                {
                    _condition += " and R.ID=:NID";
                    paramList.Add(new ParameterEntity(":NID", search.ID.ToString()));
                }
                if (!string.IsNullOrEmpty(search.STATUS_ID))
                {
                    _condition += " and R.STATUS_ID=:STATUS_ID";
                    paramList.Add(new ParameterEntity(":STATUS_ID", search.STATUS_ID.ToString()));
                }
                #region " SQL "

                _sb.Append(string.Format(@" select R.*, RD.CONTRACT_ID, RD.GRADE, RD.POINT from T_ANNUAL_REPORT R 
                LEFT JOIN T_ANNUAL_REPORT_DETAIL RD ON RD.REPORT_ID = R.ID where R.VENDOR_ID=:VENDOR_ID AND YEAR=:YEAR {0} order by R.UPDATE_DATE desc", _condition));

                #endregion " SQL "

                result = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        public DataTable GetHistoryDetails(ref string _err, string REPORT_ID)
        {
            DataTable result = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string condition = string.Empty;
                paramList.Add(new ParameterEntity(":REPORT_ID", REPORT_ID));
                string sql = string.Format(@"select H.ID,H.REPORT_ID, tv.SABBREVIATION ,to_char(H.CREATE_DATE,'DD/MM/YYYY hh24:mm:ss') as DOWNLOAD_DATE , u.SFIRSTNAME , u.SLASTNAME  , H.FILENAME,R.YEAR,u.SUSERNAME,H.DOCNO
                from T_ANNUAL_REPORT_HISTORY H
                INNER JOIN T_ANNUAL_REPORT R ON H.REPORT_ID=R.ID and  R.STATUS_ID IN ('3','4') and R.ID =:REPORT_ID
                LEFT JOIN TUSER  u  on  H.CREATE_BY = u.SUID
                LEFT JOIN TVENDOR tv on tv.SVENDORID = R.VENDOR_ID ", condition);
                result = new cmdCommonDAL().ExecuteAdaptor(sql, paramList, out _err);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        public DataTable GetHistory(ref string _err, FinalGade search)
        {
            DataTable result = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string condition = string.Empty;
                paramList.Add(new ParameterEntity(":YEAR", search.YEAR.ToString()));

                if (!string.IsNullOrEmpty(search.VENDORID))
                {
                    condition += " and VENDOR_ID=:VENDOR_ID";
                    paramList.Add(new ParameterEntity(":VENDOR_ID", search.VENDORID.ToString()));
                }

                if (search.LstStatus != null)
                {
                    if (search.LstStatus.Count > 0) condition += string.Format(" and R.STATUS_ID IN ('{0}')", string.Join("','", search.LstStatus));
                }
                else
                    condition = string.Format(" and 2=1");

                string sql = string.Format(@"select C.CONFIG_NAME,tv.SABBREVIATION ,to_char(R.UPDATE_DATE,'DD/MM/YYYY hh24:mm:ss') as UPDATE_DATE_STR
, (select u.SFIRSTNAME || ' '|| u.SLASTNAME from TUSER u  where  R.UPDATE_BY = u.SUID ) as SENDER_NAME
, (select count(*) from T_ANNUAL_REPORT_HISTORY where IS_ACTIVE='1' and REPORT_ID=R.ID) as notice , R.*
from T_ANNUAL_REPORT R
inner join C_CONFIG C on  C.CONFIG_TYPE ='ANNUAL_REPORT_TYPE' and C.ISACTIVE='1' and R.STATUS_ID=C.CONFIG_VALUE
LEFT JOIN TVENDOR tv on tv.SVENDORID = R.VENDOR_ID 
where YEAR =:YEAR {0}", condition);
                result = new cmdCommonDAL().ExecuteAdaptor(sql, paramList, out _err);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public DataTable GetStatus(ref string _err, bool IsOnlyNotice = false)
        {
            DataTable result = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string sql = string.Format(@"select * from C_CONFIG where CONFIG_TYPE ='ANNUAL_REPORT_TYPE' and ISACTIVE='1' {0}", IsOnlyNotice ? " and CONFIG_VALUE IN (3,4)" : "");
                result = new cmdCommonDAL().ExecuteAdaptor(sql, paramList, out _err);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        public DataTable GetGroup(ref string _err, bool OnlyActive = true, string VENDORID = "")
        {
            DataTable result = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                var _conditoin = string.Empty;
                if (OnlyActive) _conditoin = " and cactive='1'";
                string sql = string.Format(@"select M_GROUPS.* from M_GROUPS where M_GROUPS.ID IN (select GROUPSID from TCONTRACT where 1=1 {1}) {0}", _conditoin, (!string.IsNullOrEmpty(VENDORID)) ? " and svendorID='" + VENDORID + "'" : "");
                result = new cmdCommonDAL().ExecuteAdaptor(sql, paramList, out _err);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public int? GetKPI_NoticeInYEAR(ref string _err, FinalGade search)
        {
            try
            {
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string _condition = string.Empty;
                #region " SQL "
                string sql = string.Format(@"select count(*) as count_mail from T_KPI_HEADER where vendor_id =:VENDORID 
                and contract_id=:CONTRACTID and STATUS_ID =3 and YEAR =:YEAR");
                #endregion " SQL "
                paramList.Add(new ParameterEntity(":VENDORID", search.VENDORID.ToString()));
                paramList.Add(new ParameterEntity(":CONTRACTID", search.SCONTRACTID.ToString()));
                paramList.Add(new ParameterEntity(":YEAR", search.YEAR.ToString()));
                int? num = new cmdCommonDAL().GetCount(sql, paramList, out _err);
                return num == 12 ? 1 : 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetGrade(ref string _err)
        {
            DataTable result = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string sql = string.Format(@"select * from TGRADE where CACTIVE='1'");
                result = new cmdCommonDAL().ExecuteAdaptor(sql, paramList, out _err);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        public DataTable GetReduceScore(ref string _err, FinalGade search, bool IsFromMainPage = false)
        {
            DataTable result = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string _condition = string.Empty;
                paramList.Add(new ParameterEntity(":YEAR", search.YEAR.ToString()));

                if (!string.IsNullOrEmpty(search.VENDORID))
                {
                    _condition += string.Format(" AND decode(o.SVENDORID,'',c.SVENDORID,o.SVENDORID) =:VENDORID");
                    paramList.Add(new ParameterEntity(":VENDORID", search.VENDORID.ToString()));
                }
                if (!string.IsNullOrEmpty(search.GROUPID))
                {
                    _condition += string.Format(" AND decode(o.GROUPSID,'',c.GROUPSID ,o.GROUPSID) =:GROUPSID");
                    paramList.Add(new ParameterEntity(":GROUPSID", search.GROUPID.ToString()));
                }

                if (search.LstPROCESSID != null)
                {
                    if (search.LstPROCESSID.Count > 0) _condition += string.Format(" AND  o.SPROCESSID IN ('{0}')", string.Join("','", search.LstPROCESSID));
                }

                if (!string.IsNullOrEmpty(search.SCONTRACTNO))
                {
                    _condition += string.Format(" AND  decode(o.SCONTRACTNO,'',c.SCONTRACTNO,o.SCONTRACTNO) LIKE '%'||:SCONTRACTNO||'%'");
                    paramList.Add(new ParameterEntity(":SCONTRACTNO", search.SCONTRACTNO.ToString()));
                }

                if (!string.IsNullOrEmpty(search.DateStart))
                {
                    _condition += " and DREDUCE >= to_date(:SDUPDATE_START || ' 00.00.00','DD/MM/YYYY hh24.mi.ss')";
                    paramList.Add(new ParameterEntity(":SDUPDATE_START", search.DateStart.ToString()));
                }

                if (!string.IsNullOrEmpty(search.DateEnd))
                {
                    _condition += " and DREDUCE <= to_date(:SDUPDATE_END || ' 23.59.59','DD/MM/YYYY hh24.mi.ss')";
                    paramList.Add(new ParameterEntity(":SDUPDATE_END", search.DateEnd.ToString()));
                }

                if (!string.IsNullOrEmpty(search.CARREGISTERNO))
                {
                    _condition += string.Format(" AND ( o.SHEADREGISTERNO  LIKE '%'||:CARREGISTERNO||'%' OR o.STRAILERREGISTERNO LIKE '%'||:CARREGISTERNO||'%')");
                    paramList.Add(new ParameterEntity(":CARREGISTERNO", search.CARREGISTERNO.ToString()));
                }

                if (!string.IsNullOrEmpty(search.EMPLOYEE))
                {
                    _condition += string.Format(" AND ( o.SEMPLOYEENAME  LIKE '%'||:EMPLOYEE||'%' OR o.SPERSONALNO LIKE '%'||:EMPLOYEE||'%')");
                    paramList.Add(new ParameterEntity(":EMPLOYEE", search.EMPLOYEE.ToString()));
                }

                if (!string.IsNullOrEmpty(search.Quarter))
                    _condition += string.Format(" AND TO_CHAR(DREDUCE, 'q') = '{0}'", search.Quarter);

                if (!string.IsNullOrEmpty(search.Month))
                    _condition += string.Format(" AND TO_CHAR(DREDUCE, 'MM') = '{0}'", search.Month);

                #region " SQL "

                if (IsFromMainPage)
                    _sb.Append(string.Format(@"select GROUPNAME,SABBREVIATION,SCONTRACTID,GROUPSID,SCONTRACTNO,sum(decode(SUMNPOINT,'',0,SUMNPOINT)) as SUMNPOINT
                    ,sum(decode(DISABLE_DRIVER,'H',0,DISABLE_DRIVER)) as DISABLE_DRIVER , sum(decode(DISABLE_DRIVER,'H',1,0)) as HOLD_DRIVER ,sum(decode(BLACKLIST,'Y',1,0))as BLACKLIST, sum(COST) as COST, '0' as col_1,'0' as col_2,'0' as col_3 ,'0' as col_4 ,'0' as col_5 ,'0' as col_6/* Order เลขที่ัญญา*/
                    from ( "));

                _sb.Append(string.Format(@" SELECT DISTINCT o.NREDUCEID, trim(o.STOPICNAME) as STOPICNAME , decode(o.SABBREVIATION,'',C.SABBREVIATION ,o.SABBREVIATION) as SABBREVIATION
                , decode(o.SCONTRACTID,'',c.SCONTRACTID,o.SCONTRACTID) as SCONTRACTID, decode(o.SCONTRACTNO,'',c.SCONTRACTNO,o.SCONTRACTNO) as SCONTRACTNO 
                ,decode(o.SVENDORID,'',c.SVENDORID,o.SVENDORID) as SVENDORID , decode(o.GROUPSID,'',c.GROUPSID,o.GROUPSID) as GROUPSID,
                (SELECT NAME GROUPNAME FROM M_GROUPS WHERE ID =  decode(o.GROUPSID,'',c.GROUPSID,o.GROUPSID)  ) GROUPNAME,
                  TO_CHAR(o.DREDUCE, 'dd/mm/yyyy', 'NLS_DATE_LANGUAGE = ENGLISH') AS DREDUCE ,
                  TO_CHAR(DREDUCE, 'q') AS Quarter, SPROCESSID, o.SREDUCEBY, o.SPROCESSNAME, o.SEMPLOYEENAME,  o.SREDUCENAME,
                  o.SUMNPOINT, o.COST, o.DISABLE_DRIVER, BLACKLIST,  o.STATUS, o.SHEADREGISTERNO,o.SPERSONALNO, o.STRAILERREGISTERNO 
                    , CASE WHEN O.SPROCESSID = '090' THEN TO_CHAR(ACC_ACCIDENT.APPROVE_DATE, 'dd/mm/yyyy', 'NLS_DATE_LANGUAGE = ENGLISH')
                         WHEN O.SPROCESSID = '080' THEN TO_CHAR(TCOMPLAIN.COMPLAIN_DATE, 'dd/mm/yyyy', 'NLS_DATE_LANGUAGE = ENGLISH')
						 WHEN O.SPROCESSID = '100' THEN TO_CHAR(TMONTHLY_HEADER.FINAL_DATETIME, 'dd/mm/yyyy', 'NLS_DATE_LANGUAGE = ENGLISH')
                                                   ELSE TO_CHAR(O.DREDUCE, 'dd/mm/yyyy', 'NLS_DATE_LANGUAGE = ENGLISH') END AS APPROVE_DATE
                    FROM
                      (SELECT R.NREDUCEID,SCONTRACTNO,ct2.SVENDORID,SABBREVIATION, GROUPSID ,
                        R.SPROCESSID,R.SREFERENCEID,
                        CASE WHEN R.SPROCESSID IN ('080', '090', '040') THEN R.SREDUCENAME ELSE Tp.STOPICNAME
                        END STOPICNAME ,
                        PC.SPROCESSNAME,  r.DREDUCE, U.SFIRSTNAME || ' ' || U.SLASTNAME AS SREDUCEBY ,
                        ES.FNAME || ' ' || ES.LNAME AS SEMPLOYEENAME , ES.SPERSONALNO, R.SREDUCENAME, R.SHEADREGISTERNO,
                        R.STRAILERREGISTERNO , - NVL(r.NPOINT,0) AS SUMNPOINT,  NVL(r.COST,0)     AS COST,
                        CASE
                          WHEN R.SPROCESSID    = '090' AND R.DISABLE_DRIVER = -2
                          THEN 'H' ELSE NVL(r.DISABLE_DRIVER,0) || ''
                        END AS DISABLE_DRIVER,
                        CASE
                          WHEN NVL(r.BLACKLIST, '') = 1 THEN 'Y' ELSE ''
                        END AS BLACKLIST,
                        CASE WHEN ap.SAPPEALID IS NULL THEN 'ไม่ได้ยื่นอุทธรณ์'  ELSE
                        CASE WHEN ap.CSTATUS = '0' THEN 'รอพิจารณา' ELSE
                        CASE WHEN ap.CSTATUS = '1' THEN 'กำลังดำเนินการ'  ELSE
                        CASE WHEN ap.CSTATUS = '2' THEN 'ขอเอกสารเพิ่มเติม' ELSE
                        CASE WHEN ap.CSTATUS = '4' THEN 'ส่งเอกสารเพิ่มเติมแล้ว' ELSE
                        CASE WHEN ap.CSTATUS = '5' THEN 'รอคณะกรรมการพิจารณา' ELSE
                        CASE WHEN ap.CSTATUS = '6' THEN 'ไม่รับอุทธรณ์' ELSE
                        CASE WHEN ap.CSTATUS = '3' THEN  CASE WHEN ap.SSENTENCER = '1' THEN 'ตัดสิน (ถูก)' ELSE CASE WHEN ap.SSENTENCER = '2' THEN 'ตัดสิน (ผิด)' ELSE 'ตัดสิน (เปลี่ยน)' END  END  END  END END END END END END END As STATUS ,
                       CASE WHEN R.SPROCESSID  = '020' AND r.SCONTRACTID IS NULL  THEN   
                            CASE WHEN T.SCONTRACTID IS NULL THEN CASE WHEN CT2.SCONTRACTID IS NULL 
                                THEN 0 ELSE CT2.SCONTRACTID  END
                            ELSE T.SCONTRACTID
                            END
                        ELSE CASE WHEN r.SCONTRACTID IS NULL THEN CT2.SCONTRACTID ELSE  r.SCONTRACTID END
                        END AS SCONTRACTID
                      FROM ((((((TREDUCEPOINT r
                      LEFT JOIN TTRUCK t
                      ON R.SHEADREGISTERNO = T.SHEADREGISTERNO )
                      LEFT JOIN
                        (SELECT distinct tct.SCONTRACTID,tct.STRUCKID, ct.SCONTRACTNO,ct.SVENDORID, ct.GROUPSID,tv.SABBREVIATION
                        FROM TCONTRACT_TRUCK tct
                        LEFT JOIN TCONTRACT ct  ON tct.SCONTRACTID         = ct.SCONTRACTID
                        LEFT JOIN TVENDOR tv on tv.SVENDORID = ct.SVENDORID
                        WHERE NVL(ct.CACTIVE,'Y')  = 'Y'
                        ) ct2 ON NVL(T.STRUCKID,1) = NVL(CT2.STRUCKID,1))
                      LEFT JOIN
                        (SELECT STOPICID,STOPICNAME,SVERSION FROM TTOPIC ) tp
                      ON R.STOPICID               = Tp.STOPICID
                      AND NVL(R.SVERSIONLIST,'1') = TP.SVERSION )
                      LEFT JOIN TAPPEAL ap
                      ON R.NREDUCEID = ap.NREDUCEID)
                      LEFT JOIN
                        (SELECT SPERSONELNO AS SPERSONALNO, FULLNAME,FNAME,LNAME, STRANS_ID, SEMPLOYEEID
                        FROM (SELECT E.SPERSONELNO,esa.FNAME,esa.LNAME, E.INAME || esa.FNAME || ' ' || esa.LNAME AS FULLNAME,
                            E.STEL , E.STRANS_ID, E.SEMPLOYEEID
                          FROM TEMPLOYEE e
                          INNER JOIN TEMPLOYEE_SAP esa
                          ON E.SEMPLOYEEID = esa.SEMPLOYEEID )
                        ) es ON R.SDRIVERNO = ES.SEMPLOYEEID ) 
                      LEFT JOIN TUSER u  ON R.SREDUCEBY = U.SUID  )
                        LEFT JOIN TPROCESS pc
                        ON R.SPROCESSID               = PC.SPROCESSID
                        WHERE NVL(r.CACTIVE,'0')      = '1'
                        AND TO_CHAR(r.DREDUCE,'YYYY') =:YEAR
                        AND (
                        CASE WHEN TO_CHAR(DREDUCE, 'q') ='1' AND R.SPROCESSID          IN
                            (SELECT DISTINCT SPROCESSID FROM M_CONFIG_QUARTER
                            LEFT JOIN M_CONFIG_QUARTER_TITLE
                            ON M_CONFIG_QUARTER.ID = M_CONFIG_QUARTER_TITLE.CONFIG_QUARTER_ID
                            WHERE M_CONFIG_QUARTER.YEARS = :YEAR
                            AND M_CONFIG_QUARTER.QUARTER = 1
                            AND M_CONFIG_QUARTER.CACTIVE = 1
                            ) THEN 1
                            WHEN TO_CHAR(DREDUCE, 'q') ='2' AND R.SPROCESSID          IN
                            (SELECT DISTINCT SPROCESSID FROM M_CONFIG_QUARTER
                            LEFT JOIN M_CONFIG_QUARTER_TITLE
                            ON M_CONFIG_QUARTER.ID = M_CONFIG_QUARTER_TITLE.CONFIG_QUARTER_ID                   
                            WHERE M_CONFIG_QUARTER.YEARS = :YEAR
                            AND M_CONFIG_QUARTER.QUARTER = 2
                            AND M_CONFIG_QUARTER.CACTIVE = 1
                            ) THEN 1
                            WHEN TO_CHAR(DREDUCE, 'q') ='3' AND R.SPROCESSID          IN
                            (SELECT DISTINCT SPROCESSID FROM M_CONFIG_QUARTER
                            LEFT JOIN M_CONFIG_QUARTER_TITLE
                            ON M_CONFIG_QUARTER.ID = M_CONFIG_QUARTER_TITLE.CONFIG_QUARTER_ID
                            WHERE M_CONFIG_QUARTER.YEARS = :YEAR
                            AND M_CONFIG_QUARTER.QUARTER = 3
                            AND M_CONFIG_QUARTER.CACTIVE = 1
                            ) THEN 1
                            WHEN TO_CHAR(DREDUCE, 'q') ='4' AND R.SPROCESSID          IN
                            (SELECT DISTINCT SPROCESSID FROM M_CONFIG_QUARTER
                            LEFT JOIN M_CONFIG_QUARTER_TITLE
                            ON M_CONFIG_QUARTER.ID = M_CONFIG_QUARTER_TITLE.CONFIG_QUARTER_ID
                            WHERE M_CONFIG_QUARTER.YEARS = :YEAR
                            AND M_CONFIG_QUARTER.QUARTER = 4
                            AND M_CONFIG_QUARTER.CACTIVE = 1
                            ) THEN 1
                            ELSE 0
                        END) = 1
                        ) o LEFT JOIN (SELECT distinct ct.SCONTRACTID , ct.SCONTRACTNO,ct.SVENDORID, ct.GROUPSID,tv.SABBREVIATION
   from TCONTRACT ct  LEFT JOIN TVENDOR tv on tv.SVENDORID = ct.SVENDORID
   WHERE NVL(ct.CACTIVE,'Y')  = 'Y' ) C on C.SCONTRACTID = o.SCONTRACTID
                        LEFT JOIN ACC_ACCIDENT ON O.STOPICNAME = ACC_ACCIDENT.ACCIDENT_ID
   LEFT JOIN TCOMPLAIN ON O.STOPICNAME = TCOMPLAIN.DOC_ID
   LEFT JOIN TMONTHLY_HEADER ON O.SREFERENCEID = to_char(TMONTHLY_HEADER.TRANSACTION_ID)
where 1=1 {0}", _condition));

                if (IsFromMainPage) _sb.Append(string.Format(@" ) group by SABBREVIATION,GROUPNAME,SCONTRACTID,GROUPSID,SCONTRACTNO order by SABBREVIATION, SCONTRACTNO  asc"));


                #endregion " SQL "

                result = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public DataTable GetDocStatus(ref string _err, FinalGade search, bool ISVendor = false)
        {
            DataTable result = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                List<ParameterEntity> paramList = new List<ParameterEntity>();
                string sql = string.Format(@"select T_ANNUAL_REPORT.*, to_char(SEND_TO_VENDOR_DATE,'DD/MM/YYYY') as ACTION_DATE  from T_ANNUAL_REPORT where YEAR=:YEAR order by T_ANNUAL_REPORT.UPDATE_DATE desc");
                paramList.Add(new ParameterEntity(":YEAR", search.YEAR.ToString()));
                result = new cmdCommonDAL().ExecuteAdaptor(sql, paramList, out _err);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public List<F_UPLOAD> GetUploadFile(ref string _err, int ID)
        {
            List<F_UPLOAD> result = new List<F_UPLOAD>();
            try
            {
                if (ID > 0)
                {
                    _err = string.Empty;
                    var _sb = new System.Text.StringBuilder();
                    List<ParameterEntity> paramList = new List<ParameterEntity>();
                    _sb.Append(System.String.Format(@"select f.ID, f.FILENAME_SYSTEM,f.FILENAME_USER,f.FULLPATH ,f.REF_INT,f.REF_STR,f.UPLOAD_ID,UPLOAD_NAME from f_upload  f INNER JOIN M_UPLOAD_TYPE ON f.UPLOAD_ID = M_UPLOAD_TYPE.UPLOAD_ID where f.ref_int =:ID and f.ISACTIVE = 1 and REF_STR='ANNUAL_REPORT_FILE'"));
                    paramList.Add(new ParameterEntity(":ID", ID.ToString()));
                    DataTable dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);
                    Utility.DataModelHelper.CreateListN<F_UPLOAD>(ref result, dt);
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("ID");
                    dt.Columns.Add("FILENAME_SYSTEM");
                    dt.Columns.Add("FILENAME_USER");
                    dt.Columns.Add("FULLPATH");
                    dt.Columns.Add("REF_INT");
                    dt.Columns.Add("REF_STR");
                    dt.Columns.Add("UPLOAD_ID");
                    dt.Columns.Add("UPLOAD_NAME");
                    Utility.DataModelHelper.CreateListN<F_UPLOAD>(ref result, dt);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
        public DataTable GetFinalScore(ref string _err, FinalGade search, string SPROCESSID, bool ISVendor = false)
        {
            DataTable result = null;
            try
            {
                DataTable dtAuditScore = new QuestionnaireBLL().GetListScoreByYear(ref _err, int.Parse(search.YEAR), int.Parse(search.YEAR), search.VENDORID, ISVendor, true);
                DataTable dtDocStatus = GetDocStatus(ref _err, search, ISVendor);
                dtDocStatus.DefaultView.Sort = " STATUS_ID asc";
                /// GET DATAFROM QUARERY
                DataTable dt = QuarterReportBLL.Instance.GetData(search.YEAR, search.VENDORID, SPROCESSID, 0);
                dt.Columns.Add("AUDIT_SCORE", typeof(decimal));
                dt.Columns.Add("ALL_QUARTER", typeof(decimal));
                dt.Columns.Add("KPI_SCORE_BY_YEAR", typeof(decimal));
                dt.Columns.Add("SUM_ALL_SCORE", typeof(decimal));

                dt.Columns.Add("ALL_QUARTER_NOTICE", typeof(int));
                dt.Columns.Add("ALL_KPI_NOTICE", typeof(int));
                dt.Columns.Add("CAN_ANNUAL_NOTICE", typeof(int));
                dt.Columns.Add("DOC_STATUS", typeof(int));
                dt.Columns.Add("GRADE", typeof(string));
                dt.Columns.Add("ACTION_DATE", typeof(string));

                List<string> lstExpression = new List<string>();
                if (!string.IsNullOrEmpty(search.GROUPNAME))
                {
                    lstExpression.Add(string.Format("GROUPNAME ='{0}'", search.GROUPNAME));
                }

                if (!string.IsNullOrEmpty(search.SCONTRACTNO))
                {
                    lstExpression.Add(string.Format("SCONTRACTNO ='{0}'", search.SCONTRACTNO));
                }

                if (lstExpression.Count > 0)
                {
                    string expression = string.Join(" and ", lstExpression);
                    DataRow[] foundRows = null;
                    foundRows = dt.Select(expression);

                    if (foundRows != null)
                    {
                        DataTable dtFilter = dt.Clone();
                        foreach (var row in foundRows) dtFilter.Rows.Add(row.ItemArray);
                        dt = null;
                        dt = dtFilter.Copy();
                    }
                }

                foreach (DataRow item in dt.Rows)
                {
                    //item["QUARTER1"] = ((double)((double.Parse(item["JAN"] + string.Empty) + double.Parse(item["FEB"] + string.Empty) + double.Parse(item["MAR"] + string.Empty)) / 3)).ToString("N");
                    //item["QUARTER2"] = ((double)((double.Parse(item["APR"] + string.Empty) + double.Parse(item["MAY"] + string.Empty) + double.Parse(item["JUN"] + string.Empty)) / 3)).ToString("N");
                    //item["QUARTER3"] = ((double)((double.Parse(item["JUL"] + string.Empty) + double.Parse(item["AUG"] + string.Empty) + double.Parse(item["SEP"] + string.Empty)) / 3)).ToString("N");
                    //item["QUARTER4"] = ((double)((double.Parse(item["OCT"] + string.Empty) + double.Parse(item["NOV"] + string.Empty) + double.Parse(item["DEC"] + string.Empty)) / 3)).ToString("N");

                    decimal total1 = 0, total2 = 0, total3 = 0, total4 = 0;
                    decimal value1 = 0, value2 = 0, value3 = 0, value4 = 0, value5 = 0, value6 = 0, value7 = 0, value8 = 0, value9 = 0, value10 = 0, value11 = 0, value12 = 0;
                    decimal valueQ1 = 0, valueQ2 = 0, valueQ3 = 0, valueQ4 = 0, totalall = 0;

                    if (decimal.Parse(item["JAN"].ToString()) > -1)
                        total1 = 1 + total1;
                    if (decimal.Parse(item["FEB"].ToString()) > -1)
                        total1 = 1 + total1;
                    if (decimal.Parse(item["MAR"].ToString()) > -1)
                        total1 = 1 + total1;
                    if (decimal.Parse(item["JAN"].ToString()) == -1)
                        value1 = 0;
                    else
                        value1 = decimal.Parse(item["JAN"].ToString());
                    if (decimal.Parse(item["FEB"].ToString()) == -1)
                        value2 = 0;
                    else
                        value2 = decimal.Parse(item["FEB"].ToString());
                    if (decimal.Parse(item["MAR"].ToString()) == -1)
                        value3 = 0;
                    else
                        value3 = decimal.Parse(item["MAR"].ToString());
                    if (total1 > 0)
                        item["QUARTER1"] = Math.Round(((decimal)((value1 + value2 + value3) / total1)), 0, MidpointRounding.AwayFromZero);
                    else
                        item["QUARTER1"] = Math.Round(((decimal)((value1 + value2 + value3))), 0, MidpointRounding.AwayFromZero);
                    if (decimal.Parse(item["APR"].ToString()) > -1)
                        total2 = 1 + total2;
                    if (decimal.Parse(item["MAY"].ToString()) > -1)
                        total2 = 1 + total2;
                    if (decimal.Parse(item["JUN"].ToString()) > -1)
                        total2 = 1 + total2;
                    if (decimal.Parse(item["APR"].ToString()) == -1)
                        value4 = 0;
                    else
                        value4 = decimal.Parse(item["APR"].ToString());
                    if (decimal.Parse(item["MAY"].ToString()) == -1)
                        value5 = 0;
                    else
                        value5 = decimal.Parse(item["MAY"].ToString());
                    if (decimal.Parse(item["JUN"].ToString()) == -1)
                        value6 = 0;
                    else
                        value6 = decimal.Parse(item["JUN"].ToString());
                    if (total2 > 0)
                        item["QUARTER2"] = Math.Round(((decimal)((value4 + value5 + value6) / total2)), 0, MidpointRounding.AwayFromZero);
                    else
                        item["QUARTER2"] = Math.Round(((decimal)((value4 + value5 + value6))), 0, MidpointRounding.AwayFromZero);
                    if (decimal.Parse(item["JUL"].ToString()) > -1)
                        total3 = 1 + total3;
                    if (decimal.Parse(item["AUG"].ToString()) > -1)
                        total3 = 1 + total3;
                    if (decimal.Parse(item["SEP"].ToString()) > -1)
                        total3 = 1 + total3;
                    if (decimal.Parse(item["JUL"].ToString()) == -1)
                        value7 = 0;
                    else
                        value7 = decimal.Parse(item["JUL"].ToString());
                    if (decimal.Parse(item["AUG"].ToString()) == -1)
                        value8 = 0;
                    else
                        value8 = decimal.Parse(item["AUG"].ToString());
                    if (decimal.Parse(item["SEP"].ToString()) == -1)
                        value9 = 0;
                    else
                        value9 = decimal.Parse(item["SEP"].ToString());
                    if (total3 > 0)
                        item["QUARTER3"] = Math.Round(((decimal)((value7 + value8 + value9) / total3)), 0, MidpointRounding.AwayFromZero);
                    else
                        item["QUARTER3"] = Math.Round(((decimal)((value7 + value8 + value9))), 0, MidpointRounding.AwayFromZero);
                    if (decimal.Parse(item["OCT"].ToString()) > -1)
                        total4 = 1 + total4;
                    if (decimal.Parse(item["NOV"].ToString()) > -1)
                        total4 = 1 + total4;
                    if (decimal.Parse(item["DEC"].ToString()) > -1)
                        total4 = 1 + total4;
                    if (decimal.Parse(item["OCT"].ToString()) == -1)
                        value10 = 0;
                    else
                        value10 = decimal.Parse(item["OCT"].ToString());
                    if (decimal.Parse(item["NOV"].ToString()) == -1)
                        value11 = 0;
                    else
                        value11 = decimal.Parse(item["NOV"].ToString());
                    if (decimal.Parse(item["DEC"].ToString()) == -1)
                        value12 = 0;
                    else
                        value12 = decimal.Parse(item["DEC"].ToString());
                    if (total4 > 0)
                        item["QUARTER4"] = Math.Round(((decimal)((value10 + value11 + value12) / total4)), 0, MidpointRounding.AwayFromZero);
                    else
                        item["QUARTER4"] = Math.Round(((decimal)((value10 + value11 + value12))), 0, MidpointRounding.AwayFromZero);

                    if (decimal.Parse(item["JAN"].ToString()) == -1 && decimal.Parse(item["FEB"].ToString()) == -1 && decimal.Parse(item["MAR"].ToString()) == -1)
                        totalall = 0;
                    else
                        totalall = 1 + totalall;
                    if (decimal.Parse(item["APR"].ToString()) == -1 && decimal.Parse(item["MAY"].ToString()) == -1 && decimal.Parse(item["JUN"].ToString()) == -1)
                        totalall = 0;
                    else
                        totalall = 1 + totalall;
                    if (decimal.Parse(item["JUL"].ToString()) == -1 && decimal.Parse(item["AUG"].ToString()) == -1 && decimal.Parse(item["SEP"].ToString()) == -1)
                        totalall = 0;
                    else
                        totalall = 1 + totalall;
                    if (decimal.Parse(item["OCT"].ToString()) == -1 && decimal.Parse(item["NOV"].ToString()) == -1 && decimal.Parse(item["DEC"].ToString()) == -1)
                        totalall = 0;
                    else
                        totalall = 1 + totalall;

                    if (decimal.Parse(item["QUARTER1"].ToString()) < 0)
                        valueQ1 = 0;
                    else
                        valueQ1 = decimal.Parse(item["QUARTER1"].ToString());
                    if (decimal.Parse(item["QUARTER2"].ToString()) < 0)
                        valueQ2 = 0;
                    else
                        valueQ2 = decimal.Parse(item["QUARTER2"].ToString());
                    if (decimal.Parse(item["QUARTER3"].ToString()) < 0)
                        valueQ3 = 0;
                    else
                        valueQ3 = decimal.Parse(item["QUARTER3"].ToString());
                    if (decimal.Parse(item["QUARTER4"].ToString()) < 0)
                        valueQ4 = 0;
                    else
                        valueQ4 = decimal.Parse(item["QUARTER4"].ToString());

                    item["ALL_QUARTER"] = Math.Round(((decimal)(((valueQ1 + valueQ2 + valueQ3 + valueQ4) / totalall) * decimal.Parse("0.30"))), 0, MidpointRounding.AwayFromZero);

                    string expression = string.Format("SVENDORID={0}", item["SVENDORID"].ToString());
                    DataRow[] foundRows = null;
                    foundRows = dtAuditScore.Select(expression);
                    item["AUDIT_SCORE"] = foundRows.Length > 0 ? double.Parse((double.Parse(foundRows[0][search.YEAR] + string.Empty) * 0.3).ToString("0.00")) : 0.0;
                    item["ALL_QUARTER_NOTICE"] = item["VIEW1"] + string.Empty + item["VIEW2"] + string.Empty + item["VIEW3"] + string.Empty + item["VIEW4"] + string.Empty == "ViewViewViewView" ? 1 : 0;
                    search.SCONTRACTID = item["SCONTRACTID"].ToString();
                    search.VENDORID = item["SVENDORID"].ToString();
                    item["ALL_KPI_NOTICE"] = GetKPI_NoticeInYEAR(ref _err, search) > 0 ? 1 : 0;

                    DataRow[] foundRowsDoc = null;
                    foundRowsDoc = dtDocStatus.Select(string.Format("VENDOR_ID='{0}'", item["SVENDORID"].ToString()));
                    item["DOC_STATUS"] = 1;
                    if (foundRowsDoc.Length > 0)
                    {
                        /// รอแจ้งข้อมูลหัวข้อย่อย	1
                        /// รอแจ้งผล (ข้อมูลครบ)	2
                        /// แจ้งผลแล้ว	3
                        /// ยกเลิกเอกสาร	4
                        string STATUS_ID = string.IsNullOrEmpty(foundRowsDoc[0]["STATUS_ID"] + string.Empty) ? "0" : foundRowsDoc[0]["STATUS_ID"].ToString();
                        int status = int.Parse(STATUS_ID);
                        item["DOC_STATUS"] = status > 2 ? status : 1;
                        item["ACTION_DATE"] = foundRowsDoc[0]["ACTION_DATE"];
                    }

                    item["CAN_ANNUAL_NOTICE"] = int.Parse(item["ALL_QUARTER_NOTICE"] + string.Empty) + int.Parse(item["ALL_KPI_NOTICE"] + string.Empty) == 2 ? 2 : 1;
                }

                result = dt.Copy();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        #region + Instance +
        private static FinalGradeBLL _instance;
        public static FinalGradeBLL Instance
        {
            get
            {
                _instance = new FinalGradeBLL();
                return _instance;
            }
        }
        #endregion
    }
}
