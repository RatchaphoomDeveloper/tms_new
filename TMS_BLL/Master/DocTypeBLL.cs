﻿using System;
using System.Data;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
    public class DocTypeBLL
    {
        public DataTable DocTypeSelectBLL(string Condition)
        {
            try
            {
                return DocTypeDAL.Instance.DocTypeSelect(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static DocTypeBLL _instance;
        public static DocTypeBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new DocTypeBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}