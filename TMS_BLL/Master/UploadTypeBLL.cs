﻿using System;
using TMS_DAL.Master;
using System.Data;

namespace TMS_BLL.Master
{
    public class UploadTypeBLL
    {
        UploadTypeDAL uploadTypeDAL = new UploadTypeDAL();

        public DataTable UploadTypeSelectBLL(string UploadType)
        {
            try
            {
                return uploadTypeDAL.UploadTypeSelectDAL(UploadType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UploadTypeSelectBLL2(string UploadType, string UploadTypeIND)
        {
            try
            {
                return uploadTypeDAL.UploadTypeSelectDAL2(UploadType,UploadTypeIND);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UploadTypeSelectBLL3(string UploadType, string UploadTypeIND)
        {
            try
            {
                return uploadTypeDAL.UploadTypeSelectDAL3(UploadType, UploadTypeIND);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UploadTypeSelectBLL2_Doc(string UploadType, string UploadTypeIND)
        {
            try
            {
                return uploadTypeDAL.UploadTypeSelectDAL2_Doc(UploadType, UploadTypeIND);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UploadTypeSelectAllBLL(string Condition)
        {
            try
            {
                return uploadTypeDAL.UploadTypeSelectAllDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UploadTypeSelectSearchBLL(string Condition)
        {
            try
            {
                return uploadTypeDAL.UploadTypeSelectSearchDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UploadTypeSelectSearchBLL_Full(string Condition)
        {
            try
            {
                return uploadTypeDAL.UploadTypeSelectSearchDAL_Full(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UpdateTypeInsertBLL(string UploadType, string UploadTypeName, string Extention, string MaxFileSize, int VendorDownload, int IsActive, int UserID)
        {
            try
            {
                uploadTypeDAL.UploadTypeInsertDAL(UploadType, UploadTypeName, Extention, MaxFileSize, VendorDownload, IsActive, UserID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UpdateTypeInsertBLL2(string UploadType, string UploadTypeName, string Extention, string MaxFileSize, int VendorDownload, int IsActive, int UserID, int IsUploadTypeIND, int IsCheckEndDate, int RequireDocAlertIND)
        {
            try
            {
                uploadTypeDAL.UploadTypeInsertDAL2(UploadType, UploadTypeName, Extention, MaxFileSize, VendorDownload, IsActive, UserID, IsUploadTypeIND, IsCheckEndDate, RequireDocAlertIND);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void UpdateTypeUpdateBLL(string UploadType, string UploadTypeName, string Extention, string MaxFileSize, int VendorDownload, int IsActive, int UserID)
        {
            try
            {
                uploadTypeDAL.UploadTypeUpdateDAL(UploadType, UploadTypeName, Extention, MaxFileSize, VendorDownload, IsActive, UserID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void UpdateTypeUpdateBLL2(string UploadType, string UploadTypeName, string Extention, string MaxFileSize, int VendorDownload, int IsActive, int UserID, int IsUploadTypeIND, int IsCheckEndDate, int RequireDocAlertIND)
        {
            try
            {
                uploadTypeDAL.UploadTypeUpdateDAL2(UploadType, UploadTypeName, Extention, MaxFileSize, VendorDownload, IsActive, UserID, IsUploadTypeIND, IsCheckEndDate, RequireDocAlertIND);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UpdateTypeUpdateBLL3(string UploadType, string UploadTypeName, string Extention, string MaxFileSize, int VendorDownload, int IsActive, int UserID, int IsUploadTypeIND, int IsCheckEndDate, int RequireDocAlertIND, string SpecialFlagValue)
        {
            try
            {
                uploadTypeDAL.UploadTypeUpdateDAL3(UploadType, UploadTypeName, Extention, MaxFileSize, VendorDownload, IsActive, UserID, IsUploadTypeIND, IsCheckEndDate, RequireDocAlertIND, SpecialFlagValue);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static UploadTypeBLL _instance;
        public static UploadTypeBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new UploadTypeBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}