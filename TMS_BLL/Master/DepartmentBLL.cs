﻿using System;
using System.Data;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
    public class DepartmentBLL
    {
        public DataTable DepartmentSelectBLL(string Condition)
        {
            try
            {
                return DepartmentDAL.Instance.DepartmentSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable DepartmentInsertBLL(int DEPARTMENT_ID, string DEPARTMENT_NAME, int CACTIVE, int CREATER)
        {
            try
            {
                return DepartmentDAL.Instance.DepartmentInsertDAL(DEPARTMENT_ID, DEPARTMENT_NAME, CACTIVE, CREATER);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static DepartmentBLL _instance;
        public static DepartmentBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new DepartmentBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
