﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMS_DAL.Master;
using System.Data;

namespace TMS_BLL.Master
{
    public class UserBLL
    {
        public DataTable UserSelectBLL(string Condition)
        {
            try
            {
                return UserDAL.Instance.UserSelectDAL(Condition);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public DataTable TeamSelectBLL(string Condition)
        {
            try
            {
                return UserDAL.Instance.TeamSelectDAL(Condition);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public DataTable UserSelectBLLWithDocDate(string Condition, string Field, string Join, string Rownum)
        {
            try
            {
                return UserDAL.Instance.UserSelectDALWithDocDate(Condition, Field, Join, Rownum);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public DataTable UserSelectBLLOnlyDocDate()
        {
            try
            {
                return UserDAL.Instance.UserSelectDALOnlyDocDate();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }


        public DataTable StatusSelectBLL(string Condition)
        {
            try
            {
                return UserDAL.Instance.StatusSelectDAL(Condition);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public DataTable SaleDistSelectBLL(string Condition)
        {
            try
            {
                return UserDAL.Instance.SaleDistSelectDAL(Condition);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public DataTable SoldToSelectBLL(string Search)
        {
            try
            {
                return UserDAL.Instance.SoldToSelectDAL(Search);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UserInsertBLL(int SUID, int CGROUP, string SVENDORID, string FIRSTNAME, string LASTNAME, string POSITION, string TEL, string USERNAME, string PASSWORD, string OLDPASSWORD, string EMAIL, int CACTIVE, int IS_CONTRACT, int CREATER, int DEPARTMENT, int DIVISION, int UserGroupID, string SALES_DISTRICT = "")
        {
            try
            {
                return UserDAL.Instance.UserInsertDAL(SUID, CGROUP, SVENDORID, FIRSTNAME, LASTNAME, POSITION, TEL, USERNAME, PASSWORD, OLDPASSWORD, EMAIL, CACTIVE, IS_CONTRACT, CREATER, DEPARTMENT, DIVISION, UserGroupID, SALES_DISTRICT);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable getCGroupFromUserGroupID(string Condition)
        {
            try
            {
                return UserDAL.Instance.CGroupFromUserGroupID(Condition);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public DataTable DocExpireBLL(int TEMPLATE_ID)
        {
            try
            {
                return UserDAL.Instance.DocExpireDAL(TEMPLATE_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable DocExpireAndInactiveBLL(int TEMPLATE_ID)
        {
            try
            {
                return UserDAL.Instance.DocExpireAndInactiveDAL(TEMPLATE_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetQueryDataBLL(string query)
        {
            try
            {
                return UserDAL.Instance.GetQueryDataDAL(query);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #region + Instance +
        private static UserBLL _instance;
        public static UserBLL Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new UserBLL();
                }
                return _instance;
            }
        }
        #endregion
    }
}
