﻿using System;
using System.Data;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
    public class EmailTemplateBLL
    {
        public DataTable EmailTemplateSelectAllBLL(string Condition)
        {
            try
            {
                return EmailTemplateDAL.Instance.EmailTemplateSelectAllDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void EmailTemplateUpdateBLL(string EmailType, int TemplateID, string TemplateName, string Subject, string Body)
        {
            try
            {
                EmailTemplateDAL.Instance.EmailTemplateUpdateDAL(EmailType, TemplateID, TemplateName, Subject, Body);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable EmailTemplateSelectBLL(int TemplateID)
        {
            try
            {
                DataTable dtEmail = new DataTable();
                dtEmail = EmailTemplateDAL.Instance.EmailTemplateSelectDAL(TemplateID);
                if (dtEmail.Rows.Count > 0)
                    dtEmail.Rows[0]["BODY"] = "<div style=\"font-size:20px; font-family:Angsana New\">" + dtEmail.Rows[0]["BODY"].ToString() + "</div>";

                return dtEmail;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //public DataTable EmailTemplateSelectTruckBLL(int TemplateID)
        //{
        //    try
        //    {
        //        DataTable dtEmail = new DataTable();
        //        dtEmail = EmailTemplateDAL.Instance.EmailTemplateSelectTruckDAL(TemplateID);
        //        if (dtEmail.Rows.Count > 0)
        //            dtEmail.Rows[0]["BODY"] = "<div style=\"font-size:20px; font-family:Angsana New\">" + dtEmail.Rows[0]["BODY"].ToString() + "</div>";

        //        return dtEmail;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}

        public DataTable EmailScheduleSelectAllBLL(string Condition)
        {
            try
            {
                return EmailTemplateDAL.Instance.EmailScheduleSelectAllDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void EmailScheduleUpdateBLL(int ScheduleID, int TemplateID, int ScheduleNo, int ScheduleDay, int IsActive, int CreateBy)
        {
            try
            {
                EmailTemplateDAL.Instance.EmailScheduleUpdateDAL(ScheduleID, TemplateID, ScheduleNo, ScheduleDay, IsActive, CreateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable EmailTypeSelectBLL(string Condition)
        {
            try
            {
                return EmailTemplateDAL.Instance.EmailTypeSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static EmailTemplateBLL _instance;
        public static EmailTemplateBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new EmailTemplateBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}