﻿using System;
using System.Data;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
    public class WarehouseBLL
    {
        public DataTable WarehouseSelectBLL()
        {
            try
            {
                return WarehouseDAL.Instance.WarehouseSelectDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static WarehouseBLL _instance;
        public static WarehouseBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new WarehouseBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}