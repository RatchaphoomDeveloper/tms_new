﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
    public class ContractTiedBLL
    {
        #region WorkGroupSelect
        public DataTable WorkGroupSelect()
        {
            try
            {
                return ContractTiedDAL.Instance.WorkGroupSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region GroupSelect
        public DataTable GroupSelect(string WorkGroupID)
        {
            try
            {
                return ContractTiedDAL.Instance.GroupSelect(WorkGroupID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region MContractSelectByPagesize
        public DataTable MContractSelectByPagesize(string fillter, int startIndex, int endIndex)
        {
            try
            {
                return ContractTiedDAL.Instance.MContractSelectByPagesize(fillter, startIndex, endIndex);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region MContractSelect
        public DataTable MContractSelect(string WORKGROUPID, string GROUPSID, string SVENDORID, string ID)
        {
            try
            {
                return ContractTiedDAL.Instance.MContractSelect(WORKGROUPID, GROUPSID, SVENDORID, ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region MContractSave
        public DataTable MContractSave(string I_ID, string I_NAME, string I_GROUPSID, string I_SVENDORID)
        {
            try
            {
                return ContractTiedDAL.Instance.MContractSave(I_ID,I_NAME, I_GROUPSID, I_SVENDORID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region MContractDelete
        public DataTable MContractDelete(string I_ID)
        {
            try
            {
                return ContractTiedDAL.Instance.MContractDelete(I_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region + Instance +
        private static ContractTiedBLL _instance;
        public static ContractTiedBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new ContractTiedBLL();
               // }
                return _instance;
            }
        }
        #endregion
    }
}
