﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Master;

namespace TMS_BLL.Master
{
    public class PositionBLL
    {
        #region PositionSelect
        public DataTable PositionSelect(string PERSON_TYPE_DESC, string CACTIVE)
        {
            try
            {
                return PositionDAL.Instance.PositionSelect(PERSON_TYPE_DESC, CACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region PositionCheckValidate
        public bool PositionCheckValidate(int PERSON_TYPE, string PERSON_TYPE_DESC)
        {
            try
            {
                return PositionDAL.Instance.PositionCheckValidate(PERSON_TYPE, PERSON_TYPE_DESC);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region PositionInsert
        public bool PositionInsert(string PERSON_TYPE_DESC, string CACTIVE, int? AGEMIN, int? AGEMAX)
        {
            try
            {
                return PositionDAL.Instance.PositionInsert(PERSON_TYPE_DESC, CACTIVE, AGEMIN, AGEMAX);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region PositionUpdate
        public bool PositionUpdate(int PERSON_TYPE, string PERSON_TYPE_DESC, string CACTIVE, int? AGEMIN, int? AGEMAX)
        {
            try
            {
                return PositionDAL.Instance.PositionUpdate(PERSON_TYPE, PERSON_TYPE_DESC, CACTIVE, AGEMIN, AGEMAX);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region + Instance +
        private static PositionBLL _instance;
        public static PositionBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new PositionBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
