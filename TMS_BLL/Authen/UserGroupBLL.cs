﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using TMS_DAL.AuthenUserGroupDAL;

namespace TMS_BLL.UserGroupSelectBLL
{
    public class UserGroupBLL
    {
        public DataTable UserGroupSelectBLL(string Condition)
        {
            try
            {
                return UserGroupDAL.Instance.UserGroupSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UserGroupAddBLL(int UserGroupID, string UserGroupName, string IsAdmin, string IsActive, int CreateBy, string TableSelect, string FieldSelect, string docPrefix)
        {
            try
            {
                UserGroupDAL.Instance.UserGroupAddDAL(UserGroupID, UserGroupName, IsAdmin, IsActive, CreateBy, TableSelect, FieldSelect, docPrefix);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable getUserGroupNameBLL(string Condition)
        {
            try
            {
                return UserGroupDAL.Instance.getUserGroupName(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        
        #region + Instance +
        private static UserGroupBLL _instance;
        public static UserGroupBLL Instance
        {
            get
            {
                _instance = new UserGroupBLL();
                return _instance;
            }
        }
        #endregion



    }

}