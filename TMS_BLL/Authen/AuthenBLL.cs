﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using TMS_BLL.Transaction.Complain;
using TMS_DAL.Common;
using TMS_DAL.Authentication;
using TMS_DAL.StatusDAL;

namespace TMS_BLL.Authentication
{
    public class AuthenBLL
    {
        public DataTable AuthenSelectBLL(string UserGroupID)
        {
            try
            {               
                return AuthenDAL.Instance.AuthenSelectDAL(UserGroupID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable AuthenSelectBLLOnlyGridShow(string UserGroupID)
        {
            try
            {
                return AuthenDAL.Instance.AuthenSelectDALOnlyGridShow(UserGroupID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void AuthenInsertBLL(string UserGroupID, DataTable dtAuthen, int UserID)
        {
            try
            {
                AuthenDAL.Instance.AuthenInsertDAL(UserGroupID, dtAuthen, UserID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable AuthenSelectTemplateBLL()
        {
            try
            {
                return AuthenDAL.Instance.AuthenSelectTemplateDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static AuthenBLL _instance;
        public static AuthenBLL Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AuthenBLL();
                }
                return _instance;
            }
        }
        #endregion
    }

    public class getStatusBLL
    {
        public DataTable StatusSelectBLL(string Condition)
        {
            try
            {
                return StatusDAL.Instance.StatusSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static getStatusBLL _instance;
        public static getStatusBLL Instance
        {
            get
            {
                _instance = new getStatusBLL();
                return _instance;
            }
        }
        #endregion
    }
}
