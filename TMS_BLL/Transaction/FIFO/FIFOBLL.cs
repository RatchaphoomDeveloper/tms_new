﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Transaction.FIFO;

namespace TMS_BLL.Transaction.FIFO
{
    public class FIFOBLL
    {

        #region + Instance +
        private static FIFOBLL _instance;
        public static FIFOBLL Instance
        {
            get
            {

                _instance = new FIFOBLL();
                
                return _instance;
            }
        }
        #endregion

        #region GetFIFO1
        public DataTable GetFIFO1(string SSTERMINALID, string SPERSONELNO, string SVENDORID)
        {
            try
            {
                return FIFODAL.Instance.GetFIFO1(SSTERMINALID, SPERSONELNO, SVENDORID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

    }
}
