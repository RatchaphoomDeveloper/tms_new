﻿using System;
using System.Data;
using TMS_DAL.Transaction.ContractConfirm;

namespace TMS_BLL.Transaction.ContractConfirm
{
    public class ContractConfirmBLL
    {
        public string SelectTruckIDBLL(string Condition)
        {
            try
            {
                DataTable dt = ContractConfirmDAL.Instance.SelectTruckIDDAL(Condition);

                return (dt.Rows.Count > 0) ? dt.Rows[0]["STRUCKID"].ToString() : string.Empty;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectTruckConfirmBLL(string User, string DateStart, string DateEnd, string ContractNo, string CConfirm)
        {
            try
            {
                DataTable dt = ContractConfirmDAL.Instance.SelectTruckConfirmDAL(User, DateStart, DateEnd, ContractNo, CConfirm);

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectTruckConfirmPTTBLL(string VendorID, string DEXPIRE, string ContractNo, string CConfirm, string SHEADREGISTERNO, string STERMINALID, string CSTANBY)
        {
            try
            {
                DataTable dt = ContractConfirmDAL.Instance.SelectTruckConfirmPTTDAL(VendorID, DEXPIRE, ContractNo, CConfirm, SHEADREGISTERNO, STERMINALID, CSTANBY);

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UpdateConfirmTruckBLL(string NConfirmID, string CConfirm)
        {
            try
            {
                ContractConfirmDAL.Instance.UpdateConfirmTruckDAL(NConfirmID, CConfirm);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool UpdateTruckConfirmCount(string NCONFIRMID, int NUNAVAILABLE, int NTRANSIT, int NAVAILABLE)
        {
            try
            {
                return ContractConfirmDAL.Instance.UpdateTruckConfirmCount(NCONFIRMID, NUNAVAILABLE, NTRANSIT, NAVAILABLE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region GetTcontractTruck
        public DataTable GetTcontractTruck(string I_CONTRACT_ID)
        {
            try
            {
                return ContractConfirmDAL.Instance.GetTcontractTruck(I_CONTRACT_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetTruckBySContractid
        public DataTable GetTruckBySContractid(string SCONTRACTID, string STERMINALID, string SHEADREGISTERNO, string DDATE, string CSTANBY)
        {
            try
            {
                return ContractConfirmDAL.Instance.GetTruckBySContractid(SCONTRACTID, STERMINALID, SHEADREGISTERNO, DDATE, CSTANBY);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetTruckBySContractidImport
        public DataTable GetTruckBySContractidImport(string SCONTRACTID, string STERMINALID, string SHEADREGISTERNO, string DDATE)
        {
            try
            {
                return ContractConfirmDAL.Instance.GetTruckBySContractidImport(SCONTRACTID, STERMINALID, SHEADREGISTERNO, DDATE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetMcarConfirmStatus
        public DataTable GetMcarConfirmStatus()
        {
            try
            {
                return ContractConfirmDAL.Instance.GetMcarConfirmStatus();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        public bool SaveTtruckConfirmList(string UserID, int NCONFIRMID, DataTable TTRUCKCONFIRMLIST)
        {
            try
            {
                return ContractConfirmDAL.Instance.SaveTtruckConfirmList(UserID, NCONFIRMID, TTRUCKCONFIRMLIST);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static ContractConfirmBLL _instance;
        public static ContractConfirmBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new ContractConfirmBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}