﻿using System;
using System.Data;
using TMS_DAL.Transaction.OrderPlan;

namespace TMS_BLL.Transaction.OrderPlan
{
    public class OrderPlanBLL
    {
        public DataTable WarehouseSelectBLL(string VendorID)
        {
            try
            {
                return OrderPlanDAL.Instance.WarehouseSelectDAL(VendorID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CompanySelectBLL()
        {
            try
            {
                return OrderPlanDAL.Instance.CompanySelectDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ContractSelectBLL(string VendorID)
        {
            try
            {
                return OrderPlanDAL.Instance.ContractSelectDAL( VendorID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region GetOrder1
        public DataTable GetOrder1(string DATESTART, string DATEEND, string NLINE, string STERMINALID, string TXTSEARCH2, string GROUPSID, string SVENDORID, string SCONTRACTID)
        {

            try
            {
                return OrderPlanDAL.Instance.GetOrder1(DATESTART, DATEEND, NLINE, STERMINALID, TXTSEARCH2, GROUPSID,  SVENDORID, SCONTRACTID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetOrder2
        public DataTable GetOrder2(string DATESTART, string DATEEND, string NLINE, string STERMINALID, string TXTSEARCH, string GROUPSID, string SVENDORID, string SCONTRACTID)
        {

            try
            {
                return OrderPlanDAL.Instance.GetOrder2(DATESTART, DATEEND, NLINE, STERMINALID, TXTSEARCH, GROUPSID, SVENDORID, SCONTRACTID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CheckLateFormIVMS
        public string CheckLateFormIVMS(string ConnectionString, string DONUMBER, string SPLANT, string SHIPTO, string DESTTIME, string ORDERID)
        {
            try
            {
                return OrderPlanDAL.Instance.CheckLateFormIVMS(ConnectionString, DONUMBER, SPLANT, SHIPTO, DESTTIME, ORDERID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ClearCheckLate
        public void ClearCheckLate(string ORDERID)
        {
            try
            {
                OrderPlanDAL.Instance.ClearCheckLate(ORDERID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region + Instance +
        private static OrderPlanBLL _instance;
        public static OrderPlanBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new OrderPlanBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}