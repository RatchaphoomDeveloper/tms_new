﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Transaction.OrderPlan;

namespace TMS_BLL.Transaction.OrderPlan
{
    public class PlanTransportBLL
    {

        #region + Instance +
        private static PlanTransportBLL _instance;
        public static PlanTransportBLL Instance
        {
            get
            {
               
                    _instance = new PlanTransportBLL();
                
                return _instance;
            }
        }
        #endregion

        #region GetTruckByVendorID
        public DataTable GetTruckByVendorID(string VendorID)
        {
            try
            {
                return PlanTransportDAL.Instance.GetTruckByVendorID(VendorID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetTruckByAssign
        public DataTable GetTruckByAssign(string SVENDORID, string DATESTART, string DATEEND)
        {
            try
            {
                return PlanTransportDAL.Instance.GetTruckByAssign(SVENDORID, DATESTART, DATEEND);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetOrderByAssign
        public DataTable GetOrderByAssign(string SVENDORID, string DATESTART, string DATEEND, string ASSIGNDATE, string STERMINALID, string SCONTRACTID, string STRUCKID, string SDELIVERYNO)
        {
            try
            {
                return PlanTransportDAL.Instance.GetOrderByAssign(SVENDORID, DATESTART, DATEEND, ASSIGNDATE, STERMINALID, SCONTRACTID, STRUCKID, SDELIVERYNO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetOrderByAssign2
        public DataTable GetOrderByAssign2(string SVENDORID, string ASSIGNDATE, string STERMINALID, string SCONTRACTID, string STRUCKID, string SDELIVERYNO)
        {
            try
            {
                return PlanTransportDAL.Instance.GetOrderByAssign2(SVENDORID, ASSIGNDATE, STERMINALID, SCONTRACTID, STRUCKID, SDELIVERYNO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetLateFormIVMS
        public string GetLateFormIVMS(string ConnectionString, string DONUMBER, string SPLANT, string SHIPTO, string VEHICLE, string DESTTIME, string FULLNAME, string EMAIL, string ORDERID, string DATETIMETOTERMINAL)
        {
            try
            {
                return PlanTransportDAL.Instance.GetLateFormIVMS(ConnectionString, DONUMBER, SPLANT, SHIPTO, VEHICLE, DESTTIME, FULLNAME, EMAIL, ORDERID, DATETIMETOTERMINAL);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CheckLateFormIVMS
        public string CheckLateFormIVMS(string ConnectionString, string DONUMBER, string SPLANT, string SHIPTO, string VEHICLE, string DESTTIME, string FULLNAME, string EMAIL, string ORDERID)
        {
            try
            {
                return PlanTransportDAL.Instance.CheckLateFormIVMS(ConnectionString, DONUMBER, SPLANT, SHIPTO, VEHICLE, DESTTIME, FULLNAME, EMAIL, ORDERID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CheckLateFormIVMS
        public string CheckLateFormIVMSBefore(string ConnectionString, string DONUMBER, string SPLANT, string SHIPTO, string VEHICLE, string DESTTIME, string FULLNAME, string EMAIL, string ORDERID)
        {
            try
            {
                return PlanTransportDAL.Instance.CheckLateFormIVMSBefore(ConnectionString, DONUMBER, SPLANT, SHIPTO, VEHICLE, DESTTIME, FULLNAME, EMAIL, ORDERID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}
