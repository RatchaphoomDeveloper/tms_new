﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Transaction.Accident;

namespace TMS_BLL.Transaction.Accident
{
    public class AccidentBLL
    {
        #region + Instance +
        private static AccidentBLL _instance;
        public static AccidentBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new AccidentBLL();
                //}
                return _instance;
            }
        }
        #endregion

        #region TruckSelect
        public DataTable TruckSelect(string I_TEXTSEARCH="", string I_VENDOR_ID = "")
        {
            try
            {
                return AccidentDAL.Instance.TruckSelect(I_TEXTSEARCH, I_VENDOR_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ShipmentSelect
        public DataTable ShipmentSelect(string I_TEXTSEARCH)
        {
            try
            {
                return AccidentDAL.Instance.ShipmentSelect(I_TEXTSEARCH);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GroupSelect
        public DataTable GroupSelect(string I_SCONTRACTID)
        {
            try
            {
                return AccidentDAL.Instance.GroupSelect(I_SCONTRACTID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion


        #region EmpSelect
        public DataTable EmpSelect(string I_TEXTSEARCH, string I_VENDORID)
        {
            try
            {
                return AccidentDAL.Instance.EmpSelect(I_TEXTSEARCH, I_VENDORID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractSelect
        public DataTable ContractSelect(string I_VENDORID)
        {
            try
            {
                return AccidentDAL.Instance.ContractSelect(I_VENDORID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region TerminalSelect
        public DataTable TerminalSelect(string I_TEXTSEARCH)
        {
            try
            {
                return AccidentDAL.Instance.TerminalSelect(I_TEXTSEARCH);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Save
        #region SaveTab1
        public DataTable SaveTab1(string I_ACCIDENT_ID, string I_ACCIDENT_DATE, string I_REPORT_PTT, string I_REPORTER, string I_VENDOR_REPORT_TIME, string I_SEND_CONSIDER, string I_PTT_APPROVE, string I_USERID, string I_INFORMER_NAME, int I_CACTIVE, string I_DETAIL_ACCIDENTTYPE_ID, string I_DETAIL_SHIPMENT_NO
            , string I_DETAIL_STRUCKID, string I_DETAIL_SVENDORID, string I_DETAIL_GROUPID, string I_DETAIL_SCONTRACTID, string I_DETAIL_SEMPLOYEEID, string I_DETAIL_AGE, string I_DETAIL_SEMPLOYEEID2, string I_DETAIL_AGE2, string I_DETAIL_SEMPLOYEEID3, string I_DETAIL_AGE3, string I_DETAIL_SEMPLOYEEID4, string I_DETAIL_AGE4,int I_DETAIL_DRIVER_NO, string I_DETAIL_SOURCE, string I_DETAIL_LOCATIONS, string I_DETAIL_GPSL, string I_DETAIL_GPSR
            , string I_DETAIL_PRODUCT_ID, DataSet dsDO)
        {
            try
            {
                return AccidentDAL.Instance.SaveTab1(I_ACCIDENT_ID, I_ACCIDENT_DATE, I_REPORT_PTT
            , I_REPORTER, I_VENDOR_REPORT_TIME, I_SEND_CONSIDER, I_PTT_APPROVE, I_USERID, I_INFORMER_NAME, I_CACTIVE, I_DETAIL_ACCIDENTTYPE_ID, I_DETAIL_SHIPMENT_NO
            , I_DETAIL_STRUCKID, I_DETAIL_SVENDORID, I_DETAIL_GROUPID, I_DETAIL_SCONTRACTID, I_DETAIL_SEMPLOYEEID, I_DETAIL_AGE, I_DETAIL_SEMPLOYEEID2, I_DETAIL_AGE2, I_DETAIL_SEMPLOYEEID3, I_DETAIL_AGE3, I_DETAIL_SEMPLOYEEID4, I_DETAIL_AGE4, I_DETAIL_DRIVER_NO, I_DETAIL_SOURCE, I_DETAIL_LOCATIONS, I_DETAIL_GPSL, I_DETAIL_GPSR
            , I_DETAIL_PRODUCT_ID, dsDO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region SaveTab2
        public DataTable SaveTab2(string I_ACCIDENT_ID, int I_CACTIVE, string I_USERID, DataSet dsTIME, DataSet dsPARTY, DataSet dsINJURY, DataSet dsDAMAGE, DataSet dsLEAK_PRODUCT, DataSet dsTRANSPORT, DataSet dsEFFICE_CORP, DataSet dsFile, bool Serious, string I_SEMPLOYEEID)
        {
            try
            {
                return AccidentDAL.Instance.SaveTab2(I_ACCIDENT_ID, I_CACTIVE, I_USERID, dsTIME, dsPARTY, dsINJURY, dsDAMAGE, dsLEAK_PRODUCT, dsTRANSPORT, dsEFFICE_CORP, dsFile, Serious, I_SEMPLOYEEID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region SaveTab3Vendor
        public DataTable SaveTab3Vendor(string I_ACCIDENT_ID, int I_CACTIVE, string I_USERID, string I_DAMAGE, DataSet dsFile)
        {
            try
            {
                return AccidentDAL.Instance.SaveTab3Vendor(I_ACCIDENT_ID, I_CACTIVE, I_USERID, I_DAMAGE, dsFile);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ApproveTab3
        public DataTable ApproveTab3(string I_ACCIDENT_ID, int I_CACTIVE, string I_USERID, int I_APPROVE_DATE, DataSet dsFile)
        {
            try
            {
                return AccidentDAL.Instance.ApproveTab3(I_ACCIDENT_ID, I_CACTIVE, I_USERID, I_APPROVE_DATE, dsFile);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region SaveTab3
        public DataTable SaveTab3(string I_ACCIDENT_ID, string I_USERID, DataSet dsFile, bool Serious, string I_SEMPLOYEEID)
        {
            try
            {
                return AccidentDAL.Instance.SaveTab3(I_ACCIDENT_ID, I_USERID, dsFile, Serious, I_SEMPLOYEEID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region SaveTab4
        public DataTable SaveTab4(string I_ACCIDENT_ID, int I_CACTIVE, string I_USERID, string UploadType, int CusScoreTypeID, decimal TotalPoint, decimal TotalCost, int TotalDisableDriver, DataSet dsScoreList, int CostCheck, string CostCheckDate, DataSet dsScoreListCar, bool isValiDate, decimal CostOther, int isSaveCusScore, string RemarkTab3, decimal OilLose, int Blacklist, string Sentencer, DataSet dsFile, string I_SEMPLOYEEID)
        {
            try
            {
                return AccidentDAL.Instance.SaveTab4(I_ACCIDENT_ID, I_CACTIVE, I_USERID, UploadType, CusScoreTypeID, TotalPoint, TotalCost, TotalDisableDriver, dsScoreList, CostCheck, CostCheckDate, dsScoreListCar, isValiDate, CostOther, isSaveCusScore, RemarkTab3, OilLose, Blacklist, Sentencer, dsFile, I_SEMPLOYEEID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

        #region Appeal
        #region AccidentAppealSave
        public DataTable AccidentAppealSave(string I_ACCIDENT_ID, int I_CACTIVE, string I_USERID, int CusScoreTypeID, decimal TotalPoint, decimal TotalCost, int TotalDisableDriver, DataSet dsScoreList, DataSet dsScoreListCar, decimal CostOther, int isSaveCusScore, string RemarkTab3, decimal OilLose, string AppealID, int Blacklist)
        {
            try
            {
                return AccidentDAL.Instance.AccidentAppealSave(I_ACCIDENT_ID, I_CACTIVE, I_USERID, CusScoreTypeID, TotalPoint, TotalCost, TotalDisableDriver, dsScoreList, dsScoreListCar, CostOther, isSaveCusScore, RemarkTab3, OilLose, AppealID, Blacklist);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #endregion

        #region UpdateStatus
        public DataTable UpdateStatus(string I_ACCIDENT_ID, int I_CACTIVE, string I_USERID)
        {
            try
            {
                return AccidentDAL.Instance.UpdateStatus(I_ACCIDENT_ID, I_CACTIVE, I_USERID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentSelect
        public DataTable AccidentSelect(string I_TEXTSEARCH, string I_ACCIDENT_DATE, string I_WORKGROUPID, string I_GROUPID, string I_SVENDORID, string I_SCONTRACTID, string I_TYPE, string I_CACTIVE, string I_CGROUP, string I_ACCIDENT_DATE_TO)
        {
            try
            {
                return AccidentDAL.Instance.AccidentSelect(I_TEXTSEARCH, I_ACCIDENT_DATE, I_WORKGROUPID, I_GROUPID, I_SVENDORID, I_SCONTRACTID, I_TYPE, I_CACTIVE, I_CGROUP, I_ACCIDENT_DATE_TO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Select
        #region AccidentTab1Select
        public DataTable AccidentTab1Select(string I_ACCIDENT_ID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentTab1Select(I_ACCIDENT_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentTab2Select
        public DataSet AccidentTab2Select(string I_ACCIDENT_ID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentTab2Select(I_ACCIDENT_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentTab3VendorSelect
        public DataSet AccidentTab3VendorSelect(string I_ACCIDENT_ID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentTab3VendorSelect(I_ACCIDENT_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentTab3Select
        public DataSet AccidentTab3Select(string I_ACCIDENT_ID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentTab3Select(I_ACCIDENT_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentTab4Select
        public DataSet AccidentTab4Select(string I_ACCIDENT_ID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentTab4Select(I_ACCIDENT_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentConcludeSelect
        public DataTable AccidentConcludeSelect(string I_YEAR, string I_SVENDORID, string I_SCONTRACTID, string I_GROUPID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentConcludeSelect(I_YEAR, I_SVENDORID, I_SCONTRACTID, I_GROUPID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion
        
        #region AccidentStatusSelect
        public DataTable AccidentStatusSelect()
        {
            try
            {
                return AccidentDAL.Instance.AccidentStatusSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentType
        #region AccidentTypeSelect
        public DataTable AccidentTypeSelect(string I_NAME, string I_CACTIVE)
        {
            try
            {
                return AccidentDAL.Instance.AccidentTypeSelect(I_NAME, I_CACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentTypeCheckValidate
        public bool AccidentTypeCheckValidate(int ID, string NAME)
        {
            try
            {
                return AccidentDAL.Instance.AccidentTypeCheckValidate(ID, NAME);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentTypeInsert
        public bool AccidentTypeInsert(string NAME, string CACTIVE, string USERID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentTypeInsert(NAME, CACTIVE, USERID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentTypeUpdate
        public bool AccidentTypeUpdate(int ID, string NAME, string CACTIVE, string USERID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentTypeUpdate(ID, NAME, CACTIVE, USERID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

        #region AccidentEventSelect
        public DataTable AccidentEventSelect()
        {
            try
            {
                return AccidentDAL.Instance.AccidentEventSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentRequestFile
        public DataTable AccidentRequestFile(string UploadType)
        {
            try
            {
                return AccidentDAL.Instance.AccidentRequestFile(UploadType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Tab4
        #region AccidentScoreDetailSelect
        public DataTable AccidentScoreDetailSelect(string ACCIDENT_ID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentScoreDetailSelect(ACCIDENT_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentScoreCarSelect
        public DataTable AccidentScoreCarSelect(string ACCIDENT_ID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentScoreCarSelect(ACCIDENT_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentScoreSelect
        public DataTable AccidentScoreSelect(string ACCIDENT_ID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentScoreSelect(ACCIDENT_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentFile4Select
        public DataTable AccidentFile4Select(string ACCIDENT_ID, string UploadTypeID, string I_IS_VENDOR_DOWNLOAD)
        {
            try
            {
                return AccidentDAL.Instance.AccidentFile4Select(ACCIDENT_ID, UploadTypeID, I_IS_VENDOR_DOWNLOAD);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentDuplicateTotal
        public DataTable AccidentDuplicateTotal(string ACCIDENT_ID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentDuplicateTotal(ACCIDENT_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentCostUpdate
        public DataTable AccidentCostUpdate(string I_ACCIDENT_ID, string I_COST_CHECK, string I_COST_CHECK_DATE, int I_CACTIVE, string I_USERID, string I_UPLOAD_TYPE, DataSet dsFile, DataSet dsFine, string I_AMOUNT_TOTAL)
        {
            try
            {
                return AccidentDAL.Instance.AccidentCostUpdate(I_ACCIDENT_ID, I_COST_CHECK, I_COST_CHECK_DATE, I_CACTIVE, I_USERID, I_UPLOAD_TYPE, dsFile, dsFine, I_AMOUNT_TOTAL);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentChangeStatus
        public DataTable AccidentChangeStatus(string I_ACCIDENT_ID, int I_CACTIVE, string I_USERID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentChangeStatus(I_ACCIDENT_ID, I_CACTIVE, I_USERID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentScoreDetailASelect
        public DataTable AccidentScoreDetailASelect(string ACCIDENT_ID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentScoreDetailASelect(ACCIDENT_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentScoreCarASelect
        public DataTable AccidentScoreCarASelect(string ACCIDENT_ID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentScoreCarASelect(ACCIDENT_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentScoreDetailPSelect
        public DataTable AccidentScoreDetailPSelect(string ACCIDENT_ID, string I_DELIVERY_DATE, string I_STRUCKID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentScoreDetailPSelect(ACCIDENT_ID, I_DELIVERY_DATE, I_STRUCKID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentScoreCarPSelect
        public DataTable AccidentScoreCarPSelect(string ACCIDENT_ID, string I_DELIVERY_DATE, string I_STRUCKID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentScoreCarPSelect(ACCIDENT_ID, I_DELIVERY_DATE, I_STRUCKID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentScoreASelect
        public DataTable AccidentScoreASelect(string ACCIDENT_ID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentScoreASelect(ACCIDENT_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentAppealSelect
        public DataTable AccidentAppealSelect(string ACCIDENT_ID)
        {
            try
            {
                return AccidentDAL.Instance.AccidentAppealSelect(ACCIDENT_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion
    }
}
