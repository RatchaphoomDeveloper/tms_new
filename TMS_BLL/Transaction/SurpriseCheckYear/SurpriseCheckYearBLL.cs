﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Transaction.SurpriseCheckYear;

namespace TMS_BLL.Transaction.SurpriseCheckYear
{
    public class SurpriseCheckYearBLL
    {
        public DataTable SurpriseCheckYearSelect(string Condition)
        {
            try
            {
                return SurpriseCheckYearDAL.Instance.SurpriseCheckYearSelect(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable SurpriseCheckTeamSelect(string Condition)
        {
            try
            {
                return SurpriseCheckYearDAL.Instance.SurpriseCheckTeamSelect(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable SurpriseCheckGroupSelectBLL(string Condition)
        {
            try
            {
                return SurpriseCheckYearDAL.Instance.SurpriseCheckGroupSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable SurpriseCheckVendorSelectBLL(string Condition)
        {
            try
            {
                return SurpriseCheckYearDAL.Instance.SurpriseCheckVendorSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable SurpriseChkReportBLL(string I_YEAR, string I_CONDITION)
        {
            try
            {
                return SurpriseCheckYearDAL.Instance.SurpriseChkReportDAL(I_YEAR, I_CONDITION);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable SurpriseChkReportYearBLL(string I_YEAR, string I_CONDITION)
        {
            try
            {
                return SurpriseCheckYearDAL.Instance.SurpriseChkReportYearDAL(I_YEAR, I_CONDITION);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable SurpriseCheckYearTruckSelectBLL(string Condition)
        {
            try
            {
                return SurpriseCheckYearDAL.Instance.SurpriseCheckYearTruckSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable SurpriseCheckYearTruckFileSelectBLL(string Condition)
        {
            try
            {
                return SurpriseCheckYearDAL.Instance.SurpriseCheckYearTruckFileSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable DataTruckSelectBLL(string Condition)
        {
            try
            {
                return SurpriseCheckYearDAL.Instance.DataTruckSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable SurpriseCheckYearSaveBLL(string I_ID, string I_TOTALCAR, string I_SVENDORID, string I_SCONTRACTID, string I_APPOINTDATE, string I_APPOINTLOCATE, string I_USERCHECK, string I_TEAMCHECK, string I_SUID, string I_ISACTIVE, string I_REMARK, DataSet I_DATA)
        {
            try
            {
                return SurpriseCheckYearDAL.Instance.SurpriseCheckYearSaveDAL(I_ID, I_TOTALCAR, I_SVENDORID, I_SCONTRACTID, I_APPOINTDATE, I_APPOINTLOCATE, I_USERCHECK, I_TEAMCHECK, I_SUID, I_ISACTIVE, I_REMARK, I_DATA);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void SurpriseCheckYearTruckSaveBLL(string I_DOC_ID, string I_SHEAD, string I_STRAIL, string I_IS_ACTIVE, string I_SUID, string I_TYPE)
        {
            try
            {
                SurpriseCheckYearDAL.Instance.SurpriseCheckYearTruckSaveDAL(I_DOC_ID, I_SHEAD, I_STRAIL, I_IS_ACTIVE, I_SUID, I_TYPE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void SurpriseCheckYearTruckFileSaveBLL(string I_DOC_ID, DataSet DS)
        {
            try
            {
                SurpriseCheckYearDAL.Instance.SurpriseCheckYearTruckFileSaveDAL(I_DOC_ID, DS);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SurpriseCheckYearTruckUpdateStatusBLL(string I_ID, string I_USERID, string I_IS_ACTIVE, string I_PATH_PTT = "", string I_PATH_VENDOR = "")
        {
            try
            {
                SurpriseCheckYearDAL.Instance.SurpriseCheckYearTruckUpdateStatusDAL(I_ID, I_USERID, I_IS_ACTIVE, I_PATH_PTT, I_PATH_VENDOR);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #region + Instance +
        private static SurpriseCheckYearBLL _instance;
        public static SurpriseCheckYearBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new SurpriseCheckYearBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }


}
