﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Transaction.SurpriseCheck;

namespace TMS_BLL.Transaction.SurpriseCheck
{
    public class SurpriseCheckContractBLL
    {
        public DataTable SurpriseCheckContractSelect(string I_TRUCK, string I_VENDORID, string I_CONTRACTID, string I_STARTDATE, string I_ENDDATE, string I_IS_ACTIVE)
        {
            try
            {
                return SurpriseCheckContractDAL.Instance.SurpriseCheckContractSelect(I_TRUCK, I_VENDORID, I_CONTRACTID, I_STARTDATE, I_ENDDATE, I_IS_ACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet SurpriseCheckContractSelectByID(string ID)
        {
            try
            {
                return SurpriseCheckContractDAL.Instance.SurpriseCheckContractSelectByID(ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet SurpriseCheckContractListSelectByID(string ID)
        {
            try
            {
                return SurpriseCheckContractDAL.Instance.SurpriseCheckContractListSelectByID(ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet SurpriseCheckContractListServiceSelectByID(string ID)
        {
            try
            {
                return SurpriseCheckContractDAL.Instance.SurpriseCheckContractListServiceSelectByID(ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable SurpriseCheckContractSave(DataSet dsData)
        {
            try
            {
                return SurpriseCheckContractDAL.Instance.SurpriseCheckContractSave(dsData);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SurpriseCheckContractListSave(DataSet dsData)
        {
            try
            {
                SurpriseCheckContractDAL.Instance.SurpriseCheckContractListSave(dsData);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void SurpriseCheckContractChangeStatusSave(int ID, int Status, int User, string Remark)
        {
            try
            {
                SurpriseCheckContractDAL.Instance.SurpriseCheckContractChangeStatusSave(ID, Status, User, Remark);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable MCheckContractListSelect()
        {
            try
            {
                return SurpriseCheckContractDAL.Instance.MCheckContractListSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable TruckSelect(string I_SCONTRACTID, string I_SVENDORID)
        {
            try
            {
                return SurpriseCheckContractDAL.Instance.TruckSelect(I_SCONTRACTID, I_SVENDORID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckSelectByChasis(string I_CHASIS, string I_STRANSPORTID)
        {
            try
            {
                return SurpriseCheckContractDAL.Instance.TruckSelectByChasis(I_CHASIS, I_STRANSPORTID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ServiceTruckSelectByChasis(string I_CHASIS, string I_STRANSPORTID, string I_TRUCK_TYPE)
        {
            try
            {
                return SurpriseCheckContractDAL.Instance.ServiceTruckSelectByChasis(I_CHASIS, I_STRANSPORTID, I_TRUCK_TYPE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable StatusSelectByType(string I_STATUS_TYPE)
        {
            try
            {
                return SurpriseCheckContractDAL.Instance.StatusSelectByType(I_STATUS_TYPE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #region + Instance +
        private static SurpriseCheckContractBLL _instance;
        public static SurpriseCheckContractBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new SurpriseCheckContractBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
