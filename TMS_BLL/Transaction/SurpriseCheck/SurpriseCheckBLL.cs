﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Transaction.SurpriseCheck;

namespace TMS_BLL.Transaction.SurpriseCheck
{
    public class SurpriseCheckBLL
    {

        #region SurpriseCheckSelect
        public DataTable SurpriseCheckSelect(string Condition)
        {
            try
            {
                return SurpriseCheckDAL.Instance.SurpriseCheckSelect(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region SurpriseCheckCarSelect
        public DataTable SurpriseCheckCarSelect(string Condition)
        {
            try
            {
                return SurpriseCheckDAL.Instance.SurpriseCheckCarSelect(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CheckTruckComplainInsert
        public DataTable CheckTruckComplainInsert(string DocID, DataTable dtHeader, string SDETAIL, string SCOMPLAINFNAME, string SCOMPLAINLNAME, string SCOMPLAINDIVISION, string DDATECOMPLAIN, string TTIMECOMPLAIN, string CTYPEPERSON
                                            , string SCOMPLAINBY, string SCREATE, string SUPDATE, string I_NSURPRISECHECKID)
        {
            try
            {
                return SurpriseCheckDAL.Instance.CheckTruckComplainInsert(DocID, dtHeader, SDETAIL, SCOMPLAINFNAME, SCOMPLAINLNAME, SCOMPLAINDIVISION, DDATECOMPLAIN, TTIMECOMPLAIN, CTYPEPERSON
                                            , SCOMPLAINBY, SCREATE, SUPDATE, I_NSURPRISECHECKID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CheckTruckComplainCanEdit
        public DataTable CheckTruckComplainCanEdit(string DocID)
        {
            try
            {
                return SurpriseCheckDAL.Instance.CheckTruckComplainCanEdit(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region CheckTruckComplainSelect
        public DataTable CheckTruckComplainSelect(string Condition)
        {
            try
            {
                return SurpriseCheckDAL.Instance.CheckTruckComplainSelect(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region CheckTruckComplainSelectHeader
        public DataTable CheckTruckComplainSelectHeader(string Condition)
        {
            try
            {
                return SurpriseCheckDAL.Instance.CheckTruckComplainSelectHeader(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CheckTruckComplainCancelDoc
        public void CheckTruckComplainCancelDoc(string DocID)
        {
            try
            {
                SurpriseCheckDAL.Instance.CheckTruckComplainCancelDoc(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region CheckTruckUpdateVendorPath
        public void CheckTruckUpdatePath(string I_PATH_PTT, string I_PATH_VENDOR, string I_KEY)
        {
            try
            {
                SurpriseCheckDAL.Instance.CheckTruckUpdatePath(I_PATH_PTT, I_PATH_VENDOR, I_KEY);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region DriverUpdateSelect
        public DataTable DriverUpdateSelect(string DocID)
        {
            try
            {
                return SurpriseCheckDAL.Instance.DriverUpdateSelect(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CheckTruckComplainChangeStatus
        public void CheckTruckComplainChangeStatus(string DocID, int DocStatusID, int UpdateBy)
        {
            try
            {
                SurpriseCheckDAL.Instance.CheckTruckComplainChangeStatus(DocID, DocStatusID, UpdateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverUpdateLog
        public void DriverUpdateLog(string ID)
        {
            try
            {
                SurpriseCheckDAL.Instance.DriverUpdateLog(ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CheckTruckComplainLockDriver
        public void CheckTruckComplainLockDriver(string DocID, int Total, int CreateBy)
        {
            try
            {
                SurpriseCheckDAL.Instance.CheckTruckComplainLockDriver(DocID, Total, CreateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverUpdateSelect2
        public DataTable DriverUpdateSelect2(string DocID)
        {
            try
            {
                return SurpriseCheckDAL.Instance.DriverUpdateSelect2(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region PersonalDetailSelect
        public DataTable PersonalDetailSelect(string EmployeeID)
        {
            try
            {
                return SurpriseCheckDAL.Instance.PersonalDetailSelect(EmployeeID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region SurpriseCheckAddEdit
        public DataTable CheckListSelect()
        {
            try
            {
                return SurpriseCheckDAL.Instance.CheckListSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CheckTruckSelect(string I_STRUCKID, string I_IS_YEAR, string I_MODE)
        {
            try
            {
                return SurpriseCheckDAL.Instance.CheckTruckSelect(I_STRUCKID, I_IS_YEAR, I_MODE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable CheckTruckEmailSelectBLL(string I_NSURPRISECHECKID)
        {
            try
            {
                return SurpriseCheckDAL.Instance.CheckTruckEmailSelectDAL(I_NSURPRISECHECKID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable CheckTruckFileSelect(string I_STRUCKID, string I_SPROCESS, string I_IS_YEAR)
        {
            try
            {
                return SurpriseCheckDAL.Instance.CheckTruckFileSelect(I_STRUCKID, I_SPROCESS, I_IS_YEAR);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void CheckTruckFileSave(string I_SCHECKID, string I_SPROCESS, string I_YEAR_ID, DataSet dsData)
        {
            try
            {
                SurpriseCheckDAL.Instance.CheckTruckFileSave(I_SCHECKID, I_SPROCESS, I_YEAR_ID, dsData);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void CheckTruckItemUpdate(string I_YEAR_ID, DataSet dsData)
        {
            try
            {
                SurpriseCheckDAL.Instance.CheckTruckItemUpdate(I_YEAR_ID, dsData);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CheckTruckFileOtherSelect(string I_STRUCKID, string I_SCONTRACTID, string I_IS_YEAR)
        {
            try
            {
                return SurpriseCheckDAL.Instance.CheckTruckFileOtherSelect(I_STRUCKID, I_SCONTRACTID, I_IS_YEAR);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void CheckTruckFileOtherSave(string I_SCHECKID, string I_STRUCKID, string I_SCONTRACTID, DataSet dsData)
        {
            try
            {
                SurpriseCheckDAL.Instance.CheckTruckFileOtherSave(I_SCHECKID, I_STRUCKID, I_SCONTRACTID, dsData);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void SurpriseCheckYearSaveBLL(string I_ID, string I_TOTALCAR, string I_SVENDORID, string I_SCONTRACTID, string I_APPOINTDATE, string I_APPOINTLOCATE, string I_USERCHECK, string I_TEAMCHECK, string I_SUID, string I_ISACTIVE)
        {
            try
            {
                SurpriseCheckDAL.Instance.SurpriseCheckYearSaveDAL(I_ID, I_TOTALCAR, I_SVENDORID, I_SCONTRACTID, I_APPOINTDATE, I_APPOINTLOCATE, I_USERCHECK, I_TEAMCHECK, I_SUID, I_ISACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region + Instance +
        private static SurpriseCheckBLL _instance;
        public static SurpriseCheckBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new SurpriseCheckBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }


}
