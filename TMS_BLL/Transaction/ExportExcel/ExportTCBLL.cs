﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TMS_DAL.Transaction.ExportExcel;

namespace TMS_BLL.Transaction.ExportExcel
{
    public class ExportTCBLL
    {
        public DataTable ExportTC(string Query)
        {
            try
            {
                return TMS_DAL.Transaction.ExportExcel.ExportTC.Instance.ExportTCDAL(Query);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #region + Instance +
        private static ExportTCBLL _instance;
        public static ExportTCBLL Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ExportTCBLL();
                }
                return _instance;
            }
        }
        #endregion
    }
}
