﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Transaction.KM;

namespace TMS_BLL.Transaction.KM
{
    public class KMBLL
    {
        public DataTable SelectKMBLL(string Condition)
        {
            try
            {
                return KMDAL.Instance.SelectKMDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectKMVendorBLL(string Condition)
        {
            try
            {
                return KMDAL.Instance.SelectKMVendorDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectKMContractBLL(string Condition)
        {
            try
            {
                return KMDAL.Instance.SelectKMContractDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectKMWorkgroupBLL(string Condition)
        {
            try
            {
                return KMDAL.Instance.SelectKMWorkgroupDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectCountKMBLL(string Condition)
        {
            try
            {
                return KMDAL.Instance.SelectCountKMDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable SelectLikeViewKMBLL(string Condition)
        {
            try
            {
                return KMDAL.Instance.SelectLikeViewKMDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectUserLikeKMBLL(string Condition)
        {
            try
            {
                return KMDAL.Instance.SelectUserLikeKMDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectKMBookmarkStatusBLL(string Condition)
        {
            try
            {
                return KMDAL.Instance.SelectKMBookmarkDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectKMTagBLL(string Condition)
        {
            try
            {
                return KMDAL.Instance.SelectKMTagDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectKMUploadBLL(string Condition)
        {
            try
            {
                return KMDAL.Instance.SelectKMUploadDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectKMSharetoBLL(string Condition)
        {
            try
            {
                return KMDAL.Instance.SelectKMSharetoDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectKMCommentBLL(string Condition)
        {
            try
            {
                return KMDAL.Instance.SelectKMCommentDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectKMViewBLL(string Condition)
        {
            try
            {
                return KMDAL.Instance.SelectKMViewDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectKMBookBLL(string Condition)
        {
            try
            {
                return KMDAL.Instance.SelectKMBookDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        #region InsertKMBLL
        public DataTable InsertKMBLL(int KM, string TopicName, string TopicID, int Creater, int Department, int Division, string CoP, int Knowledge, DataTable Keyword, string OtherKey, string Executive, DataTable Doc, int Status, DataTable Employee, DataTable Usergroup, DataTable Divis, DataTable Depart, DataTable Cont_Username, DataTable Cont_Group, DataTable Cont_Transgroup, DataTable Cont_Name, DataTable Cont_Number, string OtherEmail)
        {
            try
            {
                return KMDAL.Instance.InsertKMDAL(KM, TopicName, TopicID, Creater, Department, Division, CoP, Knowledge, Keyword, OtherKey, Executive, Doc, Status, Employee, Usergroup, Divis, Depart, Cont_Username, Cont_Group, Cont_Transgroup, Cont_Name, Cont_Number, OtherEmail);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectKMEmailBLL(int Type, string Detail)
        {
            try
            {
                return KMDAL.Instance.SelectKMEmailDAL(Type, Detail);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectKMCheckShareBLL(int Type, int UserID)
        {
            try
            {
                return KMDAL.Instance.SelectKMCheckShareDAL(Type, UserID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable InsertKMBookmarkBLL(int KM, int Creater, int Status)
        {
            try
            {
                return KMDAL.Instance.InsertKMBookmarkDAL(KM, Creater, Status);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable InsertKMCommentBLL(int KM_Comment, int KM, int Creater, int Status, string Detail)
        {
            try
            {
                return KMDAL.Instance.InsertKMCommentDAL(KM_Comment, KM, Creater, Status, Detail);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable InsertKMViewBLL(int KM)
        {
            try
            {
                return KMDAL.Instance.InsertKMViewDAL(KM);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable InsertKMLikeBLL(int KM, int Creater, int Status)
        {
            try
            {
                return KMDAL.Instance.InsertKMBLikeDAL(KM, Creater, Status);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable InsertKMLikeWebBLL(int KM, int Creater, int Status)
        {
            try
            {
                return KMDAL.Instance.InsertKMBLikeWebDAL(KM, Creater, Status);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable DeleteKMHeaderBLL(int KM, int Creater, int Status)
        {
            try
            {
                return KMDAL.Instance.DeleteKMBHeaderDAL(KM, Creater, Status);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region + Instance +
        private static KMBLL _instance;
        public static KMBLL Instance
        {
            get
            {

                _instance = new KMBLL();

                return _instance;
            }
        }
        #endregion
    }
}
