﻿using System;
using System.Data;
using TMS_DAL.Transaction.Report;

namespace TMS_BLL.Transaction.Report
{
    public class MonthlyReportBLL
    {
        public DataTable MonthlyReportRequestFileBLL(string Condition)
        {
            try
            {
                return MonthlyReportDAL.Instance.MonthlyReportRequestFileDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckTPNRequestFileBLL()
        {
            try
            {
                return MonthlyReportDAL.Instance.TruckTPNRequestFileDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ReportSaveBLL(bool IsSaveFileUpload, DataTable dtUpload, string Year, int MonthID, string VendorID, int CreateBy, int StatusID, int ReportID, int IsPass, int IsUploadComplete, int CusScore, int isSaveConfirmDate, string SCONTRACTID)
        {
            try
            {
                MonthlyReportDAL.Instance.ReportSaveDAL(IsSaveFileUpload, dtUpload, Year, MonthID, VendorID, CreateBy, StatusID, ReportID, IsPass, IsUploadComplete, CusScore, isSaveConfirmDate, SCONTRACTID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CanUploadSelectBLL()
        {
            try
            {
                return MonthlyReportDAL.Instance.CanUploadSelectDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CanUploadSelectAllBLL()
        {
            try
            {
                return MonthlyReportDAL.Instance.CanUploadSelectAllDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable MonthSelectBLL(string Year)
        {
            try
            {
                return MonthlyReportDAL.Instance.MonthSelectDAL(Year);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable YearSelectBLL()
        {
            try
            {
                return MonthlyReportDAL.Instance.YearSelectDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CusScoreSelectBLL(string Year, int MonthID, string VendorID, string SCONTRACTID)
        {
            try
            {
                return MonthlyReportDAL.Instance.CusScoreSelectDAL(Year, MonthID, VendorID, SCONTRACTID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ReportSaveTab4BLL(string Year, int MonthID, string VendorID, int StatusID, DataTable dtScore, int UserID, bool IsValidate, string SCONTRACTID)
        {
            try
            {
                MonthlyReportDAL.Instance.ReportSaveTab4DAL(Year, MonthID, VendorID, StatusID, dtScore, UserID, IsValidate, SCONTRACTID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ReportCheckSelectBLL(string Year, int MonthID, string Condition)
        {
            try
            {
                return MonthlyReportDAL.Instance.ReportCheckSelectDAL(Year, MonthID, Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ReportCheckSelectTab6BLL(string Year, string Condition)
        {
            try
            {
                return MonthlyReportDAL.Instance.ReportCheckSelectTab6DAL(Year, Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string ReportCheckSelectDetailBLL(string Year, int MonthID, string Condition)
        {
            try
            {
                string strReturn = string.Empty;

                DataTable dt = MonthlyReportDAL.Instance.ReportCheckSelectDetailDAL(Year, MonthID, Condition);
                if (dt.Rows.Count == 0)
                    strReturn = string.Empty;
                else if (string.Equals(dt.Rows[0]["IS_UPLOAD_COMPLETE"].ToString(), "1") && !string.Equals(dt.Rows[0]["DOC_STATUS_ID"].ToString(), "1"))
                    strReturn = "O";
                else
                    strReturn = "X";

                return strReturn;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ReportCheckSelectDetailTab6BLL(string Year, int MonthID, string Condition, ref string Status, ref string StatusDate, string RequestType, string RefString, int RefInt)
        {
            try
            {
                string strReturn = string.Empty;

                DataTable dt = MonthlyReportDAL.Instance.ReportCheckSelectDetailTab6DAL(Year, MonthID, Condition, RequestType, RefString, RefInt);
                if (dt.Rows.Count == 0)
                    strReturn = string.Empty;
                else if ((string.Equals(dt.Rows[0]["DOC_STATUS_ID"].ToString(), "2") || string.Equals(dt.Rows[0]["DOC_STATUS_ID"].ToString(), "4")) && string.Equals(dt.Rows[0]["IS_DOC_FINAL"].ToString(), "N") && !string.Equals(dt.Rows[0]["DATE_CONFIRM"].ToString(), string.Empty))
                    Status = "X-";
                else if (string.Equals(dt.Rows[0]["DOC_STATUS_ID"].ToString(), "2") || string.Equals(dt.Rows[0]["DOC_STATUS_ID"].ToString(), "3"))
                    Status = "P";
                else if (string.Equals(dt.Rows[0]["IS_UPLOAD_COMPLETE"].ToString(), "1") && !string.Equals(dt.Rows[0]["DOC_STATUS_ID"].ToString(), "1") && string.Equals(dt.Rows[0]["IS_DOC_FINAL"].ToString(), "Y"))
                    Status = (string.Equals(dt.Rows[0]["IS_PASS"].ToString(), "1")) ? "O+" : "O-";
                else
                    Status = "X";

                if (dt.Rows.Count > 0)
                    StatusDate = dt.Rows[0]["DATE_CONFIRM"].ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetDateNowBLL()
        {
            try
            {
                return MonthlyReportDAL.Instance.GetDateNowDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet GetEmailReportMonthlyNotPassBLL(string DeliveryDepartmentCode, string VendorID)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = MonthlyReportDAL.Instance.GetEmailReportMonthlyNotPassDAL(DeliveryDepartmentCode, VendorID);

                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ReportSelectConfigBLL()
        {
            try
            {
                return MonthlyReportDAL.Instance.ReportSelectConfigDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckSelectSpecialValueBLL()
        {
            try
            {
                return MonthlyReportDAL.Instance.TruckSelectSpecialValueDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static MonthlyReportBLL _instance;
        public static MonthlyReportBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new MonthlyReportBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}