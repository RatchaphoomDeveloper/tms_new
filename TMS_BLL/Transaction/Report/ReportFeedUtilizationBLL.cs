﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.Transaction.Report;

namespace TMS_BLL.Transaction.Report
{
    public class ReportFeedUtilizationBLL
    {
        #region + Instance +
        private static ReportFeedUtilizationBLL _instance;
        public static ReportFeedUtilizationBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new ReportFeedUtilizationBLL();
                //}
                return _instance;
            }
        }
        #endregion

        #region ReportFeedUtilizationSelect
        public DataTable ReportFeedUtilizationSelect(int GROUPID, string DDELIVERYSTART, string DDELIVERYEND, string SESSIONID)
        {
            try
            {
                return ReportFeedUtilizationDAL.Instance.ReportFeedUtilizationSelect(GROUPID, DDELIVERYSTART, DDELIVERYEND, SESSIONID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GroupSelect
        public DataTable GroupSelect()
        {
            try
            {
                return ReportFeedUtilizationDAL.Instance.GroupSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}
