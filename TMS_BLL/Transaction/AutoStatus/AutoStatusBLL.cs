﻿using System;
using System.Data;
using TMS_DAL.Transaction.AutoStatus;

namespace TMS_BLL.Transaction.AutoStatus
{
    public class AutoStatusBLL
    {
        public void DriverLogInsertBLL(DataTable dtData)
        {
            try
            {
                AutoStatusDAL.Instance.DriverLogInsertDAL(dtData);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable DriverLogSelectBLL(string Condition, string DateStart, string DateEnd)
        {
            try
            {
                return AutoStatusDAL.Instance.DriverLogSelectDAL(Condition, DateStart, DateEnd);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static AutoStatusBLL _instance;
        public static AutoStatusBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new AutoStatusBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}