﻿using System;
using System.Data;
using TMS_DAL.Transaction.Appeal;

namespace TMS_BLL.Transaction.Appeal
{
    public class AppealBLL
    {
        public DataTable AppealComplainSelectBLL(string SVENDORID, string Condition)
        {
            try
            {
                return AppealDAL.Instance.AppealComplainSelectDAL(SVENDORID, Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #region AppealSentencerTypeSelect
        public DataTable AppealSentencerTypeSelect(string I_SSENTENCERCODE)
        {
            try
            {
                return AppealDAL.Instance.AppealSentencerTypeSelect(I_SSENTENCERCODE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region AppealSelectByID
        public DataSet AppealSelectByID(string I_APPEALID)
        {
            try
            {
                return AppealDAL.Instance.AppealSelectByID(I_APPEALID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region AppealConsiderSave
        public DataTable AppealConsiderSave(string I_SAPPEALID,
                                          int I_USERID,
                                          DataSet I_DATATABLE,
                                          DataTable dtUpload)
        {
            try
            {
                return AppealDAL.Instance.AppealConsiderSave(I_SAPPEALID,
                                          I_USERID,
                                          I_DATATABLE, dtUpload);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region AppealChangeSave
        public DataTable AppealChangeSave(string I_SAPPEALID,
                                          int I_USERID,
                                          DataSet I_DATATABLE,
                                          DataTable dtUpload)
        {
            try
            {
                return AppealDAL.Instance.AppealChangeSave(I_SAPPEALID,
                                          I_USERID,
                                          I_DATATABLE, dtUpload);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region AppealSentencerTypeSelect
        public DataTable AppealSentencerTypeSelect()
        {
            try
            {
                return AppealDAL.Instance.AppealSentencerTypeSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region AppealVendorSave
        public DataTable AppealVendorSave(string I_SAPPEALID,
                                          int I_USERID,
                                          DataSet I_DATATABLE,
                                          DataTable dtUpload)
        {
            try
            {
                return AppealDAL.Instance.AppealVendorSave(I_SAPPEALID,
                                          I_USERID,
                                          I_DATATABLE, dtUpload);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region AppealSelect
        public DataTable AppealSelect(string I_SEARCH,
                                     string I_STARTDATE,
                                     string I_ENDDATE,
                                     string I_DOCID,
                                     string I_STATUS,
                                     string I_APPEALID,
                                     string I_TYPE)
        {
            try
            {
                return AppealDAL.Instance.AppealSelect(I_SEARCH,
                                     I_STARTDATE,
                                     I_ENDDATE,
                                     I_DOCID,
                                     I_STATUS,
                                     I_APPEALID,
                                     I_TYPE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AppealORSelect
        public DataTable AppealORSelect(string I_SEARCH,
                                     string I_STARTDATE,
                                     string I_ENDDATE,
                                     string I_DOCID,
                                     string I_STATUS,
                                     string I_APPEALID,
                                     string I_TYPE)
        {
            try
            {
                return AppealDAL.Instance.AppealORSelect(I_SEARCH,
                                     I_STARTDATE,
                                     I_ENDDATE,
                                     I_DOCID,
                                     I_STATUS,
                                     I_APPEALID,
                                     I_TYPE,
                                     null);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion


        #region AppealVendorSelect
        public DataTable AppealVendorSelect(string I_SEARCH,
                                     string I_STARTDATE,
                                     string I_ENDDATE,
                                     string I_DOCID,
                                     string I_STATUS,
                                     string I_APPEALID,
                                     string I_TYPE,
                                     string I_SVENDORID)
        {
            try
            {
                return AppealDAL.Instance.AppealVendorSelect(I_SEARCH,
                                     I_STARTDATE,
                                     I_ENDDATE,
                                     I_DOCID,
                                     I_STATUS,
                                     I_APPEALID,
                                     I_TYPE,
                                     I_SVENDORID,
                                     null);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region AppealVendorSelect
        public DataTable AppealVendorSelectByID(
                                     string I_TYPE,
                                     string I_V_ID)
        {
            try
            {
                return AppealDAL.Instance.AppealVendorSelect(string.Empty,
                                     string.Empty,
                                     string.Empty,
                                     string.Empty,
                                     string.Empty,
                                     string.Empty,
                                     I_TYPE,
                                     string.Empty,
                                     I_V_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion
        public DataTable AppealAccidentSelectBLL(string SVENDORID, string Condition)
        {
            try
            {
                return AppealDAL.Instance.AppealAccidentSelectDAL(SVENDORID, Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable AppealMonthlyReportSelectBLL(string SVENDORID, string Condition)
        {
            try
            {
                return AppealDAL.Instance.AppealMonthlyReportSelectDAL(SVENDORID, Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UpdateStatusComplainBLL(string DocID, int DocStatusID, string DeliveryDate)
        {
            try
            {
                AppealDAL.Instance.UpdateStatusComplainDAL(DocID, DocStatusID, DeliveryDate);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ComplainCheckStatusBLL()
        {
            try
            {
                AppealDAL.Instance.ComplainCheckStatusDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable AppealSelectBLL(string AppealID)
        {
            try
            {
                return AppealDAL.Instance.AppealSelectDAL(AppealID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable AppealSelectAllBLL(string AppealID)
        {
            try
            {
                return AppealDAL.Instance.AppealSelectAllDAL(AppealID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void AppealSaveImportFileBLL(DataTable dtUpload, string DocID, string UploadType, int CreateBy)
        {
            try
            {
                AppealDAL.Instance.AppealSaveImportFileDAL(dtUpload, DocID, UploadType, CreateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ImportFileSelectBLL(string DocID, string UploadType)
        {
            try
            {
                return AppealDAL.Instance.ImportFileSelectDAL(DocID, UploadType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static AppealBLL _instance;
        public static AppealBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new AppealBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}