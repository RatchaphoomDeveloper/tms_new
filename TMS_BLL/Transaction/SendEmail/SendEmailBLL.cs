﻿using System;
using System.Data;
using TMS_DAL.Transaction.SendEmail;

namespace TMS_BLL.Transaction.SendEmail
{
    public class SendEmailBLL
    {
        public DataTable SendEmailReportSelectBLL(int TemplateID, int ReportID)
        {
            try
            {
                return SendEmailDAL.Instance.SendEmailReportSelectDAL(TemplateID, ReportID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SendEmailCarSelectBLL(int TemplateID)
        {
            try
            {
                return SendEmailDAL.Instance.SendEmailCarSelectDAL(TemplateID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static SendEmailBLL _instance;
        public static SendEmailBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new SendEmailBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
