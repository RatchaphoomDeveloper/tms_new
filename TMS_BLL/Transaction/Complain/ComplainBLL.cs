﻿using System;
using TMS_DAL.Transaction.Complain;
using System.Data;
using System.Text;
using TMS_DAL.Common;
using TMS_Entity;
using System.Collections.Generic;

namespace TMS_BLL.Transaction.Complain
{
    public class ComplainBLL
    {
        public DataTable ComplainSelectBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.ComplainSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainSelectLockDriverBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.ComplainSelectLockDriverDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable UploadDocLateSelectBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.UploadDocLateSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SupplainSelectBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.SupplainSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckWorkgroupBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.TruckWorkgroupDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainSelectHeaderBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.ComplainSelectHeaderDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void ComplainChangeStatusBLL(string DocID)
        {
            try
            {
                ComplainDAL.Instance.ComplainChangeStatusDAL(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void ComplainChangStatusBLL(string DocID)
        {
            try
            {
                ComplainDAL.Instance.ComplainChangStatusDAL(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable ComplainSelectAddDriverBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.ComplainSelectAddDriverDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable ComplainCanEditSelectBLL(string DocID, string SCREATE, string OTP, int OTPType)
        {
            try
            {
                return ComplainDAL.Instance.ComplainCanEditSelectDAL(DocID, SCREATE, OTP, OTPType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainSelectDriverBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.ComplainSelectDriverDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainSelectHistoryBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.ComplainSelectHistoryDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainExportSelectBLL(string SSERVICEID)
        {
            try
            {
                return ComplainDAL.Instance.ComplainExportDAL(SSERVICEID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainImportFileBLL(string SSERVICEID)
        {
            try
            {
                return ComplainDAL.Instance.ComplainImportFileDAL(SSERVICEID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainRequestFileBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.ComplainRequestFileDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainInsertBLL(string DocID, DataTable dtHeader, string SDETAIL, string SCOMPLAINFNAME, string SCOMPLAINLNAME, string SCOMPLAINDIVISION, string DDATECOMPLAIN, string TTIMECOMPLAIN, string CTYPEPERSON
                                            , string SCOMPLAINBY, string SCREATE, string SUPDATE, string OTP, int OTPType)
        {
            try
            {
                return ComplainDAL.Instance.ComplainInsertDAL(DocID, dtHeader, SDETAIL, SCOMPLAINFNAME, SCOMPLAINLNAME, SCOMPLAINDIVISION, DDATECOMPLAIN, TTIMECOMPLAIN, CTYPEPERSON
                                            , SCOMPLAINBY, SCREATE, SUPDATE, OTP, OTPType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable OutboundSelectBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.OutboundSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ComplainAddDriverBLL(string DocID, string DRIVERNO)
        {
            try
            {
                ComplainDAL.Instance.ComplainAddDriverDAL(DocID, DRIVERNO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void ComplainInsertLockDriverNewBLL(string DocID, string DriverNo, string CreateBy, string VendorId)
        {
            try
            {
                ComplainDAL.Instance.ComplainInsertLockDriverNewDAL(DocID, DriverNo, CreateBy, VendorId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void ComplainDelDriverBLL(string DocID)
        {
            try
            {
                ComplainDAL.Instance.ComplainDelDriverDAL(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ComplainUpdateDriverBLL(string DocID, string DRIVERNO, int Active, string Days, string Remark, int Blacklist)
        {
            try
            {
                ComplainDAL.Instance.ComplainUpdateDriverDAL(DocID, DRIVERNO, Active, Days, Remark, Blacklist);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ComplainUpdateOldDriverBLL(string DocID, string DRIVERNO, int Active, string Days, string Remark, int Blacklist)
        {
            try
            {
                ComplainDAL.Instance.ComplainUpdateOldDriverDAL(DocID, DRIVERNO, Active, Days, Remark, Blacklist);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable OutboundSelectBLL2(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.OutboundSelectDAL2(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable VendorSelectBLL()
        {
            try
            {
                return ComplainDAL.Instance.VendorSelectDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable EmployeeSelectBLL()
        {
            try
            {
                return ComplainDAL.Instance.EmployeeSelectDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainListSelectBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.ComplainListSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainRequireFieldBLL(int COMPLAIN_TYPE_ID, string FIELD_TYPE)
        {
            try
            {
                return ComplainDAL.Instance.ComplainRequireFieldDAL(COMPLAIN_TYPE_ID, FIELD_TYPE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainCanEditBLL(string DocID)
        {
            try
            {
                return ComplainDAL.Instance.ComplainCanEditDAL(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ContractSelectBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.ContractSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ContractSelectBLL2(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.ContractSelectDAL2(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckSelectBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.TruckSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckOutContractSelectBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.TruckOutContractSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable PersonalSelectBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.PersonalSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable LoginSelectBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.LoginSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ComplainAttachInsertBLL(DataTable dtUpload, string DocID, int CreateBy)
        {
            try
            {
                ComplainDAL.Instance.ComplainAttachInsertDAL(dtUpload, DocID, CreateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainCancelDocBLL(string DocID, int CreateBy)
        {
            try
            {
                return ComplainDAL.Instance.ComplainCancelDocDAL(DocID, CreateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ScoreSelectBLL()
        {
            try
            {
                return ComplainDAL.Instance.ScoreSelectDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CusScoreSelectBLL()
        {
            try
            {
                return ComplainDAL.Instance.CusScoreSelectDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ComplainChangeStatusBLL(string DocID, int DocStatusID, int UpdateBy)
        {
            try
            {
                ComplainDAL.Instance.ComplainChangeStatusDAL(DocID, DocStatusID, UpdateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable PersonalDetailSelectBLL(string EmployeeID)
        {
            try
            {
                return ComplainDAL.Instance.PersonalDetailSelectDAL(EmployeeID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void LockDriverInsertBLL(string DocID, string EmployeeID, string DateStart, string DateEnd, string Status, string DriverStatus, int CreateBy)
        {
            try
            {
                ComplainDAL.Instance.LockDriverInsertDAL(DocID, EmployeeID, DateStart, DateEnd, Status, DriverStatus, CreateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable DriverUpdateSelectBLL(string DocID)
        {
            try
            {
                return ComplainDAL.Instance.DriverUpdateSelectDAL(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void DriverUpdateLogBLL(string DocID, string ID)
        {
            try
            {
                ComplainDAL.Instance.DriverUpdateLogDAL(DocID, ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void DriverUpdateTMSBLL(string EmployeeID, string StatusTMS, string StatusSAP, string StatusBan, string Remark, string Blacklist)
        {
            try
            {
                ComplainDAL.Instance.DriverUpdateTMSDAL(EmployeeID, StatusTMS, StatusSAP, StatusBan, Remark, Blacklist);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UnlockDriverSaveBLL(string EmployeeID)
        {
            try
            {
                ComplainDAL.Instance.UnlockDriverSaveDAL(EmployeeID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ComplainLockDriverBLL(string DocID, int Total, int CreateBy, string Detail, string Blacklist, string Driver)
        {
            try
            {
                ComplainDAL.Instance.ComplainLockDriverDAL(DocID, Total, CreateBy, Detail, Blacklist, Driver);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable DriverUpdateSelect2BLL(string DocID)
        {
            try
            {
                return ComplainDAL.Instance.DriverUpdateSelect2DAL(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainExportFinalSelectBLL(string SSERVICEID)
        {
            try
            {
                return ComplainDAL.Instance.ComplainExportFinalDAL(SSERVICEID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainImportSelectBLL(string DocID)
        {
            try
            {
                return ComplainDAL.Instance.ComplainImportSelectDAL(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ExportScoreSelectBLL(string DocID)
        {
            try
            {
                return ComplainDAL.Instance.ExportScoreSelectDAL(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UpdateDriverStatusBLL(DataTable dtDriver)
        {
            try
            {
                ComplainDAL.Instance.UpdateDriverStatusDAL(dtDriver);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ChangeStatusBLL(string DocID, int DocStatusID, int UpdateBy)
        {
            try
            {
                ComplainDAL.Instance.ChangeStatusDAL(DocID, DocStatusID, UpdateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private string ConvertDataSetEmail(DataSet ds)
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    for (int j = 0; j < ds.Tables[i].Rows.Count; j++)
                    {
                        if (!string.Equals(ds.Tables[i].Rows[j]["EMAIL"].ToString(), string.Empty))
                        {
                            if (!string.Equals(sb.ToString(), string.Empty))
                                sb.Append(",");

                            sb.Append(ds.Tables[i].Rows[j]["EMAIL"].ToString());
                        }
                    }
                }

                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string GetEmailComplainBLL(string DeliveryDepartmentCodeAdmin, string DeliveryDepartmentCodeAdmin2, string DeliveryDepartmentCode, int UserID, string VendorID, bool IsAdmin, bool Check)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = ComplainDAL.Instance.GetEmailComplainDAL(DeliveryDepartmentCodeAdmin, DeliveryDepartmentCodeAdmin2, DeliveryDepartmentCode, UserID, VendorID, IsAdmin, Check);

                return ConvertDataSetEmail(ds);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string GetEmailComplainSaveTab3BLL(string VendorID, string DeliveryDepartmentCode, int UserID)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = ComplainDAL.Instance.GetEmailComplainSaveTab3DAL(VendorID, DeliveryDepartmentCode, UserID);

                return ConvertDataSetEmail(ds);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectDuplicateTotalCarBLL(string DocID)
        {
            try
            {
                return ComplainDAL.Instance.SelectDuplicateTotalCarDAL(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectDuplicateTotalCarPartialBLL(string DocID, string Condition)
        {
            try
            {
                return ComplainDAL.Instance.SelectDuplicateTotalCarPartialDAL(DocID, Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainEmailRequestDocBLL(string DocID)
        {
            try
            {
                return ComplainDAL.Instance.ComplainEmailRequestDocDAL(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void OTPSaveDAL(int OTPType, string OTPCode, int UserID)
        {
            try
            {
                ComplainDAL.Instance.OTPSaveDAL(OTPType, OTPCode, UserID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable OTPCheckBLL(string OTP, int OTPType)
        {
            try
            {
                return ComplainDAL.Instance.OTPCheckDAL(OTP, OTPType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable AppealSelectBLL(string DocID)
        {
            try
            {
                DataTable dt = ComplainDAL.Instance.AppealSelectDAL(DocID);
                if (dt.Rows.Count > 0)
                {
                    if (string.Equals(dt.Rows[0]["TOTAL_POINT"].ToString(), string.Empty))
                        dt.Rows[0]["TOTAL_POINT"] = 0;

                    if (string.Equals(dt.Rows[0]["TOTAL_COST"].ToString(), string.Empty))
                        dt.Rows[0]["TOTAL_COST"] = 0;

                    if (string.Equals(dt.Rows[0]["TOTAL_DISABLE"].ToString(), string.Empty))
                        dt.Rows[0]["TOTAL_DISABLE"] = 0;
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectScorePointBLL(string Point)
        {
            try
            {
                return ComplainDAL.Instance.SelectScorePointDAL(Point);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UpdateDriverStatusBLL(string SEMPLOYEEID, string CACTIVE, string DRVSTATUS)
        {
            try
            {
                ComplainDAL.Instance.UpdateDriverStatusDAL(SEMPLOYEEID, CACTIVE, DRVSTATUS);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SearchByDOBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.SearchByDODAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable SearchDateEndLockDriverBLL(string Condition)
        {
            try
            {
                return ComplainDAL.Instance.SearchDateEndLockDriverDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SentencerSelectBLL(int I_SENTENCER_TYPE_ID)
        {
            try
            {
                return ComplainDAL.Instance.SentencerSelctDAL(I_SENTENCER_TYPE_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SentencerSelectDetailBLL(string I_SSENTENCERCODE)
        {
            try
            {
                return ComplainDAL.Instance.SentencerSelctDetailDAL(I_SSENTENCERCODE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetDriverConfictBLL()
        {
            try
            {
                return ComplainDAL.Instance.GetDriverConfictDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable dtGenID_TBL_REQUEST_TMP(string sServiceType)
        {
            try
            {
                return ComplainDAL.Instance.TBL_REQUEST_TMP(sServiceType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Get Car License by หัวข้อร้องเรียน 
        /// </summary>
        /// <param name="LicenseNo">
        /// ทะเบียนรถ (หัว) 
        /// </param>
        /// <remarks>
        /// Add By ORN.K
        /// </remarks>

        public DataTable GetCarHEADREGISTERNO(string LicenseNo, ref String _err, int StartRowIndex, int PageSize)
        {
            DataTable dt = null;
            try
            {
                var _sb = new System.Text.StringBuilder();
                var hashtable = new System.Collections.Hashtable();
                _sb.Append(System.String.Format(@"SELECT T.* FROM (SELECT SHEADREGISTERNO, STRAILERREGISTERNO,
                                                    row_number() over (order by SHEADREGISTERNO ASC) rn
                                                  FROM
                                                    ( SELECT DISTINCT TTRUCK.SHEADREGISTERNO, TTRUCK.STRAILERREGISTERNO
                                                    FROM TTRUCK
                                                    LEFT JOIN TCONTRACT_TRUCK
                                                    ON TCONTRACT_TRUCK.STRUCKID = TTRUCK.STRUCKID
                                                    LEFT JOIN TCONTRACT
                                                    ON TCONTRACT_TRUCK.SCONTRACTID = TCONTRACT.SCONTRACTID
                                                    LEFT JOIN TVENDOR
                                                    ON TCONTRACT.SVENDORID = TVENDOR.SVENDORID
                                                    WHERE TTRUCK.CACTIVE   = 'Y'
                                                    AND TVENDOR.CACTIVE    = '1'
                                                    AND TCONTRACT.CACTIVE  = 'Y'
                                                    AND TTRUCK.SHEADREGISTERNO LIKE '%' ||:HEADREGISTERNO ||'%'
                                                    )
                                                  ) T
                                                WHERE rn BETWEEN :StartRowIndex  * :PageSize + 1 AND(( :StartRowIndex  * :PageSize + 1) + :PageSize) - 1"));
                //ComplainListSelectBLL<

                List<ParameterEntity> paramList = new List<ParameterEntity>();

                paramList.Add(new ParameterEntity(":HEADREGISTERNO", LicenseNo));
                paramList.Add(new ParameterEntity(":StartRowIndex", StartRowIndex.ToString()));
                paramList.Add(new ParameterEntity(":PageSize", PageSize.ToString()));

                dt = new cmdCommonDAL().ExecuteAdaptor(_sb.ToString(), paramList, out _err);

                //hashtable.Add(":HEADREGISTERNO", LicenseNo);
                //hashtable.Add(":StartRowIndex", StartRowIndex);
                //hashtable.Add(":PageSize", PageSize);
                //DataAccessClient.Instance.ExecuteAdaptor(_sb.ToString(), hashtable, out dt, out _err);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable VendorSelectByHeadRegisterNoBLL(string HeadRegisterNo)
        {
            try
            {
                return ComplainDAL.Instance.VendorSelectDAL(String.Format(@"AND ven.SVENDORID IN
                                                              (SELECT DISTINCT TCONTRACT.SVENDORID
                                                              FROM TCONTRACT_TRUCK
                                                              LEFT JOIN TTRUCK
                                                              ON TCONTRACT_TRUCK.STRUCKID = TTRUCK.STRUCKID
                                                              LEFT JOIN TCONTRACT
                                                              ON TCONTRACT_TRUCK.SCONTRACTID = TCONTRACT.SCONTRACTID
                                                              LEFT JOIN TVENDOR
                                                              ON TCONTRACT.SVENDORID = TVENDOR.SVENDORID
                                                              WHERE TTRUCK.CACTIVE     = 'Y'
                                                              AND TVENDOR.CACTIVE    = '1'
                                                              AND TCONTRACT.CACTIVE  = 'Y' 
                                                              AND TTRUCK.SHEADREGISTERNO LIKE '%{0}%'
                                                              )", HeadRegisterNo.Replace("'", "''")));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region + Instance +
        private static ComplainBLL _instance;
        public static ComplainBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new ComplainBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}