﻿using System;
using TMS_DAL.Transaction.Complain;
using System.Data;
using System.Collections.Generic;

namespace TMS_BLL.Transaction.Complain
{
    public class ComplainImportFileBLL
    {
        public void ImportFileInsertBLL(int SSERVICEID, DataTable dtFile, string UserID)
        {
            try
            {
                ComplainImportFileDAL.Instance.ImportFileInsertDAL(SSERVICEID, dtFile, UserID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ImportFileSelectBLL(string DocID, string UploadType)
        {
            try
            {
                return ComplainImportFileDAL.Instance.ImportFileSelectDAL(DocID, UploadType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ImportFileSelectBLL2(string DocID, string UploadType,string UploadTypeIND)
        {
            try
            {
                return ComplainImportFileDAL.Instance.ImportFileSelectDAL2(DocID, UploadType, UploadTypeIND);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ImportFileSelectBLL3(string DocID, string UploadType, string UploadTypeIND)
        {
            try
            {
                return ComplainImportFileDAL.Instance.ImportFileSelectDAL3(DocID, UploadType, UploadTypeIND);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ImportFileSelectBLL(string DocID, string UploadType, string Condition)
        {
            try
            {
                return ComplainImportFileDAL.Instance.ImportFileSelectDAL(DocID, UploadType, Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ComplainAttachInsertBLL(DataTable dtUpload, string DocID, int CreateBy, string SSOLUTION, string SPROTECT, string SRESPONSIBLE, int DocStatusID, string CCEmail, string UploadType)
        {
            try
            {
                ComplainImportFileDAL.Instance.ComplainAttachInsertDAL(dtUpload, DocID, CreateBy, SSOLUTION, SPROTECT, SRESPONSIBLE, DocStatusID, CCEmail, UploadType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ComplainTab3SaveBLL(DataTable dtUpload, string DocID, int CreateBy, string UploadType, int CusScoreTypeID, decimal TotalPoint, decimal TotalCost, int TotalDisableDriver, DataTable dtScoreList, int DocStatusID, int CostCheck, DateTime? CostCheckDate, DataTable dtScoreListCar, bool isValiDate, decimal CostOther, int isSaveCusScore, string RemarkTab3, decimal OilLose, int Blacklist, string Sentencer, Dictionary<int, string> flagVendorDownload = null)
        {
            try
            {
                ComplainImportFileDAL.Instance.ComplainTab3SaveDAL(dtUpload, DocID, CreateBy, UploadType, CusScoreTypeID, TotalPoint, TotalCost, TotalDisableDriver, dtScoreList, DocStatusID, CostCheck, CostCheckDate, dtScoreListCar, isValiDate, CostOther, isSaveCusScore, RemarkTab3, OilLose, Blacklist, Sentencer, flagVendorDownload);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ComplainTab3AppealSaveBLL(string DocID, int CreateBy, int CusScoreTypeID, decimal TotalPoint, decimal TotalCost, int TotalDisableDriver, DataTable dtScoreList, DataTable dtScoreListCar, decimal CostOther, int isSaveCusScore, string RemarkTab3, decimal OilLose, string AppealID, int Blacklist)
        {
            try
            {
                ComplainImportFileDAL.Instance.ComplainTab3AppealSaveDAL(DocID, CreateBy, CusScoreTypeID, TotalPoint, TotalCost, TotalDisableDriver, dtScoreList, dtScoreListCar, CostOther, isSaveCusScore, RemarkTab3, OilLose, AppealID, Blacklist);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainSelectTab3ScoreBLL(string DocID)
        {
            try
            {
                return ComplainImportFileDAL.Instance.ComplainSelectTab3ScoreDAL(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainSelectTab3ScoreAppealBLL(string DocID)
        {
            try
            {
                return ComplainImportFileDAL.Instance.ComplainSelectTab3ScoreAppealDAL(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainSelectTab3ScoreDetailBLL(string DocID)
        {
            try
            {
                return ComplainImportFileDAL.Instance.ComplainSelectTab3ScoreDetailDAL(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainSelectTab3ScoreDetailPartialBLL(string DocID, string StruckID, string DeliveryDate)
        {
            try
            {
                return ComplainImportFileDAL.Instance.ComplainSelectTab3ScoreDetailPartialDAL(DocID, StruckID, DeliveryDate);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainSelectTab3ScoreCarBLL(string DocID)
        {
            try
            {
                return ComplainImportFileDAL.Instance.ComplainSelectTab3ScoreCarDAL(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainSelectTab3ScoreCarPartialBLL(string DocID, string StruckID, string DeliveryDate)
        {
            try
            {
                return ComplainImportFileDAL.Instance.ComplainSelectTab3ScoreCarPartialDAL(DocID, StruckID, DeliveryDate);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainSelectTab3ScoreDetailAppealBLL(string DocID)
        {
            try
            {
                return ComplainImportFileDAL.Instance.ComplainSelectTab3ScoreDetailAppealDAL(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ComplainSelectTab3ScoreCarAppealBLL(string DocID)
        {
            try
            {
                return ComplainImportFileDAL.Instance.ComplainSelectTab3ScoreCarAppealDAL(DocID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ComplainUpdateCostBLL(string DocID, int CostCheck, DateTime? CostCheckDate, int DocStatusID, DataTable dtUploadCost, int CreateBy, string UploadType)
        {
            try
            {
                ComplainImportFileDAL.Instance.ComplainUpdateCostDAL(DocID, CostCheck, CostCheckDate, DocStatusID, dtUploadCost, CreateBy, UploadType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static ComplainImportFileBLL _instance;
        public static ComplainImportFileBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new ComplainImportFileBLL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}