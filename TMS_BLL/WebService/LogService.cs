﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace TMS_BLL.WebService
{
	public class LogService
	{
		#region Member
		private static string sSource = "TMS WebService";
		private static string sLog = "Application"; 
		#endregion

		public static void InitLogService()
		{
			if(!EventLog.SourceExists(sSource))
				EventLog.CreateEventSource(sSource, sLog);
		}

		public static void ExceptionLog(string message)
		{
			InitLogService();
			EventLog.WriteEntry(sSource, message);
			EventLog.WriteEntry(sSource, message, EventLogEntryType.Error);
		}
	}
}
