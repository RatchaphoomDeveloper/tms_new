﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS_DAL.WebService;

namespace TMS_BLL.WebService
{
    public class WebServiceBLL
    {
        #region TestWebServiceSelect
        public DataSet TestWebServiceSelect()
        {
            try
            {
                return WebServiceDAL.Instance.TestWebServiceSelect();
            }
            catch (Exception ex)
            {
                LogService.ExceptionLog(ex.Message);
                return null;
            }
        }
        #endregion

        #region IsUserAuthen
        public bool IsUserAuthen(string user, string pass)
        {
            try
            {
                return WebServiceDAL.Instance.IsUserAuthen(user, pass);
            }
            catch (Exception ex)
            {
                LogService.ExceptionLog(ex.Message);
                return false;
            }
        }
        #endregion

        #region + Instance +
        private static WebServiceBLL _instance;
        public static WebServiceBLL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new WebServiceBLL();
                //}
                return _instance;
            }
        }
        #endregion

        #region GetUserLogin
        public DataSet GetUserLogin(int sid)
        {
            try
            {
                return WebServiceDAL.Instance.GetUserLogin(sid);
            }
            catch (Exception ex)
            {
                //LogService.ExceptionLog(ex.Message);
                //return null;
                throw ex;
            }
        }
        #endregion

        #region GetVendor
        public DataSet GetVendor()
        {
            try
            {
                return WebServiceDAL.Instance.GetVendor();
            }
            catch (Exception ex)
            {
                //LogService.ExceptionLog(ex.Message);
                //return null;
                throw ex;
            }
        }
        #endregion

        #region GetContract
        public DataSet GetContract(string vendorID)
        {
            try
            {
                return WebServiceDAL.Instance.GetContract(vendorID);
            }
            catch (Exception ex)
            {
                //LogService.ExceptionLog(ex.Message);
                //return null;
                throw ex;
            }
        }
        #endregion

        #region GetDriver
        public DataSet GetDriver(string vendorID)
        {
            try
            {
                return WebServiceDAL.Instance.GetDriver(vendorID);
            }
            catch (Exception ex)
            {
                //LogService.ExceptionLog(ex.Message);
                //return null;
                throw ex;
            }
        }
        #endregion

        #region GetDepartment
        public DataTable GetDepartment()
        {
            try
            {
                return WebServiceDAL.Instance.GetDepartment();
            }
            catch (Exception ex)
            {
                //LogService.ExceptionLog(ex.Message);
                //return null;
                throw ex;
            }
        }
        #endregion

        #region GetDivision
        public DataTable GetDivision(string departmentID = "")
        {
            try
            {
                return WebServiceDAL.Instance.GetDivision(departmentID);
            }
            catch (Exception ex)
            {
                //LogService.ExceptionLog(ex.Message);
                //return null;
                throw ex;
            }
        }
        #endregion

        #region GetWorkGroup
        public DataSet GetWorkGroup()
        {
            try
            {
                return WebServiceDAL.Instance.GetWorkGroup();
            }
            catch (Exception ex)
            {
                //LogService.ExceptionLog(ex.Message);
                //return null;
                throw ex;
            }
        }
        #endregion

        #region GetGroup
        public DataSet GetGroup(int workgroupID)
        {
            try
            {
                return WebServiceDAL.Instance.GetGroup(workgroupID);
            }
            catch (Exception ex)
            {
                //LogService.ExceptionLog(ex.Message);
                //return null;
                throw ex;
            }
        }
        #endregion

        #region GetTerminal
        public DataSet GetTerminal()
        {
            try
            {
                return WebServiceDAL.Instance.GetTerminal();
            }
            catch (Exception ex)
            {
                //LogService.ExceptionLog(ex.Message);
                //return null;
                throw ex;
            }
        }
        #endregion

        #region GetKM
        public DataSet GetKM(string keyword)
        {
            try
            {
                return WebServiceDAL.Instance.GetKM(keyword);
            }
            catch (Exception ex)
            {
                //LogService.ExceptionLog(ex.Message);
                //return null;
                throw ex;
            }
        }
        #endregion

        #region GetContentFilePath
        public DataTable GetContentFilePath(string condition)
        {
            try
            {
                return WebServiceDAL.Instance.GetContentFilePath(condition);
            }
            catch (Exception ex)
            {
                //LogService.ExceptionLog(ex.Message);
                //return null;
                throw ex;
            }
        }
        #endregion

        #region GetTruckList
        public DataSet GetTruckList(string keyword)
        {
            try
            {
                return WebServiceDAL.Instance.GetTruckList(keyword);
            }
            catch (Exception ex)
            {
                //LogService.ExceptionLog(ex.Message);
                //return null;
                throw ex;
            }
        }
        #endregion

        #region GetTruckDetail
        public DataSet GetTruckDetail(string headLicensePlate, string tailLicensePlate)
        {
            try
            {
                return WebServiceDAL.Instance.GetTruckDetail(headLicensePlate, tailLicensePlate);
            }
            catch (Exception ex)
            {
                //LogService.ExceptionLog(ex.Message);
                //return null;
                throw ex;
            }
        }
        #endregion

        #region GetTruckLog
        public DataSet GetTruckLog(string licensePlate, string licenseTrailPlate)
        {
            try
            {
                return WebServiceDAL.Instance.GetTruckLog(licensePlate, licenseTrailPlate);
            }
            catch (Exception ex)
            {
                //LogService.ExceptionLog(ex.Message);
                //return null;
                throw ex;
            }
        }
        #endregion

        #region GetShipToList
        public DataSet GetShipToList(string Condition)
        {
            try
            {
                return WebServiceDAL.Instance.GetShipToList(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetMaterial
        public DataSet GetMaterial(string Condition)
        {
            try
            {
                return WebServiceDAL.Instance.Get_Material(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetMaterialGroup
        public DataSet GetMaterialGroup(string Condition)
        {
            try
            {
                return WebServiceDAL.Instance.Get_Material_Group(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetPrvReg
        public DataSet GetPrvReg(string Condition)
        {
            try
            {
                return WebServiceDAL.Instance.GetPrvReg(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetChecker
        public DataSet GetChecker(string Condition)
        {
            try
            {
                return WebServiceDAL.Instance.Get_Checker(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region GetCheckerService
        public DataSet GetCheckerService(string Condition, string I_TEXTSEARCH)
        {
            try
            {
                return WebServiceDAL.Instance.Get_Checker_Service(Condition, I_TEXTSEARCH);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region getShiptoBySaleDistrict
        public DataSet getShiptoBySaleDistrict(string Condition)
        {
            try
            {
                return WebServiceDAL.Instance.getShiptoBySaleDistrict(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetSurpriseCheckVendorSelect
        public DataSet GetSurpriseCheckVendorSelect(string VENDORID, string I_TEXTSEARCH, string I_IS_YEAR)
        {
            try
            {
                return WebServiceDAL.Instance.GET_SURPRISECHECK_CHECKLIST_VENDOR(VENDORID, I_TEXTSEARCH, I_IS_YEAR);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetSurpriseCheckPTTSelect
        public DataSet GetSurpriseCheckPTTSelect(string I_TEXTSEARCH, string I_DATESTART, string I_TERMINALID, string I_CHECKER)
        {
            try
            {
                return WebServiceDAL.Instance.GET_SURPRISECHECK_CHECKLIST_PTT(I_TEXTSEARCH, I_DATESTART, I_TERMINALID, I_CHECKER);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetSurpriseCheckPTTConfirmSelect
        public DataSet GetSurpriseCheckPTTConfirmSelect(string I_TEXTSEARCH, string I_IS_YEAR)
        {
            try
            {
                return WebServiceDAL.Instance.GET_SURPRISECHECK_CHECKLIST_PTT_CONFIRM(I_TEXTSEARCH, I_IS_YEAR);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetSurpriseCheckChecklist
        public DataSet GetSurpriseCheckChecklist(string Condition)
        {
            try
            {
                return WebServiceDAL.Instance.GET_SURPRISECHECK_CHECKLIST(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetMasterFilter
        public DataSet GetMasterFilter(string Filter)
        {
            try
            {
                return WebServiceDAL.Instance.GetMasterFilter(Filter);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}