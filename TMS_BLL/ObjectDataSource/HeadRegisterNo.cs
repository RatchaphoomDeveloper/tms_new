﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using TMS_BLL.Transaction.Complain;
using TMS_DAL.Common;

namespace TMS_BLL.ObjectDataSource
{
    public class HeadRegisterNo
    {
        public HeadRegisterNo()
        {

        }
        public List<objHeadRegiterNo> GetList(string HeadRegisterNo, int pageSize, int startRowIndex, int maximumRows)
        {
            try
            {
                var _err = string.Empty;
                var _sb = new System.Text.StringBuilder();
                var hashtable = new System.Collections.Hashtable();
                var objHeadRegiterNo = new List<objHeadRegiterNo>();
                DataTable dt = new ComplainBLL().GetCarHEADREGISTERNO(HeadRegisterNo, ref _err, startRowIndex, pageSize);
                if (dt != null)
                    ModelUtil.CreateList<objHeadRegiterNo>(ref objHeadRegiterNo, dt);

                return objHeadRegiterNo;
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }
    }

    public class objHeadRegiterNo
    {
        public string SHEADREGISTERNO { get; set; }
        public string STRAILERREGISTERNO { get; set; }
    }

    #region Model and data handle Utilities
    public static class ModelUtil
    {
        //INTERFACE
        public static void CreateIDataTable<T>(ref DataTable datatable)
        {
            Type entityType = typeof(T);
            DataTable table = new DataTable(entityType.Name);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            foreach (PropertyDescriptor prop in properties)
            {
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            datatable = table;
            return;
        }

        //DATA
        public static void CreateObject<T>(ref T item, DataTable dataTable)
        {
            try
            {
                item = (T)Activator.CreateInstance<T>();

                if (item.GetType().IsGenericType && item is IEnumerable) throw new Exception();

                Type type = item.GetType();

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    for (int j = 0; j < dataTable.Columns.Count; j++)
                    {
                        string colName = Convert.ToString(dataTable.Columns[j]);
                        PropertyInfo prop = type.GetProperty(colName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                        if (prop == null) continue;

                        object value = dataTable.Rows[i][colName];

                        if (IsNullableType(prop.PropertyType))
                        {
                            if (value == DBNull.Value)
                                value = null;

                            prop.SetValue(item, value, null);

                            //prop.SetValue(TempObject, dataTable.Rows[i][colName], null);
                        }
                        else
                        {
                            prop.SetValue(item, Convert.ChangeType(dataTable.Rows[0][colName], prop.PropertyType), null);
                        }

                    }

                }
                return;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void CreateList<T>(ref List<T> item, DataTable dataTable)
        {
            try
            {
                item = new List<T>();

                if (item.GetType().IsGenericType && item is IEnumerable == false) throw new Exception();
                Type ItemType = item.GetType().GetGenericArguments()[0];

                #region Comment Code for Check Nullable Type

                //MemberInfo[] memberInfos = ItemType.GetMembers(BindingFlags.Public | BindingFlags.Instance);

                //foreach (MemberInfo member in memberInfos)
                //{
                //    if (member.MemberType == MemberTypes.Property)
                //    {
                //        PropertyInfo propInfo = member as PropertyInfo;
                //        if (IsNullableType(propInfo.PropertyType))
                //        {

                //        }
                //        else
                //        {

                //        }

                //    }
                //    else if (member.MemberType == MemberTypes.Field)
                //    {
                //        FieldInfo fieldInfo = member as FieldInfo;
                //    }
                //}

                #endregion

                List<T> ObjectList = new List<T>();

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    T TempObject = (T)Activator.CreateInstance<T>();
                    for (int j = 0; j < dataTable.Columns.Count; j++)
                    {
                        string colName = Convert.ToString(dataTable.Columns[j]);
                        PropertyInfo prop = ItemType.GetProperty(colName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                        if (prop == null) continue;

                        object value = dataTable.Rows[i][colName];

                        if (IsNullableType(prop.PropertyType))
                        {
                            if (value == DBNull.Value)
                                value = null;

                            prop.SetValue(TempObject, value, null);

                            //prop.SetValue(TempObject, dataTable.Rows[i][colName], null);
                        }
                        else
                        {
                            prop.SetValue(TempObject, Convert.ChangeType(dataTable.Rows[i][colName], prop.PropertyType), null);
                        }

                    }
                    ObjectList.Add(TempObject);
                }
                item = ObjectList;
                return;
            }
            catch (Exception ex)
            {
                item = null;
                throw ex;
            }
        }

        /// <summary>
        /// Function for Check Nullable Type
        /// </summary>
        /// <param name="propertyType"></param>
        /// <returns></returns>
        private static bool IsNullableType(Type propertyType)
        {
            return (propertyType.IsGenericType) && (object.ReferenceEquals(propertyType.GetGenericTypeDefinition(), typeof(Nullable<>)));
        }

    }
    #endregion
}
