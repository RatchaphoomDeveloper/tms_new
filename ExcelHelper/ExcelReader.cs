﻿using System.Data;
using GemBox.Spreadsheet;

namespace ExcelHelper
{
    public class ExcelReader
    {
        public DataTable ReadExcel(string sourceFile, int startRow = 4)
        {
            GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");

            ExcelFile ef = ExcelFile.Load(sourceFile);

            // Select the first worksheet from the file.
            ExcelWorksheet ws = ef.Worksheets[0];

            DataTable dt = ws.CreateDataTable(new CreateDataTableOptions()
            {
                ColumnHeaders = false,
                StartRow = startRow,
                NumberOfColumns = ws.Columns.Count,
                NumberOfRows = ws.Rows.Count
            });

            return dt;
        }
    }
}