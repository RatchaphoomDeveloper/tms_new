﻿using System;
using System.Net.Mail;
using System.Net;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Net.Mime;

namespace EmailHelper
{
    public static class MailService
    {
        public static void SendMail(string MailTo, string Subject, string Body, string MailCC = "", string EmailListCode = "", string ColumnEmailName = "", string PathAttach = "")
        {
            try
            {
                //MailTo = "zsuntipab.k@pttdigital.com";
                //MailTo = MailTo.Replace("suphakit.k@pttor.com", string.Empty);
                //MailTo = MailTo.Replace("nut.t@pttor.com", string.Empty);
                //MailTo = MailTo.Replace("sake.k@pttor.com", string.Empty);
                //MailTo = MailTo.Replace("thanyavit.k@pttor.com", string.Empty);
                DataTable dtEmail = TMS_DAL.Master.EmailDAL.Instance.EmailListSelect(EmailListCode);
                if (dtEmail.Rows.Count > 0)
                {
                    var listEmail = dtEmail.Select("EMAIL_TYPE = 'TO'");
                    if (listEmail.Any())
                    {
                        var mailto = listEmail.Select(it => it[ColumnEmailName] + string.Empty).ToArray();
                        MailTo += "," + string.Join(",", mailto);
                    }

                    listEmail = dtEmail.Select("EMAIL_TYPE = 'CC'");
                    if (listEmail.Any())
                    {
                        var mailto = listEmail.Select(it => it[ColumnEmailName] + string.Empty).ToArray();
                        MailCC += "," + string.Join(",", mailto);
                        if (MailCC.StartsWith(","))
                        {
                            MailCC = MailCC.Remove(0, 1);
                        }
                    }
                }
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(ConfigurationSettings.AppSettings["SMTPServer"].ToString());
                mail.From = new MailAddress(ConfigurationSettings.AppSettings["MailFrom"].ToString());
                string[] mailToList = MailTo.Split(',');
                foreach (string mailto in mailToList)
                {
                    if (!mailto.Replace(" ", string.Empty).Equals(string.Empty))
                    {
                        //if (string.Equals(mailto, "suphakit.k@pttor.com")
                        //    || string.Equals(mailto, "nut.t@pttor.com")
                        //    || string.Equals(mailto, "sake.k@pttor.com")
                        //    || string.Equals(mailto, "thanyavit.k@pttor.com"))
                        //{
                        //    mail.To.Add("TEST_" + mailto);
                        //}
                        //else
                        mail.To.Add(mailto.Trim());
                    }
                }
                //mail.To.Add(MailTo);

                if (MailCC != "")
                {
                    string[] mailCCList = MailCC.Split(',');
                    foreach (string mailcc in mailCCList)
                    {
                        if (!mailcc.Replace(" ", string.Empty).Equals(string.Empty))
                            mail.CC.Add(mailcc);
                    }
                }

                if (PathAttach != "")
                {
                    string[] attachFile = PathAttach.Split(',');
                    foreach (string att in attachFile)
                    {
                        if (!string.Equals(att, string.Empty))
                        {
                            mail.Attachments.Add(new Attachment(att, MediaTypeNames.Application.Octet));
                        }
                    }
                }

                //สำหรับ Test
                //mail.To.Clear();
                //mail.To.Add("zsuntipab.k@pttdigital.com");
                //mail.To.Add("zmaliwan.p@pttdigital.com");
                //mail.To.Add("zsarawut.b@pttdigital.com");
                //mail.To.Add("issara.s@pttdigital.com");
                mail.Subject = Subject;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                SmtpServer.Port = int.Parse(ConfigurationSettings.AppSettings["SMTPServer_Port"].ToString());
                SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationSettings.AppSettings["MailCredentials_User"].ToString(), ConfigurationSettings.AppSettings["MailCredentials_Pass"].ToString());
                SmtpServer.EnableSsl = Convert.ToBoolean(ConfigurationSettings.AppSettings["MailEnableSSL"].ToString());
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void SendBooking(DateTime Now, DateTime UtcNow, DateTime End, string Subject, string Body, string Location, string MailTo, string MailCC = "", string EmailListCode = "", string ColumnEmailName = "")
        {
            try
            {
                DataTable dtEmail = TMS_DAL.Master.EmailDAL.Instance.EmailListSelect(EmailListCode);
                if (dtEmail.Rows.Count > 0)
                {
                    var listEmail = dtEmail.Select("EMAIL_TYPE = 'TO'");
                    if (listEmail.Any())
                    {
                        var mailto = listEmail.Select(it => it[ColumnEmailName] + string.Empty).ToArray();
                        MailTo += "," + string.Join(",", mailto);
                    }

                    listEmail = dtEmail.Select("EMAIL_TYPE = 'CC'");
                    if (listEmail.Any())
                    {
                        var mailto = listEmail.Select(it => it[ColumnEmailName] + string.Empty).ToArray();
                        MailCC += "," + string.Join(",", mailto);
                        if (MailCC.StartsWith(","))
                        {
                            MailCC = MailCC.Remove(0, 1);
                        }
                    }
                }

                SmtpClient SmtpServer = new SmtpClient(ConfigurationSettings.AppSettings["SMTPServer"].ToString());
                MailMessage mail = new MailMessage();
                //mail.From = new MailAddress(ConfigurationSettings.AppSettings["MailFrom"].ToString());
                //mail.To.Add(new MailAddress("zsarawut.b@pttdigital.com"));

                mail.From = new MailAddress(ConfigurationSettings.AppSettings["MailFrom"].ToString());
                string[] mailToList = MailTo.Split(',');
                foreach (string mailto in mailToList)
                {
                    if (!mailto.Replace(" ", string.Empty).Equals(string.Empty))
                    {
                        mail.To.Add(mailto.Trim());
                    }
                }
                //mail.To.Add(MailTo);

                if (MailCC != "")
                {
                    string[] mailCCList = MailCC.Split(',');
                    foreach (string mailcc in mailCCList)
                    {
                        if (!mailcc.Replace(" ", string.Empty).Equals(string.Empty))
                            mail.CC.Add(mailcc);
                    }
                    MailTo = MailTo + "," + MailCC;
                }

                mail.Subject = Subject;
                mail.Body = Body;
                mail.IsBodyHtml = true;

                StringBuilder str = new StringBuilder();
                str.AppendLine("BEGIN:VCALENDAR");
                str.AppendLine("PRODID:-//Ahmed Abu Dagga Blog");
                str.AppendLine("VERSION:2.0");
                str.AppendLine("METHOD:REQUEST");
                str.AppendLine("BEGIN:VEVENT");
                str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", Now));
                str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", UtcNow));
                str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", End));
                str.AppendLine("LOCATION: " + Location);
                str.AppendLine(string.Format("UID:{0}", Guid.NewGuid()));
                str.AppendLine(string.Format("DESCRIPTION:{0}", mail.Body));
                str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", mail.Body));
                str.AppendLine(string.Format("SUMMARY:{0}", mail.Subject));
                str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", mail.From.Address));

                str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", mail.To[0].DisplayName, MailTo));

                str.AppendLine("BEGIN:VALARM");
                str.AppendLine("TRIGGER:-PT15M");
                str.AppendLine("ACTION:DISPLAY");
                str.AppendLine("DESCRIPTION:Reminder");
                str.AppendLine("END:VALARM");
                str.AppendLine("END:VEVENT");
                str.AppendLine("END:VCALENDAR");
                System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType("text/calendar");
                ct.Parameters.Add("method", "REQUEST");
                AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), ct);
                mail.AlternateViews.Add(avCal);

                SmtpServer.Port = int.Parse(ConfigurationSettings.AppSettings["SMTPServer_Port"].ToString());
                SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationSettings.AppSettings["MailCredentials_User"].ToString(), ConfigurationSettings.AppSettings["MailCredentials_Pass"].ToString());
                SmtpServer.EnableSsl = Convert.ToBoolean(ConfigurationSettings.AppSettings["MailEnableSSL"].ToString());
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}