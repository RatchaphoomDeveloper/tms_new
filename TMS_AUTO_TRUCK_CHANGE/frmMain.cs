﻿using System;
using System.Data;
using System.Windows.Forms;
using TMS_BLL.Transaction.Complain;
using System.Threading;
using TMS_BLL.Transaction.AutoStatus;
using TMS_BLL.Transaction.SurpriseCheck;
using TMS_BLL.Master;
using TMS.SAPHelper.Interface;
using TMS.Entity.Interface;
using System.Globalization;
using System.Linq;
using EmailHelper;

namespace TMS_AUTO_TRUCK_CHANGE
{
    public partial class frmMain : Form
    {
        DataTable dtVeh;
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitialForm();

            //เรียกใช้ตามนี้นะ
            int CreateBy = 0;
            DataTable dtResult = new DataTable();

            #region + TU +
            //TUEntity tuEntity = new TMS.Entity.Interface.TUEntity()
            //{
            //    //ใส่พารามิเตอร์ลงไปให้ครบนะ
            //    TU_ID = string.Empty,
            //    TU_MAXWGT = string.Empty,
            //    TU_NUMBER = string.Empty
            //};
            //dtResult = TU.UpdateTU(tuEntity, CreateBy);
            #endregion

            #region + Vichicle +
            //VehicleEntity vehEntity = new VehicleEntity()
            //{
            //    //ใส่พารามิเตอร์ลงไปให้ครบนะ
            //    VEH_ID = string.Empty,
            //    VEH_TEXT = string.Empty,
            //    VEH_TYPE = string.Empty
            //};
            //dtResult = Vehical.UpdateVeh(vehEntity, CreateBy);
            #endregion
        }

        private void InitialForm()
        {
            try
            {
                this.EmailTruckAge();                         //อีเมล์แจ้งเตือนรถเกิน 8 ปีล่วงหน้า
                this.EmailTruckAgeSysdate();                  //อีเมล์แจ้งระงับรถเกิน 8 ปี
                this.EmailTruckDocAge();                        //อีเมล์แจ้งเตือนเอกสารแนบหมดอายุ
                this.EmailTruckDocAgeExpire();                  //อีเมล์แจ้งระงับเอกสารแนบหมดอายุ
                this.EmailComplain();
                this.ChangeStatusComplain();
                this.ChangeStatusSurpriseCheck();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                Thread.Sleep(2000);
                Application.Exit();
            }
        }

        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            this.Show();
            WindowState = FormWindowState.Normal;
            this.BringToFront();
        }

        private void frmMain_Shown(object sender, EventArgs e)
        {
            //WindowState = FormWindowState.Minimized;
            //Hide();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                Properties.Settings.Default["ExecuteTime"] = dtpExecuteTime.Text;
                Properties.Settings.Default.Save();

                MessageBox.Show("Save Success...");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (string.Equals(DateTime.Now.ToString("HH:mm"), dtpExecuteTime.Text))
                    this.EmailTruckAgeSysdate();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmdSendNow_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการปรับสถานะ รถ ทันที ใช่หรือไม่", "ยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    this.EmailTruckAgeSysdate();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void EmailTruckAge()
        {
            try
            {
                dtVeh = CarBLL.Instance.TruckAgeExpireBLL(124);
                if (dtVeh.Rows.Count > 0)
                {
                    for (int i = 0; i < dtVeh.Rows.Count; i++)
                    {
                        this.SendEmailTruckAge(124, ComplainBLL.Instance.GetEmailComplainBLL("", "", "", 441, dtVeh.Rows[i]["STRANSPORTID"].ToString().Trim(), true, true), i);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void EmailTruckAgeSysdate()
        {
            try
            {
                dtVeh = CarBLL.Instance.TruckAgeExpireSysdateBLL(126);
                if (dtVeh.Rows.Count > 0)
                {
                    for (int i = 0; i < dtVeh.Rows.Count; i++)
                    {
                        DataTable dtResult = TMS.SAPHelper.Interface.Vehical.UpdateVeh(this.GeneateVehEntity(i, 126), 0);
                        if (dtResult.Rows.Count > 0)
                        {
                            if (!string.Equals(dtResult.Rows[0]["STATUS"].ToString(), "S"))
                            {
                                LOG.Instance.SaveLog("แก้ไขข้อมูลรถใน SAP ไม่สำเร็จ", dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim(), "441", "<span class=\"red\">" + "รถ " + dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim() + " อายุครบ 8 ปี" + "</span>", "");
                                continue;
                            }
                            else
                            {
                                LOG.Instance.SaveLog("แก้ไขข้อมูลรถใน SAP สำเร็จ", dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim(), "441", "<span class=\"red\">" + "รถ " + dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim() + " อายุครบ 8 ปี" + "</span>", "");
                            }

                            if (string.Equals(dtResult.Rows[0]["STATUS"].ToString(), "S"))
                            {
                                DataTable dtUpTruck = CarBLL.Instance.UpdateTruckExpireBLL(dtVeh.Rows[i]["STRUCKID"].ToString().Trim());
                                if (dtUpTruck.Rows.Count > 0)
                                {
                                    LOG.Instance.SaveLog("แก้ไขข้อมูลรถใน TMS สำเร็จ", dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim(), "441", "<span class=\"red\">" + "รถ " + dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim() + " อายุครบ 8 ปี" + "</span>", "");
                                    this.SendEmailTruckAge(126, ComplainBLL.Instance.GetEmailComplainBLL("", "", "", 441, dtVeh.Rows[i]["STRANSPORTID"].ToString().Trim(), true, true), i);
                                }
                                else
                                {
                                    LOG.Instance.SaveLog("แก้ไขข้อมูลรถใน TMS ไม่สำเร็จ", dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim(), "441", "<span class=\"red\">" + "รถ " + dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim() + " อายุครบ 8 ปี" + "</span>", "");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void EmailTruckDocAge()
        {
            try
            {
                string struckid = "";
                dtVeh = CarBLL.Instance.TruckDocExpireBLL(123);
                if (dtVeh.Rows.Count > 0)
                {
                    # region
                    //for (int i = 0; i < dtVeh.Rows.Count; i++)
                    //{
                    //    if (dtVeh.Rows[i]["CHECKDOC"].ToString().Trim() == "1")
                    //    {
                    //        if (dtVeh.Rows[i]["DOCEXPIRE"].ToString().Trim() == "0")
                    //        {
                    //            DataRow[] dr = dtVeh.Select("STRUCKID = '" + dtVeh.Rows[i]["STRUCKID"].ToString().Trim() + "' AND CHECKDOC = 2");
                    //            if (dr.Any())
                    //            {
                    //                foreach (DataRow row in dr)
                    //                {
                    //                    if (row["DOCEXPIRE"].ToString().Trim() == "0")
                    //                    {
                    //                        struckid = dtVeh.Rows[i]["STRUCKID"].ToString().Trim();
                    //                    }
                    //                }
                    //            }
                    //            else
                    //            {
                    //                struckid = dtVeh.Rows[i]["STRUCKID"].ToString().Trim();
                    //            }
                    //        }
                    //    }
                    //    else if (dtVeh.Rows[i]["CHECKDOC"].ToString().Trim() == "2")
                    //    {
                    //        if (dtVeh.Rows[i]["DOCEXPIRE"].ToString().Trim() == "0")
                    //        {
                    //            DataRow[] dr = dtVeh.Select("STRUCKID = '" + dtVeh.Rows[i]["STRUCKID"].ToString().Trim() + "' AND CHECKDOC = 1");
                    //            if (dr.Any())
                    //            {
                    //                foreach (DataRow row in dr)
                    //                {
                    //                    if (row["DOCEXPIRE"].ToString().Trim() == "0")
                    //                    {
                    //                        struckid = dtVeh.Rows[i]["STRUCKID"].ToString().Trim();
                    //                    }
                    //                }
                    //            }
                    //            else
                    //            {
                    //                struckid = dtVeh.Rows[i]["STRUCKID"].ToString().Trim();
                    //            }
                    //        }
                    //    }
                    //    else if (dtVeh.Rows[i]["CHECKDOC"].ToString().Trim() == "3")
                    //    {
                    //        if (dtVeh.Rows[i]["DOCEXPIRE"].ToString().Trim() == "0")
                    //            struckid = dtVeh.Rows[i]["STRUCKID"].ToString().Trim();
                    //    }

                    # endregion
                    for (int i = 0; i < dtVeh.Rows.Count; i++)
                    {
                        if (dtVeh.Rows[i]["IS_CHECK_UPLOAD_END_DATE_IND"].ToString().Trim() == "1")
                            this.SendEmailTruckDoc(123, ComplainBLL.Instance.GetEmailComplainBLL("", "", "", 441, dtVeh.Rows[i]["STRANSPORTID"].ToString().Trim(), true, true), i);
                    }
                    //if (!string.IsNullOrEmpty(struckid))
                    //{
                    //    this.SendEmailTruckDoc(123, "zsarawut.b@pttdigital.com", i);
                    //}
                    //}
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void EmailTruckDocAgeExpire()
        {
            try
            {
                string struckid = "", sap_status = "", struckid_lastup = "";
                dtVeh = CarBLL.Instance.TruckDocExpireSysdateBLL(125);
                if (dtVeh.Rows.Count > 0)
                {
                    for (int i = 0; i < dtVeh.Rows.Count; i++)
                    {
                        if (dtVeh.Rows[i]["CHECKDOC"].ToString().Trim() == "1")
                        {
                            if (dtVeh.Rows[i]["DOCEXPIRE"].ToString().Trim() == "0")
                            {
                                DataRow[] dr = dtVeh.Select("STRUCKID = '" + dtVeh.Rows[i]["STRUCKID"].ToString().Trim() + "' AND CHECKDOC = 2");
                                if (dr.Any())
                                {
                                    foreach (DataRow row in dr)
                                    {
                                        if (row["DOCEXPIRE"].ToString().Trim() == "0")
                                        {
                                            struckid = dtVeh.Rows[i]["STRUCKID"].ToString().Trim();
                                        }
                                    }
                                }
                                else
                                {
                                    struckid = dtVeh.Rows[i]["STRUCKID"].ToString().Trim();
                                }
                            }
                        }
                        else if (dtVeh.Rows[i]["CHECKDOC"].ToString().Trim() == "2")
                        {
                            if (dtVeh.Rows[i]["DOCEXPIRE"].ToString().Trim() == "0")
                            {
                                DataRow[] dr = dtVeh.Select("STRUCKID = '" + dtVeh.Rows[i]["STRUCKID"].ToString().Trim() + "' AND CHECKDOC = 1");
                                if (dr.Any())
                                {
                                    foreach (DataRow row in dr)
                                    {
                                        if (row["DOCEXPIRE"].ToString().Trim() == "0")
                                        {
                                            struckid = dtVeh.Rows[i]["STRUCKID"].ToString().Trim();
                                        }
                                    }
                                }
                                else
                                {
                                    struckid = dtVeh.Rows[i]["STRUCKID"].ToString().Trim();
                                }
                            }
                        }
                        else if (dtVeh.Rows[i]["CHECKDOC"].ToString().Trim() == "3")
                        {
                            if (dtVeh.Rows[i]["DOCEXPIRE"].ToString().Trim() == "0")
                                struckid = dtVeh.Rows[i]["STRUCKID"].ToString().Trim();
                        }

                        if (!string.IsNullOrEmpty(struckid) && struckid == dtVeh.Rows[i]["STRUCKID"].ToString().Trim() && struckid_lastup != dtVeh.Rows[i]["STRUCKID"].ToString().Trim() && dtVeh.Rows[i]["SCARTYPEID"].ToString().Trim() != "4" && dtVeh.Rows[i]["REQUIRE_DOC_ALERT_TYPE"].ToString().Trim() == "1")
                        {
                            DataTable dtResult = TMS.SAPHelper.Interface.Vehical.UpdateVeh(this.GeneateVehEntity(i, 125), 0);
                            if (dtResult.Rows.Count > 0)
                            {
                                if (!string.Equals(dtResult.Rows[0]["STATUS"].ToString(), "S"))
                                {
                                    LOG.Instance.SaveLog("แก้ไขข้อมูลรถใน SAP ไม่สำเร็จ", dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim(), "441", "<span class=\"red\">" + "เอกสารหมดอายุ" + "</span>", "");
                                    continue;
                                }
                                else
                                {
                                    sap_status = dtResult.Rows[0]["STATUS"].ToString();
                                    LOG.Instance.SaveLog("แก้ไขข้อมูลรถใน SAP สำเร็จ", dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim(), "441", "<span class=\"red\">" + "เอกสารหมดอายุ" + "</span>", "");
                                }
                            }
                            struckid_lastup = dtVeh.Rows[i]["STRUCKID"].ToString().Trim();
                        }

                        if ((!string.IsNullOrEmpty(sap_status) && struckid == dtVeh.Rows[i]["STRUCKID"].ToString().Trim() && sap_status == "S" && dtVeh.Rows[i]["REQUIRE_DOC_ALERT_TYPE"].ToString().Trim() == "1") || (dtVeh.Rows[i]["SCARTYPEID"].ToString().Trim() == "4" && dtVeh.Rows[i]["REQUIRE_DOC_ALERT_TYPE"].ToString().Trim() == "1" && struckid == dtVeh.Rows[i]["STRUCKID"].ToString().Trim()))
                        {
                            DataTable dtUpTruck = CarBLL.Instance.UpdateTruckExpireBLL(dtVeh.Rows[i]["STRUCKID"].ToString().Trim());
                            if (dtUpTruck.Rows.Count > 0)
                            {
                                LOG.Instance.SaveLog("แก้ไขข้อมูลรถใน TMS สำเร็จ", dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim(), "441", "<span class=\"red\">" + "เอกสาร " + dtVeh.Rows[i]["UPLOAD_NAME"].ToString().Trim() + " หมดอายุ" + "</span>", "");
                                if (dtVeh.Rows[i]["IS_CHECK_UPLOAD_END_DATE_IND"].ToString().Trim() == "1")
                                    this.SendEmailTruckDoc(125, ComplainBLL.Instance.GetEmailComplainBLL("", "", "", 441, dtVeh.Rows[i]["STRANSPORTID"].ToString().Trim(), true, true), i);
                            }
                            else
                            {
                                LOG.Instance.SaveLog("แก้ไขข้อมูลรถใน TMS ไม่สำเร็จ", dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim(), "441", "<span class=\"red\">" + "เอกสาร " + dtVeh.Rows[i]["UPLOAD_NAME"].ToString().Trim() + " หมดอายุ" + "</span>", "");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private VehicleEntity GeneateVehEntity(int i, int temp)
        {
            try
            {
                DataTable dtItem = new DataTable();
                dtItem.Columns.Add("VEHICLE");
                dtItem.Columns.Add("SEQ_NMBR");
                dtItem.Columns.Add("TU_NUMBER");

                dtItem.Rows.Add((dtVeh.Rows[i]["SHEADREGISTERNO"] + string.Empty).Trim(), ((i + 1) + string.Empty).Trim(), (dtVeh.Rows[i]["SCARTYPEID"] + string.Empty).Trim() == "0" ? (dtVeh.Rows[i]["SHEADREGISTERNO"] + string.Empty).Trim() : (dtVeh.Rows[i]["STRAILERREGISTERNO"] + string.Empty).Trim());
                VehicleEntity vehEntity = new VehicleEntity
                {
                    VEHICLE = (dtVeh.Rows[i]["SHEADREGISTERNO"] + string.Empty).Trim(),
                    CARRIER = (dtVeh.Rows[i]["STRANSPORTID"] + string.Empty).Trim() == "" ? "PF0015" : (dtVeh.Rows[i]["STRANSPORTID"] + string.Empty).Trim(),
                    CLASS_GRP = temp == 126 ? (dtVeh.Rows[i]["CLASSGRP"] + string.Empty).Trim().Substring(0, 6) + "1" : (dtVeh.Rows[i]["CLASSGRP"] + string.Empty).Trim(),
                    DEPOT = (dtVeh.Rows[i]["DEPOT"] + string.Empty).Trim() == "" ? "#!" : (dtVeh.Rows[i]["DEPOT"] + string.Empty).Trim(),
                    REG_DATE = (dtVeh.Rows[i]["DREGISTER"] + string.Empty).Trim() == "" ? "1900-01-01" : DateTimeCheck((dtVeh.Rows[i]["DREGISTER"] + string.Empty).Trim()),
                    ROUTE = (dtVeh.Rows[i]["ROUTE"] + string.Empty).Trim() == "" ? "ZTD001" : (dtVeh.Rows[i]["ROUTE"] + string.Empty).Trim(),
                    VEH_ID = "#!",//VEH_ID = row["VEH_ID"] + "",
                    TPPOINT = (dtVeh.Rows[i]["TPPOINT"] + string.Empty).Trim() == "" ? "#!" : (dtVeh.Rows[i]["TPPOINT"] + string.Empty).Trim(),
                    VEH_STATUS = (dtVeh.Rows[i]["CACTIVE"] + string.Empty).Trim() == "" ? " " : ((dtVeh.Rows[i]["CACTIVE"] + string.Empty).Trim() == "Y" ? "1" : "2"),
                    VEH_TEXT = (dtVeh.Rows[i]["VEH_TEXT"] + string.Empty).Trim() == "" ? "XX(0,0,0,0,0,0)" : (dtVeh.Rows[i]["VEH_TEXT"] + string.Empty).Trim(),
                    VHCLSIGN = "B4",
                    TU_ITEM = dtItem,
                };
                return vehEntity;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void EmailComplain()
        {
            try
            {
                DataTable dt = ComplainBLL.Instance.ComplainSelectBLL(" AND DOC_STATUS_ID in ('1','2')");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["IS_URGENT"].ToString() == "1")
                    {
                        if (DateTime.ParseExact(dt.Rows[i]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) == DateTime.ParseExact(dt.Rows[i]["COMPLAIN_VENDOR_DELAY"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo))
                        {
                            DataTable dtReqDoc = new DataTable();
                            dtReqDoc.Columns.Add("LINK");
                            dtReqDoc.Columns.Add("VENDOR");
                            dtReqDoc.Columns.Add("COMPLAIN");
                            dtReqDoc.Columns.Add("CONTRACT");
                            dtReqDoc.Columns.Add("CAR");
                            dtReqDoc.Columns.Add("SOURCE");
                            dtReqDoc.Columns.Add("COMPLAIN_DEPARTMENT");
                            dtReqDoc.Columns.Add("CREATE_DEPARTMENT");
                            string Link = "https://hq-srvtst-s08/tms_dup/" + "admin_Complain_add.aspx?Doc=" + dt.Rows[i]["DOC_ID"].ToString();// Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/Pages/Other/" + "admin_Complain_ReqDoc.aspx?DocID=" + MachineKey.Encode(Encoding.UTF8.GetBytes(DocID), MachineKeyProtection.All);
                            dtReqDoc.Rows.Add(Link, dt.Rows[i]["SVENDORID"].ToString(), dt.Rows[i]["DOC_ID"].ToString(), dt.Rows[i]["CONTRACT_NAME"].ToString(), dt.Rows[i]["SHEADREGISTERNO"].ToString(), dt.Rows[i]["WAREHOUSE_TO"].ToString(), dt.Rows[i]["SCOMPLAINFNAME"].ToString(), dt.Rows[i]["FULLNAME"].ToString());
                            this.SendEmail(11, dtReqDoc, dt.Rows[i]["CC_EMAIL"].ToString());
                        }
                        if (DateTime.ParseExact(dt.Rows[i]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) == DateTime.ParseExact(dt.Rows[i]["CHECK_DATE_FORWARD"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo))
                        {
                            DataTable dtReqDoc = new DataTable();
                            dtReqDoc.Columns.Add("LINK");
                            dtReqDoc.Columns.Add("VENDOR");
                            dtReqDoc.Columns.Add("COMPLAIN");
                            dtReqDoc.Columns.Add("CONTRACT");
                            dtReqDoc.Columns.Add("CAR");
                            dtReqDoc.Columns.Add("SOURCE");
                            dtReqDoc.Columns.Add("COMPLAIN_DEPARTMENT");
                            dtReqDoc.Columns.Add("CREATE_DEPARTMENT");
                            string Link = "https://hq-srvtst-s08/tms_dup/" + "admin_Complain_add.aspx?Doc=" + dt.Rows[i]["DOC_ID"].ToString();// Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/Pages/Other/" + "admin_Complain_ReqDoc.aspx?DocID=" + MachineKey.Encode(Encoding.UTF8.GetBytes(DocID), MachineKeyProtection.All);
                            dtReqDoc.Rows.Add(Link, dt.Rows[i]["SVENDORID"].ToString(), dt.Rows[i]["DOC_ID"].ToString(), dt.Rows[i]["CONTRACT_NAME"].ToString(), dt.Rows[i]["SHEADREGISTERNO"].ToString(), dt.Rows[i]["WAREHOUSE_TO"].ToString(), dt.Rows[i]["SCOMPLAINFNAME"].ToString(), dt.Rows[i]["FULLNAME"].ToString());
                            this.SendEmail(11, dtReqDoc, dt.Rows[i]["CC_EMAIL"].ToString());
                        }
                    }
                    else
                    {
                        if (DateTime.ParseExact(dt.Rows[i]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) == DateTime.ParseExact(dt.Rows[i]["COMPLAIN_VENDOR_NORMAL"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo))
                        {
                            DataTable dtReqDoc = new DataTable();
                            dtReqDoc.Columns.Add("LINK");
                            dtReqDoc.Columns.Add("VENDOR");
                            dtReqDoc.Columns.Add("COMPLAIN");
                            dtReqDoc.Columns.Add("CONTRACT");
                            dtReqDoc.Columns.Add("CAR");
                            dtReqDoc.Columns.Add("SOURCE");
                            dtReqDoc.Columns.Add("COMPLAIN_DEPARTMENT");
                            dtReqDoc.Columns.Add("CREATE_DEPARTMENT");
                            string Link = "";// Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/Pages/Other/" + "admin_Complain_ReqDoc.aspx?DocID=" + MachineKey.Encode(Encoding.UTF8.GetBytes(DocID), MachineKeyProtection.All);
                            dtReqDoc.Rows.Add(Link, dt.Rows[i]["SVENDORID"].ToString(), dt.Rows[i]["DOC_ID"].ToString(), dt.Rows[i]["CONTRACT_NAME"].ToString(), dt.Rows[i]["SHEADREGISTERNO"].ToString(), dt.Rows[i]["WAREHOUSE_TO"].ToString(), dt.Rows[i]["SCOMPLAINFNAME"].ToString(), dt.Rows[i]["FULLNAME"].ToString());
                            this.SendEmail(11, dtReqDoc, dt.Rows[i]["CC_EMAIL"].ToString());
                        }
                        if (DateTime.ParseExact(dt.Rows[i]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) == DateTime.ParseExact(dt.Rows[i]["CHECK_DATE_FORWARD"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo))
                        {
                            DataTable dtReqDoc = new DataTable();
                            dtReqDoc.Columns.Add("LINK");
                            dtReqDoc.Columns.Add("VENDOR");
                            dtReqDoc.Columns.Add("COMPLAIN");
                            dtReqDoc.Columns.Add("CONTRACT");
                            dtReqDoc.Columns.Add("CAR");
                            dtReqDoc.Columns.Add("SOURCE");
                            dtReqDoc.Columns.Add("COMPLAIN_DEPARTMENT");
                            dtReqDoc.Columns.Add("CREATE_DEPARTMENT");
                            string Link = "";// Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/Pages/Other/" + "admin_Complain_ReqDoc.aspx?DocID=" + MachineKey.Encode(Encoding.UTF8.GetBytes(DocID), MachineKeyProtection.All);
                            dtReqDoc.Rows.Add(Link, dt.Rows[i]["SVENDORID"].ToString(), dt.Rows[i]["DOC_ID"].ToString(), dt.Rows[i]["CONTRACT_NAME"].ToString(), dt.Rows[i]["SHEADREGISTERNO"].ToString(), dt.Rows[i]["WAREHOUSE_TO"].ToString(), dt.Rows[i]["SCOMPLAINFNAME"].ToString(), dt.Rows[i]["FULLNAME"].ToString());
                            this.SendEmail(11, dtReqDoc, dt.Rows[i]["CC_EMAIL"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void ChangeStatusComplain()
        {
            try
            {
                DataTable dtChange = ComplainBLL.Instance.ComplainSelectBLL(" AND DOC_STATUS_ID IN ('1','2')");
                for (int i = 0; i < dtChange.Rows.Count; i++)
                {
                    if (dtChange.Rows[i]["IS_URGENT"].ToString() == "1")
                    {
                        if ((DateTime.ParseExact(dtChange.Rows[i]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) > DateTime.ParseExact(dtChange.Rows[i]["COMPLAIN_PTT_DELAY"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) && (int.Parse(dtChange.Rows[i]["COUNT_DOC"].ToString()) > 0)))
                        {
                            ComplainBLL.Instance.ComplainChangeStatusBLL(dtChange.Rows[i]["DOC_ID"].ToString());

                            if (dtChange.Rows[i]["LOCK_DRIVER"].ToString() == "1")
                                this.SendEmail(2, dtChange, dtChange.Rows[i]["CC_EMAIL"].ToString());
                            else
                                this.SendEmail(1, dtChange, dtChange.Rows[i]["CC_EMAIL"].ToString());
                        }
                        else if ((DateTime.ParseExact(dtChange.Rows[i]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) > DateTime.ParseExact(dtChange.Rows[i]["COMPLAIN_PTT_NORMAL"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo)) && (int.Parse(dtChange.Rows[i]["COUNT_DOC"].ToString()) == 0))
                        {
                            ComplainBLL.Instance.ComplainChangStatusBLL(dtChange.Rows[i]["DOC_ID"].ToString());
                        }
                    }
                    else
                    {
                        if ((DateTime.ParseExact(dtChange.Rows[i]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) > DateTime.ParseExact(dtChange.Rows[i]["COMPLAIN_PTT_NORMAL"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo)) && (int.Parse(dtChange.Rows[i]["COUNT_DOC"].ToString()) > 0))
                        {
                            ComplainBLL.Instance.ComplainChangeStatusBLL(dtChange.Rows[i]["DOC_ID"].ToString());

                            if (dtChange.Rows[i]["LOCK_DRIVER"].ToString() == "1")
                                this.SendEmail(2, dtChange, dtChange.Rows[i]["CC_EMAIL"].ToString());
                            else
                                this.SendEmail(1, dtChange, dtChange.Rows[i]["CC_EMAIL"].ToString());
                        }
                        else if ((DateTime.ParseExact(dtChange.Rows[i]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) > DateTime.ParseExact(dtChange.Rows[i]["COMPLAIN_PTT_NORMAL"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo)) && (int.Parse(dtChange.Rows[i]["COUNT_DOC"].ToString()) == 0))
                        {
                            ComplainBLL.Instance.ComplainChangStatusBLL(dtChange.Rows[i]["DOC_ID"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void ChangeStatusSurpriseCheck()
        {
            try
            {
                DataTable dtListCar = SurpriseCheckBLL.Instance.SurpriseCheckCarSelect(" AND TO_CHAR(DEND_LIST,'DD/MM/YYYY') = '" + DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "'");
                dtVeh = dtListCar.Copy();
                if (dtListCar.Rows.Count > 0)
                {
                    for (int i = 0; i < dtVeh.Rows.Count; i++)
                    {
                        DataTable dtResult = TMS.SAPHelper.Interface.Vehical.UpdateVeh(this.GeneateVehEntity(i, 0), 0);
                        if (dtResult.Rows.Count > 0)
                        {
                            if (!string.Equals(dtResult.Rows[0]["STATUS"].ToString(), "S"))
                            {
                                LOG.Instance.SaveLog("แก้ไขข้อมูลรถใน SAP ไม่สำเร็จ", dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim(), "441", "<span class=\"red\">" + "รถ " + dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim() + " ระงับกรณีไม่แก้ไขรถที่โดน Surprisecheck ในเวลาที่กำหนด" + "</span>", "");
                                continue;
                            }
                            else
                            {
                                LOG.Instance.SaveLog("แก้ไขข้อมูลรถใน SAP สำเร็จ", dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim(), "441", "<span class=\"red\">" + "รถ " + dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim() + " ระงับกรณีไม่แก้ไขรถที่โดน Surprisecheck ในเวลาที่กำหนด" + "</span>", "");
                            }

                            if (string.Equals(dtResult.Rows[0]["STATUS"].ToString(), "S"))
                            {
                                DataTable dtUpTruck = CarBLL.Instance.UpdateTruckExpireBLL(dtVeh.Rows[i]["STRUCKID"].ToString().Trim());
                                if (dtUpTruck.Rows.Count > 0)
                                {
                                    LOG.Instance.SaveLog("แก้ไขข้อมูลรถใน TMS สำเร็จ", dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim(), "441", "<span class=\"red\">" + "รถ " + dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim() + " ระงับกรณีไม่แก้ไขรถที่โดน Surprisecheck ในเวลาที่กำหนด" + "</span>", "");
                                    this.SendEmailTruckAge(126, ComplainBLL.Instance.GetEmailComplainBLL("", "", "", 441, dtVeh.Rows[i]["STRANSPORTID"].ToString().Trim(), true, true), i);
                                }
                                else
                                {
                                    LOG.Instance.SaveLog("แก้ไขข้อมูลรถใน TMS ไม่สำเร็จ", dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim(), "441", "<span class=\"red\">" + "รถ " + dtVeh.Rows[i]["SHEADREGISTERNO"].ToString().Trim() + " ระงับกรณีไม่แก้ไขรถที่โดน Surprisecheck ในเวลาที่กำหนด" + "</span>", "");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void SendEmail(int TemplateID, DataTable dt, string CC)
        {
            //return; // ไม่ต้องส่ง E-mail

            try
            {
                DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
                DataTable dtComplainEmail = new DataTable();
                if (dtTemplate.Rows.Count > 0)
                {
                    string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                    string Body = dtTemplate.Rows[0]["BODY"].ToString();

                    if (TemplateID == 1)
                    {
                        dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(dt.Rows[0]["COMPLAIN"].ToString());

                        Subject = Subject.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());

                        Body = Body.Replace("{CAR}", dtComplainEmail.Rows[0]["CAR"].ToString());
                        Body = Body.Replace("{DRIVER}", dtComplainEmail.Rows[0]["DRIVER"].ToString());
                        Body = Body.Replace("{VENDOR}", dtComplainEmail.Rows[0]["VENDOR"].ToString());
                        Body = Body.Replace("{CONTRACT}", dtComplainEmail.Rows[0]["CONTRACT"].ToString());
                        Body = Body.Replace("{SOURCE}", dtComplainEmail.Rows[0]["WAREHOUSE"].ToString());
                        Body = Body.Replace("{COMPLAIN}", dtComplainEmail.Rows[0]["COMPLAIN"].ToString());
                        Body = Body.Replace("{COMPLAIN_DEPARTMENT}", dtComplainEmail.Rows[0]["COMPLAIN_DEPARTMENT"].ToString());
                        Body = Body.Replace("{CREATE_DEPARTMENT}", dtComplainEmail.Rows[0]["CREATE_DEPARTMENT"].ToString());
                        string Link = "https://hq-srvtst-s08/tms_dup/" + "admin_Complain_add.aspx?Doc=" + dtComplainEmail.Rows[0]["DOCID"].ToString();
                        Body = Body.Replace("{LINK}", "<a href=\"" + Link + "\">คลิกที่นี่</a>");

                        string CCEmail = CC;
                        string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;

                        MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, 1100, dtComplainEmail.Rows[0]["VENDORID"].ToString(), false, true) + CCEmail + CreateMail, Subject, Body);
                    }
                    else if (TemplateID == 2)
                    {
                        dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(dt.Rows[0]["COMPLAIN"].ToString());

                        Subject = Subject.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());

                        Body = Body.Replace("{CAR}", dtComplainEmail.Rows[0]["CAR"].ToString());
                        Body = Body.Replace("{DRIVER}", dtComplainEmail.Rows[0]["DRIVER"].ToString());
                        Body = Body.Replace("{VENDOR}", dtComplainEmail.Rows[0]["VENDOR"].ToString());
                        Body = Body.Replace("{CONTRACT}", dtComplainEmail.Rows[0]["CONTRACT"].ToString());
                        Body = Body.Replace("{SOURCE}", dtComplainEmail.Rows[0]["WAREHOUSE"].ToString());
                        Body = Body.Replace("{COMPLAIN}", dtComplainEmail.Rows[0]["COMPLAIN"].ToString());
                        Body = Body.Replace("{COMPLAIN_DEPARTMENT}", dtComplainEmail.Rows[0]["COMPLAIN_DEPARTMENT"].ToString());
                        Body = Body.Replace("{CREATE_DEPARTMENT}", dtComplainEmail.Rows[0]["CREATE_DEPARTMENT"].ToString());
                        Body = Body.Replace("{LOCK_DATE}", dtComplainEmail.Rows[0]["LOCK_DATE"].ToString());
                        string Link = "https://hq-srvtst-s08/tms_dup/" + "admin_Complain_add.aspx?Doc=" + dtComplainEmail.Rows[0]["DOCID"].ToString();
                        Body = Body.Replace("{LINK}", "<a href=\"" + Link + "\">คลิกที่นี่</a>");

                        string CCEmail = CC;
                        string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;

                        MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, 1100, dtComplainEmail.Rows[0]["VENDORID"].ToString(), false, true) + CCEmail + CreateMail, Subject, Body);
                    }
                    else if (TemplateID == 11)
                    {
                        dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(dt.Rows[0]["COMPLAIN"].ToString());

                        Subject = Subject.Replace("{DOCID}", dt.Rows[0]["COMPLAIN"].ToString());
                        Body = Body.Replace("{DOCID}", dt.Rows[0]["COMPLAIN"].ToString());
                        Body = Body.Replace("{VENDOR}", dt.Rows[0]["VENDOR"].ToString());
                        Body = Body.Replace("{COMPLAIN}", dt.Rows[0]["COMPLAIN"].ToString());
                        Body = Body.Replace("{CONTRACT}", dt.Rows[0]["CONTRACT"].ToString());
                        Body = Body.Replace("{CAR}", dt.Rows[0]["CAR"].ToString());
                        Body = Body.Replace("{SOURCE}", dt.Rows[0]["SOURCE"].ToString());
                        Body = Body.Replace("{COMPLAIN_DEPARTMENT}", dt.Rows[0]["COMPLAIN_DEPARTMENT"].ToString());
                        Body = Body.Replace("{CREATE_DEPARTMENT}", dt.Rows[0]["CREATE_DEPARTMENT"].ToString());
                        string Link = dt.Rows[0]["LINK"].ToString();
                        Body = Body.Replace("{LINK}", "<a href=\"" + Link + "\">คลิกที่นี่</a>");

                        string CCEmail = CC;
                        string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;

                        MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, 1100, dtComplainEmail.Rows[0]["VENDORID"].ToString(), false, true) + CCEmail + CreateMail, Subject, Body);
                    }
                }
            }
            catch (Exception ex)
            {
                //alertFail(ex.Message);
            }
        }

        private void SendEmailTruckAge(int TemplateID, string Txt, int row)
        {
            try
            {
                DataSet dsSendEmail = new DataSet();
                DataTable dt = new DataTable();
                dt.Columns.Add("EMAIL");
                string EmailList = string.Empty;

                DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
                DataTable dtEmail = new DataTable();
                if (dtTemplate.Rows.Count > 0)
                {
                    string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                    string Body = dtTemplate.Rows[0]["BODY"].ToString();

                    Body = Body.Replace("{Vendor}", dtVeh.Rows[row]["SABBREVIATION"].ToString().Trim());
                    Body = Body.Replace("{LicenseNo}", dtVeh.Rows[row]["SHEADREGISTERNO"].ToString().Trim());
                    if (TemplateID == 124)
                        Body = Body.Replace("{DayLeft}", dtVeh.Rows[row]["DAYEXPIRE"].ToString().Trim());
                    Body = Body.Replace("{ExpiredDate}", dtVeh.Rows[row]["TRUCKEXPIREDATE"].ToString().Trim());

                    dt.Rows.Add("");
                    MailService.SendMail(Txt, Subject, Body, "", "VendorEmployeeAdd", "EMAIL");

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void SendEmailTruckDoc(int TemplateID, string Txt, int row)
        {
            try
            {
                DataSet dsSendEmail = new DataSet();
                DataTable dt = new DataTable();
                dt.Columns.Add("EMAIL");
                string EmailList = string.Empty;

                DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
                DataTable dtEmail = new DataTable();
                if (dtTemplate.Rows.Count > 0)
                {
                    string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                    string Body = dtTemplate.Rows[0]["BODY"].ToString();

                    Body = Body.Replace("{Vendor}", dtVeh.Rows[row]["SABBREVIATION"].ToString().Trim());
                    Body = Body.Replace("{DocType}", dtVeh.Rows[row]["UPLOAD_NAME"].ToString().Trim());
                    Body = Body.Replace("{LicenseNo}", dtVeh.Rows[row]["SHEADREGISTERNO"].ToString().Trim());
                    if (TemplateID == 123)
                        Body = Body.Replace("{DayLeft}", dtVeh.Rows[row]["DAYEXPIRE"].ToString().Trim());
                    Body = Body.Replace("{ExpiredDate}", dtVeh.Rows[row]["DOCEXPIREDATE"].ToString().Trim());

                    dt.Rows.Add("");
                    MailService.SendMail(Txt, Subject, Body, "", "VendorEmployeeAdd", "EMAIL");

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private string DateTimeCheck(string sDate)
        {
            string Date_Result = "1900-01-01";

            if (!string.IsNullOrEmpty(sDate))
            {
                if (Convert.ToDateTime(sDate).Year > 1500)
                {
                    Date_Result = Convert.ToDateTime(sDate).ToString("yyyy-MM-dd", new CultureInfo("en-US"));
                }
            }

            return Date_Result;
        }

    }
}