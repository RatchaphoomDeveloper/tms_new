﻿using System;
using System.Data;
using System.Windows.Forms;
using TMS_BLL.Transaction.Complain;
using System.Threading;
using TMS_BLL.Transaction.AutoStatus;
using TMS_BLL.Master;
using EmailHelper;

namespace TMS_DRIVER_JOB
{
    public partial class frmMain : Form
    {
        DataTable dtDriverExpire;
        public static int VendorEmployeeDocumentExpired = 121;//แจ้งเรื่องอายุเอกสารหมดอายุ
        public frmMain()
        {
             InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitialForm();
        }

        private void InitialForm()
        {
            try
            {
                this.DriverCardExpire();              //อีเมล์แจ้งเตือน เอกสารหมดอายุ 
                this.DriverCardExpireAndInactive();   //อีเมล์แจ้งเตือน เอกสารหมดอายุ และ ระงับ พขร. 
               
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                Thread.Sleep(2000);
                Application.Exit();
            }
        }

        private void DriverCardExpire()
        {
            try
            {
                dtDriverExpire = UserBLL.Instance.DocExpireBLL(VendorEmployeeDocumentExpired);
                for (int i = 0; i < dtDriverExpire.Rows.Count; i++)
                {
                    //  public static int VendorEmployeeDocumentExpired = 121;//แจ้งเรื่องอายุเอกสารหมดอายุ
                    DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(VendorEmployeeDocumentExpired);
                    DataTable dtComplainEmail = new DataTable();
                    if (dtTemplate.Rows.Count > 0)
                    {
                        //MailService.SendMail(9, 2, string.Empty, string.Empty, dtCard);
                        string getname = dtTemplate.Rows[0]["STATUS"].ToString();
                        string EmailList = "zsupachai.p@pttdigital.com"; //ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, 0, "", false, false);
                        //string EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dr["SVENDORID"] + string.Empty, false, false);
                        string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                        string Body = dtTemplate.Rows[0]["BODY"].ToString();
                        //MailService.SendMail(EmailList, Subject, Body);
                        //MailService.SendMail("suphakit.k@pttplc.com" + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), Session["SVDID"].ToString(), true, false), Subject, Body);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void DriverCardExpireAndInactive()
        {
            try
            {
                dtDriverExpire = UserBLL.Instance.DocExpireAndInactiveBLL(VendorEmployeeDocumentExpired);
                for (int i = 0; i < dtDriverExpire.Rows.Count; i++)
                {
                    //Step1 Update EmpStatus (All)                    
                    updateDriverStatus(dtDriverExpire);

                    //Step2 Send Email
                    DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(VendorEmployeeDocumentExpired);
                    DataTable dtComplainEmail = new DataTable();
                    if (dtTemplate.Rows.Count > 0)
                    {
                        // MailService.SendMail(9, 2, string.Empty, string.Empty, dtCard);
                        // string EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, 0, "", false, false);
                        string EmailList = "zsupachai.p@pttdigital.com,";
                        string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                        string Body = dtTemplate.Rows[0]["BODY"].ToString();
                        //MailService.SendMail(EmailList, Subject, Body);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void updateDriverStatus(DataTable dtDriver)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string result = string.Empty;
                string[] resultCheck;

                //DataTable dtDriver = ComplainBLL.Instance.GetDriverConfictBLL();
                //dtDriver.Columns.Add("STATUS");
                //dtDriver.Columns.Add("MESSAGE");
                for (int i = 0; i < dtDriver.Rows.Count; i++)
                {
                    string DriverStatus = string.Empty;
                    DriverStatus = "1";//ระงับ พขร
                    //if (string.Equals(dtDriver.Rows[i]["DRVSTATUS_TMS"].ToString(), "1"))
                    //    DriverStatus = " ";
                    //else if (string.Equals(dtDriver.Rows[i]["DRVSTATUS_TMS"].ToString(), "0"))
                    //    DriverStatus = "1";
                    //else if (string.Equals(dtDriver.Rows[i]["DRVSTATUS_TMS"].ToString(), "2"))
                    //    DriverStatus = "2";

                    try
                    {
                        SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                        {
                            //DRIVER_CODE = dtDriver.Rows[i]["DRIVER_CODE"].ToString(),
                            //PERS_CODE = dtDriver.Rows[i]["PERS_CODE"].ToString(),
                            //LICENSE_NO = dtDriver.Rows[i]["LICENSE_NO"].ToString(),
                            //LICENSENOE_FROM = dtDriver.Rows[i]["VALID_FROM"].ToString(),
                            //LICENSENO_TO = dtDriver.Rows[i]["VALID_TO"].ToString(),
                            //Phone_1 = (string.Equals(dtDriver.Rows[i]["STEL"].ToString(), string.Empty)) ? "-" : dtDriver.Rows[i]["STEL"].ToString(),
                            //Phone_2 = (string.Equals(dtDriver.Rows[i]["STEL2"].ToString(), string.Empty)) ? "-" : dtDriver.Rows[i]["STEL2"].ToString(),
                            //FIRST_NAME = dtDriver.Rows[i]["FIRST_NAME"].ToString(),
                            //LAST_NAME = dtDriver.Rows[i]["LAST_NAME"].ToString(),
                            //CARRIER = dtDriver.Rows[i]["STRANS_ID"].ToString(),
                            //DRV_STATUS = DriverStatus

                            DRIVER_CODE = "5500006159",
                            PERS_CODE = "1251288843327",
                            LICENSE_NO = "สบ.00227/53",
                            LICENSENOE_FROM = "25/01/2016",
                            LICENSENO_TO = "25/02/2017",
                            Phone_1 = "088",
                            Phone_2 = "089",
                            FIRST_NAME = "sakchai",
                            LAST_NAME = "weaweawe",
                            CARRIER = "0010001778",
                            DRV_STATUS = "1"
                        };

                        result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                        resultCheck = result.Split(':');

                        dtDriver.Rows[i]["STATUS"] = resultCheck[0];
                        if (string.Equals(dtDriver.Rows[i]["STATUS"].ToString(), "Y"))
                            dtDriver.Rows[i]["MESSAGE"] = string.Empty;
                        else
                            dtDriver.Rows[i]["MESSAGE"] = resultCheck[1];
                    }
                    catch (Exception ex)
                    {
                        dtDriver.Rows[i]["STATUS"] = "N";
                        dtDriver.Rows[i]["MESSAGE"] = ex.Message;
                    }
                }

                AutoStatusBLL.Instance.DriverLogInsertBLL(dtDriver);
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

    
    }
}