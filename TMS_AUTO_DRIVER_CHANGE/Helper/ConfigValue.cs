﻿
namespace TMS_AUTO_DRIVER_CHANGE.Helper
{
    public static class ConfigValue
    {
        public static string DriverEnable = " ";                                             //ใช้งาน
        public static string DriverDisable = "1";                                            //ระงับชั่วคราว
        public static string DriverOut = "2";                                                //Blacklist

        public static int EmailComplainCreateNoLock = 15;                               //แจ้งเรื่องร้องเรียน ไม่ระงับ พขร.
        public static int EmailComplainCreateLock = 16;                               //แจ้งเรื่องร้องเรียน ระงับ พขร.
        public static int EmailReportMonthlyNotPass = 6;                         //ผลการตรวจสอบรายงาน (ไม่ผ่าน)
        public static int EmailComplainRequest = 7;                              //เรื่องร้องเรียน ขอเอกสารเพิ่มเติม
        public static int EmailKnowledgeManagement = 122;                              //เรื่อง Knowledge management
        public static int EmailTruckAdd = 31;                                     // เพิ่มอีเมลล์แจ้งเพิ่มรถ
        public static int EmailTruckEdit = 32;                                    // แก้ไขข้อมูลรถ
        public static int EmailTruckComment = 33;                                    // ขอข้อมูลเพิ่มเติมม
        public static int EmailTruckNoApprove = 34;                                    // ไม่อนุม้ติรถ
        public static int EmailTruckApprove = 35;                                    // อนุม้ติรถ
        public static int EmailVehicleAdd = 36;                                    // เพิ่มการผูกรถ
        public static int EmailVehicleApprove = 37;                                    // เพิ่มอีเมลล์อนุมัติการผูกรถ
        public static int EmailVehicleNoApprove = 38;                                    // เพิ่มการไม่อนุมัติการผูกรถ
        public static int EmailStopCar = 39;                                   //รถระงับชั่วคราว
        public static int EmailApproveCar = 40;                                     //แจ้งอนุญาติใช้รถ
        public static int EmailBlacklistCar = 41;                                     //แจ้งBlacklist
        public static int EmailComplainCusScore = 18;                                       //พิจารณาตัดคะแนนเรียบร้อย
        public static int EmailComplainAppeal = 19;                                       //ผู้ขนส่งยื่นอุทธรณ์
        public static int EmailComplainAppealConfirm = 20;                                       //แจ้งผลการอุทธรณ์
        public static int EmailNotSendTruck = 42;                                       //แจ้งเตือนผู้ขนส่งไม่ยืนยันรถ

        public static string GetClickHere(string URL)
        {
            return "<a href=\"" + URL + "\">คลิกที่นี่</a>";
        }


        //Department
        public static string DeliveryDepartmentCode = "80000368";                //รหัสหน่วยงาน รข.

        public static string DeliveryDepartmentCodeAdmin = "80000368";                //รหัสหน่วยงาน ผจ. รข.
        public static string DeliveryDepartmentCodeAdmin2 = "80000369";                //รหัสหน่วยงาน ผจ. ปง.

        public static string DeliveryDepartmentCodeAdminPosition = "ผจ.ขปน.";                //ชื่อตำแหน่ง ผจ. รข.
        public static string DeliveryDepartmentCodeAdmin2Position = "ผจ.ปง.";                //ชื่อตำแหน่ง ผจ. ปง.
    }
}