﻿using System;
using System.Data;
using System.Threading;
using System.Windows.Forms;
using TMS_BLL.Transaction.Complain;
using System.Threading;
using TMS_BLL.Transaction.AutoStatus;
using TMS_BLL.Master;
using TMS_BLL.Transaction;
using TMS_AUTO_DRIVER_CHANGE.Helper;
using EmailHelper;
using System.Linq;
using System.Globalization;
using System.Text.RegularExpressions;

namespace TMS_AUTO_DRIVER_CHANGE
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitialForm();
        }

        private void InitialForm()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                VendorBLL.Instance.ErrorLogInsert("1", "Start Job", "สำเร็จ", "TMSDriverJob_Auto_Driver_Job");
                this.AutoChangeStatus();

                this.EmailComplain();
                this.ChangeStatusComplain();
                this.ContractExpire();
                this.UnlockDriverAuto();
                this.SendEmailComplainUploadLate();

                //notifyIcon1.Icon = TMS_SendEmail.Properties.Resources.mail;
                //dtpExecuteTime.Text = Properties.Settings.Default.ExecuteTime;

                //lblEnvironment.Text = string.Format(lblEnvironment.Text, Properties.Settings.Default.Environment);
                //lblVersion.Text = string.Format(lblVersion.Text, Assembly.GetEntryAssembly().GetName().Version.ToString());
                VendorBLL.Instance.ErrorLogInsert("1", "Stop Job", "สำเร็จ", "TMSDriverJob_Auto_Driver_Job");
            }
            catch (Exception ex)
            {
                VendorBLL.Instance.ErrorLogInsert("0", "Start Job Fail", ex.Message, "TMSDriverJob_Auto_Drive_Job");
            }
            finally
            {
                Thread.Sleep(2000);
                this.Cursor = Cursors.Default;
                Application.Exit();
            }
        }
        private void EmailComplain()
        {
            try
            {
                DataTable dt = ComplainBLL.Instance.ComplainSelectBLL(" AND DOC_STATUS_ID = '2'");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["IS_URGENT"].ToString() == "1")
                    {
                        if (DateTime.ParseExact(dt.Rows[i]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) == DateTime.ParseExact(dt.Rows[i]["COMPLAIN_VENDOR_DELAY"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo))
                        {
                            DataTable dtReqDoc = new DataTable();
                            dtReqDoc.Columns.Add("LINK");
                            dtReqDoc.Columns.Add("VENDOR");
                            dtReqDoc.Columns.Add("COMPLAIN");
                            dtReqDoc.Columns.Add("CONTRACT");
                            dtReqDoc.Columns.Add("CAR");
                            dtReqDoc.Columns.Add("SOURCE");
                            dtReqDoc.Columns.Add("COMPLAIN_DEPARTMENT");
                            dtReqDoc.Columns.Add("CREATE_DEPARTMENT");
                            string Link = "https://pttweb9.pttplc.com/TMSLPG/Pages/Other/" + "admin_Complain_add.aspx?Doc=" + dt.Rows[i]["DOC_ID"].ToString();// Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/Pages/Other/" + "admin_Complain_ReqDoc.aspx?DocID=" + MachineKey.Encode(Encoding.UTF8.GetBytes(DocID), MachineKeyProtection.All);
                            dtReqDoc.Rows.Add(Link, dt.Rows[i]["SVENDORID"].ToString(), dt.Rows[i]["DOC_ID"].ToString(), dt.Rows[i]["CONTRACT_NAME"].ToString(), dt.Rows[i]["SHEADREGISTERNO"].ToString(), dt.Rows[i]["WAREHOUSE_TO"].ToString(), dt.Rows[i]["SCOMPLAINFNAME"].ToString(), dt.Rows[i]["FULLNAME"].ToString());
                            this.SendEmail(11, dt.Rows[i]["DOC_ID"].ToString(), ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, 1100, dt.Rows[i]["SVENDORID"].ToString(), false, true));
                        }
                        if (DateTime.ParseExact(dt.Rows[i]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) == DateTime.ParseExact(dt.Rows[i]["CHECK_DATE_FORWARD"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo))
                        {
                            DataTable dtReqDoc = new DataTable();
                            dtReqDoc.Columns.Add("LINK");
                            dtReqDoc.Columns.Add("VENDOR");
                            dtReqDoc.Columns.Add("COMPLAIN");
                            dtReqDoc.Columns.Add("CONTRACT");
                            dtReqDoc.Columns.Add("CAR");
                            dtReqDoc.Columns.Add("SOURCE");
                            dtReqDoc.Columns.Add("COMPLAIN_DEPARTMENT");
                            dtReqDoc.Columns.Add("CREATE_DEPARTMENT");
                            string Link = "https://pttweb9.pttplc.com/TMSLPG/Pages/Other/" + "admin_Complain_add.aspx?Doc=" + dt.Rows[i]["DOC_ID"].ToString();// Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/Pages/Other/" + "admin_Complain_ReqDoc.aspx?DocID=" + MachineKey.Encode(Encoding.UTF8.GetBytes(DocID), MachineKeyProtection.All);
                            dtReqDoc.Rows.Add(Link, dt.Rows[i]["SVENDORID"].ToString(), dt.Rows[i]["DOC_ID"].ToString(), dt.Rows[i]["CONTRACT_NAME"].ToString(), dt.Rows[i]["SHEADREGISTERNO"].ToString(), dt.Rows[i]["WAREHOUSE_TO"].ToString(), dt.Rows[i]["SCOMPLAINFNAME"].ToString(), dt.Rows[i]["FULLNAME"].ToString());
                            this.SendEmail(11, dt.Rows[i]["DOC_ID"].ToString(), ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, 1100, dt.Rows[i]["SVENDORID"].ToString(), false, true));
                        }
                    }
                    else
                    {
                        if (DateTime.ParseExact(dt.Rows[i]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) == DateTime.ParseExact(dt.Rows[i]["COMPLAIN_VENDOR_NORMAL"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo))
                        {
                            DataTable dtReqDoc = new DataTable();
                            dtReqDoc.Columns.Add("LINK");
                            dtReqDoc.Columns.Add("VENDOR");
                            dtReqDoc.Columns.Add("COMPLAIN");
                            dtReqDoc.Columns.Add("CONTRACT");
                            dtReqDoc.Columns.Add("CAR");
                            dtReqDoc.Columns.Add("SOURCE");
                            dtReqDoc.Columns.Add("COMPLAIN_DEPARTMENT");
                            dtReqDoc.Columns.Add("CREATE_DEPARTMENT");
                            string Link = "";// Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/Pages/Other/" + "admin_Complain_ReqDoc.aspx?DocID=" + MachineKey.Encode(Encoding.UTF8.GetBytes(DocID), MachineKeyProtection.All);
                            dtReqDoc.Rows.Add(Link, dt.Rows[i]["SVENDORID"].ToString(), dt.Rows[i]["DOC_ID"].ToString(), dt.Rows[i]["CONTRACT_NAME"].ToString(), dt.Rows[i]["SHEADREGISTERNO"].ToString(), dt.Rows[i]["WAREHOUSE_TO"].ToString(), dt.Rows[i]["SCOMPLAINFNAME"].ToString(), dt.Rows[i]["FULLNAME"].ToString());
                            this.SendEmail(11, dt.Rows[i]["DOC_ID"].ToString(), ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, 1100, dt.Rows[i]["SVENDORID"].ToString(), false, true));
                        }
                        if (DateTime.ParseExact(dt.Rows[i]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) == DateTime.ParseExact(dt.Rows[i]["CHECK_DATE_FORWARD"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo))
                        {
                            DataTable dtReqDoc = new DataTable();
                            dtReqDoc.Columns.Add("LINK");
                            dtReqDoc.Columns.Add("VENDOR");
                            dtReqDoc.Columns.Add("COMPLAIN");
                            dtReqDoc.Columns.Add("CONTRACT");
                            dtReqDoc.Columns.Add("CAR");
                            dtReqDoc.Columns.Add("SOURCE");
                            dtReqDoc.Columns.Add("COMPLAIN_DEPARTMENT");
                            dtReqDoc.Columns.Add("CREATE_DEPARTMENT");
                            string Link = "";// Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/Pages/Other/" + "admin_Complain_ReqDoc.aspx?DocID=" + MachineKey.Encode(Encoding.UTF8.GetBytes(DocID), MachineKeyProtection.All);
                            dtReqDoc.Rows.Add(Link, dt.Rows[i]["SVENDORID"].ToString(), dt.Rows[i]["DOC_ID"].ToString(), dt.Rows[i]["CONTRACT_NAME"].ToString(), dt.Rows[i]["SHEADREGISTERNO"].ToString(), dt.Rows[i]["WAREHOUSE_TO"].ToString(), dt.Rows[i]["SCOMPLAINFNAME"].ToString(), dt.Rows[i]["FULLNAME"].ToString());
                            this.SendEmail(11, dt.Rows[i]["DOC_ID"].ToString(), ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, 1100, dt.Rows[i]["SVENDORID"].ToString(), false, true));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void ChangeStatusComplain()
        {
            try
            {
                DataTable dtChange = ComplainBLL.Instance.ComplainSelectBLL(" AND DOC_STATUS_ID IN ('1','2')");
                for (int i = 0; i < dtChange.Rows.Count; i++)
                {
                    if (dtChange.Rows[i]["IS_URGENT"].ToString() == "1")
                    {
                        if ((DateTime.ParseExact(dtChange.Rows[i]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) > DateTime.ParseExact(dtChange.Rows[i]["COMPLAIN_PTT_DELAY"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) && (int.Parse(dtChange.Rows[i]["COUNT_DOC"].ToString()) > 0)))
                        {
                            ComplainBLL.Instance.ComplainChangeStatusBLL(dtChange.Rows[i]["DOC_ID"].ToString());

                            if (dtChange.Rows[i]["LOCK_DRIVER"].ToString() == "1")
                                this.SendEmail(ConfigValue.EmailComplainCreateLock, dtChange.Rows[i]["DOC_ID"].ToString(), dtChange.Rows[i]["CC_EMAIL"].ToString());
                            else
                                this.SendEmail(ConfigValue.EmailComplainCreateNoLock, dtChange.Rows[i]["DOC_ID"].ToString(), dtChange.Rows[i]["CC_EMAIL"].ToString());
                        }
                        else if ((DateTime.ParseExact(dtChange.Rows[i]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) > DateTime.ParseExact(dtChange.Rows[i]["COMPLAIN_PTT_NORMAL"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo)) && (int.Parse(dtChange.Rows[i]["COUNT_DOC"].ToString()) == 0))
                        {
                            ComplainBLL.Instance.ComplainChangStatusBLL(dtChange.Rows[i]["DOC_ID"].ToString());
                        }
                    }
                    else
                    {
                        if ((DateTime.ParseExact(dtChange.Rows[i]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) > DateTime.ParseExact(dtChange.Rows[i]["COMPLAIN_PTT_NORMAL"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo)) && (int.Parse(dtChange.Rows[i]["COUNT_DOC"].ToString()) > 0))
                        {
                            ComplainBLL.Instance.ComplainChangeStatusBLL(dtChange.Rows[i]["DOC_ID"].ToString());

                            if (dtChange.Rows[i]["LOCK_DRIVER"].ToString() == "1")
                                this.SendEmail(ConfigValue.EmailComplainCreateLock, dtChange.Rows[i]["DOC_ID"].ToString(), dtChange.Rows[i]["CC_EMAIL"].ToString());
                            else
                                this.SendEmail(ConfigValue.EmailComplainCreateNoLock, dtChange.Rows[i]["DOC_ID"].ToString(), dtChange.Rows[i]["CC_EMAIL"].ToString());
                        }
                        else if ((DateTime.ParseExact(dtChange.Rows[i]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) > DateTime.ParseExact(dtChange.Rows[i]["COMPLAIN_PTT_NORMAL"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo)) && (int.Parse(dtChange.Rows[i]["COUNT_DOC"].ToString()) == 0))
                        {
                            ComplainBLL.Instance.ComplainChangStatusBLL(dtChange.Rows[i]["DOC_ID"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void SendEmail(int TemplateID, string DocID, string Email)
        {
            //return; // ไม่ต้องส่ง E-mail

            try
            {
                DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
                DataTable dtComplainEmail = new DataTable();
                if (dtTemplate.Rows.Count > 0)
                {
                    this.removeInvalidEmailBeforSend(Email);
                    string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                    string Body = dtTemplate.Rows[0]["BODY"].ToString();

                    if (TemplateID == ConfigValue.EmailComplainCreateNoLock)
                    {
                        dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(DocID);

                        Subject = Subject.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());

                        Body = Body.Replace("{CAR}", dtComplainEmail.Rows[0]["CAR"].ToString());
                        //Body = Body.Replace("{DRIVER}", dtComplainEmail.Rows[0]["DRIVER"].ToString());
                        Body = Body.Replace("{VENDOR}", dtComplainEmail.Rows[0]["VENDOR"].ToString());
                        Body = Body.Replace("{CONTRACT}", dtComplainEmail.Rows[0]["CONTRACT"].ToString());
                        Body = Body.Replace("{SOURCE}", dtComplainEmail.Rows[0]["WAREHOUSE"].ToString());
                        Body = Body.Replace("{COMPLAIN}", dtComplainEmail.Rows[0]["COMPLAIN"].ToString());
                        Body = Body.Replace("{COMPLAIN_DEPARTMENT}", dtComplainEmail.Rows[0]["COMPLAIN_DEPARTMENT"].ToString());
                        Body = Body.Replace("{CREATE_DEPARTMENT}", dtComplainEmail.Rows[0]["CREATE_DEPARTMENT"].ToString());
                        string Link = "https://pttweb9.pttplc.com/TMSLPG/Pages/Other/" + "admin_Complain_add.aspx?Doc=" + DocID; // Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "admin_Complain_add.aspx?DocID=" + ConfigValue.GetEncodeText(DocID);
                        Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                        string CCEmail = (!string.Equals(Email.Trim(), string.Empty)) ? "," + Email.Trim() : string.Empty;
                        string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;

                        MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(ConfigValue.DeliveryDepartmentCodeAdmin, string.Empty, ConfigValue.DeliveryDepartmentCode, int.Parse("243"), string.Empty, true, true) + CCEmail + CreateMail, Subject, Body);
                    }
                    else if (TemplateID == ConfigValue.EmailComplainCreateLock)
                    {
                        dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(DocID);

                        Subject = Subject.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());

                        Body = Body.Replace("{CAR}", dtComplainEmail.Rows[0]["CAR"].ToString());
                        //Body = Body.Replace("{DRIVER}", dtComplainEmail.Rows[0]["DRIVER"].ToString());
                        Body = Body.Replace("{VENDOR}", dtComplainEmail.Rows[0]["VENDOR"].ToString());
                        Body = Body.Replace("{CONTRACT}", dtComplainEmail.Rows[0]["CONTRACT"].ToString());
                        Body = Body.Replace("{SOURCE}", dtComplainEmail.Rows[0]["WAREHOUSE"].ToString());
                        Body = Body.Replace("{COMPLAIN}", dtComplainEmail.Rows[0]["COMPLAIN"].ToString());
                        Body = Body.Replace("{COMPLAIN_DEPARTMENT}", dtComplainEmail.Rows[0]["COMPLAIN_DEPARTMENT"].ToString());
                        Body = Body.Replace("{CREATE_DEPARTMENT}", dtComplainEmail.Rows[0]["CREATE_DEPARTMENT"].ToString());
                        Body = Body.Replace("{LOCK_DATE}", dtComplainEmail.Rows[0]["LOCK_DATE"].ToString());
                        string Link = "https://pttweb9.pttplc.com/TMSLPG/Pages/Other/" + "admin_Complain_add.aspx?Doc=" + DocID; // Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "admin_Complain_add.aspx?DocID=" + ConfigValue.GetEncodeText(DocID);
                        Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                        string CCEmail = (!string.Equals(Email.Trim(), string.Empty)) ? "," + Email.Trim() : string.Empty;
                        string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;

                        MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(ConfigValue.DeliveryDepartmentCodeAdmin, string.Empty, ConfigValue.DeliveryDepartmentCode, int.Parse("243"), dtComplainEmail.Rows[0]["VENDORID"].ToString(), true, true) + CCEmail + CreateMail, Subject, Body);
                    }
                    else if (TemplateID == 11)
                    {
                        dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(DocID);

                        Subject = Subject.Replace("{DOCID}", DocID);
                        Body = Body.Replace("{DOCID}", DocID);
                        Body = Body.Replace("{VENDOR}", dtComplainEmail.Rows[0]["VENDOR"].ToString());
                        Body = Body.Replace("{COMPLAIN}", DocID);
                        Body = Body.Replace("{CONTRACT}", dtComplainEmail.Rows[0]["CONTRACT"].ToString());
                        Body = Body.Replace("{CAR}", dtComplainEmail.Rows[0]["CAR"].ToString());
                        Body = Body.Replace("{SOURCE}", dtComplainEmail.Rows[0]["WAREHOUSE"].ToString());
                        Body = Body.Replace("{COMPLAIN_DEPARTMENT}", dtComplainEmail.Rows[0]["COMPLAIN_DEPARTMENT"].ToString());
                        Body = Body.Replace("{CREATE_DEPARTMENT}", dtComplainEmail.Rows[0]["CREATE_DEPARTMENT"].ToString());
                        string Link = "";// Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/admin_Complain_ReqDoc.aspx?DocID=" + MachineKey.Encode(Encoding.UTF8.GetBytes(DocID), MachineKeyProtection.All);
                        Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                        string CCEmail = (!string.Equals(dtComplainEmail.Rows[0]["CCEMAIL"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["CCEMAIL"].ToString() : string.Empty;
                        string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;

                        MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, 1100, dtComplainEmail.Rows[0]["VENDORID"].ToString(), false, true) + CCEmail + CreateMail, Subject, Body);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        private void removeInvalidEmailBeforSend(string Email)
        {
            if (!string.IsNullOrEmpty(Email))
            {
                Email = Email.Trim();
                var lstEmail = Email.Split(',').ToList();
                foreach (var lst in lstEmail)
                {
                    if (!IsValidEmail(lst)) lstEmail.Remove(lst);
                }
                Email = String.Join(",", lstEmail);
            }
        }
        private bool IsValidEmail(string _email)
        {
            return Regex.IsMatch(_email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        }
        private void ContractExpire()
        {
            try
            {
                DataTable dtTruck = ContractBLL.Instance.TruckContractExpireBLL();
                DataTable dtDriver = ContractBLL.Instance.DriverContractExpireBLL();
                this.DriverExpire(dtDriver);

                //set Flag = 'N'
                ContractBLL.Instance.UpdateContractExpireBLL();
            }
            catch (Exception ex)
            {

            }
        }

        private void UnlockDriverAuto()
        {
            try
            {
                DataTable dtDriver = ComplainBLL.Instance.ComplainSelectLockDriverBLL(string.Empty);

                for (int i = 0; i < dtDriver.Rows.Count; i++)
                {
                    DataTable dtDriverDetail = ComplainBLL.Instance.PersonalDetailSelectBLL(dtDriver.Rows[i]["SEMPLOYEEID"].ToString());

                    SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                    {
                        DRIVER_CODE = dtDriverDetail.Rows[0]["EMPSAPID"].ToString(),
                        PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
                        LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
                        LICENSENOE_FROM = dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(),
                        LICENSENO_TO = dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(),
                        Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
                        Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
                        FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
                        LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
                        CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
                        DRV_STATUS = ConfigValue.DriverEnable
                    };

                    string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                    string resultCheck = result.Substring(0, 1);

                    if (!string.Equals(resultCheck, "N"))
                    {
                        ComplainBLL.Instance.DriverUpdateTMSBLL(dtDriver.Rows[i]["SEMPLOYEEID"].ToString(), "0", "1", "2", "Update By System", "0");
                        ComplainBLL.Instance.UnlockDriverSaveBLL(dtDriver.Rows[i]["SEMPLOYEEID"].ToString());
                    }
                }

            }
            catch (Exception ex)
            {

            }
        }
        private void SendEmailComplainUploadLate()
        {
            try
            {
                DataTable dtDriver = ComplainBLL.Instance.UploadDocLateSelectBLL(string.Empty);
                if (dtDriver.Rows.Count > 0)
                {
                    for (int i = 0; i < dtDriver.Rows.Count; i++)
                        this.SendEmail(11, dtDriver.Rows[i]["DOC_ID"].ToString(), dtDriver.Rows[i]["VENDOR_NAME"].ToString(), dtDriver.Rows[i]["SCONTRACTNO"].ToString(), dtDriver.Rows[i]["SHEADREGISTERNO"].ToString(), dtDriver.Rows[i]["OrganizName"].ToString(), dtDriver.Rows[i]["CC_EMAIL"].ToString());
                }
            }
            catch (Exception ex)
            {

            }
        }
        private void SendEmail(int TemplateID, string DocID, string Vendor, string Contract, string Sheadregisterno, string Organiz, string CCEmailData)
        {
            //return; // ไม่ต้องส่ง E-mail

            try
            {
                DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
                DataTable dtComplainEmail = new DataTable();
                if (dtTemplate.Rows.Count > 0)
                {
                    string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                    string Body = dtTemplate.Rows[0]["BODY"].ToString();

                    if (TemplateID == 11)
                    {
                        dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(DocID);

                        Subject = Subject.Replace("{DOCID}", DocID);
                        Body = Body.Replace("{DOCID}", DocID);
                        Body = Body.Replace("{VENDOR}", Vendor);
                        Body = Body.Replace("{COMPLAIN}", DocID);
                        Body = Body.Replace("{CONTRACT}", Contract);
                        Body = Body.Replace("{CAR}", Sheadregisterno);
                        Body = Body.Replace("{SOURCE}", Organiz);
                        Body = Body.Replace("{COMPLAIN_DEPARTMENT}", dtComplainEmail.Rows[0]["COMPLAIN_DEPARTMENT"].ToString());
                        Body = Body.Replace("{CREATE_DEPARTMENT}", dtComplainEmail.Rows[0]["CREATE_DEPARTMENT"].ToString());
                        string Link = "";// "https://ptttms.pttplc.com/TMS/admin_Complain_ReqDoc.aspx?DocID=" + MachineKey.Encode(Encoding.UTF8.GetBytes(DocID), MachineKeyProtection.All);
                        Body = Body.Replace("{LINK}", "<a href=\"" + Link + "\">คลิกที่นี่</a>");

                        string CCEmail = (!string.Equals(CCEmailData, string.Empty)) ? "," + CCEmailData : string.Empty;
                        string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;

                        MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, 1100, dtComplainEmail.Rows[0]["VENDORID"].ToString(), false, true) + CCEmail + CreateMail, Subject, Body);
                    }
                }
            }
            catch (Exception ex)
            {
                //alertFail(ex.Message);
            }
        }
        private void DriverExpire(DataTable dtDriver)
        {
            try
            {
                string result = string.Empty;
                string[] resultCheck;

                dtDriver.Columns.Add("STATUS");
                dtDriver.Columns.Add("MESSAGE");
                for (int i = 0; i < dtDriver.Rows.Count; i++)
                {
                    string DriverStatus = string.Empty;
                    if (string.Equals(dtDriver.Rows[i]["DRVSTATUS_TMS"].ToString(), "1"))
                        DriverStatus = " ";
                    else if (string.Equals(dtDriver.Rows[i]["DRVSTATUS_TMS"].ToString(), "0"))
                        DriverStatus = "1";
                    else if (string.Equals(dtDriver.Rows[i]["DRVSTATUS_TMS"].ToString(), "2"))
                        DriverStatus = "2";

                    try
                    {
                        SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                        {
                            DRIVER_CODE = dtDriver.Rows[i]["DRIVER_CODE"].ToString(),
                            PERS_CODE = dtDriver.Rows[i]["PERS_CODE"].ToString(),
                            LICENSE_NO = dtDriver.Rows[i]["LICENSE_NO"].ToString(),
                            LICENSENOE_FROM = dtDriver.Rows[i]["VALID_FROM"].ToString(),
                            LICENSENO_TO = dtDriver.Rows[i]["VALID_TO"].ToString(),
                            Phone_1 = (string.Equals(dtDriver.Rows[i]["STEL"].ToString(), string.Empty)) ? "-" : dtDriver.Rows[i]["STEL"].ToString(),
                            Phone_2 = (string.Equals(dtDriver.Rows[i]["STEL2"].ToString(), string.Empty)) ? "-" : dtDriver.Rows[i]["STEL2"].ToString(),
                            FIRST_NAME = dtDriver.Rows[i]["FIRST_NAME"].ToString(),
                            LAST_NAME = dtDriver.Rows[i]["LAST_NAME"].ToString(),
                            //CARRIER = dtDriver.Rows[i]["STRANS_ID"].ToString(),
                            CARRIER = string.Empty,
                            DRV_STATUS = DriverStatus
                        };

                        result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                        resultCheck = result.Split(':');

                        dtDriver.Rows[i]["STATUS"] = resultCheck[0];
                        if (string.Equals(dtDriver.Rows[i]["STATUS"].ToString(), "Y"))
                            dtDriver.Rows[i]["MESSAGE"] = string.Empty;
                        else
                            dtDriver.Rows[i]["MESSAGE"] = resultCheck[1];

                        VendorBLL.Instance.VendorUpdateInuseBySemployeeid(dtDriver.Rows[i]["SEMPLOYEEID"].ToString() + string.Empty, "0");

                        #region Log
                        string Detail = "ยกเลิกงานจ้างงาน พขร.<br/>";
                        Detail += "ชื่อ : " + dtDriver.Rows[i]["FIRST_NAME"].ToString() + " " + dtDriver.Rows[i]["LAST_NAME"].ToString() + "<br/>";

                        //Detail += "บริษัท : ผขส. ขอเปลี่ยนข้อมูล<br/>";
                        VendorBLL.Instance.LogInsert(Detail, dtDriver.Rows[i]["PERS_CODE"].ToString(), null, "0", "0", 0, dtDriver.Rows[i]["STRANS_ID"].ToString(), 0);
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        dtDriver.Rows[i]["STATUS"] = "N";
                        dtDriver.Rows[i]["MESSAGE"] = ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            this.Show();
            WindowState = FormWindowState.Normal;
            this.BringToFront();
        }

        private void frmMain_Shown(object sender, EventArgs e)
        {
            //WindowState = FormWindowState.Minimized;
            //Hide();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                Properties.Settings.Default["ExecuteTime"] = dtpExecuteTime.Text;
                Properties.Settings.Default.Save();

                MessageBox.Show("Save Success...");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (string.Equals(DateTime.Now.ToString("HH:mm"), dtpExecuteTime.Text))
                    this.AutoChangeStatus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmdSendNow_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการปรับสถานะ พขร. ทันที ใช่หรือไม่", "ยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    this.AutoChangeStatus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void AutoChangeStatus()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string result = string.Empty;
                string[] resultCheck;

                DataTable dtDriver = ComplainBLL.Instance.GetDriverConfictBLL();
                dtDriver.Columns.Add("STATUS");
                dtDriver.Columns.Add("MESSAGE");
                for (int i = 0; i < dtDriver.Rows.Count; i++)
                {
                    string DriverStatus = string.Empty;
                    if (string.Equals(dtDriver.Rows[i]["DRVSTATUS_TMS"].ToString(), "1"))
                        DriverStatus = " ";
                    else if (string.Equals(dtDriver.Rows[i]["DRVSTATUS_TMS"].ToString(), "0"))
                        DriverStatus = "1";
                    else if (string.Equals(dtDriver.Rows[i]["DRVSTATUS_TMS"].ToString(), "2"))
                        DriverStatus = "2";

                    try
                    {
                        SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                        {
                            DRIVER_CODE = dtDriver.Rows[i]["DRIVER_CODE"].ToString(),
                            PERS_CODE = dtDriver.Rows[i]["PERS_CODE"].ToString(),
                            LICENSE_NO = dtDriver.Rows[i]["LICENSE_NO"].ToString(),
                            LICENSENOE_FROM = dtDriver.Rows[i]["VALID_FROM"].ToString(),
                            LICENSENO_TO = dtDriver.Rows[i]["VALID_TO"].ToString(),
                            Phone_1 = (string.Equals(dtDriver.Rows[i]["STEL"].ToString(), string.Empty)) ? "-" : dtDriver.Rows[i]["STEL"].ToString(),
                            Phone_2 = (string.Equals(dtDriver.Rows[i]["STEL2"].ToString(), string.Empty)) ? "-" : dtDriver.Rows[i]["STEL2"].ToString(),
                            FIRST_NAME = dtDriver.Rows[i]["FIRST_NAME"].ToString(),
                            LAST_NAME = dtDriver.Rows[i]["LAST_NAME"].ToString(),
                            CARRIER = dtDriver.Rows[i]["STRANS_ID"].ToString(),
                            DRV_STATUS = DriverStatus



                            //DRIVER_CODE = "5500006159",
                            //PERS_CODE = "1251288843327",
                            //LICENSE_NO = "สบ.00227/53",
                            //LICENSENOE_FROM = "25/01/2016",
                            //LICENSENO_TO = "25/02/2017",
                            //Phone_1 = "088",
                            //Phone_2 = "089",
                            //FIRST_NAME = "sakchai",
                            //LAST_NAME = "weaweawe",
                            //CARRIER = "0010001778",
                            //DRV_STATUS = "2"
                        };

                        result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                        resultCheck = result.Split(':');

                        dtDriver.Rows[i]["STATUS"] = resultCheck[0];
                        if (string.Equals(dtDriver.Rows[i]["STATUS"].ToString(), "Y"))
                            dtDriver.Rows[i]["MESSAGE"] = string.Empty;
                        else
                            dtDriver.Rows[i]["MESSAGE"] = resultCheck[1];



                        VendorBLL.Instance.ErrorLogInsert("1", "Update Job", "สำเร็จ อัพเดทสถานะ พชร." + dtDriver.Rows[i]["DRIVER_CODE"].ToString(), "TMSDriverJob_Auto_Driver_Job");

                    }
                    catch (Exception ex)
                    {
                        dtDriver.Rows[i]["STATUS"] = "N";
                        dtDriver.Rows[i]["MESSAGE"] = ex.Message;
                        VendorBLL.Instance.ErrorLogInsert("0", "Update Fail", ex.Message, "TMSDriverJob_Auto_Driver_Job");
                    }
                }

                AutoStatusBLL.Instance.DriverLogInsertBLL(dtDriver);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //private void SendEmail()
        //{
        //    this.Cursor = Cursors.WaitCursor;
        //    try
        //    {
        //        int TemplateID;
        //        DataTable dtEmail;

        //        TemplateID = 3;                                                                     //รายงานประจำเดือน
        //        dtEmail = SendEmailBLL.Instance.SendEmailReportSelectBLL(TemplateID, 1);
        //        this.SendEmailReport(dtEmail);

        //        TemplateID = 4;                                                                     //รายงานประจำ 3 เดือน
        //        dtEmail = SendEmailBLL.Instance.SendEmailReportSelectBLL(TemplateID, 2);
        //        this.SendEmailReport(dtEmail);

        //        TemplateID = 5;                                                                     //รายงานประจำ 6 เดือน
        //        dtEmail = SendEmailBLL.Instance.SendEmailReportSelectBLL(TemplateID, 3);
        //        this.SendEmailReport(dtEmail);

        //        TemplateID = 21;                                                                     //แจ้งเตือน ปตท ให้ตรวจรายงาน
        //        dtEmail = SendEmailBLL.Instance.SendEmailReportSelectBLL(TemplateID, 0);
        //        if (dtEmail.Rows.Count > 0)
        //            this.SendWarningPTT(TemplateID);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        this.Cursor = Cursors.Default;
        //    }
        //}

        //private void SendEmailReport(DataTable dtEmail)
        //{
        //    try
        //    {
        //        for (int i = 0; i < dtEmail.Rows.Count; i++)
        //        {
        //            //MailService.SendMail(dtEmail.Rows[i]["EMAIL"].ToString() + ", zsakchai.p@pttict.com", dtTemplate.Rows[0]["SUBJECT"].ToString(), Body);
        //            MailService.SendMail("zsakchai.p@pttict.com," + dtEmail.Rows[i]["EMAIL"].ToString(), dtEmail.Rows[0]["SUBJECT"].ToString(), dtEmail.Rows[0]["BODY"].ToString());
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //throw new Exception(ex.Message);
        //    }
        //}

        //private void SendWarningPTT(int TemplateID)
        //{
        //    try
        //    {
        //        DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
        //        if (dtTemplate.Rows.Count > 0)
        //        {
        //            string Email = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, "80000368", 0, string.Empty, false);

        //            string Body = dtTemplate.Rows[0]["BODY"].ToString();

        //            MailService.SendMail("zsakchai.p@pttict.com,zsomchart.j@pttict.com," + Email, dtTemplate.Rows[0]["SUBJECT"].ToString(), Body);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //throw new Exception(ex.Message);
        //    }
        //}
    }
}